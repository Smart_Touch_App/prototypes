﻿using iAnywhere.MobiLink.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SyncEngine
{
    public class Synchronizer
    {
        CancellationTokenSource cts = new CancellationTokenSource();
        CancellationToken _ct;

        private static Dictionary<string, DeploymentLib.SyncManager> _Clients = new Dictionary<string, DeploymentLib.SyncManager>();
        private BlockingCollection<_Request> _SyncRequests = new BlockingCollection<_Request>();
        private List<_Request> _RequestResults = new List<_Request>();

        public struct _Request
        {
            public string RequestID;
            public string Publication;
            public string SQLAnywherePath;
            public string CMDLine;
            public int ClientPort;
            public string ServerHost;
            public int ServerPort;
            public string User;
            public string Password;

            public DateTime SyncStartedOn;
            public DateTime SyncEndedOn;
            public TimeSpan Elapsed;

        }


        public int SyncClientsCount
        {
            get { return _Clients.Count(); }
        }


        public List<_Request> RequestResults
        {
            get { return _RequestResults; }
        }

        public void Enqueue(_Request request)
        {
            _SyncRequests.Add(request);
        }

        public Task Synchronize { get; set; }

        public Synchronizer()
        {

        }

        public bool Start()
        {
            _ct = cts.Token;
            Synchronize = Task.Factory.StartNew((c) =>
           {
               while (true)
               {

                   try
                   {
                       var Request = new _Request();
                       if (_SyncRequests.TryTake(out Request, -1, _ct))
                       {
                           Task.Factory.StartNew(() =>
                           {
                               var Req = Request;
                               var STx = System.Diagnostics.Stopwatch.StartNew();
                               Req.SyncStartedOn = DateTime.Now;
                               DeploymentLib.SyncManager Man = StartClient(Req);
                               if (Man.Sync(Req.Publication, 300000))
                               {
                                   Req.SyncEndedOn = DateTime.Now;
                                   Req.Elapsed = STx.Elapsed;
                                   _RequestResults.Add(Req);
                               }
                           });
                       }
                   }
                   catch
                   {
                       break;
                   }
               }


           }, _ct, TaskCreationOptions.LongRunning);

            return true;
        }




        private DeploymentLib.SyncManager StartClient(_Request Request)
        {
            string Key = string.Format("{0}{1}", Request.ClientPort, Request.CMDLine);
            if (!_Clients.ContainsKey(Key))
            {
                try
                {
                    DeploymentLib.SyncManager man = new DeploymentLib.SyncManager();
                    man.StartClient(Request.SQLAnywherePath, Request.CMDLine, Request.ClientPort, Request.ServerHost, Request.ServerPort, Request.User, Request.Password);
                    while (!(man.isStarted && man.isConnected))
                    {
                        System.Threading.Thread.Sleep(50);
                    }

                   while(! Monitor.TryEnter(_Clients))
                    {

                    }
                    _Clients.Add(Key, man);
                    Monitor.Exit(_Clients);

                }
                catch
                {

                    throw;
                }
            }

            return _Clients[Key];

        }




    }

}

