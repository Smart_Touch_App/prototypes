﻿namespace cuiTestProject
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;


    public partial class UIMap
    {

        /// <summary>
        /// RecordedOrderCreation - Use 'RecordedOrderCreationParams' to pass parameters into this method.
        /// </summary>
        public void RecordedOrderCreation()
        {
            #region Variable Declarations
            WinWindow uIUserLoginWindow = this.UIUserLoginWindow;
            WpfText uILoginIDText = this.UIUserLoginWindow1.UILoginIDText;
            WpfEdit uIItemEdit = this.UIUserLoginWindow1.UIItemEdit;
            WpfText uILoginText = this.UIUserLoginWindow1.UILoginText;
            WpfText uIServiceRouteText = this.UIRouteHomeWindow.UIServiceRouteText;
            WinWindow uIServiceRouteWindow = this.UIServiceRouteWindow;
            WpfText uICUSTOMERText = this.UIServiceRouteWindow1.UICUSTOMERText;
            WpfText uIOrderTemplateText = this.UICustomerHomeWindow.UIOrderTemplateText;
            WinWindow uIOrderTemplateWindow = this.UIOrderTemplateWindow;
            WpfCheckBox uIItemCheckBox = this.UIOrderTemplateWindow1.UIItemCheckBox;
            WpfCheckBox uIItemCheckBox1 = this.UIOrderTemplateWindow1.UIItemCheckBox1;
            WpfText uIPlaceOrderText = this.UIOrderTemplateWindow1.UIPlaceOrderText;
            WpfText uIOrderNo7831Text = this.UIOrderWindow.UIOrderNo7831Text;
            WpfCheckBox uIItemCheckBox2 = this.UIOrderWindow.UIItemCheckBox;
            WpfWindow uIOrderWindow = this.UIOrderWindow;
            WpfText uIAddText = this.UIOrderWindow.UIAddText;
            WpfText uIOKText = this.UIAlertWindow.UIOKText;
            WinWindow uIOrderWindow1 = this.UIOrderWindow1;
            WpfText uISearchhereText = this.UIOrderWindow.UISearchhereText;
            WpfText uISalesCat1Text = this.UIOrderWindow.UISalesCat1Text;
            WpfButton uIItemButton = this.UIOrderWindow.UIItemButton;
            WpfCheckBox uIItemCheckBox11 = this.UIOrderWindow.UIItemCheckBox1;
            WpfCheckBox uIItemCheckBox21 = this.UIOrderWindow.UIItemCheckBox2;
            WpfText uIPreviewText = this.UIOrderWindow.UIPreviewText;
            WpfEdit uIItemEdit1 = this.UIPreviewOrderWindow.UIItemEdit;
            WpfWindow uIPreviewOrderWindow = this.UIPreviewOrderWindow;
            WpfText uICompleteandSignText = this.UIPreviewOrderWindow.UICompleteandSignText;
            WpfCustom uIItemCustom = this.UIPreviewOrderWindow.UITestInkCanvasCustom.UIItemCustom;
            WpfText uIAcceptText = this.UIPreviewOrderWindow.UIAcceptText;
            WpfText uIDeliverToCustomerText = this.UIPickOrderWindow.UIDeliverToCustomerText;
            WpfText uIItem0EAText = this.UIPickOrderWindow.UIItem0EAText;
            WpfText uIManualPickText = this.UIPickOrderWindow.UIManualPickText;
            WpfWindow uIWpfWindow = this.UIPickOrderWindow.UIWpfWindow;
            WpfText uIContinueText = this.UIPickOrderWindow.UIContinueText;
            WpfWindow uIPickOrderWindow = this.UIPickOrderWindow;
            WpfText uIScannerOffText = this.UIPickOrderWindow.UIScannerOffText;
            WpfEdit uITextboxEdit = this.UIPickOrderWindow.UITextboxEdit;
            WpfEdit uITextboxEdit1 = this.UIPickOrderWindow.UITextboxEdit1;
            WpfText uIEAText = this.UIPickOrderWindow.UIEAText;
            WpfText uIPrintPickSlipText = this.UIPickOrderWindow.UIPrintPickSlipText;
            WpfText uIPrintInvoiceText = this.UIPickOrderWindow.UIPrintInvoiceText;
            WpfText uICSText = this.UIPickOrderWindow.UICSText;
            WpfText uIItemText = this.UIPickOrderWindow.UIItemCustom.UIItemText;
            WpfEdit uITxt_OrderQtyEdit = this.UIPickOrderWindow.UITxt_OrderQtyEdit;
            WpfText uICSText1 = this.UIPickOrderWindow.UICSText1;
            WpfText uIEAText1 = this.UIPickOrderWindow.UIEAText1;
            WpfText uIHoldText = this.UIPickOrderWindow.UIHoldText;
            WpfText uIOKText1 = this.UIConfirmWindow.UIOKText;
            WpfText uIOrderHistoryText = this.UICustomerHomeWindow.UIOrderHistoryText;
            WpfImage uIItemImage = this.UICustomerHomeWindow.UIItemImage;
            WpfText uIPricingText = this.UICustomerHomeWindow.UIPricingText;
            WpfText uIBranch1783Route783Text = this.UICustomerHomeWindow.UINavigationHeaderCustom.UIBranch1783Route783Text;
            WpfText uIItem1Text = this.UICustomerHomeWindow.UIItem1Text;
            WpfText uIThursday04162015Text = this.UICustomerHomeWindow.UIThursday04162015Text;
            WpfCustom uINavigationHeaderCustom = this.UICustomerHomeWindow.UINavigationHeaderCustom;
            WinEdit uIItemEdit2 = this.UIFBM783waitingSQLAnywWindow.UIItemWindow.UIItemEdit;
            WinButton uIShutdownButton = this.UIFBM783waitingSQLAnywWindow.UIShutdownWindow.UIShutdownButton;
            WpfText uIXText = this.UICustomerHomeWindow.UINavigationHeaderCustom.UIXText;
            #endregion

            // Type '{CapsLock}' in 'UserLogin' window
            Keyboard.SendKeys(uIUserLoginWindow, this.RecordedOrderCreationParams.UIUserLoginWindowSendKeys, ModifierKeys.None);

            // Click 'Login ID' label
            Mouse.Click(uILoginIDText, new Point(51, 9));

            // Click text box
            Mouse.Click(uIItemEdit, new Point(39, 19));

            // Type '********' in text box
            Keyboard.SendKeys(uIItemEdit, this.RecordedOrderCreationParams.UIItemEditSendKeys, true);

            // Type 'fbm783' in 'UserLogin' window
            Keyboard.SendKeys(uIUserLoginWindow, this.RecordedOrderCreationParams.UIUserLoginWindowSendKeys1, ModifierKeys.None);

            // Click 'Login' label
            Mouse.Click(uILoginText, new Point(4, 12));

            // Click 'Service Route' label
            Mouse.Click(uIServiceRouteText, new Point(60, 6));

            // Type '{CapsLock}' in 'ServiceRoute' window
            Keyboard.SendKeys(uIServiceRouteWindow, this.RecordedOrderCreationParams.UIServiceRouteWindowSendKeys, ModifierKeys.None);

            // Click 'CUSTOMER' label
            Mouse.Click(uICUSTOMERText, new Point(55, 8));

            // Type '{CapsLock}' in 'ServiceRoute' window
            Keyboard.SendKeys(uIServiceRouteWindow, this.RecordedOrderCreationParams.UIServiceRouteWindowSendKeys1, ModifierKeys.None);

            // Double-Click 'CUSTOMER' label
            Mouse.DoubleClick(uICUSTOMERText, new Point(52, 10));

            // Click 'Order Template' label
            Mouse.Click(uIOrderTemplateText, new Point(64, 14));

            // Type '{CapsLock}' in 'OrderTemplate' window
            Keyboard.SendKeys(uIOrderTemplateWindow, this.RecordedOrderCreationParams.UIOrderTemplateWindowSendKeys, ModifierKeys.None);

            // Select check box numbered 2 in 'OrderTemplate' window
            uIItemCheckBox.Checked = this.RecordedOrderCreationParams.UIItemCheckBoxChecked;

            // Select check box numbered 10 in 'OrderTemplate' window
            uIItemCheckBox1.Checked = this.RecordedOrderCreationParams.UIItemCheckBox1Checked;

            // Click 'Place Order' label
            Mouse.Click(uIPlaceOrderText, new Point(10, 14));

            // Click 'Order No. 7831' label
            Mouse.Click(uIOrderNo7831Text, new Point(395, 22));

            // Clear check box
            uIItemCheckBox2.Checked = this.RecordedOrderCreationParams.UIItemCheckBoxChecked1;

            // Click 'Order' window
            Mouse.Click(uIOrderWindow, new Point(146, 227));

            // Click 'Order' window
            Mouse.Click(uIOrderWindow, new Point(79, 227));

            // Click 'Order' window
            Mouse.Click(uIOrderWindow, new Point(22, 743));

            // Click 'Add' label
            Mouse.Click(uIAddText, new Point(2, 9));

            // Click 'OK' label
            Mouse.Click(uIOKText, new Point(12, 11));

            // Clear check box
            uIItemCheckBox2.Checked = this.RecordedOrderCreationParams.UIItemCheckBoxChecked2;

            // Click 'Order' window
            Mouse.Click(uIOrderWindow, new Point(59, 229));

            // Right-Click 'Order' window
            Mouse.Click(uIOrderWindow, MouseButtons.Right, ModifierKeys.None, new Point(59, 229));

            // Type '{Right}' in 'Order' window
            Keyboard.SendKeys(uIOrderWindow1, this.RecordedOrderCreationParams.UIOrderWindow1SendKeys, ModifierKeys.None);

            // Click 'Order' window
            Mouse.Click(uIOrderWindow, new Point(131, 224));

            // Double-Click 'Order' window
            Mouse.DoubleClick(uIOrderWindow, new Point(131, 225));

            // Click 'Order' window
            Mouse.Click(uIOrderWindow, new Point(131, 225));

            // Right-Click 'Order' window
            Mouse.Click(uIOrderWindow, MouseButtons.Right, ModifierKeys.None, new Point(131, 225));

            // Click 'Search here...' label
            Mouse.Click(uISearchhereText, new Point(112, 8));

            // Click 'Sales Cat 1' label
            Mouse.Click(uISalesCat1Text, new Point(35, 11));

            // Click 'Search here...' label
            Mouse.Click(uISearchhereText, new Point(114, 7));

            // Type 'A{Enter}' in 'Order' window
            Keyboard.SendKeys(uIOrderWindow1, this.RecordedOrderCreationParams.UIOrderWindow1SendKeys1, ModifierKeys.None);

            // Click button numbered 8 in 'Order' window
            Mouse.Click(uIItemButton, new Point(29, 15));

            // Select check box numbered 3 in 'Order' window
            uIItemCheckBox11.Checked = this.RecordedOrderCreationParams.UIItemCheckBox1Checked1;

            // Select check box numbered 4 in 'Order' window
            uIItemCheckBox21.Checked = this.RecordedOrderCreationParams.UIItemCheckBox2Checked;

            // Click 'Order' window
            Mouse.Click(uIOrderWindow, new Point(1298, 485));

            // Click 'Preview' label
            Mouse.Click(uIPreviewText, new Point(4, 16));

            // Type '' in text box
            uIItemEdit1.Text = this.RecordedOrderCreationParams.UIItemEditText;

            // Click 'PreviewOrder' window
            Mouse.Click(uIPreviewOrderWindow, new Point(984, 443));

            // Click 'PreviewOrder' window
            Mouse.Click(uIPreviewOrderWindow, new Point(822, 469));

            // Click 'Complete and Sign' label
            Mouse.Click(uICompleteandSignText, new Point(29, 5));

            // Click custom control
            Mouse.Click(uIItemCustom, new Point(153, 120));

            // Move custom control
            //System parameter 'Show window contents while dragging' is not set.This could lead to incorrect recording of drag actions.
            Mouse.StartDragging(uIItemCustom, new Point(98, 137));
            Mouse.StopDragging(uIItemCustom, 84, 21);

            // Move custom control
            //System parameter 'Show window contents while dragging' is not set.This could lead to incorrect recording of drag actions.
            Mouse.StartDragging(uIItemCustom, new Point(143, 137));
            Mouse.StopDragging(uIItemCustom, 19, -6);

            // Click 'Accept' label
            Mouse.Click(uIAcceptText, new Point(15, 11));

            // Click 'Deliver To Customer' label
            Mouse.Click(uIDeliverToCustomerText, new Point(47, 17));

            // Click 'Deliver To Customer' label
            Mouse.Click(uIDeliverToCustomerText, new Point(41, 13));

            // Click '0 EA' label
            Mouse.Click(uIItem0EAText, new Point(5, 11));

            // Click 'Manual Pick' label
            Mouse.Click(uIManualPickText, new Point(19, 12));

            // Click 'Wpf' window
            Mouse.Click(uIWpfWindow, new Point(141, 159));

            // Click 'Wpf' window
            Mouse.Click(uIWpfWindow, new Point(277, 122));

            // Last action on list item was not recorded because the control does not have any good identification property.

            // Click 'Continue' label
            Mouse.Click(uIContinueText, new Point(8, 8));

            // Click '0 EA' label
            Mouse.Click(uIItem0EAText, new Point(4, 12));

            // Double-Click '0 EA' label
            Mouse.DoubleClick(uIItem0EAText, new Point(4, 12));

            // Click '0 EA' label
            Mouse.Click(uIItem0EAText, new Point(4, 12));

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(137, 275));

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(941, 270));

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(282, 158));

            // Click 'Scanner Off' label
            Mouse.Click(uIScannerOffText, new Point(5, 15));

            // Type '1' in 'textbox' text box
            uITextboxEdit.Text = this.RecordedOrderCreationParams.UITextboxEditText;

            // Type '1' in 'textbox' text box
            uITextboxEdit1.Text = this.RecordedOrderCreationParams.UITextboxEdit1Text;

            // Click 'EA' label
            Mouse.Click(uIEAText, new Point(27, 14));

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(917, 198));

            // Click 'Scanner Off' label
            Mouse.Click(uIScannerOffText, new Point(12, 13));

            // Click 'Print Pick Slip' label
            Mouse.Click(uIPrintPickSlipText, new Point(15, 13));

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(1038, 89));

            // Double-Click 'Deliver To Customer' label
            Mouse.DoubleClick(uIDeliverToCustomerText, new Point(28, 12));

            // Click 'Scanner Off' label
            Mouse.Click(uIScannerOffText, new Point(25, 7));

            // Click 'Print Invoice' label
            Mouse.Click(uIPrintInvoiceText, new Point(38, 8));

            // Click 'Print Pick Slip' label
            Mouse.Click(uIPrintPickSlipText, new Point(59, 12));

            // Click 'Deliver To Customer' label
            Mouse.Click(uIDeliverToCustomerText, new Point(41, 3));

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(719, 277));

            // Click 'CS' label
            Mouse.Click(uICSText, new Point(2, 29));

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(1238, 503));

            // Click '↲' label
            Mouse.Click(uIItemText, new Point(4, 19));

            // Type '{Enter}' in 'txt_OrderQty' text box
            Keyboard.SendKeys(uITxt_OrderQtyEdit, this.RecordedOrderCreationParams.UITxt_OrderQtyEditSendKeys, ModifierKeys.None);

            // Last mouse action was not recorded.

            // Click 'Pick Order' window
            Mouse.Click(uIPickOrderWindow, new Point(179, 293));

            // Type '{Enter}' in 'txt_OrderQty' text box
            Keyboard.SendKeys(uITxt_OrderQtyEdit, this.RecordedOrderCreationParams.UITxt_OrderQtyEditSendKeys1, ModifierKeys.None);

            // Click 'CS' label
            Mouse.Click(uICSText1, new Point(53, 16));

            // Click 'textbox' text box
            Mouse.Click(uITextboxEdit, new Point(13, 28));

            // Type '1' in 'textbox' text box
            uITextboxEdit1.Text = this.RecordedOrderCreationParams.UITextboxEdit1Text1;

            // Click 'EA' label
            Mouse.Click(uIEAText1, new Point(35, 16));

            // Click 'Print Pick Slip' label
            Mouse.Click(uIPrintPickSlipText, new Point(21, 12));

            // Click 'Deliver To Customer' label
            Mouse.Click(uIDeliverToCustomerText, new Point(25, 13));

            // Click 'Hold' label
            Mouse.Click(uIHoldText, new Point(4, 13));

            // Click 'OK' label
            Mouse.Click(uIOKText1, new Point(8, 14));

            // Click 'Order History' label
            Mouse.Click(uIOrderHistoryText, new Point(31, 14));

            // Click image
            Mouse.Click(uIItemImage, new Point(12, 22));

            // Click 'Pricing' label
            Mouse.Click(uIPricingText, new Point(35, 14));

            // Click 'Branch - 1783 | Route - 783' label
            Mouse.Click(uIBranch1783Route783Text, new Point(151, 8));

            // Click '1' label
            Mouse.Click(uIItem1Text, new Point(2, 7));

            // Click 'Thursday, 04/16/2015' label
            Mouse.Click(uIThursday04162015Text, new Point(102, 12));

            // Click 'NavigationHeader' custom control
            Mouse.Click(uINavigationHeaderCustom, new Point(1319, 8));

            // Click text box
            Mouse.Click(uIItemEdit2, new Point(806, 165));

            // Click 'S&hut down' button
            Mouse.Click(uIShutdownButton, new Point(30, 8));

            // Click 'x' label
            Mouse.Click(uIXText, new Point(2, 9));

            // Click 'OK' label
            Mouse.Click(uIOKText1, new Point(10, 15));
        }

        public virtual RecordedOrderCreationParams RecordedOrderCreationParams
        {
            get
            {
                if ((this.mRecordedOrderCreationParams == null))
                {
                    this.mRecordedOrderCreationParams = new RecordedOrderCreationParams();
                }
                return this.mRecordedOrderCreationParams;
            }
        }

        private RecordedOrderCreationParams mRecordedOrderCreationParams;
    }
    /// <summary>
    /// Parameters to be passed into 'RecordedOrderCreation'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "12.0.31101.0")]
    public class RecordedOrderCreationParams
    {

        #region Fields
        /// <summary>
        /// Type '{CapsLock}' in 'UserLogin' window
        /// </summary>
        public string UIUserLoginWindowSendKeys = "{CapsLock}";

        /// <summary>
        /// Type '********' in text box
        /// </summary>
        public string UIItemEditSendKeys = "euX4sCJh368ZqJJ3VFBGEQij0rqbhb8gLWPvUINOhzqcgPDRGDSk9JQIHr9lIToRbFctfKGQ7jk=";

        /// <summary>
        /// Type 'fbm783' in 'UserLogin' window
        /// </summary>
        public string UIUserLoginWindowSendKeys1 = "fbm783";

        /// <summary>
        /// Type '{CapsLock}' in 'ServiceRoute' window
        /// </summary>
        public string UIServiceRouteWindowSendKeys = "{CapsLock}";

        /// <summary>
        /// Type '{CapsLock}' in 'ServiceRoute' window
        /// </summary>
        public string UIServiceRouteWindowSendKeys1 = "{CapsLock}";

        /// <summary>
        /// Type '{CapsLock}' in 'OrderTemplate' window
        /// </summary>
        public string UIOrderTemplateWindowSendKeys = "{CapsLock}";

        /// <summary>
        /// Select check box numbered 2 in 'OrderTemplate' window
        /// </summary>
        public bool UIItemCheckBoxChecked = true;

        /// <summary>
        /// Select check box numbered 10 in 'OrderTemplate' window
        /// </summary>
        public bool UIItemCheckBox1Checked = true;

        /// <summary>
        /// Clear check box
        /// </summary>
        public bool UIItemCheckBoxChecked1 = false;

        /// <summary>
        /// Clear check box
        /// </summary>
        public bool UIItemCheckBoxChecked2 = false;

        /// <summary>
        /// Type '{Right}' in 'Order' window
        /// </summary>
        public string UIOrderWindow1SendKeys = "{Right}";

        /// <summary>
        /// Type 'A{Enter}' in 'Order' window
        /// </summary>
        public string UIOrderWindow1SendKeys1 = "A{Enter}";

        /// <summary>
        /// Select check box numbered 3 in 'Order' window
        /// </summary>
        public bool UIItemCheckBox1Checked1 = true;

        /// <summary>
        /// Select check box numbered 4 in 'Order' window
        /// </summary>
        public bool UIItemCheckBox2Checked = true;

        /// <summary>
        /// Type '' in text box
        /// </summary>
        public string UIItemEditText = "";

        /// <summary>
        /// Type '1' in 'textbox' text box
        /// </summary>
        public string UITextboxEditText = "1";

        /// <summary>
        /// Type '1' in 'textbox' text box
        /// </summary>
        public string UITextboxEdit1Text = "1";

        /// <summary>
        /// Type '{Enter}' in 'txt_OrderQty' text box
        /// </summary>
        public string UITxt_OrderQtyEditSendKeys = "{Enter}";

        /// <summary>
        /// Type '{Enter}' in 'txt_OrderQty' text box
        /// </summary>
        public string UITxt_OrderQtyEditSendKeys1 = "{Enter}";

        /// <summary>
        /// Type '1' in 'textbox' text box
        /// </summary>
        public string UITextboxEdit1Text1 = "1";
        #endregion
}
}
