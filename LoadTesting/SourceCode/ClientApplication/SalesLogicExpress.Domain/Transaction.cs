﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public class Transaction : ModelBase
    {

        /// <summary>
        /// ActionObjects of transaction
        /// </summary>
        public int ID
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        /// <summary>
        /// Activities of the transaction
        /// </summary>
        public List<Activity> Activities
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public ActivityState State
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        /// <summary>
        /// true indicates serviced, false indicated void
        /// </summary>
        public int CustomerID
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        bool ValidateActivities()
        {
            // Check every activity for its state
            return false;
        }
        void SaveTransactionLog()
        {
            // Save Activities to remote db
            var activities = this.Activities;
        }
        List<TransactionActivity> SplitActivitiesByImportance()
        {
            var activities = this.Activities;
            return new List<TransactionActivity>();
        }
        void AddTransactionToQueue()
        {
        }
        // Note : 
        // TransactionActivity object will be added to inmemmory queue
        // Transaction along with activites in the transaction will be saved to the TransactionLog

    }
}
