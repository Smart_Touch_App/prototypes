﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class PickOrderItem : OrderItem
    {
        int _OrderQty = 0, _PickQtyInPrimaryUOM = 0, _LastManualPickReasonCode = 0, _OnHandQuantity = 0, _OrderQtyInPrimaryUOM = 0, _ExceptionQtyInPrimaryUOM = 0, _ExceptionQtyInOrderUOM = 0, _AvailableQuantity = 0;
        string _ExceptionReason;
        char _ItemStatus;


        public PickOrderItem()
        {

        }
        public PickOrderItem(OrderItem orderItem)
        {
            this.ItemNumber = orderItem.ItemNumber;
            this.ItemDescription = orderItem.ItemDescription;
            this.UM = orderItem.UM;
            this.UMPrice = orderItem.UMPrice;
            this.UnitPrice = orderItem.UnitPrice;
            this.SalesCat1 = orderItem.SalesCat1;
            this.SalesCat5 = orderItem.SalesCat5;
            this.ActualQtyOnHand = orderItem.ActualQtyOnHand;
            this.OrderQty = orderItem.OrderQty;
            this.PickedQuantityInPrimaryUOM = 0;
            this.ItemStatus = 'U';
            this.UMConversionFactor = orderItem.UMConversionFactor;
            this.OrderQtyInPrimaryUOM = Convert.ToInt32(orderItem.OrderQty * orderItem.UMConversionFactor);
            this.PrimaryUM = orderItem.PrimaryUM;
        }



        public int LastManualPickReasonCode
        {
            get
            {
                return _LastManualPickReasonCode;
            }
            set
            {
                _LastManualPickReasonCode = value;
            }
        }

        public int AvailableQuantity
        {
            get
            {
                return ActualQtyOnHand - PickedQuantityInPrimaryUOM;
            }
            set
            {
                _AvailableQuantity = value;
                OnPropertyChanged("AvailableQuantity");
            }
        }

        public int ExceptionQtyInOrderUOM
        {
            get
            {
                return Convert.ToInt32(_ExceptionQtyInPrimaryUOM / UMConversionFactor);
            }
            set
            {
                _ExceptionQtyInOrderUOM = value;
                OnPropertyChanged("ExceptionQtyInOrderUOM");
                OnPropertyChanged("ExceptionQtyInPrimaryUOM");

            }
        }

        public int ExceptionQtyInPrimaryUOM
        {
            get
            {
                return _ExceptionQtyInPrimaryUOM;
            }
            set
            {
                _ExceptionQtyInPrimaryUOM = value;
                OnPropertyChanged("ExceptionQtyInPrimaryUOM");
                OnPropertyChanged("ExceptionQtyInOrderUOM");

            }
        }

        public int OrderQtyInPrimaryUOM
        {
            get
            {
                return _OrderQtyInPrimaryUOM;
            }
            set
            {
                _OrderQtyInPrimaryUOM = value;
                OnPropertyChanged("OrderQtyInPrimaryUOM");
            }
        }
        public int PickedQuantityInPrimaryUOM
        {
            get
            {
                return _PickQtyInPrimaryUOM;
            }
            set
            {
                _PickQtyInPrimaryUOM = value;
                OnPropertyChanged("PickedQuantityInPrimaryUOM");
                OnPropertyChanged("ItemStatus");
                OnPropertyChanged("AvailableQuantity");

            }
        }


        public string ExceptionReason
        {
            get
            {
                return _ExceptionReason;
            }
            set
            {
                _ExceptionReason = value;
                OnPropertyChanged("ExceptionReason");
            }
        }
        //U = UnderPick,O=OverPick,D=Done
        public char ItemStatus
        {
            get
            {
                if (PickedQuantityInPrimaryUOM > OrderQtyInPrimaryUOM)
                {
                    return 'O';
                }
                else if (PickedQuantityInPrimaryUOM < OrderQtyInPrimaryUOM)
                {
                    return 'U';
                }
                else if (PickedQuantityInPrimaryUOM == OrderQtyInPrimaryUOM)
                {
                    return 'D';
                }
                return 'U';
            }
            set
            {
                OnPropertyChanged("ItemStatus");
            }
        }
        public override string ToString()
        {
            return this.ItemNumber.ToString().Trim();
        }
    }
}
