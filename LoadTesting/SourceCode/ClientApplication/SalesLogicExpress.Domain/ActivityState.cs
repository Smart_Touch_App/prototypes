﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public enum ActivityState
    {
        Started,
        Indeterminate,
        Discarded,
        Failed,
        Completed,
    }
}
