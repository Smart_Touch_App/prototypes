﻿using System.Collections.Generic;
namespace SalesLogicExpress.Domain
{
    public class Item : Helpers.ModelBase
    {
        #region Properties
        private int _ActualQtyOnHand;
        string _ItemNumber, _UM, _UMPrice, _ItemDescription, _SalesCat1, _SalesCat4, _SalesCat5, _StkType, _ShortDesc, _PrimaryUM;
        decimal _ExtendedPrice, _UnitPrice;
        private int _QtyOnHand;
        double _AverageStopQty = 0;
        List<string> _AppliedUMS;
        bool _IsTaxable;
        int _OrderQty;

        public int OrderQty
        {
            get
            {
                return _OrderQty;
            }
            set
            {
                _OrderQty = value;
                this.OnPropertyChanged("ExtendedPrice");
            }
        }

        public int ActualQtyOnHand
        {
            get
            {

                return _ActualQtyOnHand;

            }
            set
            {
                _ActualQtyOnHand = value;
                OnPropertyChanged("ActualQtyOnHand");  // Trigger the change event if the value is changed!

            }
        }

        public bool IsTaxable
        {
            get { return _IsTaxable; }
            set
            {
                _IsTaxable = value;
                OnPropertyChanged("IsTaxable");
            }

        }
        public List<string> AppliedUMS
        {
            get { return _AppliedUMS; }
            set { _AppliedUMS = value; }
        }

        public double AverageStopQty
        {
            get { return _AverageStopQty; }
            set
            {
                _AverageStopQty = value;
                OnPropertyChanged("AverageStopQty");
            }
        }

        public int QtyOnHand
        {
            get
            {
                return (this.ActualQtyOnHand - this.OrderQty) < 0 ? 0 : (this.ActualQtyOnHand - this.OrderQty);
            }
            set
            {
                _QtyOnHand = value;
                OnPropertyChanged("QtyOnHand");  // Trigger the change event if the value is changed!

            }
        }
        public string ItemNumber
        {
            get
            {
                return _ItemNumber;
            }
            set
            {
                _ItemNumber = value;
                OnPropertyChanged("ItemNumber");
            }
        }

        public string ItemDescription
        {
            get
            {
                return _ItemDescription;
            }
            set
            {
                _ItemDescription = value;
                OnPropertyChanged("ItemDescription");
            }
        }
        public string SalesCat1
        {
            get
            {
                return _SalesCat1;
            }
            set
            {
                _SalesCat1 = value;
                OnPropertyChanged("SalesCat1");
            }
        }
        public string SalesCat5
        {
            get
            {
                return _SalesCat5;
            }
            set
            {
                _SalesCat5 = value;
                OnPropertyChanged("SalesCat5");
            }
        }
        public string SalesCat4
        {
            get
            {
                return _SalesCat4;
            }
            set
            {
                _SalesCat4 = value;
                OnPropertyChanged("SalesCat5");
            }
        }
        public string StkType
        {
            get
            {
                return _StkType;
            }
            set
            {
                _StkType = value;
                OnPropertyChanged("StkType");
            }
        }
        public string UM
        {
            get
            {
                return _UM;
            }
            set
            {
                _UM = value;
                OnPropertyChanged("UM");
                this.OnPropertyChanged("ExtendedPrice");

            }
        }
        public string UMPrice
        {
            get
            {
                return _UMPrice;
            }
            set
            {
                _UMPrice = value;
                OnPropertyChanged("UMPrice");
            }
        }
        public decimal ExtendedPrice
        {
            get
            {
                return _ExtendedPrice;
            }
            set
            {
                _ExtendedPrice = value;
                OnPropertyChanged("ExtendedPrice");
            }
        }
        public decimal UnitPrice
        {
            get
            {
                return _UnitPrice;
            }
            set
            {
                _UnitPrice = value;
                OnPropertyChanged("UnitPrice");
                this.OnPropertyChanged("ExtendedPrice");

            }
        }
        public string ShortDesc
        {
            get
            {
                return _ShortDesc;
            }
            set
            {
                _ShortDesc = value;
                OnPropertyChanged("ShortDesc");
            }
        }

        public string PrimaryUM
        {
            get
            {
                return _PrimaryUM;
            }
            set
            {
                _PrimaryUM = value;
                OnPropertyChanged("PrimaryUM");
            }
        }
        #endregion

        public Item()
        {

        }
    }
}
