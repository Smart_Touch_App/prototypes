﻿namespace SalesLogicExpress.Domain
{
    public class Customer : Visitee
    {
        public Customer()
        {
        }
        public string VisteeType { get { return "CUSTOMER"; } }

        public string BillType
        {
            get;
            set;
        }
        public string Company
        {
            get;
            set;
        }
        public string CustomerNo
        {
            get;
            set;
        }
        public string CustomerOrdTemplate
        {
            get;
            set;
        }
        public string DeliveryCode
        {
            get;
            set;
        }

        public string Route
        {
            get;
            set;
        }
        public string RouteBranch
        {
            get;
            set;
        }


        public string Shop
        {
            get;
            set;
        }

        public string Type
        { get; set; }

        public string SaleStatus
        {
            get;
            set;
        }
        public string SaleStatusReason
        {
            get;
            set;
        }
        public bool Activity { get; set; }
        public bool IsCustomerOnAccount { get; set; }
        public int CompletedActivity { get; set; }
        public int PendingActivity { get; set; }


        bool _visible = true;
        public bool ItemVisible
        {
            get
            {
                return _visible;
            }
            set
            {
                _visible = value;
                OnPropertyChanged("ItemVisible");
            }
        }
        public override string ToString()
        {
            return CustomerNo + ":" + Name;
        }
    }
}