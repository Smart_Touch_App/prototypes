﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SalesLogicExpress.Domain
{
    public class CustomerContact : Helpers.ModelBase, IDataErrorInfo
    {
        public string CustomerID { get; set; }
        public string ContactID { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public List<Email> EmailList { get; set; }
        public Email DefaultEmail { get; set; }
        public Phone DefaultPhone { get; set; }
        public List<Phone> PhoneList { get; set; }
        public bool IsDefault { get; set; }
        public string ReferenceContact { get; set; }
        public bool IsActive { get; set; }
        bool _IsSelected = false;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        public override string ToString()
        {
            return string.Concat("ContactID:", ContactID, ", ContactName:", ContactName, ", ContactTitle:", ContactTitle, ", IsDefault:", IsDefault, " ");
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get { return null; }
        }
    }
    public class Email : Helpers.ModelBase
    {
        public int Index { get; set; }

        public string ContactID { get; set; }
        public string FormattedValue
        {
            get
            {
                return Value.Trim();
            }
        }
        public string Value { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public bool IsDefault { get; set; }
        bool _IsSelected;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        public override string ToString()
        {
            return string.Concat("Type:", Type, ", Value:", Value, ", IsDefault:", IsDefault, " ");
        }
    }
    public class Phone : Helpers.ModelBase
    {
        public int Index { get; set; }

        public string ContactID { get; set; }
        public string FormattedValue
        {
            get
            {
                return AreaCode.Trim() + "-" + Value.Trim();
            }
        }
        public string Value { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public string Extension { get; set; }
        public string AreaCode { get; set; }
        public bool IsDefault { get; set; }
        bool _IsSelected;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        public override string ToString()
        {
            return string.Concat("Type:", Type, ", Value:", Value, ", IsDefault:", IsDefault, ", AreaCode:", AreaCode, ", Extension:", Extension);
        }
    }
}
