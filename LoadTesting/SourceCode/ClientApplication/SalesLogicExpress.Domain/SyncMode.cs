﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public enum SyncMode
    {
        Delayed,
        Immediate,
    }
}
