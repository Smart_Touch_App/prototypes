﻿namespace SalesLogicExpress.Domain
{
    public class OrderItem : Item
    {
        private decimal _OldUnitPrice;
        private double _UMConversionFactor;
        int _ReasonCode;
        bool _InclOnDemand;
        public OrderItem()
        {
        }
        public OrderItem(Item item)
        {
            this.ItemNumber = item.ItemNumber;
            this.ItemDescription = item.ItemDescription;
            this.UM = item.UM;
            this.SalesCat1 = item.SalesCat1;
            this.SalesCat5 = item.SalesCat5;
            this.UMPrice = item.UMPrice;
            this.UnitPrice = item.UnitPrice;
            this.StkType = item.StkType;
            this.ShortDesc = item.ShortDesc;
            this.PrimaryUM = item.PrimaryUM;
            this.ExtendedPrice = item.ExtendedPrice;
        }
        public OrderItem(TemplateItem item)
        {
            this.ItemNumber = item.ItemNumber;
            this.ItemDescription = item.ItemDescription;
            this.UM = item.UM;
            this.UMPrice = item.UMPrice;
            this.UnitPrice = item.UnitPrice;
            this.ExtendedPrice = item.ExtendedPrice;
            this.SalesCat1 = item.SalesCat1;
            this.SalesCat5 = item.SalesCat5;
            this.OrderQty = item.OrderQty;
            this.ShortDesc = item.ShortDesc;
            this.PrimaryUM = item.PrimaryUM;
            this.ReasonCode = 1;
            this.AppliedUMS = item.AppliedUMS;
        }
        public OrderItem(string itemNumber, string itemDescription, int orderQuantity, decimal unitPrice, string umPrice, decimal extendedPrice, int reasonCode, int onHandQuantity, string um)
        {
            this.ItemNumber = itemNumber;
            this.ItemDescription = itemDescription;
            this.OrderQty = orderQuantity;
            this.UnitPrice = unitPrice;
            this.UMPrice = umPrice;
            this.ExtendedPrice = extendedPrice;
            this.ReasonCode = reasonCode;
            this.QtyOnHand = onHandQuantity;
            this.UM = um;
        }


        public bool InclOnDemand
        {
            get
            {
                return _InclOnDemand;
            }
            set
            {
                _InclOnDemand = value;
            }
        }

       

        public double UMConversionFactor
        {
            get
            {
                //As discussed user will have to enter order qty in multiples of Primary UOM ratio
                return _UMConversionFactor;//< 1 ? 1 : _UMConversionFactor;
            }
            set
            {
                _UMConversionFactor = value;
                OnPropertyChanged("UMConversionFactor");
            }
        }


        public int ReasonCode
        {
            get
            {
                return _ReasonCode;
            }
            set
            {
                _ReasonCode = value;
                this.OnPropertyChanged("ReasonCode");
            }
        }

        public string OrderDate
        {
            get;
            set;
        }



        public override string ToString()
        {
            return ItemNumber;
        }

    }
}
