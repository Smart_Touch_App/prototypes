﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public enum TransactionType
    {
        Order,
        Pick,
        Delivery,
        VoidOrder,
        Payment,
        Quote,
        PreOrderAvailable,
        PreOrderCreate,
        Samples,
    }

    public enum StopType
    {
        Planned,
        UnPlanned,
    }
}
