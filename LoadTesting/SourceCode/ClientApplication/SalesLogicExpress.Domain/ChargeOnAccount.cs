﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ChargeOnAccount : Helpers.ModelBase
    {
        decimal _CreditLimit;
        decimal _TotalBalanceAmount;
        string _RequestCode;
        string _ApprovalCode;
        int _ReasonCodeId;
        decimal _TemporaryCharge;
        Boolean _IsApprovalCodeNeeded;

        public Boolean IsApprovalCodeNeeded
        {
            get
            {
                return CreditLimit < TotalBalanceAmount ? true : false;
            }
            set
            {
                _IsApprovalCodeNeeded = value;
                OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }

        public decimal TotalBalanceAmount
        {
            get
            {
                return _TotalBalanceAmount;
            }
            set
            {
                _TotalBalanceAmount = value;
                OnPropertyChanged("TotalBalanceAmount");
                OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }
        public string RequestCode
        {
            get
            {
                return IsApprovalCodeNeeded ? _RequestCode : "";
            }
            set
            {
                _RequestCode = value;
                OnPropertyChanged("RequestCode");

            }
        }
        public string ApprovalCode
        {
            get
            {
                return _ApprovalCode;
            }
            set
            {
                _ApprovalCode = value;
                OnPropertyChanged("ApprovalCode");

            }
        }
        public int ReasonCodeId
        {
            get
            {
                return _ReasonCodeId;
            }
            set
            {
                _ReasonCodeId = value;
                OnPropertyChanged("ReasonCodeId");

            }
        }
        public decimal TemporaryCharge
        {
            get
            {
                return _TemporaryCharge;
            }
            set
            {
                _TemporaryCharge = value;
                OnPropertyChanged("TemporaryCharge");

            }
        }

        public decimal CreditLimit
        {
            get
            {
                return _CreditLimit;
            }
            set
            {
                _CreditLimit = value;
                OnPropertyChanged("CreditLimit");
                OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }
    }
}
