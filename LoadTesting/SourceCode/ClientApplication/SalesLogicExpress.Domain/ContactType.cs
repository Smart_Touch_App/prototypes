﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ContactType
    {
        public string TypeID { get; set; }
        public string Type { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
