﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public class Visitee : ModelBase
    {
        int _SequenceNo = 0;
        public int SequenceNo
        {
            get { return this._SequenceNo; }
            set
            {
                if (value != this._SequenceNo)
                {
                    this._SequenceNo = value;
                    this.OnPropertyChanged("SequenceNo");
                }
            }
        }
        public string Address
        {
            get;
            set;
        }
        public string City
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }
        public string State
        {
            get;
            set;
        }
        public string Zip
        {
            get;
            set;
        }

        public int TransactionCount
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }
    }
}
