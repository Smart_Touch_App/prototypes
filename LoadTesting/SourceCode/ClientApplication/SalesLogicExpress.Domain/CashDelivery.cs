﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class CashDelivery : Helpers.ModelBase
    {
        decimal _PreviousBalanceAmount;
        decimal _CurrentInvoiceAmount;
        decimal _TotalBalanceAmount;
        double _PaymentAmount;
        bool _PaymentMode;
        string _ChequeNo;
        DateTime _ChequeDate;
        Boolean _IsCashDeliveryAllowed;
        decimal _CreditLimit;
        Boolean _IsApprovalCodeNeeded;

        public decimal CreditLimit
        {
            get
            {
                return _CreditLimit;
            }
            set
            {
                _CreditLimit = value;
                OnPropertyChanged("CreditLimit");
                OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }
        public Boolean IsApprovalCodeNeeded
        {
            get
            {
                return CreditLimit < TotalBalanceAmount ? true : false;
            }
            set
            {
                _IsApprovalCodeNeeded = value;
                OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }
        public Boolean IsCashDeliveryAllowed
        {
            get
            {
                return TotalBalanceAmount.ToString("F2") == PaymentAmount.ToString("F2") ? true : false;
            }
            set
            {
                _IsCashDeliveryAllowed = value;
                OnPropertyChanged("IsCashDeliveryAllowed");
            }
        }

        public decimal PreviousBalanceAmount
        {
            get
            {
                return _PreviousBalanceAmount;
            }
            set
            {
                _PreviousBalanceAmount = value;
                OnPropertyChanged("PreviousBalanceAmount");

            }
        }
        public decimal CurrentInvoiceAmount
        {
            get
            {
                return _CurrentInvoiceAmount;
            }
            set
            {
                _CurrentInvoiceAmount = value;
                OnPropertyChanged("CurrentInvoiceAmount");

            }
        }
        public decimal TotalBalanceAmount
        {
            get
            {
                return _TotalBalanceAmount;
            }
            set
            {
                _TotalBalanceAmount = value;
                OnPropertyChanged("TotalBalanceAmount");
                OnPropertyChanged("IsCashDeliveryAllowed");

            }
        }
        public double PaymentAmount
        {
            get
            {
                return _PaymentAmount;
            }
            set
            {
                _PaymentAmount = value;
                OnPropertyChanged("PaymentAmount");
                OnPropertyChanged("IsCashDeliveryAllowed");
            }
        }
        //0=Cash,1=Cheque
        public bool PaymentMode
        {
            get
            {
                if (!_PaymentMode)
                {
                    ChequeNo = "";
                }
                return _PaymentMode;
            }
            set
            {
                _PaymentMode = value;
                OnPropertyChanged("PaymentMode");
                OnPropertyChanged("ChequeNo");

            }
        }
        public string ChequeNo
        {
            get
            {
                return _ChequeNo;
            }
            set
            {
                _ChequeNo = value;
                OnPropertyChanged("ChequeNo");

            }
        }
        public DateTime ChequeDate
        {
            get
            {
                return _ChequeDate;
            }
            set
            {
                _ChequeDate = value;
                OnPropertyChanged("ChequeDate");

            }
        }

    }
}
