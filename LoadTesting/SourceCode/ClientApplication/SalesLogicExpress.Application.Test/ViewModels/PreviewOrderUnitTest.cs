﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SalesLogicExpress.Application.Test.ViewModels.Test
{
    [TestClass]
    public class PreviewOrderUnitTest
    {
        #region Variables
        ObservableCollection<Domain.OrderItem> PreviewOrderItemsCollection = new ObservableCollection<Domain.OrderItem>();
        Application.ViewModels.PreviewOrder previewOrder;
        int dataCount = 10;
        public readonly Guid Token = Guid.NewGuid();
        #endregion

        #region CommonMethods
        #region TestPreAndPost
        [TestInitialize]
        public void TestInitialize()
        {
            ObservableCollection<Domain.OrderItem> orderItemsCollection = new ObservableCollection<Domain.OrderItem>();

            string[] salesCat1 = new string[2];

            salesCat1[0] = "COF";
            salesCat1[1] = "ALL";

            Random random = new Random();

            for (int i = 0; i < dataCount; i++)
            {
                orderItemsCollection.Add(new Domain.OrderItem { ItemDescription = "xyz", SalesCat1 = salesCat1[random.Next(2)], ItemNumber = i + 1000.ToString(), UM = "CS", UnitPrice = 12, OrderQty = 5 + i, ExtendedPrice = 15 });
            }
            previewOrder = new PreviewOrder(orderItemsCollection);

            PreviewOrderItemsCollection = previewOrder.PreviewItems;

            previewOrder.MessageToken = this.Token;

            Messenger.Default.Register<Helpers.NavigateToView>(this, Token, (action) => NavigateToViewExecute(action));

        }

        [TestCleanup]
        public void TestCleanup()
        {
            PreviewOrderItemsCollection.Clear();
        }
        #endregion

        public object NavigateToViewExecute(Helpers.NavigateToView action)
        {
            Assert.AreEqual(PreviewOrderItemsCollection.Count, (action.Payload as ObservableCollection<OrderItem>).Count);
            return null;
        }
        #endregion

        #region TestMethods
        [TestMethod()]
        public void PreviewOrderLoading()
        {
            Assert.AreEqual(dataCount, PreviewOrderItemsCollection.Count);
            Assert.AreEqual(PreviewOrderItemsCollection.Where(s => s.SalesCat1 == "COF").ToList().Sum(s => s.ExtendedPrice), decimal.Parse(previewOrder.TotalCoffeeValue.Replace(@"$", @"").ToString()));
            Assert.AreEqual(PreviewOrderItemsCollection.Where(s => s.SalesCat1 == "ALL").ToList().Sum(s => s.ExtendedPrice), decimal.Parse(previewOrder.TotalAlliedValue.Replace(@"$", @"").ToString()));
            Assert.AreEqual(PreviewOrderItemsCollection.Sum(s => s.ExtendedPrice), decimal.Parse(previewOrder.OrderTotalValue.Replace(@"$", @"").ToString()));
            Assert.AreEqual(PreviewOrderItemsCollection.Sum(s => s.ExtendedPrice) + decimal.Parse(previewOrder.EnergySurchargeValue.Replace(@"$", @"").ToString()) + decimal.Parse(previewOrder.SalesTaxValue.Replace(@"$", @"").ToString()), decimal.Parse(previewOrder.InvoiceTotalValue.Replace(@"$", @"").ToString()));
        }

        [TestMethod]
        public void AddEnergySurchargeWithReasonCode()
        {
            decimal PreviousEnergySurcharge = decimal.Parse(previewOrder.PreviewInputText.Replace(@"$", @""));

            decimal PreviousInvoiceTotal = decimal.Parse(previewOrder.InvoiceTotalValue.Replace(@"$", @""));

            previewOrder.PreviewInputText = "12";

            previewOrder.ComboBoxSelectedIndex = 1;
            previewOrder.UpdateAllTotals.Execute(null);

            Assert.AreNotEqual(PreviousEnergySurcharge, decimal.Parse(previewOrder.EnergySurchargeValue.Replace(@"$", @"")));
            Assert.AreNotEqual(PreviousInvoiceTotal, decimal.Parse(previewOrder.InvoiceTotalValue.Replace(@"$", @"")));


            Assert.AreEqual(decimal.Parse(previewOrder.OrderTotalValue.Replace(@"$", @"")) + decimal.Parse(previewOrder.EnergySurchargeValue.Replace(@"$", @"")) + decimal.Parse(previewOrder.SalesTaxValue.Replace(@"$", @"")), decimal.Parse(previewOrder.InvoiceTotalValue.Replace(@"$", @"")));

        }

        [TestMethod]
        public void AddEnergySurchargeWithOutReasonCode()
        {
            decimal PreviousEnergySurcharge = decimal.Parse(previewOrder.PreviewInputText.Replace(@"$", @""));

            decimal PreviousInvoiceTotal = decimal.Parse(previewOrder.InvoiceTotalValue.Replace(@"$", @""));

            previewOrder.PreviewInputText = "12";

            previewOrder.ComboBoxSelectedIndex = 0;
            previewOrder.UpdateAllTotals.Execute(null);

            Assert.AreEqual(PreviousEnergySurcharge, decimal.Parse(previewOrder.EnergySurchargeValue.Replace(@"$", @"")));
            Assert.AreEqual(PreviousInvoiceTotal, decimal.Parse(previewOrder.InvoiceTotalValue.Replace(@"$", @"")));

        }

        [TestMethod]
        public void PickOrderFromPreview()
        {
            previewOrder.PreviewInputText = "12";

            previewOrder.ComboBoxSelectedIndex = 1;
            previewOrder.UpdateAllTotals.Execute(null);

            previewOrder.PickOrder.Execute(null);

        }

        #endregion
    }
}
