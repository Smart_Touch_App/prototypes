﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;

namespace SalesLogicExpress.Application.Test.ViewModels
{
    [TestClass]
    public class OrderUnitTest
    {
   
        [TestMethod]
        public void TestCreateOrder()
        {

            int OrderID = 60863;
            int CustomerId = 1108911;



                SalesLogicExpress.Application.Managers.UserManager.UserId = 1; //Sarvesh
                SalesLogicExpress.Application.Managers.UserManager.UserRoute = "783";

                var OrderManager = new SalesLogicExpress.Application.Managers.OrderManager();
                ObservableCollection<OrderItem> OrderItems = new ObservableCollection<OrderItem>();

                

                var it1 = new Item()
                {
                    ItemNumber = "1",
                    UnitPrice = 6.15999984741211M,
                    ExtendedPrice = 6.15999984741211M,
                    OrderQty = 2,
                    CreatedDate = DateTime.Now.ToString(),
                    IsTaxable = false,
                    ActualQtyOnHand = 5,
                    AverageStopQty = 3,
                    QtyOnHand = 10,
                    SalesCat1 = ""
                };
                var item1 = new OrderItem(it1);
                OrderItems.Add(item1);

                // SalesLogicExpress.Application.Helpers.ResourceManager.ActiveDatabaseConnectionString = connectionString;

                OrderManager.SaveOrderHeader(OrderID, CustomerId);
                OrderManager.SaveOrder(OrderID, CustomerId, OrderItems);

                SalesLogicExpress.Application.Managers.TransactionManager man = new SalesLogicExpress.Application.Managers.TransactionManager();

                man.AddTransactionInQueueForSync(SalesLogicExpress.Application.Managers.Transaction.SaveOrder,
                                                 SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);


        }




        [TestMethod]
        public void TestGetOrderHistoryForCustomer()
        {
            var OrderManager = new SalesLogicExpress.Application.Managers.OrderManager();
            var OrderList = OrderManager.GetOrderHistoryForCustomer("1108911");

            int CountOfOrders = 0;
            if (OrderList != null && OrderList.Tables.Count > 0)
                CountOfOrders = OrderList.Tables[0].Rows.Count;
        }

  

    }
}
