﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
namespace SalesLogicExpress.Application.ViewModels.Test
{
    [TestClass()]
    public class OrderTemplateUnitTest
    {
        #region Variables
        ObservableCollection<Domain.TemplateItem> templateItemsCollection = new ObservableCollection<Domain.TemplateItem>();
        Application.ViewModels.OrderTemplate orderTemplate;
        int dataCount = 10;
        public readonly Guid Token = Guid.NewGuid();
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        #endregion

        #region CommonMethods
        #region TestPreAndPost
        [TestInitialize]
        public void InitializeData()
        {
            string customerID = "1165497";
            string routeID = "783";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(new Customer());
            Customer customer = (Customer)SalesLogicExpress.Application.ViewModels.CommonNavInfo.GetSelectedCustomer();
            orderTemplate = new OrderTemplate(customer);
            templateItemsCollection = orderTemplate.TemplateItems;
            orderTemplate.MessageToken = this.Token;
            Messenger.Default.Register<Helpers.ConfirmWindow>(this, Token, (action) => ConfirmWindowExecute(action));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            templateItemsCollection.Clear();
        }
        #endregion

        public object ConfirmWindowExecute(Helpers.ConfirmWindow action)
        {
            action.Confirmed = true;
            return null;
        }
        #endregion

        #region TestMethods
        [TestMethod()]
        public void OrderTemplateLoading()
        {

            //have some items
            Assert.AreNotEqual(0, templateItemsCollection.Count);
            //all items order qty = 0 
            Assert.IsFalse(templateItemsCollection.Where(s => s.OrderQty != 0).Any());
            //Customer info
            Assert.IsNotNull(orderTemplate.Customer);
        }

        [TestMethod]
        public async Task SearchItems()
        {

            string searchString = "792";

            //Search on type
            await orderTemplate.SearchItems(searchString, false);
            Assert.AreNotEqual(0, orderTemplate.SearchItemList.Count);
            Assert.IsTrue(orderTemplate.SearchItemList.All(s => s.ItemNumber.Contains(searchString) || s.ItemDescription.Contains(searchString)));

            //Search on button click
            await orderTemplate.SearchItems(searchString, true);
            Assert.AreNotEqual(0, orderTemplate.SearchItemList.Count);
            Assert.IsTrue(orderTemplate.SearchItemList.All(s => s.ItemNumber.Contains(searchString) || s.ItemDescription.Contains(searchString)));

        }

        [TestMethod]
        public void LoadTemplateWithUsualQuantity()
        {

            orderTemplate.LoadTemplateWithUsualQuantity.Execute(null);

            Assert.IsTrue(templateItemsCollection.All(s => s.OrderQty == s.UsualQty));
        }

        [TestMethod]
        public void LoadTemplateWithPreOrderQuantity()
        {

            orderTemplate.LoadTemplateWithPreOrderQuantity.Execute(null);

            Assert.IsTrue(templateItemsCollection.All(s => s.OrderQty == s.PreOrderQty));
        }

        [TestMethod]
        public void ResetBackTemplateQuantity()
        {

            orderTemplate.LoadTemplateWithPreOrderQuantity.Execute(null);

            Assert.IsTrue(templateItemsCollection.All(s => s.OrderQty == s.PreOrderQty));

            orderTemplate.ResetBackTemplateQuantity.Execute(null);

            Assert.IsTrue(templateItemsCollection.All(s => s.OrderQty == 0));

        }

        [TestMethod]
        public async Task AddItemToTemplate()
        {

            await orderTemplate.SearchItems("cof", false);

            int previousTemplateItemsCount = templateItemsCollection.Count;
            int previousSearchItemsCount = orderTemplate.SearchItemList.Count;

            //Search Items
            ObservableCollection<object> resultList = new ObservableCollection<object>();

            resultList.Add(orderTemplate.SearchItemList[0]);

            // Add Item
            orderTemplate.AddItemToTemplate.Execute(resultList);

            // Verify Template Item count
            Assert.AreEqual(previousTemplateItemsCount + 1, templateItemsCollection.Count);

            // Verify SearchList Count
            Assert.AreEqual(previousSearchItemsCount - 1, orderTemplate.SearchItemList.Count);
            Domain.Item AddedItem = resultList[0] as Domain.Item;

            // Verify Item number in Template items
            Assert.IsTrue(templateItemsCollection.Any(s => s.ItemNumber == AddedItem.ItemNumber));
        }

        [TestMethod]
        public void CreateOrder()
        {

            templateItemsCollection[0].OrderQty = 2;
            templateItemsCollection[1].OrderQty = 2;

            orderTemplate.CreateOrder.Execute(null);

            Assert.AreEqual(templateItemsCollection.Count(s => s.OrderQty == 2), 2);
        }

        [TestMethod]
        public void DeleteItemFromTemplate()
        {

            Domain.TemplateItem PreviousTemplateItem = templateItemsCollection[0];

            ObservableCollection<object> resultList = new ObservableCollection<object>();

            resultList.Add(templateItemsCollection[0]);

            orderTemplate.DeleteItemFromTemplate.Execute(resultList);

            // check for deleted item in the template itemcollection
            Assert.IsFalse(templateItemsCollection.Contains(PreviousTemplateItem));

        }

        #endregion

    }

}
