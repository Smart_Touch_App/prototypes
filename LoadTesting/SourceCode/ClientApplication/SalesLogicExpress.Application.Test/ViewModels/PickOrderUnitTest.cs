﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace SalesLogicExpress.Application.ViewModels.Test
{
    [TestClass()]
    public class PickOrderUnitTest
    {
        #region Variables
        ObservableCollection<Domain.PickOrderItem> PickorderItemsCollection = new ObservableCollection<Domain.PickOrderItem>();
        Application.ViewModels.PickOrder pickOrder;
        int dataCount = 10;
        #endregion

        #region CommonMethods
        
        #region TestPreAndPost
        [TestInitialize]
        public void InitializeData()
        {
            ObservableCollection<Domain.OrderItem> orderItemsCollection = new ObservableCollection<Domain.OrderItem>();

            for (int i = 0; i < dataCount; i++)
            {
                orderItemsCollection.Add(new Domain.OrderItem { ItemDescription = "xyz", ItemNumber = i + 1000.ToString(), UM = "CS", UnitPrice = 12, 
                                                                OrderQty = 5 + i, ExtendedPrice = 15 });
            }
            pickOrder = new PickOrder(orderItemsCollection);
            PickorderItemsCollection = pickOrder.PickOrderItems;
        }

        [TestCleanup]
        public void TestCleanup()
        {
            PickorderItemsCollection.Clear();
        }
        #endregion

        public void PickUnPickItem(bool IsPick, Domain.PickOrderItem item)
        {
            pickOrder.IsAddingToPalette = IsPick;
            pickOrder.ScanByDevice.Execute(item);
        }

        public void VariousDataCombinations(int minPickCount, int maxPickCount, int minUnPickCount, int maxUnPickCount)
        {
            // Pick Items
            for (int i = minPickCount; i < maxPickCount; i++)
            {
                Domain.PickOrderItem Pickitem = PickorderItemsCollection[new Random().Next(i, dataCount - 1)];
                PickUnPickItem(true, Pickitem);
            }

            // UnPick Items
            for (int i = minUnPickCount; i < maxUnPickCount; i++)
            {
                Domain.PickOrderItem UnPickitem = PickorderItemsCollection[new Random().Next(i, dataCount - 1)];
                PickUnPickItem(false, UnPickitem);
            }

        }

        #endregion




        #region TestMethods
        [TestMethod()]
        public void PickOderLoadingCount()
        {
            Assert.AreEqual(dataCount, PickorderItemsCollection.Count);
        }

        [TestMethod]
        public void PickItemByScannerOrManual()
        {
            Domain.PickOrderItem item = PickorderItemsCollection[new Random().Next(0, dataCount - 1)];
            int PreviousPickQty = item.PickedQuantityInPrimaryUOM = 0;
            PickUnPickItem(true, item);
            Assert.AreEqual(PreviousPickQty + 1, item.PickedQuantityInPrimaryUOM);
        }

        [TestMethod]
        public void UnPickItemByScannerOrManual()
        {

            Domain.PickOrderItem item = PickorderItemsCollection[new Random().Next(0, dataCount - 1)];
            int PreviousPickQty = item.PickedQuantityInPrimaryUOM = 1;
            PickUnPickItem(false, item);
            Assert.AreEqual(PreviousPickQty - 1, item.PickedQuantityInPrimaryUOM);
        }

        [TestMethod]
        public void VerifyPickOrderStatus()
        {

            // only pick
            VariousDataCombinations(0, 2, 0, 0);

            // only unpick
            VariousDataCombinations(0, 0, 2, 4);

            // pick < unpick
            VariousDataCombinations(0, 2, 2, 8);

            // pick > unpick
            VariousDataCombinations(4, 8, 2, 4);

            Assert.AreEqual(pickOrder.DoneItemsQtyCount - pickOrder.OverPickItemsQtyCount, pickOrder.TotalOrderQtyCount - pickOrder.UnderPickItemsQtyCount);
            Assert.AreEqual(pickOrder.TotalOrderQtyCount - pickOrder.DoneItemsQtyCount, pickOrder.UnderPickItemsQtyCount - pickOrder.OverPickItemsQtyCount);
            Assert.AreEqual(pickOrder.DoneItemsQtyCount + pickOrder.UnderPickItemsQtyCount, pickOrder.TotalOrderQtyCount + pickOrder.OverPickItemsQtyCount);

        }

        [TestMethod]
        public void CompletePickOrder()
        {

            foreach (Domain.PickOrderItem pickOrderItem in PickorderItemsCollection.ToList())
            {
                for (int i = 0; i < pickOrderItem.OrderQty; i++)
                {
                    PickUnPickItem(true, pickOrderItem);
                }
            }

            Assert.AreEqual(pickOrder.TotalOrderQtyCount, pickOrder.DoneItemsQtyCount);
            Assert.AreEqual(pickOrder.UnderPickItemsQtyCount, 0);
            Assert.AreEqual(pickOrder.OverPickItemsQtyCount, 0);

        }
        #endregion


    }
}
