﻿using iAnywhere.MobiLink.Client;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iAnywhere.MobiLink.Script;
using System.Configuration;
using log4net;
namespace SalesLogicExpress.Application.Managers
{
    public class SyncManager
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.Order");
        DbmlsyncClient _syncClient = null;
        DBSC_StartType dbStartType;
        UInt32 syncHandle;
        public List<UInt32> syncHandles = new List<UInt32>();
        public SyncUpdateType SyncState { get; set; }
        ~SyncManager()
        {
            ShutDownSyncClient();
        }
        public void ShutDownSyncClient()
        {
            if (SyncClient != null)
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:ShutDownSyncClient]");
                try
                {
                    SyncClient.ShutdownServer(DBSC_ShutdownType.DBSC_SHUTDOWN_ON_EMPTY_QUEUE);
                    SyncClient.WaitForServerShutdown(10000);
                    SyncClient.Disconnect();
                    SyncClient.Fini();
                    _syncClient = null;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][ShutDownSyncClient][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:ShutDownSyncClient]");

            }
        }

        public void RestartSyncClient()
        {
            if (SyncClient != null)
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:RestartSyncClient]");
                try
                {
                    SyncClient.ShutdownServer(DBSC_ShutdownType.DBSC_SHUTDOWN_ON_EMPTY_QUEUE);
                    SyncClient.WaitForServerShutdown(10000);
                    SyncClient.Disconnect();
                    SyncClient.Fini();

                    System.Threading.Thread.Sleep(1000);

                    _syncClient = DbmlsyncClient.InstantiateClient();
                    _syncClient.Init();

                    // Setting the "server path" is usually required on Windows
                    // Mobile/CE. In other environments the server path is usually
                    // not required unless you SA install is not in your path or
                    // you have multiple versions of the product installed
                    _syncClient.SetProperty("server path", "C:\\Program Files\\SQL Anywhere 16\\Bin64");
                    _syncClient.StartServer(3426, "-c SERVER=remote_eng;DBN=remote_db;UID=dba;PWD=sql", 60000, out dbStartType);
                    _syncClient.Connect(null, 3426, "dba", "sql");
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][RestartSyncClient][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:RestartSyncClient]");
            }
        }
        public DbmlsyncClient SyncClient
        {
            get
            {
                if (_syncClient == null)
                {
                    Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncClient]");
                    try
                    {
                        _syncClient = DbmlsyncClient.InstantiateClient();
                        _syncClient.Init();
                        // Setting the "server path" is usually required on Windows
                        // Mobile/CE. In other environments the server path is usually
                        // not required unless you SA install is not in your path or
                        // you have multiple versions of the product installed
                        _syncClient.SetProperty("server path", "C:\\Program Files\\SQL Anywhere 16\\Bin64");
                        _syncClient.StartServer(3426, "-c SERVER=remote_eng;DBN=remote_db;UID=dba;PWD=sql", 60000, out dbStartType);
                        _syncClient.Connect(null, 3426, "dba", "sql");
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SyncClient][ExceptionStackTrace = " + ex.StackTrace + "]");
                        throw;
                    }
                    Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SyncClient]");

                }
                return _syncClient;
            }
        }

        NetworkManager network = new NetworkManager();
        public SyncQueueManager queueManager;
        AppServiceManager serviceManager;
        public SyncManager(SyncQueueManager queue)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncManager-Parameterized-Constructor]");
            try
            {
                queueManager = queue;
                network.InternetStateChanged += network_InternetStateChanged;
                queueManager.QueueChanged += queueManager_QueueChanged;
                serviceManager = new AppServiceManager();
                serviceManager.InitiateQueryForDataFromServer();
                serviceManager.ServiceStateChanged += serviceManager_ServiceStateChanged;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SyncManager-Parameterized-Constructor][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SyncManager-Parameterized-Constructor]");
        }

        void serviceManager_ServiceStateChanged(object sender, ServiceStateChangeEventArgs e)
        {
            if (e.DataAvailable)
            {
                // Process payload
            }
        }
        public SyncManager()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncManager-Constructor]");
            try
            {
                serviceManager = new AppServiceManager();
                serviceManager.InitiateQueryForDataFromServer();
                serviceManager.ServiceStateChanged += serviceManager_ServiceStateChanged;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SyncManager-Constructor][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SyncManager-Constructor]");
        }

        public void InitializePendingQueue(SyncQueueManager queue)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:InitializePendingQueue]");
            try
            {
                queueManager = queue;
                network.InternetStateChanged += network_InternetStateChanged;
                queueManager.QueueChanged += queueManager_QueueChanged;
                queueManager.InitializeQueue();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][InitializePendingQueue][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:InitializePendingQueue]");
        }
        void queueManager_QueueChanged(object sender, SyncQueueChangedEventArgs e)
        {
            if (e.State == QueueChangeType.Added && network.IsInterNetConnected())
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:queueManager_QueueChanged]");
                System.Diagnostics.Debug.WriteLine("Internet available and queue changed");
                System.Diagnostics.Debug.WriteLine("Process Queue");
                if ((queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueImmediate) || queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueEverything)))
                {
                    System.Diagnostics.Debug.WriteLine("Internet available and queue pending");
                    System.Diagnostics.Debug.WriteLine("Process Queue");
                    if (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueImmediate))
                    {
                        Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncQueueImmediate]");
                        System.Diagnostics.Debug.WriteLine("Process SyncQueueImmediate Queue");
                        ProcessQueue(queueManager.SyncQueueImmediate);
                        Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SyncQueueImmediate]");
                    }
                    if (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueEverything))
                    {
                        Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncQueueEverything]");
                        System.Diagnostics.Debug.WriteLine("Process SyncQueueEverything Queue");
                        ProcessQueue(queueManager.SyncQueueEverything);
                        Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SyncQueueEverything]");
                    }
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:queueManager_QueueChanged]");
            }
        }
        // Check if internet connection is available
        void network_InternetStateChanged(object sender, InternetStateChangedEventArgs e)
        {
            // If internet connection is available, process the queue
            if (e.State == InternetState.Connected && (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueImmediate) || queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueEverything)))
            {
                System.Diagnostics.Debug.WriteLine("Internet available and queue pending");
                System.Diagnostics.Debug.WriteLine("Process Queue");
                if (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueImmediate))
                {
                    ProcessQueue(queueManager.SyncQueueImmediate);
                }
                if (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueEverything))
                {
                    ProcessQueue(queueManager.SyncQueueEverything);
                }
            }
            // If internet connection is not available, persist the Sync Queue in the database
            if (e.State == InternetState.Disconnected)
            {
                System.Diagnostics.Debug.WriteLine("Internet not available return..");
            }
        }
        async void ProcessQueue(Queue<string> queue)
        {
            await Task.Run(() =>
            {
                if (!ResourceManager.NetworkInfo.IsInterNetConnected() || !serviceManager.IsServerReachable())
                {
                    return;
                }
                Queue<string> processQueue = queue;
                String transaction = null;
                System.Diagnostics.Debug.WriteLine("Trying to Process SyncQueueEverything Queue");
                //check if any transactions are yet to get synced
                while (queueManager.IsTransactionInSyncQueue(processQueue))
                {
                    //Read Queue
                    transaction = queueManager.GetTransactionFromQueue(processQueue);
                    System.Diagnostics.Debug.WriteLine(string.Format("Processing SyncQueueEverything {0} from Queue", transaction));
                    if (null != transaction)
                    {
                        // Synchronize the transaction viz a publication/profile
                        if (SynchronizeDatabase(transaction))
                        {
                            System.Diagnostics.Debug.WriteLine(string.Format("SyncQueueEverything synched {0} from Queue", transaction));
                            //Remove the transactions from DB Queue
                            int response = queueManager.RemoveTransactionFromQueue(transaction, processQueue);
                            System.Diagnostics.Debug.WriteLine(string.Format("RemoveTransactionFromQueue {0} ", response));
                        }
                    }
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.LastSynched = DateTime.Now.ToString("M'/'dd'/'yyyy' 'hh:mm");
                }
                System.Diagnostics.Debug.WriteLine("Process SyncQueueEverything Queue Complete");
            });

        }
        private bool SynchronizeDatabase(String publication)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SynchronizeDatabase][publication="+publication+"]");
            DBSC_Event dbEvent;
            bool isSyncSuccessful = true;
            //// Setting the "server path" is usually required on Windows
            //// Mobile/CE. In other environments the server path is usually
            //// not required unless you SA install is not in your path or
            //// you have multiple versions of the product installed
            syncHandle = SyncClient.Sync(publication, "");
            syncHandles.Add(syncHandle);
            try
            {
                while (SyncClient.GetEvent(out dbEvent, DbmlsyncClient.DBSC_INFINITY)
                      == DBSC_GetEventRet.DBSC_GETEVENT_OK)
                {
                    if (dbEvent.hdl == syncHandle)
                    {
                        Console.WriteLine("Event Type : {0}", dbEvent.type);
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                            ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.SyncFailed);
                            isSyncSuccessful = false;
                            break;
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_CANCEL)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                            isSyncSuccessful = false;
                            break;
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG)
                        {
                            if (dbEvent.str1.Contains("Network Error"))
                            {
                                ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.SyncFailed);
                                break;
                            }
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_FIRST_INTERNAL)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_INFO_MSG)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_UPLOAD_COMMITTED)
                        {
                            NotifyUpdate(SyncUpdateType.UploadComplete);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_SYNC_DONE)
                        {
                            NotifyUpdate(SyncUpdateType.SyncComplete);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_DOWNLOAD_COMMITTED)
                        {
                            NotifyUpdate(SyncUpdateType.DownloadComplete);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSyncSuccessful = false;
                ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.SyncFailed);
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SynchronizeDatabase][publication=" + publication + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            NotifyUpdate(SyncUpdateType.Idle);
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SynchronizeDatabase][publication=" + publication + "]");
            return isSyncSuccessful;
        }
        public event EventHandler<SyncUpdatedEventArgs> SyncProgressChanged;
        public void NotifyUpdate(SyncUpdateType update)
        {
            SyncUpdatedEventArgs args = new SyncUpdatedEventArgs();
            args.State = update;
            args.StateChangedTime = DateTime.Now;
            OnSyncProgress(args);
        }
        protected virtual void OnSyncProgress(SyncUpdatedEventArgs e)
        {
            EventHandler<SyncUpdatedEventArgs> handler = SyncProgressChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public async void CancelSync()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:CancelSync]");
            await Task.Run(() =>
            {
                try
                {
                    foreach (UInt32 handle in syncHandles)
                        SyncClient.CancelSync(handle);
                    syncHandles.Clear();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][CancelSync][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:CancelSync]");
        }
    }

    public class SyncUpdatedEventArgs : EventArgs
    {
        public SyncUpdateType State { get; set; }
        public DateTime StateChangedTime { get; set; }
    }
    public enum SyncUpdateType
    {
        Idle,
        UploadComplete,
        DownloadComplete,
        SyncComplete,
        SyncFailed,
        ServerNotReachable
    }
}