﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class PickManager
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.PickManager");

        public PickManager()
        {

        }

        public int UpdateManualPickReasonAndCountForPick(PickOrderItem pickItem)
        {
            return DbEngine.ExecuteNonQuery("update BUSDTA.PickOrder set reason_code_id=" + pickItem.LastManualPickReasonCode + "," +
            " ManuallyPickCount= ( SELECT max(isnull(ManuallyPickCount,0))+1  from BUSDTA.PickOrder where Item_Number='" + pickItem.ItemNumber + "' and Order_Id=" + Order.OrderId + ")" +
            " where Item_Number='" + pickItem.ItemNumber + "' and Order_Id=" + Order.OrderId);
        }

        public int UpdateManualPickReasonAndCountForException(PickOrderItem exceptionItem)
        {
            return DbEngine.ExecuteNonQuery("update BUSDTA.PickOrder_Exception set ManualPickReasonCode=" + exceptionItem.LastManualPickReasonCode + "," +
            " ManuallyPickCount= ( SELECT max(isnull(ManuallyPickCount,0))+1  from  BUSDTA.PickOrder_Exception where Item_Number='" + exceptionItem.ItemNumber + "' and Order_Id=" + Order.OrderId + ")" +
            " where Item_Number='" + exceptionItem.ItemNumber + "' and Order_Id=" + Order.OrderId);
        }


        public ObservableCollection<ReasonCode> GetReasonListForManualPick()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();

            try
            {

                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Manual Pick'").Tables[0];

                ReasonCode ReasonCode = new ReasonCode();


                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["Code"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetReasonListForManualPick  Message: " + e.StackTrace);
                throw;
            }

            return reasonCodeList;

        }

        public bool IsPickListCreated(string OrderId)
        {
            try
            {
                int countPickItems = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.PickOrder where order_id = '" + OrderId + "'"));
                if (countPickItems == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, IsPickListCreated(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public string GetPickListStatus(string OrderId)
        {
            try
            {
                string IsOnHold = DbEngine.ExecuteScalar("select distinct IsOnHold from busdta.PickOrder where order_id = '" + OrderId + "'");

                if (IsOnHold == "0" || IsOnHold == "")
                {
                    return "NotHold";

                }
                else
                {
                    return "Hold";
                }
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetPickListStatus(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int DeletePickList(string OrderId)
        {
            try
            {
                return DbEngine.ExecuteNonQuery("delete from busdta.PickOrder where order_id = '" + OrderId + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, DeletePickList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }
        public int DeletePickedItem(string OrderId, string ItemNumber)
        {
            try
            {
                return DbEngine.ExecuteNonQuery("delete from busdta.PickOrder where order_id = '" + OrderId + "' and item_number='" + ItemNumber + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, DeletePickedItem(" + OrderId + ", " + ItemNumber + ")  Message: " + e.StackTrace);
                throw;
            }
        }
        public int UpdatePickedItem(string OrderId, object Pickeditem)
        {
            try
            {
                PickOrderItem item = Pickeditem as PickOrderItem;

                return DbEngine.ExecuteNonQuery("Update busdta.PickOrder set  Order_Qty=" + item.OrderQty + " , Order_UOM='" + item.UM + "',order_qty_primary_UOM=" + item.OrderQtyInPrimaryUOM + " where order_id = '" + OrderId + "' and item_number='" + item.ItemNumber + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, UpdatePickedItem(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }
        public int InsertPickList(string OrderId, ObservableCollection<PickOrderItem> pickedItems)
        {
            try
            {
                foreach (PickOrderItem item in pickedItems)
                {
                    InsertPickItem(OrderId, item);
                }

                return 1;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, InsertPickList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int InsertPickItem(string OrderId, PickOrderItem item)
        {
            try
            {
                return DbEngine.ExecuteNonQuery(
                        @"insert into  BUSDTA.PickOrder 
                    (Order_ID,Item_Number,Order_Qty,Order_UOM,Picked_Qty_Primary_UOM,Primary_UOM,Order_Qty_Primary_UOM) 
                    values (" + OrderId + ",'" + item.ItemNumber + "'," + item.OrderQty + ",'" + item.UM + "','" + item.PickedQuantityInPrimaryUOM + "','" + item.PrimaryUM + "','" + item.OrderQtyInPrimaryUOM + "')"
                        );
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, InsertPickItem(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public ObservableCollection<PickOrderItem> GetPickItemsFromDB(string OrderId)
        {
            try
            {
                ObservableCollection<PickOrderItem> pickItems = new ObservableCollection<PickOrderItem>();

                DataTable dt = DbEngine.ExecuteDataSet(@"select po.Item_Number,IMDSC1 as ItemDescription ,po.Order_Qty,po.Order_UOM,Picked_Qty_Primary_UOM,Primary_UOM,Order_Qty_Primary_UOM,IMITM as ShortDesc,od.Unit_Price,od.Extn_Price,od.IsTaxable
 from BUSDTA.PickOrder po join busdta.F4101
            on po.Item_Number = imlitm join busdta.Order_Header oh on po.Order_ID = oh.Order_ID join busdta.Order_Detail od on oh.Order_ID=od.Order_ID and od.Item_Number=po.Item_Number where po.order_id = '" + OrderId + "'").Tables[0];

                Random RandomInt = new Random();

                PickOrderItem pickItem = new PickOrderItem();

                foreach (DataRow dr in dt.Rows)
                {
                    pickItem = new PickOrderItem();
                    pickItem.ItemNumber = dr["Item_Number"].ToString();
                    pickItem.ItemDescription = dr["ItemDescription"].ToString();
                    pickItem.OrderQty = Convert.ToInt32(dr["Order_Qty"].ToString());
                    pickItem.UM = dr["Order_UOM"].ToString();
                    pickItem.PickedQuantityInPrimaryUOM = Convert.ToInt32(dr["Picked_Qty_Primary_UOM"].ToString());
                    pickItem.PrimaryUM = dr["Primary_UOM"].ToString();
                    pickItem.OrderQtyInPrimaryUOM = Convert.ToInt32(dr["Order_Qty_Primary_UOM"].ToString());
                    //To be removed coz of temparary dynamic value it should come from some other source
                    pickItem.AvailableQuantity = RandomInt.Next(pickItem.OrderQtyInPrimaryUOM, pickItem.OrderQtyInPrimaryUOM + 100);
                    pickItem.ActualQtyOnHand = RandomInt.Next(pickItem.AvailableQuantity, pickItem.AvailableQuantity + 100);
                    pickItem.ShortDesc = dr["ShortDesc"].ToString();
                    pickItem.UMConversionFactor = new Managers.PricingManager().jdeUOMConversion(pickItem.UM, pickItem.PrimaryUM, Convert.ToInt32(pickItem.ShortDesc));
                    pickItem.UnitPrice = Convert.ToDecimal(dr["Unit_Price"].ToString());
                    pickItem.ExtendedPrice = Convert.ToDecimal(dr["Extn_Price"].ToString());

                    if (dr["IsTaxable"].ToString() != "")
                    {
                        pickItem.IsTaxable = Convert.ToBoolean(dr["IsTaxable"].ToString());

                    }

                    pickItems.Add(pickItem);
                }
                return pickItems;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetPickItemsFromDB(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int UpdatePickQty(string OrderId, string ItemNumber, string Picked_Qty_Primary_UOM)
        {
            try
            {
                return DbEngine.ExecuteNonQuery(" update busdta.PickOrder set Picked_Qty_Primary_UOM ='" + Picked_Qty_Primary_UOM + "' where order_id='" + OrderId + "' and Item_number = '" + ItemNumber + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, UpdatePickQty(" + OrderId + "," + Picked_Qty_Primary_UOM + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int HoldPickList(string OrderId)
        {
            try
            {
                int result = DbEngine.ExecuteNonQuery(" update busdta.PickOrder set IsOnHold ='1' where order_id='" + OrderId + "'");
                return result;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, HoldPickList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int SaveException(string OrderId, PickOrderItem Item)
        {
            try
            {
                bool IsExceptionExists = DbEngine.ExecuteScalar(" select count(*) from busdta.PickOrder_Exception pe where pe.Order_ID='" + OrderId + "' and pe.Item_Number='" + Item.ItemNumber + "'") == "0" ? false : true;

                int result;

                if (IsExceptionExists)
                {
                    //Update
                    result = DbEngine.ExecuteNonQuery(" update busdta.PickOrder_Exception pe  set pe.exception_qty=" + Item.ExceptionQtyInOrderUOM + ",pe.UOM='" + Item.UM + "',pe.exception_reason='" + Item.ExceptionReason + "' where pe.Order_ID='" + OrderId + "' and pe.Item_Number='" + Item.ItemNumber + "'");
                }
                else
                {
                    //Insert
                    result = DbEngine.ExecuteNonQuery(" insert into busdta.PickOrder_Exception (order_id,Item_number,Exception_Qty,UOM,Exception_Reason) values('" + OrderId + "','" + Item.ItemNumber + "'," + Item.ExceptionQtyInOrderUOM + ",'" + Item.UM + "','" + Item.ExceptionReason + "')");
                }
                return result;

            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, SaveException(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int DeleteException(string OrderId, PickOrderItem Item)
        {
            try
            {
                return DbEngine.ExecuteNonQuery(" delete from busdta.PickOrder_Exception pe  where pe.Order_ID='" + OrderId + "' and pe.Item_Number='" + Item.ItemNumber + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, DeleteException(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public ObservableCollection<PickOrderItem> GetExceptionList(string OrderId)
        {
            try
            {
                ObservableCollection<PickOrderItem> ExceptionItems = new ObservableCollection<PickOrderItem>();

                DataTable dt = DbEngine.ExecuteDataSet(@"select pe.Item_Number,IMDSC1 as ItemDescription ,isnull(Order_Qty,0) Order_Qty,pe.UOM Order_UOM,
isnull(Picked_Qty_Primary_UOM,0) Picked_Qty_Primary_UOM,Primary_UOM,isnull(Order_Qty_Primary_UOM,0) Order_Qty_Primary_UOM,IMITM as ShortDesc ,
isnull(pe.Exception_Qty,0) Exception_Qty,pe.Exception_Reason Exception_Reason
from busdta.PickOrder_Exception  pe join busdta.F4101
            on Item_Number = imlitm left join BUSDTA.PickOrder po on po.Order_ID = pe.Order_Id and po.Item_Number=pe.Item_Number 
 where pe.order_id = '" + OrderId + "'").Tables[0];


                PickOrderItem pickItem = new PickOrderItem();

                foreach (DataRow dr in dt.Rows)
                {
                    pickItem = new PickOrderItem();
                    pickItem.ItemNumber = dr["Item_Number"].ToString();
                    pickItem.ItemDescription = dr["ItemDescription"].ToString();
                    pickItem.OrderQty = Convert.ToInt32(dr["Order_Qty"].ToString());
                    pickItem.UM = dr["Order_UOM"].ToString();
                    pickItem.PickedQuantityInPrimaryUOM = Convert.ToInt32(dr["Picked_Qty_Primary_UOM"].ToString());
                    pickItem.PrimaryUM = dr["Primary_UOM"].ToString();
                    pickItem.OrderQtyInPrimaryUOM = Convert.ToInt32(dr["Order_Qty_Primary_UOM"].ToString());
                    pickItem.ShortDesc = dr["ShortDesc"].ToString();
                    pickItem.UMConversionFactor = new Managers.PricingManager().jdeUOMConversion(pickItem.UM, pickItem.PrimaryUM, Convert.ToInt32(pickItem.ShortDesc));
                    pickItem.ExceptionQtyInPrimaryUOM = Convert.ToInt32(Convert.ToInt32(dr["Exception_Qty"].ToString()) * pickItem.UMConversionFactor);
                    pickItem.ExceptionQtyInOrderUOM = Convert.ToInt32(dr["Exception_Qty"].ToString());
                    pickItem.ExceptionReason = dr["Exception_Reason"].ToString();
                    ExceptionItems.Add(pickItem);
                }
                return ExceptionItems;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetExceptionList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

    }
}
