﻿using System;
using System.Linq;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.Helpers;
namespace SalesLogicExpress.Application.Managers
{
    public class CustomerManager
    {
        public static Customer GetCustomerForRoute(string routeID, string customerID)
        {
            string customerByRouteQuery = "";
            customerByRouteQuery = customerByRouteQuery + "SELECT AIZON,AISTOP as DeliveryCode,a.aban8 AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , ";
            customerByRouteQuery = customerByRouteQuery + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
            customerByRouteQuery = customerByRouteQuery + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
            customerByRouteQuery = customerByRouteQuery + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip ";
            customerByRouteQuery = customerByRouteQuery + "FROM busdta.f0101 AS a ";
            customerByRouteQuery = customerByRouteQuery + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
            customerByRouteQuery = customerByRouteQuery + "JOIN busdta.f0101 AS b ON a.aban81 =b.aban8 ";
            customerByRouteQuery = customerByRouteQuery + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
            customerByRouteQuery = customerByRouteQuery + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
            customerByRouteQuery = customerByRouteQuery + "where AIZON>' ' and AISTOP>' ' and a.aban8='" + customerID + "'";

            DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerByRouteQuery);
            Customer customer = null;

            ObservableCollection<Customer> customersCollection = new ObservableCollection<Customer>();
            if (queryResult.HasData())
            {
                string[] deliveryCodes = new string[] { "11", "1A", "1E" };
                Random randomCode = new Random();
                customer = new Customer
                {
                    CustomerNo = queryResult.Tables[0].Rows[0]["CustomerNo"].ToString(),
                    Phone = queryResult.Tables[0].Rows[0]["Phone#"].ToString(),
                    Address = queryResult.Tables[0].Rows[0]["Address"].ToString(),
                    City = queryResult.Tables[0].Rows[0]["City"].ToString(),
                    Name = queryResult.Tables[0].Rows[0]["Name"].ToString(),
                    Zip = queryResult.Tables[0].Rows[0]["Zip"].ToString(),
                    State = queryResult.Tables[0].Rows[0]["State"].ToString(),
                    Shop = queryResult.Tables[0].Rows[0]["Bill_To_Name"].ToString(),
                    SequenceNo = 0,
                    DeliveryCode = queryResult.Tables[0].Rows[0]["DeliveryCode"].ToString(),
                    Route = queryResult.Tables[0].Rows[0]["Route"].ToString(),
                    RouteBranch = queryResult.Tables[0].Rows[0]["Route_Branch"].ToString(),
                    BillType = queryResult.Tables[0].Rows[0]["Billto_Type"].ToString(),
                };
            }
            return customer;
        }
        /// <summary>
        /// Get all customer which come under a route
        /// </summary>
        /// <param name="routeID">Route ID</param>
        /// <returns>Observable collection of customer class object</returns>
        public ObservableCollection<Customer> GetCustomersForRoute(string routeID)
        {
            string customerByRouteQuery = "";
            customerByRouteQuery = customerByRouteQuery + "SELECT AIZON,AISTOP as DeliveryCode,a.aban8 AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , ";
            customerByRouteQuery = customerByRouteQuery + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
            customerByRouteQuery = customerByRouteQuery + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
            customerByRouteQuery = customerByRouteQuery + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip,AITRAR as TransMode,AICO Company ";
            customerByRouteQuery = customerByRouteQuery + "FROM busdta.f0101 AS a ";
            customerByRouteQuery = customerByRouteQuery + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
            customerByRouteQuery = customerByRouteQuery + "JOIN busdta.f0101 AS b ON a.aban81 =b.aban8 ";
            customerByRouteQuery = customerByRouteQuery + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
            customerByRouteQuery = customerByRouteQuery + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
            customerByRouteQuery = customerByRouteQuery + "where AIZON>' ' and AISTOP>' ' ";

            DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerByRouteQuery);
            DataSet todaysOrders = Helpers.DbEngine.ExecuteDataSet("select * from busdta.Order_Header oh where datepart(month,oh.Created_On) = datepart(month,getdate()) and datepart(day,oh.Order_Date) = datepart(day,getdate())");


            ObservableCollection<Customer> customersCollection = new ObservableCollection<Customer>();
            if (queryResult.HasData())
            {
                string[] deliveryCodes = new string[] { "11", "1A", "1E" };
                Random randomCode = new Random();
                Customer customer;
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                    customer = new Customer
                    {
                        CustomerNo = customerDataRow["CustomerNo"].ToString(),
                        Phone = customerDataRow["Phone#"].ToString(),
                        Address = customerDataRow["Address"].ToString(),
                        City = customerDataRow["City"].ToString(),
                        Name = customerDataRow["Name"].ToString(),
                        Zip = customerDataRow["Zip"].ToString(),
                        State = customerDataRow["State"].ToString(),
                        Shop = customerDataRow["Bill_To_Name"].ToString(),
                        SequenceNo = index,
                        DeliveryCode = customerDataRow["DeliveryCode"].ToString(),
                        Route = customerDataRow["Route"].ToString(),
                        RouteBranch = customerDataRow["Route_Branch"].ToString(),
                        BillType = customerDataRow["Billto_Type"].ToString(),
                        IsCustomerOnAccount = !string.IsNullOrEmpty(customerDataRow["TransMode"].ToString()) && customerDataRow["TransMode"].ToString().Trim() == "CSH" ? false : true,
                        Company = customerDataRow["Company"].ToString()
                    };
                    if (todaysOrders.HasData())
                    {
                        todaysOrders.Tables[0].DefaultView.RowFilter = string.Format("Customer_Id='{0}'", customer.CustomerNo);
                        customer.Activity = todaysOrders.Tables[0].DefaultView.ToTable().Rows.Count > 0 ? true : false;

                        todaysOrders.Tables[0].DefaultView.RowFilter = string.Format("Customer_Id='{0}' and (Order_State ='DeliveryConfirmation' Or Order_Sub_State ='Void')", customer.CustomerNo);
                        customer.CompletedActivity = todaysOrders.Tables[0].DefaultView.ToTable().Rows.Count > 0 ? todaysOrders.Tables[0].DefaultView.ToTable().Rows.Count : 0;


                        todaysOrders.Tables[0].DefaultView.RowFilter = string.Format("Customer_Id='{0}' and Order_State <> 'DeliveryConfirmation' and Order_Sub_State <> 'Void'", customer.CustomerNo);
                        customer.PendingActivity = todaysOrders.Tables[0].DefaultView.ToTable().Rows.Count > 0 ? todaysOrders.Tables[0].DefaultView.ToTable().Rows.Count : 0;
                    }

                    customersCollection.Add(customer);
                }
                var orderedlist = customersCollection.OrderBy(x => x.CustomerNo);

                ObservableCollection<Customer> OrderList = new ObservableCollection<Customer>(orderedlist);

                for (var stopNo = 1; stopNo <= OrderList.Count; stopNo++)
                {
                    OrderList[stopNo - 1].SequenceNo = stopNo;
                }
                customersCollection = OrderList;
                customersCollection.ElementAt(0).SaleStatus = "SALE";
                customersCollection.ElementAt(0).SaleStatusReason = "SALE";
                customersCollection.ElementAt(1).SaleStatus = "NOSALE";
                customersCollection.ElementAt(1).SaleStatusReason = "NO SALE - CREDIT HOLD";
            }
            return customersCollection;
        }
        public ObservableCollection<CustomerContact> GetCustomerContact(string customerID)
        {
            string customerContactQuery = "select ABAN8 'Customer#','' as Title,'' as ContactName, ABALPH 'Customer Name',WPPHTP 'Phone Type' ";
            customerContactQuery = customerContactQuery + " ,WPPH1 'Phone#', EAEMAL 'Email' ";
            customerContactQuery = customerContactQuery + " from BUSDTA.F0101, BUSDTA.F0115, BUSDTA.F01151 ";
            customerContactQuery = customerContactQuery + " where WPAN8 = ABAN8 and WPAN8 = EAAN8 and WPAN8 in (select ABAN8 from  ";
            customerContactQuery = customerContactQuery + " BUSDTA.F0101 where ABAN8 like '%" + customerID + "%')";
            DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerContactQuery);

            ObservableCollection<CustomerContact> customerContactCollection = new ObservableCollection<CustomerContact>();
            if (queryResult.HasData())
            {
                Random rd = new Random();
                string[] contactNames = new string[] { "David Pinto", "Willam Johnsons", "Jacob Smith", "Logan White", "Jayden Lee" };
                string[] contactTitles = new string[] { "Manager", "Stores", "Front Office" };
                CustomerContact customerContact = null;
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                    //customerContact = new CustomerContact
                    //{
                    //    Name = contactNames[rd.Next(0, contactNames.Length - 1)],
                    //    Title = contactTitles[rd.Next(0, contactTitles.Length - 1)],
                    //    Phone = customerDataRow["Phone#"].ToString(),
                    //    PhoneType = customerDataRow["Phone Type"].ToString(),
                    //    Email = customerDataRow["Email"].ToString(),
                    //    //         EmailType = customerDataRow]["Email Type"].ToString()
                    //};
                    //customerDataRow["ContactName"] = customerContact.Name;
                    //customerDataRow["Title"] = customerContact.Title;
                    //customerContactCollection.Add(customerContact);
                }
            }
            return customerContactCollection;
        }

        public void AddNewContactDetails()
        {
            string AddContactQuery = "INSERT INTO BUSDTA.M0111(CDAN8,CDIDLN,CDRCK7,CDCNLN,CDPHTP,CDAR1,CDPH1,CDEXTN1,CDDFLTPH1,";
            AddContactQuery = AddContactQuery + "CDAR2,CDPH2,CDEXTN2,CDDFLTPH2,CDAR3,CDPH3,CDEXTN3,CDDFLTPH3,CDAR4,CDPH4,CDEXTN4,";
            AddContactQuery = AddContactQuery + "CDDFLTPH4,CDETP,CDEMAL1,CDDFLTEM1,CDEMAL2,CDDFLTEM2,CDEMAL3,CDDFLTEM3,CDFNAME,";
            AddContactQuery = AddContactQuery + "CDLNAME,CDTITL,CDID,CDACTV,CDDFLT,CDREFLN)";
            AddContactQuery = AddContactQuery + "values(1108911,0,2,0,NULL,,4057212466,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,";
            AddContactQuery = AddContactQuery + "NULL,NULL,NULL,NULL,NULL,NULL,,NULL,NULL,NULL,NULL,NULL,'Johnny','Dre',NULL,19,NULL,NULL,NULL)";
        }
    }
}
