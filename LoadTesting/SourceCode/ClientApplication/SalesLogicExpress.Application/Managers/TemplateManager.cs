﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.ViewModels;
using System.Globalization;

namespace SalesLogicExpress.Application.Managers
{
    public class TemplateManager
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.TemplateManager");
        /// <summary>
        /// Get all items in the template defined for the customer
        /// </summary>
        /// <param name="customerID">Invoice Number</param>
        /// <returns>Observable collection of TemplateItem class object</returns>
        public ObservableCollection<TemplateItem> GetTemplateItemsForCustomer(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:GetTemplateItemsForCustomer][customerID=" + customerID + "]");
            ObservableCollection<TemplateItem> result = new ObservableCollection<TemplateItem>();
            try
            {
                string Query = "select OTORTP as Template_Name , OTan8 as Invoice#, OTOSEQ as Sequence#, OTLITM as Item#, IMDSC1 as ItemDescription, IMITM as ShortDesc " +
                    ",0 as OrderQty, OTQTYU/10000 as UsualQty, 0 as PreOrderQty,OTUOM as UM,IMUOM4 as UM_Price,IMLNTY as StkType,IMSRP1  as SalesCat1, " +
                    "IMSRP5 as SalesCat5, Dateformat(BUSDTA.DateG2J(OTEFTJ),'mm/dd/yyyy') as EffectiveDate, Dateformat(BUSDTA.DateG2J(OTEXDJ),'mm/dd/yyyy') as ExpiredDate ,imuom1 as PrimaryUOM " +
                    "from busdta.F4015 ,busdta.F4101 where OTAn8  = " + customerID + " and OTORTP = 'S" + customerID + "' and ltrim(rtrim(otlitm)) = ltrim(rtrim(imlitm)) Order by OTOSEQ";

                DataSet templateItems = Helpers.DbEngine.ExecuteDataSet(Query);

                ItemManager itemManager = new ItemManager();

                Random qtyOnHand = new Random();

                if (templateItems.HasData())
                {
                    foreach (DataRow templateItem in templateItems.Tables[0].Rows)
                    {
                        TemplateItem order = new TemplateItem();
                        order.OrderQty = Convert.ToInt32(templateItem["OrderQty"].ToString());
                        //double UsualQty = (double)templateItem.ItemArray[7];
                        //if (UsualQty.ToString().Split('.').Length == 2)
                        //{
                        //    UsualQty = Convert.ToDouble(UsualQty.ToString().Split('.')[1].Substring(0, UsualQty.ToString().Split('.')[1].Length));
                        //}
                        //double PreOrderQty = (double)templateItem.ItemArray[8];
                        //if (PreOrderQty.ToString().Split('.').Length == 2)
                        //{
                        //    PreOrderQty = Convert.ToDouble(PreOrderQty.ToString().Split('.')[1].Substring(0, PreOrderQty.ToString().Split('.')[1].Length));
                        //}
                        order.UsualQty = Convert.ToInt32(templateItem["UsualQty"].ToString());
                        order.PreOrderQty = Convert.ToInt32(templateItem["PreOrderQty"].ToString());
                        order.UM = templateItem["UM"].ToString();
                        order.UMPrice = templateItem["UM_Price"].ToString();
                        order.SeqNo = Convert.ToInt32(templateItem["Sequence#"].ToString());
                        order.ItemNumber = templateItem["Item#"].ToString();
                        order.ItemDescription = templateItem["ItemDescription"].ToString();
                        order.EffectiveFrom = templateItem["EffectiveDate"].ToString();
                        order.EffectiveThru = templateItem["ExpiredDate"].ToString();
                        order.StkType = templateItem["StkType"].ToString();
                        order.SalesCat1 = templateItem["SalesCat1"].ToString();
                        order.SalesCat5 = templateItem["SalesCat5"].ToString();
                        order.ShortDesc = templateItem["ShortDesc"].ToString();
                        order.PrimaryUM = templateItem["PrimaryUOM"].ToString();
                        order.InclOnTmplt = true;
                        order.AppliedUMS = itemManager.GetAppliedUMs(order.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(order.ItemNumber) : new List<string>() { order.UM };
                        order.ActualQtyOnHand = qtyOnHand.Next(5, 50);

                        result.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][GetTemplateItemsForCustomer][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:GetTemplateItemsForCustomer][customerID=" + customerID + "]");
            return result;
        }

        ///<summary>
        ///Gets preorder datea collection 
        ///</summary>
        ///<param name="customerID">Customer Number</param>
        public ObservableCollection<TemplateItem> GetPreOrderItems(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:GetPreOrderItems][customerID=" + customerID + "]");

            ObservableCollection<TemplateItem> result = new ObservableCollection<TemplateItem>();
            try
            {
                //string QueryPreOrder = "select OPORTP as Template_Name , OPAN8 as Invoice#, OPOSEQ as Sequence#, OPLITM as Item# , IMDSC1 as ItemDescription, IMITM as ShortDesc," +
                //"OPQTYU/10000 as PreOrderQty,OTQTYU/10000 as UsualQty,OPUOM as UM,OPLNTY as StkType,Dateformat(BUSDTA.DateG2J(OTEFTJ),'mm/dd/yyyy') as " +
                //"EffectiveDate, Dateformat(BUSDTA.DateG2J(OTEXDJ),'mm/dd/yyyy') as ExpiredDate ,SRP1  as SalesCat1," +
                //"SRP5 as SalesCat5 from busdta.M4016 ,busdta.F4015, BUSDTA.f4101 where OPAN8=" + customerID + " and OPORTP='S" + customerID + "'" + " and ltrim(rtrim(oplitm)) = ltrim(rtrim(imlitm)) order by OPOSEQ";

                //String Query = QueryTemplate + "right outer join" + QueryPreOrder;

                string QueryPreOrder = " select OPORTP as Template_Name , OPAN8 as Invoice#, OPOSEQ as Sequence#, OPLITM as Item# , IMDSC1 as ItemDescription, IMITM as ShortDesc,OPQTYU/10000 as PreOrderQty,OPQTYU/10000" +
                                       " as UsualQty,OPUOM as UM,OPLNTY as StkType,Dateformat(BUSDTA.DateG2J(OTEFTJ),'mm/dd/yyyy') as EffectiveDate, Dateformat(BUSDTA.DateG2J(OTEXDJ),'mm/dd/yyyy') as ExpiredDate ,SRP1" +
                                       " as SalesCat1,SRP5 as SalesCat5 from BUSDTA.f4101 im left join busdta.M4016 po ON im.IMLITM = po.OPLITM left join" +
                                       " busdta.F4015 ot on po.OPAN8 = ot.otan8 and po.OPLITM= ot.otlitm where OPAN8=" + customerID + " and OPORTP='S" + customerID + "' and ltrim(rtrim(oplitm)) = ltrim(rtrim(imlitm)) " +
                                       " GROUP BY OPLITM, OPORTP, OPAN8, OPOSEQ, IMDSC1, IMITM, OPQTYU,OTQTYU,OPUOM,OPLNTY,OTEFTJ,OTEXDJ,SRP1,SRP5" +
                                       " order by OPOSEQ";

                DataSet templateItems = Helpers.DbEngine.ExecuteDataSet(QueryPreOrder);

                ItemManager itemManager = new ItemManager();

                Random qtyOnHand = new Random();

                if (templateItems.HasData())
                {
                    foreach (DataRow templateItem in templateItems.Tables[0].Rows)
                    {
                        TemplateItem order = new TemplateItem();
                        order.UsualQty = Convert.ToInt32(templateItem["PreOrderQty"]);
                        order.PreOrderQty = Convert.ToInt32(templateItem["PreOrderQty"]);
                        order.UM = templateItem["UM"].ToString();
                        order.SeqNo = Convert.ToInt32(templateItem["Sequence#"].ToString());
                        order.ItemNumber = templateItem["Item#"].ToString();
                        order.ItemDescription = templateItem["ItemDescription"].ToString().Trim();
                        order.ShortDesc = templateItem["ShortDesc"].ToString().Trim();
                        if (templateItem["EffectiveDate"].ToString() == null || templateItem["EffectiveDate"].ToString() == "")
                            order.EffectiveFrom = DateTime.Now.ToString("MM/dd/yyyy");
                        else
                            order.EffectiveFrom = templateItem["EffectiveDate"].ToString();
                        if (templateItem["ExpiredDate"].ToString() == null || templateItem["ExpiredDate"].ToString() == "")
                            order.EffectiveThru = DateTime.Now.ToString("MM/dd/yyyy");
                        else
                            order.EffectiveThru = templateItem["EffectiveDate"].ToString();
                        order.StkType = templateItem["StkType"].ToString();
                        order.SalesCat1 = templateItem["SalesCat1"].ToString();
                        order.SalesCat5 = templateItem["SalesCat5"].ToString();
                        order.InclOnTmplt = false;
                        order.AppliedUMS = itemManager.GetAppliedUMs(order.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(order.ItemNumber) : new List<string>() { order.UM };
                        order.ActualQtyOnHand = qtyOnHand.Next(5, 50);

                        result.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][GetTemplateItemsForCustomer][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        /// <summary>
        /// Applies Pricing algorithm to Items in templates based on OrderQuantity and Unit of measure
        /// </summary>
        /// <param name="templateItems">ObservableCollection of Template Items</param>
        /// <param name="routeBranch">Route branch</param>
        /// <param name="shipToCustomerID">Invoice No to whom the items are to be delivered</param>
        /// <returns>ObservableCollection of Order items</returns>
        public SalesLogicExpress.Application.ViewModels.TrulyObservableCollection<OrderItem> ApplyPricingToTemplates(ObservableCollection<TemplateItem> templateItems, string routeBranch, string shipToCustomerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "]");
            SalesLogicExpress.Application.ViewModels.TrulyObservableCollection<OrderItem> orderItemFromTemplate = new SalesLogicExpress.Application.ViewModels.TrulyObservableCollection<OrderItem>();
            try
            {
                decimal unitPrice = 0;
                Random randomProvider = new Random();
                PricingManager pricingManager = new PricingManager();
                Random qtyOnHand = new Random();
                foreach (TemplateItem item in templateItems)
                {
                    if (item.OrderQty <= 0)
                    {
                        continue;
                    }
                    unitPrice = 0;
                    OrderItem orderItem = new OrderItem(item);
                    unitPrice = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                    orderItem.UnitPrice = unitPrice == 0 ? unitPrice : unitPrice;
                    orderItem.ExtendedPrice = (orderItem.UM == orderItem.UMPrice) ? decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice : (decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice * decimal.Parse(pricingManager.jdeUOMConversion(orderItem.UM, orderItem.UMPrice, int.Parse(orderItem.ShortDesc)).ToString()));
                    orderItem.ShortDesc = item.ShortDesc;
                    orderItem.QtyOnHand = item.QtyOnHand;
                    orderItem.AverageStopQty = item.AverageStopQty;
                    orderItem.ActualQtyOnHand = item.ActualQtyOnHand;
                    orderItem.InclOnDemand = true;
                    orderItemFromTemplate.Add(orderItem);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "]");
            return orderItemFromTemplate;
        }
        /// <summary>
        /// Save Template modifications for the given customer
        /// </summary>
        /// <param name="templateItems">List of Template Items</param>
        /// <param name="customerNumber">Invoice Number</param>
        public void SaveTemplate(List<TemplateItem> templateItems, string customerNumber)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "]");
            try
            {
                string cmdDeleteTemplate = "Delete from busdta.F4015 where OTORTP = 'S" + customerNumber + "' and otan8 = " + customerNumber + "";
                DbEngine.ExecuteNonQuery(cmdDeleteTemplate);
                foreach (TemplateItem templateItem in templateItems)
                {
                    string cmdSaveTemplate = "insert into busdta.F4015 (OTORTP, OTAN8, OTOSEQ, OTITM, OTLITM, OTQTYU, OTUOM, OTLNTY, OTEFTJ, OTEXDJ) " +
                        "values " +
                        "('S" + customerNumber + "', " + customerNumber + ", " + templateItem.SeqNo + ", " + templateItem.ShortDesc + ", '" + templateItem.ItemNumber + "' " +
                        ", " + templateItem.UsualQty * 10000 + ", '" + templateItem.UM + "', 'S', 108319, 115365);";
                    DbEngine.ExecuteNonQuery(cmdSaveTemplate);
                }
                ResourceManager.Transaction.AddTransactionInQueueForSync(Transaction.SaveTemplate, SyncQueueManager.Priority.everything);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "]");
        }

        /// <summary>
        /// Save PreOrder for the given customer
        /// </summary>
        /// <param name="preOrderItems">List of Template Items</param>
        /// <param name="customerNumber">Invoice Number</param>
        public void SavePreOrder(ObservableCollection<TemplateItem> preOrderItems, string customerNumber,string StopDate)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "]");
            try 
            {
                //DateTime dtStop = Convert.ToDateTime(StopDate);
                //string stopdate = dtStop.ToString("yyyy-MM-dd");
                string cmdDeletePreOrder = "Delete from busdta.M4016 where OPORTP = 'S" + customerNumber + "' and OPAN8 = " + customerNumber + "";
                DbEngine.ExecuteNonQuery(cmdDeletePreOrder);
                
                foreach (TemplateItem templateItem in preOrderItems)
                {
                 
                    if (templateItem.PreOrderQty > 0)
                    {
                        string cmdSavePreOrder = "insert into busdta.M4016 (OPORTP, OPAN8, OPOSEQ, OPITM, OPLITM, OPQTYU, OPUOM, OPLNTY, SRP1,SRP5,CSCRBY,CSCRDT,CSUPBY,CSUPDT) " +
                            "values " +
                            "('S" + customerNumber + "', " + customerNumber + ", " + templateItem.SeqNo + ", " + templateItem.ShortDesc + ", '" + templateItem.ItemNumber + "' " +
                            ", " + templateItem.PreOrderQty * 10000 + ", '" + templateItem.UM + "', 'S', "+ "'" + templateItem.SalesCat1 + "'" +
                            "," + "'" + templateItem.SalesCat5 + "'" + "," + "'" + CommonNavInfo.UserName + "'" + "," +"'"+ DateTime.Now.ToString("yyyy-MM-dd")+"'" + "," + "'" + CommonNavInfo.UserName + "'" + "," + "'"+DateTime.Now.ToString("yyyy-MM-dd")+"'" + ");";
                        DbEngine.ExecuteNonQuery(cmdSavePreOrder);
                    }
                }
                // ResourceManager.Transaction.AddTransactionInQueueForSync(Transaction.SaveTemplate, SyncQueueManager.Priority.everything);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "]");

        }
    }
}






