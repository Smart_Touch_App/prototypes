﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class ContactManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ContactManager");

        public ObservableCollection<CustomerContact> GetCustomerContacts(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:GetCustomerContacts][customerID=" + customerID + "]");
            ObservableCollection<CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            try
            {
                //string query = "select ct.CTCD, as ContactType,isnull(ct1.CTCD,ct5.CTCD) as ContactType1, "+
                //    " isnull(ct2.CTCD,ct6.CTCD) as ContactType2,isnull(ct3.CTCD,'') as ContactType3,  isnull(ct.CTDSC1,ct4.CTDSC1) as ContactTypeDesc  " +
                //    " ,  isnull(ct1.CTDSC1,ct5.CTDSC1) as ContactTypeDesc1 ,  isnull(ct2.CTDSC1,ct6.CTDSC1) as ContactTypeDesc2 ,  isnull(ct3.CTDSC1,'') as ContactTypeDesc3,  c.* from busdta.M0111 c  ";
                //query = query + " left outer join busdta.M080111 ct on ct.CTID = c.CDPHTP1 ";
                //query = query + " left outer join busdta.M080111 ct1 on ct.CTID = c.CDPHTP2  ";
                //query = query + " left outer join busdta.M080111 ct2 on ct2.CTID = c.CDPHTP3  ";
                //query = query + " left outer join busdta.M080111 ct3 on ct3.CTID = c.CDPHTP4 ";
                //query = query + " left outer join busdta.M080111 ct4 on ct4.CTID = c.CDETP1 ";
                //query = query + " left outer join busdta.M080111 ct5 on ct5.CTID = c.CDETP2 ";
                //query = query + " left outer join busdta.M080111 ct6 on ct6.CTID = c.CDETP3 where c.CDAN8='{0}'";
                string query = "select tm.CTCD AS CD1,ISNULL(tm1.CTCD,tm.CTCD) AS CD2 ,ISNULL(tm2.CTCD,tm.CTCD) AS CD3, ISNULL(tm3.CTCD,tm.CTCD) AS CD4, " +
                                " tm.CTDSC1 AS CDSC1,ISNULL(tm1.CTDSC1,tm.CTDSC1) AS CDSC2 ,ISNULL(tm2.CTDSC1,tm.CTDSC1) AS CDSC3, ISNULL(tm3.CTDSC1,tm.CTDSC1) AS CDSC4, " +
                                " tm4.CTCD AS CD5, ISNULL(tm5.CTCD,tm4.CTCD) AS CD6, ISNULL(tm6.CTCD,tm4.CTCD) AS CD7, " +
                                " tm4.CTDSC1 AS CDSC5, ISNULL(tm5.CTDSC1,tm4.CTDSC1) AS CDSC6, ISNULL(tm6.CTDSC1,tm4.CTDSC1) AS CDSC7, " +
                                " ct.* from busdta.M0111  ct " +  
                                " left join busdta.M080111  tm on tm.CTID = ct.CDPHTP1 " +
                                " left join busdta.M080111  tm1 on tm1.CTID = ct.CDPHTP2 " +
                                " left join busdta.M080111  tm2 on tm2.CTID = ct.CDPHTP3 " +
                                " left join busdta.M080111  tm3 on tm3.CTID = ct.CDPHTP4 " +
                                " LEFT JOIN BUSDTA.M080111 tm4 on tm4.CTID = ct.CDETP1 " +
                                " LEFT JOIN BUSDTA.M080111 tm5 on tm5.CTID = ct.CDETP2 " +
                                " LEFT JOIN BUSDTA.M080111 tm6 on tm6.CTID = ct.CDETP3 where ct.CDAN8='{0}'";

                query = string.Format(query, customerID);
                DataSet result = DbEngine.ExecuteDataSet(query);
                CustomerContact contact;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contact = new CustomerContact();
                            contact.ContactName = row["CDGNNM"].ToString().Trim() + " " + row["CDSRNM"].ToString().Trim();
                            contact.ContactTitle = row["CDTITL"].ToString();
                            contact.ContactID = row["CDIDLN"].ToString();
                            contact.EmailList = new List<Email>();
                            if (!string.IsNullOrEmpty(row["CDEMAL1"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 1, ContactID = row["CDIDLN"].ToString(), Type = row["CD5"].ToString().Trim() + " - " + row["CDSC5"].ToString().Trim(), Value = row["CDEMAL1"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM1"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM1"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDEMAL2"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 2, ContactID = row["CDIDLN"].ToString(), Type = row["CD6"].ToString().Trim() + " - " + row["CDSC6"].ToString().Trim(), Value = row["CDEMAL2"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM2"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM2"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDEMAL3"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 3, ContactID = row["CDIDLN"].ToString(), Type = row["CD7"].ToString().Trim() + " - " + row["CDSC7"].ToString().Trim(), Value = row["CDEMAL3"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM3"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM3"].ToString()) : false });
                            }
                            //contact.EmailList.Add(new Email { Type = row["CDETP3"].ToString(), Value = row["CDEMAL4"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM4"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM4"].ToString()) : false });
                            if (contact.EmailList.Count > 0)
                            {
                                contact.DefaultEmail = contact.EmailList.FirstOrDefault(em => em.IsDefault == true);
                            }
                            contact.PhoneList = new List<Phone>();


                            if (!string.IsNullOrEmpty(row["CDPH1"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 1, ContactID = row["CDIDLN"].ToString(), Type = row["CD1"].ToString().Trim() + " - " + row["CDSC1"].ToString().Trim(), Value = row["CDPH1"].ToString().Trim(), Extension = row["CDEXTN1"].ToString().Trim(), AreaCode = row["CDAR1"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH1"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH1"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH2"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 2, ContactID = row["CDIDLN"].ToString(), Type = row["CD2"].ToString().Trim() + " - " + row["CDSC2"].ToString().Trim(), Value = row["CDPH2"].ToString().Trim(), Extension = row["CDEXTN2"].ToString().Trim(), AreaCode = row["CDAR2"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH2"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH2"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH3"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 3, ContactID = row["CDIDLN"].ToString(), Type = row["CD3"].ToString().Trim() + " - " + row["CDSC3"].ToString().Trim(), Value = row["CDPH3"].ToString().Trim(), Extension = row["CDEXTN3"].ToString().Trim(), AreaCode = row["CDAR3"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH3"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH3"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH4"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 4, ContactID = row["CDIDLN"].ToString(), Type = row["CD4"].ToString().Trim() + " - " + row["CDSC4"].ToString().Trim(), Value = row["CDPH4"].ToString().Trim(), Extension = row["CDEXTN4"].ToString().Trim(), AreaCode = row["CDAR4"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH4"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH4"].ToString()) : false });
                            }
                            if (contact.PhoneList.Count > 0)
                            {
                                contact.DefaultPhone = contact.PhoneList.FirstOrDefault(em => em.IsDefault == true);
                            }
                            contact.IsActive = !string.IsNullOrEmpty(row["CDACTV"].ToString()) ? Convert.ToBoolean(row["CDACTV"].ToString()) : false;
                            contact.IsDefault = !string.IsNullOrEmpty(row["CDDFLT"].ToString()) ? Convert.ToBoolean(row["CDDFLT"].ToString()) : false;
                            contactList.Add(contact);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetCustomerContacts][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetCustomerContacts][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:GetCustomerContacts][customerID=" + customerID + "]");
            return contactList;
        }
        public ObservableCollection<ContactType> GetContactTypes()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:GetContactTypes]");
            ObservableCollection<ContactType> contactTypeList = new ObservableCollection<ContactType>();
            try
            {
                string query = "select * from busdta.M080111";
                DataSet result = DbEngine.ExecuteDataSet(query);
                ContactType contactType;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contactType = new ContactType();
                            contactType.TypeID = row["CTID"].ToString().Trim();
                            contactType.Type = row["CTTYP"].ToString().Trim();
                            contactType.Code = row["CTCD"].ToString().Trim();
                            contactType.Description = row["CTDSC1"].ToString().Trim();
                            contactTypeList.Add(contactType);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactTypes][ExceptionStackTrace = " + ex.StackTrace + "]");
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactTypes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:GetContactTypes]");
            return contactTypeList;
        }
        public int AddCustomerContact(CustomerContact contact)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:AddCustomerContact][contact=" + contact + "]");
            ObservableCollection<CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            try
            {
                string queryUniqueID = "SELECT COALESCE((MAX(CDIDLN)+1),0) FROM BUSDTA.M0111 WHERE CDAN8 = '" + contact.CustomerID + "';";
                int uniqueID = Convert.ToInt32(DbEngine.ExecuteScalar(queryUniqueID));                

                string query = "INSERT INTO BUSDTA.M0111 (\"CDIDLN\",\"CDRCK7\",\"CDCNLN\",\"CDAN8\",\"CDAR1\",\"CDPH1\",\"CDEXTN1\",\"CDPHTP1\",\"CDDFLTPH1\",\"CDEMAL1\",\"CDETP1\",\"CDDFLTEM1\",\"CDGNNM\",\"CDTITL\",\"CDID\",\"CDACTV\",\"CDDFLT\")";
                query = query + " VALUES(";
                query = query + "'" + uniqueID + "',";
                query = query + "'" + "0" + "',";
                query = query + "'" + "0" + "',";
                query = query + "'" + contact.CustomerID + "',";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].AreaCode + "'" : "NULL") + ",";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].Value + "'" : "NULL") + ",";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].Extension + "'" : "NULL") + ",";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].Type + "'" : "NULL") + ",";
                query = query + "1,";
                query = query + (contact.EmailList.Count > 0 ? "'" + contact.EmailList[0].Value + "'" : "NULL") + ",";
                query = query + (contact.EmailList.Count > 0 ? "'" + contact.EmailList[0].Type + "'" : "NULL") + ",";
                query = query + "1,";
                query = query + "'" + contact.ContactName + "',";
                query = query + "'" + contact.ContactTitle + "',";
                query = query + '0' + ",";
                query = query + "1,";
                query = query + "1";
                query = query + ")";
                result = DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][AddCustomerContact][contact=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:AddCustomerContact][contact=" + contact + "]");
            return result;
        }
        public int DeleteCustomerContact(CustomerContact contact)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:DeleteCustomerContact][contactID=" + contact + "]");
            ObservableCollection<CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            try
            {
                string query = "DELETE FROM BUSDTA.M0111 WHERE CDIDLN ='";
                query = query + contact.ContactID + "' AND CDAN8='";
                query = query + contact.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][DeleteCustomerContact][contactID=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:DeleteCustomerContact][contactID=" + contact + "]");

            return result;
        }
        //update existing contact
        public int SaveEditedContact(CustomerContact customerContact)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveEditedContact][contact=" + customerContact + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();

            int result = -1;
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDGNNM = '" + customerContact.ContactName + "',";
                query = query + "CDTITL = '" + customerContact.ContactTitle + "',";
                query = query + "CDAR1 = " + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].AreaCode + "'" : "NULL") + ",";
                query = query + "CDPH1 = " + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].Value + "'" : "NULL") + ",";
                query = query + "CDEXTN1 =" + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].Extension + "'" : "NULL") + ",";
                //query = query + "CDPHTP1 = '" + PhoneTypeID + "',";
                query = query + "CDPHTP1 = " + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].TypeID + "'" : "NULL") + ",";
                query = query + "CDEMAL1 = " + (customerContact.EmailList.Count > 0 ? "'" + customerContact.EmailList[0].Value + "'" : "NULL") + ",";
                query = query + "CDETP1 = " + (customerContact.EmailList.Count > 0 ? "'" + customerContact.EmailList[0].TypeID + "'" : "NULL") ;
                query = query + " WHERE CDIDLN ='" + customerContact.ContactID + "' AND ";
                query = query + "CDAN8 ='" ;
                query = query + customerContact.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveEditedContact][contact=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveEditedContact][contact=" + contact + "]");
            return result;
        }
        //update exsting emailid of contact
        public int SaveEditedEmail(CustomerContact contactEmail, int index)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveEditedEmail][contact=" + contactEmail + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();
            int i = index;
            
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDEMAL" +i + "= ";
                query = query + (contactEmail.EmailList.Count > 0 ? "'" + contactEmail.EmailList[0].Value + "'" : "NULL" )+ ",";
                query = query + " CDETP" + i + "=" + "'" + contactEmail.EmailList[0].TypeID + "'";
                query = query + " WHERE CDIDLN ='" + contactEmail.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactEmail.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveEditedEmail][contact=" + contactEmail + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveEditedEmail][contact=" + contactEmail + "]");
            return result;
        }
        //update existing phone no. of contact
        public int SaveEditedPhone(CustomerContact contactPhone, int index)
        {
            int result = -1;

            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveEditedPhone][contact=" + contactPhone + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();

            int i = index;
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDAR" + i +" =";
                query = query + (contactPhone.PhoneList.Count > 0 ? "'" + contactPhone.PhoneList[0].AreaCode + "'" : "NULL") + ",";
                query = query + "CDPH" + i + "=" + (contactPhone.PhoneList.Count > 0 ? "'" + contactPhone.PhoneList[0].Value + "'" : "NULL");
                query = query + ", CDEXTN" + i + "=" + (contactPhone.PhoneList.Count > 0 ? "'" + contactPhone.PhoneList[0].Extension + "'" : "NULL");
                query = query + ", CDPHTP" + i + "=" + "'" + contactPhone.PhoneList[0].TypeID + "'";
                query = query + " WHERE CDIDLN ='" + contactPhone.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactPhone.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveEditedPhone][contact=" + contactPhone + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveEditedPhone][contact=" + contactPhone + "]");
            return result;
        }
        public int GetTotalPhoneNumber(string contactID, string customerID)
        {
            int result = -1;
            try 
            {
                string query = "SELECT ISNULL(COUNT(c.CDAR1),0) + isnull(COUNT(c.CDAR2),0) + ";
                query = query + " isnull(COUNT(c.CDAR3),0)+ ISNULL(COUNT(c.CDAR4),0) ";
                query = query + " FROM BUSDTA.M0111 c WHERE c.CDAN8 = '" + customerID + "' AND ";
                query = query + " c.CDIDLN = '" + contactID + "';";

                result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                result = result + 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetTotalPhoneNumber][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        // Adding new phone no. to contact
        public int SaveContactPhone(Phone contactPhone , string CustomerID)
        {
            string contactID = contactPhone.ContactID;

            int result = -1;
            int i = GetTotalPhoneNumber(contactID, CustomerID);
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:AddCustomerContact][contact=" + contactPhone + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();
            try
            {
              string query = " UPDATE BUSDTA.M0111 SET "+
                             " CDAR" + i + " =  '" + contactPhone.AreaCode + "', " +
                             " CDPH"+ i + " =  '" + contactPhone.Value + "', " +
                             " CDEXTN"+ i+ "= '" + contactPhone.Extension + "', " +
                             " CDPHTP"+i+" = '" + contactPhone.TypeID + "'" +
                             " WHERE CDIDLN ='" + contactPhone.ContactID + "' AND CDAN8 = '" + CustomerID + "' ";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveContactPhone][contact=" + contactPhone + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveContactPhone][contact=" + contactPhone + "]");
            return result;
        }    
        public int GetTotalEmailNumber(string contactID, string customerID)
        {
            int result = -1;
            try
            {
                string query = "SELECT ISNULL(COUNT(c.CDEMAL1),0) + isnull(COUNT(c.CDEMAL2),0) + ";
                query = query + " isnull(COUNT(c.CDEMAL3),0) ";
                query = query + " FROM BUSDTA.M0111 c WHERE c.CDAN8 = '" + customerID + "' AND";
                query = query + " c.CDIDLN = '" + contactID + "';";

                result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                result = result + 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetTotalPhoneNumber][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        //adding new emailid to contact
        public int SaveContactEmail(Email contactEmail, string CustomerID)
        {
            string contactID = contactEmail.ContactID;
            int result = -1;
            int i = GetTotalEmailNumber(contactID, CustomerID);
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveContactEmail][contact=" + contactEmail + "]");
           // ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDEMAL" + i + "= '" + contactEmail.Value + "', ";
                       query = query + "CDETP" + i + " ='"+ contactEmail.TypeID + "' ";
                       query = query + " WHERE CDIDLN ='" + contactEmail.ContactID + "' AND ";
                       query = query + " CDAN8 = '" + CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveContactEmail][contact=" + contactEmail + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveContactEmail][contact=" + contactEmail + "]");
            return result;
        }

        public CustomerContact GetContactNameAndTitle(string contactID, string customerID)
        {
            //CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            CustomerContact contact = new CustomerContact();
            try
            {
                string query = "SELECT TOP 1 CDGNNM, CDSRNM, CDTITL FROM BUSDTA.M0111 WHERE CDIDLN ='" + contactID + "' AND ";
                query = query + " CDAN8 ='";
                query = query + customerID + "';";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
               // CustomerContact contact;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contact = new CustomerContact();
                            contact.ContactName = row["CDGNNM"].ToString().Trim() + " " + row["CDSRNM"].ToString().Trim();
                            contact.ContactTitle = row["CDTITL"].ToString();
                            //contactList.Add(contact);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactNameAndTitle][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return contact;
        }

        public int DeleteContactEmail(CustomerContact contactEmail, int index)
        {
            int result = -1;
            int i = index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:DeleteContactEmail][contact=" + contactEmail + "]");
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDEMAL" + i + "= NULL, CDETP" + i + "= NULL";
                query = query + " WHERE CDIDLN ='" + contactEmail.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactEmail.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][DeleteContactEmail][contact=" + contactEmail + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:DeleteContactEmail][contact=" + contactEmail + "]");
            return result;
        }

        public int DeleteContactPhone(CustomerContact contactPhone, int index)
        {
            int result = -1;
            int i = index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:DeleteContactPhone][contact=" + contactPhone + "]");
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDAR" + i + "= NULL, CDPH" + i + "= NULL, CDEXTN" + i + "= NULL, CDPHTP" + i+ "= NULL";
                query = query + " WHERE CDIDLN ='" + contactPhone.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactPhone.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][DeleteContactPhone][contact=" + contactPhone + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:DeleteContactPhone][contact=" + contactPhone + "]");
            return result;
        }
    }
}
