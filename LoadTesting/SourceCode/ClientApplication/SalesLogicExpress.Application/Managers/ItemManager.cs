﻿using System;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using System.Data;
using System.Collections.Generic;
namespace SalesLogicExpress.Application.Managers
{

    public class ItemManager
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ItemManager");
        public ItemManager()
        {

        }

        /// <summary>
        /// Search for items
        /// </summary>
        /// <param name="searchTerm">term to search</param>
        /// <returns>ObservableCollection of Item class object</returns>
        /// <remarks>Search on items in made on itemNumber and item description</remarks>
        public ObservableCollection<Item> SearchItem(string searchTerm)
        {
            ObservableCollection<Item> itemCollection = new ObservableCollection<Item>();
            try
            {
                string[] supportedKeyword = new string[] { "IMLITM", "IMDSC1" };
                string[] searchTermkeywords = searchTerm.Trim().Replace("  ", " ").Split(' ');
                string conditionQuery = string.Empty, queryPlaceholder = string.Empty;
                foreach (string keyword in supportedKeyword)
                {
                    conditionQuery = conditionQuery + "(";
                    foreach (string term in searchTermkeywords)
                    {
                        conditionQuery += keyword + " like '%" + term + "%' ";
                        if (searchTermkeywords[searchTermkeywords.Length - 1] != term)
                        {
                            conditionQuery += " or ";
                        }
                    }
                    if (supportedKeyword[supportedKeyword.Length - 1] != keyword)
                    {
                        conditionQuery += " ) or ";
                    }
                    else
                    {
                        conditionQuery += " ) or ";
                    }
                }

                conditionQuery = conditionQuery + "(0=1) ";
                if (searchTermkeywords.Length == 1)
                {
                    queryPlaceholder = conditionQuery;
                    queryPlaceholder = queryPlaceholder.Replace(") and (", ") or (");
                    conditionQuery = "(" + conditionQuery + ") or (" + queryPlaceholder + ")";
                }
                string searchQuery = "select IMLITM as Item#, IMDSC1 as ItemDescription, IMLITM ,1 as OrderQty,1 as PreOrderQty, 1 as UsualQty, IMUOM4 as UM, IMUOM4 as UM_Price, " +
                    "IMLNTY as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5, dateformat(today(), 'mm/dd/yyyy') as EffectiveDate, dateformat(today(), 'mm/dd/yyyy') as ExpiredDate,  IMITM as ShortDesc,imuom1 as PrimaryUOM,IMSRP4 as SalesCat4 " +
                    "from busdta.F4101 left outer join busdta.F4102 on BUSDTA.F4101.IMLITM = busdta.f4102.IBLITM " +
                    "where IBLITM = IMLITM and " + conditionQuery;

                DataSet searchQueryResults = Helpers.DbEngine.ExecuteDataSet(searchQuery);
                if (searchQueryResults.HasData())
                {
                    Item item;
                    foreach (DataRow dr in searchQueryResults.Tables[0].Rows)
                    {
                        item = new Item();
                        item.ItemNumber = dr["Item#"].ToString();
                        item.ItemDescription = dr["ItemDescription"].ToString();
                        item.UM = dr["UM"].ToString();
                        item.UMPrice = dr["UM_Price"].ToString();
                        item.SalesCat1 = dr["SalesCat1"].ToString();
                        item.SalesCat4 = dr["SalesCat4"].ToString();
                        item.SalesCat5 = dr["SalesCat5"].ToString();
                        item.ShortDesc = dr["ShortDesc"].ToString();
                        item.StkType = dr["StkType"].ToString();
                        item.PrimaryUM = dr["PrimaryUOM"].ToString();
                        itemCollection.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return itemCollection;
        }

        public List<string> GetAppliedUMs(string ItemNumber)
        {
            List<string> AppliedUms = new List<string>();

            DataTable dt = DbEngine.ExecuteDataSet(@"select distinct(UMUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + ItemNumber + "')" +
           " union select distinct(UMRUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + ItemNumber + "')").Tables[0];

            foreach (DataRow item in dt.Rows)
            {
                AppliedUms.Add(item["UMUM"].ToString());
            }
            return AppliedUms;
        }

        public ObservableCollection<Item> GetSuuggestedItems(string customerID)
        {
            ObservableCollection<Item> itemCollection = new ObservableCollection<Item>();
            try
            {
                //TODO : Map suggested items specific to customer
                string suggestedItemQuery = "select IMLITM as Item#, IMDSC1 as ItemDescription, IMLITM ,1 as OrderQty,1 as PreOrderQty, 1 as UsualQty, IMUOM4 as UM, IMUOM4 as UM_Price, " +
                    "IMLNTY as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5, dateformat(today(), 'mm/dd/yyyy') as EffectiveDate, dateformat(today(), 'mm/dd/yyyy') as ExpiredDate,  IMITM as ShortDesc " +
                    "from busdta.F4101 left outer join busdta.F4102 on BUSDTA.F4101.IMLITM = busdta.f4102.IBLITM   " +
                    "where IMLITM like '%7801727%' or IMLITM like '%5881729%' or IMLITM like '%7851212%' or IMLITM like '%08111%' ";
                DataSet suggestedItems = Helpers.DbEngine.ExecuteDataSet(suggestedItemQuery);
                if (suggestedItems.HasData())
                {
                    Item item;
                    foreach (DataRow dr in suggestedItems.Tables[0].Rows)
                    {
                        item = new Item();
                        item.ItemNumber = dr["Item#"].ToString();
                        item.ItemDescription = dr["ItemDescription"].ToString();
                        item.UM = dr["UM"].ToString();
                        item.UMPrice = dr["UM_Price"].ToString();
                        item.SalesCat1 = dr["SalesCat1"].ToString();
                        item.SalesCat5 = dr["SalesCat5"].ToString();
                        item.ShortDesc = dr["ShortDesc"].ToString();
                        itemCollection.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return itemCollection;
        }
    }
}
