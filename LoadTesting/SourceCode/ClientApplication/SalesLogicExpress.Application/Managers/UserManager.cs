﻿using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    /// <summary>
    /// Static class for getting logged in user's Route and Branch
    /// </summary>
    public static class UserManager
    {
        private static string _userRoute,_RouteEntered, _userBranch, _userName,_RouteName;
        private static int _userId;
        public static string UserRoute
        {
            get
            {
                if (string.IsNullOrEmpty(_userRoute))
                {
                    _userRoute = DbEngine.ExecuteScalar("select FFROUT from busdta.F56M0001 where FFUSER =" +"'"+_RouteEntered+"'"+";");
                }
                return _userRoute.Trim();
            }
            set
            {
                _userRoute = value;
            }
        }
        public static string RouteEntered
        {
            get { return _RouteEntered; }
            set { _RouteEntered = value; }
        }
        public static string RouteName
        {
            get
            {
                if (string.IsNullOrEmpty(_RouteName))
                {
                    _RouteName = DbEngine.ExecuteScalar("select BUSDTA.GetUDCDescription('42', 'RT'," + "'" + UserRoute + "') 'RouteDescription' from dummy;");

                }
                return _RouteName.Trim(); 
            }
            set { _RouteName = value; }
        }
        public static string UserBranch
        {
            get
            {
                if (string.IsNullOrEmpty(_userBranch))
                {
                    _userBranch = DbEngine.ExecuteScalar("select FFMCU from busdta.F56M0001;");
                }
                return _userBranch.Trim();
            }
        }

        public static string UserName
        {
            get
            {
                return _userName.Trim();
            }
            set
            {
                _userName = value;
            }
        }

        public static int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public static int GetUserId(string UserName, string Password = "")
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar(" select App_user_id from busdta.user_master where App_User = '" + UserName + "' "));
        }
    }
}
