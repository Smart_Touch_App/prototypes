﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class CollectionManager
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CollectionManager");

        public CashDelivery GetCashDeliveryInfo(string CustmerId, string orderId)
        {
            try
            {
                CashDelivery cashDelivery = new CashDelivery();

                DataSet cashDeliveryResult = DbEngine.ExecuteDataSet(@" SELECT isnull(CSOBAL,0) PrvBal ,oh.Invoice_Total, isnull(CSOBAL,0) + isnull(oh.Invoice_Total,0) as Total ,isnull(pd.PDPAMT,0) as PDPAMT,isnull(pd.PDPMODE,0) as PDPMODE,pd.PDCHQNO,pd.PDCHQDT
                                                FROM BUSDTA.Order_Header oh 
                                                left join BUSDTA.Payment_Ref_Map prm on oh.Order_ID = PRM.Ref_Id AND PRM.Ref_Type='ord'
                                                left join BUSDTA.M03042 pd on prm.Payment_Id = pd.PDID
                                                left join BUSDTA.M03011 sm  on  oh.Customer_Id=sm.CSAN8                                               
                                                where oh.Customer_Id='" + CustmerId + "' and oh.Order_ID=" + orderId);

                if (cashDeliveryResult.HasData())
                {
                    cashDelivery.PreviousBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["PrvBal"].ToString());
                    cashDelivery.CurrentInvoiceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Invoice_Total"].ToString());
                    cashDelivery.TotalBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Total"].ToString());
                    cashDelivery.PaymentAmount = Convert.ToDouble(cashDeliveryResult.Tables[0].Rows[0]["PDPAMT"].ToString());
                    cashDelivery.PaymentMode = Convert.ToBoolean(cashDeliveryResult.Tables[0].Rows[0]["PDPMODE"]);
                    cashDelivery.ChequeNo = cashDeliveryResult.Tables[0].Rows[0]["PDCHQNO"].ToString();
                    cashDelivery.ChequeDate = DateTime.Today;
                    cashDelivery.CreditLimit = 100;

                }

                return cashDelivery;
            }
            catch (Exception ex)
            {
                log.Error("Error in GetCashDeliveryInfo" + ex.Message);
                throw;
            }
        }

        public ChargeOnAccount GetChargeOnAccountInfo(string CustmerId, string orderId, decimal TotalBalanceAmount)
        {
            try
            {
                ChargeOnAccount chargeOnAccount = new ChargeOnAccount();

                chargeOnAccount.TotalBalanceAmount = TotalBalanceAmount;
                chargeOnAccount.TemporaryCharge = 2;
                chargeOnAccount.RequestCode = "78949875";
                //chargeOnAccount.ApprovalCode = "1234-1234";
                chargeOnAccount.CreditLimit = 100;

                return chargeOnAccount;
            }
            catch (Exception ex)
            {
                log.Error("Error in GetChargeOnAccountInfo" + ex.Message);
                throw;
            }
        }

        public int SaveCashDeliveryInfo(CashDelivery cashDelivery, string orderId, string customerId, string company)
        {
            try
            {
                double UnAppliedAmount = cashDelivery.PaymentAmount - Convert.ToDouble(cashDelivery.TotalBalanceAmount);
                double OpenBalance = Convert.ToDouble(cashDelivery.TotalBalanceAmount) - cashDelivery.PaymentAmount;

                string chequeNo = string.IsNullOrEmpty(cashDelivery.ChequeNo) ? "NULL" : cashDelivery.ChequeNo;
                string chequeDate = cashDelivery.ChequeDate.ToString("yyyy-MM-dd");

                //Check for Payment Order Mapping Availability
                if (!IsPaymentAvailableForOrder(orderId))
                {
                    int NewPaymentId = Convert.ToInt32(DbEngine.ExecuteScalar("select isnull(max(PDID),0)+1 from BUSDTA.M03042 "));
                    //Save in Payment Details

                    DbEngine.ExecuteNonQuery(@"insert into BUSDTA.M03042 (PDAN8,PDCO,PDID,PDPAMT,PDPMODE,PDCHQNO,PDCHQDT,PDCRBY,PDCRDT,PDTRMD)
                                        values(" + customerId + ",'" + company + "'," + NewPaymentId.ToString() + "," + cashDelivery.PaymentAmount + "," + Convert.ToInt32(cashDelivery.PaymentMode) + "," + chequeNo + ",'" + chequeDate + "',NULL,now(),0)");
                    //Save in Payment Mapping
                    DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Payment_Ref_Map (Payment_Id,Ref_Id,Ref_Type) values(" + NewPaymentId.ToString() + "," + orderId + ",'ord')");

                    //Check for transaction summary available for this customer
                    string resultCount = DbEngine.ExecuteScalar("SELECT count(*) FROM BUSDTA.M03011 where CSAN8 =" + customerId + " and csco='" + company + "'");


                    //Save in Transaction Summary
                    if (Convert.ToInt32(resultCount) > 0)
                    {
                        // Update transaction Summary
                        DbEngine.ExecuteNonQuery(@"update  BUSDTA.M03011 set CSUAMT=CSUAMT + " + UnAppliedAmount.ToString() + ", CSOBAL =CSOBAL + " + OpenBalance.ToString() + " where CSAN8=" + customerId + " and CSCO='" + company + "'");
                    }
                    else
                    {
                        //Insert tansaction summary
                        DbEngine.ExecuteNonQuery(@"insert into BUSDTA.M03011 (CSAN8,CSCO,CSUAMT,CSOBAL)
                                            values(" + customerId + ",'" + company + "'," + UnAppliedAmount.ToString() + "," + OpenBalance.ToString() + ")");
                    }


                }
                else
                {
                    //Update Payment Details
                    DbEngine.ExecuteNonQuery(@"update BUSDTA.M03042  set PDPAMT=" + cashDelivery.PaymentAmount + ",PDPMODE=" + Convert.ToInt32(cashDelivery.PaymentMode) + ",PDCHQNO=" + chequeNo + ",PDCHQDT='" + chequeDate + "' where pdan8=" + customerId + " and pdco='" + company + "'");

                    // Update transaction Summary 
                    //TODO: For now in edit mode we are just aading on existing need to change logic later
                    DbEngine.ExecuteNonQuery(@"update  BUSDTA.M03011 set CSUAMT=CSUAMT + " + UnAppliedAmount.ToString() + ", CSOBAL =CSOBAL + " + OpenBalance.ToString() + " where CSAN8=" + customerId + " and CSCO='" + company + "'");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in SaveCashDeliveryInfo" + ex.Message);
                throw;
            }
            return 1;
        }

        public bool IsPaymentAvailableForOrder(string orderId)
        {
            string resultCount = DbEngine.ExecuteScalar("SELECT count(*) FROM BUSDTA.Payment_Ref_Map where Ref_Id=" + orderId + " and Ref_Type='ord' ");

            if (Convert.ToInt32(resultCount) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


    }
}
