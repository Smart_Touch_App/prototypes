﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using VertexWrapper;
using System.Configuration;
using SalesLogicExpress.Domain.Vertex;
using log4net;
using System.Runtime.Serialization.Json;
using System.IO;

namespace SalesLogicExpress.Application.Managers
{
    class TaxManager
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.TaxManager");
        public TaxManager()
        {
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][Start:TaxManagerConstructor]");
                string vertexPath = ConfigurationManager.AppSettings["VERTEX_PATH"].ToString();
                log.Info("Vertex_PATH =" + vertexPath);
                string getUserPath, GetProcessPath;
                getUserPath = Environment.GetEnvironmentVariable("PATH", System.EnvironmentVariableTarget.User);
                GetProcessPath = Environment.GetEnvironmentVariable("PATH", System.EnvironmentVariableTarget.Process);
                string SetUserPath, SetProcessPath;
                SetProcessPath = GetProcessPath + ";" + vertexPath;
                SetUserPath = getUserPath + ";" + vertexPath;
                Environment.SetEnvironmentVariable("PATH", SetUserPath, EnvironmentVariableTarget.User);
                Environment.SetEnvironmentVariable("PATH", SetProcessPath, EnvironmentVariableTarget.Process);
                log.Info("After Setting Path");
                log.Info("getUserPath =" + getUserPath);
                log.Info("GetProcessPath =" + GetProcessPath);
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][End:TaxManagerConstructor]");


            }
            catch (Exception e)
            {
                log.Error("[SalesLogicExpress.Application.Managers][TaxManager][TaxManagerConstructor][ExceptionStackTrace = " + e.StackTrace + "]");
            }

        }
      public  bool CalculateTax(ref InvoiceObject objOrderInvoice)
        {
            
          //Calculates tax for invoice
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][Start:CalculateTax]");
                VertexApi vertexApiobj = new VertexApi();

                int ItemNumberCount = objOrderInvoice.InvoiceItemList.Count; ;
                string CompCode = String.Empty;
                string Customer = String.Empty;
                string CustomerClassCode = String.Empty;
                double GeoCode =0;
                string Datasource = String.Empty;
                string InvoiceNumber = String.Empty;
                string ProductSetCode = String.Empty;
                string[] ItemNumber = new string[ItemNumberCount];
                string[] ProductCode = new string[ItemNumberCount];
                string[] ComponentCode = new string[ItemNumberCount];
                int[] ProductQty = new int[ItemNumberCount];
                double[] ExtendedAmount = new double[ItemNumberCount];
                double InvoiceTax = 0;
                double[] ItemTax = new double[ItemNumberCount];
                double GrossAmt = 0;
                bool ReturnStatus;

                CompCode = ConfigurationManager.AppSettings["CompanyCode"].ToString();
                Customer = objOrderInvoice.Customer;
                GeoCode = objOrderInvoice.GeoCode;
                CustomerClassCode = objOrderInvoice.CustomerClassCode;
                InvoiceNumber = objOrderInvoice.InvoiceNumber;
                ProductSetCode = objOrderInvoice.ProductSetCode;
                Datasource = ConfigurationManager.AppSettings["VERTEX_DataSource"].ToString();
                for (int index = 0; index < ItemNumberCount; index++)
                {
                    ItemNumber[index] = objOrderInvoice.InvoiceItemList[index].ItemNumber;
                    ProductCode[index] = objOrderInvoice.InvoiceItemList[index].ProductCode;
                    ComponentCode[index] = "";
                    ProductQty[index] = objOrderInvoice.InvoiceItemList[index].ProductQuantity;
                    ExtendedAmount[index] = Convert.ToDouble(objOrderInvoice.InvoiceItemList[index].ExtendedAmount);

                }
                //Api calls to vertex library
                unsafe
                {
                    vertexApiobj.Initialize(&ReturnStatus,Datasource);
                    vertexApiobj.SetInvoice(CompCode,
                                      Customer,
                                      CustomerClassCode,
                                      GeoCode,
                                      InvoiceNumber,
                                      ProductSetCode,
                                      ItemNumberCount,
                                      ItemNumber,
                                      ProductCode,
                                      ComponentCode,
                                      ProductQty,
                                      ExtendedAmount,
                                      &ReturnStatus);
                    vertexApiobj.GetTax(InvoiceNumber, &InvoiceTax, ItemTax, &GrossAmt, &ReturnStatus);
                    vertexApiobj.Cleanup(&ReturnStatus);

                }

                objOrderInvoice.GrossAmt = GrossAmt;
                objOrderInvoice.InvoiceTax = InvoiceTax;
                InvoiceObject orderInvoice = new InvoiceObject();
                orderInvoice = objOrderInvoice;
                for (int index = 0; index < ItemNumberCount; index++)
                {
                    objOrderInvoice.InvoiceItemList.Where(d => d.ItemNumber == orderInvoice.InvoiceItemList[index].ItemNumber).FirstOrDefault().ItemTax = ItemTax[index];
                }
                objOrderInvoice = orderInvoice;
                log.Info("Invoice Tax : = " + objOrderInvoice.InvoiceTax);
                string BaseDirectory = System.AppDomain.CurrentDomain.BaseDirectory + "VertexInvoices\\";
                if (!System.IO.Directory.Exists(BaseDirectory))
                {
                    Directory.CreateDirectory(BaseDirectory);
                }
                FileStream fse = new FileStream("VertexInvoices\\" + objOrderInvoice.InvoiceNumber + "_"+objOrderInvoice.Customer+(InvoiceTax==0?"_NO_TAX":"_TAX")+ ".txt", FileMode.Create, FileAccess.Write);
                DataContractJsonSerializer DCJS = new DataContractJsonSerializer(typeof(InvoiceObject));
                DCJS.WriteObject(fse, objOrderInvoice);
                fse.Close();
               log.Info("[SalesLogicExpress.Application.Managers][TaxManager][End:TaxManagerConstructor]");
                return true;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][TaxManager][CalculateTax][ExceptionStackTrace = " + ex.StackTrace + "]");
                return false;
            }
           
            
        }

    
    }
}
