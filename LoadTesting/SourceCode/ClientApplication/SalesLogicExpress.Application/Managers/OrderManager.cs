﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
namespace SalesLogicExpress.Application.Managers
{
    public class OrderManager
    {

        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.OrderManager");
        public OrderManager()
        {

        }

        public OrderSubState GetOrderSubStatus(string orderId)
        {
            string substate = DbEngine.ExecuteScalar("select order_sub_state from busdta.order_header WHERE order_id=" + orderId);

            if (string.IsNullOrEmpty(substate))
            {
                return OrderSubState.None;
            }
            else
            {
                return OrderSubState.Hold.ToString() == substate ? OrderSubState.Hold : OrderSubState.Void;
            }

        }

        public bool IsItemPickedForOrder(string orderId)
        {
            int itemCount = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.pickOrder where order_id=" + orderId + " and Picked_Qty_Primary_UOM !=0"));
            if (itemCount == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public int GetNewOrderNum()
        {
            int OrderNum = 0;
            string strOrderNum = DbEngine.ExecuteScalar("select  isnull(max(OrderSeries),0)+1 from BUSDTA.Order_Header;");
            OrderNum = Convert.ToInt32(ViewModels.CommonNavInfo.UserRoute.Trim() + strOrderNum.ToString());
            return OrderNum;
        }

        public int GetUncomletedLastOrderIdIfAny(string CustomerId)
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar("select isnull(max(Order_ID),0) from BUSDTA.Order_Header where customer_id=" + CustomerId + " and Order_state != '" + OrderState.DeliveryConfirmation + "' and (order_sub_state !='" + OrderSubState.Hold + "' and order_sub_state !='" + OrderSubState.Void + "')"));
        }

        public int SaveOrder(int OrderId, int CustomerId, ObservableCollection<OrderItem> OrderItems)
        {
            int result = 0;

            try
            {
                //check for Order id
                string strOrderId = DbEngine.ExecuteScalar("select Order_ID from BUSDTA.Order_Header where Order_ID='" + OrderId.ToString() + "'");

                //Add mode
                if (string.IsNullOrEmpty(strOrderId))
                {
                    //Insert in Order_Header
                    result = SaveOrderHeader(OrderId, CustomerId);
                }
                else
                {
                    //Update Order_Header
                    UpdateOrderHeader(OrderId);

                }

                //Delete and Insert in Order_details
                DeleteAndInsertOrderItems(OrderItems, OrderId);

            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public void DeleteOrderItems(int OrderId)
        {
            try
            {
                DbEngine.ExecuteNonQuery("delete from BUSDTA.Order_Detail where Order_ID = " + OrderId.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetNewOrderSeries()
        {
            int OrderSeries = 0;
            string strOrderNum = DbEngine.ExecuteScalar("select isnull(max(OrderSeries),0)+1 from BUSDTA.Order_Header;");
            OrderSeries = Convert.ToInt32(strOrderNum);
            return OrderSeries;
        }

        public int SaveOrderHeader(int OrderId, int CustomerId)
        {
            int result = 0;
            try
            {
                result = DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Order_Header (
                    Order_ID,
                    OrderSeries,
                    RouteNo,
                    Customer_Id,
                    Order_Date,
                    Created_By,
                    Created_On,
                    Is_Deleted)
                    values(
                    '" + OrderId + @"', " + GetNewOrderSeries() + "," + ViewModels.CommonNavInfo.UserRoute + "," +
                    "'" + CustomerId + @"',
                    date(now()),
                    " + Managers.UserManager.UserId + @",
                    now(),
                    0
                    );");
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public int UpdateOrderHeader(int OrderId)
        {
            int result = 0;
            try
            {
                result = DbEngine.ExecuteNonQuery(@"update BUSDTA.Order_Header set                     
                    Order_Date = date(now()),
                    Created_On = date(now())
                    where  Order_ID=" + OrderId.ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }


        public ObservableCollection<ViewModels.ReasonCode> GetReasonListForVoidOrder()
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();
            DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Order'").Tables[0];

            ViewModels.ReasonCode ReasonCode = new ViewModels.ReasonCode();


            foreach (DataRow dr in dt.Rows)
            {
                ReasonCode = new ViewModels.ReasonCode();
                ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                ReasonCode.Code = dr["Code"].ToString();
                reasonCodeList.Add(ReasonCode);
            }


            return reasonCodeList;

        }

        public ObservableCollection<ViewModels.ReasonCode> GetReasonListFor(string Type)
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();
            DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='" + Type + "'").Tables[0];

            ViewModels.ReasonCode ReasonCode = new ViewModels.ReasonCode();


            foreach (DataRow dr in dt.Rows)
            {
                ReasonCode = new ViewModels.ReasonCode();
                ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                ReasonCode.Code = dr["Code"].ToString();
                reasonCodeList.Add(ReasonCode);
            }


            return reasonCodeList;

        }


        public void DeleteAndInsertOrderItems(ObservableCollection<OrderItem> OrderItems, int OrderId)
        {
            try
            {
                DeleteOrderItems(OrderId);

                foreach (OrderItem orderItem in OrderItems)
                {
                    DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Order_Detail(
                        Order_ID,
                        Item_Number,
                        Order_Qty,
                        Order_UOM,
                        Unit_Price,
                        Extn_Price,
                        Reason_Code
                        )values
                        (
                        '" + OrderId + @"',
                        '" + orderItem.ItemNumber + @"',
                        " + orderItem.OrderQty + @",
                        '" + orderItem.UM + @"',
                        " + orderItem.UnitPrice + @",
                        " + orderItem.ExtendedPrice + @",
                        '" + orderItem.ReasonCode + @"'
                        );
                        ");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ObservableCollection<OrderItem> GetOrderForCustomer()
        {
            return null;
        }
        public void CreateOrderForCustomer()
        {

        }
        /// <summary>
        /// Gets the Order history for customer
        /// </summary>
        /// <param name="customerID">Cutomer No</param>
        /// <returns>DataSet with two tables, first table has OrderHistory Items and the second table includes Order Date headers</returns>
        public DataSet GetOrderHistoryForCustomer(string customerID)
        {
            DataSet dsOrderHistory = null;
            try
            {
                Dictionary<string, object> procParamaters = new Dictionary<string, object>();
                procParamaters.Add("@shipTo", customerID);
                int result = Helpers.DbEngine.ExecuteNonQuery("BUSDTA.prepareHistoryRecords", procParamaters, true);

                string queryOrderHistory = "select *,0 as AverageQty from busdta.localOrderHistoryPanel";
                string queryOrderHistoryHeader = "select chronology,convert(varchar(15),SHDOCO) as OrderNumber,Dateformat(BUSDTA.DateG2J(SHTRDJ),'mm/dd/yyyy') as OrderDate from busdta.localLastOrders order by convert(varchar(15),SHDOCO) desc";

                Dictionary<string, string> orderHistoryAndDateQueryList = new Dictionary<string, string>();
                orderHistoryAndDateQueryList.Add("OrderHistory", queryOrderHistory);
                orderHistoryAndDateQueryList.Add("OrderHistoryHeaders", queryOrderHistoryHeader);
                dsOrderHistory = Helpers.DbEngine.ExecuteDataSet(orderHistoryAndDateQueryList);


                if (dsOrderHistory.HasData())
                {
                    dsOrderHistory.Tables[0].Columns["imlitm"].ColumnName = "ItemCode";
                    dsOrderHistory.Tables[0].Columns["imdsc1"].ColumnName = "ItemDesc";
                    int historyAvailableFor = dsOrderHistory.Tables[0].Columns.Count - 3;
                    foreach (DataRow orderHistoryItem in dsOrderHistory.Tables[0].Rows)
                    {
                        double average = 0;
                        historyAvailableFor = dsOrderHistory.Tables[0].Columns.Count - 3;

                        if (historyAvailableFor > 10)
                        {
                            historyAvailableFor = 10;
                        }

                        for (int i = 0; i < historyAvailableFor; i++)
                        {
                            average = average + (string.IsNullOrEmpty(orderHistoryItem["H" + (i + 1) + "_Qty"].ToString()) ? 0 : Convert.ToInt32(orderHistoryItem["H" + (i + 1) + "_Qty"]));
                            if (string.IsNullOrEmpty(orderHistoryItem["H" + (i + 1) + "_Qty"].ToString()))
                            {
                                orderHistoryItem["H" + (i + 1) + "_Qty"] = 0;
                            }
                        }
                        average = (average == 0) ? 0 : average / historyAvailableFor;
                        orderHistoryItem["AverageQty"] = average;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }

            return dsOrderHistory;
        }

        public DataTable GetNewOrderItems(string CustomerId)
        {
            DataTable dt = new DataTable();
            DataTable ExpectedDT = new DataTable();

            dt = DbEngine.ExecuteDataSet(@"select od.Item_Number, im.imdsc1,od.Order_Qty,oh.order_date,oh.order_id
 from BUSDTA.Order_Detail od join BUSDTA.Order_Header oh on od.order_id=oh.order_id join busdta.F4101 im on od.Item_Number= im.IMLITM where oh.customer_id='" + CustomerId + "'" +
" order by oh.order_id").Tables[0];

            string previousOrderId = "";

            ExpectedDT.Columns.Add("Item Number");
            ExpectedDT.Columns.Add("Item Desc");

            foreach (DataRow item in dt.Rows)
            {
                if (item["order_id"].ToString() != previousOrderId)
                {
                    previousOrderId = item["order_id"].ToString();
                    ExpectedDT.Columns.Add("H_Qty");
                }
                if (ExpectedDT.Select("Item_Number ='" + item["Item_Number"].ToString() + "'").CopyToDataTable().Rows.Count > 0) ;
                {

                }
            }

            return ExpectedDT;
        }
        public void SaveInvoiceItemsForReport(ObservableCollection<OrderItem> orderItems, int invoiceNo)
        {
            try
            {
                string truncateInvoiceDetailsQuery = "truncate table BUSDTA.InvoiceDetails;";
                Helpers.DbEngine.ExecuteNonQuery(truncateInvoiceDetailsQuery);

                string insertInvoiceDetailsQuery = "BUSDTA.SaveMasterInvoice";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                foreach (OrderItem item in orderItems)
                {
                    parameters.Clear();
                    parameters.Add("@InvoiceNo", invoiceNo);
                    parameters.Add("@OrderQty", item.OrderQty);
                    parameters.Add("@UM", item.UM);
                    parameters.Add("@ItemCode", item.ItemNumber);
                    parameters.Add("@ProductDesc", item.ItemDescription);
                    parameters.Add("@UnitPrice", item.UnitPrice);
                    parameters.Add("@ExtendedPrice", item.ExtendedPrice);
                    parameters.Add("@OrderDate", item.OrderDate);
                    parameters.Add("@SalesCat1", item.SalesCat1);
                    Helpers.DbEngine.ExecuteNonQuery(insertInvoiceDetailsQuery, parameters, true);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public OrderItem ApplyPricingToItem(Item item, string routeBranch, string shipToCustomerID)
        {
            decimal unitPrice = 0;
            Random randomProvider = new Random();
            OrderItem orderItem = new OrderItem(item);
            PricingManager pricingManager = new PricingManager();
            unitPrice = 0;
            orderItem.QtyOnHand = randomProvider.Next(0, 50);
            orderItem.OrderQty = 1;
            unitPrice = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
            orderItem.UnitPrice = unitPrice == 0 ? unitPrice : unitPrice;
            orderItem.ExtendedPrice = (orderItem.UM == orderItem.UMPrice) ? decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice : (decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice * decimal.Parse(pricingManager.jdeUOMConversion(orderItem.UMPrice, orderItem.UM, int.Parse(orderItem.ShortDesc)).ToString()));
            orderItem.AppliedUMS = new ItemManager().GetAppliedUMs(orderItem.ItemNumber).Count != 0 ? new ItemManager().GetAppliedUMs(orderItem.ItemNumber) : new List<string>() { orderItem.UM };
            return orderItem;
        }
        public DataTable GetAllOpenOrdersForItem(string ItemNumber)
        {
            DataTable dt = new DataTable();

            dt = DbEngine.ExecuteDataSet(@"SELECT oh.Order_id,customer_id,ct.ABALPH CustomerName,pk.Order_Qty_Primary_UOM,pk.Primary_UOM,pk.Picked_Qty_Primary_UOM FROM BUSDTA.Order_Detail od join BUSDTA.Order_Header oh on od.Order_ID = oh.Order_ID join BUSDTA.f0101 ct on ct.ABAN8=oh.Customer_Id 
join busdta.PickOrder pk on oh.Order_ID=pk.Order_ID and od.Item_Number=pk.Item_Number where od.Item_Number='" + ItemNumber + "' and oh.Order_State!='" + OrderState.OrderDelivered.ToString() + "'").Tables[0];
            return dt;
        }

        public class PriceOvrCodes
        {
            public ObservableCollection<PriceOvrCodes> GetPriceOvrCodes()
            {
                ObservableCollection<PriceOvrCodes> codes = new ObservableCollection<PriceOvrCodes>();
                codes.Add(new PriceOvrCodes { Id = 1, Name = "" });
                codes.Add(new PriceOvrCodes { Id = 2, Name = "CPR" });
                codes.Add(new PriceOvrCodes { Id = 3, Name = "PKN" });
                codes.Add(new PriceOvrCodes { Id = 4, Name = "SMP" });
                codes.Add(new PriceOvrCodes { Id = 5, Name = "TPR" });
                return codes;
            }
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public void SaveVoidOrderReasonCode(string orderNo, int reasonId)
        {
            string query = "update busdta.Order_Header set VoidReason='{0}', updated_at=now() where Order_ID='{1}'";
            query = string.Format(query, reasonId.ToString(), orderNo.ToString());
            DbEngine.ExecuteNonQuery(query);
        }
        public static void UpdateOrderStatus(string orderNo, OrderState state, OrderSubState subState)
        {
            string query = "update busdta.Order_Header set Order_State='{0}' , Order_Sub_State='{1}', updated_at=now() where Order_ID='{2}'";
            query = string.Format(query, state.ToString(), (subState.ToString() == "None" ? "" : subState.ToString()), orderNo);
            DbEngine.ExecuteNonQuery(query);
            SalesLogicExpress.Application.ViewModels.Order.OrderStatus = state;
            SalesLogicExpress.Application.ViewModels.Order.OrderSubStatus = subState;
            ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.HoldOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);
        }
        public enum OrderState
        {
            OrderTemplate,
            OrderEntry,
            OrderPreview,
            OrderAcceptance,
            PickOrder,
            CashCollection,
            DeliveryConfirmation,
            DeliveryAcceptance,
            OrderDelivered
        }
        public enum OrderSubState
        {
            Void,
            Hold,
            None
        }
        public List<string> ValidateUM(string ItemCode)
        {
            List<string> validUM = new List<string>();
            try
            {

                DataTable dt = new DataTable();

                dt = DbEngine.ExecuteDataSet("select   distinct(UMUM) UM from busdta.F41002,busdta.F4101 union " +
                                                "select   distinct(UMRUM) UM from busdta.F41002,busdta.F4101 " +
                                                "where umitm = imitm and rtrim(imlitm) = '" + ItemCode + "'").Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    validUM.Add(dr["UM"].ToString());

                }
            }
            catch (Exception e)
            {
                log.Error("Page:OrderManager.cs,Method:validateUM Parameter:" + ItemCode + " Message: " + e.StackTrace);
            }

            return validUM;
        }
        public string GetParent(int mnABNumber)
        {
            string txtShipTo = null;
            string txtParent = null;
            string txtParentDesc = null;

            txtParent = DbEngine.ExecuteScalar("SELECT AIAN8 from BUSDTA.F03012 where AIAN8 in (select MAPA8 from BUSDTA.F0150 where MAAN8=" + mnABNumber + " and MAOSTP='   ')");
            if (string.IsNullOrEmpty(txtParent))
            {
                txtParent = txtShipTo;
            }

            txtParentDesc = DbEngine.ExecuteScalar("select ABAN8 ||', '|| ABALPH AS ParentDesc from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where rtrim(ltrim(ABAN8)) = '" + txtParent + "'");
            if (string.IsNullOrEmpty(txtParentDesc))
            {
                txtParentDesc = txtShipTo;
            }

            return txtParentDesc;
        }

        public string GetSoldTo(int mnABNumber)
        {
            string txtBillTo = null;
            string txtSoldTo = null;

            txtBillTo = DbEngine.ExecuteScalar("SELECT AIAN8 from BUSDTA.F03012 where AIAN8 in (Select ABAN81 from BUSDTA.F0101 where ABAN8=" + mnABNumber + ")");

            //cmd.Parameters.Add("@AN8", SADbType.Integer).Value = mnABNumber;

            txtSoldTo = DbEngine.ExecuteScalar("select ABAN8 ||', '|| ABALPH AS SoldTo from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where rtrim(ltrim(ABAN8)) = '" + txtBillTo + "'");

            if (string.IsNullOrEmpty(txtSoldTo))
            {
                txtSoldTo = txtBillTo;
            }

            return txtSoldTo;
        }
        public string GetHoldCode(string strBranchId)
        {
            return DbEngine.ExecuteScalar("select AIHDAR from busdta.F03012 where aian8 = '" + strBranchId + "'");
        }

    }
}
