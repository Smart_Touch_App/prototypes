﻿using iAnywhere.MobiLink.Client;
using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
namespace SalesLogicExpress.Application.Managers
{
    /// <summary>
    /// This class includes all methods necessary to setup route database and validate  users
    /// *******************************************************************************************************************
    /// Setting up a user typically involves
    /// *******************************************************************************************************************
    /// 1. Check whether the database for the given route is present or not
    /// 2. Check whether the user is registered in the database.
    /// 3. If the route database exists then it will have all authorized users in it(pulled through earlier db sync) and if
    ///     the user trying to log in is not present in the database, then the user is invalid.
    /// 4. If the route database doesnot exists then create remote database for the route
    ///     4.1 Register remoteid for the database using the route and device info
    ///     4.2 Add entry in remote database for route to device mapping.
    /// 5. Sync the database (This will pull in all data for route and users along with it.)
    /// 6. Now Check whether the user is registered in the newly created database from step 4.
    /// 7. if the user trying to log in is not present in the database, then the user is invalid.
    /// *******************************************************************************************************************
    /// </summary>
    public class SetupManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.UserLogin");

        private string remoteDBPrefix = "remote_";
        bool IsFirstLogin = false;
        AppServiceManager serviceManager;
        public SetupManager()
        {
            serviceManager = new AppServiceManager();
        }

        private string routeDbConnectionString = System.Configuration.ConfigurationManager.AppSettings["connectionString"].ToString();    //"Dsn=dsn_remote_{0};uid=DBA;databasename=remote_db;servername=remote_eng;integrated=NO;encryption=NONE";

        public string RouteDbConnectionString { get { return routeDbConnectionString; } }

        public string RemoteID { get; set; }

        /// <summary>
        /// Directory where the route database is to be created
        /// </summary>
        public Boolean IsRouteDatabasePresent(string route)
        {
            String databaseDirectory = string.Concat(ResourceManager.DatabaseDirectory, "\\", remoteDBPrefix, route);
            bool dbFileExists = System.IO.Directory.Exists(databaseDirectory) && System.IO.File.Exists(string.Concat(databaseDirectory, "\\remote.db"));
            String userDatabaseConnection = string.Format(routeDbConnectionString, route);
            String selectAppUserCommand = "select App_User from BUSDTA.user_master";
            DataSet appUsers = DbEngine.ExecuteDataSet(selectAppUserCommand);
            return appUsers.HasData() && dbFileExists ? true : false;
        }

        public void SetRemoteID(string deviceID, string route)
        {
            RemoteID = string.Concat(deviceID, "_", route);
        }
        public Boolean IsDeviceRegistered(string deviceID)
        {
            String checkDeviceRegistrationCommand = string.Format("select * from busdta.Device_Master dm where dm.Device_Id = ('{0}') and Active='1'", deviceID);
            DataSet deviceRegistered = DbEngine.ExecuteDataSet(checkDeviceRegistrationCommand);
            return deviceRegistered.HasData() ? true : false;

        }
        public Boolean ValidateUser(string userName, string password, string route, string deviceID)
        {
            String validateUserCommand = string.Format("select app_user from busdta.user_master um, busdta.Device_Master dm " +
                    "where um.app_user = '{0}' and um.app_user_id in (select app_user_id from busdta.Route_User_Map where route_id ='{1}') " +
                "and dm.Device_Id = ('{2}')", userName, route, deviceID);
            DataSet appUser = DbEngine.ExecuteDataSet(validateUserCommand);
            return appUser.HasData() ? true : false;
        }

        public void CreateRouteDatabase(String route)
        {
            CopyRequiredFiles(route);
            String databaseDirectory = string.Concat(ResourceManager.DatabaseDirectory, "\\", remoteDBPrefix, route);
            //CreateEnvironmentVariables(databaseDirectory);
            CreateRemoteDB(databaseDirectory, route);
            IsFirstLogin = true;// This flags indicates the very first time login. This flag helps to synchronize for very first time.
        }

        public void CleanupInstalledDatabase(string route)
        {
            string installedDatabaseDirectory = string.Concat(ResourceManager.DatabaseDirectory, "\\", remoteDBPrefix, route);
            DbmlsyncClient syncClient;
            syncClient = DbmlsyncClient.InstantiateClient();
            syncClient.Init();
            syncClient.SetProperty("server path", "C:\\Program Files\\SQL Anywhere 16\\Bin64");
            //syncClient.StartServer(3426, "-c SERVER=remote_eng;DBN=remote_db;UID=dba;PWD=sql", 5000, out dbStartType);
            syncClient.Connect(null, 3426, "dba", "sql");
            syncClient.ShutdownServer(DBSC_ShutdownType.DBSC_SHUTDOWN_CLEANLY);
            syncClient.WaitForServerShutdown(10000);
            syncClient.Disconnect();
            syncClient.Fini();
            DeleteFiles(installedDatabaseDirectory);
        }
        async void DeleteFiles(string installDirectory)
        {
            System.Threading.Thread.Sleep(2000);
            await Task.Run(() =>
            {
                //System.IO.Directory.Delete(installDirectory, true);
            });
        }
        private void CopyRequiredFiles(String route)
        {
            string sourcePath = ResourceManager.DatabaseResourcesDirectory;
            string targetPath = string.Concat(ResourceManager.DatabaseDirectory, "\\", remoteDBPrefix, route);
            string fileName = null;
            string destFile = null;
            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }
            // To copy all the files in one directory to another directory.
            // Get the files in the source folder. (To recursively iterate through
            // all subfolders under the current directory, see
            // "How to: Iterate Through a Directory Tree.")
            // Note: Check for target path was performed previously
            //       in this code example.
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);
                // Copy the files and overwrite destination files if they already exist.
                foreach (string s in files)
                {
                    // Use static Path methods to extract only the file name from the path.
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(targetPath, fileName);
                    System.IO.File.Copy(s, destFile, true);
                }
            }
        }

        private void CreateEnvironmentVariables(String databaseDirectory)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + "sqlanyenv.bat");
            processInfo.UseShellExecute = false;
            processInfo.WorkingDirectory = databaseDirectory;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;
            process = Process.Start(processInfo);
            process.WaitForExit();
            // *** Read the streams ***
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            exitCode = process.ExitCode;
            process.Close();
        }

        private void CreateRemoteDB(String databaseDirectory, String route)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SetupManager][start : CreateRemoteDB()][Parameters:route=" + route + "][Start time: " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt") + "]");
                int exitCode;
                ProcessStartInfo processInfo;
                Process process;
                processInfo = new ProcessStartInfo("cmd.exe", "/c " + "setup2.bat 123" + " " + route);
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.WorkingDirectory = databaseDirectory;
                // *** Redirect the output ***
                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;
                process = Process.Start(processInfo);
                // *** Read the streams ***
                string output = process.StandardOutput.ReadToEnd();
                string error = process.StandardError.ReadToEnd();
                exitCode = process.ExitCode;
                process.Close();
                Logger.Info("[SalesLogicExpress.Application.Managers][SetupManager][End : CreateRemoteDB()][End time: " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt") + "]");
            }
            catch (Exception ex)
            {
                Logger.Error(" [SalesLogicExpress.Application.Managers][SetupManager][CreateRemoteDB()][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }
        // This method replaces the localhost in the Rremote_setup file with the IP address of the Mobilink Server
        public void ConfigureRemoteDbEndPoint(string endPoint)
        {
            try
            {
                string defaultIP = "host=localhost";
                string baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
                string remoteSqlPath = baseDirectory + @"\Resources\RemoteDbSetupFiles\remote_setup.sql";
                string customSqlPath = baseDirectory + @"\Resources\RemoteDbSetupFiles\CustomizeRemote.sql";
                System.IO.DriveInfo[] allDrives = System.IO.DriveInfo.GetDrives();
                string drivePath;

                string text = System.IO.File.ReadAllText(remoteSqlPath);
                text = text.Replace(defaultIP, "host=" + endPoint);
                System.IO.File.WriteAllText(remoteSqlPath, text);

                text = System.IO.File.ReadAllText(customSqlPath);
                text = text.Replace(defaultIP, "host=" + endPoint);
                System.IO.File.WriteAllText(customSqlPath, text);
                return;
            }
            catch (Exception ex)
            {
                Logger.Error(" [SalesLogicExpress.Application.Managers][SetupManager][ConfigureRemoteDbEndPoint][endpoint=" + endPoint + "][ExceptionStackTrace = "+ex.StackTrace+"]");
                
            }
            //bool filePresent = false;
            //foreach (System.IO.DriveInfo d in allDrives)
            //{
            //    if (d.IsReady == true)
            //    {
            //        if (d.DriveType == System.IO.DriveType.Fixed)
            //        {
            //            drivePath = d.Name + @"\RemoteIP.txt";
            //            filePresent = System.IO.File.Exists(drivePath );
            //            if (filePresent)
            //            {
            //                endPoint = System.IO.File.ReadAllText(drivePath);

            //                string text = System.IO.File.ReadAllText(remoteSqlPath);
            //                text = text.Replace(defaultIP, endPoint);
            //                System.IO.File.WriteAllText(remoteSqlPath, text);

            //                text = System.IO.File.ReadAllText(customSqlPath);
            //                text = text.Replace(defaultIP, endPoint);
            //                System.IO.File.WriteAllText(customSqlPath, text);
            //                break;
            //            }
            //        }
            //    }
            //}
            
        }
        private bool SynchronizeDatabase(String publication)
        {
            if (!serviceManager.IsServerReachable() || !ResourceManager.NetworkInfo.IsInterNetConnected())
            {
                return false;
            }
            DBSC_Event dbEvent;
            UInt32 syncHandle;
            bool isSyncSuccessful = true;
            Logger.Info(" [SalesLogicExpress.Application.Managers][SetupManager][SynchronizeDatabase()][Params:Publication=" + publication + "]");
            syncHandle = ResourceManager.Synchronization.SyncClient.Sync(publication, "");
            ResourceManager.Synchronization.syncHandles.Add(syncHandle);
            while (ResourceManager.Synchronization.SyncClient.GetEvent(out dbEvent, DbmlsyncClient.DBSC_INFINITY)
                      == DBSC_GetEventRet.DBSC_GETEVENT_OK)
            {
                if (dbEvent.hdl == syncHandle)
                {
                    Console.WriteLine("Event Type : {0}", dbEvent.type);
                    if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG)
                    {
                        Console.WriteLine("Info : {0}", dbEvent.str1);
                        isSyncSuccessful = false;
                        ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.SyncFailed);
                        Logger.Error(" [SalesLogicExpress.Application.Managers][SetupManager][SynchronizeDatabase()][Params:Publication=" + publication + "]");
                        break;
                    }
                    if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_FIRST_INTERNAL)
                    {
                        Console.WriteLine("Info : {0}", dbEvent.str1);
                    } if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_INFO_MSG)
                    {
                        Console.WriteLine("Info : {0}", dbEvent.str1);
                    }
                    if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG)
                    {
                        if (dbEvent.str1.Contains("Network Error")) {
                            ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.SyncFailed);
                            Logger.Error(" [SalesLogicExpress.Application.Managers][SetupManager][SynchronizeDatabase()][Params:Publication=" + publication + "]");
                            break;
                        }
                    }
                    if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_UPLOAD_COMMITTED)
                    {
                        ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.UploadComplete);
                    }
                    if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_SYNC_DONE)
                    {
                        ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.SyncComplete);
                    }
                    if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_DOWNLOAD_COMMITTED)
                    {
                        ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.DownloadComplete);
                        break;
                    }
                }
            }
            if (IsFirstLogin)
            {
                // The client is shutdown inorder to pick the other publications that are generated after the initial setup
                // when the user is customized.
                ResourceManager.Synchronization.ShutDownSyncClient();
            }
            else
            {
              //  ResourceManager.Synchronization.RestartSyncClient();
            }
            Logger.Info(" [SalesLogicExpress.Application.Managers][SetupManager][SynchronizeDatabase()][Params:Publication=" + publication + "][Info : End Synchronization time : " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt") + "]");
            return isSyncSuccessful;
        }

        public void RegisterDeviceAsRemote(String remoteID, String deviceID, String route)
        {
            // 1. Check if the device is already registered, if yes then return
            string isDeviceRegisteredCommand = string.Format("select Device_Id from BUSDTA.Route_Device_Map where Device_Id = '{0}' and Remote_Id = '{1}' and Route_Id = '{2}'", deviceID, remoteID, route);
            DataSet registeredDevices = DbEngine.ExecuteDataSet(isDeviceRegisteredCommand);
            if (registeredDevices.HasData())
            {
                return;
            }

            // 2. if the device is not registered set up remote
            String setRemoteIDcommand = string.Format("SET OPTION PUBLIC.ml_remote_id = '{0}'", remoteID); ;
            DbEngine.ExecuteNonQuery(setRemoteIDcommand);

            // 3. Add mapping in route db for device
            String addRouteToDeviceMappingCommand = string.Format("insert into busdta.Route_Device_Map (Route_Id, Device_Id, Active, Remote_Id) values ('{0}', '{1}', 1, '{2}')", route, deviceID, remoteID);
            DbEngine.ExecuteNonQuery(addRouteToDeviceMappingCommand);
        }

        public bool SynchronizeUsers()
        {
            return SynchronizeDatabase("prof_validate_user");
        }

        public bool SynchronizeDataForRoute()
        {
            IsFirstLogin = false;
            return SynchronizeDatabase("prof_everything");
        }

        public void SubscribeUserToPublication(String route, String loginID)
        {
            String databaseDirectory = null;
            databaseDirectory = string.Concat(ResourceManager.DatabaseDirectory, "\\", remoteDBPrefix, route);
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + "CustomizeRemote.bat " + route + " " + route);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.WorkingDirectory = databaseDirectory;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;
            // processInfo.Arguments = "123" + " " + strRoute;

            process = Process.Start(processInfo);
            // process.WaitForExit();

            // *** Read the streams ***
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();
        }
    }
}