﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using SalesLogicExpress.Application.Helpers;
using System.Collections.ObjectModel;
namespace SalesLogicExpress.Application.Managers
{
    public class SyncQueueManager
    {
        public event EventHandler<SyncQueueChangedEventArgs> QueueChanged;
        Queue<string> _syncQueueEverything = new Queue<string>();
        Queue<string> _syncQueueImmediate = new Queue<string>();
        public class SyncQueueItem
        {
            public string Transaction { get; set; }
            public int Count { get; set; }
        }
        public int TotalPendingSync { get; set; }
        public SyncQueueManager()
        {
        }
        public ObservableCollection<SyncQueueItem> GetPendingSyncItems()
        {
            ObservableCollection<SyncQueueItem> item = new ObservableCollection<SyncQueueItem>();
            string Query = string.Format("select transaction, count(transaction) as count from busdta.SYNC_QUEUE group by transaction HAVING transaction <> 'Get Route Data' and transaction <> 'Users'");
            DataSet dsTransactionQueue = Helpers.DbEngine.ExecuteDataSet(Query);
            if (dsTransactionQueue.HasData())
            {
                foreach (DataRow dr in dsTransactionQueue.Tables[0].Rows)
                {
                    item.Add(new SyncQueueItem { Count = Convert.ToInt32(dr["count"]), Transaction = dr["transaction"].ToString() });
                }

                System.Diagnostics.Debug.WriteLine(string.Format("GetPendingSyncItems {0}", dsTransactionQueue.Tables[0].Rows.Count));
            }
            return item;
        }
        public void InitializeQueue()
        {
            string Query = string.Format("Select * from BUSDTA.SYNC_QUEUE");
            DataSet dsTransactionQueue = Helpers.DbEngine.ExecuteDataSet(Query);
            if (dsTransactionQueue.HasData())
            {
                // Add to in memory queue
                foreach (DataRow transaction in dsTransactionQueue.Tables[0].Rows)
                {
                    if (transaction["priority"].ToString().Equals("everything"))
                    {
                        SyncQueueEverything.Enqueue(transaction["publicationProfile"].ToString());
                    }
                    else
                    {
                        SyncQueueImmediate.Enqueue(transaction["publicationProfile"].ToString());
                    }
                }
                SyncQueueChangedEventArgs args = new SyncQueueChangedEventArgs();
                args.State = QueueChangeType.Added;
                args.StateChangedTime = DateTime.Now;
                OnQueueStateChanged(args);
            }
        }
        public Queue<string> SyncQueueEverything
        {
            get
            {
                return _syncQueueEverything;
            }
            set
            {
                _syncQueueEverything = value;
            }
        }
        public Queue<string> SyncQueueImmediate
        {
            get
            {
                return _syncQueueImmediate;
            }
            set
            {
                _syncQueueImmediate = value;
            }
        }
        protected virtual void OnQueueStateChanged(SyncQueueChangedEventArgs e)
        {
            EventHandler<SyncQueueChangedEventArgs> handler = QueueChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public void AddTransactionToQueue(Transaction transaction, Priority priority)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("AddTransactionToQueue {0}", transaction));
            string transactionName = string.Empty;
            transactionName = transaction.GetEnumDescription().Split('#')[1];
            int recordsAffected = -1;
            if ("immediate".Contains(priority.GetEnumDescription()))
            {
                if (!SyncQueueImmediate.Contains(transactionName))
                {
                    System.Diagnostics.Debug.WriteLine(string.Format("immediate {0}", transactionName));
                    SyncQueueImmediate.Enqueue(transactionName);
                    System.Diagnostics.Debug.WriteLine(string.Format("SyncQueueImmediate {0}", SyncQueueImmediate.Count));

                }// Add To Db Queue
                recordsAffected = PersistTransactionInDBQueue(transaction.GetEnumDescription().Split('#')[0], transaction.GetEnumDescription().Split('#')[1], priority.ToString());
                System.Diagnostics.Debug.WriteLine(string.Format("PersistTransactionInDBQueue {0}", recordsAffected));
                SyncQueueChangedEventArgs args = new SyncQueueChangedEventArgs();
                args.State = QueueChangeType.Added;
                args.StateChangedTime = DateTime.Now;
                OnQueueStateChanged(args);
            }

            if ("everything".Contains(priority.GetEnumDescription()))
            {
                if (!SyncQueueEverything.Contains(transactionName))
                {
                    System.Diagnostics.Debug.WriteLine(string.Format("everything {0}", transactionName));
                    SyncQueueEverything.Enqueue(transactionName);
                    System.Diagnostics.Debug.WriteLine(string.Format("SyncQueueEverything {0}", SyncQueueEverything.Count));

                }  // Add To Db Queue
                recordsAffected = PersistTransactionInDBQueue(transaction.GetEnumDescription().Split('#')[0], transaction.GetEnumDescription().Split('#')[1], priority.ToString());
                System.Diagnostics.Debug.WriteLine(string.Format("PersistTransactionInDBQueue {0}", recordsAffected));
                SyncQueueChangedEventArgs args = new SyncQueueChangedEventArgs();
                args.State = QueueChangeType.Added;
                args.StateChangedTime = DateTime.Now;
                OnQueueStateChanged(args);
            }
            TotalPendingSync = SyncQueueEverything.Count + SyncQueueImmediate.Count;
            System.Diagnostics.Debug.WriteLine(string.Format("TotalPendingSync {0}", TotalPendingSync));
        }
        public enum Priority
        {
            immediate,
            everything
        }
        public int RemoveTransactionFromQueue(string transaction, Queue<string> SyncQueue)
        {
            int recordsAffected = -1;
            System.Diagnostics.Debug.WriteLine(string.Format("RemoveTransactionFromQueue SyncQueue Count {0}", SyncQueue.Count));
            if (SyncQueue.Count != 0)
            {
                SyncQueue.Dequeue();
                recordsAffected = RemoveTransactionFromDBQueue(transaction);
                System.Diagnostics.Debug.WriteLine(string.Format("RemoveTransactionFromQueue SyncQueue {0}", SyncQueue.Count));
                System.Diagnostics.Debug.WriteLine(string.Format("RemoveTransactionFromDBQueue recordsAffected {0}", recordsAffected));
            }
            TotalPendingSync = SyncQueueEverything.Count + SyncQueueImmediate.Count;
            System.Diagnostics.Debug.WriteLine(string.Format("TotalPendingSync {0}", TotalPendingSync));
            SyncQueueChangedEventArgs args = new SyncQueueChangedEventArgs();
            args.State = QueueChangeType.Removed;
            args.StateChangedTime = DateTime.Now;
            OnQueueStateChanged(args);
            return recordsAffected;
        }
        internal string GetTransactionFromQueue(Queue<string> SyncQueue)
        {
            string transaction = null;
            if (SyncQueue.Count != 0)
            {
                transaction = SyncQueue.Peek().ToString();
            }
            return transaction;
        }
        // This method checks if there are any pending transaction in the queue, that are needed to be synchronized.
        // If there are any pending transactions in the queue , it returns a boolean 'True' value
        internal bool IsTransactionInSyncQueue(Queue<string> SyncQueue)
        {
            if (SyncQueue.Count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // This method perists the state of the Sync Queue in the Database.
        int PersistTransactionInDBQueue(string transaction, string publicationProfile, string priority)
        {
            string Query = string.Format("insert into BUSDTA.SYNC_QUEUE (transaction, publicationprofile, priority) values ('{0}','{1}','{2}');", transaction, publicationProfile, priority);
            return Helpers.DbEngine.ExecuteNonQuery(Query);
        }
        int RemoveTransactionFromDBQueue(string transaction)
        {
            string Query = "delete from BUSDTA.SYNC_QUEUE where publicationProfile = '" + transaction + "'";
            return Helpers.DbEngine.ExecuteNonQuery(Query);
        }

    }

    public class SyncQueueChangedEventArgs : EventArgs
    {
        public QueueChangeType State { get; set; }
        public DateTime StateChangedTime { get; set; }
    }
    public enum QueueChangeType
    {
        Added,
        Removed
    }
}
