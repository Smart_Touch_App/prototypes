﻿using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class AppServiceManager
    {
        public bool UseAppService { get; set; }
        public event EventHandler<ServiceStateChangeEventArgs> ServiceStateChanged;
        public AppServiceManager()
        {
            //Changed By Arvind from 
            //UseAppService = Convert.ToBoolean(ConfigurationManager.AppSettings["userAppService"].ToString()) == true ? true : false;
            UseAppService =  false;
        }
        protected virtual void OnServiceStateChange(ServiceStateChangeEventArgs e)
        {
            EventHandler<ServiceStateChangeEventArgs> handler = ServiceStateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public bool IsServerReachable()
        {
            if (!UseAppService) {
                return true;
            }
            try
            {
                var Result = ResourceManager.ServiceClient.IsServerAvailable();
                return Result.IsServerReachable;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public void InitiateQueryForDataFromServer()
        {
            if (UseAppService)
            {
                QueryServerForNewData();
            }
        }
        async void QueryServerForNewData()
        {
            try
            {
                // ResourceManager.ActiveRemoteID is set on user login
                var Result = await ResourceManager.ServiceClient.NewDataAvailableAsync(ResourceManager.ActiveRemoteID);
                if (Result.Payload.Length > 0)
                {
                    NotifyUpdate(true, Result.Payload);
                }
            }
            catch (System.TimeoutException ex)
            {
                QueryServerForNewData();
                var message = ex.Message;
            }

        }
        void NotifyUpdate(bool isDataAvailable, string[] payload)
        {
            ServiceStateChangeEventArgs args = new ServiceStateChangeEventArgs();
            args.DataAvailable = isDataAvailable;
            args.Payload = payload;
            args.StateChangedTime = DateTime.Now;
            OnServiceStateChange(args);
        }
    }
    public class ServiceStateChangeEventArgs : EventArgs
    {
        public bool IsServerReachable { get; set; }
        public bool DataAvailable { get; set; }
        public string[] Payload { get; set; }
        public DateTime StateChangedTime { get; set; }
    }
}
