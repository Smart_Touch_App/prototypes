﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
namespace SalesLogicExpress.Application.Managers
{
    public enum Transaction
    {
        [Description("Users#prof_validate_user")]
        GetLatestUsers,
        [Description("Orders Created#prof_immediate")]
        SaveOrder,
        [Description("Orders Accepted#prof_immediate")]
        AcceptOrder,
        [Description("Orders on Hold#prof_immediate")]
        HoldOrder,
        [Description("Template Saved#prof_everything")]
        SaveTemplate,
        [Description("Order Delivered#prof_immediate")]
        DeliverOrder,
        [Description("Order Void#prof_immediate")]
        VoidOrder,
        [Description("Get Route Data#prof_everything")]
        GetEverything
    }
    public class TransactionManager
    {
        SyncQueueManager queue;
        public TransactionManager()
        {
            queue = new Application.Managers.SyncQueueManager();
            Application.Managers.SyncManager syncManager = ResourceManager.Synchronization;
            syncManager.InitializePendingQueue(queue);
            syncManager.SyncProgressChanged += syncManager_SyncProgressChanged;
        }

        void syncManager_SyncProgressChanged(object sender, SyncUpdatedEventArgs e)
        {
            if (e.State == SyncUpdateType.SyncComplete)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.ToString("M'/'dd'/'yyyy' 'hh:mm tt"));
            }
            if (e.State == SyncUpdateType.UploadComplete)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.ToString("M'/'dd'/'yyyy' 'hh:mm tt"));
            }
            if (e.State == SyncUpdateType.DownloadComplete)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.ToString("M'/'dd'/'yyyy' 'hh:mm tt"));
            }
        }
        public void AddTransactionInQueueForSync(Transaction transaction, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority priority)
        {
            queue.AddTransactionToQueue(transaction, priority);
        }
        public TransactionManager(SyncQueueManager queueManager)
        {
            queue = queueManager;
        }

    }
}
