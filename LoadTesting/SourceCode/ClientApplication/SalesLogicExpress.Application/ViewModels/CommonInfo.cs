﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CommonNavInfo : AppViewModelBase
    {
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;
        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }
        #region Actions
        public DelegateCommand MoveToView { get; set; }
        public DelegateCommand CancelSync { get; set; }

        static bool _IsBackwardNavigationEnabled = true;
        public static bool IsBackwardNavigationEnabled
        {
            get { return _IsBackwardNavigationEnabled; }
            set
            {
                _IsBackwardNavigationEnabled = value;
                NotifyStaticPropertyChanged("IsBackwardNavigationEnabled");
            }
        }

        #endregion

        #region Properties
        public static Guid MessageToken { get; set; }
        public static string UserRoute
        {
            get
            {
                return UserManager.UserRoute;
            }
            set
            {
                UserManager.UserRoute = value;
            }
        }
        public static string UserBranch
        {
            get
            {
                return UserManager.UserBranch;
            }
        }
        public static string UserName
        {
            get
            {
                return UserManager.UserName.ToUpper();
            }
            set
            {
                UserManager.UserName = value;
            }
        }
        private static string _ViewTitle;
        public static string ViewTitle
        {
            get { return _ViewTitle; }
            set
            {
                _ViewTitle = value;
                NotifyStaticPropertyChanged("ViewTitle");
            }
        }
        private static bool _IsNetworkAvailable;
        public static bool IsNetworkAvailable
        {
            get { return _IsNetworkAvailable; }
            set
            {
                _IsNetworkAvailable = value;
                NotifyStaticPropertyChanged("IsNetworkAvailable");
            }
        }
        private static bool _IsSyncGoing;
        public static bool IsSyncGoing
        {
            get { return _IsSyncGoing; }
            set { _IsSyncGoing = value; NotifyStaticPropertyChanged("IsSyncGoing"); }
        }
        public static ViewModelMappings.View CurrentView { get; set; }
        public static ViewModelMappings.View PreviousView { get; set; }
        public static void SetPreviousViewTitle(string title)
        {
            previousViewTitle = title;
        }
        static string previousViewTitle;
        public static string PreviousViewTitle
        {
            get { return previousViewTitle; }
            set
            {
                previousViewTitle = value;
                NotifyStaticPropertyChanged("PreviousViewTitle");
            }
        }
        public static string LastSynched { get; set; }
        public static bool ShowBackNavigation { get; set; }
        public static SalesLogicExpress.Domain.Customer Customer { get; set; }
        private bool _IsCollectionEmpty;
        public bool IsCollectionEmpty
        {
            get
            {
                return _IsCollectionEmpty;
            }
            set
            {
                _IsCollectionEmpty = value;
                OnPropertyChanged("IsCollectionEmpty");
            }
        }
        bool _networkConnected = false;
        public bool NetWorkConnected
        {
            get { return _networkConnected; }
            set
            {
                _networkConnected = value;
                OnPropertyChanged("NetWorkConnected");
            }
        }
        bool _syncInProgress = false;
        public bool SyncInProgress
        {
            get { return _syncInProgress; }
            set
            {
                _syncInProgress = value;
                OnPropertyChanged("SyncInProgress");
            }
        }
        string _NullSyncedMessage;
        public string NullSyncedMessage
        {
            get
            {
                return _NullSyncedMessage;
            }
            set
            {
                _NullSyncedMessage = value;
                OnPropertyChanged("NullSyncedMessage");
            }
        }

        #endregion

        #region Constructor
        public CommonNavInfo()
        {
            InitializeCommands();
        }
        ~CommonNavInfo()
        {
            ResourceManager.NetworkInfo.InternetStateChanged -= NetworkInfo_InternetStateChanged;
            ResourceManager.Synchronization.SyncProgressChanged -= Synchronization_SyncProgressChanged;
            ResourceManager.Synchronization.queueManager.QueueChanged -= queueManager_QueueChanged;
        }
        void InitializeCommands()
        {
            ResourceManager.NetworkInfo.InternetStateChanged += NetworkInfo_InternetStateChanged;
            TransactionManager transaction = ResourceManager.Transaction;
            ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;
            ResourceManager.Synchronization.queueManager.QueueChanged += queueManager_QueueChanged;
            GetPendingSyncItems();
            IsNetworkAvailable = ResourceManager.NetworkInfo.IsInterNetConnected();
            MoveToView = new DelegateCommand((param) =>
            {
                if (IsBackwardNavigationEnabled)
                {
                    var moveToView = new Helpers.NavigateToView { NextViewName = PreviousView, CurrentViewName = CurrentView, CloseCurrentView = true };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
                else
                {
                    // ToDo Remove this condition in future when Pick order from VOID state, back navigation discussed
                    if (CurrentView == ViewModelMappings.View.PickOrder)
                    {
                        return;
                    }
                    IsBackwardNavigationEnabled = true;
                }
            });
            CancelSync = new DelegateCommand((param) =>
            {
                ResourceManager.Synchronization.CancelSync();
            });
        }

        void queueManager_QueueChanged(object sender, SyncQueueChangedEventArgs e)
        {
            GetPendingSyncItems();
        }
        void GetPendingSyncItems()
        {
            try
            {
                ObservableCollection<SalesLogicExpress.Application.Managers.SyncQueueManager.SyncQueueItem> items = ResourceManager.Synchronization.queueManager.GetPendingSyncItems();
                if (items.Count != 0)
                {
                    if (System.Windows.Application.Current != null)
                        System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                       {
                           StateCollection.Clear();
                           foreach (SalesLogicExpress.Application.Managers.SyncQueueManager.SyncQueueItem item in items)
                           {
                               StateCollection.Add(item);
                           }
                           IsCollectionEmpty = false;
                       });
                }
                else
                {
                    IsCollectionEmpty = true;
                    NullSyncedMessage = "No items to be synced.";
                }
            }
            catch (Exception ex)
            {

            }

        }
        void Synchronization_SyncProgressChanged(object sender, SyncUpdatedEventArgs e)
        {
            try
            {
                if (System.Windows.Application.Current != null)
                    System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                          {
                              IsSyncGoing = (e.State == SyncUpdateType.Idle) ? false : true;
                          });
            }
            catch (Exception ex)
            {

            }
        }

        void NetworkInfo_InternetStateChanged(object sender, InternetStateChangedEventArgs e)
        {
            IsNetworkAvailable = (e.State == InternetState.Connected) ? true : false;
            GetPendingSyncItems();
        }
        public static void SetSelectedCustomer(SalesLogicExpress.Domain.Customer customer)
        {
            Customer = customer;
        }
        public static object GetSelectedCustomer()
        {
            return Customer;
        }
        public static void SetLastSynched(string lastSynched)
        {
            LastSynched = lastSynched;
        }

        #endregion

        private ObservableCollection<SalesLogicExpress.Application.Managers.SyncQueueManager.SyncQueueItem> _StateCollection = new ObservableCollection<SalesLogicExpress.Application.Managers.SyncQueueManager.SyncQueueItem>();
        public ObservableCollection<SalesLogicExpress.Application.Managers.SyncQueueManager.SyncQueueItem> StateCollection
        {
            get
            {
                return _StateCollection;
            }
            set { _StateCollection = value; OnPropertyChanged("StateCollection"); }
        }
    }
}
