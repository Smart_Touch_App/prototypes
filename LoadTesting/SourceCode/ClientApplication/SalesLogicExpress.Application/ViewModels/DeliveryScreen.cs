﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Linq;
using System.Data;
using System.Text;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Windows.Ink;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Markup;
using System.Windows.Controls;
using log4net;
using SalesLogicExpress.Domain;



namespace SalesLogicExpress.Application.ViewModels
{
    public class DeliveryScreen : ViewModelBase
    {
        public DeliveryScreen(object payload)
        {
            // TODO: Complete member initialization
            this.payload = payload;
            OrderId = Order.OrderId;
            ObservableCollection<Models.OrderItem> DeliveryScreenCollection = (ObservableCollection<Models.OrderItem>)payload;
            DeliveryScreenItems = DeliveryScreenCollection;
            _SignStroke = new StrokeCollection();
            GetAllTotals();
            InitializeCommands();

            IsCanvasEnable = true;

            if (PreviewOrder.GlobalInkCanvas != null)
            {
                if (PreviewOrder.GlobalInkCanvas.Strokes.Count > 0)
                {
                    SignStroke.Add(PreviewOrder.GlobalInkCanvas.Strokes);
                    IsCanvasEnable = false;
                }
            }
        }
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.OrderManager");



        public DeliveryScreen()
        {
            InitializeCommands();
        }

        // Public Var's

        #region Command Declarations
        public DelegateCommand PickOrder { get; set; }
        public DelegateCommand SaveSign { get; set; }
        public DelegateCommand ClearCanvas { get; set; }
        public DelegateCommand GetReasonCode { get; set; }
        public DelegateCommand OrderDelivered { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand HoldOrder { get; set; }

        #endregion
        string strDateTime = DateTime.Now.ToString("M'/'dd'/'yyyy' - 'hh:mm ") + DateTime.Now.ToString("tt").ToLower();
        private object payload;
        private StrokeCollection _SignStroke = null;
        private int _OrderId;
        private ObservableCollection<Models.OrderItem> _DeliveryScreen = new ObservableCollection<Models.OrderItem>();
        private bool _IsCanvasEnable;
        # region Properties
        public ReasonCode ReasonCodeList = new ReasonCode(string.Empty);
        public int OrderId
        {
            get { return Order.OrderId; }
            set { _OrderId = value; }
        }

        public bool IsCanvasEnable
        {
            get { return _IsCanvasEnable; }
            set
            {
                _IsCanvasEnable = value;
                OnPropertyChanged("IsCanvasEnable");

            }
        }

        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");

            }
        }

        public Guid MessageToken { get; set; }
        public string CurrentDate
        {
            get { return strDateTime; }
        }

        private bool _ToggleSignPanelVisibility = false;
        public bool ToggleSignPanelVisibility
        {
            get
            {
                return _ToggleSignPanelVisibility;

            }
            set
            {
                _ToggleSignPanelVisibility = value;
                OnPropertyChanged("ToggleSignPanelVisibility");
            }
        }

        // OrderItem Collection for DeliveryScreen Grid
        // public ObservableCollection<Models.TemplateItem> PreviewItems
        public ObservableCollection<Models.OrderItem> DeliveryScreenItems
        {
            get
            {

                ///  this.previewitems = new Managers.TemplateManager().GetTemplateItemsForCustomer("1108911");
                return _DeliveryScreen;
            }
            set
            {
                _DeliveryScreen = value;
                OnPropertyChanged("PreviewItems");
            }
        }

        #endregion

        # region Properties For Textbox Values

        float varTotalCoffeeValue, varTotalAlliedValue, varOrderTotalValue, varEnergySurchargeValue, varSalesTaxValue, varInvoiceTotalValue;
        private string _TotalCoffeeValue = "0.00", _TotalAlliedValue = "0.00",
                       _OrderTotalValue = "0.00", _EnergySurchargeValue = "0.00",
                       _SalesTaxValue = "0.00", _InvoiceTotalValue = "0.00";
        public string TotalCoffeeValue
        {
            get
            {
                return "$" + _TotalCoffeeValue;
            }
            set
            {
                _TotalCoffeeValue = value;
                OnPropertyChanged("TotalCoffeeValue");
            }
        }

        public string TotalAlliedValue
        {
            get { return "$" + _TotalAlliedValue; }
            set { _TotalAlliedValue = value; OnPropertyChanged("TotalAlliedValue"); }
        }

        public string OrderTotalValue
        {
            get { return "$" + _OrderTotalValue; }
            set
            {
                _OrderTotalValue = value;
                OnPropertyChanged("OrderTotalValue");
            }
        }

        public string EnergySurchargeValue
        {
            get { return "$" + _EnergySurchargeValue; }
            set
            {
                _EnergySurchargeValue = value;
                OnPropertyChanged("EnergySurchargeValue");
            }
        }
        public string SalesTaxValue
        {
            get { return "$" + _SalesTaxValue; }
            set { _SalesTaxValue = value; OnPropertyChanged("SalesTaxValue"); }
        }
        public string InvoiceTotalValue
        {
            get { return "$" + _InvoiceTotalValue; }
            set { _InvoiceTotalValue = value; OnPropertyChanged("InvoiceTotalValue"); }
        }

        #endregion


        #region methods
        // Synchronize to calculate values on right panel
        private void GetAllTotals()
        {
            try
            {
                DataTable dset = new DataTable();

                dset = DbEngine.ExecuteDataSet("select * from BUSDTA.Order_Header where  Order_ID =" + "'" + OrderId + "'").Tables[0];
                foreach (DataRow dr in dset.Rows)
                {
                    varEnergySurchargeValue = (float)(dr["Energy_Surcharge"]);
                    EnergySurchargeValue = varEnergySurchargeValue.ToString("0.00");
                    varInvoiceTotalValue = (float)(dr["Invoice_Total"]);
                    InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");
                    varOrderTotalValue = (float)(dr["Order_Total_Amt"]);
                    OrderTotalValue = varOrderTotalValue.ToString("0.00");
                    varSalesTaxValue = (float)(dr["Sales_Tax_Amt"]);
                    SalesTaxValue = varSalesTaxValue.ToString("0.00");
                    varTotalAlliedValue = (float)(dr["Total_Allied"]);
                    TotalAlliedValue = varTotalAlliedValue.ToString("0.00");
                    varTotalCoffeeValue = (float)(dr["Total_Coffee"]);
                    TotalCoffeeValue = varTotalCoffeeValue.ToString("0.00");

                }
            }
            catch (Exception)
            {
                log.Error("Error occured in GetAllTotals(): Method");
            }
        }
        #endregion

        private void InitializeCommands()
        {
            VoidOrder = new DelegateCommand((param) =>
          {
              if (ReasonCodeList != null)
                  Order.VoidOrderReasonId = ReasonCodeList.Id;
              // Set reason for void Order if not available 
              if (Order.VoidOrderReasonId == 0)
              {
                  ReasonCodeList.ParentViewModel = this;
                  ReasonCodeList.MessageToken = MessageToken;
                  var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = ReasonCodeList };
                  Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                  if (OpenDialog.Cancelled)
                  {
                      OrderStateChangeArgs arg = new OrderStateChangeArgs();
                      arg.State = OrderState.None;
                      ReasonCodeList.OnStateChanged(arg);
                  }
                  return;
              }
              else
              {
                  // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0

                  Order.VoidOrderReasonId = 0;
              }
          });

            HoldOrder = new DelegateCommand((e) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.DeliveryAcceptance, Managers.OrderManager.OrderSubState.Hold);
                ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.HoldOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = true, ShowAsModal = false, Payload = CommonNavInfo.Customer };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            OrderDelivered = new DelegateCommand((param) =>
            {
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.DeliveryConfirmation, Managers.OrderManager.OrderSubState.None);
                if (ViewModels.PreviewOrder.GlobalInkCanvas != null)
                    PreviewOrder.GlobalInkCanvas.Strokes.Clear();
            });

            PickOrder = new DelegateCommand((param) =>
             {
                 var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = false, Payload = DeliveryScreenItems, ShowAsModal = false };
                 Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
             });

            SaveSign = new DelegateCommand((param) =>
            {

                StrokeCollection inkCanvasStrokes = param as StrokeCollection;

                if (inkCanvasStrokes.Count == 0)
                {
                    var alertWindow = new Helpers.AlertWindow { Message = "Please sign" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertWindow, MessageToken);
                    return;

                }

                RenderTargetBitmap rtb = new RenderTargetBitmap((int)canvasWidth - 10, (int)canvasHeight, 0, 0, PixelFormats.Pbgra32);
                try
                {

                    rtb.Render(InkCanvas);


                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();


                    encoder.Frames.Add(BitmapFrame.Create(rtb));

                    FileStream file = new FileStream("test.jpg", FileMode.Create);

                    encoder.Save(file);
                    file.Close();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                    Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.OrderAcceptance, Managers.OrderManager.OrderSubState.None);

                    ResourceManager.Transaction.AddTransactionInQueueForSync(SalesLogicExpress.Application.Managers.Transaction.AcceptOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);


                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = false, Payload = DeliveryScreenItems, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);


                }
                catch (Exception ex)
                {
                }
            });
            ClearCanvas = new DelegateCommand((param) =>
            {
                SignStroke.Clear();
                IsCanvasEnable = true;
            });

        }


        public double canvasWidth { get; set; }

        public double canvasHeight { get; set; }

        public InkCanvas InkCanvas { get; set; }

    }
}
