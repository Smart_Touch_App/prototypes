﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using log4net;
using System.Windows.Ink;

namespace SalesLogicExpress.Application.ViewModels
{
    public class PickOrder : ViewModelBase, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PickOrder");

        #region DelegateCommand
        public DelegateCommand ScanByDevice { get; set; }
        public DelegateCommand HoldPickList { get; set; }
        public DelegateCommand VerifyPickOrder { get; set; }
        public DelegateCommand ManualPick { get; set; }
        public DelegateCommand GetManualPickReasonList { get; set; }
        public DelegateCommand RemoveExceptions { get; set; }
        public DelegateCommand DeliverToCustomer { get; set; }
        public DelegateCommand SwitchScannerMode { get; set; }
        public DelegateCommand PickByKeyboard { get; set; }
        public DelegateCommand SetManualPickReason { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand SetPickItemLabels { get; set; }
        public DelegateCommand ChargeOnAccount { get; set; }
        public DelegateCommand AcceptChargeOnAccount { get; set; }
        public DelegateCommand DeclineChargeOnAccount { get; set; }
        public DelegateCommand ClearSignInChargeOnAccount { get; set; }
        public ReasonCode selectedReason;


        #endregion

        #region Contructors



        public PickOrder()
        {

        }
        public PickOrder(object orderItems)
        {
            try
            {


                pickManager = new Managers.PickManager();
                selectedReason = new ReasonCode("");
                PickOrderItems.CollectionChanged += PickOrderItems_CollectionChanged;
                PickOrderItems.ItemPropertyChanged += PickOrderItems_ItemPropertyChanged;

                ObservableCollection<Models.OrderItem> orderItemsCollectionFromOrder = orderItems as ObservableCollection<Models.OrderItem>;
                ObservableCollection<Models.PickOrderItem> PickorderItemsCollectionFromDB = pickManager.GetPickItemsFromDB(Order.OrderId.ToString());
                ObservableCollection<Models.PickOrderItem> PickorderItemsCollectionFromOrder = new ObservableCollection<PickOrderItem>();

                foreach (Models.OrderItem orderItem in orderItemsCollectionFromOrder)
                {
                    orderItem.UMConversionFactor = new Managers.PricingManager().jdeUOMConversion(orderItem.UM, orderItem.PrimaryUM, Convert.ToInt32(orderItem.ShortDesc));
                    PickOrderItem pickOrderItem = new Models.PickOrderItem(orderItem);
                    PickorderItemsCollectionFromOrder.Add(pickOrderItem);
                }


                bool IsPickListExists = pickManager.IsPickListCreated(Order.OrderId.ToString());
                string PickStatus = pickManager.GetPickListStatus(Order.OrderId.ToString());

                if (IsPickListExists && PickStatus != "Hold")
                {

                    foreach (Models.PickOrderItem item in PickorderItemsCollectionFromOrder)
                    {
                        // New item added from Order
                        if (!PickorderItemsCollectionFromDB.Any(i => i.ItemNumber == item.ItemNumber))
                        {
                            pickManager.InsertPickItem(Order.OrderId.ToString(), item);
                        }
                    }
                    foreach (Models.PickOrderItem item in PickorderItemsCollectionFromDB)
                    {
                        // Existing item deleted from Order
                        if (!PickorderItemsCollectionFromOrder.Any(i => i.ItemNumber == item.ItemNumber))
                        {
                            pickManager.DeletePickedItem(Order.OrderId.ToString(), item.ItemNumber);


                            // Add to exception list in scanner ON mode
                            item.ExceptionReason = "Incorrect Item";
                            item.ExceptionQtyInPrimaryUOM = item.OrderQtyInPrimaryUOM;
                            pickManager.SaveException(Order.OrderId.ToString(), item);

                        }

                        // Change in Order Qty or Order UM
                        if (PickorderItemsCollectionFromOrder.Any(i => i.ItemNumber == item.ItemNumber))
                        {
                            pickManager.UpdatePickedItem(Order.OrderId.ToString(), PickorderItemsCollectionFromOrder.FirstOrDefault(i => i.ItemNumber == item.ItemNumber));

                            PickorderItemsCollectionFromOrder.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                            UpdateExceptions(PickorderItemsCollectionFromOrder.FirstOrDefault(i => i.ItemNumber == item.ItemNumber), IsValidItem(PickorderItemsCollectionFromOrder.FirstOrDefault(i => i.ItemNumber == item.ItemNumber)));
                        }

                    }



                }

                if (IsPickListExists && PickStatus == "Hold")
                {
                    //Just get the data from db

                }

                if (!IsPickListExists)
                {
                    pickManager.InsertPickList(Order.OrderId.ToString(), PickorderItemsCollectionFromOrder);
                }



                // get the data from db
                PickorderItemsCollectionFromDB = pickManager.GetPickItemsFromDB(Order.OrderId.ToString());

                ExceptionItems = pickManager.GetExceptionList(Order.OrderId.ToString());

                foreach (PickOrderItem pickOrderItem in PickorderItemsCollectionFromDB)
                {
                    PickOrderItems.Add(pickOrderItem);
                }

                // Pass existing Order Sub state Is it VOID or HOLD 
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.PickOrder, Order.OrderSubStatus);

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = Order.OrderSubStatus == Managers.OrderManager.OrderSubState.Void ? false : true;

                ManualPickReasonList = pickManager.GetReasonListForManualPick();

                InitializeCommands();
            }
            catch (Exception e)
            {
                Logger.Error("SalesLogicExpress.ViewModels.PickOrder, PickOrder()  Message: " + e.StackTrace);
            }
        }

        #endregion

        #region Methods
        private void InitializeCommands()
        {
            AcceptChargeOnAccount = new DelegateCommand((param) =>
            {
                if (SignStroke.Count == 0)
                {
                    var Alert = new AlertWindow { Message = "Please Sign" };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }
                if (ChargeOnAccountContext.ApprovalCode != "12341234" && ChargeOnAccountContext.CreditLimit < ChargeOnAccountContext.TotalBalanceAmount)
                {
                    var Alert = new AlertWindow { Message = "Please Enter Valid Approval Code" };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }

                DeliverToCustomerUpdateAndNavigate();
            });
            DeclineChargeOnAccount = new DelegateCommand((param) =>
           {

           });
            ClearSignInChargeOnAccount = new DelegateCommand((param) =>
           {
               SignStroke.Clear();
           });


            VoidOrder = new DelegateCommand((param) =>
        {
            if (ReasonCodeList != null)
                Order.VoidOrderReasonId = ReasonCodeList.Id;
            // Set reason for void Order if not available 
            if (Order.VoidOrderReasonId == 0)
            {
                ReasonCodeList.ParentViewModel = this;
                ReasonCodeList.MessageToken = MessageToken;
                var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = ReasonCodeList };
                Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                if (OpenDialog.Cancelled)
                {
                    OrderStateChangeArgs arg = new OrderStateChangeArgs();
                    arg.State = OrderState.None;
                    ReasonCodeList.OnStateChanged(arg);
                }
                return;
            }
            else
            {
                // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0

                Order.VoidOrderReasonId = 0;
            }
        });

            ChargeOnAccount = new DelegateCommand((param) =>
            {
                Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                ChargeOnAccountContext = collectionManager.GetChargeOnAccountInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString(), CashDeliveryContext.TotalBalanceAmount);
                COAReasonList = new Managers.OrderManager().GetReasonListFor("Charge on Account");
                _SignStroke = new StrokeCollection();

                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { },MessageToken);

                DialogWindow ChargeOnAccountDialog = new DialogWindow();
                ChargeOnAccountDialog.Title = "Charge on Account";
                ChargeOnAccountDialog.TemplateKey = "ChargeOnAccount";
                Messenger.Default.Send<DialogWindow>(ChargeOnAccountDialog, MessageToken);
            });

            SetPickItemLabels = new DelegateCommand((param) =>
            {
                Models.PickOrderItem orderItem = param as Models.PickOrderItem;
                PickedItemNo = orderItem.ItemNumber.Trim();
                PickedItemUM = orderItem.UM;
                ManualEnteredQty = orderItem.OrderQty;
            });
            ScanByDevice = new DelegateCommand((param) =>
                {
                    ScanOperation(param);
                    LastScanItem = param as PickOrderItem;
                });
            ManualPick = new DelegateCommand((param) =>
            {
                if (param == null)
                {
                    string strMessage;
                    if (IsAddingToPalette)
                    {
                        strMessage = Constants.PickOrder.SelectItemToPickAlert;
                    }
                    else
                    {
                        strMessage = Constants.PickOrder.SelectItemToUnPickAlert;
                    }
                    var Alert = new AlertWindow { Message = strMessage };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }
                Models.PickOrderItem item = param as Models.PickOrderItem;
                if (item.LastManualPickReasonCode != 0)
                {
                    ScanOperation(param);
                }
                else
                {
                    if (IsScannerOn && LastScanItem != item)
                    {
                        var Alert = new AlertWindow { Message = Constants.PickOrder.SelectReasonForManualPickAlert };
                        Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                        return;
                    }
                    else
                    {
                        ScanOperation(param);
                    }
                }

            });
            GetManualPickReasonList = new DelegateCommand((param) =>
            {
                string strMessage;
                if (IsAddingToPalette)
                {
                    param = SelectedPickItem;
                    strMessage = Constants.PickOrder.SelectItemToPickAlert;
                }
                else
                {
                    param = SelectedExceptionItem;
                    strMessage = Constants.PickOrder.SelectItemToUnPickAlert;
                }


                if (param == null)
                {
                    var Alert = new AlertWindow { Message = strMessage };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }

                var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });

            SetManualPickReason = new DelegateCommand((param) =>
            {
                selectedReason = param as ReasonCode;

                if (selectedReason == null)
                {
                    ErrorText = "Please Choose Reason Code";
                }

                if (IsAddingToPalette)
                {
                    SelectedPickItem.LastManualPickReasonCode = selectedReason.Id;
                    //Update in DB
                    pickManager.UpdateManualPickReasonAndCountForPick(SelectedPickItem);
                }
                else
                {
                    SelectedExceptionItem.LastManualPickReasonCode = selectedReason.Id;
                    //Update in DB
                    pickManager.UpdateManualPickReasonAndCountForException(SelectedExceptionItem);
                }

            });

            HoldPickList = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                if (confirmMessage.Confirmed)
                {
                    OrderStateChangeArgs arg = new OrderStateChangeArgs();
                    arg.State = OrderState.Hold;
                    selectedReason.OnStateChanged(arg);
                    System.Threading.Thread.Sleep(50);
                    Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                }
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.PickOrder, Managers.OrderManager.OrderSubState.Hold);
                pickManager.HoldPickList(Order.OrderId.ToString());
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, ShowAsModal = false, Payload = CommonNavInfo.Customer };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            RemoveExceptions = new DelegateCommand((s) =>
            {

                var confirmMessage = new Helpers.ConfirmWindow { Message = Constants.PickOrder.RemoveExceptionConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                IsAddingToPalette = false;
            });

            DeliverToCustomer = new DelegateCommand(param =>
            {
                if (param != null)
                {
                    bool? IsCashDelivery = param as bool?;
                    if (IsCashDelivery.Value)
                    {
                        if (!CashDeliveryContext.IsCashDeliveryAllowed)
                        {
                            Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Total Balance Amount should match with Payment Amount." }, MessageToken);
                            return;
                        }
                        else
                        {
                            Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                            collectionManager.SaveCashDeliveryInfo(CashDeliveryContext, Order.OrderId.ToString(), CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.Company);
                            OrderStateChangeArgs arg = new OrderStateChangeArgs();
                            arg.State = OrderState.Hold;
                            selectedReason.OnStateChanged(arg);
                            System.Threading.Thread.Sleep(50);
                            DeliverToCustomerUpdateAndNavigate();
                            return;
                        }
                    }
                }
                if (!CommonNavInfo.Customer.IsCustomerOnAccount)
                {
                    Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                    CashDeliveryContext = collectionManager.GetCashDeliveryInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString());
                    DialogWindow CashCollectionDialog = new DialogWindow();
                    CashCollectionDialog.Title = "Cash Delivery";
                    CashCollectionDialog.TemplateKey = "CashDelivery";
                    Messenger.Default.Send<DialogWindow>(CashCollectionDialog, MessageToken);
                }
                else
                {
                    DeliverToCustomerUpdateAndNavigate();
                }
            });

            SwitchScannerMode = new DelegateCommand((param) =>
            {
                bool InvertMode = !IsScannerOn;
                IsScannerOn = InvertMode;
            });

        }

        private void DeliverToCustomerUpdateAndNavigate()
        {
            Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.OrderDelivered, Managers.OrderManager.OrderSubState.None);
            //Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "Order has been picked successfully" }, MessageToken);
            ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.DeliverOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);

            ObservableCollection<OrderItem> ItemsToDeliver = new ObservableCollection<OrderItem>();

            foreach (PickOrderItem pickItem in PickOrderItems)
            {
                ItemsToDeliver.Add(pickItem);
            }
            Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);

            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = true, ShowAsModal = false, CurrentViewName = ViewModelMappings.View.PickOrder, Payload = ItemsToDeliver };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        }


        private void PickOrderItems_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PickOrderItem pickedItem = sender as PickOrderItem;

            if (e.PropertyName == "PickedQuantityInPrimaryUOM")
            {
                //Updt qty in DB
                pickManager.UpdatePickQty(Order.OrderId.ToString(), pickedItem.ItemNumber, pickedItem.PickedQuantityInPrimaryUOM.ToString());

                OnPropertyChanged("IsPickCompleted");
                //ScanOperation(pickedItem);
            }


        }



        private void PickOrderItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }




        // check for item validity and return Item Status
        public string IsValidItem(Models.PickOrderItem item)
        {
            string result = "";

            if (item.OrderQtyInPrimaryUOM < item.PickedQuantityInPrimaryUOM)
            {
                result = "Over Picked";

            }
            else if (PickOrderItems.Count != 0 && !PickOrderItems.Any(i => i.ItemNumber == item.ItemNumber))
            {
                result = "Incorrect Item";
            }
            else
            {
                result = "Valid";
            }


            //result = "Invalid Item Code";


            return result;
        }


        //update picked quantity in memory collection
        public void AddingToPalette(PickOrderItem item)
        {
            item.PickedQuantityInPrimaryUOM = Convert.ToInt32(item.PickedQuantityInPrimaryUOM + item.UMConversionFactor);
        }

        //update picked quantity in memory collection
        public void RemovingFromPalette(PickOrderItem item)
        {
            if (item.PickedQuantityInPrimaryUOM > 0)
            {
                if (item.OrderQtyInPrimaryUOM == item.PickedQuantityInPrimaryUOM)
                {
                    SpeechSynthesizer speechSynth = new SpeechSynthesizer();
                    speechSynth.Speak("Cannot unpick ordered item");
                    return;
                }
                item.PickedQuantityInPrimaryUOM = Convert.ToInt32(item.PickedQuantityInPrimaryUOM - item.UMConversionFactor);
            }
            else
            {

                //var Alert = new AlertWindow { Message = "Picked item doesn't exist in palette" };
                //Messenger.Default.Send<AlertWindow>(Alert, MessageToken);

            }
            if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber))
            {
                PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = Convert.ToInt32(PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM - item.UMConversionFactor);
            }
        }

        // picking/unpicking qty from keypad
        public void ManuallyPicking(PickOrderItem item)
        {

            int intPickedQuantityInPrimaryUOM = Convert.ToInt32(ManualEnteredQty * item.UMConversionFactor);

            if (intPickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
            {
                var Alert = new AlertWindow { Message = "Picked item qty exceeds order qty" };
                Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                return;
            }

            item.PickedQuantityInPrimaryUOM = intPickedQuantityInPrimaryUOM;

            if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber))
            {
                PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = intPickedQuantityInPrimaryUOM;
            }

            ManualEnteredQty = 0;
            IsManualPick = false;
            item.LastManualPickReasonCode = 0;
        }

        // save exception in Insert/update
        public void SaveException(string OrderId, PickOrderItem item)
        {
            pickManager.SaveException(OrderId, item);
        }

        // update exception list and made changes in DB data for transaction
        public void UpdateExceptions(PickOrderItem item, string ItemValidity)
        {
            if (ItemValidity == "Over Picked")
            {
                if (ExceptionItems.Any(i => i.ItemNumber == item.ItemNumber))
                {
                    ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                    ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - item.OrderQtyInPrimaryUOM;
                }
                else
                {
                    ExceptionItems.Add(new Models.PickOrderItem()
                    {
                        OrderQtyInPrimaryUOM = item.OrderQtyInPrimaryUOM,
                        PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM,
                        ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - item.OrderQtyInPrimaryUOM,
                        ExceptionQtyInOrderUOM = Convert.ToInt32(item.ExceptionQtyInPrimaryUOM / item.UMConversionFactor),
                        PrimaryUM = item.PrimaryUM,
                        ItemNumber = item.ItemNumber,
                        ItemDescription = item.ItemDescription,
                        ItemStatus = item.ItemStatus,
                        ExceptionReason = ItemValidity,
                        UMConversionFactor = item.UMConversionFactor,
                        UM = item.UM
                    });
                }
                pickManager.SaveException(Order.OrderId.ToString(), ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber));
            }
            else if (ItemValidity == "Valid" || ItemValidity == "Incorrect Item")
            {
                if (ExceptionItems.Any(i => i.ItemNumber == item.ItemNumber))
                {
                    foreach (var ExceptionItem in ExceptionItems)
                    {
                        if (ExceptionItem.ItemNumber == item.ItemNumber)
                        {
                            ExceptionItems.Remove(ExceptionItem);
                            break;
                        }
                    }
                }
                pickManager.DeleteException(Order.OrderId.ToString(), item);
            }
        }

        // comman scan operation function for every pick/ unpick from scanner or manual
        private void ScanOperation(object param)
        {
            Models.PickOrderItem item = param as Models.PickOrderItem;

            //Adding mode from scanner (Pick list)
            if (IsAddingToPalette && IsScannerOn && !IsManualPick)
            {
                AddingToPalette(item);
            }

            //Removing mode from scanner (Exception list)
            if (!IsAddingToPalette && IsScannerOn && !IsManualPick)
            {
                RemovingFromPalette(item);
            }

            //Picking using keypad
            if (IsManualPick)
            {
                ManuallyPicking(item);
            }

            UpdateExceptions(item, IsValidItem(item));

            //Updt qty in DB
            pickManager.UpdatePickQty(Order.OrderId.ToString(), item.ItemNumber, item.PickedQuantityInPrimaryUOM.ToString());

            if (IsScannerOn)
            {
                // Sort Pick List
                PickOrderItems = new TrulyObservableCollection<PickOrderItem>(PickOrderItems.OrderByDescending(x => x.ItemStatus));
            }



            // Change Scanner mode to adding, If there are no any exception
            if (ExceptionItems.Count == 0)
            {
                IsAddingToPalette = true;
            }

            // Update All relevant properties
            OnPropertyChanged("PickOrderItems");
            OnPropertyChanged("TotalOrderQtyCount");
            OnPropertyChanged("DoneItemsQtyCount");
            OnPropertyChanged("UnderPickItemsQtyCount");
            OnPropertyChanged("OverPickItemsQtyCount");
            OnPropertyChanged("IsPickCompleted");
            OnPropertyChanged("IsAddingToPalette");
            OnPropertyChanged("CanRemoveExceptions");


            //Set selected Item
            if (IsAddingToPalette)
            {
                SelectedPickItem = item;

            }
            else
            {
                SelectedExceptionItem = item;

            }
            OnPropertyChanged("SelectedPickItem");
            OnPropertyChanged("SelectedExceptionItem");



            if (Order.OrderSubStatus == Managers.OrderManager.OrderSubState.Void && PickOrderItems.Count == 0 && ExceptionItems.Count == 0)
            {
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Items unpicked successfully" }, MessageToken);
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, ShowAsModal = false, Payload = CommonNavInfo.Customer };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

            }

        }
        #endregion

        #region Variables
        private TrulyObservableCollection<Models.PickOrderItem> _PickOrderItems = new TrulyObservableCollection<PickOrderItem>();
        private ObservableCollection<Models.PickOrderItem> _ExceptionItems = new ObservableCollection<PickOrderItem>();
        public bool _IsAddingToPalette = true, _IsScannerOn = true, _IsManualPick = false, _CanRemoveExceptions = false;
        public int _PackSlipNo = new Random().Next(1000, 10000), _ManualEnteredQty, _PickedItemQty;
        private string _OrderNumber, _PickedItemUM, _PickedItemNo;
        ObservableCollection<ReasonCode> _ManualPickReasonList;
        ObservableCollection<ReasonCode> _COAReasonList;
        public Models.PickOrderItem _SelectedPickItem;
        public Models.PickOrderItem _SelectedExceptionItem;
        public Models.PickOrderItem _LastScanItem;
        private Managers.PricingManager pricingManager;
        private Managers.PickManager pickManager;
        private string _ErrorText;
        public ReasonCode ReasonCodeList = new ReasonCode(string.Empty);
        CashDelivery _CashDeliveryContext;
        ChargeOnAccount _ChargeOnAccountContext;
        StrokeCollection _SignStroke;
        #endregion

        #region Properties


        public string ErrorText
        {
            get { return _ErrorText; }
            set { _ErrorText = value; OnPropertyChanged("ErrorText"); }
        }
        public CashDelivery CashDeliveryContext
        {
            get { return _CashDeliveryContext; }
            set
            {
                _CashDeliveryContext = value;
                OnPropertyChanged("CashDeliveryContext");
            }
        }
        public ChargeOnAccount ChargeOnAccountContext
        {
            get { return _ChargeOnAccountContext; }
            set
            {
                _ChargeOnAccountContext = value;
                OnPropertyChanged("_ChargeOnAccountContext");
            }
        }
        public Models.PickOrderItem LastScanItem
        {
            get
            {
                return _LastScanItem;
            }
            set
            {
                _LastScanItem = value;
                OnPropertyChanged("LastScanItem");
            }
        }

        public Models.PickOrderItem SelectedPickItem
        {
            get
            {
                return _SelectedPickItem;
            }
            set
            {
                _SelectedPickItem = value;
                OnPropertyChanged("SelectedPickItem");
            }
        }
        public Models.PickOrderItem SelectedExceptionItem
        {
            get
            {
                return _SelectedExceptionItem;
            }
            set
            {
                _SelectedExceptionItem = value;
                OnPropertyChanged("SelectedExceptionItem");
            }
        }

        public ObservableCollection<ReasonCode> ManualPickReasonList
        {
            get
            {
                return _ManualPickReasonList;
            }
            set
            {
                _ManualPickReasonList = value;
            }
        }
        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");
            }
        }
        public string PickedItemNo
        {
            get
            {
                return _PickedItemNo;
            }
            set
            {
                _PickedItemNo = value;
                OnPropertyChanged("PickedItemNo");
            }
        }

        public int PickedItemQty
        {
            get
            {
                return _PickedItemQty;
            }
            set
            {
                _PickedItemQty = value;
                OnPropertyChanged("PickedItemQty");
            }
        }

        public string PickedItemUM
        {
            get
            {
                return _PickedItemUM;
            }
            set
            {
                _PickedItemUM = value;
                OnPropertyChanged("PickedItemUM");
            }
        }

        public int ManualEnteredQty
        {
            get
            {
                return _ManualEnteredQty;
            }
            set
            {
                _ManualEnteredQty = value;
                OnPropertyChanged("ManualEnteredQty");
            }
        }

        public ObservableCollection<ReasonCode> COAReasonList
        {
            get
            {
                return _COAReasonList;
            }
            set
            {
                _COAReasonList = value;
            }
        }

        public bool CanRemoveExceptions
        {
            get
            {
                return ExceptionItems.Count > 0 && !PickOrderItems.Any(i => i.OrderQtyInPrimaryUOM > i.PickedQuantityInPrimaryUOM) ? true : false;
            }
            set
            {
                _CanRemoveExceptions = value;
                OnPropertyChanged("CanRemoveExceptions");
            }
        }

        public bool IsManualPick
        {
            get
            {
                return _IsManualPick;
            }
            set
            {
                _IsManualPick = value;
                OnPropertyChanged("IsManualPick");
            }
        }

        public bool IsPickCompleted
        {
            get
            {
                if (this._PickOrderItems != null)
                    return !this._PickOrderItems.Any(w => w.PickedQuantityInPrimaryUOM != w.OrderQtyInPrimaryUOM) && ExceptionItems.Count == 0 && PickOrderItems.Count != 0;
                return false;
            }
            set
            {
                OnPropertyChanged("IsPickCompleted");
            }
        }
        public Guid MessageToken { get; set; }

        public int UnderPickItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Where(w => w.PickedQuantityInPrimaryUOM < w.OrderQtyInPrimaryUOM).Sum(s => s.OrderQtyInPrimaryUOM - s.PickedQuantityInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("UnderPickItemsQtyCount");
            }
        }
        public int OverPickItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Where(w => w.PickedQuantityInPrimaryUOM > w.OrderQtyInPrimaryUOM).Sum(s => s.PickedQuantityInPrimaryUOM - s.OrderQtyInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("OverPickItemsQtyCount");
            }
        }
        public bool IsAddingToPalette
        {
            get
            {
                return _IsAddingToPalette;
            }
            set
            {
                _IsAddingToPalette = value;
                OnPropertyChanged("IsAddingToPalette");

            }
        }

        public bool IsScannerOn
        {
            get
            {
                return _IsScannerOn;
            }
            set
            {
                _IsScannerOn = value;
                OnPropertyChanged("IsScannerOn");

            }
        }
        public int PackSlipNo
        {
            get { return _PackSlipNo; }
            set
            {
                _PackSlipNo = value;
                OnPropertyChanged("PackSlipNo");
            }
        }
        public int DoneItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Sum(s => s.PickedQuantityInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("DoneItemsQtyCount");
            }
        }

        public int TotalOrderQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Sum(s => s.OrderQtyInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("TotalOrderQtyCount");
            }
        }
        private Models.Customer _Customer;
        public Models.Customer Customer
        {
            get
            {
                if (_Customer == null)
                {
                    _Customer = new Models.Customer();
                    _Customer.Name = "STOP N SHOP";
                    _Customer.CustomerNo = "121343";
                    _Customer.Phone = "9878767655";
                    _Customer.Route = "783";
                    _Customer.City = "Oklahoma City";
                    _Customer.State = "OK";
                    _Customer.Zip = "72132";
                }
                return _Customer;
            }
            set
            {
                _Customer = value;
            }
        }

        public string OrderNumber
        {
            get { return Order.OrderId.ToString(); }
            set
            {
                if (value != _OrderNumber)
                {
                    _OrderNumber = value;
                    OnPropertyChanged("OrderNumber");
                }
            }

        }
        public TrulyObservableCollection<Models.PickOrderItem> PickOrderItems
        {
            get { return _PickOrderItems; }
            set
            {
                _PickOrderItems = value;
                OnPropertyChanged("PickOrderItems");
            }
        }

        public ObservableCollection<Models.PickOrderItem> ExceptionItems
        {
            get { return _ExceptionItems; }
            set { _ExceptionItems = value; }
        }
        #endregion





        public string Error
        {
            get { return ErrorText; }

        }

        public string this[string columnName]
        {
            get
            {

                if (columnName == "SelectedPickItem")
                {

                }


                return ErrorText;
            }
        }
    }

    public class TrulyObservableCollection<T> : ObservableCollection<T>
        where T : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler ItemPropertyChanged;

        public TrulyObservableCollection()
            : base()
        {
            CollectionChanged += new NotifyCollectionChangedEventHandler(TrulyObservableCollection_CollectionChanged);
        }


        public TrulyObservableCollection(IEnumerable<T> collection)
            : base(collection)
        {
            foreach (T item in collection)
                item.PropertyChanged += ItemPropertyChanged;

            HookupCollectionChangedEvent();
        }

        public TrulyObservableCollection(List<T> list)
            : base(list)
        {
            list.ForEach(item => item.PropertyChanged += ItemPropertyChanged);

            HookupCollectionChangedEvent();
        }

        private void HookupCollectionChangedEvent()
        {
            CollectionChanged += new NotifyCollectionChangedEventHandler(TrulyObservableCollection_CollectionChanged);
        }


        void TrulyObservableCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                if (e.NewItems != null)
                {
                    foreach (Object item in e.NewItems)
                    {
                        (item as INotifyPropertyChanged).PropertyChanged -= new PropertyChangedEventHandler(item_PropertyChanged);
                        (item as INotifyPropertyChanged).PropertyChanged += new PropertyChangedEventHandler(item_PropertyChanged);
                    }
                }
            if (e.Action == NotifyCollectionChangedAction.Remove)
                if (e.OldItems != null)
                {
                    foreach (Object item in e.OldItems)
                    {
                        (item as INotifyPropertyChanged).PropertyChanged -= new PropertyChangedEventHandler(item_PropertyChanged);
                    }
                }
        }

        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //   NotifyCollectionChangedEventArgs a = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            //  OnCollectionChanged(a);

            if (ItemPropertyChanged != null)
            {
                ItemPropertyChanged(sender, e);
            }
        }
    }


}
