﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        private bool _IsInTransition;
        public bool IsInTransition
        {
            get
            {
                return _IsInTransition;
            }
            set
            {
                _IsInTransition = value;
                OnPropertyChanged("IsInTransition");
            }
        }

        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public DateTime StateChangedTime { get; set; }
        }
    }
}
