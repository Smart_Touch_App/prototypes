﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Data;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.ComponentModel;
using log4net;
using System.Windows.Threading;

namespace SalesLogicExpress.Application.ViewModels
{
    public class PreOrder : ViewModelBase
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PreOrder");
        CommonNavInfo navInfo = new CommonNavInfo();

        #region Commands
        public DelegateCommand AcceptPreOrder { get; set; }
        public DelegateCommand LoadPreOrderWithUsualQuantity { get; set; }
        public DelegateCommand ResetBackPreOrderQuantity { get; set; }
        public DelegateCommand DeleteItemFromPreOrder { get; set; }
        public DelegateCommand BeginGridCellEdit { get; set; }
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand SearchItem { get; set; }
        public DelegateCommand SearchResultTextChange { get; set; }
        public DelegateCommand AddItemToTemplate { get; set; }
        public DelegateCommand GridCellEditEnded { get; set; }
        public DelegateCommand UpdateAvailableQty { get; set; }
        public DelegateCommand DeleteItemFromTemplate { get; set; }
        public DelegateCommand LoadTemplateWithUsualQuantity { get; set; }
        public DelegateCommand ResetBackTemplateQuantity { get; set; }
        #endregion
        #region Constructor

        public PreOrder()
        {
            Initialize();
        }
        public PreOrder(object payload)
        {
            List<object> listObj = payload as List<object>;
            PreOrderMockData data = listObj[0] as PreOrderMockData;
            Customer = (Customer)listObj[1];
            StopDate = data.Stopdate;
            WeekDay = data.Weekday;
            Initialize();
            IsBusy = true;
            GetPreOrderItems();

        }
        #endregion
        #region Variables
        Managers.TemplateManager templateManager;
        Managers.ItemManager itemManager;
        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public DateTime StateChangedTime { get; set; }
        }

        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();

        #endregion
        #region Properties
        private ObservableCollection<Models.Item> _SearchItems = new ObservableCollection<Models.Item>();
        public ObservableCollection<Models.Item> SearchItemList
        {
            get
            {
                return this._SearchItems;
            }
            set
            {
                this._SearchItems.Clear();
                this._SearchItems = value;
                OnPropertyChanged("SearchItemList");
                OnPropertyChanged("SearchResultText");
            }
        }
        private List<int> _previousSequenceNumbers;
        public List<int> PreviousSequenceNumbers
        {
            get { return _previousSequenceNumbers; }
            set { _previousSequenceNumbers = value; }
        }

        bool CheckIsEditEnded = true;
        string _SearchText;
        public string SearchText
        {
            get { return _SearchText; }
            set { _SearchText = value; OnPropertyChanged("SearchText"); }
        }
        private int _SearchItemsCount;
        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }
        public Guid MessageToken { get; set; }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        private Models.Customer _Customer;
        public Models.Customer Customer
        {
            get;
            set;
        }
        private string _StopDate;
        public string StopDate
        {
            get { return _StopDate; }
            set { _StopDate = value; }
        }
        private string _SearchResultText;
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Items found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }
        bool _SearchResultTextVisibility = false;
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }
        private string _WeekDay;
        public string WeekDay
        {
            get { return _WeekDay; }
            set { _WeekDay = value; }
        }
        public DataTable _OrderHistory;
        public DataTable OrderHistory
        {
            get { return _OrderHistory; }
            set { _OrderHistory = value; OnPropertyChanged("OrderHistory"); }
        }
        public DataTable _OrderHistoryHeaders;
        public DataTable OrderHistoryHeaders
        {
            get { return _OrderHistoryHeaders; }
            set { _OrderHistoryHeaders = value; OnPropertyChanged("OrderHistoryHeaders"); }
        }
        private ObservableCollection<Models.TemplateItem> _PreOrderItems = new ObservableCollection<TemplateItem>();
        public ObservableCollection<Models.TemplateItem> PreOrderItems
        {
            get
            {
                return this._PreOrderItems;
            }
            set
            {
                _PreOrderItems = value;
                OnPropertyChanged("PreOrderItems");
            }
        }
        private Dictionary<string, string> OrderQuantityMap = new Dictionary<string, string>();
        public int OldSequenceNo { get; set; }

        #endregion
        #region Methods
        private void Initialize()
        {
            templateManager = new Managers.TemplateManager();
            itemManager = new Managers.ItemManager();
            if (this._SearchItems != null)
                _SearchItemsCount = this._SearchItems.Count;
            InitializeCommands();
        }
        void InitializeCommands()
        {
            Random qtyOnHand = new Random();
            AcceptPreOrder = new DelegateCommand((pram) =>
            {
                templateManager.SavePreOrder(PreOrderItems, Customer.CustomerNo,StopDate);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PreOrder, CloseCurrentView = true, Payload = null, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
            LoadPreOrderWithUsualQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.OrderQty = item.UsualQty;
                }
            });
            ResetBackPreOrderQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.OrderQty = 0;
                }

            });
            DeleteItemFromPreOrder = new DelegateCommand((Items) =>
            {
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                int selectionCount = selectedItems.Count;
                for (int index = 0; index < selectionCount; index++)
                {
                    PreOrderItems.Remove(selectedItems[index] as Models.TemplateItem);
                    index--;
                    selectionCount--;
                }
                OnPropertyChanged("TemplateItemsCount");
            });

            BeginGridCellEdit = new DelegateCommand((param) =>
            {


                ObservableCollection<Models.TemplateItem> TempItems = new ObservableCollection<TemplateItem>();
                TempItems = (ObservableCollection<Models.TemplateItem>)param;
                PreviousSequenceNumbers = new List<int>();


                for (int index = 0; index < TempItems.Count; index++)
                {
                    PreviousSequenceNumbers.Add(TempItems[index].SeqNo);
                }
                CheckIsEditEnded = true;

            });
            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchItemOnType.Execute(SearchText);
            });
            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), false);
            });
            SearchItem = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), true);
            });
            SearchResultTextChange = new DelegateCommand((param) =>
            {
                SearchItemsCount = Convert.ToInt32(param);
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            });
            AddItemToTemplate = new DelegateCommand((Items) =>
            {
                Managers.ItemManager itemManager = new Managers.ItemManager();
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                int selectionCount = selectedItems.Count;
                int maxSequenceNo = PreOrderItems.Count > 0 ? PreOrderItems.Max(item => item.SeqNo) : 0;
                int seqNo = 1;
                for (int index = 0; index < selectionCount; index++)
                {
                    if (PreOrderItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectedItemExists, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        continue;
                    }
                    Models.TemplateItem addedItem = new TemplateItem((selectedItems[index] as Models.Item));
                    addedItem.EffectiveFrom = DateTime.Now.ToString("MM/dd/yyyy");
                    addedItem.EffectiveThru = DateTime.Now.ToString("MM/dd/yyyy");
                    SearchItemList.Remove(selectedItems[index] as Models.Item);
                    addedItem.SeqNo = maxSequenceNo + (seqNo * 5);
                    addedItem.ActualQtyOnHand = qtyOnHand.Next(5, 50);
                    string itemNumber = addedItem.ItemNumber;
                    // OrderItem order = new OrderItem(new Item());
                    addedItem.AppliedUMS = itemManager.GetAppliedUMs(addedItem.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(addedItem.ItemNumber) : new List<string>() { addedItem.UM };
                    PreOrderItems.Insert(0, addedItem);
                    index--;
                    selectionCount--;
                    seqNo++;
                    SearchItemsCount = SearchItemList.Count;
                    OnPropertyChanged("SearchItemsCount");
                    OnPropertyChanged("SearchResultText");
                }
                OnPropertyChanged("TemplateItemsCount");
            });
            GridCellEditEnded = new DelegateCommand((param) =>
            {

               OnPropertyChanged("TemplateItems");
                
            });

            UpdateAvailableQty = new DelegateCommand((param) =>
            {
                TemplateItem templateItem = param as TemplateItem;
                templateItem.QtyOnHand = (templateItem.ActualQtyOnHand - templateItem.OrderQty) < 0 ? 0 : (templateItem.ActualQtyOnHand - templateItem.OrderQty);
            });

            DeleteItemFromTemplate = new DelegateCommand((Items) =>
            {
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                int selectionCount = selectedItems.Count;
                for (int index = 0; index < selectionCount; index++)
                {
                    PreOrderItems.Remove(selectedItems[index] as Models.TemplateItem);
                    index--;
                    selectionCount--;
                }
                OnPropertyChanged("TemplateItemsCount");
            });

            LoadTemplateWithUsualQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.PreOrderQty = item.UsualQty;
                }

            });
            ResetBackTemplateQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.PreOrderQty = 0;
                }
            });
        }
        async void GetPreOrderItems()
        {
            await Task.Run(() =>
            {//**********************************************************************************************************

                Managers.OrderManager orderManager = new Managers.OrderManager();
                // Get Templates
                //**********************************************************************************************************
                PreOrderItems = templateManager.GetTemplateItemsForCustomer(Customer.CustomerNo);
                ObservableCollection<TemplateItem> PreOrder = new ObservableCollection<TemplateItem>();
                PreOrder = templateManager.GetPreOrderItems(Customer.CustomerNo);
                ObservableCollection<TemplateItem> items = this._PreOrderItems;;
                ObservableCollection<TemplateItem> updateItems = new ObservableCollection<TemplateItem>();
                ObservableCollection<TemplateItem> inserItems = new ObservableCollection<TemplateItem>();
                foreach (Models.TemplateItem item in items)
                {
                    if (PreOrder.Any(x => x.ItemNumber == item.ItemNumber))
                    {
                        //Models.TemplateItem i = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                        //updateItems.Add(i);
                        Models.TemplateItem i  = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                        //this._PreOrderItems.ElementAt(this._PreOrderItems.IndexOf(i)).PreOrderQty = i.PreOrderQty;
                        item.PreOrderQty = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber).PreOrderQty;
                        item.UM = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber).UM;
                        PreOrder.Remove(i);
                    }
                }
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                   new Action(() =>
                   {
                       foreach (Models.TemplateItem item in PreOrder)
                       {
                           items.Add(item);
                       }
                   }
               ));
                
               
                //**********************************************************************************************************
                ModelChangeArgs args = new ModelChangeArgs();
                args.StateChangedTime = DateTime.Now;
                OnModelChanged(args);
                IsBusy = false;
                OnPropertyChanged("TemplateItemsCount");

            });

        }
        public async Task SearchItems(string searchTerm, bool skipMinLengthCheck)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:SearchItems][searchTerm=" + searchTerm + ",skipMinLengthCheck="+skipMinLengthCheck+"]");
            await Task.Delay(500, tokenForCancelTask.Token);
            if (!skipMinLengthCheck)
            {
                if (searchTerm != null && searchTerm.ToString().Length < 3)
                {
                    return;
                }
                if (searchTerm != null && searchTerm.ToString().Length == 0)
                {
                    SearchItemList.Clear();
                    this.OnPropertyChanged("SearchItemsCount");
                    this.OnPropertyChanged("SearchResultText");
                    return;
                }
            }
            SearchItemList.Clear();
            SearchItemList = itemManager.SearchItem(searchTerm.ToString());
            SearchItemsCount = SearchItemList.Count;
            this.OnPropertyChanged("SearchItemsCount");
            this.OnPropertyChanged("SearchResultText");
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:SearchItems][searchTerm=" + searchTerm + ",skipMinLengthCheck=" + skipMinLengthCheck + "]");
           
        }
        public Telerik.Windows.Data.SortDescriptor SortGrid()
        {

            Telerik.Windows.Data.SortDescriptor descriptor = new Telerik.Windows.Data.SortDescriptor();
            descriptor.Member = "SeqNo";
            descriptor.SortDirection = ListSortDirection.Ascending;

            return descriptor;
        }
        #endregion
        /********************************* 
         
        string str = "1/18/2015";
        DateTime dt = Convert.ToDateTime(str); 
        Console.WriteLine(dt);
        Console.WriteLine("Year: {0}, Month: {1}, Day: {2}", dt.Year, dt.Month, dt.Day);
        string currentdate = DateTime.Now.ToString("MM/dd/yyyy");
        Console.WriteLine(currentdate);
         
         *********************************/
    }
}
