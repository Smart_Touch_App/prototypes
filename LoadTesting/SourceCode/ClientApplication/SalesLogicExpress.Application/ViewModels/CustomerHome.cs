﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;

using System.Text.RegularExpressions;


namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerHome : ViewModelBase, IDataErrorInfo
    {
        public class Contact : ViewModelBase, IDataErrorInfo
        {
            private CustomerContact _CustomerContact;

            private int _Index;
            public int Index
            {
                get
                {
                    return _Index;
                }
                set
                {
                    _Index = value;
                    OnPropertyChanged("Index");
                }
            }
            public CustomerContact CustomerContact
            {
                get { return _CustomerContact; }
                set { _CustomerContact = value; }
            }
            public DelegateCommand PropertyLostFocus { get; set; }


            private bool _ToggleAddButton = false;

            public bool ToggleAddButton
            {
                get
                {
                    return _ToggleAddButton;
                }
                set
                {
                    _ToggleAddButton = value;
                    OnPropertyChanged("ToggleAddButton");
                }
            }
            public Contact()
            {
                InitializeCommands();
            }
            void InitializeCommands()
            {
                PropertyLostFocus = new DelegateCommand((param) =>
                {
                    if (string.IsNullOrEmpty(_FirstName))
                    {
                        IsStringEmpty = true;
                        OnPropertyChanged("FirstName");
                    }
                    else IsStringEmpty = false;
                });
            }
            string _Title, _ContactID, _FirstName, _LastName, _Phone, _PhonePart1, _PhonePart2, _PhonePart3, _SelectedPhoneType, _SelectedEmailType, _AreaCode, _Extension, _Email;
            public string ContactID
            {
                get
                {
                    return _ContactID;
                }
                set
                {
                    _ContactID = value;
                    OnPropertyChanged("ContactID");
                }
            }
            public string Title
            {
                get
                {
                    return _Title;
                }
                set
                {
                    _Title = value;
                    if (letterOnlyRegex.IsMatch(_Title))
                    {
                        IsValidTitle = true;
                        if (!string.IsNullOrEmpty(_FirstName))
                            ToggleAddButton = true;
                    }

                    else
                    {
                        IsValidTitle = false;
                    }

                    OnPropertyChanged("Title");
                }
            }
            public string FirstName
            {
                get
                {
                    _FirstName.Trim();
                    return _FirstName;
                }
                set
                {
                    _FirstName = value;
                    if (string.IsNullOrEmpty(_FirstName))
                    {
                        IsStringEmpty = true;
                    }
                    else
                    {
                        IsStringEmpty = false;

                        if (letterOnlyRegex.IsMatch(_FirstName))
                        {
                            IsValidFirstName = true;
                            ToggleAddButton = true;
                        }

                        else
                        {
                            IsValidFirstName = false;
                        }
                    }
                    OnPropertyChanged("FirstName");
                }
            }
            public string LastName
            {
                get
                {
                    _LastName.Trim();
                    return _LastName;
                }
                set
                {
                    _LastName = value;
                    OnPropertyChanged("LastName");
                }
            }
            public string Phone
            {
                get
                {
                    return PhonePart1 + "-" + PhonePart2;
                }
                set
                {
                    _Phone = value;

                    if (IsValidAreaCode && IsValidPhone_1 && IsValidPhone_2 && IsValidExtension)
                    {
                        IsValidPhone = true;
                        ToggleAddButton = true;
                    }
                    else
                        IsValidPhone = false;


                    //else IsStringEmpty = true;

                    OnPropertyChanged("Phone");
                }
            }
            public string PhonePart1
            {
                get
                {
                    return _PhonePart1;
                }
                set
                {
                    _PhonePart1 = value;
                    if (_PhonePart1 == string.Empty)
                        IsPhoneEmpty = false;

                    else
                    {
                        IsPhoneEmpty = true;
                        if (numericOnlyRegex.IsMatch(_PhonePart1) && (_PhonePart1.Length < 4))
                        {
                            IsValidPhone_1 = true;
                            if (!string.IsNullOrEmpty(_FirstName))
                                ToggleAddButton = true;
                        }

                        else
                        {
                            IsValidPhone_1 = false;
                        }
                    }
                    OnPropertyChanged("PhonePart1");
                    OnPropertyChanged("Phone");
                }
            }
            public string PhonePart2
            {
                get
                {
                    return _PhonePart2;
                }
                set
                {
                    _PhonePart2 = value;
                    if (_PhonePart2 == string.Empty)
                        IsPhoneEmpty = false;
                    else
                    {
                        IsPhoneEmpty = true;
                        if (numericOnlyRegex.IsMatch(_PhonePart2) && (_PhonePart2.Length < 5))
                        {
                            IsValidPhone_2 = true;
                            if (!string.IsNullOrEmpty(_FirstName))
                                ToggleAddButton = true;
                        }
                        else
                        {
                            IsValidPhone_2 = false;
                        }
                    }
                    OnPropertyChanged("PhonePart2");
                    OnPropertyChanged("Phone");
                }
            }
            public string PhonePart3
            {
                get
                {
                    return _PhonePart3;
                }
                set
                {
                    _PhonePart3 = value;
                    if (numericOnlyRegex.IsMatch(_PhonePart3))
                    {
                        IsValidPhone_3 = true;
                        if (!string.IsNullOrEmpty(_FirstName))
                            ToggleAddButton = true;
                    }

                    else
                    {
                        IsValidPhone_3 = false;
                    }
                    OnPropertyChanged("PhonePart3");
                }
            }
            public string Extension
            {
                get
                {
                    return _Extension;
                }
                set
                {
                    _Extension = value;
                    if (_Extension == string.Empty)
                        IsPhoneEmpty = false;

                    else
                    {
                        IsPhoneEmpty = true;
                        if (alphaNumericRegex.IsMatch(_Extension) && (_Extension.Length < 8))
                        {
                            IsValidExtension = true;
                            if (!string.IsNullOrEmpty(_FirstName))
                                ToggleAddButton = true;
                        }
                        else
                        {
                            IsValidExtension = false;
                        }
                    }
                    OnPropertyChanged("Extension");
                    OnPropertyChanged("Phone");
                }
            }
            public string AreaCode
            {
                get
                {
                    return _AreaCode;
                }
                set
                {
                    _AreaCode = value;
                    if (_AreaCode == string.Empty)
                        IsPhoneEmpty = false;

                    else
                    {
                        IsPhoneEmpty = true;
                        if (numericOnlyRegex.IsMatch(_AreaCode) && (_AreaCode.Length < 4))
                        {
                            IsValidAreaCode = true;
                            if (!string.IsNullOrEmpty(_FirstName))
                                ToggleAddButton = true;
                        }
                        else
                        {
                            IsValidAreaCode = false;
                        }
                    }
                    OnPropertyChanged("AreaCode");
                    OnPropertyChanged("Phone");
                }
            }
            public string EmailID
            {
                get
                {
                    return _Email;
                }
                set
                {
                    _Email = value;
                    if (string.IsNullOrEmpty(_Email))
                        IsStringEmpty = false;

                    else
                    {
                        IsStringEmpty = true;
                        if (emailRegex.IsMatch(_Email))
                        {
                            IsValidEmail = true;
                            if (!string.IsNullOrEmpty(_FirstName))
                                ToggleAddButton = true;
                        }
                        else
                            IsValidEmail = false;
                    }
                    OnPropertyChanged("EmailID");
                }
            }
            public string SelectedPhoneType
            {
                get
                {
                    return _SelectedPhoneType;
                }
                set
                {
                    _SelectedPhoneType = value;
                    OnPropertyChanged("SelectedPhoneType");
                }
            }
            public string SelectedEmailType
            {
                get
                {
                    return _SelectedEmailType;
                }
                set
                {
                    _SelectedEmailType = value;
                    OnPropertyChanged("SelectedEmailType");
                }
            }
            private int _PhoneTypeID;
            public int PhoneTypeID
            {
                get
                {
                    return _PhoneTypeID;
                }
                set
                {
                    _PhoneTypeID = value;
                    OnPropertyChanged("PhoneTypeID");
                }
            }
            private int _EmailTypeID;
            public int EmailTypeID
            {
                get
                {
                    return _EmailTypeID;
                }
                set
                {
                    _EmailTypeID = value;
                    OnPropertyChanged("EmailTypeID");
                }
            }
            public ObservableCollection<ContactType> PhoneType { get; set; }
            public ObservableCollection<ContactType> EmailType { get; set; }

            public string Error
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            private Regex letterOnlyRegex = new Regex(@"^[A-Za-z ]*$");
            private Regex numericOnlyRegex = new Regex(@"[0-9]+");
            private Regex alphaNumericRegex = new Regex(@"[a-zA-Z0-9]+");
            private Regex emailRegex = new Regex(@"^([a-z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-z0-9_\-]+\.)+))([a-z]{2,4}|[0-9]{1,3})(\]?)$");

            bool IsValidPhone = true;
            bool IsStringEmpty = false;
            bool IsValidFirstName = true;
            bool IsValidEmail = true;
            bool IsValidPhone_1 = true;
            bool IsValidPhone_2 = true;
            bool IsValidTitle = true;
            bool IsValidPhone_3 = true;
            bool IsValidExtension = true;
            bool IsValidAreaCode = true;
            bool IsPhoneEmpty = false;
            public string this[string columnName]
            {
                get
                {
                    string result = string.Empty;

                    if (columnName == "FirstName" && IsStringEmpty)
                    {
                        result = "* Field is required!";
                        ToggleAddButton = false;
                    }
                    else if (columnName == "FirstName" && (FirstName.Length < 3))
                    {
                        result = "* Invalid Name.";
                        ToggleAddButton = false;
                    }
                    else if (columnName == "FirstName" && !IsValidFirstName)
                    {
                        result = "* Name can contain only letters!";
                        ToggleAddButton = false;
                    }
                    else if (columnName == "Title" && !IsValidTitle)
                    {
                        result = "* Title can contain only letters!";
                        ToggleAddButton = false;
                    }
                    else if (columnName == "EmailID" && IsStringEmpty)
                    {
                        if (!IsValidEmail)
                        {
                            result = "* Invalid email address!";
                            ToggleAddButton = false;
                        }
                    }
                    else if (columnName == "AreaCode" && IsPhoneEmpty)
                    {
                        if (!IsValidAreaCode)
                        {
                            result = "* Field can contain only numbers!";
                            ToggleAddButton = false;
                        }
                    }
                    else if (columnName == "PhonePart1" && IsPhoneEmpty)
                    {
                        if (!IsValidPhone_1)
                        {
                            result = "* Field can contain only numbers!";
                            ToggleAddButton = false;
                        }
                    }
                    else if (columnName == "PhonePart2" && IsPhoneEmpty)
                    {
                        if (!IsValidPhone_2)
                        {
                            result = "* Field can contain only numbers!";
                            ToggleAddButton = false;
                        }
                    }
                    else if (columnName == "Extension" && IsPhoneEmpty)
                    {
                        if (!IsValidExtension)
                        {
                            result = "* Field can contain only numbers!";
                            ToggleAddButton = false;
                        }
                    }

                    return result;
                }
            }
        }
        Contact _NewContact = new Contact();
        public Contact NewContact
        {
            get
            {
                return _NewContact;
            }
            set
            {
                _NewContact = value;
                OnPropertyChanged("NewContact");
            }
        }
        #region Properties and Delegates
        public DelegateCommand ShowOrderTemplate { get; set; }
        public DelegateCommand AddNewContact { get; set; }
        public DelegateCommand AddNewPhone { get; set; }
        public DelegateCommand AddNewEmail { get; set; }
        public DelegateCommand SelectAllContacts { get; set; }
        public DelegateCommand SelectContact { get; set; }
        public DelegateCommand SaveNewContact { get; set; }
        public DelegateCommand SaveNewPhone { get; set; }
        public DelegateCommand SaveNewEmail { get; set; }
        public DelegateCommand EditContact { get; set; }
        public DelegateCommand EditPhone { get; set; }
        public DelegateCommand EditEmail { get; set; }
        public DelegateCommand SaveEditedContact { get; set; }
        public DelegateCommand SaveEditedEmail { get; set; }
        public DelegateCommand SaveEditedPhone { get; set; }
        public DelegateCommand DeleteEmail { get; set; }
        public DelegateCommand DeletePhone { get; set; }
        public DelegateCommand DeleteContactFromContactList { get; set; }
        public DelegateCommand OpenPreOrderDialog { get; set; }
        public DelegateCommand MoveToPreOrder { get; set; }

        private TrulyObservableCollection<Models.CustomerContact> _CustomerContacts;
        public ObservableCollection<CustomerContact> CustomerContactList { get; set; }
        public TrulyObservableCollection<Models.CustomerContact> CustomerContacts
        {
            get
            {
                return _CustomerContacts;
            }
            set
            {
                _CustomerContacts = value;
                OnPropertyChanged("CustomerContacts");
            }
        }
        private int _CheckSelectedIndex = 0;
        public int CheckSelectedIndex
        {
            get { return _CheckSelectedIndex; }
            set { _CheckSelectedIndex = value; OnPropertyChanged("CheckSelectedIndex"); }
        }
        private bool _IsButtonEnabled = false;
        public bool IsButtonEnabled
        {
            get { return _IsButtonEnabled; }
            set { _IsButtonEnabled = value; OnPropertyChanged("IsButtonEnabled"); }
        }
        public Models.Customer Customer
        {
            get;
            set;
        }
        private CustomerContact _CustomerContact;
        public Models.CustomerContact CustomerContact
        {
            get
            {
                return _CustomerContact;
            }
            set
            {
                _CustomerContact = value;
                OnPropertyChanged("CustomerContact");
            }
        }

        public ObservableCollection<PreOrderMockData> _NextStopDates = new ObservableCollection<PreOrderMockData>();
        public ObservableCollection<PreOrderMockData> NextStopDates
        {
            get
            {
                return _NextStopDates;
            }
            set
            {
                _NextStopDates = value;
            }


        }

        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }

        private bool _SelectAllCheck = false;
        public bool SelectAllCheck
        {
            get
            {
                return _SelectAllCheck;
            }
            set
            {
                _SelectAllCheck = value;
                OnPropertyChanged("SelectAllCheck");
            }
        }
        string _TodaysDate;
        public string TodaysDate
        {
            get
            {
                return _TodaysDate;
            }
            set
            {
                _TodaysDate = value;
                OnPropertyChanged("TodaysDate");
            }
        }
        string _TodaysDateFormatted;
        public string TodaysDateFormatted
        {
            get { return _TodaysDateFormatted; }
            set { _TodaysDateFormatted = value; }
        }
        public Guid MessageToken { get; set; }
        bool _ShowContactPanel;
        public bool ShowContactPanel
        {
            get
            {
                return _ShowContactPanel;
            }
            set
            {
                _ShowContactPanel = value;
                OnPropertyChanged("ShowContactPanel");
            }
        }
        #endregion

        #region Constructor And Methods

        public CustomerHome(object customer)
        {
            Customer = (Customer)customer;
            InitializeCommands();
            GetCustomerContact();
            GetCustomerContacts();
            GetContactType();
            NextStopDates.Add(new PreOrderMockData { Stopdate = (DateTime.Now.AddDays(7).ToString("MM/dd/yyyy")).Replace('-', '/'), Weekday = DateTime.Now.AddDays(7).ToString("dddd") });
            NextStopDates.Add(new PreOrderMockData { Stopdate = (DateTime.Now.AddDays(14).ToString("MM/dd/yyyy")).Replace('-', '/'), Weekday = DateTime.Now.AddDays(14).ToString("dddd") });
            NextStopDates.Add(new PreOrderMockData { Stopdate = (DateTime.Now.AddDays(21).ToString("MM/dd/yyyy")).Replace('-', '/'), Weekday = DateTime.Now.AddDays(21).ToString("dddd") });
            NextStopDates.Add(new PreOrderMockData { Stopdate = (DateTime.Now.AddDays(28).ToString("MM/dd/yyyy")).Replace('-', '/'), Weekday = DateTime.Now.AddDays(28).ToString("dddd") });
            NextStopDates.Add(new PreOrderMockData { Stopdate = (DateTime.Now.AddDays(35).ToString("MM/dd/yyyy")).Replace('-', '/'), Weekday = DateTime.Now.AddDays(35).ToString("dddd") });

        }
        void GetContactType()
        {
            Managers.ContactManager manager = new Managers.ContactManager();
            ObservableCollection<ContactType> types = manager.GetContactTypes();
            NewContact = new Contact();
            List<ContactType> list = types.Where(item => item.Type.Trim().Equals("PHONE")).ToList<ContactType>();
            NewContact.PhoneType = new ObservableCollection<ContactType>();
            foreach (var item in list)
            {
                NewContact.PhoneType.Add(item);
            }
            list = types.Where(item => item.Type.Trim().Equals("EMAIL")).ToList<ContactType>();
            NewContact.EmailType = new ObservableCollection<ContactType>();
            foreach (var item in list)
            {
                NewContact.EmailType.Add(item);
            }
        }
        void InitializeCommands()
        {

            TodaysDate = DateTime.Now.ToString("dddd, MM/dd/yyyy");
            TodaysDateFormatted = (DateTime.Now.ToString("dddd, MM/dd/yyyy")).Replace('-', '/');
            SaveNewContact = new DelegateCommand((Selected) =>
            {
                Phone phoneDetail = new Phone();
                phoneDetail.AreaCode = NewContact.AreaCode;
                phoneDetail.Extension = NewContact.Extension;
                phoneDetail.IsDefault = false;
                phoneDetail.Value = NewContact.Phone;
                phoneDetail.Type = NewContact.SelectedPhoneType;

                Email emailDetail = new Email();
                emailDetail.Value = NewContact.EmailID;
                emailDetail.Type = NewContact.SelectedEmailType;
                emailDetail.IsDefault = false;

                Managers.ContactManager manager = new Managers.ContactManager();
                Models.CustomerContact customerContactItem = new Models.CustomerContact();
                customerContactItem.EmailList = new List<Email>();
                if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.EmailList.Add(emailDetail);
                }
                customerContactItem.PhoneList = new List<Phone>();
                if (phoneDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.PhoneList.Add(phoneDetail);
                }
                customerContactItem.ContactName = NewContact.FirstName;
                customerContactItem.ContactTitle = NewContact.Title;
                customerContactItem.IsActive = true;
                customerContactItem.IsDefault = false;
                customerContactItem.CustomerID = Customer.CustomerNo;
                manager.AddCustomerContact(customerContactItem);

                NewContact.Title = string.Empty;
                NewContact.FirstName = string.Empty;
                NewContact.SelectedEmailType = string.Empty;
                NewContact.SelectedPhoneType = string.Empty;
                NewContact.EmailID = string.Empty;
                NewContact.PhonePart1 = string.Empty;
                NewContact.PhonePart2 = string.Empty;
                NewContact.Extension = string.Empty;
                NewContact.AreaCode = string.Empty;
                GetCustomerContacts();
            });
            SaveNewPhone = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContact = new Models.CustomerContact();

                string CustomerID = Customer.CustomerNo;
                Phone phoneDetail = new Phone();
                phoneDetail.ContactID = NewContact.ContactID;
                phoneDetail.AreaCode = NewContact.AreaCode;
                phoneDetail.Extension = NewContact.Extension;
                phoneDetail.IsDefault = false;
                phoneDetail.Value = NewContact.Phone;
                phoneDetail.Type = NewContact.SelectedPhoneType;

                Managers.ContactManager contactManager = new Managers.ContactManager();
                contactManager.SaveContactPhone(phoneDetail, CustomerID);
                GetCustomerContacts();
            });

            SaveNewEmail = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContact = new Models.CustomerContact();

                string CustomerID = Customer.CustomerNo;
                Email emailDetail = new Email();
                emailDetail.ContactID = NewContact.ContactID;
                emailDetail.Value = NewContact.EmailID;
                emailDetail.Type = NewContact.SelectedEmailType;
                emailDetail.IsDefault = false;

                Managers.ContactManager contactManager = new Managers.ContactManager();
                contactManager.SaveContactEmail(emailDetail, CustomerID);
                GetCustomerContacts();
            });
            SelectContact = new DelegateCommand((Selected) =>
            {
                bool select = (bool)Selected;
                if (!select)
                {
                    SelectAllCheck = select;
                }
            });
            SelectAllContacts = new DelegateCommand((SelectAll) =>
            {
                bool select = (bool)SelectAll;
                foreach (CustomerContact cust in CustomerContactList)
                {
                    cust.IsSelected = select;
                }
            });
            ShowOrderTemplate = new DelegateCommand((param) =>
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Order Template";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(Customer);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.OrderTemplate, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, Payload = Customer, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            DeleteContactFromContactList = new DelegateCommand((param) =>
            {
                List<CustomerContact> contactList = CustomerContactList.Where(contact => contact.IsSelected ==true).ToList<CustomerContact>();
                int selectedContacts = contactList.Count();
                if (selectedContacts == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Please select contact to delete!", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteEmailConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                Managers.ContactManager manager = new Managers.ContactManager();
                Models.CustomerContact customerContact = new Models.CustomerContact();
                customerContact.CustomerID = Customer.CustomerNo;

                for (int index = 0; index < selectedContacts; index++)
                {
                    customerContact.ContactID = contactList[index].ContactID;
                    manager.DeleteCustomerContact(customerContact);
                }

                GetCustomerContacts();
            });
            AddNewContact = new DelegateCommand((param) =>
            {
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewContact", Title = "Add New Contact" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            AddNewPhone = new DelegateCommand((param) =>
            {
                Phone customerPhone = param as Phone;
                CustomerContact customerContact = param as CustomerContact;
                Managers.ContactManager manager = new Managers.ContactManager();
                string customerID = Customer.CustomerNo;

                //customerContact = manager.GetContactNameAndTitle(NewContact.ContactID, customerID);
                NewContact.ContactID = customerContact.ContactID;
                NewContact.FirstName = customerContact.ContactName;
                NewContact.Title = customerContact.ContactTitle;

                NewContact.PhonePart1 = string.Empty;
                NewContact.PhonePart2 = string.Empty;
                NewContact.Extension = string.Empty;
                NewContact.AreaCode = string.Empty;

                var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewPhone", Title = "Add New Phone Number" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            AddNewEmail = new DelegateCommand((param) =>
            {

                CustomerContact customerContact = param as CustomerContact;
                Managers.ContactManager manager = new Managers.ContactManager();
                string customerID = Customer.CustomerNo;

                NewContact.ContactID = customerContact.ContactID;
                NewContact.FirstName = customerContact.ContactName; //string.IsNullOrEmpty(customerContact.ContactName) ? "" : customerContact.ContactName.ToString();
                NewContact.Title = customerContact.ContactTitle;//string.IsNullOrEmpty(customerContact.ContactTitle) ? "" : customerContact.ContactTitle.ToString();
                NewContact.EmailID = string.Empty;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewEmail", Title = "Add New Email" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            // command on double tap for Edit Contact Pop-up
            EditContact = new DelegateCommand((param) =>
            {

                CustomerContact customerContact = param as CustomerContact;
                int PhoneTypeID = 0;
                int EmailTypeID = 0;
                try
                {
                    NewContact.FirstName = customerContact.ContactName;
                    NewContact.Title = customerContact.ContactTitle;
                    NewContact.SelectedPhoneType = customerContact.PhoneList[0].Type;
                    string phoneCode = NewContact.SelectedPhoneType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.PhoneType)
                    {
                        if (item.Type == "PHONE" && item.Code == phoneCode)
                        {
                            PhoneTypeID = Convert.ToInt32(item.TypeID);
                            break;
                        }
                    }

                    NewContact.AreaCode = customerContact.PhoneList[0].AreaCode;
                    NewContact.PhonePart1 = customerContact.PhoneList[0].Value.Split('-')[0];
                    NewContact.PhonePart2 = customerContact.PhoneList[0].Value.Split('-')[1];
                    NewContact.Extension = customerContact.PhoneList[0].Extension;
                    NewContact.EmailID = customerContact.EmailList[0].Value;
                    NewContact.SelectedEmailType = customerContact.EmailList[0].Type;
                    string emailCode = NewContact.SelectedEmailType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.EmailType)
                    {
                        if (item.Type == "EMAIL" && item.Code == emailCode)
                        {
                            EmailTypeID = Convert.ToInt32(item.TypeID);
                            break;
                        }
                    }
                    NewContact.ContactID = customerContact.ContactID;
                }
                catch (Exception ex)
                {

                }
                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingContact", Title = "Edit Contact" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });

            EditPhone = new DelegateCommand((param) =>
            {
                CustomerContact customerContact = new CustomerContact();
                Managers.ContactManager manager = new Managers.ContactManager();

                Phone customerPhone = param as Phone;
                int PhoneTypeID = 0;
                string customerID = Customer.CustomerNo;
                try
                {
                    NewContact.ContactID = customerPhone.ContactID;
                    customerContact = manager.GetContactNameAndTitle(NewContact.ContactID, customerID);
                    NewContact.Index = customerPhone.Index;
                    NewContact.FirstName = string.IsNullOrEmpty(customerContact.ContactName) ? "" : customerContact.ContactName.ToString();
                    NewContact.Title = customerContact.ContactTitle.ToString();
                    NewContact.SelectedPhoneType = customerPhone.Type;
                    string phoneCode = NewContact.SelectedPhoneType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.PhoneType)
                    {
                        if (item.Type == "PHONE" && item.Code == phoneCode)
                        {
                            PhoneTypeID = Convert.ToInt32(item.TypeID);
                            break;
                        }
                    }
                    NewContact.AreaCode = customerPhone.AreaCode;
                    NewContact.PhonePart1 = customerPhone.Value.Split('-')[0];
                    NewContact.PhonePart2 = customerPhone.Value.Split('-')[1];
                    NewContact.Extension = customerPhone.Extension;
                }
                catch (Exception ex)
                {

                }
                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingPhone", Title = "Edit Phone" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            EditEmail = new DelegateCommand((param) =>
            {
                Email customerEmail = param as Email;
                CustomerContact customerContact = new CustomerContact();
                Managers.ContactManager manager = new Managers.ContactManager();
                int EmailTypeID = 0;
                try
                {
                    string customerID = Customer.CustomerNo;
                    NewContact.ContactID = customerEmail.ContactID;
                    NewContact.Index = customerEmail.Index;
                    customerContact = manager.GetContactNameAndTitle(NewContact.ContactID, customerID);
                    NewContact.FirstName = customerContact.ContactName.ToString();
                    NewContact.Title = customerContact.ContactTitle.ToString();
                    NewContact.EmailID = customerEmail.Value;
                    NewContact.SelectedEmailType = customerEmail.Type;
                    string emailCode = NewContact.SelectedEmailType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.EmailType)
                    {
                        if (item.Type == "EMAIL" && item.Code == emailCode)
                        {
                            EmailTypeID = Convert.ToInt32(item.TypeID);
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingEmail", Title = "Edit Email" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            // command to save edited contact
            SaveEditedContact = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContactItem = new CustomerContact();
                Phone phoneDetail = new Phone();
                double result;
                bool flag;
                phoneDetail.AreaCode = NewContact.AreaCode;
                phoneDetail.Value = NewContact.Phone;
                phoneDetail.Extension = NewContact.Extension;

                flag = Double.TryParse(NewContact.SelectedPhoneType, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out result);
                if (!flag)
                {
                    string phoneCode = NewContact.SelectedPhoneType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.PhoneType)
                    {
                        if (item.Type == "PHONE" && item.Code == phoneCode)
                        {
                            phoneDetail.TypeID = Convert.ToInt32(item.TypeID);
                            break;
                        }
                    }
                }
                else
                {
                    phoneDetail.TypeID = Convert.ToInt32(NewContact.SelectedPhoneType);
                }
                phoneDetail.IsDefault = false;

                Email emailDetail = new Email();
                emailDetail.Value = NewContact.EmailID;

                emailDetail.IsDefault = false;

                flag = Double.TryParse(NewContact.SelectedEmailType, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out result);
                if (!flag)
                {
                    string emailCode = NewContact.SelectedEmailType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.EmailType)
                    {
                        if (item.Type == "EMAIL" && item.Code == emailCode)
                        {
                            emailDetail.TypeID = Convert.ToInt32(item.TypeID);
                            break;
                        }
                    }
                }
                else
                {
                    emailDetail.TypeID = Convert.ToInt32(NewContact.SelectedEmailType);
                }
                Managers.ContactManager manager = new Managers.ContactManager();

                customerContactItem.EmailList = new List<Email>();
                if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.EmailList.Add(emailDetail);
                }
                customerContactItem.PhoneList = new List<Phone>();
                if (phoneDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.PhoneList.Add(phoneDetail);
                }
                customerContactItem.ContactID = NewContact.ContactID;
                customerContactItem.ContactName = NewContact.FirstName;
                customerContactItem.ContactTitle = NewContact.Title;
                customerContactItem.CustomerID = Customer.CustomerNo;
                customerContactItem.IsDefault = false;

                manager.SaveEditedContact(customerContactItem);
                GetCustomerContacts();
            });

            SaveEditedEmail = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContactItem = new Models.CustomerContact();
                Managers.ContactManager contactManager = new Managers.ContactManager();
                double result;
                bool flag;
                Email emailDetail = new Email();
                //emailDetail.Index = NewContact.Index;
                emailDetail.Value = NewContact.EmailID;

                flag = Double.TryParse(NewContact.SelectedEmailType, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out result);
                if (!flag)
                {
                    string emailCode = NewContact.SelectedEmailType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.EmailType)
                    {
                        if (item.Type == "EMAIL" && item.Code == emailCode)
                        {
                            emailDetail.TypeID = NewContact.EmailType.IndexOf(item);
                            break;
                        }
                    }
                }
                else
                {
                    emailDetail.TypeID = Convert.ToInt32(NewContact.SelectedEmailType);
                }

                if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.EmailList = new List<Email>();
                    customerContactItem.EmailList.Add(emailDetail);
                }
                customerContactItem.ContactID = NewContact.ContactID;
                customerContactItem.CustomerID = Customer.CustomerNo;
                contactManager.SaveEditedEmail(customerContactItem, NewContact.Index);
                GetCustomerContacts();
            });
            SaveEditedPhone = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContactItem = new Models.CustomerContact();
                Managers.ContactManager contactManager = new Managers.ContactManager();
                customerContactItem.CustomerID = Customer.CustomerNo;
                customerContactItem.ContactID = NewContact.ContactID;
                Phone phoneDetail = new Phone();
                phoneDetail.Index = NewContact.Index;
                phoneDetail.AreaCode = NewContact.AreaCode;
                phoneDetail.Value = NewContact.Phone;
                phoneDetail.Extension = NewContact.Extension;
                double result;
                bool flag;

                flag = Double.TryParse(NewContact.SelectedPhoneType, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out result);
                if (!flag)
                {
                    string phoneCode = NewContact.SelectedPhoneType.Split('-')[0].Trim();
                    foreach (ContactType item in NewContact.PhoneType)
                    {
                        if (item.Type == "PHONE" && item.Code == phoneCode)
                        {
                            phoneDetail.TypeID = Convert.ToInt32(item.TypeID);
                            break;
                        }
                    }
                }
                else
                {
                    phoneDetail.TypeID = Convert.ToInt32(NewContact.SelectedPhoneType);
                }

                phoneDetail.IsDefault = false;

                customerContactItem.PhoneList = new List<Phone>();
                if (phoneDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.PhoneList.Add(phoneDetail);
                }
                customerContactItem.ContactID = NewContact.ContactID;
                customerContactItem.CustomerID = Customer.CustomerNo;
                contactManager.SaveEditedPhone(customerContactItem, phoneDetail.Index);
                GetCustomerContacts();
            });
            DeleteEmail = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContactItem = new Models.CustomerContact();
                Managers.ContactManager contactManager = new Managers.ContactManager();

                Email emailDetail = new Email();
                //emailDetail.Index = NewContact.Index;
                emailDetail.Value = NewContact.EmailID;
                if (string.IsNullOrEmpty(NewContact.SelectedEmailType))
                {
                    emailDetail.Type = "Email Address";
                }
                else
                {
                    emailDetail.Type = NewContact.SelectedEmailType;
                }

                if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.EmailList = new List<Email>();
                    customerContactItem.EmailList.Add(emailDetail);
                }
                customerContactItem.ContactID = NewContact.ContactID;
                customerContactItem.CustomerID = Customer.CustomerNo;

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteEmailConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                contactManager.DeleteContactEmail(customerContactItem, NewContact.Index);
                GetCustomerContacts();
            });

            DeletePhone = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContactItem = new Models.CustomerContact();
                Managers.ContactManager contactManager = new Managers.ContactManager();
                customerContactItem.CustomerID = Customer.CustomerNo;
                customerContactItem.ContactID = NewContact.ContactID;

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeletePhoneConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                contactManager.DeleteContactPhone(customerContactItem, NewContact.Index);
                GetCustomerContacts();
            });
            OpenPreOrderDialog = new DelegateCommand((param) =>
            {
                var dialog = new Helpers.DialogWindow { TemplateKey = "OpenPreOrderDialog", Title = "Create Pre-Order" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            MoveToPreOrder = new DelegateCommand((param) =>
            {
                PreOrderMockData POMD = param as PreOrderMockData;
                List<object> payload = new List<object>();
                payload.Add(POMD);
                payload.Add(Customer);
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pre-Order";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(Customer);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PreOrder, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, Payload = payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
        }
        void GetCustomerContact()
        {
            Managers.CustomerManager customerManager = new Managers.CustomerManager();
            Managers.ContactManager contactManager = new Managers.ContactManager();
            ObservableCollection<CustomerContact> customerContactCollection = contactManager.GetCustomerContacts(Customer.CustomerNo);
            //ObservableCollection<CustomerContact> customerContactCollection = customerManager.GetCustomerContact(Customer.CustomerNo);
            if (customerContactCollection.Count() == 0)
            {
                CustomerContact = new Models.CustomerContact
                {
                    //ContactName = "NA",
                    //ContactTitle = "NA",
                    //ContactName = "NA",
                    //Email = "NA",
                    //Phone = "NA",
                    //PhoneType = "NA",
                    //Title = "NA"
                };
            }
            else
            {
                CustomerContact = customerContactCollection.ElementAt(0);
            }
        }
        void GetCustomerContacts()
        {

            Managers.ContactManager contactManager = new Managers.ContactManager();
            ObservableCollection<CustomerContact> customerContactCollection = contactManager.GetCustomerContacts(Customer.CustomerNo);
            if (customerContactCollection.Count > 0)
            {
                if (CustomerContactList == null || CustomerContactList.Count == 0)
                {
                    CustomerContactList = new ObservableCollection<CustomerContact>();
                }
                else
                {
                    CustomerContactList.Clear();
                }
                //CustomerContactList.CollectionChanged += CustomerContactList_CollectionChanged;
                foreach (CustomerContact item in customerContactCollection)
                {
                    CustomerContactList.Add(item);
                }
            }
        }

        void CustomerContactList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (CustomerContact item in e.OldItems)
                {
                    item.PropertyChanged -= EntityViewModelPropertyChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (CustomerContact item in e.NewItems)
                {
                    item.PropertyChanged += EntityViewModelPropertyChanged;
                }
            }
        }
        public void EntityViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }
        #endregion

        public string Error
        {
            get { return ""; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "CheckSelectedIndex" && !(CheckSelectedIndex == -1))
                {
                    IsButtonEnabled = true;
                    return "";
                }
                else
                {
                    IsButtonEnabled = false;
                }
                return "";
            }
        }
    }
    public class PreOrderMockData
    {
        public string Stopdate { set; get; }
        public string Weekday { set; get; }
    }
}
