﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class UserLogin : ViewModelBase
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.UserLogin");

        string progressMessage, loginCaption;
        bool enableLogin;
        public DelegateCommand LoginUser { get; set; }
        public string LoginID { get; set; }
        public bool EnableLogin
        {
            get { return enableLogin; }
            set
            {
                enableLogin = value;
                OnPropertyChanged("enableLogin");
            }
        }
        public string LoginCaption
        {
            get { return loginCaption; }
            set
            {
                loginCaption = value;
                OnPropertyChanged("LoginCaption");
            }
        }
        public string Password
        {
            set;
            get;
        }
        public string DeviceID { get; set; }
        public bool IsValidUser { get; set; }
        public string Route { get; set; }

        bool isNetworkAvailable;
        public bool IsNetworkAvailable
        {
            get { return isNetworkAvailable; }
            set
            {
                isNetworkAvailable = value;
                IsNetworkOffline = !value;
                OnPropertyChanged("IsNetworkAvailable");
            }
        }
        bool isOffline;
        public bool IsNetworkOffline
        {
            get { return isOffline; }
            set
            {
                isOffline = value;
                OnPropertyChanged("IsNetworkOffline");
            }
        }
        public string ProgressMessage
        {
            get { return progressMessage; }
            set
            {
                progressMessage = value;
                OnPropertyChanged("ProgressMessage");
            }
        }
        public Guid MessageToken { get; set; }

        public UserLogin()
        {
        log4net.Config.XmlConfigurator.Configure();
            IsValidUser = true;
            EnableLogin = true;
            LoginCaption = "Login";
            CommonNavInfo.UserRoute = Route;
            InitializeCommands();
            //   SalesLogicExpress.Application.Helpers.ODBCManager.RemoveDSN("dsn_remote_FBM783");
        }
        void InitializeCommands()
        {
            IsNetworkAvailable = ResourceManager.NetworkInfo.IsInterNetConnected();
            ResourceManager.NetworkInfo.InternetStateChanged += NetworkInfo_InternetStateChanged;

            LoginUser = new DelegateCommand((param) =>
            {
                EnableLogin = false;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                var progressIndicator = new Progress<string>(ReportProgress);
                //TempLoginUserExecute(progressIndicator);
                Managers.UserManager.RouteEntered = Route;
                LoginUserExecute(progressIndicator);
               
            });

        }

        void NetworkInfo_InternetStateChanged(object sender, Managers.InternetStateChangedEventArgs e)
        {
            if (e.State == Managers.InternetState.Connected)
            {
                //System.Diagnostics.Debug.WriteLine("Internet Connected");
                IsNetworkAvailable = true;
            }
            else
            {
                //System.Diagnostics.Debug.WriteLine("Internet disonnected");
                IsNetworkAvailable = false;
            }
        }
        void ReportProgress(string value)
        {
            ProgressMessage = value;
        }

        async void LoginUserExecute(IProgress<string> progress)
        {
            bool isRouteDbPresent = false;
            bool isDeviceRegistered = true;
            await Task.Run(() =>
            {
                Managers.SetupManager setupManager = new Managers.SetupManager();
                bool userSynched = false, routeDataSynched = false;
                ResourceManager.CloseConnectionInstance();
                ResourceManager.ActiveDatabaseConnectionString = string.Format(setupManager.RouteDbConnectionString, Route);
                progress.Report("Checking database for route...");
                isRouteDbPresent = setupManager.IsRouteDatabasePresent(Route);
                Logger.Info("Route db existence checked " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                if (!isRouteDbPresent)
                {
                    if (!IsNetworkAvailable)
                    {
                        return;
                    }
                    progress.Report("Setting up database...");
                    setupManager.CreateRouteDatabase(Route);
                    Logger.Info("Route database created " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    progress.Report("Setting up remote");
                    setupManager.SetRemoteID(DeviceID, Route);
                    Logger.Info("Remote ID set in db " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    progress.Report("Registering device...");
                    setupManager.RegisterDeviceAsRemote(setupManager.RemoteID, DeviceID, Route);
                    Logger.Info("Registering device as remote " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    //}
                    progress.Report("Synchronizing users...");
                    userSynched = setupManager.SynchronizeUsers();
                    if (!userSynched)
                    {
                        ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.GetLatestUsers, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.everything);
                    }
                    Logger.Info("SynchronizeUsers " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    progress.Report("Validating user...");

                    //ToDo - To Be Uncommented before release -- Commented by arvind as Device ID was giving problem
                    //if (!setupManager.IsDeviceRegistered(DeviceID))
                    //{
                    //    isDeviceRegistered = false;
                    //    return;
                    //}
                }
                IsValidUser = setupManager.ValidateUser(LoginID, Password, Route, DeviceID);
                Logger.Info("Validated user login " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                progress.Report("User validated...");
                if (IsValidUser && !isRouteDbPresent)
                {
                    ResourceManager.ActiveRemoteID = Route;
                    if (!isRouteDbPresent)
                    {
                        progress.Report("Subscribing to publication...");
                        setupManager.SubscribeUserToPublication(Route, LoginID);
                        Logger.Info("Subscribe User To Publication " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    }
                    progress.Report("Synchronizing data for route...");
                   routeDataSynched = setupManager.SynchronizeDataForRoute();
                    if (!routeDataSynched)
                    {
                        ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.GetEverything, Managers.SyncQueueManager.Priority.everything);
                    }
                    // initialize the navigation header for the first view
                    ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.AddHours(-6).ToString("M'/'dd'/'yyyy' 'hh:mm") + DateTime.Now.AddHours(-6).ToString(" tt").ToLower());
                    ViewModels.CommonNavInfo.ShowBackNavigation = false;
                    ViewModels.CommonNavInfo.ViewTitle = "Customer Initial";
                }
                if (!isRouteDbPresent && !IsValidUser)
                {
                    progress.Report("Cleaning installed resources...");
                    ResourceManager.CloseConnectionInstance();
                    setupManager.CleanupInstalledDatabase(Route);
                }
                ViewModels.CommonNavInfo.UserName = LoginID;
                Managers.UserManager.UserId = Managers.UserManager.GetUserId(LoginID);

            });
            if (!IsNetworkAvailable && !isRouteDbPresent)
            {
                EnableLogin = true;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                ProgressMessage = string.Empty;
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "Internet connection required for initial setup.  \n Please connect to internet and try again.." }, MessageToken);
                return;
            }
            if (!isDeviceRegistered)
            {
                EnableLogin = true;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                ProgressMessage = string.Empty;
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "Device is not registered or active. \n Kindly contact IT Administration.\n Device ID : " + DeviceID }, MessageToken);
                
                return;
            }

            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.UserLogin, Payload = Route, CloseCurrentView = true, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            EnableLogin = true;
            LoginCaption = EnableLogin ? "Login" : "Logging in..";
        }

        async void TempLoginUserExecute(IProgress<string> progress)
        {
            await Task.Run(() =>
           {
               Managers.SetupManager setupManager = new Managers.SetupManager();
               Managers.CustomerManager customerManager = new Managers.CustomerManager();
               progress.Report("Getting data for customers...");
               ObservableCollection<Customer> customers = customerManager.GetCustomersForRoute(Route);
               Logger.Info(string.Concat("GetCustomersForRoute(", Route, ")", DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt")));
               Customer randomCustomer = customers.Where(s => s.CustomerNo == "1108911").FirstOrDefault() as Customer;

               // initialize the navigation header for the first view
               ViewModels.CommonNavInfo.SetSelectedCustomer(randomCustomer);
               ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.AddHours(-6).ToString("M'/'dd'/'yyyy' 'hh:mm") + DateTime.Now.AddHours(-6).ToString(" tt").ToLower());
               ViewModels.CommonNavInfo.ShowBackNavigation = false;
               ViewModels.CommonNavInfo.ViewTitle = "Order Template";
               ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
               ViewModels.CommonNavInfo.UserName = LoginID;
           });
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.UserLogin, Payload = Route, CloseCurrentView = true, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            EnableLogin = true;
            LoginCaption = EnableLogin ? "Login" : "Logging in..";

        }

    }
}
