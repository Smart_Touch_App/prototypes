﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using log4net;
using System.Threading;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ServiceRoute : ViewModelBase
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress");
        DateTime currentdate = DateTime.Today;
        CustomerManager CustList = new CustomerManager();

        public DelegateCommand CreateStop { get; set; }
        public DelegateCommand MoveStop { get; set; }
        public DelegateCommand ShowMoveStopDialog { get; set; }
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand ChangeDate { get; set; }
        public DelegateCommand ViewStopSequencing { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand ViewSelectionChanged { get; set; }
        public DelegateCommand CustomerSelectedCommand { get; set; }
        public DelegateCommand ShowCustomerHome { get; set; }
        public DelegateCommand OrderTemplate { get; set; }
        public Customer _SelectedItem = new Customer();
        public Customer SelectedItem
        {
            get
            {
                return _SelectedItem;
            }
            set
            {
                _SelectedItem = value;
            }
        }
        public Guid MessageToken { get; set; }
        public string Route { get; set; }
        string _SearchText;
        public string SearchText 
        {
            get { return _SearchText; }
            set { _SearchText = value; OnPropertyChanged("SearchText"); }
        }
        public string Day
        {
            get;
            set;
        }
        public string Date { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public ObservableCollection<Customer> _CustomerList = new ObservableCollection<Customer>();
        public ObservableCollection<Customer> CustomerList
        {
            get
            {
                return _CustomerList;
            }
            set
            {
                _CustomerList = value;
            }
        }
        public int TotalStops { get; set; }
        public int TotalStopsServiced { get; set; }
        public int TotalStopsRequired { get; set; }
        public int PendingTransactions { get; set; }
        public int ItemReturns { get; set; }
        string _DailyStopDisplayDate;
        public DateTime DailyStopDate;
        public string DailyStopDisplayDate
        {
            get
            {
                return _DailyStopDisplayDate;
            }
            set
            {
                _DailyStopDisplayDate = value;
                OnPropertyChanged("DailyStopDisplayDate");
            }
        }
        //*******************
        public int TotalCustomers { get; set; }
        public int TotalCustomersOnCreditHold { get; set; }
        public int ItemReturnsByCustomers { get; set; }
        public string CustomersDisplayDate { get; set; }
        public ServiceRoute(object payload)
        {
            Initialize();
            InitializeCommands();
            Route = payload.ToString();
            Day = currentdate.ToString("dddd") + ", ";
            Month = currentdate.ToString("MMMM") + "  ";
            Date = currentdate.Day.ToString() + ", ";
            Year = currentdate.Year.ToString() + " ";
            ViewModels.CommonNavInfo.UserRoute = Route;
            DailyStopDate = DateTime.Now;
            
            DailyStopDisplayDate = CustomersDisplayDate = DateTime.Now.ToString("dddd, MM/dd/yyyy");
            LoadDataStatistics();
        }

        private void LoadDataStatistics()
        {
            int serviced = CustomerList.Count(item => item.Activity == true);
            int total = CustomerList.Count();
            TotalCustomersOnCreditHold = 0;
            TotalStops = TotalCustomers = CustomerList.Count() > 0 ? CustomerList.Count() : 0;
            TotalStopsRequired = total-serviced;
            TotalStopsServiced = serviced;
            PendingTransactions = CustomerList.Where(item => item.PendingActivity>0).Sum(s=>s.PendingActivity);
            
        }

        public ServiceRoute()
        {
            // TODO: Complete member initialization
        }

        void InitializeCommands()
        {
            ShowCustomerHome = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    Customer cust = param as Customer;
                    //SetPayload(param);
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(cust);

                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = false, Payload = cust, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }

            });
            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchItemOnType.Execute(SearchText);
            });
            CustomerSelectedCommand = new DelegateCommand((param) =>
             {
                 if(param !=null){
                     Customer cust = param as Customer;
                     //SetPayload(param);
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(cust);

                     var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = false, Payload = SelectedItem, ShowAsModal = false };
                     Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                 } 
               
             });
            OrderTemplate = new DelegateCommand((param) =>
             {
                 if (param == null)
                 {
                     var alertMessage = new Helpers.AlertWindow { Message = "Please select a customer to proceed", MessageIcon = "Alert" };
                     Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                     return;
                 }
                 else
                 {
                     //SetPayload(param);
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Order Template";
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
                     SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);

                     var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = false, Payload = SelectedItem, ShowAsModal = false };
                     Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                     //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.OrderTemplate, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = false, Payload = SelectedItem, ShowAsModal = false };
                     //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                 }
             });
            CreateStop = new DelegateCommand((StopName) =>
            {

            });
            ViewSelectionChanged = new DelegateCommand((StopName) =>
            {

            });
            ShowMoveStopDialog = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    Customer cust = param as Customer;
                    SelectedItem = param as Customer;
                    var dialog = new Helpers.DialogWindow { TemplateKey = "MoveDailyStop", Title = "Move Stop" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                } 
            });
            MoveStop = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                }
            });
            CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {
                if (searchTerm == "")
                {
                    foreach (Customer cust in CustomerList)
                    {
                        cust.ItemVisible = cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                    }
                }
                Task.Delay(500, tokenForCancelTask.Token);
                if (searchTerm != null && searchTerm.ToString().Length < 3)
                {
                    return;
                }
                foreach (Customer cust in CustomerList)
                {
                    cust.ItemVisible = cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                }
            });
            ChangeDate = new DelegateCommand((Direction) =>
            {
                DailyStopDate = Direction.ToString().ToUpper().Equals("PREV") ? DailyStopDate.AddDays(-1) : DailyStopDate.AddDays(1);
                DailyStopDisplayDate = DailyStopDate.ToString("dddd, MM/dd/yyyy");
            });
            ViewStopSequencing = new DelegateCommand((param) =>
            {

            });
        }

        private void SetPayload(object param)
        {
            SelectedItem = (Customer)param;
        }
        private void Initialize()
        {
            LoadCustomers();
            InitializeNavigationHeaders();

        }
        public void LoadCustomers() {
            CustomerList = CustList.GetCustomersForRoute("FBM783");
        }
        private void InitializeNavigationHeaders()
        {
        }




    }

    public class CollectionData
    {
        public CollectionData(string a, int b)
        {
            OrderState = a;
            OrderCount = b;
        }
        public string OrderState { set; get; }
        public int OrderCount { get; set; }
    }
}
