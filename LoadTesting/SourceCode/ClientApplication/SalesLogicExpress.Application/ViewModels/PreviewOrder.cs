﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Windows.Ink;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Markup;
using System.Windows.Controls;
using SalesLogicExpress.Domain.Vertex;
using log4net;


namespace SalesLogicExpress.Application.ViewModels
{
    public class PreviewOrder : ViewModelBase, IDataErrorInfo
    {
        public PreviewOrder(object payload)
        {
            // TODO: Complete member initialization
            this.payload = payload;
            OrderId = Order.OrderId;
            _SignStroke = new StrokeCollection(); 
            if (GlobalInkCanvas != null)
            {
                if (GlobalInkCanvas.Strokes.Count > 0)
                {
                    SignStroke.Add(GlobalInkCanvas.Strokes);
                
                }
            }
            InitializeCommands();
            IsBusy = true;
            LoadData();
        }

        async void LoadData() {
            await Task.Run(() => {
                ObservableCollection<Models.OrderItem> PreviewOrderCollection = (ObservableCollection<Models.OrderItem>)payload;
                PreviewItems = PreviewOrderCollection;
                CalculateTax();
                CalulateTotal(PreviewOrderCollection);
                IsBusy = false;
            });
        }
        public PreviewOrder()
        {
            InitializeCommands();
        }

        // Public Var's

        #region Command Declarations
        public DelegateCommand UpdateAllTotals { get; set; }
        public DelegateCommand PickOrder { get; set; }
        public DelegateCommand SaveSign { get; set; }
        public DelegateCommand ClearCanvas { get; set; }
        public DelegateCommand CompleteAndSign { get; set; }
        public DelegateCommand ResetText { get; set; }
        public DelegateCommand GetReasonCode { get; set; }
        #endregion
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress");

        string strDateTime = DateTime.Now.ToString("M'/'dd'/'yyyy' - 'hh:mm ") + DateTime.Now.ToString("tt").ToLower();
        private object payload;
        private bool _ComboBoxItemVisibility = false;
        private bool _ComboBoxIsEnabled = false;
        private int _ComboBoxSelectedIndex = 0;
        private bool _ToggleUpdateButton = false;
        private string _SurchargeReasonCode;
        private string _previewTextBoxText = "0.00";
        private string _ErrorText = String.Empty;
        private StrokeCollection _SignStroke = null;
        private int _OrderId;
        private ObservableCollection<Models.OrderItem> previewOrder = new ObservableCollection<Models.OrderItem>();
        bool ResetTextEnable = true;
        bool IsEmptyAllowed = true;
        # region Properties
        public int OrderId
        {
            get { return Order.OrderId; }
            set { _OrderId = value; }
        }
        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");
            }
        }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public Guid MessageToken { get; set; }
        public bool ComboBoxItemVisibility
        {
            get { return _ComboBoxItemVisibility; }
            set { _ComboBoxItemVisibility = value; OnPropertyChanged("ComboBoxItemVisibility"); }
        }
        public string CurrentDate
        {
            get { return strDateTime; }
        }
        public bool ComboBoxIsEnabled
        {
            get { return _ComboBoxIsEnabled; }
            set
            {
                _ComboBoxIsEnabled = value;
                OnPropertyChanged("ComboBoxIsEnabled");
            }

        }


        public int ComboBoxSelectedIndex
        {
            get { return _ComboBoxSelectedIndex; }
            set { _ComboBoxSelectedIndex = value; OnPropertyChanged("ComboBoxSelectedIndex"); }
        }


        public bool ToggleUpdateButton
        {
            get { return _ToggleUpdateButton; }
            set { _ToggleUpdateButton = value; OnPropertyChanged("ToggleUpdateButton"); }
        }

        public string SurchargeReasonCode
        {
            get { return _SurchargeReasonCode; }
            set
            {
                _SurchargeReasonCode = value;
            }
        }
        public string ErrorText
        {
            get { return _ErrorText; }
            set { _ErrorText = value; OnPropertyChanged("ErrorText"); }
        }

        public string PreviewInputText
        {
            get { return _previewTextBoxText; }
            set
            {
                _previewTextBoxText = value;

                OnPropertyChanged("PreviewInputText");
            }
        }

        private bool _ToggleSignPanelVisibility = false;
        public bool ToggleSignPanelVisibility
        {
            get
            {
                return _ToggleSignPanelVisibility;

            }
            set
            {
                _ToggleSignPanelVisibility = value;
                OnPropertyChanged("ToggleSignPanelVisibility");
            }
        }

        // OrderItem Collection for PreviewOrder Grid
        // public ObservableCollection<Models.TemplateItem> PreviewItems
        public ObservableCollection<Models.OrderItem> PreviewItems
        {
            get
            {

                ///  this.previewitems = new Managers.TemplateManager().GetTemplateItemsForCustomer("1108911");
                return previewOrder;
            }
            set
            {
                previewOrder = value;
                OnPropertyChanged("PreviewItems");
            }
        }

        #endregion

        # region Properties For Textbox Values

        float varTotalCoffeeValue, varTotalAlliedValue, varOrderTotalValue, varEnergySurchargeValue, varSalesTaxValue, varInvoiceTotalValue;
        private string _TotalCoffeeValue = "0.00", _TotalAlliedValue = "0.00",
                       _OrderTotalValue = "0.00", _EnergySurchargeValue = "0.00",
                       _SalesTaxValue = "0.00", _InvoiceTotalValue = "0.00";
        public string TotalCoffeeValue
        {
            get
            {
                return "$" + _TotalCoffeeValue;
            }
            set
            {
                _TotalCoffeeValue = value;
                OnPropertyChanged("TotalCoffeeValue");
            }
        }

        public string TotalAlliedValue
        {
            get { return "$" + _TotalAlliedValue; }
            set { _TotalAlliedValue = value; OnPropertyChanged("TotalAlliedValue"); }
        }

        public string OrderTotalValue
        {
            get { return "$" + _OrderTotalValue; }
            set
            {
                _OrderTotalValue = value;
                OnPropertyChanged("OrderTotalValue");
            }
        }

        public string EnergySurchargeValue
        {
            get { return "$" + _EnergySurchargeValue; }
            set
            {
                _EnergySurchargeValue = value;
                OnPropertyChanged("EnergySurchargeValue");
            }
        }
        public string SalesTaxValue
        {
            get { return "$" + _SalesTaxValue; }
            set { _SalesTaxValue = value; OnPropertyChanged("SalesTaxValue"); }
        }
        public string InvoiceTotalValue
        {
            get { return "$" + _InvoiceTotalValue; }
            set { _InvoiceTotalValue = value; OnPropertyChanged("InvoiceTotalValue"); }
        }

        #endregion


        # region Validations




        public string Error
        {
            get { return ErrorText; }
        }

        public string this[string columnName]
        {

            get
            {


                double value;
                double.TryParse(PreviewInputText.ToString(), out value);
                //Check for multiple decimal points
                if (!double.TryParse(PreviewInputText.ToString(), out value) && !(PreviewInputText == ""))
                {
                    ErrorText = "Please enter amount in format : 999.99";
                    ComboBoxIsEnabled = false;
                    ToggleUpdateButton = false;
                }
                // Check for default value and value greater than 1000
                else if (columnName == "PreviewInputText" && PreviewInputText != "0.00" && value >= 1000)
                {
                    ErrorText = "Please enter value less than $1000";
                    ComboBoxIsEnabled = false;
                    ToggleUpdateButton = false;
                }
                // Check for empty value
                else if (columnName == "PreviewInputText" && PreviewInputText == "")
                {
                    if (!IsEmptyAllowed)
                    {
                        ErrorText = "Value cannot be empty";
                        ComboBoxIsEnabled = false;
                        ToggleUpdateButton = false;
                    }
                }
                // if value is correct
                else
                {
                    ErrorText = "";
                    ComboBoxIsEnabled = false;
                    ToggleUpdateButton = false;
                    if (!(value == 0))
                    {
                        ComboBoxIsEnabled = true;
                        //Check for reason code 
                        if (columnName == "ComboBoxSelectedIndex" && ComboBoxSelectedIndex == 0)
                        {
                            ErrorText = Constants.OrderPreview.SelectReasonForSurchargeAlert;
                            ToggleUpdateButton = false;
                        }
                        // Check for if user select default reason code text
                        else if (columnName == "ComboBoxSelectedIndex" || columnName == "PreviewInputText" && ComboBoxSelectedIndex != 0)
                        {
                            ToggleUpdateButton = true;
                            ComboBoxItemVisibility = true;
                        }

                    }



                }

                return ErrorText;

            }
        }
        # endregion

        #region methods
        // Synchronize to calculate values on right panel
        void CalulateTotal(ObservableCollection<Models.OrderItem> orderitem)
        {

            foreach (Models.OrderItem order in orderitem)
            {

                varOrderTotalValue = varOrderTotalValue + float.Parse(order.ExtendedPrice.ToString());
                if (order.SalesCat1.Equals("COF"))
                {
                    varTotalCoffeeValue = varTotalCoffeeValue + float.Parse(order.ExtendedPrice.ToString());

                }
                if (order.SalesCat1.Equals("ALL"))
                {
                    varTotalAlliedValue = varTotalAlliedValue + float.Parse(order.ExtendedPrice.ToString());

                }
            }
            TotalCoffeeValue = varTotalCoffeeValue.ToString("0.00");
            TotalAlliedValue = varTotalAlliedValue.ToString("0.00");
            OrderTotalValue = varOrderTotalValue.ToString("0.00");
            varInvoiceTotalValue = (varOrderTotalValue + varEnergySurchargeValue + varSalesTaxValue);
            InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");

        }

        // Synchronize to update totals after updating surcharge value
        private void UpdateTotals()
        {

            if (ComboBoxSelectedIndex != 0)
            {
                varEnergySurchargeValue = float.Parse(PreviewInputText.ToString());
                EnergySurchargeValue = varEnergySurchargeValue.ToString("0.00");
                varInvoiceTotalValue = (varOrderTotalValue + varEnergySurchargeValue + varSalesTaxValue);
                InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");
                PreviewInputText = varEnergySurchargeValue.ToString("0.00");

            }

        }
        private void CalculateTax()
        {
            InvoiceObject objInvoice = new InvoiceObject();

            //Calculate tax for current invoice.
            string custClasscode = DbEngine.ExecuteScalar("select AIAC11 from BUSDTA.F03012 where AIAN8 =" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
            string geocode = DbEngine.ExecuteScalar("select AITXA1 from BUSDTA.F03012 where AIAN8 =" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
            long _geocode = Convert.ToInt32(geocode.Substring(1));
            objInvoice.Customer = SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo;
            objInvoice.CustomerClassCode = custClasscode;
            objInvoice.GeoCode = _geocode;
            objInvoice.InvoiceNumber = OrderId.ToString();
            objInvoice.ProductSetCode = "";
            foreach (Models.OrderItem item in PreviewItems)
            {
                string opsetcode = DbEngine.ExecuteScalar("select IBPRP0 from BUSDTA.F4102 where IBLITM =" + "'" + item.ItemNumber + "'");
                string setcode = opsetcode.Trim();
                objInvoice.InvoiceItemList.Add(new InvoiceItem(setcode, item));

            }
            bool bTaxComputed = false;
            try
            {
                Managers.TaxManager taxManager = new Managers.TaxManager();
                bTaxComputed = taxManager.CalculateTax(ref objInvoice);
            }
            catch (FileNotFoundException ex)
            {
                bTaxComputed = false;
                Logger.Debug("Tax manager FileNotFoundException: ",ex);
            }
            catch (Exception ex)
            {
                Logger.Debug("Tax manager Exception: ", ex);
            }
            if (bTaxComputed)
            {
                try
                {
                    Logger.Info("Return Value TaxCalulated() :" + bTaxComputed);
                    varSalesTaxValue = (float)objInvoice.InvoiceTax;
                    SalesTaxValue = varSalesTaxValue.ToString("0.00");
                    OnPropertyChanged("SalesTaxValue");

                    for (int index = 0; index < objInvoice.InvoiceItemList.Count; index++)
                    {
                        string itemNumber = (objInvoice.InvoiceItemList[index].ItemNumber.Trim());
                        if ((objInvoice.InvoiceItemList[index].ItemTax != 0))
                        {
                            Logger.Info("Before Updating IsTaxable value to 1");
                            PreviewItems[index].IsTaxable = true;
                            string updateTaxStatus = "update BUSDTA.Order_Detail set IsTaxable =1 where Order_ID=" + "'" + OrderId + "'" + " and Item_Number=" + "'" + itemNumber + "'";
                            DbEngine.ExecuteNonQuery(updateTaxStatus);
                            Logger.Info("After Updating IsTaxable value to 1");
                        }
                        else
                        {
                            Logger.Info("Before Updating IsTaxable value to 0");
                            string updateTaxStatus = "update BUSDTA.Order_Detail set IsTaxable =0 where Order_ID=" + "'" + OrderId + "'" + " and Item_Number=" + "'" + itemNumber + "'";
                            DbEngine.ExecuteNonQuery(updateTaxStatus);
                            PreviewItems[index].IsTaxable = false;
                            Logger.Info("After Updating IsTaxable value to 1");
                        }
                    }
                }
                catch (Exception ex)
                {
                    var Alert = new AlertWindow { Message = "Tax Routine Failed" };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    Logger.Debug("File: ProviewOrder.cs Method:CalculateTax()  Failed" + ex.InnerException.Message);

                }
            }
            else
            {
                // TODO: ONLY SHOW DURING DEVELOPMENT STAGE
            }

        }
        #endregion
        private void InitializeCommands()
        {


            UpdateAllTotals = new DelegateCommand((param) =>
            {
                UpdateTotals();
            });


            PickOrder = new DelegateCommand((param) =>
            {
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.PreviewOrder, CloseCurrentView = false, Payload = PreviewItems, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            SaveSign = new DelegateCommand((param) =>
            {
                Logger.Info("PreviewOrder SaveSign Enter");

                try
                {
                    GlobalInkCanvas = InkCanvas;
                    RenderTargetBitmap rtb = new RenderTargetBitmap((int)canvasWidth - 10, (int)canvasHeight, 0, 0, PixelFormats.Pbgra32);
                    rtb.Render(InkCanvas);
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();


                    encoder.Frames.Add(BitmapFrame.Create(rtb));

                    FileStream file = new FileStream("test.jpg", FileMode.Create);

                    encoder.Save(file);
                    file.Close();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                    double totalcoffee = Math.Round(varTotalCoffeeValue, 4);
                    double totalallied = Math.Round(varTotalAlliedValue, 4);
                    double energysurcharge = Math.Round(varEnergySurchargeValue, 4);
                    double ordertotal = Math.Round(varOrderTotalValue, 4);
                    double salestax = Math.Round(varSalesTaxValue, 4);
                    double invoiceTotal = Math.Round(varInvoiceTotalValue, 4);

                    string query = "update busdta.Order_Header set Total_Coffee =" + totalcoffee + ", Total_Allied =" + totalallied +
                                    ", Energy_Surcharge =" + energysurcharge + ", Order_Total_Amt =" + ordertotal + ", Sales_Tax_Amt = " + salestax +
                                    ", Invoice_Total =" + invoiceTotal + ", Surcharge_Reason_Code =" + "'" + SurchargeReasonCode + "'" + "where Order_ID =" + Order.OrderId;
                    Logger.Info("Preview Order update busdta.Order_Header");
                    DbEngine.ExecuteNonQuery(query);
                    Logger.Info("Preview Order update busdta.Order_Header complete");

                    Logger.Info("Preview Order UpdateOrderStatus");
                    Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.OrderAcceptance, Managers.OrderManager.OrderSubState.None);
                    Logger.Info("Preview Order UpdateOrderStatus exit");

                    ResourceManager.Transaction.AddTransactionInQueueForSync(SalesLogicExpress.Application.Managers.Transaction.AcceptOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);

                    Logger.Info("Preview Order Navigation Initiate");

                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.PreviewOrder, CloseCurrentView = false, Payload = PreviewItems, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);



                }
                catch (Exception ex)
                {
                    Logger.Debug("File: PreviewOrder.cs Method:SaveSign Command" + ex.InnerException.ToString());
                }
            });
            ClearCanvas = new DelegateCommand((param) =>
            {
                SignStroke.Clear();
                base.OnPropertyChanged("SignStroke");
            });
            CompleteAndSign = new DelegateCommand((param) =>
            {
                if (!ToggleSignPanelVisibility)
                    ToggleSignPanelVisibility = true;
                else
                    ToggleSignPanelVisibility = false;
            });
            ResetText = new DelegateCommand((param) =>
            {
                if (ResetTextEnable)
                {
                    PreviewInputText = "";
                    ResetTextEnable = false;
                    IsEmptyAllowed = false;
                }
            });
            GetReasonCode = new DelegateCommand((param) =>
            {

                SurchargeReasonCode = param.ToString();
            });
        }


        public double canvasWidth { get; set; }

        public double canvasHeight { get; set; }

        public InkCanvas InkCanvas { get; set; }
        public static InkCanvas GlobalInkCanvas { get; set; }

    }
}
