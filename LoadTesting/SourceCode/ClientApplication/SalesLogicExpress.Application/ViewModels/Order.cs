﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using Telerik.Windows.Controls;
using Models = SalesLogicExpress.Domain;
using System.Data;
using System.Collections.Generic;
using SalesLogicExpress.Application.Managers;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class Order : ViewModelBase, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.Order");

        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public DateTime StateChangedTime { get; set; }
        }

        private object payload;
        public ReasonCode reasonCode;
        Managers.ItemManager itemManager;
        Managers.OrderManager orderManager;
        public DataTable _OrderHistory;
        public DataTable _OrderHistoryHeaders;
        public ObservableCollection<OpenOrder> _AllOpenOrdersForItem;
        public static bool DialogCancelled = false;
        private ObservableCollection<Item> _SuggestedItemList;
        private ObservableCollection<SalesSummary> _SalesSummaryList;

        public DelegateCommand GetOpenOrdersForItem { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand HoldOrder { get; set; }
        public DelegateCommand AddSearchItemsToOrder { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand SetExtendedPrice { get; set; }
        public DelegateCommand AddSuggestedItemsToOrder { get; set; }
        public DelegateCommand SearchResultTextChange { set; get; }

        public DelegateCommand SearchItem { get; set; }

        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand PreviewOrder { get; set; }
        public DelegateCommand DeleteItemFromOrder { get; set; }
        public DelegateCommand OpenExpander { set; get; }
        public ObservableCollection<Item> _SearchItems = new ObservableCollection<Item>();

        private TrulyObservableCollection<Models.OrderItem> _OrderItems;

        private bool _SearchResultTextVisibility;
        private bool _ToggleExpander = false;
        public bool ToggleExpander
        {
            get { return _ToggleExpander; }
            set
            {
                _ToggleExpander = value;
                OnPropertyChanged("ToggleExpander");
            }
        }
        private bool _IsBusy;
        private bool _EnablePreview;
        private static int _OrderId;
        private static int _VoidOrderReasonId;
        private static OrderManager.OrderState _OrderStatus;
        private static OrderManager.OrderSubState _OrderSubStatus;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public bool EnablePreview
        {
            get
            {
                return _EnablePreview;
            }
            set
            {
                _EnablePreview = value;
                OnPropertyChanged("EnablePreview");
            }
        }
        private string _SearchResultText;
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Item(s) found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }

        private int _SearchItemsCount;

        private string _ErrorText;



        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }
        string _SearchText;
        public string SearchText
        {
            get { return _SearchText; }
            set { _SearchText = value; OnPropertyChanged("SearchText"); }
        }
        public string ParentCode { get; set; }
        public string HoldCode { get; set; }
        public string SoldTo { get; set; }

        public string ErrorText
        {
            get
            {
                return _ErrorText;
            }
            set
            {
                _ErrorText = value;
                OnPropertyChanged("ErrorText");
            }
        }

        public static int VoidOrderReasonId
        {
            get { return _VoidOrderReasonId; }
            set { _VoidOrderReasonId = value; }
        }

        public static int OrderId
        {
            get { return _OrderId; }
            set { _OrderId = value; }
        }

        public static OrderManager.OrderState OrderStatus
        {
            get
            {
                return _OrderStatus;
            }
            set
            {
                _OrderStatus = value;
            }
        }
        public static OrderManager.OrderSubState OrderSubStatus
        {
            get
            {
                return _OrderSubStatus;
            }
            set
            {
                _OrderSubStatus = value;
            }
        }

        public ObservableCollection<OpenOrder> AllOpenOrdersForItem
        {
            get { return _AllOpenOrdersForItem; }
            set
            {
                _AllOpenOrdersForItem = value;
                OnPropertyChanged("AllOpenOrdersForItem");
            }
        }

        public DataTable OrderHistory
        {
            get { return _OrderHistory; }
            set { _OrderHistory = value; OnPropertyChanged("OrderHistory"); }
        }
        public DataTable OrderHistoryHeaders
        {
            get { return _OrderHistoryHeaders; }
            set { _OrderHistoryHeaders = value; OnPropertyChanged("OrderHistoryHeaders"); }
        }

        public TrulyObservableCollection<Models.OrderItem> OrderItems
        {
            get
            {
                return this._OrderItems;
            }
            set
            {
                _OrderItems = value;
                OnPropertyChanged("OrderItems");

            }
        }
        public ObservableCollection<Models.Item> SearchItemList
        {
            get
            {
                return this._SearchItems;
            }
            set
            {
                this._SearchItems = value;
                OnPropertyChanged("SearchItemList");
                this.OnPropertyChanged("SearchItemsCount");
                this.OnPropertyChanged("SearchResultText");
            }
        }

        public ObservableCollection<SalesSummary> SalesSummaryList
        {
            get
            {
                _SalesSummaryList = new ObservableCollection<SalesSummary>();
                _SalesSummaryList.Add(new SalesSummary { Parameter = "Amnt. Collected ($)", Regular = 3000 });
                _SalesSummaryList.Add(new SalesSummary { Parameter = "Sold Coffee (Lbs)", Regular = 110, Promo = 150 });
                _SalesSummaryList.Add(new SalesSummary { Parameter = "Sold Allied ($)", Regular = 150, Promo = 160 });
                return _SalesSummaryList;

            }
            set
            {
                this._SalesSummaryList = value;
                OnPropertyChanged("SalesSummaryList");
            }
        }

        public ObservableCollection<Models.Item> SuggestedItemList
        {
            get
            {
                return this._SuggestedItemList;
            }
            set
            {
                this._SuggestedItemList = value;
                OnPropertyChanged("SuggestedItemList");
            }
        }
        public Guid MessageToken { get; set; }


        public Order()
        {
            InitializeCommands();
        }

        public Order(object payload)
        {
            try
            {
                this.payload = payload;
                IsBusy = true;
                reasonCode = new ReasonCode(string.Empty);
                itemManager = new Managers.ItemManager();
                orderManager = new Managers.OrderManager();
                InitializeCommands();
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.Error("Error: Order(object payload)" + ex.StackTrace.ToString());
            }
        }
        async void LoadData()
        {
            await Task.Run(() =>
            {
                try
                {
                    TrulyObservableCollection<Models.OrderItem> OrderCollection = (TrulyObservableCollection<Models.OrderItem>)this.payload;
                    OrderItems = OrderCollection;
                    EnablePreview = OrderItems.Count == 0 ? false : true;
                    OrderId = Order.OrderId;
                    GetOrderHistory();
                    GetSuggestedItems();
                    GetSalesHeader();
                    OrderItems.CollectionChanged += OrderItems_CollectionChanged;
                    OrderItems.ItemPropertyChanged += OrderItems_ItemPropertyChanged;
                    //**********************************************************************************************************
                    ModelChangeArgs args = new ModelChangeArgs();
                    args.StateChangedTime = DateTime.Now;
                    OnModelChanged(args);
                    //**********************************************************************************************************
                    if (this._SearchItems != null)
                        _SearchItemsCount = this._SearchItems.Count;
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    IsBusy = false;
                    throw;
                }
            });
        }
        private void OrderItems_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {


        }

        private void OrderItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //  ViewModels.PreviewOrder.GlobalInkCanvas = new System.Windows.Controls.InkCanvas();

        }

        public void GetSalesHeader()
        {
            try
            {
                ParentCode = orderManager.GetParent(Convert.ToInt32(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo));//get parent
                HoldCode = orderManager.GetHoldCode(SalesLogicExpress.Application.ViewModels.CommonNavInfo.UserBranch);//get HoldCode
                SoldTo = orderManager.GetSoldTo(Convert.ToInt32(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo));
                //If Parent is null, Show SoldTo
                if (string.IsNullOrEmpty(ParentCode))
                {
                    ParentCode = SoldTo;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error:GetSalesHeader" + ex.StackTrace.ToString());
                throw;
            }
        }

        public class SalesHeader
        {
            public SalesHeader(string orderNumber, string orderDate)
            {
                OrderNumber = orderNumber;
                OrderDate = orderDate;
            }
            public string OrderNumber;
            public string OrderDate;
        }
        public class SalesSummary
        {
            public string Parameter { get; set; }
            public int Regular { get; set; }
            public int Promo { get; set; }

        }
        private void GetOrderHistory()
        {
            try
            {
                Managers.OrderManager orderManager = new Managers.OrderManager();
                DataSet RowsAndHeaders = orderManager.GetOrderHistoryForCustomer(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                OrderHistory = RowsAndHeaders.Tables[0];
                OrderHistoryHeaders = RowsAndHeaders.Tables[1];
            }
            catch (Exception ex)
            {
                Logger.Error("Error:GetOrderHistory" + ex.StackTrace.ToString());
                throw;
            }
        }

        private void GetSuggestedItems()
        {
            try
            {
                SuggestedItemList = new ObservableCollection<Item>();
                SuggestedItemList = itemManager.GetSuuggestedItems(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
            }
            catch (Exception ex)
            {
                Logger.Error("Error:GetSuggestedItems" + ex.StackTrace.ToString());
                throw;
            }
        }

        public decimal GetExtentedPrice(decimal Qty, decimal UnitPrice, string PriceUM, string AppliedUM, string ItemNo)
        {
            decimal ExtendedPrice = -1;
            try
            {
                if (PriceUM == AppliedUM)
                {
                    ExtendedPrice = Qty * UnitPrice;
                }
                else
                {
                    Managers.PricingManager pricingManager = new Managers.PricingManager();

                    decimal factor = decimal.Parse(pricingManager.jdeUOMConversion(PriceUM, AppliedUM, Int32.Parse(ItemNo)).ToString());

                    ExtendedPrice = Qty * UnitPrice * factor;

                }

            }
            catch (Exception ex)
            {


            }
            return ExtendedPrice;
        }

        public bool IsValidUM(string AppliedUM, string ItemNo, string PriceUM)
        {
            bool result = false;
            try
            {
                Managers.PricingManager pricingManager = new Managers.PricingManager();

                if (orderManager.ValidateUM(ItemNo).Contains(AppliedUM) && decimal.Parse(pricingManager.jdeUOMConversion(PriceUM, AppliedUM, Int32.Parse(ItemNo)).ToString()) >= 0)
                    result = true;

            }
            catch (Exception ex)
            {


            }
            return result;
        }
        public double AveragerQty(string itemNumber)
        {

            double avgQty = 0;

            foreach (DataRow dr in OrderHistory.Rows)
            {
                if (dr["ItemCode"].ToString() == itemNumber)
                {
                    avgQty = Convert.ToDouble(dr["AverageQty"]);
                    break;
                }
                else
                {
                    avgQty = 0;
                }
            }
            return avgQty;
        }
        public ObservableCollection<OpenOrder> GetAllOpenOrdersForItem(string ItemNumber)
        {
            ObservableCollection<OpenOrder> openOrders = new ObservableCollection<OpenOrder>();

            DataTable dt = orderManager.GetAllOpenOrdersForItem(ItemNumber);

            OpenOrder openOrder;

            foreach (DataRow item in dt.Rows)
            {
                openOrder = new OpenOrder();
                openOrder.CustomerId = item["customer_id"].ToString();
                openOrder.CustomerName = item["CustomerName"].ToString();
                openOrder.OrderId = item["Order_id"].ToString();
                openOrder.OrderQty = Convert.ToInt32(item["Order_Qty_Primary_UOM"].ToString());
                openOrder.PickQty = Convert.ToInt32(item["Picked_Qty_Primary_UOM"].ToString());
                openOrder.PrimaryUM = item["Primary_UOM"].ToString();
                openOrders.Add(openOrder);
            }


            return openOrders;

        }
        private void InitializeCommands()
        {
            Random qtyOnHand = new Random();
            VoidOrder = new DelegateCommand((param) =>
            {
                //reasonCode = param as ReasonCode;

                if (reasonCode != null)
                    VoidOrderReasonId = reasonCode.Id;

                // Set reason for void Order if not available 
                if (VoidOrderReasonId == 0)
                {
                    //ReasonCode r = new ReasonCode("");
                    reasonCode.ParentViewModel = this;
                    reasonCode.MessageToken = MessageToken;
                    var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = reasonCode };
                    Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                    DialogCancelled = OpenDialog.Cancelled;

                    return;
                }
                else
                {
                    // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0
                    VoidOrderReasonId = 0;
                }

            });

            HoldOrder = new DelegateCommand((e) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed || confirmMessage.Cancelled) return;

                if (confirmMessage.Confirmed)
                {
                    OrderStateChangeArgs arg = new OrderStateChangeArgs();
                    arg.State = OrderState.Hold;

                    reasonCode.OnStateChanged(arg);
                }

                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.OrderEntry, Managers.OrderManager.OrderSubState.Hold);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.Order, CloseCurrentView = true, ShowAsModal = false, Payload = CommonNavInfo.Customer };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            GetOpenOrdersForItem = new DelegateCommand((param) =>
           {

               Models.OrderItem OrderItem = param as Models.OrderItem;

               AllOpenOrdersForItem = GetAllOpenOrdersForItem(OrderItem.ItemNumber);

               if (AllOpenOrdersForItem.Count > 0)
               {
                   var dialog = new Helpers.DialogWindow { TemplateKey = "AllOpenOrdersForItem", Title = "Open Orders for " + OrderItem.ItemNumber.Trim() + ", " + OrderItem.ItemDescription, Payload = null };
                   Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
               }
               else
               {
                   var alertWindow = new Helpers.AlertWindow { Message = "There is no any open Order for this item" };
                   Messenger.Default.Send<Helpers.AlertWindow>(alertWindow, MessageToken);
               }

           });
            SetExtendedPrice = new DelegateCommand((selectedItem) =>
            {
                OrderItem orderItem = selectedItem as OrderItem;

                if (orderItem.UM != orderItem.UMPrice)
                {
                    if (!IsValidUM(orderItem.UM, orderItem.ShortDesc, orderItem.UMPrice))
                    {
                        Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "This is not a valid UM" }, MessageToken);
                        orderItem.UM = orderItem.UMPrice;
                        return;
                    }
                    else
                    {
                        //Switch UM 
                        orderItem.ExtendedPrice = GetExtentedPrice(orderItem.OrderQty, orderItem.UnitPrice, orderItem.UM, orderItem.UMPrice, orderItem.ShortDesc);
                        return;
                    }
                }

                //Update On hand Qty
                if ((orderItem.ActualQtyOnHand - orderItem.OrderQty) < 0)
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Order quantity exceeds On hand quantity" }, MessageToken);
                }
                orderItem.QtyOnHand = (orderItem.ActualQtyOnHand - orderItem.OrderQty) < 0 ? 0 : (orderItem.ActualQtyOnHand - orderItem.OrderQty);


                //update extended price
                orderItem.ExtendedPrice = GetExtentedPrice(orderItem.OrderQty, orderItem.UnitPrice, orderItem.UMPrice, orderItem.UM, orderItem.ShortDesc);

            });

            SearchResultTextChange = new DelegateCommand((param) =>
            {

                SearchItemsCount = Convert.ToInt32(param);
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            });


            // Add item from search to template list
            AddSearchItemsToOrder = new DelegateCommand((Items) =>
            {
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                int selectionCount = selectedItems.Count;

                for (int index = 0; index < selectionCount; index++)
                {
                    //Check for Existing Item in Order list
                    if (OrderItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                    {
                        continue;
                    }

                    Managers.OrderManager ordermanager = new Managers.OrderManager();
                    Models.OrderItem addedItem = ordermanager.ApplyPricingToItem(selectedItems[index] as Models.Item, SalesLogicExpress.Application.Managers.UserManager.UserBranch, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                    SearchItemList.Remove(selectedItems[index] as Models.Item);

                    addedItem.ActualQtyOnHand = qtyOnHand.Next(5, 50);
                    string itemNumber = addedItem.ItemNumber;
                    addedItem.AverageStopQty = AveragerQty(itemNumber);  //returning avg value    
                    // OrderItem order = new OrderItem(new Item());
                    addedItem.AppliedUMS = itemManager.GetAppliedUMs(addedItem.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(addedItem.ItemNumber) : new List<string>() { addedItem.UM };


                    OrderItems.Insert(0, addedItem);
                    EnablePreview = OrderItems.Count == 0 ? false : true;
                    index--;
                    selectionCount--;
                    SearchItemsCount = SearchItemList.Count;
                    OnPropertyChanged("SearchItemsCount");
                    OnPropertyChanged("SearchResultText");
                }
                OnPropertyChanged("SearchItemList");
                OnPropertyChanged("OrderItems");
                ViewModels.PreviewOrder.GlobalInkCanvas = new System.Windows.Controls.InkCanvas();

            });

            AddSuggestedItemsToOrder = new DelegateCommand((Items) =>
           {
               ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
               if (selectedItems == null || selectedItems.Count == 0)
               {
                   var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                   Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                   return;
               }
               int selectionCount = selectedItems.Count;


               for (int index = 0; index < selectionCount; index++)
               {

                   //Check for Existing Item in Order list
                   if (OrderItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                   {
                       continue;
                   }

                   Managers.OrderManager ordermanager = new Managers.OrderManager();
                   Models.OrderItem addedItem = ordermanager.ApplyPricingToItem(selectedItems[index] as Models.Item, SalesLogicExpress.Application.Managers.UserManager.UserBranch, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                   SuggestedItemList.Remove(selectedItems[index] as Models.Item);
                   OrderItems.Insert(0, addedItem);
                   index--;
                   selectionCount--;
               }
               OnPropertyChanged("SuggestedItemList");
               OnPropertyChanged("OrderItems");
           });

            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchItemOnType.Execute(SearchText);
            });
            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), false);
            });
            SearchItem = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), true);
            });
            PreviewOrder = new DelegateCommand((o) =>
           {
               IsBusy = true;
               SaveOrder(Convert.ToInt32(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo), OrderItems);
               PreviewOrderItems();
           });
            DeleteItemFromOrder = new DelegateCommand((Items) =>
            {
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                int selectionCount = selectedItems.Count;
                for (int index = 0; index < selectionCount; index++)
                {
                    OrderItems.Remove(selectedItems[index] as Models.OrderItem);
                    index--;
                    selectionCount--;
                }
                EnablePreview = OrderItems.Count == 0 ? false : true;
                OnPropertyChanged("TemplateItemsCount");
                ViewModels.PreviewOrder.GlobalInkCanvas = new System.Windows.Controls.InkCanvas();

            });

            OpenExpander = new DelegateCommand((param) =>
            {
                switch (param.ToString())
                {
                    case "Search":
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case "Sales History":
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case "Suggested Items":
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case "Sales Summary":
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    default:
                        ToggleExpander = false;
                        break;

                }

            });
        }


        private void SaveOrder(int CustomerId, ObservableCollection<OrderItem> OrderItems)
        {
            orderManager.SaveOrder(Order.OrderId, CustomerId, OrderItems);
        }

        private void PreviewOrderItems()
        {
            ResourceManager.Transaction.AddTransactionInQueueForSync(SalesLogicExpress.Application.Managers.Transaction.SaveOrder, SyncQueueManager.Priority.immediate);
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PreviewOrder, CurrentViewName = ViewModelMappings.View.Order, CloseCurrentView = false, Payload = OrderItems, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            IsBusy = false;
        }
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();

        public async Task SearchItems(string searchTerm, bool skipMinLengthCheck)
        {
            await Task.Delay(500, tokenForCancelTask.Token);
            if (!skipMinLengthCheck)
            {
                if (searchTerm != null && searchTerm.ToString().Length < 3)
                {
                    return;
                }
                if (searchTerm != null && searchTerm.ToString().Length == 0)
                {
                    SearchItemList.Clear();
                    this.OnPropertyChanged("SearchItemsCount");
                    return;
                }
            }
            SearchItemList.Clear();
            SearchItemList = itemManager.SearchItem(searchTerm.ToString());
            SearchItemsCount = SearchItemList.Count;
            this.OnPropertyChanged("SearchItemsCount");
            this.OnPropertyChanged("SearchResultText");
        }
        private void CalulateTotal(ObservableCollection<OrderItem> OrderCollection)
        {
            // throw new NotImplementedException();
        }

        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException();
            }
        }

        public string this[string columnName]
        {
            get { return null; }
        }



    }


    public class OpenOrder
    {
        public string OrderId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }

        public int OrderQty { get; set; }
        public int PickQty { get; set; }

        public string PrimaryUM { get; set; }

    }

}
