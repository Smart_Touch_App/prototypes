﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ReasonCode : ViewModelBase, IDataErrorInfo
    {
        public ReasonCode()
        {

        }
        public DelegateCommand VoidOrder { get; set; }

        public event EventHandler<OrderStateChangeArgs> StateChanged;

        public Guid MessageToken { get; set; }

        public string ErrorText { get; set; }
        public ReasonCode(string Type)
        {
            VoidOrderReasonList = new OrderManager().GetReasonListForVoidOrder();

            VoidOrder = new DelegateCommand((param) =>
           {
               ReasonCode rc = param as ReasonCode;

               if (rc == null)
               {
                   ErrorText = Constants.Common.SelectVoidOrderReasonAlert;
                   return;
               }
               else
               {
                   ErrorText = "";
               }

               new Managers.OrderManager().SaveVoidOrderReasonCode(Order.OrderId.ToString(), rc.Id);

               ObservableCollection<OrderItem> OrderItems = new ObservableCollection<OrderItem>();

               Helpers.NavigateToView moveToView = new NavigateToView();
               moveToView.CloseCurrentView = true;
               moveToView.ShowAsModal = false;
               OrderStateChangeArgs arg = new OrderStateChangeArgs();
               arg.State = OrderState.Void;
               OnStateChanged(arg);
               System.Threading.Thread.Sleep(50);
               if (ParentViewModel.GetType() == typeof(Order))
               {
                   Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), OrderManager.OrderState.OrderEntry, OrderManager.OrderSubState.Void);
                   moveToView.CurrentViewName = ViewModelMappings.View.Order;
               }
               else
               {
                   Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), OrderManager.OrderState.DeliveryAcceptance, OrderManager.OrderSubState.Void);
                   moveToView.CurrentViewName = ViewModelMappings.View.DeliveryScreen;
               }
               ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.VoidOrder, SyncQueueManager.Priority.immediate);
               if (new OrderManager().IsItemPickedForOrder(Order.OrderId.ToString()))
               {
                   // Navigate to pick order
                   moveToView.NextViewName = ViewModelMappings.View.PickOrder;
                   moveToView.Refresh = true;
                   moveToView.Payload = OrderItems;
                   Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                   Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
               }
               else
               {
                   // Navigate to customer home
                   moveToView.NextViewName = ViewModelMappings.View.CustomerHome;
                   moveToView.Payload = CommonNavInfo.Customer;
                   Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                   Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
               }
           });
        }
        private ObservableCollection<ReasonCode> _VoidOrderReasonList;
        public ObservableCollection<ReasonCode> VoidOrderReasonList
        {
            get
            {
                return _VoidOrderReasonList;
            }
            set
            {
                _VoidOrderReasonList = value;
            }
        }
        public object ParentViewModel { get; set; }

        public int Id { get; set; }
        public string Code { get; set; }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }

        public void OnStateChanged(OrderStateChangeArgs e)
        {
            EventHandler<OrderStateChangeArgs> handler = StateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }


    }

    public class OrderStateChangeArgs : EventArgs
    {
        public OrderState State { get; set; }
        public DateTime StateChangedTime { get; set; }
    }
    public enum OrderState
    {
        Void,
        Hold,
        None
    }
}
