﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Data;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.ComponentModel;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class OrderTemplate : ViewModelBase
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.OrderTemplate");
        CommonNavInfo navInfo = new CommonNavInfo();
        private TrulyObservableCollection<Models.OrderItem> _OrderItems;
        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public DateTime StateChangedTime { get; set; }
        }
        #region Action Commands exposed to View
        public DelegateCommand AddItemToTemplate { get; set; }
        public DelegateCommand ClearSearchText { get; set; }

        public DelegateCommand SearchItem { get; set; }
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand SaveTemplate { get; set; }
        public DelegateCommand SavePreOrder { get; set; }
        public DelegateCommand CreateOrder { get; set; }
        public DelegateCommand DeleteItemFromTemplate { get; set; }
        public DelegateCommand LoadTemplateWithUsualQuantity { get; set; }
        public DelegateCommand LoadTemplateWithPreOrderQuantity { get; set; }
        public DelegateCommand ResetBackTemplateQuantity { get; set; }
        public DelegateCommand OpenExpander { set; get; }
        public DelegateCommand UpdateTemplateItemsFromOrder { set; get; }
        public TrulyObservableCollection<Models.OrderItem> OrderItems
        {
            get
            {
                return this._OrderItems;
            }
            set
            {
                _OrderItems = value;
                OnPropertyChanged("OrderItems");

            }
        }


        public DelegateCommand SearchResultTextChange { set; get; }
        public DelegateCommand GridCellEditEnded { get; set; }
        public DelegateCommand BeginGridCellEdit { get; set; }
        public DelegateCommand UpdateAvailableQty { get; set; }

        private Dictionary<string, string> OrderQuantityMap = new Dictionary<string, string>();

        CancellationTokenSource tokenSource = new CancellationTokenSource();
        private string OldSeqNo;
        bool CheckIsEditEnded = true;
        bool _SearchResultTextVisibility = false;
        #endregion

        #region Properties exposed to View
        Timer searchTimer;
        private delegate void Method();
        Managers.TemplateManager templateManager;
        Managers.ItemManager itemManager;
        public Guid MessageToken { get; set; }

        string _SearchText;
        public string SearchText
        {
            get { return _SearchText; }
            set { _SearchText = value; OnPropertyChanged("SearchText"); }
        }
        private CommonNavInfo _NavigationInfo;
        public CommonNavInfo NavigationInfo
        {
            get
            {
                return _NavigationInfo = new CommonNavInfo();
            }
            set
            {
                _NavigationInfo = value;
            }
        }
        public List<int> PreviousSequenceNumbers
        {
            get { return _previousSequenceNumbers; }
            set { _previousSequenceNumbers = value; }
        }

        private string _OrderNumber;
        private List<int> _previousSequenceNumbers;
        public string OrderNumber
        {
            get { return _OrderNumber; }
            set
            {
                if (value != _OrderNumber)
                {
                    _OrderNumber = value;
                    OnPropertyChanged("OrderNumber");
                }
            }
        }
        private bool _CloseDropdown;
        public bool CloseDropdown
        {
            get { return _CloseDropdown; }
            set { _CloseDropdown = value; OnPropertyChanged("CloseDropdown"); }
        }
        private string _SearchResultText;
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Items found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        private bool _ToggleExpander = false;
        public bool ToggleExpander
        {
            get { return _ToggleExpander; }
            set
            {
                _ToggleExpander = value;
                OnPropertyChanged("ToggleExpander");
            }
        }

        private ObservableCollection<Models.Item> _SearchItems = new ObservableCollection<Models.Item>();
        public ObservableCollection<Models.Item> SearchItemList
        {
            get
            {
                return this._SearchItems;
            }
            set
            {
                this._SearchItems.Clear();
                this._SearchItems = value;
                OnPropertyChanged("SearchItemList");
                OnPropertyChanged("SearchResultText");
            }
        }

        private ObservableCollection<Models.TemplateItem> _TemplateItems = new ObservableCollection<TemplateItem>();
        public ObservableCollection<Models.TemplateItem> TemplateItems
        {
            get
            {
                return this._TemplateItems;
            }
            set
            {
                _TemplateItems = value;
                OnPropertyChanged("TemplateItems");
            }
        }
        public int TemplateItemsCount
        {
            get
            {
                if (this._TemplateItems != null) return this._TemplateItems.Count;
                return 0;
            }
            set
            {
                OnPropertyChanged("TemplateItemsCount");
            }
        }
        private int _SearchItemsCount;

        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }
        private Models.Customer _Customer;
        public Models.Customer Customer
        {
            get;
            set;
        }
        public DataTable _OrderHistory;
        public DataTable _OrderHistoryHeaders;
        public DataTable OrderHistory
        {
            get { return _OrderHistory; }
            set { _OrderHistory = value; OnPropertyChanged("OrderHistory"); }
        }

        public DataTable OrderHistoryHeaders
        {
            get { return _OrderHistoryHeaders; }
            set { _OrderHistoryHeaders = value; OnPropertyChanged("OrderHistoryHeaders"); }
        }

        #endregion

        public OrderTemplate()
        {
            Initialize();
        }
        public OrderTemplate(object customer)
        {
            Customer = (Customer)customer;
            Initialize();
            IsBusy = true;
            GetTemplateItems();
            GetOrderHistory();
        }
        async void GetTemplateItems()
        {
            await Task.Run(() =>
            {
                // Get Order headers
                //**********************************************************************************************************
                Managers.OrderManager orderManager = new Managers.OrderManager();
                DataSet RowAndHeader = null;
                RowAndHeader = orderManager.GetOrderHistoryForCustomer(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                OrderHistory = RowAndHeader.Tables[0];
                OrderHistoryHeaders = RowAndHeader.Tables[1];

                // Get Templates
                //**********************************************************************************************************
                TemplateItems = templateManager.GetTemplateItemsForCustomer(Customer.CustomerNo);
                foreach (Models.TemplateItem item in this._TemplateItems)
                {
                    OrderQuantityMap.Add(item.ItemNumber, item.OrderQty.ToString());

                    foreach (DataRow dr in OrderHistory.Rows)
                    {
                        if (dr["ItemCode"].ToString() == item.ItemNumber)
                        {
                            item.AverageStopQty = Convert.ToDouble(dr["AverageQty"].ToString());
                            break;
                        }
                        else
                        {
                            item.AverageStopQty = 0;

                        }
                    }
                }
                //**********************************************************************************************************
                ModelChangeArgs args = new ModelChangeArgs();
                args.StateChangedTime = DateTime.Now;
                OnModelChanged(args);
                IsBusy = false;
                OnPropertyChanged("TemplateItemsCount");

            });
        }
        async void GetOrderHistory()
        {
            await Task.Run(() =>
            {

                try
                {
                   

                }
                catch (Exception)
                {
                    //throw;
                }
            });
        }

        private void Initialize()
        {
            templateManager = new Managers.TemplateManager();
            itemManager = new Managers.ItemManager();
            if (this._SearchItems != null)
                _SearchItemsCount = this._SearchItems.Count;
            InitializeCommands();
        }
        private void InitializeCommands()
        {
            Random qtyOnHand = new Random();
            UpdateTemplateItemsFromOrder = new DelegateCommand((param) =>
            {
                UpdateOrderTemplateItems();
            });


            UpdateAvailableQty = new DelegateCommand((param) =>
            {
                TemplateItem templateItem = param as TemplateItem;
                templateItem.QtyOnHand = (templateItem.ActualQtyOnHand - templateItem.OrderQty) < 0 ? 0 : (templateItem.ActualQtyOnHand - templateItem.OrderQty);
            });


            SaveTemplate = new DelegateCommand((param) =>
            {
                List<TemplateItem> saveTemplateItemList = this._TemplateItems.Where(item => item.InclOnTmplt == true).ToList<TemplateItem>();
                templateManager.SaveTemplate(saveTemplateItemList, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                var alertMessage = new Helpers.AlertWindow { Message = "Template saved successfully", MessageIcon = "Save Successful", Header = "Success" };
                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
            });
            SavePreOrder = new DelegateCommand((param) =>
            {
              //  templateManager.SavePreOrder(preOrderItems);
            });
            LoadTemplateWithUsualQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._TemplateItems)
                {
                    item.OrderQty = item.UsualQty;
                }
                CloseDropdown = false;
                OnPropertyChanged("CloseDropdown");

            });
            LoadTemplateWithPreOrderQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._TemplateItems)
                {
                    item.OrderQty = item.PreOrderQty;
                }
                CloseDropdown = false;
                OnPropertyChanged("CloseDropdown");
            });
            ResetBackTemplateQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._TemplateItems)
                {
                    item.OrderQty = 0;
                }
                CloseDropdown = false;
                OnPropertyChanged("CloseDropdown");
            });
            // Add item from search to template list
            AddItemToTemplate = new DelegateCommand((Items) =>
            {
                Managers.ItemManager itemManager = new Managers.ItemManager();
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                int selectionCount = selectedItems.Count;
                int maxSequenceNo = TemplateItems.Count > 0 ? TemplateItems.Max(item => item.SeqNo) : 0;
                int seqNo = 1;
                for (int index = 0; index < selectionCount; index++)
                {
                    if (TemplateItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                    {
                        continue;
                    }
                    Models.TemplateItem addedItem = new TemplateItem((selectedItems[index] as Models.Item));
                    addedItem.EffectiveFrom = DateTime.Now.ToString("mm/dd/yyyy");
                    addedItem.EffectiveThru = DateTime.Now.ToString("mm/dd/yyyy");
                    SearchItemList.Remove(selectedItems[index] as Models.Item);
                    addedItem.SeqNo = maxSequenceNo + (seqNo * 5);
                    addedItem.ActualQtyOnHand = qtyOnHand.Next(5, 50);
                    string itemNumber = addedItem.ItemNumber;
                    addedItem.AverageStopQty = AveragerQty(itemNumber);  //returning avg value    
                    // OrderItem order = new OrderItem(new Item());
                    addedItem.AppliedUMS = itemManager.GetAppliedUMs(addedItem.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(addedItem.ItemNumber) : new List<string>() { addedItem.UM };
                    TemplateItems.Insert(0, addedItem);
                    index--;
                    selectionCount--;
                    seqNo++;
                    SearchItemsCount = SearchItemList.Count;
                    OnPropertyChanged("SearchItemsCount");
                    OnPropertyChanged("SearchResultText");
                }
                OnPropertyChanged("TemplateItemsCount");
            });

            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchItemOnType.Execute(SearchText);
            });
            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), false);
            });
            SearchItem = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), true);
            });
            CreateOrder = new DelegateCommand((o) =>
            {
                IsBusy = true;
                try
                {
                    List<TemplateItem> saveTemplateItemList = this._TemplateItems.Where(item => item.InclOnTmplt == true).ToList<TemplateItem>();
                    templateManager.SaveTemplate(saveTemplateItemList, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                    CreateOrderItems();
                    if (ViewModels.PreviewOrder.GlobalInkCanvas != null)
                        ViewModels.PreviewOrder.GlobalInkCanvas.Strokes.Clear();
                    Order.OrderSubStatus = Managers.OrderManager.OrderSubState.None;
                    Order.VoidOrderReasonId = 0;
                }
                catch (Exception ex)
                {
                    Logger.Error("Erro in CreateOrder" + ex.StackTrace.ToString());
                    IsBusy = false;
                }
            });
            DeleteItemFromTemplate = new DelegateCommand((Items) =>
            {
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                int selectionCount = selectedItems.Count;
                for (int index = 0; index < selectionCount; index++)
                {
                    TemplateItems.Remove(selectedItems[index] as Models.TemplateItem);
                    index--;
                    selectionCount--;
                }
                OnPropertyChanged("TemplateItemsCount");
            });

            OpenExpander = new DelegateCommand((param) =>
            {

                if (!ToggleExpander) { ToggleExpander = true; }
                else { ToggleExpander = false; };
            });

            BeginGridCellEdit = new DelegateCommand((param) =>
            {


                ObservableCollection<Models.TemplateItem> TempItems = new ObservableCollection<TemplateItem>();
                TempItems = (ObservableCollection<Models.TemplateItem>)param;
                PreviousSequenceNumbers = new List<int>();


                for (int index = 0; index < TempItems.Count; index++)
                {
                    PreviousSequenceNumbers.Add(TempItems[index].SeqNo);
                }
                CheckIsEditEnded = true;

            });
            GridCellEditEnded = new DelegateCommand((param) =>
            {

                if (CheckIsEditEnded)
                {
                    TemplateItem TempItem = param as TemplateItem;


                    if (PreviousSequenceNumbers.Contains(TempItem.SeqNo))
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Duplicate Sequence number", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        CheckIsEditEnded = false;
                        TempItem.SeqNo = OldSequenceNo;
                    }
                    else
                    {
                        OnPropertyChanged("TemplateItems");

                    }

                }
                else
                {
                    OnPropertyChanged("TemplateItems");
                }

            });
            SearchResultTextChange = new DelegateCommand((param) =>
            {
                //Item itm = param as Item;

                SearchItemsCount = Convert.ToInt32(param);
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            });


        }

        public double AveragerQty(string itemNumber)
        {

            double avgQty = 0;

            foreach (DataRow dr in OrderHistory.Rows)
            {
                if (dr["ItemCode"].ToString() == itemNumber)
                {
                    avgQty = Convert.ToDouble(dr["AverageQty"]);
                    break;
                }
                else
                {
                    avgQty = 0;
                }
            }
            return avgQty;
        }

        //public List<string> GetAppliedUms(int itemNumber)
        //{
        //    List<string> AppliedUms = new List<string>();

        //    DataTable dt = DbEngine.ExecuteDataSet(@"select distinct(UMUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + itemNumber + "')" +
        //   " union select distinct(UMRUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + itemNumber + "')").Tables[0];

        //    foreach (DataRow item in dt.Rows)
        //    {
        //        AppliedUms.Add(item["UMUM"].ToString());
        //    }
        //    return AppliedUms;
        //}

        public int OldSequenceNo { get; set; }

        public bool IsDuplicate { get; set; }

        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        public async Task SearchItems(string searchTerm, bool skipMinLengthCheck)
        {
            await Task.Delay(500, tokenForCancelTask.Token);
            if (!skipMinLengthCheck)
            {
                if (searchTerm != null && searchTerm.ToString().Length < 3)
                {
                    return;
                }
                if (searchTerm != null && searchTerm.ToString().Length == 0)
                {
                    SearchItemList.Clear();
                    this.OnPropertyChanged("SearchItemsCount");
                    this.OnPropertyChanged("SearchResultText");
                    return;
                }
            }
            SearchItemList.Clear();
            SearchItemList = itemManager.SearchItem(searchTerm.ToString());
            SearchItemsCount = SearchItemList.Count;
            this.OnPropertyChanged("SearchItemsCount");
            this.OnPropertyChanged("SearchResultText");
        }



        async void CreateOrderItems()
        {
            await Task.Run(() =>
            {
                try
                {
                    OrderItems = templateManager.ApplyPricingToTemplates(TemplateItems, SalesLogicExpress.Application.Managers.UserManager.UserBranch, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                }
                catch (Exception ex)
                {
                    Logger.Error("Erro in CreateOrderItems" + ex.Message.ToString());

                }
            });

            try
            {
                SaveOrder(Convert.ToInt32(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo), OrderItems);
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), Managers.OrderManager.OrderState.OrderEntry, Managers.OrderManager.OrderSubState.None);
            }
            catch (Exception ex)
            {
                Logger.Error("Erro in SaveOrder" + ex.InnerException.Message);
            }


            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.Order, CurrentViewName = ViewModelMappings.View.OrderTemplate, CloseCurrentView = false, Payload = OrderItems, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            IsBusy = false;
        }

        private void SaveOrder(int CustomerId, ObservableCollection<OrderItem> OrderItems)
        {
            Managers.OrderManager orderManager = new Managers.OrderManager();

            int existingOrderId = orderManager.GetUncomletedLastOrderIdIfAny(CommonNavInfo.Customer.CustomerNo);

            if (existingOrderId != 0)
            {
                Order.OrderId = existingOrderId;
            }
            else
            {
                Order.OrderId = orderManager.GetNewOrderNum();
            }

            orderManager.SaveOrder(Order.OrderId, CustomerId, OrderItems);


        }

        public void UpdateOrderTemplateItems()
        {
            try
            {
                if (OrderItems != null)
                {

                    if (TemplateItems.Count > 0)
                    {
                        foreach (TemplateItem item in TemplateItems)
                        {
                            if (item.OrderQty > 0 && !OrderItems.Any(s => s.ItemNumber == item.ItemNumber))
                            {
                                // If any Item deleted from Order
                                item.OrderQty = 0;
                            }
                        }
                    }
                    if (OrderItems.Count > 0)
                    {
                        foreach (OrderItem item in OrderItems)
                        {
                            if (!TemplateItems.Any(s => s.ItemNumber == item.ItemNumber))
                            {
                                // If new Item Added to Order
                                Models.TemplateItem addedItem = new TemplateItem(item);
                                addedItem.OrderQty = item.OrderQty;
                                addedItem.EffectiveFrom = DateTime.Now.ToString("mm/dd/yyyy");
                                addedItem.EffectiveThru = DateTime.Now.ToString("mm/dd/yyyy");
                                //addedItem.SeqNo = maxSequenceNo + (seqNo * 5);
                                addedItem.QtyOnHand = item.QtyOnHand;
                                addedItem.AverageStopQty = item.AverageStopQty;  //returning avg value    
                                addedItem.ActualQtyOnHand = item.ActualQtyOnHand;
                                // OrderItem order = new OrderItem(new Item());
                                addedItem.AppliedUMS = item.AppliedUMS;
                                TemplateItems.Add(addedItem);
                            }
                            else
                            {
                                // If any Item Edited from Order
                                TemplateItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).OrderQty = item.OrderQty;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).UM = item.UM;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).QtyOnHand = item.QtyOnHand;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).ActualQtyOnHand = item.ActualQtyOnHand;
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Telerik.Windows.Data.SortDescriptor SortGrid()
        {

            Telerik.Windows.Data.SortDescriptor descriptor = new Telerik.Windows.Data.SortDescriptor();
            descriptor.Member = "SeqNo";
            descriptor.SortDirection = ListSortDirection.Ascending;

            return descriptor;
        }

    }
}
