﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RouteHome : ViewModelBase
    {
        public Guid MessageToken { get; set; }
        public DelegateCommand NavigateToServiceRoute { get; set; }
        public string Route { get; set; }
        public string CurrentDate { get; set; }
        public string RouteName { get; set; }
        public RouteHome()
        {
            
        }
        public RouteHome(object payload)
        {
            Route = CommonNavInfo.UserRoute;
            RouteName = Managers.UserManager.RouteName;
            CurrentDate = DateTime.Now.ToString("dddd, MM/dd/yyyy");
            InitializeCommands();
        }
        void InitializeCommands()
        {
            NavigateToServiceRoute = new DelegateCommand((param) =>
            {
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ServiceRoute, CurrentViewName = ViewModelMappings.View.RouteHome, Payload = Route, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
 
            });
        }
    }
}
