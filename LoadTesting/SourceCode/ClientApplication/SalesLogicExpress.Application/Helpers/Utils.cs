﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
    public static class Utils
    {
        /// <summary>
        /// Extension Synchronize to check whether the DataSet has data or not
        /// </summary>
        /// <param name="DataSetObject"></param>
        /// <returns>true if DataSet has Atleast one table and that table has rows in it.</returns>
        public static bool HasData(this DataSet DataSetObject)
        {
            if (!(DataSetObject == null || DataSetObject.Tables.Count == 0 || DataSetObject.Tables[0].Rows.Count == 0))
            {
                return true;
            }
            return false;
    
        }
        public static string GetEnumDescription(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return enumValue.ToString();
            }
        
        }
        public static string GetEnumPriority(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
            DefaultPropertyAttribute[] attributes = (DefaultPropertyAttribute[])fi.GetCustomAttributes(typeof(DefaultPropertyAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].ToString();
            }
            else
            {
                return enumValue.ToString();
            }

        }
    }

}
