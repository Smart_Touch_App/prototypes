﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
   public class DialogWindow
    {
       public object Payload { get; set; }
       public string Title { get; set; }
       public string TemplateKey { get; set; }
       public object ContentTemplate { get; set; }
       public bool Cancelled { get; set; }
       public bool Confirmed { get; set; }
       
    }  
}
