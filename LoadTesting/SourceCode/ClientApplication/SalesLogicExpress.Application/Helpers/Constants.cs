﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
    public class Constants
    {

        public class Common
        {
            public const string HoldOrderConfirmation = "Do you want to hold this order?";
            public const string SelectVoidOrderReasonAlert = "Please select a reason to void this order.";
            public const string DeleteItemConfirmation = "Are you sure you want to delete selected items?";
            public const string SelectItemForDeleteAlert = "Please select items to delete.";
            public const string SelectItemToAddAlert = "Please select at least one item to add.";
            public const string AppExitConfirmation = "Are you sure you want to exit the application?";
            public const string SelectedItemExists = "Selected item is already present in template.";
        }

        public class OrderTemplate
        {

        }

        public class Order
        {

        }
        public class OrderPreview
        {
            public const string SelectReasonForSurchargeAlert = "Please select a reason code.";

        }
        public class PickOrder
        {
            public const string SelectItemToPickAlert = "Please select an item from pick list to manual pick.";
            public const string SelectItemToUnPickAlert = "Please select an item from exception list to remove.";
            public const string SelectReasonForManualPickAlert = "Please select a reason before manual pick.";
            public const string RemoveExceptionConfirmation = "Are you sure you want to remove items?";

        }
        public class DeliveryScreen
        {

        }
        public class Contacts
        {
            public const string DeleteContactConfirmation = "Are you sure you want to delete selected contact?";
            public const string DeleteEmailConfirmation = "Are you sure you want to delete this email?";
            public const string DeletePhoneConfirmation = "Are you sure you want to delete this phone number?";
        }
    }
}
