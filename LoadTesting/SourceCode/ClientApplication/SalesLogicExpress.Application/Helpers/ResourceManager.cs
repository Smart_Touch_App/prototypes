﻿using iAnywhere.Data.SQLAnywhere;
using System;
using System.Configuration;

namespace SalesLogicExpress.Application.Helpers
{
    public static class ResourceManager
    {
        //Added By Arvind
        static System.Collections.Generic.Dictionary<string, string> _Connections = new System.Collections.Generic.Dictionary<string, string>();


        static AppService.AppServiceClient serviceClient = null;
        static SAConnection connection = null;
        static string remoteID = null;
        static SalesLogicExpress.Application.Managers.TransactionManager transactionManager = null;
        static SalesLogicExpress.Application.Managers.SyncManager syncManager = null;
        static SalesLogicExpress.Application.Managers.NetworkManager networkManager = null;
        static string databaseDirectory, databaseResourcesDirectory, deviceID, activeDatabaseConnectionString;
        public static string DatabaseDirectory
        {
            get
            {
                return databaseDirectory;
            }
            set
            {
                if (string.IsNullOrEmpty(databaseDirectory))
                {
                    databaseDirectory = value;
                }
            }
        }
        public static Managers.NetworkManager NetworkInfo
        {
            get
            {
                if (networkManager == null)
                {
                    networkManager = new Managers.NetworkManager();
                }
                return networkManager;
            }
        }
        public static AppService.AppServiceClient ServiceClient
        {
            get
            {
                if (serviceClient == null)
                {
                    serviceClient = new AppService.AppServiceClient();
                }
                return serviceClient;
            }
        }
        public static Managers.TransactionManager Transaction
        {
            get
            {
                if (transactionManager == null)
                {
                    transactionManager = new Managers.TransactionManager();
                }
                return transactionManager;
            }
        }
        public static Managers.SyncManager Synchronization
        {
            get
            {
                if (syncManager == null)
                {
                    syncManager = new Managers.SyncManager();
                }
                return syncManager;
            }
        }
        public static string DeviceID
        {
            get
            {
                return deviceID;
            }
            set
            {
                if (string.IsNullOrEmpty(deviceID))
                {
                    deviceID = value;
                }
            }
        }
        public static string ActiveRemoteID
        {
            get
            {
                return remoteID;
            }
            set
            {
                if (string.IsNullOrEmpty(remoteID))
                {
                    remoteID = value;
                }
            }
        }
        public static string ActiveDatabaseConnectionString
        {
            get
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["useDefaultDb"].ToString()) == true) {
                    return ConfigurationManager.AppSettings["connectionString"].ToString() ;
                }


                return activeDatabaseConnectionString;

                //return _Connections[AppDomain.CurrentDomain.FriendlyName];
            }
            set
            {
                //Chnged By Arvind
                //_Connections.Add(AppDomain.CurrentDomain.FriendlyName, value);
               activeDatabaseConnectionString = value;
            }
        }
        public static string DatabaseResourcesDirectory
        {
            get
            {
                return databaseResourcesDirectory;
            }
            set
            {
                if (string.IsNullOrEmpty(databaseResourcesDirectory))
                {
                    databaseResourcesDirectory = value;
                }
            }
        }

        #region Methods for Database Connection instance
        public static SAConnection GetOpenConnectionInstance()
        {
            try
            {
                if (connection == null )
                {
                    connection = new SAConnection(ActiveDatabaseConnectionString);
                    connection.Open();
                }
                if (connection.State == System.Data.ConnectionState.Closed || connection.State == System.Data.ConnectionState.Broken)
                {
                    connection.Open();
                }
                return connection;
            }
            catch (System.Exception ex)
            {
                return null;
            }

        }
        public static void CloseConnectionInstance()
        {
            if (connection == null) return;
            if (connection.State == System.Data.ConnectionState.Open || connection.State == System.Data.ConnectionState.Broken)
            {
                connection.Close();
                connection.Dispose();
            }
        }
        #endregion
    }
}
