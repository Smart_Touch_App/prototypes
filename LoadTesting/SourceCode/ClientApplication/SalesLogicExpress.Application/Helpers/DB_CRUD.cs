﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using iAnywhere.Data.SQLAnywhere;
using log4net;


namespace SalesLogicExpress.Application.Helpers
{
    public class DB_CRUD
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Helpers.DB_CRUD");
        SAConnection connection;
        public DB_CRUD()
        {
            try
            {
                connection = ResourceManager.GetOpenConnectionInstance();
            }
            catch (Exception e)
            {
                log.Error("Page:DB_CRUD.cs,Synchronize:Constructor  Message: " + e.Message);
                throw e;
            }
        }

        ~DB_CRUD()
        {
            try
            {
            }
            catch (Exception e)
            {
                log.Error("Page:DB_CRUD.cs,Synchronize:Desctructor  Message: " + e.Message);
                throw e;
            }
        }

        public Boolean checkValidity(SACommand CommandToExecute)
        {
            bool result = false;
            try
            {
                SADataReader reader = null;
                reader = CommandToExecute.ExecuteReader();
                if (reader.Read())
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                log.Error("Page:DB_CRUD.cs,Synchronize:checkValidity, Query: " + CommandToExecute.CommandText + ",  Message : " + ex.Message);
            }
            return result;
        }

        public void CloseConnection()
        {
            connection.Close();
        }

        public SADataReader ExecuteReader(SACommand CommandToExecute)
        {
            SADataReader reader = null;
            try
            {
                CommandToExecute.Connection = connection;
                reader = CommandToExecute.ExecuteReader();
            }
            catch (Exception ex)
            {
                log.Error("Page:DB_CRUD.cs,Synchronize:ExecuteReader, Query: " + CommandToExecute.CommandText + ",  Message : " + ex.Message);
            }
            return reader;
        }

        public object ExecuteScalar(SACommand CommandToExecute)
        {
            try
            {
                CommandToExecute.Connection = connection;
                return CommandToExecute.ExecuteScalar();
            }
            catch (Exception ex)
            {
                log.Error("Page:DB_CRUD.cs,Synchronize:ExecuteScalar, Query: " + CommandToExecute.CommandText + ",  Message : " + ex.Message);
            }
            return string.Empty;
        }

        public SAConnection GetActiveConnection()
        {
            return connection;
        }

        public DataTable GetDataIntoDt(SACommand CommandToExecute)
        {
            DataTable resultData = new DataTable();
            try
            {
                CommandToExecute.Connection = connection;
                SADataAdapter da = new SADataAdapter(CommandToExecute);
                da.Fill(resultData);
            }
            catch (Exception ex)
            {
                log.Error("Page:DB_CRUD.cs,Synchronize:GetDataIntoDt, Query: " + CommandToExecute.CommandText + ",  Message : " + ex.Message);
            }
            return resultData;
        }

     
        public int Insertdata(SACommand CommandToExecute)
        {
            try
            {
                CommandToExecute.Connection = connection;
                int i = CommandToExecute.ExecuteNonQuery();
                return i;
            }
            catch (Exception ex)
            {
                log.Error("Page:DB_CRUD.cs,Synchronize:Insertdata, Query: " + CommandToExecute.CommandText + ",  Message : " + ex.Message);
            }
            return -1;
        }
    }
}
