﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Application.Helpers
{
    public class AlertWindow
    {
        public string Message
        {
            get;
            set;
        }
        public string MessageIcon
        {
            get;
            set;
        }
         public string Header
        {
            get;
            set;
        }
    }
}
