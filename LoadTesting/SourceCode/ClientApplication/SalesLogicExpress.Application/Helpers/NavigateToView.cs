﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
    public class NavigateToView
    {
        public ViewModelMappings.View NextViewName { get; set; }
        public ViewModelMappings.View CurrentViewName { get; set; }
        public bool CloseCurrentView { get; set; }
        public object Payload { get; set; }
        public bool ShowAsModal { get; set; }
        public bool Refresh { get; set; }
        private bool _ConfirmWindow = false;
        public bool ConfirmWindow { get { return _ConfirmWindow; }
            set { _ConfirmWindow = value; }
        }
    }
}
