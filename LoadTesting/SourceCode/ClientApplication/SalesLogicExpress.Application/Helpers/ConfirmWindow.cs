﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Application.Helpers
{
    public class ConfirmWindow
    {
        public string Message
        {
            get;
            set;
        }
        public string MessageIcon
        {
            get;
            set;
        }
        public bool Confirmed
        {
            get;
            set;
        }
        public bool Cancelled
        {
            get;
            set;
        }



    }
    public class CloseDialogWindow
    {
        public string DialogKey
        {
            get;
            set;
        }

    }
}
