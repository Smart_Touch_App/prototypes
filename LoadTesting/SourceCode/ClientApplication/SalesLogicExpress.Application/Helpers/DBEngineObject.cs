﻿using iAnywhere.Data.SQLAnywhere;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace SalesLogicExpress.Application.Helpers
{
    public  class DbEngineObject
    {
        string ConnectionString;
        SAConnection dbConnection;

        public DbEngineObject()
        {
            ConnectionString = ConfigurationManager.AppSettings.Get("connectionString");
            SAConnection dbConnection = new SAConnection(ConnectionString);
            dbConnection.Open();
        

        }

        public  DataSet ExecuteDataSet(string Query)
        {
            return ExecuteDataSet(Query, null, null, null);
        }
        public  DataSet ExecuteDataSet(string Query, string SourceTable)
        {
            return ExecuteDataSet(Query, null, SourceTable, null);
        }
        public  DataSet ExecuteDataSet(Dictionary<string, string> QueryListWithSourceTable)
        {
            DataSet result = new DataSet();
            try
            {
                string query, sourceTableName;
                //using (SAConnection dbConnection = new SAConnection(ConnectionString))
                {
                    //dbConnection.Open();
                    using (SACommand command = new SACommand(string.Empty, dbConnection))
                    {
                        command.CommandType = CommandType.Text;
                        using (SADataAdapter adapter = new SADataAdapter(command))
                        {
                            foreach (KeyValuePair<string, string> queryItem in QueryListWithSourceTable)
                            {
                                sourceTableName = queryItem.Key;
                                query = queryItem.Value;
                                command.CommandText = query;
                                adapter.Fill(result, sourceTableName);
                            }
                        }
                    }
                    //dbConnection.Close();
                    //dbConnection.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        public  DataSet ExecuteDataSet(string Query, Dictionary<string, object> Parameters, string SourceTable, DataSet ReferenceDataSet)
        {
            DataSet result = ReferenceDataSet == null ? new DataSet() : ReferenceDataSet;
            try
            {
                //using (SAConnection dbConnection = new SAConnection(ConnectionString))
                {
                   // dbConnection.Open();
                    if (dbConnection == null) return null;
                    using (SACommand command = new SACommand(string.Empty, dbConnection))
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = Query;
                        AddParameters(Parameters, command);
                        using (SADataAdapter adapter = new SADataAdapter(command))
                        {
                            if (string.IsNullOrEmpty(SourceTable))
                            {
                                adapter.Fill(result);
                            }
                            else
                            {
                                adapter.Fill(result, SourceTable);
                            }
                        }
                    }

                    //dbConnection.Close();
                    //dbConnection.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        public  DataSet ExecuteDataSet(string Query, Dictionary<string, object> Parameters)
        {
            return ExecuteDataSet(Query, Parameters, null, null);
        }
        public  int ExecuteNonQuery(string Query)
        {
            return ExecuteNonQuery(Query, null);
        }
        public  int ExecuteNonQuery(string Query, Dictionary<string, object> Parameters)
        {
            return ExecuteNonQuery(Query, Parameters, false);
        }
        public  int ExecuteNonQuery(string Query, Dictionary<string, object> Parameters, bool isStoreProcedure)
        {
            int result = 0;
            try
            {
               // using (SAConnection dbConnection = new SAConnection(ConnectionString))
                {
                    //dbConnection.Open();
                    if (dbConnection == null) return -1;
                    using (SACommand command = new SACommand(string.Empty, dbConnection))
                    {
                        command.CommandType = isStoreProcedure ? CommandType.StoredProcedure : CommandType.Text;
                        command.CommandText = Query;
                        AddParameters(Parameters, command);
                        result = command.ExecuteNonQuery();
                    }
                    //dbConnection.Close();
                    //dbConnection.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        private  void AddParameters(Dictionary<string, object> Parameters, SACommand command)
        {
            if (Parameters != null)
            {
                foreach (KeyValuePair<string, object> param in Parameters)
                {
                    command.Parameters.Add(param.Key, param.Value);
                }
            }
        }
        public  string ExecuteScalar(string Query)
        {
            return ExecuteScalar(Query, null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Query"></param>
        /// <param name="Parameters"></param>
        /// <param name="isStoredProcedure">true if the query input is the name of a procedure</param>
        /// <param name="ParameterType">True for in, False for out</param>
        /// <returns></returns>
        public  string ExecuteScalar(string Query, Dictionary<string, object> Parameters, bool isStoredProcedure, Dictionary<string, bool> ParameterType)
        {
            string result = string.Empty;
            try
            {
                //using (SAConnection dbConnection = new SAConnection(ConnectionString))
                {
                    //dbConnection.Open();
                    using (SACommand command = new SACommand(string.Empty, dbConnection))
                    {
                        command.CommandType = isStoredProcedure ? CommandType.StoredProcedure : CommandType.Text;
                        command.CommandText = Query;
                        SAParameter param;
                        if (Parameters != null)
                        {
                            foreach (KeyValuePair<string, object> parameterItem in Parameters)
                            {
                                param = command.CreateParameter();
                                if (ParameterType.ContainsKey(parameterItem.Key))
                                {
                                    param.Direction = ParameterType[parameterItem.Key] ? ParameterDirection.Input : ParameterDirection.Output;
                                }
                                param.Value = parameterItem.Value;
                                param.ParameterName = parameterItem.Key;
                                command.Parameters.Add(param);
                            }
                        }
                        result = command.ExecuteScalar().ToString();
                    }

                  //  dbConnection.Close();
                  //  dbConnection.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        public  string ExecuteScalar(string Query, Dictionary<string, object> Parameters)
        {
            string result = string.Empty;
            try
            {
                //using (SAConnection dbConnection = new SAConnection(ConnectionString))
                {
                    //dbConnection.Open();
                    if (dbConnection == null) return null;
                    using (SACommand command = new SACommand(string.Empty, dbConnection))
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = Query;
                        AddParameters(Parameters, command);
                        result = Convert.ToString(command.ExecuteScalar());
                    }

                  //  dbConnection.Close();
                   // dbConnection.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
    }
}
