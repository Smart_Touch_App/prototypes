﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections;
using System.Reflection;
namespace SalesLogicExpress.Application.Helpers
{
    public sealed class ViewModelMappings
    {
        ViewModelMappings()
        {
        }
        public enum View
        {
            None,
            CustomerListing,
            OrderTemplate,
            Order,
            ConfirmAndSign,
            PreviewOrder,
            PickOrder,
            UserLogin,
            ServiceRoute,
            CustomerHome,
            DeliveryScreen,
            RouteHome,
            PreOrder
        }
    }
}