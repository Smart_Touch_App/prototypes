﻿using System.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Telerik.Windows.Automation.Peers;
using ApplicationHelpers = SalesLogicExpress.Application.Helpers;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using System.Runtime.InteropServices;
namespace SalesLogicExpress.Presentation
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private readonly ILog log = LogManager.GetLogger("App.xaml.cs");
        public static string customerID = "1165497";
        public static string routeID = "783";
        public App()
        {
            Activated += App_Activated;
            Deactivated += App_Deactivated;
            // Setup path for database initialization and needed resource paths
            ApplicationHelpers.ResourceManager.DatabaseResourcesDirectory = string.Concat(System.AppDomain.CurrentDomain.BaseDirectory, "Resources\\RemoteDbSetupFiles");
            ApplicationHelpers.ResourceManager.DatabaseDirectory = string.Concat(System.AppDomain.CurrentDomain.BaseDirectory, "Databases");
            // Get the device unique ID
            ApplicationHelpers.ResourceManager.DeviceID =  DeviceManager.Device.GenerateDeviceID();
            // Create an active and singleton database connection
            //ApplicationHelpers.ResourceManager.GetOpenConnectionInstance();

            this.InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            AutomationManager.AutomationMode = AutomationMode.Disabled;
        }

        void App_Deactivated(object sender, EventArgs e)
        {
            SalesLogicExpress.Presentation.Helpers.Taskbar.Show();
        }

        void App_Activated(object sender, EventArgs e)
        {
            SalesLogicExpress.Presentation.Helpers.Taskbar.Hide();
        }

        public void RCB_VoidOrderReasonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        // TODO : Check how we can handle essaging at app level instead of window level.
        private object NavigateToViewExecute(NavigateToView action)
        {
            System.Windows.Window windowToShow, currentWindow;
            windowToShow = (Window)Helpers.WindowMappings.WindowInstance(action.NextViewName, action);
            currentWindow = (Window)Helpers.WindowMappings.WindowInstance(action.CurrentViewName, action);
            windowToShow.Visibility = System.Windows.Visibility.Visible;
            if (action.CloseCurrentView)
            {
                currentWindow.Close();
            }
            else
            {
                currentWindow.Visibility = System.Windows.Visibility.Hidden;
            }
            if (action.ShowAsModal)
            {
                windowToShow.ShowDialog();
                return null;
            }
            windowToShow.Show();

            return null;
        }
        //[DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        //private const uint KEYEVENTF_EXTENDEDKEY = 0x1;  // Release key

        private void Application_Exit(object sender, ExitEventArgs e)
        {

            ///*Tempararily commented*/
            //keybd_event(0x14, 0x45, 0, (UIntPtr)0);
            //keybd_event(0x14, 0x45, KEYEVENTF_EXTENDEDKEY, (UIntPtr)0);

            ApplicationHelpers.ResourceManager.CloseConnectionInstance();
            ApplicationHelpers.ResourceManager.Synchronization.ShutDownSyncClient();
            SalesLogicExpress.Presentation.Helpers.Taskbar.Show();

        }
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            log.Error(string.Format("UnhandledException, Message:{0}", e.Exception.StackTrace));
            // Signal that we handled things--prevents Application from exiting
            e.Handled = true;
        }
    }

}
