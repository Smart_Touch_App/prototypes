﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Presentation.Helpers
{
    public enum WindowOrientation
    {
        Landscape,
        Portrait
    }
    public class BaseWindow : System.Windows.Window
    {

        #region Variables
        TimeSpan sp = new TimeSpan(0, 0, 0, 0, 1000);
        public WindowOrientation WindowOrientation;
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Presentation.Helpers");
        public readonly Guid Token = Guid.NewGuid();
        Control keyboard = null;
        public Control DefaultFocusElement = null;
        List<ViewModelMappings.View> _AssociatedWindow = new List<ViewModelMappings.View>();
        public List<ViewModelMappings.View> AssociatedWindows
        {
            get
            {
                return _AssociatedWindow;
            }
            set
            {
                _AssociatedWindow = value;
            }
        }
        #endregion

        #region Constructor
        public BaseWindow()
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Start:Constructor][WindowTitle=" + this.Title + "]");
            // Subscribe to messages for displaying UI dialogs and navigation changes
            Messenger.Default.Register<AlertWindow>(this, Token, (action) => AlertWindowExecute(action));
            Messenger.Default.Register<ConfirmWindow>(this, Token, (action) => ConfirmWindowExecute(action));
            Messenger.Default.Register<DialogWindow>(this, Token, (action) => DialogWindowExecute(action));
            Messenger.Default.Register<NavigateToView>(this, Token, (action) => NavigateToViewExecute(action));
            Messenger.Default.Register<CloseDialogWindow>(this, Token, (action) => CloseWindowExecute(action));

            log4net.Config.XmlConfigurator.Configure();
            Closing += Window_Closing;
            Loaded += Window_Loaded;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged += DisplaySettingsChanged;
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            double width = SystemParameters.PrimaryScreenWidth;
            double height = SystemParameters.PrimaryScreenHeight;
            this.Width = 0;
            this.Height = 0;
            this.Left = width;
            this.Top = 0;
            this.WindowStyle = System.Windows.WindowStyle.None;
            this.ResizeMode = System.Windows.ResizeMode.NoResize;
            this.WindowState = WindowState.Normal;
            //System.Diagnostics.Debug.WriteLine(string.Format("BaseWindow : this.Height {0}, this.Width {1}, this.Left {2}, this.Top{3},this.Title {4}", this.Height, this.Width, this.Left, this.Top, this.Title));

            // Hook keyboard control to all textbox elements in window
            // Make sure that all controls are loaded and then attach keyboard for textbox elements
            AttachKeyboard();
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][End:Constructor][WindowTitle=" + this.Title + "]");
        }

        private void AttachKeyboard()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                foreach (TextBox textbox in FindLogicalChildren<TextBox>(this))
                {
                    textbox.GotFocus += textbox_GotFocus;
                    textbox.LostFocus += textbox_LostFocus;
                }
                foreach (TextBox textbox in FindVisualChildren<TextBox>(this))
                {
                    textbox.GotFocus += textbox_GotFocus;
                    textbox.LostFocus += textbox_LostFocus;
                }
                foreach (PasswordBox pb in FindVisualChildren<PasswordBox>(this))
                {
                    if (pb.Focusable)
                    {
                        pb.GotFocus += PasswordBox_GotFocus;
                        pb.LostFocus += PasswordBox_LostFocus;
                    }
                }
                // Grid view edit templates are not caught in either visual or logical child of the window
                // So for dispalying keyboard on cell edit , tap the begin edit event of gridview to plug the keyboard
                // toggle functionlaity
                foreach (RadGridView gridView in FindVisualChildren<RadGridView>(this))
                {
                    gridView.GotFocus += gridView_GotFocus;
                }
                foreach (RadGridView gridView in FindLogicalChildren<RadGridView>(this))
                {
                    gridView.GotFocus += gridView_GotFocus;
                }

            }));
        }

        private object CloseWindowExecute(CloseDialogWindow action)
        {
            Telerik.Windows.Controls.RadWindowManager.Current.CloseAllWindows();
            return null;
        }
        private void gridView_GotFocus(object sender, RoutedEventArgs e)
        {
            ToggleKeyboardForGridCellEdit();
        }
        #endregion

        #region Helper Methods for View Navigation/Transition

        #region Events for Transition
        public event EventHandler<TransitionStateEventArgs> TransitionStateChanged;
        protected virtual void OnTransitionStateChange(TransitionStateEventArgs e)
        {
            EventHandler<TransitionStateEventArgs> handler = TransitionStateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class TransitionStateEventArgs : EventArgs
        {
            public TransitionState State { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        public enum TransitionState
        {
            Started,
            Complete
        }
        #endregion

        ViewModelMappings.View currentViewName = ViewModelMappings.View.None;
        private object NavigateToViewExecute(NavigateToView action)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Start:NavigateToViewExecute][Window.Title=" + this.Title + "]");
            try
            {
                TransitionStateEventArgs args = new TransitionStateEventArgs();
                args.State = TransitionState.Started;
                OnTransitionStateChange(args);
                currentViewName = ViewModelMappings.View.None;
                System.Windows.Window windowToShow, windowToClose;
                windowToShow = (Window)Helpers.WindowMappings.WindowInstance(action.NextViewName, action);
                if (action.CloseCurrentView)
                {
                    windowToClose = (Window)Helpers.WindowMappings.WindowInstance(action.CurrentViewName);
                    if (windowToClose != null)
                    {
                        currentViewName = action.CurrentViewName;
                    }
                }
                else
                {
                    currentViewName = ViewModelMappings.View.None;
                }
                WindowMappings.currentWindow = this;
                WindowMappings.nextWindow = windowToShow;
                if (windowToShow.IsLoaded)
                {
                    if (action.Refresh)
                    {
                        (windowToShow as BaseWindow).RefreshWindow(action.Payload);
                    }
                    windowToShow.Show();
                    MovePrevious();
                }
                else
                {
                    //Note: Winndow.Show method is synchronous, which means it executes after the  window is loaded and ready for interaction.
                    windowToShow.Show();
                    MoveNext();
                }
                Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][End:NavigateToViewExecute][Window.Title=" + this.Title + "]");
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][NavigateToViewExecute][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return null;
            }
        }
        void MoveNext()
        {
            Storyboard sb = new Storyboard();
            DoubleAnimation animation = new DoubleAnimation(SystemParameters.PrimaryScreenWidth, 0, new Duration(sp));
            animation.AccelerationRatio = .2;
            animation.DecelerationRatio = .8;
            animation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(animation, WindowMappings.nextWindow);
            Storyboard.SetTargetProperty(animation, new PropertyPath(LeftProperty));

            double width = SystemParameters.PrimaryScreenWidth;
            double height = SystemParameters.PrimaryScreenHeight;
            WindowMappings.nextWindow.Width = width;
            WindowMappings.nextWindow.Height = height;
            WindowMappings.nextWindow.Left = width;

            DoubleAnimation nextanimation = new DoubleAnimation(0, -SystemParameters.PrimaryScreenWidth, new Duration(sp));
            nextanimation.AccelerationRatio = .2;
            nextanimation.DecelerationRatio = .8;
            nextanimation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(nextanimation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(nextanimation, new PropertyPath(LeftProperty));

            DoubleAnimation opacityAnimation = new DoubleAnimation(1, 0, new Duration(sp));
            opacityAnimation.AutoReverse = true;
            Storyboard.SetTarget(opacityAnimation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(OpacityProperty));

            sb.Children.Add(animation);
            sb.Children.Add(nextanimation);
            //sb.Children.Add(opacityAnimation);
            sb.Completed += MoveNext_Completed;
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
            sb.Begin(this);
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext][WindowMappings.currentWindow.Title=" + WindowMappings.currentWindow.Title + "][WindowMappings.nextWindow.Title=" + WindowMappings.nextWindow.Title + "]");
        }
        void MoveNext_Completed(object sender, EventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext_Completed][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
            WindowMappings.currentWindow.Hide();
            TransitionStateEventArgs args = new TransitionStateEventArgs();
            args.State = TransitionState.Complete;
            OnTransitionStateChange(args);
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext_Completed][WindowTitle=" + WindowMappings.currentWindow.Title + "][Hidden]");
        }
        void MovePrevious()
        {
            Storyboard sb = new Storyboard();
            WindowMappings.currentWindow.Left = 0;
            DoubleAnimation animation = new DoubleAnimation(0, SystemParameters.PrimaryScreenWidth, new Duration(sp));
            animation.AccelerationRatio = .2;
            animation.DecelerationRatio = .8;
            animation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(animation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(animation, new PropertyPath(LeftProperty));

            DoubleAnimation opacityAnimation = new DoubleAnimation(1, 0, new Duration(sp));
            opacityAnimation.AutoReverse = true;
            Storyboard.SetTarget(opacityAnimation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(OpacityProperty));

            double width = SystemParameters.PrimaryScreenWidth;
            double height = SystemParameters.PrimaryScreenHeight;
            WindowMappings.nextWindow.Width = width;
            WindowMappings.nextWindow.Height = height;
            WindowMappings.nextWindow.Left = -width;

            DoubleAnimation nextanimation = new DoubleAnimation(-SystemParameters.PrimaryScreenWidth, 0, new Duration(sp));
            nextanimation.AccelerationRatio = .2;
            nextanimation.DecelerationRatio = .8;
            nextanimation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(nextanimation, WindowMappings.nextWindow);
            Storyboard.SetTargetProperty(nextanimation, new PropertyPath(LeftProperty));

            sb.Children.Add(animation);
            sb.Children.Add(nextanimation);
            //sb.Children.Add(opacityAnimation);
            sb.Completed += MovePrevious_Completed;
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
            sb.Begin(this);
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious][WindowMappings.currentWindow.Title=" + WindowMappings.currentWindow.Title + "][WindowMappings.nextWindow.Title=" + WindowMappings.nextWindow.Title + "]");
        }
        void MovePrevious_Completed(object sender, EventArgs e)
        {
            if (currentViewName != ViewModelMappings.View.None)
            {
                Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowTitle=" + WindowMappings.currentWindow.Title + "][Closed]");
                Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
                WindowMappings.currentWindow.Tag = "NOWARN";
                WindowMappings.currentWindow.Close();
                Helpers.WindowMappings.DisposeWindowInstance(currentViewName);
            }
        }
        #endregion

        #region Window events and virtual methods
        public void MinimizeWindow()
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Uri iconUri = new Uri("Resources/Images/appicon.ico", UriKind.RelativeOrAbsolute);
            //this.Icon = System.Windows.Media.Imaging.BitmapFrame.Create(iconUri);
            AlwaysShowNumericKeyboard();
        }
        public void AlwaysShowNumericKeyboard()
        {
            if (AlwaysShowKeyboard)
            {
                ShowKeyboard("Numeric");
            }
        }

        public virtual void HideWindow()
        {

        }
        public virtual void RefreshWindow(object payload)
        {

        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Window_Closing][Window.Title=" + this.Title + "]");
            // Window close event is fired in tow cases, 
            // 1: when we click on close button of window
            // 2: when we do a back naviagtion.
            // the NOWARN tag is set when we do a backward navigation in application, where we want to 
            // close the existing window and move to previous one, and dont want to exit the application.
            // When we click the close button of any window, the window tag is not set and a warning is displayed
            // but when we do a back navigation we set the tag , indicating that we dont to close 
            // the app and just want to navigate backword
            if (((Window)sender).Tag != null && ((Window)sender).Tag.ToString() == "NOWARN")
            {
                if (AssociatedWindows.Count > 0)
                {
                    System.Windows.Window windowToClose = null;
                    for (int windowIndex = 0; windowIndex < AssociatedWindows.Count; windowIndex++)
                    {
                        windowToClose = (Window)Helpers.WindowMappings.WindowInstance(AssociatedWindows.ElementAt(windowIndex));
                        if (windowToClose != null)
                        {
                            windowToClose.Tag = "NOWARN";
                            windowToClose.Close();
                            Helpers.WindowMappings.DisposeWindowInstance(AssociatedWindows.ElementAt(windowIndex));
                        }
                    }
                }
                return;
            }

            DialogParameters parameters = new DialogParameters();
            parameters.Content = Application.Helpers.Constants.Common.AppExitConfirmation;
            bool? dialogResult = null;
            parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
            RadWindow.Confirm(parameters);
            if (dialogResult == true)
            {
                System.Windows.Application.Current.Shutdown();
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion

        #region Keyboard support methods
        public bool AlwaysShowKeyboard { get; set; }

        public void textbox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (keyboard != null)
            {
                if (!AlwaysShowKeyboard)
                {
                    keyboard.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        public void textbox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!AlwaysShowKeyboard)
            {
                ShowKeyboard(((TextBox)sender).Tag);
            }
        }
        void PasswordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!AlwaysShowKeyboard)
            {
                ShowKeyboard(((PasswordBox)sender).Tag);
            }
        }
        void PasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (keyboard != null)
            {
                if (!AlwaysShowKeyboard)
                {
                    keyboard.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
        public void ShowKeyboard(object obj = null)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Start:ShowKeyboard][Window.Title=" + this.Title + "]");
            try
            {
                if (keyboard == null)
                {
                    IEnumerable<SalesLogicExpress.Views.Common.Keyboard> keyboardInstance = FindVisualChildren<SalesLogicExpress.Views.Common.Keyboard>(this);
                    keyboard = keyboardInstance.Count() == 0 ? null : keyboardInstance.First();
                }
                if (keyboard != null)
                {
                    keyboard.Tag = obj;
                    keyboard.Visibility = System.Windows.Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][ShowKeyboard][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][End:ShowKeyboard][Window.Title=" + this.Title + "]");
        }

        private void GridViewEditStart(object sender, GridViewBeginningEditRoutedEventArgs e)
        {
            ToggleKeyboardForGridCellEdit();
        }
        async void ToggleKeyboardForGridCellEdit()
        {
            // wait for 100ms, let the grid render the edit template for the cell, 
            // and then find textbox instance inside the cell
            System.Threading.Thread.Sleep(100);

            // This call is async (non ui thread) so that the main method continues 
            // executing and this runs in background
            await Task.Run(() =>
            {
                // Since we want to access the keyboard which is in then UI thread, we will use the Application
                // Dispatcher to invoke UI related methods for accessing the currently focussed element
                // and hook keyboard if needed.
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action(() =>
                    {
                        FrameworkElement focussedElement = System.Windows.Input.Keyboard.FocusedElement as FrameworkElement;

                        if (focussedElement != null)
                        {
                            if (focussedElement.GetType() == typeof(TextBox))
                            {
                                ((TextBox)focussedElement).LostFocus -= textbox_LostFocus;
                                ((TextBox)focussedElement).LostFocus += textbox_LostFocus;
                                ((TextBox)focussedElement).GotFocus -= textbox_GotFocus;
                                ((TextBox)focussedElement).GotFocus += textbox_GotFocus;
                                ShowKeyboard(focussedElement.Tag);
                            }
                        }
                    }
                ));
            });

        }
        #endregion

        #region Orientation change support method
        protected virtual void DisplaySettingsChanged(object sender, EventArgs e)
        {

            if (SystemParameters.PrimaryScreenWidth > SystemParameters.PrimaryScreenHeight)
            {
                WindowOrientation = WindowOrientation.Landscape;
            }
            else
            {
                WindowOrientation = WindowOrientation.Portrait;

            }
            if (this.IsActive)
            {
                //  this.WindowState = System.Windows.WindowState.Maximized;
            }
        }
        #endregion

        #region Helper methods to find nested child controls
        public static IEnumerable<T> FindLogicalChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                foreach (object rawChild in LogicalTreeHelper.GetChildren(depObj))
                {
                    if (rawChild is DependencyObject)
                    {
                        DependencyObject child = (DependencyObject)rawChild;
                        if (child is T)
                        {
                            yield return (T)child;
                        }

                        foreach (T childOfChild in FindLogicalChildren<T>(child))
                        {
                            yield return childOfChild;
                        }
                    }
                }
            }
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = System.Windows.Media.VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }
                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        #endregion

        #region Helper methods for showing modal dialogs/alerts/confirm dialogs
        private object DialogWindowExecute(DialogWindow action)
        {

            // we have different modal dialog templates in a resource file
            // Resource file is merged in my current window
            // using Resource manager, extract template with x:Name or x:Key as action.title
            // Set content to rad window
            string contentTemplate = WindowMappings.GetResourceName(action.TemplateKey);
            RadWindow dialogWindow = new RadWindow();
            dialogWindow.Owner = this;
            dialogWindow.Name = action.TemplateKey;
            dialogWindow.ModalBackground = Brushes.Gray;
            dialogWindow.MaxWidth = this.ActualWidth / 1.2;
            dialogWindow.MaxHeight = this.ActualHeight / 1.2;
            dialogWindow.Header = action.Title;
            dialogWindow.ContentTemplate = this.FindResource(contentTemplate) as DataTemplate;
            dialogWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            dialogWindow.WindowState = System.Windows.WindowState.Normal;
            dialogWindow.CanMove = false;
            dialogWindow.ResizeMode = System.Windows.ResizeMode.NoResize;
            dialogWindow.HideMaximizeButton = true;
            dialogWindow.HideMinimizeButton = true;
            dialogWindow.DataContext = action.Payload != null ? action.Payload : this.DataContext;
            dialogWindow.SetBinding(Window.ContentProperty, "");
            dialogWindow.Closed += dialogWindow_Closed;
            dialogWindow.Loaded += dialogWindow_Loaded;
            dialogWindow.ShowDialog();
            action.Cancelled = dialogWindow.DialogResult == null ? true : false;
            action.Confirmed = dialogWindow.DialogResult == null ? false : dialogWindow.DialogResult.Value;
            return null;
        }

        private void dialogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            AttachKeyboard();
        }


        private void dialogWindow_Closed(object sender, WindowClosedEventArgs e)
        {

            if (DefaultFocusElement != null)
                DefaultFocusElement.Focus();
        }
        private object ConfirmWindowExecute(ConfirmWindow action)
        {
            DialogParameters parameters = new DialogParameters();
            parameters.Content = action.Message;
            bool? dialogResult = null;
            parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
            RadWindow.Confirm(parameters);
            if (dialogResult == null)
            {
                action.Cancelled = true;
                return null;
            }
            action.Confirmed = (bool)dialogResult.Value;
            return null;
        }
        private object AlertWindowExecute(SalesLogicExpress.Application.Helpers.AlertWindow action)
        {
            DialogParameters dp = new DialogParameters();
            dp.Header = action.Header;
            // dp.IconContent = action.MessageIcon;
            dp.Content = action.Message;
            RadWindow.Alert(dp);
            return null;
        }
        #endregion

    }
    /// <summary>
    /// Taskbar Class is used to Show/Hide Windows Taskbar.
    /// </summary>
    public class Taskbar
    {
        [DllImport("user32.dll")]
        private static extern int FindWindow(string className, string windowText);

        [DllImport("user32.dll")]
        private static extern int ShowWindow(int hwnd, int command);

        [DllImport("user32.dll")]
        public static extern int FindWindowEx(int parentHandle, int childAfter, string className, int windowTitle);

        [DllImport("user32.dll")]
        private static extern int GetDesktopWindow();

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 1;

        protected static int Handle
        {
            get
            {
                return FindWindow("Shell_TrayWnd", "");
            }
        }

        protected static int HandleOfStartButton
        {
            get
            {
                int handleOfDesktop = GetDesktopWindow();
                int handleOfStartButton = FindWindowEx(handleOfDesktop, 0, "button", 0);
                return handleOfStartButton;
            }
        }

        private Taskbar()
        {
            // hide ctor
        }

        public static void Show()
        {
            ShowWindow(Handle, SW_SHOW);
            ShowWindow(HandleOfStartButton, SW_SHOW);
        }

        public static void Hide()
        {
            ShowWindow(Handle, SW_HIDE);
            ShowWindow(HandleOfStartButton, SW_HIDE);
        }
    }
}
