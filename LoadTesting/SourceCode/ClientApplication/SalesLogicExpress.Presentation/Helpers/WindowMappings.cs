﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
namespace SalesLogicExpress.Presentation.Helpers
{
    public class WindowMappings
    {
        static Dictionary<ViewModelMappings.View, WindowMap> windowMappings = new Dictionary<ViewModelMappings.View, WindowMap>();
        static Dictionary<string, string> resourceMappings = new Dictionary<string, string>();
        public static System.Windows.Window currentWindow, nextWindow;
        private static void LoadMappings()
        {
            if (resourceMappings.Count == 0)
            {
                resourceMappings.Add("VerifyPickOrders", "ModalDialogForTemplate");
                resourceMappings.Add("ManualPickReason", "ModelDialogForManualPick");
                resourceMappings.Add("AllOpenOrdersForItem", "ModelDialogForOpenOrderList");
                resourceMappings.Add("MoveDailyStop", "ModalDialogMoveStop");
                resourceMappings.Add("VoidOrderReason", "ModelDialogForVoidOrderReason");
                resourceMappings.Add("AddNewContact", "ModalDialogForAddNewContact");
                resourceMappings.Add("CashDelivery", "ModelDialogForCashDelivery");
                resourceMappings.Add("ChargeOnAccount", "ModelDialogForChargeOnAccount");
                resourceMappings.Add("EditExistingContact", "ModalDialogForEditContact");
                resourceMappings.Add("EditExistingPhone", "ModalDialogForEditPhone");
                resourceMappings.Add("EditExistingEmail", "ModalDialogForEditEmail");
                resourceMappings.Add("AddNewPhone", "ModalDialogForAddPhone");
                resourceMappings.Add("AddNewEmail", "ModalDialogForAddEmail");
                resourceMappings.Add("OpenPreOrderDialog", "PreOrderDialog");
            }
            if (windowMappings.Count == 0)
            {
                windowMappings.Add(ViewModelMappings.View.OrderTemplate, new WindowMap(typeof(Views.OrderTemplate), null, null));
                windowMappings.Add(ViewModelMappings.View.ServiceRoute, new WindowMap(typeof(Views.ServiceRoute), null, null));
                windowMappings.Add(ViewModelMappings.View.Order, new WindowMap(typeof(Views.Order), null, null));
                windowMappings.Add(ViewModelMappings.View.ConfirmAndSign, new WindowMap(typeof(Views.ConfirmAndSignOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.PreviewOrder, new WindowMap(typeof(Views.PreviewOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.PickOrder, new WindowMap(typeof(Views.PickOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.UserLogin, new WindowMap(typeof(Views.UserLogin), null, null));
                windowMappings.Add(ViewModelMappings.View.CustomerHome, new WindowMap(typeof(Views.CustomerHome), null, null));
                windowMappings.Add(ViewModelMappings.View.DeliveryScreen, new WindowMap(typeof(Views.DeliveryScreen), null, null));
                windowMappings.Add(ViewModelMappings.View.RouteHome, new WindowMap(typeof(Views.RouteHome), null, null));
                windowMappings.Add(ViewModelMappings.View.PreOrder, new WindowMap(typeof(Views.PreOrder), null, null));
            }
        }
        public static string GetResourceName(string resourceKey)
        {
            LoadMappings();
            return resourceMappings[resourceKey];
        }
        public static Type GetWindow(ViewModelMappings.View viewName)
        {
            LoadMappings();
            return windowMappings[viewName].window;
        }
        /// <summary>
        /// This method makes sure that we have only one instance per window open/alive when users 
        /// navigates between different screens.
        /// 
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="action"></param>
        /// <returns>returns Window object which is reffered by the viewName parameters</returns>
        public static object WindowInstance(ViewModelMappings.View viewName, NavigateToView action)
        {
            LoadMappings();
            System.Windows.Window windowInstance;
            Type window = Helpers.WindowMappings.GetWindow(action.NextViewName);
            if (windowMappings[viewName].windowInstance != null)
            {
                return windowMappings[viewName].windowInstance;
            }
            else
            {
                if (action.Payload != null)
                {
                    windowInstance = (System.Windows.Window)Activator.CreateInstance(window, action.Payload);
                }
                else
                {
                    windowInstance = (System.Windows.Window)Activator.CreateInstance(window);
                }
                windowMappings[viewName].windowInstance = windowInstance;
                return windowInstance;
            }
        }
        public static object WindowInstance(ViewModelMappings.View viewName)
        {
            if (windowMappings[viewName].windowInstance != null)
            {
                return windowMappings[viewName].windowInstance;
            }
            return null;
        }
        internal static void DisposeWindowInstance(ViewModelMappings.View view)
        {
            if (windowMappings[view].windowInstance != null)
            {
                windowMappings[view].windowInstance = null; ;
            }
        }
    }
    internal class WindowMap
    {
        public Type window;
        public object payload;
        public object windowInstance;
        public WindowMap(Type window, object payload, object windowInstance)
        {
            this.window = window;
            this.payload = payload;
            this.windowInstance = windowInstance;
        }
    }
}
