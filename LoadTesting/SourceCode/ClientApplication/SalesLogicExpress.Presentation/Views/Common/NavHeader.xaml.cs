﻿using System.Windows.Controls;
using System;
using SalesLogicExpress.Application.ViewModels;
namespace SalesLogicExpress.Views.Common
{
    /// <summary>
    /// Interaction logic for NavHeader.xaml
    /// </summary>
    public partial class NavHeader : UserControl
    {
        public NavHeader()
        {
            InitializeComponent();
        }

        public void ToggleLastSyncLabel(bool show)
        {
            LastSyncedDateLabel.Visibility = show ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;


        }

        private void CloseDropdown_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            RadDropDownButtonactive.IsOpen = false;
            RadDropDownButtondeactive.IsOpen = false;
        }

    






    }
}
