﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using SalesLogicExpress.Presentation.Helpers;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using System.ComponentModel;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for OrderTemplate.xaml
    /// </summary>
    public partial class OrderTemplate : BaseWindow
    {
        //ViewModels.OrderTemplate OrderViewModel;
        ViewModels.OrderTemplate viewModel = new ViewModels.OrderTemplate();
        object viewModelPayload;
        public OrderTemplate(object payload)
        {
            viewModelPayload = payload;
            InitializeComponent();
            this.Activated += WindowActivated;
            this.Loaded += OrderTemplate_Loaded;
        }
        //********************************************
        // TODO : Check for transition state for enabling/disabling buttons or other action elements when transiting
        //********************************************
        //this.TransitionStateChanged += OrderTemplate_TransitionStateChanged;
        //void OrderTemplate_TransitionStateChanged(object sender, BaseWindow.TransitionStateEventArgs e)
        //{
        //    viewModel.IsInTransition = e.State == TransitionState.Started ? true : false;
        //}
        //********************************************
        void viewModel_ModelChanged(object sender, ViewModels.OrderTemplate.ModelChangeArgs e)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                   new Action(() =>
                   {
                       BindOrderHistory(viewModel.OrderHistoryHeaders, viewModel.OrderHistory);
                   }
           ));
        }

        public void BindOrderHistory(DataTable OrderHistoryHeaders, DataTable OrderHistoryItems)
        {
            List<Telerik.Windows.Controls.GridViewDataColumn> columnsToAdd = new List<Telerik.Windows.Controls.GridViewDataColumn>();
            if (OrderHistoryHeaders.Rows.Count > 0)
            {
                for (int i = 0; i < OrderHistoryHeaders.Rows.Count; i++)
                {
                    Telerik.Windows.Controls.GridViewDataColumn column = new Telerik.Windows.Controls.GridViewDataColumn();
                    column.HeaderCellStyle = (Style)this.FindResource("ColumnHeaderStyle");
                    string bindingName = "H" + (i + 1) + "_Qty";
                    ViewModels.Order.SalesHeader s = new ViewModels.Order.SalesHeader(OrderHistoryHeaders.Rows[i]["OrderNumber"].ToString(), OrderHistoryHeaders.Rows[i]["OrderDate"].ToString());
                    //column.Header = dt.Rows[i]["OrderNumber"].ToString();
                    column.Tag = OrderHistoryHeaders.Rows[i]["OrderNumber"].ToString() + "|" + OrderHistoryHeaders.Rows[i]["OrderDate"].ToString();
                    OrderHistoryItems.Columns[bindingName].ColumnName = column.Tag.ToString();
                    column.DataMemberBinding = new Binding(column.Tag.ToString());
                    column.IsFilterable = false;
                    column.IsSortable = false;
                    column.HeaderTextAlignment = TextAlignment.Center;
                    column.TextAlignment = TextAlignment.Right;
                    columnsToAdd.Add(column);

                    if (i == 9) break;
                }
                for (int index = columnsToAdd.Count - 1; index >= 0; index--)
                {
                    GRD_OrderHistory.Columns.Insert(2, columnsToAdd[index]);
                }

                for (int i = 0; i < OrderHistoryItems.Rows.Count; i++)
                {
                    double averageQty = Convert.ToDouble(OrderHistoryItems.Rows[i]["AverageQty"]);
                    AverageStopQuantity(averageQty);
                }
            }
        }

        public double AverageStopQuantity(double avgqty)
        {
            return avgqty;
        }

        void OrderTemplate_Loaded(object sender, RoutedEventArgs e)
        {
            SetNavigationDefaults();
            viewModel = new ViewModels.OrderTemplate(viewModelPayload);
            viewModel.ModelChanged += viewModel_ModelChanged;
            viewModel.MessageToken = this.Token;
            this.DataContext = viewModel;// new SalesLogicExpress.Application.ViewModels.OrderTemplate();
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Order Template";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.OrderTemplate;

        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
            }
        }



        private void OrderTemplateGrid_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e)
        {
            if (e.Cell.Column.UniqueName == "SeqNo")
            {
                string strSeqNo = ((Telerik.Windows.Controls.RadNumericUpDown)(e.EditingElement)).ContentText.ToString();
                if (strSeqNo != e.OldData.ToString())
                {
                    viewModel.OldSequenceNo = Convert.ToInt32(e.OldData.ToString());
                    if (!(e.Cell.DataContext == null))
                        viewModel.GridCellEditEnded.Execute(e.Cell.DataContext);

                    OrderTemplateGrid.SortDescriptors.Add(viewModel.SortGrid());

                }
                OrderTemplateGrid.SortDescriptors.Add(viewModel.SortGrid());

            }

            if (e.Cell.Column.UniqueName == "OrderQty")
            {
                //TODO: this function is not getting called, need to place it some where else
                viewModel.UpdateAvailableQty.Execute(e.Cell.DataContext);
            }


        }

        private void BtnAddToOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OrderTemplateGrid.SortDescriptors.Add(viewModel.SortGrid());
            }
            catch (Exception es)
            {

            }
        }

        private void CloseDropdown_Click(object sender, RoutedEventArgs e)
        {
             DropdownItems.IsOpen = false;
        }

        private void BaseWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (bool.Parse(e.NewValue.ToString()) == true)
            {
                viewModel.UpdateTemplateItemsFromOrder.Execute(null);
            }
        }





    }
}
