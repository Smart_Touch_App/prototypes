﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress.Presentation.Helpers;
using System.Text.RegularExpressions;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PickOrder.xaml
    /// </summary>
    public partial class PickOrder : BaseWindow
    {

        SalesLogicExpress.Application.ViewModels.PickOrder viewModel;
        public PickOrder(object payload)
        {
            InitializeComponent();
            this.Activated += WindowActivated;
            LoadWindow(payload);
            SetNavigationDefaults();
            AssociatedWindows.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
        }

        private void LoadWindow(object payload)
        {
            viewModel = new SalesLogicExpress.Application.ViewModels.PickOrder(payload);
            viewModel.MessageToken = this.Token;
            this.DataContext = viewModel;
            viewModel.selectedReason.StateChanged += selectedReason_StateChanged;
            this.AlwaysShowKeyboard = true;
            this.DefaultFocusElement = txt_OrderQty;
        }
        public override void RefreshWindow(object payload)
        {
            LoadWindow(payload);
            base.AlwaysShowNumericKeyboard();
            base.RefreshWindow(payload);
        }
        private void selectedReason_StateChanged(object sender, Application.ViewModels.OrderStateChangeArgs e)
        {
            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            AssociatedWindows = WindowsToClosed;
        }
        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pick Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.Order;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PickOrder;
        }

        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
                //ScanningStatusBlock.FontSize = 14;
            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
                //ScanningStatusBlock.FontSize = 16;

            }
        }

        private void PickOrderGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {

            if (e.AddedItems.Count > 0)
            {
                viewModel.SetPickItemLabels.Execute(e.AddedItems[0]);
                //lbl_pickItemNo.Text = "";
                txt_OrderQty.Focus();
            }

        }

        private void txt_OrderQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var dc = (SalesLogicExpress.Application.ViewModels.PickOrder)this.DataContext;
                dc.ManualEnteredQty = txt_OrderQty.Text == "" ? 0 : int.Parse(txt_OrderQty.Text.ToString());
                dc.IsManualPick = true;
                dc.ManualPick.Execute(dc.IsAddingToPalette ? PickOrderGrid.SelectedItem : ExceptionGrid.SelectedItem);
                dc.IsManualPick = false;

            }
        }

        private void txt_OrderQty_GotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }

        private void ExceptionGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //FrameworkElement originalSender = e.OriginalSource as FrameworkElement;
            //if (originalSender != null)
            //{ 
            //var row = originalSender.Parent<GridViewRowPresenter>();
            //         if (row != null)
            //         {
            //             MessageBox.Show("The double-clicked row is " + ((Player)row.DataContext).Name);
            //         }
            //}

        }

        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
            var dc = (SalesLogicExpress.Application.ViewModels.PickOrder)this.DataContext;

            if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null)
                PickOrderGrid.SelectedItem = (((FrameworkElement)sender)).DataContext;

            //dc.ManualEnteredQty = Convert.ToInt32(e.NewValue);
            //dc.IsManualPick = true;
            //dc.ManualPick.Execute((((FrameworkElement)sender)).DataContext);
            //dc.IsManualPick = false;

        }

        private void RadButton_Click(object sender, RoutedEventArgs e)
        {
            KeyPadPanel.Visibility = KeyPadPanel.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
        }

        private void Btn_Hold_Click(object sender, RoutedEventArgs e)
        {

        }

        private void txt_OrderQty_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                e.Handled = !(new Regex(@"^\d{0,4}(\d{0,2})?$").IsMatch(e.Text));

            }
            catch (Exception ex)
            {

                // log.Error("Page:FinalOrderScreen.xaml.cs,Method:UOMPrice_PreviewTextInput, Message: " + ex.Message);

            }
        }

        private void btn_Deliver_Click(object sender, RoutedEventArgs e)
        {
            //List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            //AssociatedWindows = WindowsToClosed;
        }

        private void RCB_ManualPickReasonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            viewModel.ErrorText = "";
        }

        private void RadNumericUpDown_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void RadNumericUpDown_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btn_RemoveExceptions_Click(object sender, RoutedEventArgs e)
        {
            PickOrderGrid.SelectedItems.Clear();
        }

        private void txt_PaymentAmt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^-{0,1}\\d+\\.{0,1}\\d*$");
            e.Handled = !regex.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void txt_ChequeNo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void txt_ApprovalCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void txt_PaymentAmt_TextChanged(object sender, TextChangedEventArgs e)
        {
            //TextBox txt = sender as TextBox;
            //txt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private void txt_PaymentAmt_LostFocus(object sender, RoutedEventArgs e)
        {
            //btn_DeliverToCustomer_GotFocus(sender, e);
            //DataTemplate dt = FindResource("ModelDialogForCashDelivery") as DataTemplate;

        }

        private void btn_DeliverToCustomer_GotFocus(object sender, RoutedEventArgs e)
        {
            //FindResource("");
        }

        private void txt_PaymentAmt_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

    }
}
