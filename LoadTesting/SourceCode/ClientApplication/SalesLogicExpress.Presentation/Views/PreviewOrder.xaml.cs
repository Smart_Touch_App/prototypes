﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Windows.Controls;
using SalesLogicExpress.Views.Common;
using System.ComponentModel;

using System.Collections.ObjectModel;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PreviewOrder.xaml
    /// </summary>
    public partial class PreviewOrder : BaseWindow
    {
        SalesLogicExpress.Application.ViewModels.PreviewOrder viewmodel;
        object viewModelPayload;
        bool GoToCreateOrder;
        public PreviewOrder(object payload)
        {
            viewModelPayload = payload;
            InitializeComponent();
            this.Activated += WindowActivated;
            this.Loaded += PreviewOrder_Loaded;
        }

        void PreviewOrder_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            viewmodel = new SalesLogicExpress.Application.ViewModels.PreviewOrder(viewModelPayload);
            viewmodel.MessageToken = Token;
            MainGrid.DataContext = viewmodel;
            SetNavigationDefaults();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.StaticPropertyChanged += CommonNavInfo_StaticPropertyChanged;

        }
        public PreviewOrder()
        {

        }
        public override void HideWindow()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.StaticPropertyChanged -= CommonNavInfo_StaticPropertyChanged;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
            base.HideWindow();
        }
        ~PreviewOrder()
        {

        }
        void CommonNavInfo_StaticPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBackwardNavigationEnabled")
            {
                if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled)
                {

                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Preview Order";
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Order";
                    grdSignAndPrint.Visibility = System.Windows.Visibility.Hidden;
                    SignAndPrint.Visibility = System.Windows.Visibility.Visible;
                   // TestInkCanvas.Strokes.Clear();
                }
                else
                {
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Sign and Print";
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Preview Order";
                    grdSignAndPrint.Visibility = System.Windows.Visibility.Visible;
                    SignAndPrint.Visibility = System.Windows.Visibility.Collapsed;
                }

            }
        }

        private void WindowActivated(object sender, EventArgs e)
        {
           // TestInkCanvas.Strokes.Clear();
            SetNavigationDefaults();

        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Preview Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.Order;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PreviewOrder;
        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {
                this.OrderItemGrid.SetValue(Grid.ColumnSpanProperty, (object)2);
                this.orderSummaryGrid.SetValue(Grid.RowProperty, (object)1);
                this.orderSummaryGrid.SetValue(Grid.ColumnSpanProperty, (object)2);
                this.orderSummaryGrid.SetValue(Grid.ColumnProperty, (object)0);
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this.OrderItemGrid.ClearValue(Grid.ColumnSpanProperty);
                this.orderSummaryGrid.ClearValue(Grid.ColumnSpanProperty);
                this.orderSummaryGrid.SetValue(Grid.RowProperty, (object)0);
                this.orderSummaryGrid.SetValue(Grid.ColumnProperty, (object)1);
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void RadButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            viewmodel.canvasWidth = this.TestInkCanvas.ActualWidth;
            viewmodel.canvasHeight = this.TestInkCanvas.ActualHeight;
            viewmodel.InkCanvas = this.TestInkCanvas;
            viewmodel.SaveSign.Execute(this.TestInkCanvas.Strokes);
        }

        private void SignAndPrint_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Sign And Print";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "PreviewOrder";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = false;
        }

    }
}
