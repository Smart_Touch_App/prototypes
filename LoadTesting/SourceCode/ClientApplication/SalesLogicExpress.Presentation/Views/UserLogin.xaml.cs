﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using ApplicationHelpers = SalesLogicExpress.Application.Helpers;
using System.Configuration;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for UserLogin.xaml
    /// </summary>
    public partial class UserLogin : BaseWindow
    {
        public UserLogin()
        {
            InitializeComponent();
            ViewModels.UserLogin viewModel = new ViewModels.UserLogin();
            viewModel.MessageToken = this.Token;
            viewModel.DeviceID =  Application.Helpers.ResourceManager.DeviceID;
            viewModel.LoginID = "";
            viewModel.Password = "*****";
            viewModel.Route = "";
            this.DataContext = viewModel;
            //string id = DeviceManager.Device.GenerateDeviceID();
            string mobilinkServer = ConfigurationManager.AppSettings["MoblinkServerIP"].ToString();
            SalesLogicExpress.Application.Managers.SetupManager man = new Application.Managers.SetupManager();
            man.ConfigureRemoteDbEndPoint(mobilinkServer);

        }
    }
}
