﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for DeliveryScreen.xaml
    /// </summary>
    public partial class DeliveryScreen : BaseWindow
    {
        SalesLogicExpress.Application.ViewModels.DeliveryScreen viewmodel;
        public DeliveryScreen(object payload)
        {
            InitializeComponent();
            viewmodel = new SalesLogicExpress.Application.ViewModels.DeliveryScreen(payload);
            this.Activated += WindowActivated;
            viewmodel.MessageToken = Token;
            MainGrid.DataContext = viewmodel;
            viewmodel.ReasonCodeList.StateChanged += ReasonCodeList_StateChanged;
            // Navigation information
            SetNavigationDefaults();

        }

        void ReasonCodeList_StateChanged(object sender, Application.ViewModels.OrderStateChangeArgs e)
        {
            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            AssociatedWindows = WindowsToClosed;
        }
        public DeliveryScreen()
        {
            InitializeComponent();
        }
        private void WindowActivated(object sender, EventArgs e)
        {
            // TestInkCanvas.Strokes.Clear();
            SetNavigationDefaults();

        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Delivery Screen";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.DeliveryScreen;

            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PickOrder);
            AssociatedWindows = WindowsToClosed;
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (TestInkCanvas.Strokes.Count == 0)
            {
                viewmodel.SaveSign.Execute(this.TestInkCanvas.Strokes);
                return;
            }

            SignAndAccept.Text = "Deliver";
            btnPayment.Visibility = System.Windows.Visibility.Visible;
            btn_clear.Visibility = System.Windows.Visibility.Collapsed;
            btnDeliver.Visibility = System.Windows.Visibility.Visible;
            Accept.Visibility = System.Windows.Visibility.Collapsed;
            viewmodel.IsCanvasEnable = false;
        }

        private void btnDeliver_Click(object sender, RoutedEventArgs e)
        {
            btnVoid.Visibility = System.Windows.Visibility.Collapsed;
            btn_Hold.Visibility = System.Windows.Visibility.Collapsed;
            SignAndAccept.Text = "Delivered";
            btnDeliver.Visibility = System.Windows.Visibility.Collapsed;
            viewmodel.OrderDelivered.Execute(null);

        }

        private void btnVoid_Click(object sender, RoutedEventArgs e)
        {
            //List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            //AssociatedWindows = WindowsToClosed;
        }

        private void btn_Hold_Click(object sender, RoutedEventArgs e)
        {
            //List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PickOrder);
            //AssociatedWindows = WindowsToClosed;
        }
    }
}
