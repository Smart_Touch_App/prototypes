﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using navbar = SalesLogicExpress.Views.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Collections.ObjectModel;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ServiceRoute.xaml
    /// </summary>
    public partial class ServiceRoute : BaseWindow
    {
        object _payload;
        public ServiceRoute(object payload)
        {
            InitView(payload);
        }

        private void InitView(object payload)
        {
            _payload = payload;
            SetNavigationDefaults();
            InitializeComponent();
            this.Activated += WindowActivated;
            this.Loaded += ServiceRoute_Loaded;
            viewmodels.ServiceRoute viewModel = new viewmodels.ServiceRoute(payload);
            this.DataContext = viewModel;
            viewModel.MessageToken = this.Token;
            viewmodels.CommonNavInfo cminfo = new viewmodels.CommonNavInfo();
            NavigationHeader.DataContext = cminfo;
        }

        public ServiceRoute()
        {
            InitView(null);
        }
        private void ServiceRoute_Loaded(object sender, RoutedEventArgs e)
        {
            SetNavigationDefaults();
        }
     
        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);

            // Refreshing the DataContext
            //TODO : find a better way of refreshing the context
            viewmodels.ServiceRoute viewModel = new viewmodels.ServiceRoute(_payload);
            this.DataContext = viewModel;
            viewModel.MessageToken = this.Token;
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Service Route";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ServiceRoute;
         
        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {

            }
        }

    }
        
}
