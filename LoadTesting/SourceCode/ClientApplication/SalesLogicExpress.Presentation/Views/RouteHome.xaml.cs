﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using navbar = SalesLogicExpress.Views.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Collections.ObjectModel;


namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for RouteHome.xaml
    /// </summary>
    public partial class RouteHome : BaseWindow
    {
        public RouteHome(object payload)
        {
            InitializeComponent();
            SetNavigationDefaults();
            InitializeComponent();
            this.Activated += WindowActivated;
            this.Loaded += ServiceRoute_Loaded;
            viewmodels.RouteHome viewModel = new viewmodels.RouteHome(payload);
            this.DataContext = viewModel;
            viewModel.MessageToken = this.Token;
            viewmodels.CommonNavInfo cminfo = new viewmodels.CommonNavInfo();
            NavigationHeader.DataContext = cminfo;
        }
       
        private void ServiceRoute_Loaded(object sender, RoutedEventArgs e)
        {
            SetNavigationDefaults();
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Route Home";
         
        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {

            }
        }
      
    }
}
