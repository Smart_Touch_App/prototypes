﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress.Presentation.Helpers;
using Telerik.Windows;
using Telerik.Windows.Controls;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using System.Collections.ObjectModel;
using System.Data;
using Telerik.Windows.Controls.GridView;
using System.Text.RegularExpressions;


namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for Order.xaml
    /// </summary>
    public partial class Order : BaseWindow
    {
        decimal QtyBeforeEdit;
        object viewModelPayload;
        ViewModels.Order OrderViewModel;
        public Order()
        {
            InitializeComponent();
        }
        public Order(object payload)
        {
            viewModelPayload = payload;
            InitializeComponent();
            this.Activated += Order_Activated;
            this.Loaded += Order_Loaded;
        }
        //Add window to associated window list .
        private void reasonCode_StateChanged(object sender, ViewModels.OrderStateChangeArgs e)
        {

            if (e.State == ViewModels.OrderState.Void)
            {
                List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
                WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
                AssociatedWindows = WindowsToClosed;
            }
            else if (e.State == ViewModels.OrderState.Hold)
            {
                List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
                WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
                AssociatedWindows = WindowsToClosed;
            }
        }
        public void BindOrderHistory(DataTable OrderHistoryHeaders, DataTable OrderHistoryItems)
        {
            List<Telerik.Windows.Controls.GridViewDataColumn> columnsToAdd = new List<Telerik.Windows.Controls.GridViewDataColumn>();
            if (OrderHistoryHeaders.Rows.Count > 0)
            {
                for (int i = 0; i < OrderHistoryHeaders.Rows.Count; i++)
                {
                    Telerik.Windows.Controls.GridViewDataColumn column = new Telerik.Windows.Controls.GridViewDataColumn();
                    column.HeaderCellStyle = (Style)this.FindResource("ColumnHeaderStyle");
                    string bindingName = "H" + (i + 1) + "_Qty";
                    ViewModels.Order.SalesHeader s = new ViewModels.Order.SalesHeader(OrderHistoryHeaders.Rows[i]["OrderNumber"].ToString(), OrderHistoryHeaders.Rows[i]["OrderDate"].ToString());
                    //column.Header = dt.Rows[i]["OrderNumber"].ToString();
                    column.Tag = OrderHistoryHeaders.Rows[i]["OrderNumber"].ToString() + "|" + OrderHistoryHeaders.Rows[i]["OrderDate"].ToString();
                    OrderHistoryItems.Columns[bindingName].ColumnName = column.Tag.ToString();
                    column.DataMemberBinding = new Binding(column.Tag.ToString());
                    column.IsFilterable = false;
                    column.IsSortable = false;
                    column.HeaderTextAlignment = TextAlignment.Center;
                    column.TextAlignment = TextAlignment.Right;
                    columnsToAdd.Add(column);

                    if (i == 9) break;
                }
                for (int index = columnsToAdd.Count - 1; index >= 0; index--)
                {
                    GRD_OrderHistory.Columns.Insert(2, columnsToAdd[index]);
                }
            }
        }
        private void Order_Loaded(object sender, RoutedEventArgs e)
        {
            OrderViewModel = new ViewModels.Order(viewModelPayload);
            this.DataContext = OrderViewModel;
            OrderViewModel.reasonCode.StateChanged += reasonCode_StateChanged;
            OrderViewModel.ModelChanged += OrderViewModel_ModelChanged;
            OrderViewModel.MessageToken = this.Token;
            SetNavigationDefaults();
        }
        void OrderViewModel_ModelChanged(object sender, ViewModels.Order.ModelChangeArgs e)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                  new Action(() =>
                  {
                      BindOrderHistory(OrderViewModel.OrderHistoryHeaders, OrderViewModel.OrderHistory);
                      gridHeight = ItemGridHolder.RowDefinitions[0].ActualHeight;
                      SalesLogicExpress.Application.Managers.OrderManager.PriceOvrCodes POC = new Application.Managers.OrderManager.PriceOvrCodes();
                      ((GridViewComboBoxColumn)this.grditems.Columns["PriceOVR"]).ItemsSource = POC.GetPriceOvrCodes();
                  }
          ));
        }

        private void Order_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();

        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Create Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Order Template";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.OrderTemplate;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.Order;
        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            //if (WindowOrientation == WindowOrientation.Portrait)
            //{
            //    CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
            //    ScanningStatusBlock.FontSize = 14;
            //}
            //if (WindowOrientation == WindowOrientation.Landscape)
            //{
            //    CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
            //    ScanningStatusBlock.FontSize = 16;

            //}
        }

        public void radExpander_Expanded(object sender, RadRoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void radExpander_Collapsed(object sender, RadRoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void BtnPreviewOrder_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void BtnBackToTemplateScreen_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void suggesteditemadd_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void grdItemList_Filtered(object sender, Telerik.Windows.Controls.GridView.GridViewFilteredEventArgs e)
        {
            // TODO: Implement this method

        }

        public void OnRadGridViewFilterOperatorsLoading(object sender, Telerik.Windows.Controls.GridView.FilterOperatorsLoadingEventArgs e)
        {
            // TODO: Implement this method

        }

        public void BtnAddToOrder_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void BtnSearchItem_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void BtnCancelSearch_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void txtSearchItem_TextChanged(object sender, TextChangedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void sb_lostfocus(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void txtSearchItem_KeyUp(object sender, KeyEventArgs e)
        {
            // TODO: Implement this method

        }

        public void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            // TODO: Implement this method

        }

        public void OpenExpander(object sender, TouchEventArgs e)
        {
            // TODO: Implement this method

        }

        public void OpenExpanderByStylus(object sender, StylusEventArgs e)
        {
            // TODO: Implement this method

        }

        public void gridSalesHeader_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // TODO: Implement this method

        }

        public void UOMPrice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // TODO: Implement this method
            try
            {
                e.Handled = !(new Regex(@"^\d{0,4}(\.\d{0,2})?$").IsMatch(e.Text));

            }
            catch (Exception ex)
            {

                // log.Error("Page:FinalOrderScreen.xaml.cs,Method:UOMPrice_PreviewTextInput, Message: " + ex.Message);

            }
        }

        public void TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            // TODO: Implement this method

        }

        public void grditems_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // TODO: Implement this method

        }

        public void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            // TODO: Implement this method

        }

        public void RadButton_Click_1(object sender, RoutedEventArgs e)
        {
            // TODO: Implement this method

        }

        public void grditems_RowValidating(object sender, Telerik.Windows.Controls.GridViewRowValidatingEventArgs e)
        {
            // TODO: Implement this method

        }

        public void grditems_RowEditEnded(object sender, Telerik.Windows.Controls.GridViewRowEditEndedEventArgs e)
        {
            // TODO: Implement this method
            if (!radExpander.IsExpanded && ItemGridHolder.Tag == "COLLAPSED")
            {
                ItemGridHolder.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Star);
                ItemGridHolder.Tag = string.Empty;
            }
        }

        public void grditems_Drop(object sender, DragEventArgs e)
        {
            // TODO: Implement this method

        }

        public void grditems_CellValidating(object sender, Telerik.Windows.Controls.GridViewCellValidatingEventArgs e)
        {
            // TODO: Implement this method
        }

        public void grditems_CellEditEnded(object sender, Telerik.Windows.Controls.GridViewCellEditEndedEventArgs e)
        {
            // TODO: Implement this method
            if (e.Cell.Column.UniqueName == "OrderQty" || e.Cell.Column.UniqueName == "UnitPrice" || e.Cell.Column.UniqueName == "UM")
            {
                OrderViewModel.SetExtendedPrice.Execute(e.Cell.DataContext);

                //if (QtyBeforeEdit != OrderQty)
                //    ((System.Windows.Controls.TextBlock)(e.Cell.ParentRow.Cells[9].Content)).Text = (string)((ActualQtyOnHand - OrderQty) < 0 ? 0 : (ActualQtyOnHand - OrderQty)).ToString();

                if (e.Cell.Column.UniqueName != "OrderQty")
                {
                    if (!Equals(e.OldData, e.NewData))
                    {
                        ViewModels.PreviewOrder.GlobalInkCanvas = new System.Windows.Controls.InkCanvas();
                    }
                }


            }
        }

        public void grditems_BeginningEdit(object sender, Telerik.Windows.Controls.GridViewBeginningEditRoutedEventArgs e)
        {
            if (e.Cell.Column.UniqueName == "OrderQty")
            {
                QtyBeforeEdit = decimal.Parse(e.Cell.Value.ToString());
                e.Cell.Column.Width = 130;
            }

            GridViewCell Cell = e.Cell;
            ToggleGridHeight(Cell);

        }
        double gridHeight;

        private void ToggleGridHeight(GridViewCell Cell)
        {
            if (grditems.Items.IndexOf(Cell.DataContext) >= 7 && !radExpander.IsExpanded && ItemGridHolder.ActualHeight > 500 && ItemGridHolder.Tag != "COLLAPSED")
            {
                gridHeight = gridHeight == 0 ? ItemGridHolder.RowDefinitions[0].ActualHeight : gridHeight;
                if (ItemGridHolder.Tag != "COLLAPSED")
                {
                    if (ItemGridHolder.RowDefinitions[0].ActualHeight == gridHeight)
                    {
                        double height = ItemGridHolder.RowDefinitions[0].ActualHeight - 200;
                        ItemGridHolder.RowDefinitions[0].Height = new GridLength(height, GridUnitType.Pixel);
                    }
                    ItemGridHolder.Tag = "COLLAPSED";
                }
                grditems.ScrollIntoView(Cell.DataContext);
            }
        }
        public void grditems_AddingNewDataItem(object sender, Telerik.Windows.Controls.GridView.GridViewAddingNewEventArgs e)
        {
            // TODO: Implement this method

        }

        private void RadNumericUpDown_ValueChanged(object sender, RadRangeBaseValueChangedEventArgs e)
        {
            if (e.OldValue != null && e.NewValue != null && !Equals(e.OldValue, e.NewValue))
            {
                if (ViewModels.PreviewOrder.GlobalInkCanvas != null)
                    ViewModels.PreviewOrder.GlobalInkCanvas.Strokes.Clear();
            }
        }

        private void btn_Hold_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btn_Void_Click(object sender, RoutedEventArgs e)
        {
          
        }
    }
}
