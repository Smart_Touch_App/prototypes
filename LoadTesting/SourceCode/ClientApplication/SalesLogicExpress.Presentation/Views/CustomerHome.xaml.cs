﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using SalesLogicExpress.Presentation.Helpers;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using System.ComponentModel;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for CustomerHome.xaml
    /// </summary>
    public partial class CustomerHome : BaseWindow
    {
        ViewModels.CustomerHome viewModel =null;
        public CustomerHome(object payload)
        {
            InitializeComponent();
            SetNavigationDefaults();
            viewModel = new ViewModels.CustomerHome(payload);
            viewModel.MessageToken = this.Token;
            this.DataContext = viewModel;//
            this.Activated += WindowActivated;
        }
        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
       
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ServiceRoute;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.CustomerHome;

        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {
               
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            viewModel.NewContact.PropertyLostFocus.Execute(sender);
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            viewModel.AddNewPhone.Execute(sender);
        }

        private void PhoneListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Telerik.Windows.Controls.RadListBox item = sender as RadListBox;

            viewModel.EditPhone.Execute(item);
        }

        private void EmailListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            viewModel.EditEmail.Execute(sender);
        }

    }
}
