﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Presentation.Controls
{
    public class CustomRadListBox: RadListBox
    {
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation;

        public event EventHandler DoubleTouchDown;
        public CustomRadListBox()
        {
            this.PreviewTouchDown += CustomRadListBox_PreviewTouchDown;
        }
        protected virtual void OnDoubleTouchDown()
        {
            if (DoubleTouchDown != null)
                DoubleTouchDown(this, EventArgs.Empty);
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            double dis = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation);
            bool tapsAreCloseInDistance = dis < 20;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(.7));

            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        void CustomRadListBox_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            if (IsDoubleTap(e))
            {
                OnDoubleTouchDown();
            }
        }
    }

    public class CustomerRadExpander : RadExpander
    {
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation = new Point();

         public event EventHandler DoubleTouchDown;
         public CustomerRadExpander()
        {
            this.PreviewTouchDown += CustomerRadExpander_PreviewTouchDown;
        }
        protected virtual void OnDoubleTouchDown()
        {
            if (DoubleTouchDown != null)
                DoubleTouchDown(this, EventArgs.Empty);
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            double dis = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation);
            bool tapsAreCloseInDistance = dis < 20;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(.7));

            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        void CustomerRadExpander_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            if (IsDoubleTap(e))
            {
                OnDoubleTouchDown();
            }
        }
    }

    public class CustomRadioButton : RadRadioButton
    {
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation;

        public event EventHandler DoubleTouchDown;
        public CustomRadioButton()
        {
            this.PreviewTouchDown += CustomRadioButton_PreviewTouchDown;
        }
        protected virtual void OnDoubleTouchDown()
        {
            if (DoubleTouchDown != null)
                DoubleTouchDown(this, EventArgs.Empty);
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            double dis = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation);
            bool tapsAreCloseInDistance = dis < 20;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(.7));

            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        void CustomRadioButton_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            if (IsDoubleTap(e))
            {
                OnDoubleTouchDown();
            }
        }
    }
}
 