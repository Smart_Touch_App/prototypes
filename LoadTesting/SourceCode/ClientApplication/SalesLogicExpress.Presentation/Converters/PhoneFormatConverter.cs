﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class PhoneFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string phoneno = Regex.Replace(value.ToString(), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            try
            {
                string fphoneno = (string.IsNullOrEmpty(phoneno) ? "" : " " + String.Format("{0:(###) ###-####}", Double.Parse(phoneno)));
                return fphoneno;
            }
            catch(Exception e) {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
