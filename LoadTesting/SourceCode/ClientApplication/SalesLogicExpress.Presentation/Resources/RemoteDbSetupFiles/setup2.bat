setlocal
echo Start executing setup2.bat... > setup.log

echo Start the Environment Variable Setup >> setup.log
call sqlanyenv.bat
echo End the Environment Variable Setup >> setup.log

REM --
REM -- Define the ODBC data sources with full path to database files
REM --
echo SA Bin file location : %__SABIN% >> setup.log

"%__SABIN%\dbdsn" -w dsn_remote_%2 -y -c "uid=DBA;pwd=sql;dbf=%cd%\remote.db;server=remote_eng"
if errorlevel 1 goto RunTimeError
echo DSN set successfully >> setup.log
REM --
REM -- Construct generic portion of remote databases, without synchronization subscriptions
REM --
echo Construct generic portion of remote databases, without synchronization subscriptions... >> setup.log
"%__SABIN%\dbinit" -b remote.db
if errorlevel 1 goto RunTimeErrorD:\Arvind\Projects\Wonderbiz\SourceCode\ClientApplication\SalesLogicExpress.Presentation\Resources\RemoteDbSetupFiles\remote_setup.sql
echo Setting Schema by running remote_setup.sql... >> setup.log
"%__SABIN%\dbisql" -onerror exit -c "dsn=dsn_remote_%2" read "remote_setup.sql"
if errorlevel 1 goto RunTimeError
echo Schema created successfully... >> setup.log
goto done

:RunTimeError
echo ###Error: A step in the sample script failed with an error in setup2.bat... > setup.log
SET errorlevel=1
goto done

:done
echo End execution of setup2.bat... >> setup.log
SET __SA=
SET __SABIN=
exit /b %errorlevel%
endlocalSetu