rem ***************************************************************************
rem Copyright (c) 2014 SAP AG or an SAP affiliate company. All rights reserved.
rem ***************************************************************************
rem This sample code is provided AS IS, without warranty or liability
rem of any kind.
rem 
rem You may use, reproduce, modify and distribute this sample code
rem without limitation, on the condition that you retain the foregoing
rem copyright notice and disclaimer as to the original code.  
rem 
rem *********************************************************************
:saenv
set __SA=C:\Program Files\SQL Anywhere 16
set __SABIN=%__SA%\Bin32
rem - are we on a 64-bit machine
if [%PROCESSOR_ARCHITECTURE%]==[AMD64] set __SABIN=%__SA%\BIN64
if [%PROCESSOR_ARCHITEW6432%]==[AMD64] set __SABIN=%__SA%\BIN64
echo SA Home : %__SABIN% >> setup.log
set __SASAMPLES=%SQLANYSAMP16%
if "%__SASAMPLES%" == "" goto setsasample
goto saenv_end

:setsasample
set __SASAMPLES=%__SA%\samples

:saenv_end
set __ERRLEVEL=0
