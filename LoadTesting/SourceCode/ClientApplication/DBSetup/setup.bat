@echo off

REM --
REM -- Pre-requisites: Run SQL Anywhere16  SANY16.msi to setup the environment for SQL Anywhere
REM --


REM -- Fetch Generic DB Scripts
echo Copy files required to setup RemoteDB
copy Setup_files_Remote\remote_setup.bat remote_setup.bat
copy Setup_files_Remote\remote_setup.sql remote_setup.sql
copy Setup_files_Remote\CustomizeRemote.sql CustomizeRemote.sql
copy Setup_files_Remote\sqlanyenv.bat sqlanyenv.bat

setlocal
call sqlanyenv.bat

REM -- %1 is the username passed while running the batch file
REM --
REM -- Make directories for the remote databases. Make different folders per route.
REM --
mkdir remote_POCDB_%2
if errorlevel 1 goto AlreadyExists
REM --
REM -- Define the ODBC data sources with full path to database files
REM --
"%__SABIN%\dbdsn" -w dsn_remote_%2 -y -c "uid=DBA;pwd=sql;dbf=%cd%\remote_POCDB_%2\remote.db;server=remote_POCDB"
if errorlevel 1 goto RunTimeError

REM --
REM -- Construct generic portion of remote databases, without synchronization subscriptions
REM --
"%__SABIN%\dbinit" remote_POCDB_%2\remote.db
if errorlevel 1 goto RunTimeError
"%__SABIN%\dbisql" -onerror exit -c "dsn=dsn_remote_%2" read "%~dp0remote_setup.sql"
if errorlevel 1 goto RunTimeError

REM --
REM -- Apply those pieces that are specific to each individual database
REM --
REM "%__SABIN%\dbisql" -onerror exit -c "dsn=dsn_remote_POCDB" read "%~dp0CustomizeRemote.sql" %1 
REM if errorlevel 1 goto RunTimeError
REM goto AlreadyExists


:AlreadyExists
echo Database for Route %2 already exists...
copy Setup_files_Remote\StartRemoteDB.bat remote_POCDB_%2\StartRemoteDB.bat
copy Setup_files_Remote\SyncRemote.bat remote_POCDB_%2\SyncRemote.bat
copy DEVICE_ID.txt remote_POCDB_%2\DEVICE_ID.txt
cd remote_POCDB_%2
start cmd /k call StartRemoteDB.bat

FOR /F "tokens=* delims=" %%y in (DEVICE_ID.txt) DO set device_id=%%y
echo Device_id is %device_id%

timeout /t 15
cd..
REM --
REM -- Authorize the user
REM --
"%__SABIN%\dbisql" -onerror exit -c "UID=dba;PWD=sql;Server=remote_eng;DBN=remote_db;ASTART=No" select app_user from busdta.user_master where app_user = '%1' and app_user_id in (select app_user_id from busdta.Route_User_Map where route_id = '%2' and Route_Id = (select Route_Id from BUSDTA.Route_Device_Map where Device_Id = '%device_id%')); OUTPUT TO 'temp0.txt'

FOR /F "tokens=* delims=" %%x in (temp0.txt) DO set var1=%%x

REM -- If Login is correct, Sync the respective data else do not sync.
REM echo Comparing %1 with %var1%
IF %var1% == '%1'  GOTO LoginSuccess 
IF %var1%  NEQ '%1'  GOTO LoginFail

goto done
:LoginSuccess
echo Login Successful, Customizing... %var1% = %1
"%__SABIN%\dbisql" -onerror exit -c "UID=dba;PWD=sql;Server=remote_eng;DBN=remote_db;ASTART=No" read "CustomizeRemote.sql" [%1]
timeout /t 5
echo Login Successful, performing sync...
"%__SABIN%\dbmlsync" -c "SERVER=remote_eng;DBN=remote_db;UID=dba;PWD=sql" -n busdta -u %1
goto done

:LoginFail
echo Login Failed, Invalid User Credentials...
goto done

:RunTimeError
echo Error: A step in the sample script failed with an error.
SET errorlevel=1
goto done

:done
SET __SA=
SET __SABIN=
exit /b %errorlevel%
endlocalSetu