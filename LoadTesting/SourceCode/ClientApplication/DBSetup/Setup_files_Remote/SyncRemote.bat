@echo off

setlocal
call sqlanyenv.bat

"%__SABIN%\dbmlsync" -c "SERVER=remote_eng;DBN=remote_db;UID=dba;PWD=sql" -n pub_validate_user -u validate_user
if errorlevel 1 goto RunTimeError
goto done

:RunTimeError
echo Error: Error occurred while synchronization. File 'SyncRemote.bat'.
SET errorlevel=1

:done
SET __SA=
SET __SABIN=
SET __SASAMPLES=
exit /b %errorlevel%
endlocal