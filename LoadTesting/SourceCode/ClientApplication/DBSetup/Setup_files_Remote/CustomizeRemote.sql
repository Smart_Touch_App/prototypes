-- ***************************************************************************
-- Copyright (c) 2014 SAP AG or an SAP affiliate company. All rights reserved.
-- ***************************************************************************
-- You may use, reproduce, modify and distribute this sample code without limitation, 
-- on the condition that you retain the foregoing copyright notice and disclaimer as to the original code.  
--
-- *******************************************************************

parameters ml_userid;
go


/*------------------------------------------------------------------------------
*                         Create publication 'busdta'.
*-----------------------------------------------------------------------------*/
CREATE PUBLICATION IF NOT EXISTS "busdta" (
	TABLE "MobileDataModel"."BUSDTA"."User_Role_Map",
	TABLE "MobileDataModel"."BUSDTA"."user_master",
	TABLE "MobileDataModel"."BUSDTA"."Route_User_Map",
	TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map",
	TABLE "MobileDataModel"."BUSDTA"."F90CA086",
	TABLE "MobileDataModel"."BUSDTA"."F90CA042",
	TABLE "MobileDataModel"."BUSDTA"."F90CA003",
	TABLE "MobileDataModel"."BUSDTA"."F56M0001",
	TABLE "MobileDataModel"."BUSDTA"."F56M0000",
	TABLE "MobileDataModel"."BUSDTA"."F42119",
	TABLE "MobileDataModel"."BUSDTA"."F42019",
	TABLE "MobileDataModel"."BUSDTA"."F4106",
	TABLE "MobileDataModel"."BUSDTA"."F4102",
	TABLE "MobileDataModel"."BUSDTA"."F4101",
	TABLE "MobileDataModel"."BUSDTA"."F41002",
	TABLE "MobileDataModel"."BUSDTA"."F40942",
	TABLE "MobileDataModel"."BUSDTA"."F40941",
	TABLE "MobileDataModel"."BUSDTA"."F4092",
	TABLE "MobileDataModel"."BUSDTA"."F4076",
	TABLE "MobileDataModel"."BUSDTA"."F4075",
	TABLE "MobileDataModel"."BUSDTA"."F4072" ("ADAST", "ADITM", "ADAN8", "ADIGID", "ADCGID", "ADOGID", "ADCRCD", "ADUOM", "ADMNQ", "ADEXDJ", "ADUPMJ", "ADTDAY"),
	TABLE "MobileDataModel"."BUSDTA"."F4071",
	TABLE "MobileDataModel"."BUSDTA"."F4070",
	TABLE "MobileDataModel"."BUSDTA"."F4015",
	TABLE "MobileDataModel"."BUSDTA"."F40073",
	TABLE "MobileDataModel"."BUSDTA"."F03012",
	TABLE "MobileDataModel"."BUSDTA"."F0150",
	TABLE "MobileDataModel"."BUSDTA"."F0116",
	TABLE "MobileDataModel"."BUSDTA"."F01151",
	TABLE "MobileDataModel"."BUSDTA"."F0115",
	TABLE "MobileDataModel"."BUSDTA"."F0101",
	TABLE "MobileDataModel"."BUSDTA"."F0014",
	TABLE "MobileDataModel"."BUSDTA"."F0006",
	TABLE "MobileDataModel"."BUSDTA"."F0005",
	TABLE "MobileDataModel"."BUSDTA"."F0004",
	TABLE "MobileDataModel"."BUSDTA"."Device_Master"
)
GO

/*------------------------------------------------------------------------------
*                           Create the user {ml_userid}.
*-----------------------------------------------------------------------------*/
/*IF NOT EXISTS( SELECT 1
	     FROM SYS.SYSSYNC
	    WHERE site_name = {ml_userid}
	      AND publication_id IS NULL ) THEN
    CREATE SYNCHRONIZATION USER {ml_userid}
END IF;*/
CREATE SYNCHRONIZATION USER {ml_userid}	
GO

/*------------------------------------------------------------------------------
*         Create subscription 'busdta_all' to 'busdta' for {ml_userid}.
*-----------------------------------------------------------------------------*/
CREATE SYNCHRONIZATION SUBSCRIPTION "busdta_all" TO "busdta" FOR {ml_userid}
	TYPE tcpip ADDRESS 'host=localhost;port=2439'
	SCRIPT VERSION 'busdta'
GO

/*------------------------------------------------------------------------------
*                Create synchronization profile 'busdta_{ml_userid}'.
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE SYNCHRONIZATION PROFILE "pro_busdta_all" 'Subscription=busdta_all'
GO

COMMIT
GO

COMMIT
GO
