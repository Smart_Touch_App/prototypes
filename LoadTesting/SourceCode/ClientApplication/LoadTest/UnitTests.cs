﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
/*
 1. Target SalesLogicExpress.Application
             
 2. Invoke Unit Test with 100 concurrent virtual users. Each Virtual user will be run on a separate thread
    Also, each user will use separate RDB in app.setting
             
             
 Metrics
             
                    
 * 1. Time taken by Unit Test to complete 
                            
 * 2. Time Taken by Each Syncronization during the execution of unit test.
             
            User 1 => 2 Seconds
            User 2 => 1 Seconds
              
                    
             
             
 */


namespace LoadTestLib
{
    class UnitTests
    {
        SyncEngine.Synchronizer SyncEngine;

        public void BatchTests()
        {
            Kill();
            SyncEngine = new SyncEngine.Synchronizer();
            SyncEngine.Start();

            List<Task<Metrics>> AllRDBTasks = new List<Task<Metrics>>();
            int StartUser = 1;
            int MaxUsers = 50;
            int StartClientPort = 3550;
            int StartOrderID = 62100;
            int ServerPort = 2439;

            //Dictionary<int, SalesLogicExpress.Application.Managers.OrderManagerRefactored> OrderManagers = new Dictionary<int, SalesLogicExpress.Application.Managers.OrderManagerRefactored>();
            //for (int currentUser = StartUser; currentUser <= MaxUsers; currentUser++)
            //{
            //    OrderManagers.Add(currentUser, new SalesLogicExpress.Application.Managers.OrderManagerRefactored());
            //}


           // var OrderManager = new SalesLogicExpress.Application.Managers.OrderManagerRefactored();

            for (int currentUser = StartUser; currentUser <= MaxUsers; currentUser++)
            {
                var t = RunTestOrderCycle( currentUser, StartClientPort, StartOrderID, string.Format("User{0}", currentUser), ServerPort);

                StartClientPort++;
                StartOrderID++;
                AllRDBTasks.AddRange(t);
            }


            System.Threading.Thread.Sleep(100);
            
            Task.WaitAll(AllRDBTasks.ToArray());

            if (SyncEngine.SyncClientsCount > 0)
            {
                while (SyncEngine.RequestResults.Count < MaxUsers - StartUser + 1)
                {
                    System.Threading.Thread.Sleep(5000);

                }

            }
            Console.WindowWidth = 150;
            var ResultsSummary = LoadTestLib.LoadTest.FormatSummary(AllRDBTasks, MaxUsers - StartUser + 1);
            var ResultDetails = LoadTestLib.LoadTest.FormatDetails(AllRDBTasks, MaxUsers - StartUser + 1);


            //Result Summary
            System.Data.DataTable tblResultSummary = new System.Data.DataTable();
            tblResultSummary.Columns.Add("MethodName", typeof(string));
            tblResultSummary.Columns.Add("TotalTimeElapsed", typeof(TimeSpan));
            tblResultSummary.Columns.Add("AverageResponseTime", typeof(TimeSpan));
            tblResultSummary.Columns.Add("MinTime", typeof(TimeSpan));
            tblResultSummary.Columns.Add("MaxTime", typeof(TimeSpan));

            foreach (var d in ResultsSummary)
            {
                var r = tblResultSummary.NewRow();
                r["MethodName"] = d.MethodName;

                r["TotalTimeElapsed"] = d.TotalTimeElapsed;
                r["AverageResponseTime"] = d.AverageResponseTime;
                r["MinTime"] = d.MinTime;
                r["MaxTime"] = d.MaxTime;


                tblResultSummary.Rows.Add(r);
            }


            //Result Details
            System.Data.DataTable tblResultDetails = new System.Data.DataTable();

            var methods = ResultDetails.Select(x => x.MethodName).Distinct();

            tblResultDetails.Columns.Add("MethodName", typeof(string));

            for(int i=0;i< MaxUsers-StartUser +1;i++)
            {
                tblResultDetails.Columns.Add(string.Format("User{0}",i+1), typeof(TimeSpan));
                tblResultDetails.Columns.Add(string.Format("StartTime{0}",i+1), typeof(DateTime));
                tblResultDetails.Columns.Add(string.Format("EndTime{0}", i + 1), typeof(DateTime));

            }
            foreach (var m in methods)
            {
                var r = tblResultDetails.NewRow();
                r["MethodName"] = m;
                tblResultDetails.Rows.Add(r);
            }


            foreach (var d in ResultDetails)
            {

                 var SearchRow = d.MethodName;
                 var SearchCol = string.Format("User{0}",d.user);

                for(int i=0;i<tblResultDetails.Rows.Count;i++)
                {
                    if(tblResultDetails.Rows[i]["MethodName"].ToString() == SearchRow)
                    {
                        tblResultDetails.Rows[i][SearchCol] = d.TotalTimeElapsed;
                        tblResultDetails.Rows[i][string.Format("StartTime{0}", i + 1)] = d.StartTime;
                        tblResultDetails.Rows[i][string.Format("EndTime{0}", i + 1)] = d.EndTime;
                    }

                }
            }


            System.Data.DataTable tblResultDetailsWithStartEndTime = new System.Data.DataTable();
            tblResultDetails.Columns.Add("MethodName", typeof(string));
            tblResultDetails.Columns.Add("StartTime", typeof(DateTime));
            tblResultDetails.Columns.Add("EndTime", typeof(DateTime));

            foreach (var d in ResultDetails)
            {

                var SearchRow = d.MethodName;
                var SearchCol = string.Format("User{0}", d.user);

                for (int i = 0; i < tblResultDetails.Rows.Count; i++)
                {
                    if (tblResultDetails.Rows[i]["MethodName"].ToString() == SearchRow)
                    {
                        tblResultDetails.Rows[i][SearchCol] = d.TotalTimeElapsed;

                    }

                }
            }




            System.Data.DataTable tblSyncResults = new System.Data.DataTable();
            if (SyncEngine.SyncClientsCount > 0)
            {


                tblSyncResults.Columns.Add("RequestID", typeof(string));
                tblSyncResults.Columns.Add("ClientPort", typeof(int));
                tblSyncResults.Columns.Add("TotalTimeElapsed", typeof(TimeSpan));
                tblSyncResults.Columns.Add("SyncStartedOn", typeof(DateTime));
                tblSyncResults.Columns.Add("SyncEndedOn", typeof(DateTime));
                foreach (var req in SyncEngine.RequestResults)
                {
                    //Console.WriteLine(string.Format("User {0} on Port {1} took {2} min {3} sec {4} ms",
                    //    req.RequestID, req.ClientPort, req.Elapsed.Minutes, req.Elapsed.Seconds, req.Elapsed.Milliseconds));

                    var r = tblSyncResults.NewRow();
                    r["RequestID"] = req.RequestID;
                    r["ClientPort"] = req.ClientPort;
                    r["TotalTimeElapsed"] = req.Elapsed;
                    r["SyncStartedOn"] = req.SyncStartedOn;
                    r["SyncEndedOn"] = req.SyncEndedOn;


                    tblSyncResults.Rows.Add(r);

                }

            }



            //          ExcelUtilities.Excel.ExportToExcel(tbl);



            var k = Console.ReadKey();


        }

        //private List<Task<Metrics>> RunTestDelteOrders(int currentUser = 1, int clientPort = 3426, int OrderID = 1130, string RequestID = "User1", int ServerPort = 2439)
        //{

        //    IsolatedDomain<LoadTestLib.LoadTest> lt1 = new IsolatedDomain<LoadTestLib.LoadTest>(string.Format("User{0}", currentUser));
        //    var AD = lt1.CreateDomain(@"D:\Arvind\SourceCode\ClientApplication\LoadTest\App1.config");
        //    string conn = string.Format(@"DBF=D:\Arvind\TestBench\REmoteDBs\User{0}\Test{0}.db;UID=dba;PWD=sql", currentUser);
        //    string DB = string.Format(@"DBF=D:\Arvind\TestBench\REmoteDBs\User{0}\Test{0}.db", currentUser);



        //    var T = AD.RunLoadTest(1, 60,

        //    () =>
        //    {
        //        TestDeleteOrder(50000);
        //        Synchronize(RequestID, DB, clientPort, "localhost", ServerPort);
        //        return "DeleteOrder";
        //    });

        //    return T;
        //}



        private List<Task<Metrics>> RunTestOrderCycle(int currentUser = 1, int clientPort = 3426, int OrderID = 1130, string RequestID = "User1", int ServerPort = 2439)
        {

            IsolatedDomain<LoadTestLib.LoadTest> lt1 = new IsolatedDomain<LoadTestLib.LoadTest>(string.Format("User{0}", currentUser));
            var AD = lt1.CreateDomain(@"D:\Arvind\SourceCode\ClientApplication\LoadTest\App1.config");
            string conn = string.Format(@"Server=localhost;DBF=D:\Arvind\TestBench\REmoteDBs\User{0}\Test{0}.db;UID=dba;PWD=sql", currentUser);
            string DB = string.Format(@"D:\Arvind\TestBench\REmoteDBs\User{0}\Test{0}.db", currentUser);


            //Define Configs
            System.Configuration.ConfigurationManager.AppSettings["userAppService"] =   "false";
            System.Configuration.ConfigurationManager.AppSettings["useDefaultDb"] = "true";
            System.Configuration.ConfigurationManager.AppSettings["connectionString"] = conn;
            SalesLogicExpress.Application.Managers.UserManager.UserId = 1; //Sarvesh
            SalesLogicExpress.Application.Managers.UserManager.UserRoute = "783";



            var OrderManager = new SalesLogicExpress.Application.Managers.OrderManagerRefactored();

            var T = AD.RunLoadTest(1, 60,


            () =>
            {
                TestCreateOrder(conn, OrderManager, OrderID);
                Synchronize(RequestID, DB, clientPort, "localhost", ServerPort);
                return new _TaskInfo() { MethodName = "CreateOrder", User = currentUser.ToString() };
            }

            ,

            () =>
            {
                int OrderCount = 0;
                TestGetOrderHistoryForCustomer(OrderManager,"1108911", out OrderCount);
                return new _TaskInfo() { MethodName = string.Format("GetOrderHistoryForCustomer OrderCount {0}", OrderCount), User = currentUser.ToString() };
            }

            ,
           () =>
           {
               int OrderCount = 0;
               TestGetReasonListForVoidOrder(OrderManager, out OrderCount);
               return new _TaskInfo() { MethodName = string.Format("TestGetReasonListForVoidOrder ReasonCount {0}", OrderCount), User = currentUser.ToString() };
           },

            () =>
            {
                int OrderCount = 0;
                TestGetAllOpenOrdersForItem(OrderManager, "", out OrderCount);
                return new _TaskInfo() { MethodName = string.Format("GetAllOpenOrdersForItem OpenOrderCount {0}", OrderCount), User = currentUser.ToString() };
            },

            () =>
            {
                int OrderCount = 0;
                TestApplyPricingToItem("1", "783", "1108911", out OrderCount);
                return new _TaskInfo() { MethodName = string.Format("ApplyPricingToItem ReturnedOrderItemCount {0}", OrderCount), User = currentUser.ToString() };

            }




            );

           
            return T;



        }


        private void TestApplyPricingToItem(string itemNumber, string routeBranch, string shipToCustomerID, out int CountOfOrders)
        {

            //This test produces error on line "textBoxPrimaryUoM = itemRow["IMUOM1"].ToString();"
            CountOfOrders = 0;
            return;

            var OrderManager = new SalesLogicExpress.Application.Managers.OrderManagerRefactored();

            var it1 = new Item()
            {
                ItemNumber = itemNumber,
                UnitPrice = 6.15999984741211M,
                ExtendedPrice = 6.15999984741211M,
                OrderQty = 2,
                CreatedDate = DateTime.Now.ToString(),
                IsTaxable = false,
                ActualQtyOnHand = 5,
                AverageStopQty = 3,
                QtyOnHand = 10,
                SalesCat1 = ""
            };
            var OrderItem = OrderManager.ApplyPricingToItem(it1, routeBranch, shipToCustomerID);

            CountOfOrders = 0;
            if (OrderItem != null)
                CountOfOrders = 1;


        }



        private void TestGetAllOpenOrdersForItem(SalesLogicExpress.Application.Managers.OrderManagerRefactored OrderManager,string itemNumber, out int CountOfOrders)
        {
           // var OrderManager = new SalesLogicExpress.Application.Managers.OrderManagerRefactored();
            var OrderList = OrderManager.GetAllOpenOrdersForItem(itemNumber);

            CountOfOrders = 0;
            if (OrderList != null)
                CountOfOrders = OrderList.Rows.Count;


        }


        private void TestGetReasonListForVoidOrder(SalesLogicExpress.Application.Managers.OrderManagerRefactored OrderManager, out int CountOfOrders)
        {
            //var OrderManager = new SalesLogicExpress.Application.Managers.OrderManagerRefactored();
            var OrderList = OrderManager.GetReasonListForVoidOrder();

            CountOfOrders = 0;
            if (OrderList != null && OrderList.Count > 0)
                CountOfOrders = OrderList.Count;


        }


        private void TestGetOrderHistoryForCustomer(SalesLogicExpress.Application.Managers.OrderManagerRefactored OrderManager, string customerID, out int CountOfOrders)
        {
            //var OrderManager = new SalesLogicExpress.Application.Managers.OrderManagerRefactored();
            var OrderList = OrderManager.GetOrderHistoryForCustomer(customerID);

            CountOfOrders = 0;
            if (OrderList != null && OrderList.Tables.Count > 0)
                CountOfOrders = OrderList.Tables[0].Rows.Count;


        }



        private void TestDeleteOrder(SalesLogicExpress.Application.Managers.OrderManagerRefactored OrderManager, int OrderId)
        {
           // var OrderManager = new SalesLogicExpress.Application.Managers.OrderManagerRefactored();
            OrderManager.DeleteOrderItems(OrderId);
        }



        private void TestCreateOrder(string connectionString,SalesLogicExpress.Application.Managers.OrderManagerRefactored OrderManager, int orderId, int CustomerId = 1108911, int userID = 1, string UserRoute = "783", bool applyUserAppService = false)
        {



            ////Define Configs
            //System.Configuration.ConfigurationManager.AppSettings["userAppService"] = applyUserAppService ? "true" : "false";
            //System.Configuration.ConfigurationManager.AppSettings["useDefaultDb"] = "true";
            //System.Configuration.ConfigurationManager.AppSettings["connectionString"] = connectionString;
            //SalesLogicExpress.Application.Managers.UserManager.UserId = userID; //Sarvesh
            //SalesLogicExpress.Application.Managers.UserManager.UserRoute = UserRoute;


          //  var STx = System.Diagnostics.Stopwatch.StartNew();

          //  var StartTime = DateTime.Now;



           
            ObservableCollection<OrderItem> OrderItems = new ObservableCollection<OrderItem>();



            var it1 = new Item()
            {
                ItemNumber = "1",
                UnitPrice = 6.15999984741211M,
                ExtendedPrice = 6.15999984741211M,
                OrderQty = 2,
                CreatedDate = DateTime.Now.ToString(),
                IsTaxable = false,
                ActualQtyOnHand = 5,
                AverageStopQty = 3,
                QtyOnHand = 10,
                SalesCat1 = ""
            };
            var item1 = new OrderItem(it1);
            OrderItems.Add(item1);


            OrderManager.SaveOrderHeader(orderId, CustomerId);
            OrderManager.SaveOrder(orderId, CustomerId, OrderItems);

         //   var TimeElapsed = STx.Elapsed;


          //  var el = DateTime.Now - StartTime;

        }


        private void Kill()
        {

            var Processes = System.Diagnostics.Process.GetProcessesByName("dbmlsync");
            foreach (var p in Processes)
            {
                p.Kill();

            }


        }


        public void Synchronize(string requestId, string DB, int clientPort, string serverHost, int serverPort, string publication = "prof_immediate")
        {
            SyncEngine.Enqueue(new SyncEngine.Synchronizer._Request()
            {
                RequestID = requestId,
                SQLAnywherePath = @"C:\Program Files\SQL Anywhere 16\Bin64\",
                CMDLine = string.Format(@"-c DBF={0};UID=dba;PWD=sql", DB),
                User = "dba",
                Password = "sql",
                Publication = publication,
                ClientPort = clientPort,
                ServerPort = serverPort,
                ServerHost = serverHost
            });


        }

    }

}
