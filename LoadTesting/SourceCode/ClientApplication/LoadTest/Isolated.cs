﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Collections.Generic;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;

namespace LoadTestLib
{
    public class IsolatedDomain<T> where T : LoadTest
    {
        AppDomain ad;
        string _DomainName = string.Empty;

        public IsolatedDomain(string name)
        {
            _DomainName = name;

        }

        ~IsolatedDomain()
        {
            ad = null;
        }

        public T CreateDomain(string appConfigurationFileNamePath)
        {
            AppDomainSetup setup = new AppDomainSetup();
           // setup.ApplicationBase = System.Environment.CurrentDirectory;
            //setup.ConfigurationFile = appConfigurationFileNamePath;


            //setup.SetConfigurationBytes( appConfigurationFileNamePath );

           
            ad = AppDomain.CreateDomain(_DomainName,null,setup);
            ad.Load("SalesLogicExpress.Domain");
            ad.Load("SalesLogicExpress.Application");
          //  ad.Load("System.Configuration"); 
            ad.SetData("xDomainName", _DomainName);
            ad.SetData("APP_CONFIG_FILE", appConfigurationFileNamePath);
           
            T instance = (T)ad.CreateInstanceAndUnwrap(typeof(T).Assembly.FullName, typeof(T).FullName);

            //T instance = (T)ad.CreateInstanceFrom(typeof(LoadTest).Assembly.FullName, typeof(LoadTest).FullName).Unwrap();

            return instance;


        }

        public void Dispose()
        {

            AppDomain.Unload(ad);
            ad = null;
        }

    }
}
