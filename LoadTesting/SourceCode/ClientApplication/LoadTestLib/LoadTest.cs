﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

//using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoadTestLib
{
    [Serializable]
    public struct Metrics
    {
        public string User;
        public string MethodName;
        public TimeSpan TimeElapsed;
        public DateTime StartTime;
        public DateTime EndTime;
        public Task xTask;
    }

    public struct _TaskInfo
    {
        public string User;
        public string MethodName;
    }

    public struct _ResultSummary
    {
        public string MethodName;
        public TimeSpan TotalTimeElapsed;
        public TimeSpan AverageResponseTime;
        public TimeSpan MinTime;
        public TimeSpan MaxTime;
    

    }

    public struct _ResultDetail
    {
        public string user;
        public string MethodName;
        public TimeSpan TotalTimeElapsed;
        public DateTime StartTime;
        public DateTime EndTime;

    }


    /// <summary>
    /// Usage: 
    // VirtualUsers v1 = new VirtualUsers();
    //var T1 =  v1.RunMethod(() => {tests.IndexTest(); });
    //var T2 = v1.RunMethod(() => { tests.IndexTest(); });
    //var T3 = v1.RunMethod(() => { tests.IndexTest(); });
    //var T4 = v1.RunMethod(() => { tests.IndexTest(); });
    //var T5 = v1.RunMethod(() => { tests.IndexTest(); });
    //Task.WaitAll(T1, T2, T3, T4, T5);


    [Serializable]
    /// </summary>
    class VirtualUsers
    {
        public Task<Metrics> RunMethod(Func<_TaskInfo> TestMethod)
        {
            
            Task<Metrics> v1 = Task.Factory.StartNew(() =>
            {
                Metrics Metric = new Metrics();
                var ST = DateTime.Now;
                Metric.StartTime = ST;
                //var STx = System.Diagnostics.Stopwatch.StartNew();
                _TaskInfo tInfo = TestMethod();
                Metric.MethodName = tInfo.MethodName;
                Metric.User = tInfo.User;
                //Metric.TimeElapsed = STx.Elapsed;
                Metric.EndTime = DateTime.Now;
                Metric.TimeElapsed = Metric.EndTime - ST;
                return Metric;
            },TaskCreationOptions.LongRunning);

            System.Threading.Thread.Sleep(50);

            return v1;
        }
    }

    [Serializable]
    public class LoadTest
    {
        VirtualUsers v1 = new VirtualUsers();
        List<Task<Metrics>> RunningTasks = new List<Task<Metrics>>();


        public LoadTest()
        {
            ResetConfig();
        }

        //public List<Task<Metrics>> RunLoadTest(int virtualUsers, int durationSeconds,bool IsOrdered, params Func<string>[] methodToBeTested)
        //{

        //    var method = methodToBeTested[0];
        //    var v = v1.RunMethod(() =>
        //    {

        //        return methodToBeTested[0]();

        //    });
 
        //    if(methodToBeTested.Count() >1)
        //    {
        //        for(int i=1;i<methodToBeTested.Count()-1;i++)
        //        {
        //            v = v.ContinueWith((m)=>  
        //                                    v1.RunMethod(() =>
        //                                        {
        //                                                        return methodToBeTested[i]();

        //                                                }).Result
                                     
        //                                );

        //        }
        //    }

        //    RunningTasks.Add(v);


        //    return RunningTasks;


        //}



        public List<Task<Metrics>> RunLoadTest(int virtualUsers, int durationSeconds, params Func<_TaskInfo>[] methodToBeTested)
        {
            foreach (var method in methodToBeTested)
            {

                var v = v1.RunMethod(() =>
                  {
                      return method();

                  });
                RunningTasks.Add(v);
            }

            return RunningTasks;


        }

        public static TimeSpan ConvertToTime(int timeInMS)
        {

            return new TimeSpan(0, 0, 0, 0, timeInMS);


        }


        public static string FormatResult(List<Task<Metrics>> metrics, int virtualUsers, string methodName)
        {

            var TotalTimeElapsed = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Sum(x => x.Result.TimeElapsed.TotalMilliseconds)));
            var AverageResponseTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Average(x => x.Result.TimeElapsed.TotalMilliseconds)));
            var MinTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Min(x => x.Result.TimeElapsed.TotalMilliseconds)));
            var MaxTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Max(x => x.Result.TimeElapsed.TotalMilliseconds)));
            return string.Format("Method: {5} V.Users:{0} Avg.Time {2} Min: {3} Max: {4}", virtualUsers, TotalTimeElapsed, AverageResponseTime, MinTime, MaxTime, methodName.PadRight(70));

        }

        public static List<_ResultSummary> FormatSummary(List<Task<Metrics>> metricsAll,int virtualUsers)
        {

            List<_ResultSummary> Result = new List<_ResultSummary>();
            var Methods = metricsAll.Select(x => x.Result.MethodName).Distinct();

           // Result.Add(string.Format("V.Users:{0}", virtualUsers));
          //  var MaxLength = metricsAll.Max(x => x.Result.MethodName.Length);

            foreach(var m in Methods)
            {
                var metrics = metricsAll.Where(x => x.Result.MethodName == m);

                var TotalTimeElapsed = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Sum(x => x.Result.TimeElapsed.TotalMilliseconds)));
                var AverageResponseTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Average(x => x.Result.TimeElapsed.TotalMilliseconds)));
                var MinTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Min(x => x.Result.TimeElapsed.TotalMilliseconds)));
                var MaxTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Max(x => x.Result.TimeElapsed.TotalMilliseconds)));

                Result.Add(new _ResultSummary() { AverageResponseTime=AverageResponseTime,
                                            TotalTimeElapsed=TotalTimeElapsed, 
                                            MinTime=MinTime,MaxTime=MaxTime,
                                            MethodName = m
                });
               // Result.Add(string.Format("Method: {4} AvgResTime {1} Min: {2} Max: {3}", TotalTimeElapsed, AverageResponseTime, MinTime, MaxTime, m.PadRight(MaxLength+4)));

            }
            return Result;

        }

        public static List<_ResultDetail> FormatDetails(List<Task<Metrics>> metricsAll, int virtualUsers)
        {

            List<_ResultDetail> Result = new List<_ResultDetail>();

            foreach(var m in metricsAll)
            {
                
                Result.Add(new _ResultDetail(){ MethodName=m.Result.MethodName,
                                                user = m.Result.User,
                                                TotalTimeElapsed=m.Result.TimeElapsed,
                                                StartTime = m.Result.StartTime,
                                                EndTime=m.Result.EndTime
                });
            }

            return Result;

        }

        private static void ResetConfig()
        {
            typeof(ConfigurationManager)
                .GetField("s_initState", BindingFlags.NonPublic |
                                            BindingFlags.Static)
                .SetValue(null, 0);

            typeof(ConfigurationManager)
                .GetField("s_configSystem", BindingFlags.NonPublic |
                                            BindingFlags.Static)
                .SetValue(null, null);

            typeof(ConfigurationManager)
                .Assembly.GetTypes()
                .Where(x => x.FullName ==
                            "System.Configuration.ClientConfigPaths")
                .First()
                .GetField("s_current", BindingFlags.NonPublic |
                                        BindingFlags.Static)
                .SetValue(null, null);
        }

    }


    //[Serializable]
    //public class LoadTestWindowsApp : MarshalByRefObject
    //{


    //    List<AppDomain> AppDomains = new List<AppDomain>();

    //    List<Task<Metrics>> RunningTasks = new List<Task<Metrics>>();

    //    public List<AppDomain> RunLoadTest(int virtualUsers, int durationSeconds, Action<int> methodToBeTested)
    //    {
    //        for (int VirtualUser = 0; VirtualUser < virtualUsers; VirtualUser++)
    //        {

    //            AppDomain ad = AppDomain.CreateDomain(VirtualUser.ToString());
    //            ad.SetData("Metric", new object());
    //            ad.Load("LoadTestLib");

    //            LoadTest instance = (LoadTest)ad.CreateInstanceAndUnwrap(typeof(LoadTest).Assembly.FullName, typeof(LoadTest).FullName);

    //            ad.DoCallBack(delegate
    //            {
    //                //Metrics m = (Metrics)AppDomain.CurrentDomain.GetData("Metric");
    //                VirtualUsers v1 = new VirtualUsers();
    //                var t = v1.RunMethod(() =>{methodToBeTested(VirtualUser + 1);});
    //                AppDomain.CurrentDomain.SetData("Metric", t);
    //            });

    //            AppDomains.Add(ad);
    //            //RunningTasks.Add(t);
    //        }

    //        try
    //        {

    //            Task.WaitAll(RunningTasks.ToArray());
    //        }
    //        catch (AggregateException ex)
    //        {

    //            throw ex.Flatten();
    //        }


    //        return AppDomains;
    //    }

    //    public static TimeSpan ConvertToTime(int timeInMS)
    //    {

    //        return new TimeSpan(0, 0, 0, 0, timeInMS);


    //    }

    //    public static string FormatResult(List<Task<Metrics>> metrics, int virtualUsers, int durationSeconds, string methodName)
    //    {

    //        var TotalTimeElapsed = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Sum(x => x.Result.TimeElapsed.TotalMilliseconds)));
    //        var AverageResponseTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Average(x => x.Result.TimeElapsed.TotalMilliseconds)));
    //        var MinTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Min(x => x.Result.TimeElapsed.TotalMilliseconds)));
    //        var MaxTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(metrics.Max(x => x.Result.TimeElapsed.TotalMilliseconds)));
    //        return string.Format("Method: {5} Virtual Users: {0} Total Time {1} AvgResponseTime {2} Min: {3} Max: {4}", virtualUsers, TotalTimeElapsed, AverageResponseTime, MinTime, MaxTime, methodName);

    //    }
    //}


}
