


/*Master/Reference Pattern for Delivery Cycle (applicable for ALL routes)*/
/*NPATEL - Changed data type to better align to JDE available data types */
CREATE TABLE dbo.[DeliveryCycleMaster](
	[DeliveryDayCode] [nchar](3) NOT NULL,
	[DeliveryDayCodeDesc] [nchar](50) NULL,
	[PeriodNumber] [numeric] (4,0) NOT NULL,
	[WeekNumber] [numeric] (4,0) NOT NULL,
	[DayNumber] [numeric] (4,0) NOT NULL,
	[IsServiceStop] [numeric] (4,0) NULL,
 CONSTRAINT [PK_DeliveryCycleMaster] PRIMARY KEY CLUSTERED 
(
	[DeliveryDayCode] ASC,
	[PeriodNumber] ASC,
	[WeekNumber] ASC,
	[DayNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE dbo.[DeliveryCycleMaster] ADD  CONSTRAINT [DF__DeliveryC__IsSer__74100195]  DEFAULT ((1)) FOR [IsServiceStop]
GO



/*Insert scripts for Reference Pattern. This scripts will create all the master data that shall be shared by all routes. */
/* Request the business team complete the DelDay_NewMobile_LoadTemplate.xlsx template to populate this with real data */
/* See CSV for data created by business */
/* Once loaded for period 1,2,3 create zero set */

/*Create Zero Rows */
insert into dbo.DeliveryCycleMaster 
select DeliveryDayCode,DeliveryDayCodeDesc,0,WeekNumber,DayNumber,IsServiceStop from dbo.DeliveryCycleMaster 
	Group By DeliveryDayCode,DeliveryDayCodeDesc,WeekNumber,DayNumber,IsServiceStop 

/*Following table is pseudo script for RouteStandardSequence*/
/* NPATEL Updated format to align better to JDE data */
/* need to resolve if RouteID should be the data key for paritioning and define RouteID data dictionary defination for clarity */


DROP table dbo.[RouteStandardSequence]

CREATE TABLE dbo.[RouteStandardSequence](
	[RouteID] [nchar] (10) NOT NULL,
	[CustomerID] [numeric](8,0) NOT NULL,
	[WeekNumber]  [numeric](4,0) NOT NULL,
	[DayNumber]  [numeric](4,0) NOT NULL,
	[SequenceNumber] [numeric](4,0) NULL,
 CONSTRAINT [PK_RouteStandardSequence] PRIMARY KEY CLUSTERED 
(
	[RouteID] ASC,
	[CustomerID] ASC,
	[WeekNumber] ASC,
	[DayNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

select * from dbo.[RouteStandardSequence] order by RouteID,WeekNumber,DayNumber,SequenceNumber



/*Route Standard Sequence.To populate the Route Standard Sequence, use the query below*/
insert into DataModelWorkspace.dbo.[RouteStandardSequence]
select FFUSER,ABAN8,[WeekNumber],[DayNumber],cast(AIZON as int) from MobileDataModel.BUSDTA.F56M0001 
	join MobileDataModel.BUSDTA.F90CA003 on SMSLSM=FFAN8
	join MobileDataModel.BUSDTA.F90CA086 on CRCUAN8=SMAN8
	join MobileDataModel.BUSDTA.F0101 on ABAN8=CRCRAN8
	join MobileDataModel.BUSDTA.F03012 on AIAN8=ABAN8
	join DataModelWorkspace.dbo.[DeliveryCycleMaster] on AISTOP=DeliveryDayCode and [PeriodNumber]=0
	where AIZON>' '
	GROUP BY FFUSER,ABAN8,[WeekNumber],[DayNumber],AIZON

/* Adjust Seq Numbers */ 
drop table dbo.RouteStandardSequenceTemp 

select RouteID,CustomerID,WeekNumber,DayNumber,
	(ROW_NUMBER() over (PARTITION By RouteID,WeekNumber,DayNumber Order By  routeid,WeekNumber,DayNumber,SequenceNumber))*1 newSeqNumber
	 into dbo.RouteStandardSequenceTemp from  dbo.RouteStandardSequence

delete from dbo.RouteStandardSequence 
insert into dbo.RouteStandardSequence select * from dbo.RouteStandardSequenceTemp 

select * from dbo.RouteStandardSequence Order By routeid,WeekNumber,DayNumber,SequenceNumber

/*Find Data pertaining to particular route by changing the variables in where clause below*/
select * from dbo.RouteStandardSequence 
where routeid like <Route_Id>


--##NOTE: The above structure shall evolve with more functionality to address.
--##The above tables will get renamed as per Naming Convention.


/* TODO - Need to store in the system a global constant as a date value which is a calendar date that represents the Monday of Week 1 of Period 1.  This should be old enough that it covered all historical use cases which might be available on the system at go live*/
--A good value for testing is 12/29/2015  


/* Need a procedures that given a calendar date and the system static starting calendar date, return would be the PeriodNumber, WeekNumber, and DayNumber are the map to the input calendar date */
/* this will be used often when translating data from the delivery day code structure to the actualy event instance table
function GetPeriodMapforDate (@requestDate date,@startDate date, cyclecolumnt int (1=period,2=week,3=day )
RETURNS daycycleValue int
AS BEGIN


example call
getperiodmapfordate (1/15/2015,12/29/14,1)
return 1 
getperiodmapfordate (1/15/2015,12/29/14,2)
return 3 
getperiodmapfordate (1/15/2015,12/29/14,4)
return 4 
	
   */

   
   
/*This function will return the Period for a given date with respect to BaseDate*/
CREATE FUNCTION [BUSDTA].[GetPeriodForDate] (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int;
	DECLARE @varDiv28 numeric(3,2);
	DECLARE @PeriodNumber numeric(3,2);
	DECLARE @ModNumber numeric(3,2);

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate);
	select @varDiv28 = CEILING (@DateDifference / 28.00);
	select @ModNumber = @varDiv28%3;
	if(@ModNumber = 0)
		select @PeriodNumber = 3;
	else
		select @PeriodNumber = @ModNumber;
	RETURN @PeriodNumber;
END
GO

/*Verify the Period*/
select BUSDTA.GetPeriodForDate(CAST('20150215' as date), CAST('20141229' as date))
   
   
/*This function will return the Week for a given date with respect to BaseDate*/
CREATE FUNCTION [BUSDTA].[GetWeekForDate] (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int;
	DECLARE @varDiv7 numeric(3,2);
	DECLARE @WeekNumber numeric(3,2);
	DECLARE @ModNumber numeric(3,2);

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate);
	select @varDiv7 = CEILING (@DateDifference / 7.00);
	select @ModNumber = @varDiv7%4;
	if(@ModNumber = 0)
		select @WeekNumber = 4;
	else
		select @WeekNumber = @ModNumber;
	RETURN @WeekNumber;
END
GO

/*Query to verify week*/
select BUSDTA.GetWeekForDate(CAST('20150115' as date), CAST('20141229' as date))


/*This function will return the Day, BaseDate argument can be removed*/
CREATE FUNCTION [BUSDTA].[GetDayForDate] (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int;

	select @DateDifference = DATEPART (DW, @CalendarDate)-1;

	RETURN @DateDifference;
END
GO

/*Verify the Day*/
select BUSDTA.GetDayForDate(CAST('20150115' as date), CAST('20141229' as date))



/*Query to get the Customers for any given date. Consider date as Jan 15, 2015*/
select DCM.DeliveryDayCode, C.AIAN8, AB.ABALPH
from BUSDTA.DeliveryCycleMaster DCM 
join BUSDTA.F03012 C on DCM.DeliveryDayCode = C.AISTOP
join BUSDTA.F0101 AB on C.AIAN8 = AB.ABAN8
join BUSDTA.RouteStandardSequence R on DCM.WeekNumber = R.WeekNumber and DCM.DayNumber = R.DayNumber
where DCM.PeriodNumber = BUSDTA.GetPeriodForDate(CAST('20150115' as date), CAST('20141229' as date)) and
	  DCM.WeekNumber = BUSDTA.GetWeekForDate(CAST('20150115' as date), CAST('20141229' as date)) and 
	  DCM.DayNumber = BUSDTA.GetDayForDate(CAST('20150115' as date), CAST('20141229' as date)) and 
	  R.RouteID = 'FBM783' -- Data partitioned on Remote DB. Will be removed.
	  order by R.SequenceNumber;



	  
	  
	  
	  
	  
	  
	  
	  
	  /*Query for Customer Stop Sequencing*/
select r.CustomerID, r.WeekNumber, r.DayNumber, r.SequenceNumber 
from BUSDTA.RouteStandardSequence r  , BUSDTA.DeliveryCycleMaster d
where r.WeekNumber = 3 and -- Pass the week number
	r.DayNumber = 5 and -- Pass the Day Number
	r.RouteID = 'FBM783' and -- This can be removed on Remote DB
	d.PeriodNumber = BUSDTA.GetPeriodForDate(CAST('20150115' as date), CAST('20141229' as date)) -- '20150116' is current date. '20141229' is base date
	group by r.CustomerID, r.WeekNumber, r.DayNumber, r.SequenceNumber 
	order by r.sequencenumber

/*Save The Sequence. This logic can be called iteratively from application 
OR 
this can be made as a part of procedure, which can be called from application*/
UPDATE  BUSDTA.RouteStandardSequence r
set r.SequenceNumber = <seq_no>
where r.CustomerId = <Customer_Id> and 
	r.weekNumber = <Week_Number> and 
	r.DayNumber = <Day_Number>




/****** Object:  Table [BUSDTA].[RouteStopSequence]    Script Date: 4/9/2015 5:07:19 PM ******/
/*This table is used for Daily Stop Sequence an entry will be made in this table in followgin scenarios
1) On the Date. 2)Traversed to a date. 3) Create Stop on Date. 4) Move Stop. 5) Pre-Order.*/

CREATE TABLE [BUSDTA].[RouteStopSequence](
	[RouteStopId] [numeric](8, 0) NOT NULL,
	[RouteID] [nchar](10) NOT NULL,
	[CustomerID] [numeric](8, 0) NOT NULL,
	[StopDate] [date] NOT NULL,
	[OriginalDate] [date] NULL,
	[SequenceNumber] [numeric](4, 0) NULL,
	[VisiteeType] [nchar](10) NULL,
	[StopType] [nchar](10) NULL,
	[ReferenceStopId] [numeric](8, 0) NULL,
 CONSTRAINT [PK_RouteStopSequence_1] PRIMARY KEY CLUSTERED 
(
	[RouteStopId] ASC,
	[RouteID] ASC,
	[CustomerID] ASC,
	[StopDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/*Following insert command will be run when any of activity takes place
1) On the Date. 2)Traversed to a date. 3) Create Stop on Date. 4) Move Stop. 5) Pre-Order. */

insert into busdta.RouteStopSequence(
	RouteStopId, 
	RouteID, 
	CustomerId, 
	StopDate, 
	--OriginalDate, -- This field will be populated when a stop is moved
	SequenceNumber, 
	VisiteeType, 
	StopType
	--ReferenceStopId -- This field will be populated when a stop is moved
)
(
	select 	2, -- Numbering logic/Autoincrement
	r.RouteID, 
	r.CustomerID, 
	GETDATE(), 
	r.SequenceNumber, 
	'Cust',		-- Customer/Prospect
	'Planned'	-- Planned/UnPlanned/ReScheduled
	from BUSDTA.RouteStandardSequence r  , BUSDTA.DeliveryCycleMaster d
	where r.WeekNumber = BUSDTA.GetWeekForDate(CAST('20150116' as date), CAST('20141229' as date)) and 
		r.DayNumber =  BUSDTA.GetDayForDate(CAST('20150116' as date), CAST('20141229' as date)) and 
		r.RouteID = 'FBM783' and -- This can be removed on Remote DB
		d.PeriodNumber = BUSDTA.GetPeriodForDate(CAST('20150116' as date), CAST('20141229' as date)) 
		group by r.RouteID,r.CustomerID, r.WeekNumber, r.DayNumber, r.SequenceNumber 
)




