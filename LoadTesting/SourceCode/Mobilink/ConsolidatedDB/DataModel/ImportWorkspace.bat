@echo off
SET pwd=
SET /P pwd=Sa Password:
if "%pwd%"=="" GOTO Error
bcp DataModelWorkspace.dbo.DeliveryCycleMaster in D:\Exports\WorkspaceExports\DeliveryCycleMaster.dat -n -SMSDEVDB1 -Usa -P%pwd%
bcp DataModelWorkspace.dbo.RouteStandardSequence in D:\Exports\WorkspaceExports\RouteStandardSequence.dat -n -SMSDEVDB1 -Usa -P%pwd%
SET pwd=
echo Export Complete
GOTO End
:Error
echo Password Required
:End

