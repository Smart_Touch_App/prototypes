
/*Master/Reference Pattern for Delivery Cycle (applicable for ALL routes)*/
CREATE TABLE [BUSDTA].[DeliveryCycleMaster](
	[DeliveryDayCode] [nchar](3) NOT NULL,
	[DeliveryDayCodeDesc] [nchar](50) NULL,
	[PeriodNumber] [int] NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[IsServiceStop] [bit] NULL,
 CONSTRAINT [PK_DeliveryCycleMaster] PRIMARY KEY CLUSTERED 
(
	[DeliveryDayCode] ASC,
	[PeriodNumber] ASC,
	[WeekNumber] ASC,
	[DayNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [BUSDTA].[DeliveryCycleMaster] ADD  CONSTRAINT [DF__DeliveryC__IsSer__74100195]  DEFAULT ((1)) FOR [IsServiceStop]
GO



/*Insert scripts for Reference Pattern. This scripts will create all the master data that shall be shared by all routes.
The inserts are for Period 0-3, Week 1-4 and Day 1-5*/
insert into BUSDTA.DeliveryCycleMaster values(	'11',BUSDTA.GetUDCDescription('42', 'SP','11'),0,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11O',BUSDTA.GetUDCDescription('42', 'SP','11O'),0,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11Q',BUSDTA.GetUDCDescription('42', 'SP','11Q'),0,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21',BUSDTA.GetUDCDescription('42', 'SP','21'),0,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21O',BUSDTA.GetUDCDescription('42', 'SP','21O'),0,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21Q',BUSDTA.GetUDCDescription('42', 'SP','21Q'),0,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31',BUSDTA.GetUDCDescription('42', 'SP','31'),0,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31O',BUSDTA.GetUDCDescription('42', 'SP','31O'),0,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31Q',BUSDTA.GetUDCDescription('42', 'SP','31Q'),0,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41',BUSDTA.GetUDCDescription('42', 'SP','41'),0,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41O',BUSDTA.GetUDCDescription('42', 'SP','41O'),0,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41Q',BUSDTA.GetUDCDescription('42', 'SP','41Q'),0,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51',BUSDTA.GetUDCDescription('42', 'SP','51'),0,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51O',BUSDTA.GetUDCDescription('42', 'SP','51O'),0,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51Q',BUSDTA.GetUDCDescription('42', 'SP','51Q'),0,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12',BUSDTA.GetUDCDescription('42', 'SP','12'),0,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12O',BUSDTA.GetUDCDescription('42', 'SP','12O'),0,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12Q',BUSDTA.GetUDCDescription('42', 'SP','12Q'),0,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22',BUSDTA.GetUDCDescription('42', 'SP','22'),0,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22O',BUSDTA.GetUDCDescription('42', 'SP','22O'),0,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22Q',BUSDTA.GetUDCDescription('42', 'SP','22Q'),0,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32',BUSDTA.GetUDCDescription('42', 'SP','32'),0,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32O',BUSDTA.GetUDCDescription('42', 'SP','32O'),0,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32Q',BUSDTA.GetUDCDescription('42', 'SP','32Q'),0,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42',BUSDTA.GetUDCDescription('42', 'SP','42'),0,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42O',BUSDTA.GetUDCDescription('42', 'SP','42O'),0,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42Q',BUSDTA.GetUDCDescription('42', 'SP','42Q'),0,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52',BUSDTA.GetUDCDescription('42', 'SP','52'),0,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52O',BUSDTA.GetUDCDescription('42', 'SP','52O'),0,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52Q',BUSDTA.GetUDCDescription('42', 'SP','52Q'),0,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13',BUSDTA.GetUDCDescription('42', 'SP','13'),0,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13O',BUSDTA.GetUDCDescription('42', 'SP','13O'),0,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13Q',BUSDTA.GetUDCDescription('42', 'SP','13Q'),0,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23',BUSDTA.GetUDCDescription('42', 'SP','23'),0,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23O',BUSDTA.GetUDCDescription('42', 'SP','23O'),0,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23Q',BUSDTA.GetUDCDescription('42', 'SP','23Q'),0,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33',BUSDTA.GetUDCDescription('42', 'SP','33'),0,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33O',BUSDTA.GetUDCDescription('42', 'SP','33O'),0,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33Q',BUSDTA.GetUDCDescription('42', 'SP','33Q'),0,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43',BUSDTA.GetUDCDescription('42', 'SP','43'),0,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43O',BUSDTA.GetUDCDescription('42', 'SP','43O'),0,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43Q',BUSDTA.GetUDCDescription('42', 'SP','43Q'),0,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53',BUSDTA.GetUDCDescription('42', 'SP','53'),0,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53O',BUSDTA.GetUDCDescription('42', 'SP','53O'),0,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53Q',BUSDTA.GetUDCDescription('42', 'SP','53Q'),0,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14',BUSDTA.GetUDCDescription('42', 'SP','14'),0,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14O',BUSDTA.GetUDCDescription('42', 'SP','14O'),0,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14Q',BUSDTA.GetUDCDescription('42', 'SP','14Q'),0,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1A',BUSDTA.GetUDCDescription('42', 'SP','1A'),0,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1B',BUSDTA.GetUDCDescription('42', 'SP','1B'),0,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1E',BUSDTA.GetUDCDescription('42', 'SP','1E'),0,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24',BUSDTA.GetUDCDescription('42', 'SP','24'),0,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24O',BUSDTA.GetUDCDescription('42', 'SP','24O'),0,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24Q',BUSDTA.GetUDCDescription('42', 'SP','24Q'),0,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2A',BUSDTA.GetUDCDescription('42', 'SP','2A'),0,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2B',BUSDTA.GetUDCDescription('42', 'SP','2B'),0,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2E',BUSDTA.GetUDCDescription('42', 'SP','2E'),0,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34',BUSDTA.GetUDCDescription('42', 'SP','34'),0,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34O',BUSDTA.GetUDCDescription('42', 'SP','34O'),0,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34Q',BUSDTA.GetUDCDescription('42', 'SP','34Q'),0,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3A',BUSDTA.GetUDCDescription('42', 'SP','3A'),0,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3B',BUSDTA.GetUDCDescription('42', 'SP','3B'),0,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3E',BUSDTA.GetUDCDescription('42', 'SP','3E'),0,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44',BUSDTA.GetUDCDescription('42', 'SP','44'),0,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44O',BUSDTA.GetUDCDescription('42', 'SP','44O'),0,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44Q',BUSDTA.GetUDCDescription('42', 'SP','44Q'),0,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4A',BUSDTA.GetUDCDescription('42', 'SP','4A'),0,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4B',BUSDTA.GetUDCDescription('42', 'SP','4B'),0,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4E',BUSDTA.GetUDCDescription('42', 'SP','4E'),0,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54',BUSDTA.GetUDCDescription('42', 'SP','54'),0,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54O',BUSDTA.GetUDCDescription('42', 'SP','54O'),0,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54Q',BUSDTA.GetUDCDescription('42', 'SP','54Q'),0,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5A',BUSDTA.GetUDCDescription('42', 'SP','5A'),0,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5B',BUSDTA.GetUDCDescription('42', 'SP','5B'),0,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5E',BUSDTA.GetUDCDescription('42', 'SP','5E'),0,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11',BUSDTA.GetUDCDescription('42', 'SP','11'),1,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11O',BUSDTA.GetUDCDescription('42', 'SP','11O'),1,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11Q',BUSDTA.GetUDCDescription('42', 'SP','11Q'),1,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21',BUSDTA.GetUDCDescription('42', 'SP','21'),1,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21O',BUSDTA.GetUDCDescription('42', 'SP','21O'),1,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21Q',BUSDTA.GetUDCDescription('42', 'SP','21Q'),1,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31',BUSDTA.GetUDCDescription('42', 'SP','31'),1,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31O',BUSDTA.GetUDCDescription('42', 'SP','31O'),1,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31Q',BUSDTA.GetUDCDescription('42', 'SP','31Q'),1,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41',BUSDTA.GetUDCDescription('42', 'SP','41'),1,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41O',BUSDTA.GetUDCDescription('42', 'SP','41O'),1,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41Q',BUSDTA.GetUDCDescription('42', 'SP','41Q'),1,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51',BUSDTA.GetUDCDescription('42', 'SP','51'),1,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51O',BUSDTA.GetUDCDescription('42', 'SP','51O'),1,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51Q',BUSDTA.GetUDCDescription('42', 'SP','51Q'),1,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12',BUSDTA.GetUDCDescription('42', 'SP','12'),1,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12O',BUSDTA.GetUDCDescription('42', 'SP','12O'),1,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12Q',BUSDTA.GetUDCDescription('42', 'SP','12Q'),1,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22',BUSDTA.GetUDCDescription('42', 'SP','22'),1,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22O',BUSDTA.GetUDCDescription('42', 'SP','22O'),1,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22Q',BUSDTA.GetUDCDescription('42', 'SP','22Q'),1,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32',BUSDTA.GetUDCDescription('42', 'SP','32'),1,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32O',BUSDTA.GetUDCDescription('42', 'SP','32O'),1,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32Q',BUSDTA.GetUDCDescription('42', 'SP','32Q'),1,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42',BUSDTA.GetUDCDescription('42', 'SP','42'),1,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42O',BUSDTA.GetUDCDescription('42', 'SP','42O'),1,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42Q',BUSDTA.GetUDCDescription('42', 'SP','42Q'),1,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52',BUSDTA.GetUDCDescription('42', 'SP','52'),1,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52O',BUSDTA.GetUDCDescription('42', 'SP','52O'),1,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52Q',BUSDTA.GetUDCDescription('42', 'SP','52Q'),1,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13',BUSDTA.GetUDCDescription('42', 'SP','13'),1,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13O',BUSDTA.GetUDCDescription('42', 'SP','13O'),1,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13Q',BUSDTA.GetUDCDescription('42', 'SP','13Q'),1,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23',BUSDTA.GetUDCDescription('42', 'SP','23'),1,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23O',BUSDTA.GetUDCDescription('42', 'SP','23O'),1,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23Q',BUSDTA.GetUDCDescription('42', 'SP','23Q'),1,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33',BUSDTA.GetUDCDescription('42', 'SP','33'),1,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33O',BUSDTA.GetUDCDescription('42', 'SP','33O'),1,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33Q',BUSDTA.GetUDCDescription('42', 'SP','33Q'),1,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43',BUSDTA.GetUDCDescription('42', 'SP','43'),1,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43O',BUSDTA.GetUDCDescription('42', 'SP','43O'),1,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43Q',BUSDTA.GetUDCDescription('42', 'SP','43Q'),1,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53',BUSDTA.GetUDCDescription('42', 'SP','53'),1,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53O',BUSDTA.GetUDCDescription('42', 'SP','53O'),1,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53Q',BUSDTA.GetUDCDescription('42', 'SP','53Q'),1,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14',BUSDTA.GetUDCDescription('42', 'SP','14'),1,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14O',BUSDTA.GetUDCDescription('42', 'SP','14O'),1,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14Q',BUSDTA.GetUDCDescription('42', 'SP','14Q'),1,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1A',BUSDTA.GetUDCDescription('42', 'SP','1A'),1,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1B',BUSDTA.GetUDCDescription('42', 'SP','1B'),1,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1E',BUSDTA.GetUDCDescription('42', 'SP','1E'),1,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24',BUSDTA.GetUDCDescription('42', 'SP','24'),1,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24O',BUSDTA.GetUDCDescription('42', 'SP','24O'),1,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24Q',BUSDTA.GetUDCDescription('42', 'SP','24Q'),1,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2A',BUSDTA.GetUDCDescription('42', 'SP','2A'),1,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2B',BUSDTA.GetUDCDescription('42', 'SP','2B'),1,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2E',BUSDTA.GetUDCDescription('42', 'SP','2E'),1,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34',BUSDTA.GetUDCDescription('42', 'SP','34'),1,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34O',BUSDTA.GetUDCDescription('42', 'SP','34O'),1,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34Q',BUSDTA.GetUDCDescription('42', 'SP','34Q'),1,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3A',BUSDTA.GetUDCDescription('42', 'SP','3A'),1,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3B',BUSDTA.GetUDCDescription('42', 'SP','3B'),1,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3E',BUSDTA.GetUDCDescription('42', 'SP','3E'),1,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44',BUSDTA.GetUDCDescription('42', 'SP','44'),1,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44O',BUSDTA.GetUDCDescription('42', 'SP','44O'),1,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44Q',BUSDTA.GetUDCDescription('42', 'SP','44Q'),1,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4A',BUSDTA.GetUDCDescription('42', 'SP','4A'),1,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4B',BUSDTA.GetUDCDescription('42', 'SP','4B'),1,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4E',BUSDTA.GetUDCDescription('42', 'SP','4E'),1,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54',BUSDTA.GetUDCDescription('42', 'SP','54'),1,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54O',BUSDTA.GetUDCDescription('42', 'SP','54O'),1,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54Q',BUSDTA.GetUDCDescription('42', 'SP','54Q'),1,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5A',BUSDTA.GetUDCDescription('42', 'SP','5A'),1,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5B',BUSDTA.GetUDCDescription('42', 'SP','5B'),1,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5E',BUSDTA.GetUDCDescription('42', 'SP','5E'),1,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11',BUSDTA.GetUDCDescription('42', 'SP','11'),2,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11O',BUSDTA.GetUDCDescription('42', 'SP','11O'),2,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11Q',BUSDTA.GetUDCDescription('42', 'SP','11Q'),2,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21',BUSDTA.GetUDCDescription('42', 'SP','21'),2,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21O',BUSDTA.GetUDCDescription('42', 'SP','21O'),2,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21Q',BUSDTA.GetUDCDescription('42', 'SP','21Q'),2,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31',BUSDTA.GetUDCDescription('42', 'SP','31'),2,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31O',BUSDTA.GetUDCDescription('42', 'SP','31O'),2,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31Q',BUSDTA.GetUDCDescription('42', 'SP','31Q'),2,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41',BUSDTA.GetUDCDescription('42', 'SP','41'),2,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41O',BUSDTA.GetUDCDescription('42', 'SP','41O'),2,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41Q',BUSDTA.GetUDCDescription('42', 'SP','41Q'),2,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51',BUSDTA.GetUDCDescription('42', 'SP','51'),2,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51O',BUSDTA.GetUDCDescription('42', 'SP','51O'),2,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51Q',BUSDTA.GetUDCDescription('42', 'SP','51Q'),2,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12',BUSDTA.GetUDCDescription('42', 'SP','12'),2,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12O',BUSDTA.GetUDCDescription('42', 'SP','12O'),2,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12Q',BUSDTA.GetUDCDescription('42', 'SP','12Q'),2,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22',BUSDTA.GetUDCDescription('42', 'SP','22'),2,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22O',BUSDTA.GetUDCDescription('42', 'SP','22O'),2,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22Q',BUSDTA.GetUDCDescription('42', 'SP','22Q'),2,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32',BUSDTA.GetUDCDescription('42', 'SP','32'),2,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32O',BUSDTA.GetUDCDescription('42', 'SP','32O'),2,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32Q',BUSDTA.GetUDCDescription('42', 'SP','32Q'),2,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42',BUSDTA.GetUDCDescription('42', 'SP','42'),2,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42O',BUSDTA.GetUDCDescription('42', 'SP','42O'),2,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42Q',BUSDTA.GetUDCDescription('42', 'SP','42Q'),2,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52',BUSDTA.GetUDCDescription('42', 'SP','52'),2,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52O',BUSDTA.GetUDCDescription('42', 'SP','52O'),2,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52Q',BUSDTA.GetUDCDescription('42', 'SP','52Q'),2,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13',BUSDTA.GetUDCDescription('42', 'SP','13'),2,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13O',BUSDTA.GetUDCDescription('42', 'SP','13O'),2,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13Q',BUSDTA.GetUDCDescription('42', 'SP','13Q'),2,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23',BUSDTA.GetUDCDescription('42', 'SP','23'),2,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23O',BUSDTA.GetUDCDescription('42', 'SP','23O'),2,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23Q',BUSDTA.GetUDCDescription('42', 'SP','23Q'),2,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33',BUSDTA.GetUDCDescription('42', 'SP','33'),2,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33O',BUSDTA.GetUDCDescription('42', 'SP','33O'),2,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33Q',BUSDTA.GetUDCDescription('42', 'SP','33Q'),2,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43',BUSDTA.GetUDCDescription('42', 'SP','43'),2,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43O',BUSDTA.GetUDCDescription('42', 'SP','43O'),2,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43Q',BUSDTA.GetUDCDescription('42', 'SP','43Q'),2,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53',BUSDTA.GetUDCDescription('42', 'SP','53'),2,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53O',BUSDTA.GetUDCDescription('42', 'SP','53O'),2,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53Q',BUSDTA.GetUDCDescription('42', 'SP','53Q'),2,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14',BUSDTA.GetUDCDescription('42', 'SP','14'),2,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14O',BUSDTA.GetUDCDescription('42', 'SP','14O'),2,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14Q',BUSDTA.GetUDCDescription('42', 'SP','14Q'),2,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1A',BUSDTA.GetUDCDescription('42', 'SP','1A'),2,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1B',BUSDTA.GetUDCDescription('42', 'SP','1B'),2,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1E',BUSDTA.GetUDCDescription('42', 'SP','1E'),2,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24',BUSDTA.GetUDCDescription('42', 'SP','24'),2,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24O',BUSDTA.GetUDCDescription('42', 'SP','24O'),2,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24Q',BUSDTA.GetUDCDescription('42', 'SP','24Q'),2,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2A',BUSDTA.GetUDCDescription('42', 'SP','2A'),2,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2B',BUSDTA.GetUDCDescription('42', 'SP','2B'),2,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2E',BUSDTA.GetUDCDescription('42', 'SP','2E'),2,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34',BUSDTA.GetUDCDescription('42', 'SP','34'),2,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34O',BUSDTA.GetUDCDescription('42', 'SP','34O'),2,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34Q',BUSDTA.GetUDCDescription('42', 'SP','34Q'),2,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3A',BUSDTA.GetUDCDescription('42', 'SP','3A'),2,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3B',BUSDTA.GetUDCDescription('42', 'SP','3B'),2,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3E',BUSDTA.GetUDCDescription('42', 'SP','3E'),2,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44',BUSDTA.GetUDCDescription('42', 'SP','44'),2,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44O',BUSDTA.GetUDCDescription('42', 'SP','44O'),2,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44Q',BUSDTA.GetUDCDescription('42', 'SP','44Q'),2,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4A',BUSDTA.GetUDCDescription('42', 'SP','4A'),2,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4B',BUSDTA.GetUDCDescription('42', 'SP','4B'),2,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4E',BUSDTA.GetUDCDescription('42', 'SP','4E'),2,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54',BUSDTA.GetUDCDescription('42', 'SP','54'),2,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54O',BUSDTA.GetUDCDescription('42', 'SP','54O'),2,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54Q',BUSDTA.GetUDCDescription('42', 'SP','54Q'),2,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5A',BUSDTA.GetUDCDescription('42', 'SP','5A'),2,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5B',BUSDTA.GetUDCDescription('42', 'SP','5B'),2,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5E',BUSDTA.GetUDCDescription('42', 'SP','5E'),2,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11',BUSDTA.GetUDCDescription('42', 'SP','11'),3,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11O',BUSDTA.GetUDCDescription('42', 'SP','11O'),3,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'11Q',BUSDTA.GetUDCDescription('42', 'SP','11Q'),3,1,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21',BUSDTA.GetUDCDescription('42', 'SP','21'),3,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21O',BUSDTA.GetUDCDescription('42', 'SP','21O'),3,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'21Q',BUSDTA.GetUDCDescription('42', 'SP','21Q'),3,1,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31',BUSDTA.GetUDCDescription('42', 'SP','31'),3,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31O',BUSDTA.GetUDCDescription('42', 'SP','31O'),3,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'31Q',BUSDTA.GetUDCDescription('42', 'SP','31Q'),3,1,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41',BUSDTA.GetUDCDescription('42', 'SP','41'),3,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41O',BUSDTA.GetUDCDescription('42', 'SP','41O'),3,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'41Q',BUSDTA.GetUDCDescription('42', 'SP','41Q'),3,1,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51',BUSDTA.GetUDCDescription('42', 'SP','51'),3,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51O',BUSDTA.GetUDCDescription('42', 'SP','51O'),3,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'51Q',BUSDTA.GetUDCDescription('42', 'SP','51Q'),3,1,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12',BUSDTA.GetUDCDescription('42', 'SP','12'),3,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12O',BUSDTA.GetUDCDescription('42', 'SP','12O'),3,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'12Q',BUSDTA.GetUDCDescription('42', 'SP','12Q'),3,2,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22',BUSDTA.GetUDCDescription('42', 'SP','22'),3,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22O',BUSDTA.GetUDCDescription('42', 'SP','22O'),3,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'22Q',BUSDTA.GetUDCDescription('42', 'SP','22Q'),3,2,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32',BUSDTA.GetUDCDescription('42', 'SP','32'),3,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32O',BUSDTA.GetUDCDescription('42', 'SP','32O'),3,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'32Q',BUSDTA.GetUDCDescription('42', 'SP','32Q'),3,2,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42',BUSDTA.GetUDCDescription('42', 'SP','42'),3,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42O',BUSDTA.GetUDCDescription('42', 'SP','42O'),3,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'42Q',BUSDTA.GetUDCDescription('42', 'SP','42Q'),3,2,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52',BUSDTA.GetUDCDescription('42', 'SP','52'),3,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52O',BUSDTA.GetUDCDescription('42', 'SP','52O'),3,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'52Q',BUSDTA.GetUDCDescription('42', 'SP','52Q'),3,2,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13',BUSDTA.GetUDCDescription('42', 'SP','13'),3,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13O',BUSDTA.GetUDCDescription('42', 'SP','13O'),3,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'13Q',BUSDTA.GetUDCDescription('42', 'SP','13Q'),3,3,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23',BUSDTA.GetUDCDescription('42', 'SP','23'),3,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23O',BUSDTA.GetUDCDescription('42', 'SP','23O'),3,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'23Q',BUSDTA.GetUDCDescription('42', 'SP','23Q'),3,3,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33',BUSDTA.GetUDCDescription('42', 'SP','33'),3,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33O',BUSDTA.GetUDCDescription('42', 'SP','33O'),3,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'33Q',BUSDTA.GetUDCDescription('42', 'SP','33Q'),3,3,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43',BUSDTA.GetUDCDescription('42', 'SP','43'),3,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43O',BUSDTA.GetUDCDescription('42', 'SP','43O'),3,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'43Q',BUSDTA.GetUDCDescription('42', 'SP','43Q'),3,3,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53',BUSDTA.GetUDCDescription('42', 'SP','53'),3,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53O',BUSDTA.GetUDCDescription('42', 'SP','53O'),3,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'53Q',BUSDTA.GetUDCDescription('42', 'SP','53Q'),3,3,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14',BUSDTA.GetUDCDescription('42', 'SP','14'),3,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14O',BUSDTA.GetUDCDescription('42', 'SP','14O'),3,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'14Q',BUSDTA.GetUDCDescription('42', 'SP','14Q'),3,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1A',BUSDTA.GetUDCDescription('42', 'SP','1A'),3,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1B',BUSDTA.GetUDCDescription('42', 'SP','1B'),3,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'1E',BUSDTA.GetUDCDescription('42', 'SP','1E'),3,4,1,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24',BUSDTA.GetUDCDescription('42', 'SP','24'),3,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24O',BUSDTA.GetUDCDescription('42', 'SP','24O'),3,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'24Q',BUSDTA.GetUDCDescription('42', 'SP','24Q'),3,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2A',BUSDTA.GetUDCDescription('42', 'SP','2A'),3,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2B',BUSDTA.GetUDCDescription('42', 'SP','2B'),3,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'2E',BUSDTA.GetUDCDescription('42', 'SP','2E'),3,4,2,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34',BUSDTA.GetUDCDescription('42', 'SP','34'),3,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34O',BUSDTA.GetUDCDescription('42', 'SP','34O'),3,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'34Q',BUSDTA.GetUDCDescription('42', 'SP','34Q'),3,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3A',BUSDTA.GetUDCDescription('42', 'SP','3A'),3,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3B',BUSDTA.GetUDCDescription('42', 'SP','3B'),3,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'3E',BUSDTA.GetUDCDescription('42', 'SP','3E'),3,4,3,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44',BUSDTA.GetUDCDescription('42', 'SP','44'),3,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44O',BUSDTA.GetUDCDescription('42', 'SP','44O'),3,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'44Q',BUSDTA.GetUDCDescription('42', 'SP','44Q'),3,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4A',BUSDTA.GetUDCDescription('42', 'SP','4A'),3,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4B',BUSDTA.GetUDCDescription('42', 'SP','4B'),3,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'4E',BUSDTA.GetUDCDescription('42', 'SP','4E'),3,4,4,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54',BUSDTA.GetUDCDescription('42', 'SP','54'),3,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54O',BUSDTA.GetUDCDescription('42', 'SP','54O'),3,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'54Q',BUSDTA.GetUDCDescription('42', 'SP','54Q'),3,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5A',BUSDTA.GetUDCDescription('42', 'SP','5A'),3,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5B',BUSDTA.GetUDCDescription('42', 'SP','5B'),3,4,5,1);
insert into BUSDTA.DeliveryCycleMaster values(	'5E',BUSDTA.GetUDCDescription('42', 'SP','5E'),3,4,5,1);
GO

/*Following table is pseudo script for RouteStandardSequence*/
CREATE TABLE [BUSDTA].[RouteStandardSequence](
	[RouteID] [int] NOT NULL,
	[CustomerID] [nchar](10) NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[SequenceNumber] [int] NULL,
 CONSTRAINT [PK_RouteStandardSequence] PRIMARY KEY CLUSTERED 
(
	[RouteID] ASC,
	[CustomerID] ASC,
	[WeekNumber] ASC,
	[DayNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/*Route Standard Sequence.
To populate the Route Standard Sequence, use the query below*/
select ltrim(F0101.ABMCU) as 'RouteID', AIAN8 as 'CustomerID', dsm.Week as 'WeekNumber', 
dsm.Day as 'DayNumber', 1 as 'SequenceNumber' from busdta.F03012 
left outer join BUSDTA.F0101 on ABAN8 = AIAN8
inner join BUSDTA.dsm on dsm.DayCode1 = AISTOP 
where  1=1
group by dsm.Week,AIAN8,ABALPH ,AISTOP,dsm.Day,F0101.ABMCU
order by F0101.ABMCU 


/*Find Data pertaining to particular route by changing the variables in where clause below*/
select * from busdta.RouteStandardSequence 
where routeid like <Route_Id>


/*Calendar Pattern (Date Instanced Stops) for Delivery Cycle per Route 
(Exploded view and customer stop for a given date, without sequecing)*/
CREATE TABLE [busdta].[DeliveryCycleCalendar](
	[RouteID] [numeric](4, 0) NOT NULL,
	[CustomerId] [numeric](10, 0) NOT NULL,
	[DeliveryDayCode] [nchar](3) NULL,
	[DelDate] [date] NOT NULL,
 CONSTRAINT [PK_DeliveryCycleCalendar] PRIMARY KEY CLUSTERED 
(
	[RouteID] ASC,
	[CustomerId] ASC,
	[DelDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/*Instance of delivery cycle calendar pattern*/

CREATE TABLE [BUSDTA].[RouteStopSequence](
	[RouteID] [numeric](4, 0) NOT NULL,
	[CustomerId] [numeric](10, 0) NOT NULL,
	[DelDate] [date] NOT NULL,
	[SequenceNumber] [numeric](4, 0) NULL,
	[StopId] [numeric](3, 0) NULL,
 CONSTRAINT [PK_RouteStopSequence] PRIMARY KEY CLUSTERED 
(
	[RouteID] ASC,
	[CustomerId] ASC,
	[DelDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



--##NOTE: The above structure shall evolve with more functionality to address.
--##The above tables will get renamed as per Naming Convention.