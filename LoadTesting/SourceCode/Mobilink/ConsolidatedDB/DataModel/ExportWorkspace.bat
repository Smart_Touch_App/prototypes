@echo off
SET pwd=
SET /P pwd=Sa Password:
if "%pwd%"=="" GOTO Error
del D:\Exports\WorkspaceExports\*.* /Q
bcp DataModelWorkspace.dbo.DeliveryCycleMaster out D:\Exports\WorkspaceExports\DeliveryCycleMaster.dat -n -SMSDEVDB1 -Usa -P%pwd%
bcp DataModelWorkspace.dbo.RouteStandardSequence out D:\Exports\WorkspaceExports\RouteStandardSequence.dat -n -SMSDEVDB1 -Usa -P%pwd%
SET pwd=
echo Export Complete
GOTO End
:Error
echo Password Required
:End

