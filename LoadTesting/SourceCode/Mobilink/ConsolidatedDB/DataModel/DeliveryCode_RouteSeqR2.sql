


/*Master/Reference Pattern for Delivery Cycle (applicable for ALL routes)*/
/*NPATEL - Changed data type to better align to JDE available data types */
CREATE TABLE dbo.[DeliveryCycleMaster](
	[DeliveryDayCode] [nchar](3) NOT NULL,
	[DeliveryDayCodeDesc] [nchar](50) NULL,
	[PeriodNumber] [numeric] (4,0) NOT NULL,
	[WeekNumber] [numeric] (4,0) NOT NULL,
	[DayNumber] [numeric] (4,0) NOT NULL,
	[IsServiceStop] [numeric] (4,0) NULL,
 CONSTRAINT [PK_DeliveryCycleMaster] PRIMARY KEY CLUSTERED 
(
	[DeliveryDayCode] ASC,
	[PeriodNumber] ASC,
	[WeekNumber] ASC,
	[DayNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE dbo.[DeliveryCycleMaster] ADD  CONSTRAINT [DF__DeliveryC__IsSer__74100195]  DEFAULT ((1)) FOR [IsServiceStop]
GO



/*Insert scripts for Reference Pattern. This scripts will create all the master data that shall be shared by all routes. */
/* Request the business team complete the DelDay_NewMobile_LoadTemplate.xlsx template to populate this with real data */
/* See CSV for data created by business */
/* Once loaded for period 1,2,3 create zero set */

/*Create Zero Rows */
insert into dbo.DeliveryCycleMaster 
select DeliveryDayCode,DeliveryDayCodeDesc,0,WeekNumber,DayNumber,IsServiceStop from dbo.DeliveryCycleMaster 
	Group By DeliveryDayCode,DeliveryDayCodeDesc,WeekNumber,DayNumber,IsServiceStop 

/*Following table is pseudo script for RouteStandardSequence*/
/* NPATEL Updated format to align better to JDE data */
/* need to resolve if RouteID should be the data key for paritioning and define RouteID data dictionary defination for clarity */


DROP table dbo.[RouteStandardSequence]

CREATE TABLE dbo.[RouteStandardSequence](
	[RouteID] [nchar] (10) NOT NULL,
	[CustomerID] [numeric](8,0) NOT NULL,
	[WeekNumber]  [numeric](4,0) NOT NULL,
	[DayNumber]  [numeric](4,0) NOT NULL,
	[SequenceNumber] [numeric](4,0) NULL,
 CONSTRAINT [PK_RouteStandardSequence] PRIMARY KEY CLUSTERED 
(
	[RouteID] ASC,
	[CustomerID] ASC,
	[WeekNumber] ASC,
	[DayNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

select * from dbo.[RouteStandardSequence] order by RouteID,WeekNumber,DayNumber,SequenceNumber



/*Route Standard Sequence.To populate the Route Standard Sequence, use the query below*/
insert into DataModelWorkspace.dbo.[RouteStandardSequence]
select FFUSER,ABAN8,[WeekNumber],[DayNumber],cast(AIZON as int) from MobileDataModel.BUSDTA.F56M0001 
	join MobileDataModel.BUSDTA.F90CA003 on SMSLSM=FFAN8
	join MobileDataModel.BUSDTA.F90CA086 on CRCUAN8=SMAN8
	join MobileDataModel.BUSDTA.F0101 on ABAN8=CRCRAN8
	join MobileDataModel.BUSDTA.F03012 on AIAN8=ABAN8
	join DataModelWorkspace.dbo.[DeliveryCycleMaster] on AISTOP=DeliveryDayCode and [PeriodNumber]=0
	where AIZON>' '
	GROUP BY FFUSER,ABAN8,[WeekNumber],[DayNumber],AIZON

/* Adjust Seq Numbers */ 
drop table dbo.RouteStandardSequenceTemp 

select RouteID,CustomerID,WeekNumber,DayNumber,
	(ROW_NUMBER() over (PARTITION By RouteID,WeekNumber,DayNumber Order By  routeid,WeekNumber,DayNumber,SequenceNumber))*1 newSeqNumber
	 into dbo.RouteStandardSequenceTemp from  dbo.RouteStandardSequence

delete from dbo.RouteStandardSequence 
insert into dbo.RouteStandardSequence select * from dbo.RouteStandardSequenceTemp 

select * from dbo.RouteStandardSequence Order By routeid,WeekNumber,DayNumber,SequenceNumber

/*Find Data pertaining to particular route by changing the variables in where clause below*/
select * from dbo.RouteStandardSequence 
where routeid like <Route_Id>


--##NOTE: The above structure shall evolve with more functionality to address.
--##The above tables will get renamed as per Naming Convention.


/* TODO - Need to store in the system a global constant as a date value which is a calendar date that represents the Monday of Week 1 of Period 1.  This should be old enough that it covered all historical use cases which might be available on the system at go live*/
--A good value for testing is 12/29/2015  


/* Need a procedures that given a calendar date and the system static starting calendar date, return would be the PeriodNumber, WeekNumber, and DayNumber are the map to the input calendar date */
/* this will be used often when translating data from the delivery day code structure to the actualy event instance table
function GetPeriodMapforDate (@requestDate date,@startDate date, cyclecolumnt int (1=period,2=week,3=day )
RETURNS daycycleValue int
AS BEGIN


example call
getperiodmapfordate (1/15/2015,12/29/14,1)
return 1 
getperiodmapfordate (1/15/2015,12/29/14,2)
return 3 
getperiodmapfordate (1/15/2015,12/29/14,4)
return 4 
	
   










