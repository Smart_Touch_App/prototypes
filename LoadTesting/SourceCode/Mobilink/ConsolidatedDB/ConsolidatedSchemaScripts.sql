USE [MobileDataModel]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[BUSDTA].[FK__User_Role__App_u__37B03374]') AND parent_object_id = OBJECT_ID(N'[BUSDTA].[User_Role_Map]'))
ALTER TABLE [BUSDTA].[User_Role_Map] DROP CONSTRAINT [FK__User_Role__App_u__37B03374]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[BUSDTA].[FK__Route_Use__App_u__32EB7E57]') AND parent_object_id = OBJECT_ID(N'[BUSDTA].[Route_User_Map]'))
ALTER TABLE [BUSDTA].[Route_User_Map] DROP CONSTRAINT [FK__Route_Use__App_u__32EB7E57]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__User_Role__last___3D690CCA]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[User_Role_Map] DROP CONSTRAINT [DF__User_Role__last___3D690CCA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF_user_master_Created_On]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[user_master] DROP CONSTRAINT [DF_user_master_Created_On]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF_user_master_active]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[user_master] DROP CONSTRAINT [DF_user_master_active]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__user_mast__last___44160A59]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[user_master] DROP CONSTRAINT [DF__user_mast__last___44160A59]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__UDCKEYLIS__last___33AA9866]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[UDCKEYLIST] DROP CONSTRAINT [DF__UDCKEYLIS__last___33AA9866]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Route_Use__last___4AC307E8]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Route_User_Map] DROP CONSTRAINT [DF__Route_Use__last___4AC307E8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Route_Dev__last___51700577]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Route_Device_Map] DROP CONSTRAINT [DF__Route_Dev__last___51700577]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__ReasonCod__last___15B0212B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[ReasonCodeMaster] DROP CONSTRAINT [DF__ReasonCod__last___15B0212B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Payment_R__last___442BE449]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Payment_Ref_Map] DROP CONSTRAINT [DF__Payment_R__last___442BE449]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Order_Hea__last___55FFB06A]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Order_Header] DROP CONSTRAINT [DF__Order_Hea__last___55FFB06A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Order_Det__last___4F52B2DB]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Order_Detail] DROP CONSTRAINT [DF__Order_Det__last___4F52B2DB]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__M4016__last_modi__3D7EE6BA]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[M4016] DROP CONSTRAINT [DF__M4016__last_modi__3D7EE6BA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__M03042__last_mod__36D1E92B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[M03042] DROP CONSTRAINT [DF__M03042__last_mod__36D1E92B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__M03011__last_mod__3024EB9C]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[M03011] DROP CONSTRAINT [DF__M03011__last_mod__3024EB9C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F90CA086__last_m__47477CBF]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F90CA086] DROP CONSTRAINT [DF__F90CA086__last_m__47477CBF]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F90CA042__last_m__3F122971]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F90CA042] DROP CONSTRAINT [DF__F90CA042__last_m__3F122971]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F90CA003__last_m__4282C7A2]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F90CA003] DROP CONSTRAINT [DF__F90CA003__last_m__4282C7A2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F56M0001__last_m__3FA65AF7]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F56M0001] DROP CONSTRAINT [DF__F56M0001__last_m__3FA65AF7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F56M0000__last_m__3CC9EE4C]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F56M0000] DROP CONSTRAINT [DF__F56M0000__last_m__3CC9EE4C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F42119__last_mod__035C66C6]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F42119] DROP CONSTRAINT [DF__F42119__last_mod__035C66C6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F42019__last_mod__007FFA1B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F42019] DROP CONSTRAINT [DF__F42019__last_mod__007FFA1B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4106__last_modi__750E476F]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4106] DROP CONSTRAINT [DF__F4106__last_modi__750E476F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4102__last_modi__7231DAC4]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4102] DROP CONSTRAINT [DF__F4102__last_modi__7231DAC4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4101__last_modi__6F556E19]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4101] DROP CONSTRAINT [DF__F4101__last_modi__6F556E19]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F41002__last_mod__7DA38D70]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F41002] DROP CONSTRAINT [DF__F41002__last_mod__7DA38D70]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F40942__last_mod__7AC720C5]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F40942] DROP CONSTRAINT [DF__F40942__last_mod__7AC720C5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4092__last_modi__6C79016E]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4092] DROP CONSTRAINT [DF__F4092__last_modi__6C79016E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4076__last_modi__699C94C3]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4076] DROP CONSTRAINT [DF__F4076__last_modi__699C94C3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4075__last_modi__66C02818]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4075] DROP CONSTRAINT [DF__F4075__last_modi__66C02818]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4072__last_modi__63E3BB6D]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4072] DROP CONSTRAINT [DF__F4072__last_modi__63E3BB6D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4071__last_modi__61074EC2]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4071] DROP CONSTRAINT [DF__F4071__last_modi__61074EC2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4070__last_modi__5E2AE217]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4070] DROP CONSTRAINT [DF__F4070__last_modi__5E2AE217]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4015__last_modi__5B4E756C]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4015] DROP CONSTRAINT [DF__F4015__last_modi__5B4E756C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F40073__last_mod__77EAB41A]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F40073] DROP CONSTRAINT [DF__F40073__last_mod__77EAB41A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F03012__last_mod__587208C1]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F03012] DROP CONSTRAINT [DF__F03012__last_mod__587208C1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0150__last_modi__52B92F6B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0150] DROP CONSTRAINT [DF__F0150__last_modi__52B92F6B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0116__last_modi__4FDCC2C0]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0116] DROP CONSTRAINT [DF__F0116__last_modi__4FDCC2C0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F01151__last_mod__55959C16]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F01151] DROP CONSTRAINT [DF__F01151__last_mod__55959C16]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0115__last_modi__4D005615]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0115] DROP CONSTRAINT [DF__F0115__last_modi__4D005615]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0101__last_modi__4A23E96A]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0101] DROP CONSTRAINT [DF__F0101__last_modi__4A23E96A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0014__last_modi__39ED81A1]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0014] DROP CONSTRAINT [DF__F0014__last_modi__39ED81A1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0006__last_modi__371114F6]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0006] DROP CONSTRAINT [DF__F0006__last_modi__371114F6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0005__last_modi__3434A84B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0005] DROP CONSTRAINT [DF__F0005__last_modi__3434A84B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0004__last_modi__31583BA0]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0004] DROP CONSTRAINT [DF__F0004__last_modi__31583BA0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Device_Ma__last___69478F08]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Device_Master] DROP CONSTRAINT [DF__Device_Ma__last___69478F08]
END

GO
/****** Object:  Table [BUSDTA].[User_Role_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[User_Role_Map]') AND type in (N'U'))
DROP TABLE [BUSDTA].[User_Role_Map]
GO
/****** Object:  Table [BUSDTA].[user_master]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[user_master]') AND type in (N'U'))
DROP TABLE [BUSDTA].[user_master]
GO
/****** Object:  Table [BUSDTA].[UDCKEYLIST]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[UDCKEYLIST]') AND type in (N'U'))
DROP TABLE [BUSDTA].[UDCKEYLIST]
GO
/****** Object:  Table [BUSDTA].[Route_User_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Route_User_Map]') AND type in (N'U'))
DROP TABLE [BUSDTA].[Route_User_Map]
GO
/****** Object:  Table [BUSDTA].[Route_Device_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Route_Device_Map]') AND type in (N'U'))
DROP TABLE [BUSDTA].[Route_Device_Map]
GO
/****** Object:  Table [BUSDTA].[ReasonCodeMaster]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[ReasonCodeMaster]') AND type in (N'U'))
DROP TABLE [BUSDTA].[ReasonCodeMaster]
GO
/****** Object:  Table [BUSDTA].[PickOrder_Exception]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[PickOrder_Exception]') AND type in (N'U'))
DROP TABLE [BUSDTA].[PickOrder_Exception]
GO
/****** Object:  Table [BUSDTA].[PickOrder]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[PickOrder]') AND type in (N'U'))
DROP TABLE [BUSDTA].[PickOrder]
GO
/****** Object:  Table [BUSDTA].[Payment_Ref_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Payment_Ref_Map]') AND type in (N'U'))
DROP TABLE [BUSDTA].[Payment_Ref_Map]
GO
/****** Object:  Table [BUSDTA].[Order_Header]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Order_Header]') AND type in (N'U'))
DROP TABLE [BUSDTA].[Order_Header]
GO
/****** Object:  Table [BUSDTA].[Order_Detail]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Order_Detail]') AND type in (N'U'))
DROP TABLE [BUSDTA].[Order_Detail]
GO
/****** Object:  Table [BUSDTA].[M4016]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M4016]') AND type in (N'U'))
DROP TABLE [BUSDTA].[M4016]
GO
/****** Object:  Table [BUSDTA].[M080111]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M080111]') AND type in (N'U'))
DROP TABLE [BUSDTA].[M080111]
GO
/****** Object:  Table [BUSDTA].[M03042]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M03042]') AND type in (N'U'))
DROP TABLE [BUSDTA].[M03042]
GO
/****** Object:  Table [BUSDTA].[M03011]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M03011]') AND type in (N'U'))
DROP TABLE [BUSDTA].[M03011]
GO
/****** Object:  Table [BUSDTA].[M0111]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M0111]') AND type in (N'U'))
DROP TABLE [BUSDTA].[M0111]
GO
/****** Object:  Table [BUSDTA].[F90CA086]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F90CA086]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F90CA086]
GO
/****** Object:  Table [BUSDTA].[F90CA042]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F90CA042]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F90CA042]
GO
/****** Object:  Table [BUSDTA].[F90CA003]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F90CA003]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F90CA003]
GO
/****** Object:  Table [BUSDTA].[F56M0001]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F56M0001]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F56M0001]
GO
/****** Object:  Table [BUSDTA].[F56M0000]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F56M0000]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F56M0000]
GO
/****** Object:  Table [BUSDTA].[F42119]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F42119]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F42119]
GO
/****** Object:  Table [BUSDTA].[F42019]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F42019]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F42019]
GO
/****** Object:  Table [BUSDTA].[F4106]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4106]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4106]
GO
/****** Object:  Table [BUSDTA].[F4102]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4102]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4102]
GO
/****** Object:  Table [BUSDTA].[F4101]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4101]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4101]
GO
/****** Object:  Table [BUSDTA].[F41002]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F41002]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F41002]
GO
/****** Object:  Table [BUSDTA].[F40942]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F40942]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F40942]
GO
/****** Object:  Table [BUSDTA].[F40941]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F40941]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F40941]
GO
/****** Object:  Table [BUSDTA].[F4092]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4092]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4092]
GO
/****** Object:  Table [BUSDTA].[F4076]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4076]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4076]
GO
/****** Object:  Table [BUSDTA].[F4075]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4075]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4075]
GO
/****** Object:  Table [BUSDTA].[F4072]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4072]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4072]
GO
/****** Object:  Table [BUSDTA].[F4071]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4071]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4071]
GO
/****** Object:  Table [BUSDTA].[F4070]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4070]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4070]
GO
/****** Object:  Table [BUSDTA].[F4015]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4015]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F4015]
GO
/****** Object:  Table [BUSDTA].[F40073]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F40073]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F40073]
GO
/****** Object:  Table [BUSDTA].[F03012]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F03012]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F03012]
GO
/****** Object:  Table [BUSDTA].[F0150]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0150]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0150]
GO
/****** Object:  Table [BUSDTA].[F0116]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0116]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0116]
GO
/****** Object:  Table [BUSDTA].[F01151]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F01151]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F01151]
GO
/****** Object:  Table [BUSDTA].[F0115]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0115]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0115]
GO
/****** Object:  Table [BUSDTA].[F0111]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0111]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0111]
GO
/****** Object:  Table [BUSDTA].[F0101]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0101]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0101]
GO
/****** Object:  Table [BUSDTA].[F0014]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0014]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0014]
GO
/****** Object:  Table [BUSDTA].[F0006]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0006]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0006]
GO
/****** Object:  Table [BUSDTA].[F0005]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0005]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0005]
GO
/****** Object:  Table [BUSDTA].[F0004]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0004]') AND type in (N'U'))
DROP TABLE [BUSDTA].[F0004]
GO
/****** Object:  Table [BUSDTA].[Device_Master]    Script Date: 4/8/2015 3:37:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Device_Master]') AND type in (N'U'))
DROP TABLE [BUSDTA].[Device_Master]
GO
/****** Object:  Table [BUSDTA].[Device_Master]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Device_Master]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[Device_Master](
	[Device_Id] [varchar](30) NOT NULL,
	[Active] [int] NULL,
	[last_modified] [datetime] NOT NULL,
	[manufacturer] [nvarchar](50) NULL,
	[model] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Device_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BUSDTA].[F0004]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0004]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0004](
	[DTSY] [nchar](4) NOT NULL,
	[DTRT] [nchar](2) NOT NULL,
	[DTDL01] [nchar](30) NULL,
	[DTCDL] [float] NULL,
	[DTLN2] [nchar](1) NULL,
	[DTCNUM] [nchar](1) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0004_PK] PRIMARY KEY CLUSTERED 
(
	[DTSY] ASC,
	[DTRT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0005]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0005]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0005](
	[DRSY] [nchar](4) NOT NULL,
	[DRRT] [nchar](2) NOT NULL,
	[DRKY] [nchar](10) NOT NULL,
	[DRDL01] [nchar](30) NULL,
	[DRDL02] [nchar](30) NULL,
	[DRSPHD] [nchar](10) NULL,
	[DRHRDC] [nchar](1) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0005_PK] PRIMARY KEY CLUSTERED 
(
	[DRSY] ASC,
	[DRRT] ASC,
	[DRKY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0006]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0006]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0006](
	[MCMCU] [nchar](12) NOT NULL,
	[MCSTYL] [nchar](2) NULL,
	[MCLDM] [nchar](1) NULL,
	[MCCO] [nchar](5) NULL,
	[MCAN8] [float] NULL,
	[MCDL01] [nchar](30) NULL,
	[MCRP01] [nchar](3) NULL,
	[MCRP02] [nchar](3) NULL,
	[MCRP03] [nchar](3) NULL,
	[MCRP04] [nchar](3) NULL,
	[MCRP05] [nchar](3) NULL,
	[MCRP06] [nchar](3) NULL,
	[MCRP07] [nchar](3) NULL,
	[MCRP08] [nchar](3) NULL,
	[MCRP09] [nchar](3) NULL,
	[MCRP10] [nchar](3) NULL,
	[MCRP11] [nchar](3) NULL,
	[MCRP12] [nchar](3) NULL,
	[MCRP13] [nchar](3) NULL,
	[MCRP14] [nchar](3) NULL,
	[MCRP15] [nchar](3) NULL,
	[MCRP16] [nchar](3) NULL,
	[MCRP17] [nchar](3) NULL,
	[MCRP18] [nchar](3) NULL,
	[MCRP19] [nchar](3) NULL,
	[MCRP20] [nchar](3) NULL,
	[MCRP21] [nchar](10) NULL,
	[MCRP22] [nchar](10) NULL,
	[MCRP23] [nchar](10) NULL,
	[MCRP24] [nchar](10) NULL,
	[MCRP25] [nchar](10) NULL,
	[MCRP26] [nchar](10) NULL,
	[MCRP27] [nchar](10) NULL,
	[MCRP28] [nchar](10) NULL,
	[MCRP29] [nchar](10) NULL,
	[MCRP30] [nchar](10) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0006_PK] PRIMARY KEY CLUSTERED 
(
	[MCMCU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0014]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0014]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0014](
	[PNPTC] [nchar](3) NOT NULL,
	[PNPTD] [nchar](30) NULL,
	[PNDCP] [float] NULL,
	[PNDCD] [float] NULL,
	[PNNDTP] [float] NULL,
	[PNNSP] [float] NULL,
	[PNDTPA] [float] NULL,
	[PNPXDM] [float] NULL,
	[PNPXDD] [float] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0014_PK] PRIMARY KEY CLUSTERED 
(
	[PNPTC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0101]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0101]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0101](
	[ABAN8] [numeric](8, 0) NOT NULL,
	[ABALKY] [nchar](20) NULL,
	[ABTAX] [nchar](20) NULL,
	[ABALPH] [nchar](40) NULL,
	[ABMCU] [nchar](12) NULL,
	[ABSIC] [nchar](10) NULL,
	[ABLNGP] [nchar](2) NULL,
	[ABAT1] [nchar](3) NULL,
	[ABCM] [nchar](2) NULL,
	[ABTAXC] [nchar](1) NULL,
	[ABAT2] [nchar](1) NULL,
	[ABAN81] [float] NULL,
	[ABAN82] [float] NULL,
	[ABAN83] [float] NULL,
	[ABAN84] [float] NULL,
	[ABAN86] [float] NULL,
	[ABAN85] [float] NULL,
	[ABAC01] [nchar](3) NULL,
	[ABAC02] [nchar](3) NULL,
	[ABAC03] [nchar](3) NULL,
	[ABAC04] [nchar](3) NULL,
	[ABAC05] [nchar](3) NULL,
	[ABAC06] [nchar](3) NULL,
	[ABAC07] [nchar](3) NULL,
	[ABAC08] [nchar](3) NULL,
	[ABAC09] [nchar](3) NULL,
	[ABAC10] [nchar](3) NULL,
	[ABAC11] [nchar](3) NULL,
	[ABAC12] [nchar](3) NULL,
	[ABAC13] [nchar](3) NULL,
	[ABAC14] [nchar](3) NULL,
	[ABAC15] [nchar](3) NULL,
	[ABAC16] [nchar](3) NULL,
	[ABAC17] [nchar](3) NULL,
	[ABAC18] [nchar](3) NULL,
	[ABAC19] [nchar](3) NULL,
	[ABAC20] [nchar](3) NULL,
	[ABAC21] [nchar](3) NULL,
	[ABAC22] [nchar](3) NULL,
	[ABAC23] [nchar](3) NULL,
	[ABAC24] [nchar](3) NULL,
	[ABAC25] [nchar](3) NULL,
	[ABAC26] [nchar](3) NULL,
	[ABAC27] [nchar](3) NULL,
	[ABAC28] [nchar](3) NULL,
	[ABAC29] [nchar](3) NULL,
	[ABAC30] [nchar](3) NULL,
	[ABRMK] [nchar](30) NULL,
	[ABTXCT] [nchar](20) NULL,
	[ABTX2] [nchar](20) NULL,
	[ABALP1] [nchar](40) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0101_PK] PRIMARY KEY CLUSTERED 
(
	[ABAN8] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0111]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0111]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0111](
	[WWAN8] [float] NOT NULL,
	[WWIDLN] [float] NOT NULL,
	[WWDSS5] [float] NULL,
	[WWMLNM] [nchar](40) NULL,
	[WWATTL] [nchar](40) NULL,
	[WWREM1] [nchar](40) NULL,
	[WWSLNM] [nchar](40) NULL,
	[WWALPH] [nchar](40) NULL,
	[WWDC] [nchar](40) NULL,
	[WWGNNM] [nchar](25) NULL,
	[WWMDNM] [nchar](25) NULL,
	[WWSRNM] [nchar](25) NULL,
	[WWTYC] [nchar](1) NULL,
	[WWW001] [nchar](3) NULL,
	[WWW002] [nchar](3) NULL,
	[WWW003] [nchar](3) NULL,
	[WWW004] [nchar](3) NULL,
	[WWW005] [nchar](3) NULL,
	[WWW006] [nchar](3) NULL,
	[WWW007] [nchar](3) NULL,
	[WWW008] [nchar](3) NULL,
	[WWW009] [nchar](3) NULL,
	[WWW010] [nchar](3) NULL,
	[WWMLN1] [nchar](40) NULL,
	[WWALP1] [nchar](40) NULL,
	[WWUSER] [nchar](10) NULL,
	[WWPID] [nchar](10) NULL,
	[WWUPMJ] [numeric](18, 0) NULL,
	[WWJOBN] [nchar](10) NULL,
	[WWUPMT] [float] NULL,
	[WWNTYP] [nchar](3) NULL,
	[WWNICK] [nchar](40) NULL,
	[WWGEND] [nchar](1) NULL,
	[WWDDATE] [float] NULL,
	[WWDMON] [float] NULL,
	[WWDYR] [float] NULL,
	[WWWN001] [nchar](3) NULL,
	[WWWN002] [nchar](3) NULL,
	[WWWN003] [nchar](3) NULL,
	[WWWN004] [nchar](3) NULL,
	[WWWN005] [nchar](3) NULL,
	[WWWN006] [nchar](3) NULL,
	[WWWN007] [nchar](3) NULL,
	[WWWN008] [nchar](3) NULL,
	[WWWN009] [nchar](3) NULL,
	[WWWN010] [nchar](3) NULL,
	[WWFUCO] [nchar](10) NULL,
	[WWPCM] [nchar](10) NULL,
	[WWPCF] [nchar](3) NULL,
	[WWACTIN] [nchar](1) NULL,
	[WWCFRGUID] [nchar](36) NULL,
	[WWSYNCS] [float] NULL,
	[WWCAAD] [float] NULL,
 CONSTRAINT [F0111_PK] PRIMARY KEY CLUSTERED 
(
	[WWAN8] ASC,
	[WWIDLN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0115]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0115]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0115](
	[WPAN8] [numeric](8, 0) NOT NULL,
	[WPIDLN] [numeric](5, 0) NOT NULL,
	[WPRCK7] [numeric](5, 0) NOT NULL,
	[WPCNLN] [numeric](5, 0) NOT NULL,
	[WPPHTP] [nchar](4) NULL,
	[WPAR1] [nchar](6) NULL,
	[WPPH1] [nchar](20) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0115_PK] PRIMARY KEY CLUSTERED 
(
	[WPAN8] ASC,
	[WPIDLN] ASC,
	[WPCNLN] ASC,
	[WPRCK7] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F01151]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F01151]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F01151](
	[EAAN8] [numeric](8, 0) NOT NULL,
	[EAIDLN] [numeric](5, 0) NOT NULL,
	[EARCK7] [numeric](5, 0) NOT NULL,
	[EAETP] [nchar](4) NULL,
	[EAEMAL] [nvarchar](256) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F01151_PK] PRIMARY KEY CLUSTERED 
(
	[EAAN8] ASC,
	[EAIDLN] ASC,
	[EARCK7] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0116]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0116]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0116](
	[ALAN8] [numeric](8, 0) NOT NULL,
	[ALEFTB] [numeric](18, 0) NOT NULL,
	[ALEFTF] [nchar](1) NULL,
	[ALADD1] [nchar](40) NULL,
	[ALADD2] [nchar](40) NULL,
	[ALADD3] [nchar](40) NULL,
	[ALADD4] [nchar](40) NULL,
	[ALADDZ] [nchar](12) NULL,
	[ALCTY1] [nchar](25) NULL,
	[ALCOUN] [nchar](25) NULL,
	[ALADDS] [nchar](3) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0116_PK] PRIMARY KEY CLUSTERED 
(
	[ALAN8] ASC,
	[ALEFTB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F0150]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F0150]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F0150](
	[MAOSTP] [nchar](3) NOT NULL,
	[MAPA8] [numeric](8, 0) NOT NULL,
	[MAAN8] [numeric](8, 0) NOT NULL,
	[MABEFD] [numeric](18, 0) NULL,
	[MAEEFD] [numeric](18, 0) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F0150_PK] PRIMARY KEY CLUSTERED 
(
	[MAOSTP] ASC,
	[MAPA8] ASC,
	[MAAN8] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F03012]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F03012]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F03012](
	[AIAN8] [numeric](8, 0) NOT NULL,
	[AICO] [nchar](5) NOT NULL,
	[AIMCUR] [nchar](12) NULL,
	[AITXA1] [nchar](10) NULL,
	[AIEXR1] [nchar](2) NULL,
	[AIACL] [float] NULL,
	[AIHDAR] [nchar](1) NULL,
	[AITRAR] [nchar](3) NULL,
	[AISTTO] [nchar](1) NULL,
	[AIRYIN] [nchar](1) NULL,
	[AISTMT] [nchar](1) NULL,
	[AIARPY] [float] NULL,
	[AISITO] [nchar](1) NULL,
	[AICYCN] [nchar](2) NULL,
	[AIBO] [nchar](1) NULL,
	[AITSTA] [nchar](2) NULL,
	[AICKHC] [nchar](1) NULL,
	[AIDLC] [numeric](18, 0) NULL,
	[AIDNLT] [nchar](1) NULL,
	[AIPLCR] [nchar](10) NULL,
	[AIRVDJ] [numeric](18, 0) NULL,
	[AIDSO] [float] NULL,
	[AICMGR] [nchar](10) NULL,
	[AICLMG] [nchar](10) NULL,
	[AIAB2] [nchar](1) NULL,
	[AIDT1J] [numeric](18, 0) NULL,
	[AIDFIJ] [numeric](18, 0) NULL,
	[AIDLIJ] [numeric](18, 0) NULL,
	[AIDLP] [numeric](18, 0) NULL,
	[AIASTY] [float] NULL,
	[AISPYE] [float] NULL,
	[AIAHB] [float] NULL,
	[AIALP] [float] NULL,
	[AIABAM] [float] NULL,
	[AIABA1] [float] NULL,
	[AIAPRC] [float] NULL,
	[AIMAXO] [float] NULL,
	[AIMINO] [float] NULL,
	[AIOYTD] [float] NULL,
	[AIOPY] [float] NULL,
	[AIPOPN] [nchar](10) NULL,
	[AIDAOJ] [numeric](18, 0) NULL,
	[AIAN8R] [float] NULL,
	[AIBADT] [nchar](1) NULL,
	[AICPGP] [nchar](8) NULL,
	[AIORTP] [nchar](8) NULL,
	[AITRDC] [float] NULL,
	[AIINMG] [nchar](10) NULL,
	[AIEXHD] [nchar](1) NULL,
	[AIHOLD] [nchar](2) NULL,
	[AIROUT] [nchar](3) NULL,
	[AISTOP] [nchar](3) NULL,
	[AIZON] [nchar](3) NULL,
	[AICARS] [float] NULL,
	[AIDEL1] [nchar](30) NULL,
	[AIDEL2] [nchar](30) NULL,
	[AILTDT] [float] NULL,
	[AIFRTH] [nchar](3) NULL,
	[AIAFT] [nchar](1) NULL,
	[AIAPTS] [nchar](1) NULL,
	[AISBAL] [nchar](1) NULL,
	[AIBACK] [nchar](1) NULL,
	[AIPORQ] [nchar](1) NULL,
	[AIPRIO] [nchar](1) NULL,
	[AIARTO] [nchar](1) NULL,
	[AIINVC] [float] NULL,
	[AIICON] [nchar](1) NULL,
	[AIBLFR] [nchar](1) NULL,
	[AINIVD] [numeric](18, 0) NULL,
	[AILEDJ] [numeric](18, 0) NULL,
	[AIPLST] [nchar](1) NULL,
	[AIEDF1] [nchar](1) NULL,
	[AIEDF2] [nchar](1) NULL,
	[AIASN] [nchar](8) NULL,
	[AIDSPA] [nchar](1) NULL,
	[AICRMD] [nchar](1) NULL,
	[AIAMCR] [float] NULL,
	[AIAC01] [nchar](3) NULL,
	[AIAC02] [nchar](3) NULL,
	[AIAC03] [nchar](3) NULL,
	[AIAC04] [nchar](3) NULL,
	[AIAC05] [nchar](3) NULL,
	[AIAC06] [nchar](3) NULL,
	[AIAC07] [nchar](3) NULL,
	[AIAC08] [nchar](3) NULL,
	[AIAC09] [nchar](3) NULL,
	[AIAC10] [nchar](3) NULL,
	[AIAC11] [nchar](3) NULL,
	[AIAC12] [nchar](3) NULL,
	[AIAC13] [nchar](3) NULL,
	[AIAC14] [nchar](3) NULL,
	[AIAC15] [nchar](3) NULL,
	[AIAC16] [nchar](3) NULL,
	[AIAC17] [nchar](3) NULL,
	[AIAC18] [nchar](3) NULL,
	[AIAC19] [nchar](3) NULL,
	[AIAC20] [nchar](3) NULL,
	[AIAC21] [nchar](3) NULL,
	[AIAC22] [nchar](3) NULL,
	[AIAC23] [nchar](3) NULL,
	[AIAC24] [nchar](3) NULL,
	[AIAC25] [nchar](3) NULL,
	[AIAC26] [nchar](3) NULL,
	[AIAC27] [nchar](3) NULL,
	[AIAC28] [nchar](3) NULL,
	[AIAC29] [nchar](3) NULL,
	[AIAC30] [nchar](3) NULL,
	[AIPRSN] [nchar](8) NULL,
	[AIOPBO] [nchar](30) NULL,
	[AITIER1] [nchar](5) NULL,
	[AIPWPCP] [float] NULL,
	[AICUSTS] [nchar](1) NULL,
	[AISTOF] [nchar](1) NULL,
	[AITERRID] [float] NULL,
	[AICIG] [float] NULL,
	[AITORG] [nchar](10) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F03012_PK] PRIMARY KEY CLUSTERED 
(
	[AIAN8] ASC,
	[AICO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F40073]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F40073]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F40073](
	[HYPRFR] [nchar](2) NOT NULL,
	[HYHYID] [nchar](10) NOT NULL,
	[HYHY01] [float] NULL,
	[HYHY02] [float] NULL,
	[HYHY03] [float] NULL,
	[HYHY04] [float] NULL,
	[HYHY05] [float] NULL,
	[HYHY06] [float] NULL,
	[HYHY07] [float] NULL,
	[HYHY08] [float] NULL,
	[HYHY09] [float] NULL,
	[HYHY10] [float] NULL,
	[HYHY11] [float] NULL,
	[HYHY12] [float] NULL,
	[HYHY13] [float] NULL,
	[HYHY14] [float] NULL,
	[HYHY15] [float] NULL,
	[HYHY16] [float] NULL,
	[HYHY17] [float] NULL,
	[HYHY18] [float] NULL,
	[HYHY19] [float] NULL,
	[HYHY20] [float] NULL,
	[HYHY21] [float] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F40073_PK] PRIMARY KEY CLUSTERED 
(
	[HYPRFR] ASC,
	[HYHYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4015]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4015]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4015](
	[OTORTP] [nchar](8) NOT NULL,
	[OTAN8] [numeric](8, 0) NOT NULL,
	[OTOSEQ] [numeric](4, 0) NOT NULL,
	[OTITM] [float] NULL,
	[OTLITM] [nchar](25) NULL,
	[OTQTYU] [float] NULL,
	[OTUOM] [nchar](2) NULL,
	[OTLNTY] [nchar](2) NULL,
	[OTEFTJ] [numeric](18, 0) NULL,
	[OTEXDJ] [numeric](18, 0) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4015_PK] PRIMARY KEY CLUSTERED 
(
	[OTORTP] ASC,
	[OTAN8] ASC,
	[OTOSEQ] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4070]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4070]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4070](
	[SNASN] [nchar](8) NOT NULL,
	[SNOSEQ] [numeric](4, 0) NOT NULL,
	[SNANPS] [numeric](8, 0) NOT NULL,
	[SNAST] [nchar](8) NULL,
	[SNEFTJ] [numeric](18, 0) NULL,
	[SNEXDJ] [numeric](18, 0) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4070_PK] PRIMARY KEY CLUSTERED 
(
	[SNASN] ASC,
	[SNOSEQ] ASC,
	[SNANPS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4071]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4071]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4071](
	[ATAST] [nchar](8) NOT NULL,
	[ATPRGR] [nchar](8) NULL,
	[ATCPGP] [nchar](8) NULL,
	[ATSDGR] [nchar](8) NULL,
	[ATPRFR] [nchar](2) NULL,
	[ATLBT] [nchar](1) NULL,
	[ATGLC] [nchar](4) NULL,
	[ATSBIF] [nchar](1) NULL,
	[ATACNT] [nchar](1) NULL,
	[ATLNTY] [nchar](2) NULL,
	[ATMDED] [nchar](1) NULL,
	[ATABAS] [nchar](1) NULL,
	[ATOLVL] [nchar](1) NULL,
	[ATTXB] [nchar](1) NULL,
	[ATPA01] [nchar](1) NULL,
	[ATPA02] [nchar](1) NULL,
	[ATPA03] [nchar](1) NULL,
	[ATPA04] [nchar](1) NULL,
	[ATPA05] [nchar](1) NULL,
	[ATENBM] [nchar](1) NULL,
	[ATSRFLAG] [nchar](1) NULL,
	[ATUSADJ] [nchar](1) NULL,
	[ATATIER] [float] NULL,
	[ATBTIER] [float] NULL,
	[ATBNAD] [float] NULL,
	[ATAPRP1] [nchar](3) NULL,
	[ATAPRP2] [nchar](3) NULL,
	[ATAPRP3] [nchar](3) NULL,
	[ATAPRP4] [nchar](6) NULL,
	[ATAPRP5] [nchar](6) NULL,
	[ATAPRP6] [nchar](6) NULL,
	[ATADJGRP] [nchar](10) NULL,
	[ATMEADJ] [nchar](1) NULL,
	[ATPDCL] [nchar](1) NULL,
	[ATUSER] [nchar](10) NULL,
	[ATPID] [nchar](10) NULL,
	[ATJOBN] [nchar](10) NULL,
	[ATUPMJ] [numeric](18, 0) NULL,
	[ATTDAY] [float] NULL,
	[ATDIDP] [nchar](12) NULL,
	[ATPMTN] [nchar](12) NULL,
	[ATPHST] [nchar](1) NULL,
	[ATPA06] [nchar](1) NULL,
	[ATPA07] [nchar](1) NULL,
	[ATPA08] [nchar](1) NULL,
	[ATPA09] [nchar](1) NULL,
	[ATPA10] [nchar](1) NULL,
	[ATEFCN] [nchar](1) NULL,
	[ATAPTYPE] [nchar](2) NULL,
	[ATMOADJ] [nchar](1) NULL,
	[ATPLGRP] [nchar](3) NULL,
	[ATEXCPL] [nchar](1) NULL,
	[ATUPMX] [numeric](18, 0) NULL,
	[ATMNMXAJ] [nchar](1) NULL,
	[ATMNMXRL] [nchar](1) NULL,
	[ATTSTRSNM] [nchar](30) NULL,
	[ATADJQTY] [nchar](1) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4071_PK] PRIMARY KEY CLUSTERED 
(
	[ATAST] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4072]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4072]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4072](
	[ADAST] [nchar](8) NOT NULL,
	[ADITM] [numeric](8, 0) NOT NULL,
	[ADLITM] [nchar](25) NULL,
	[ADAITM] [nchar](25) NULL,
	[ADAN8] [numeric](8, 0) NOT NULL,
	[ADIGID] [numeric](8, 0) NOT NULL,
	[ADCGID] [numeric](8, 0) NOT NULL,
	[ADOGID] [numeric](8, 0) NOT NULL,
	[ADCRCD] [nchar](3) NOT NULL,
	[ADUOM] [nchar](2) NOT NULL,
	[ADMNQ] [numeric](15, 0) NOT NULL,
	[ADEFTJ] [numeric](18, 0) NULL,
	[ADEXDJ] [numeric](18, 0) NOT NULL,
	[ADBSCD] [nchar](1) NULL,
	[ADLEDG] [nchar](2) NULL,
	[ADFRMN] [nchar](10) NULL,
	[ADFVTR] [float] NULL,
	[ADFGY] [nchar](1) NULL,
	[ADATID] [float] NULL,
	[ADNBRORD] [float] NULL,
	[ADUOMVID] [nchar](2) NULL,
	[ADFVUM] [nchar](2) NULL,
	[ADPARTFG] [nchar](1) NULL,
	[ADAPRS] [nchar](1) NULL,
	[ADUPMJ] [numeric](18, 0) NOT NULL,
	[ADTDAY] [numeric](6, 0) NOT NULL,
	[ADBKTPID] [float] NULL,
	[ADCRCDVID] [nchar](3) NULL,
	[ADRULENAME] [nchar](10) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4072_PK] PRIMARY KEY CLUSTERED 
(
	[ADITM] ASC,
	[ADAST] ASC,
	[ADAN8] ASC,
	[ADIGID] ASC,
	[ADCGID] ASC,
	[ADOGID] ASC,
	[ADCRCD] ASC,
	[ADUOM] ASC,
	[ADMNQ] ASC,
	[ADEXDJ] ASC,
	[ADUPMJ] ASC,
	[ADTDAY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4075]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4075]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4075](
	[VBVBT] [nchar](10) NOT NULL,
	[VBCRCD] [nchar](3) NULL,
	[VBUOM] [nchar](2) NULL,
	[VBUPRC] [float] NULL,
	[VBEFTJ] [numeric](18, 0) NOT NULL,
	[VBEXDJ] [numeric](18, 0) NULL,
	[VBAPRS] [nchar](1) NULL,
	[VBUPMJ] [numeric](18, 0) NOT NULL,
	[VBTDAY] [numeric](6, 0) NOT NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4075_PK] PRIMARY KEY CLUSTERED 
(
	[VBVBT] ASC,
	[VBEFTJ] ASC,
	[VBUPMJ] ASC,
	[VBTDAY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4076]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4076]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4076](
	[FMFRMN] [nchar](10) NOT NULL,
	[FMFML] [nchar](160) NULL,
	[FMAPRS] [nchar](1) NULL,
	[FMUPMJ] [numeric](18, 0) NOT NULL,
	[FMTDAY] [numeric](6, 0) NOT NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4076_PK] PRIMARY KEY CLUSTERED 
(
	[FMFRMN] ASC,
	[FMUPMJ] ASC,
	[FMTDAY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4092]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4092]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4092](
	[GPGPTY] [nchar](1) NOT NULL,
	[GPGPC] [nchar](8) NOT NULL,
	[GPDL01] [nchar](30) NULL,
	[GPGPK1] [nchar](10) NULL,
	[GPGPK2] [nchar](10) NULL,
	[GPGPK3] [nchar](10) NULL,
	[GPGPK4] [nchar](10) NULL,
	[GPGPK5] [nchar](10) NULL,
	[GPGPK6] [nchar](10) NULL,
	[GPGPK7] [nchar](10) NULL,
	[GPGPK8] [nchar](10) NULL,
	[GPGPK9] [nchar](10) NULL,
	[GPGPK10] [nchar](10) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4092_PK] PRIMARY KEY CLUSTERED 
(
	[GPGPTY] ASC,
	[GPGPC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F40941]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F40941]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F40941](
	[IKPRGR] [nchar](8) NOT NULL,
	[IKIGP1] [nchar](6) NOT NULL,
	[IKIGP2] [nchar](6) NOT NULL,
	[IKIGP3] [nchar](6) NOT NULL,
	[IKIGP4] [nchar](6) NOT NULL,
	[IKIGP5] [nchar](6) NOT NULL,
	[IKIGP6] [nchar](6) NOT NULL,
	[IKIGP7] [nchar](6) NOT NULL,
	[IKIGP8] [nchar](6) NOT NULL,
	[IKIGP9] [nchar](6) NOT NULL,
	[IKIGP10] [nchar](6) NOT NULL,
	[IKIGID] [float] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_F40941] PRIMARY KEY CLUSTERED 
(
	[IKPRGR] ASC,
	[IKIGP1] ASC,
	[IKIGP2] ASC,
	[IKIGP3] ASC,
	[IKIGP4] ASC,
	[IKIGP5] ASC,
	[IKIGP6] ASC,
	[IKIGP7] ASC,
	[IKIGP8] ASC,
	[IKIGP9] ASC,
	[IKIGP10] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F40942]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F40942]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F40942](
	[CKCPGP] [nchar](8) NOT NULL,
	[CKCGP1] [nchar](3) NOT NULL,
	[CKCGP2] [nchar](3) NOT NULL,
	[CKCGP3] [nchar](3) NOT NULL,
	[CKCGP4] [nchar](3) NOT NULL,
	[CKCGP5] [nchar](3) NOT NULL,
	[CKCGP6] [nchar](3) NOT NULL,
	[CKCGP7] [nchar](3) NOT NULL,
	[CKCGP8] [nchar](3) NOT NULL,
	[CKCGP9] [nchar](3) NOT NULL,
	[CKCGP10] [nchar](3) NOT NULL,
	[CKCGID] [float] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F40942_PK] PRIMARY KEY CLUSTERED 
(
	[CKCPGP] ASC,
	[CKCGP1] ASC,
	[CKCGP2] ASC,
	[CKCGP3] ASC,
	[CKCGP4] ASC,
	[CKCGP5] ASC,
	[CKCGP6] ASC,
	[CKCGP7] ASC,
	[CKCGP8] ASC,
	[CKCGP9] ASC,
	[CKCGP10] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F41002]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F41002]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F41002](
	[UMMCU] [nchar](12) NOT NULL,
	[UMITM] [numeric](8, 0) NOT NULL,
	[UMUM] [nchar](2) NOT NULL,
	[UMRUM] [nchar](2) NOT NULL,
	[UMUSTR] [nchar](1) NULL,
	[UMCONV] [float] NULL,
	[UMCNV1] [float] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F41002_PK] PRIMARY KEY CLUSTERED 
(
	[UMMCU] ASC,
	[UMITM] ASC,
	[UMUM] ASC,
	[UMRUM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4101]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4101]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4101](
	[IMITM] [numeric](8, 0) NOT NULL,
	[IMLITM] [nchar](25) NULL,
	[IMAITM] [nchar](25) NULL,
	[IMDSC1] [nchar](30) NULL,
	[IMDSC2] [nchar](30) NULL,
	[IMSRP1] [nchar](3) NULL,
	[IMSRP2] [nchar](3) NULL,
	[IMSRP3] [nchar](3) NULL,
	[IMSRP4] [nchar](3) NULL,
	[IMSRP5] [nchar](3) NULL,
	[IMSRP6] [nchar](6) NULL,
	[IMSRP7] [nchar](6) NULL,
	[IMSRP8] [nchar](6) NULL,
	[IMSRP9] [nchar](6) NULL,
	[IMSRP0] [nchar](6) NULL,
	[IMPRP1] [nchar](3) NULL,
	[IMPRP2] [nchar](3) NULL,
	[IMPRP3] [nchar](3) NULL,
	[IMPRP4] [nchar](3) NULL,
	[IMPRP5] [nchar](3) NULL,
	[IMPRP6] [nchar](6) NULL,
	[IMPRP7] [nchar](6) NULL,
	[IMPRP8] [nchar](6) NULL,
	[IMPRP9] [nchar](6) NULL,
	[IMPRP0] [nchar](6) NULL,
	[IMCDCD] [nchar](15) NULL,
	[IMPDGR] [nchar](3) NULL,
	[IMDSGP] [nchar](3) NULL,
	[IMPRGR] [nchar](8) NULL,
	[IMRPRC] [nchar](8) NULL,
	[IMORPR] [nchar](8) NULL,
	[IMVCUD] [float] NULL,
	[IMUOM1] [nchar](2) NULL,
	[IMUOM2] [nchar](2) NULL,
	[IMUOM4] [nchar](2) NULL,
	[IMUOM6] [nchar](2) NULL,
	[IMUWUM] [nchar](2) NULL,
	[IMUVM1] [nchar](2) NULL,
	[IMCYCL] [nchar](3) NULL,
	[IMGLPT] [nchar](4) NULL,
	[IMPLEV] [nchar](1) NULL,
	[IMPPLV] [nchar](1) NULL,
	[IMCLEV] [nchar](1) NULL,
	[IMCKAV] [nchar](1) NULL,
	[IMSRCE] [nchar](1) NULL,
	[IMSTKT] [nchar](1) NULL,
	[IMLNTY] [nchar](2) NULL,
	[IMBACK] [nchar](1) NULL,
	[IMIFLA] [nchar](2) NULL,
	[IMTFLA] [nchar](2) NULL,
	[IMINMG] [nchar](10) NULL,
	[IMABCS] [nchar](1) NULL,
	[IMABCM] [nchar](1) NULL,
	[IMABCI] [nchar](1) NULL,
	[IMOVR] [nchar](1) NULL,
	[IMCMCG] [nchar](8) NULL,
	[IMSRNR] [nchar](1) NULL,
	[IMFIFO] [nchar](1) NULL,
	[IMLOTS] [nchar](1) NULL,
	[IMSLD] [float] NULL,
	[IMPCTM] [float] NULL,
	[IMMMPC] [float] NULL,
	[IMCMGL] [nchar](1) NULL,
	[IMUPCN] [nchar](13) NULL,
	[IMUMUP] [nchar](2) NULL,
	[IMUMDF] [nchar](2) NULL,
	[IMBBDD] [float] NULL,
	[IMCMDM] [nchar](1) NULL,
	[IMLECM] [nchar](1) NULL,
	[IMLEDD] [float] NULL,
	[IMPEFD] [float] NULL,
	[IMSBDD] [float] NULL,
	[IMU1DD] [float] NULL,
	[IMU2DD] [float] NULL,
	[IMU3DD] [float] NULL,
	[IMU4DD] [float] NULL,
	[IMU5DD] [float] NULL,
	[IMLNPA] [nchar](1) NULL,
	[IMLOTC] [nchar](3) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4101_PK] PRIMARY KEY CLUSTERED 
(
	[IMITM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4102]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4102]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4102](
	[IBITM] [numeric](8, 0) NOT NULL,
	[IBLITM] [nchar](25) NULL,
	[IBAITM] [nchar](25) NULL,
	[IBMCU] [nchar](12) NOT NULL,
	[IBSRP1] [nchar](3) NULL,
	[IBSRP2] [nchar](3) NULL,
	[IBSRP3] [nchar](3) NULL,
	[IBSRP4] [nchar](3) NULL,
	[IBSRP5] [nchar](3) NULL,
	[IBSRP6] [nchar](6) NULL,
	[IBSRP7] [nchar](6) NULL,
	[IBSRP8] [nchar](6) NULL,
	[IBSRP9] [nchar](6) NULL,
	[IBSRP0] [nchar](6) NULL,
	[IBPRP1] [nchar](3) NULL,
	[IBPRP2] [nchar](3) NULL,
	[IBPRP3] [nchar](3) NULL,
	[IBPRP4] [nchar](3) NULL,
	[IBPRP5] [nchar](3) NULL,
	[IBPRP6] [nchar](6) NULL,
	[IBPRP7] [nchar](6) NULL,
	[IBPRP8] [nchar](6) NULL,
	[IBPRP9] [nchar](6) NULL,
	[IBPRP0] [nchar](6) NULL,
	[IBCDCD] [nchar](15) NULL,
	[IBPDGR] [nchar](3) NULL,
	[IBDSGP] [nchar](3) NULL,
	[IBGLPT] [nchar](4) NULL,
	[IBORIG] [nchar](3) NULL,
	[IBSAFE] [float] NULL,
	[IBSLD] [float] NULL,
	[IBCKAV] [nchar](1) NULL,
	[IBSRCE] [nchar](1) NULL,
	[IBLOTS] [nchar](1) NULL,
	[IBMMPC] [float] NULL,
	[IBPRGR] [nchar](8) NULL,
	[IBRPRC] [nchar](8) NULL,
	[IBORPR] [nchar](8) NULL,
	[IBBACK] [nchar](1) NULL,
	[IBIFLA] [nchar](2) NULL,
	[IBABCS] [nchar](1) NULL,
	[IBABCM] [nchar](1) NULL,
	[IBABCI] [nchar](1) NULL,
	[IBOVR] [nchar](1) NULL,
	[IBSTKT] [nchar](1) NULL,
	[IBLNTY] [nchar](2) NULL,
	[IBFIFO] [nchar](1) NULL,
	[IBCYCL] [nchar](3) NULL,
	[IBINMG] [nchar](10) NULL,
	[IBSRNR] [nchar](1) NULL,
	[IBPCTM] [float] NULL,
	[IBCMCG] [nchar](8) NULL,
	[IBTAX1] [nchar](1) NULL,
	[IBBBDD] [float] NULL,
	[IBCMDM] [nchar](1) NULL,
	[IBLECM] [nchar](1) NULL,
	[IBLEDD] [float] NULL,
	[IBMLOT] [nchar](1) NULL,
	[IBSBDD] [float] NULL,
	[IBU1DD] [float] NULL,
	[IBU2DD] [float] NULL,
	[IBU3DD] [float] NULL,
	[IBU4DD] [float] NULL,
	[IBU5DD] [float] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4102_PK] PRIMARY KEY CLUSTERED 
(
	[IBMCU] ASC,
	[IBITM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F4106]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F4106]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F4106](
	[BPITM] [numeric](8, 0) NOT NULL,
	[BPLITM] [nchar](25) NULL,
	[BPMCU] [nchar](12) NOT NULL,
	[BPLOCN] [nchar](20) NOT NULL,
	[BPLOTN] [nchar](30) NOT NULL,
	[BPAN8] [numeric](8, 0) NOT NULL,
	[BPIGID] [numeric](8, 0) NOT NULL,
	[BPCGID] [numeric](8, 0) NOT NULL,
	[BPLOTG] [nchar](3) NOT NULL,
	[BPFRMP] [numeric](7, 0) NOT NULL,
	[BPCRCD] [nchar](3) NOT NULL,
	[BPUOM] [nchar](2) NOT NULL,
	[BPEFTJ] [numeric](18, 0) NULL,
	[BPEXDJ] [numeric](18, 0) NOT NULL,
	[BPUPRC] [float] NULL,
	[BPUPMJ] [numeric](18, 0) NOT NULL,
	[BPTDAY] [float] NOT NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F4106_PK] PRIMARY KEY CLUSTERED 
(
	[BPITM] ASC,
	[BPMCU] ASC,
	[BPLOCN] ASC,
	[BPLOTN] ASC,
	[BPAN8] ASC,
	[BPIGID] ASC,
	[BPCGID] ASC,
	[BPLOTG] ASC,
	[BPFRMP] ASC,
	[BPCRCD] ASC,
	[BPUOM] ASC,
	[BPEXDJ] ASC,
	[BPUPMJ] ASC,
	[BPTDAY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F42019]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F42019]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F42019](
	[SHKCOO] [nchar](5) NOT NULL,
	[SHDOCO] [numeric](8, 0) NOT NULL,
	[SHDCTO] [nchar](2) NOT NULL,
	[SHMCU] [nchar](12) NULL,
	[SHRKCO] [nchar](5) NULL,
	[SHRORN] [nchar](8) NULL,
	[SHRCTO] [nchar](2) NULL,
	[SHAN8] [float] NULL,
	[SHSHAN] [float] NULL,
	[SHPA8] [float] NULL,
	[SHDRQJ] [numeric](18, 0) NULL,
	[SHTRDJ] [numeric](18, 0) NULL,
	[SHPDDJ] [numeric](18, 0) NULL,
	[SHADDJ] [numeric](18, 0) NULL,
	[SHCNDJ] [numeric](18, 0) NULL,
	[SHPEFJ] [numeric](18, 0) NULL,
	[SHVR01] [nchar](25) NULL,
	[SHVR02] [nchar](25) NULL,
	[SHDEL1] [nchar](30) NULL,
	[SHDEL2] [nchar](30) NULL,
	[SHINMG] [nchar](10) NULL,
	[SHPTC] [nchar](3) NULL,
	[SHRYIN] [nchar](1) NULL,
	[SHASN] [nchar](8) NULL,
	[SHPRGP] [nchar](8) NULL,
	[SHTXA1] [nchar](10) NULL,
	[SHEXR1] [nchar](2) NULL,
	[SHTXCT] [nchar](20) NULL,
	[SHATXT] [nchar](1) NULL,
	[SHHOLD] [nchar](2) NULL,
	[SHROUT] [nchar](3) NULL,
	[SHSTOP] [nchar](3) NULL,
	[SHZON] [nchar](3) NULL,
	[SHFRTH] [nchar](3) NULL,
	[SHRCD] [nchar](3) NULL,
	[SHFUF2] [nchar](1) NULL,
	[SHOTOT] [float] NULL,
	[SHAUTN] [nchar](10) NULL,
	[SHCACT] [nchar](25) NULL,
	[SHCEXP] [numeric](18, 0) NULL,
	[SHORBY] [nchar](10) NULL,
	[SHTKBY] [nchar](10) NULL,
	[SHDOC1] [float] NULL,
	[SHDCT4] [nchar](2) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F42019_PK] PRIMARY KEY CLUSTERED 
(
	[SHDOCO] ASC,
	[SHDCTO] ASC,
	[SHKCOO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F42119]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F42119]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F42119](
	[SDKCOO] [nchar](5) NOT NULL,
	[SDDOCO] [numeric](8, 0) NOT NULL,
	[SDDCTO] [nchar](2) NOT NULL,
	[SDLNID] [numeric](7, 0) NOT NULL,
	[SDMCU] [nchar](12) NULL,
	[SDRKCO] [nchar](5) NULL,
	[SDRORN] [nchar](8) NULL,
	[SDRCTO] [nchar](2) NULL,
	[SDRLLN] [float] NULL,
	[SDAN8] [float] NULL,
	[SDSHAN] [float] NULL,
	[SDPA8] [float] NULL,
	[SDDRQJ] [numeric](18, 0) NULL,
	[SDTRDJ] [numeric](18, 0) NULL,
	[SDPDDJ] [numeric](18, 0) NULL,
	[SDADDJ] [numeric](18, 0) NULL,
	[SDIVD] [numeric](18, 0) NULL,
	[SDCNDJ] [numeric](18, 0) NULL,
	[SDDGL] [numeric](18, 0) NULL,
	[SDPEFJ] [numeric](18, 0) NULL,
	[SDVR01] [nchar](25) NULL,
	[SDVR02] [nchar](25) NULL,
	[SDITM] [float] NULL,
	[SDLITM] [nchar](25) NULL,
	[SDAITM] [nchar](25) NULL,
	[SDLOCN] [nchar](20) NULL,
	[SDLOTN] [nchar](30) NULL,
	[SDDSC1] [nchar](30) NULL,
	[SDDSC2] [nchar](30) NULL,
	[SDLNTY] [nchar](2) NULL,
	[SDNXTR] [nchar](3) NULL,
	[SDLTTR] [nchar](3) NULL,
	[SDEMCU] [nchar](12) NULL,
	[SDSRP1] [nchar](3) NULL,
	[SDSRP2] [nchar](3) NULL,
	[SDSRP3] [nchar](3) NULL,
	[SDSRP4] [nchar](3) NULL,
	[SDSRP5] [nchar](3) NULL,
	[SDPRP1] [nchar](3) NULL,
	[SDPRP2] [nchar](3) NULL,
	[SDPRP3] [nchar](3) NULL,
	[SDPRP4] [nchar](3) NULL,
	[SDPRP5] [nchar](3) NULL,
	[SDUOM] [nchar](2) NULL,
	[SDUORG] [float] NULL,
	[SDSOQS] [float] NULL,
	[SDSOBK] [float] NULL,
	[SDSOCN] [float] NULL,
	[SDUPRC] [float] NULL,
	[SDAEXP] [float] NULL,
	[SDPROV] [nchar](1) NULL,
	[SDINMG] [nchar](10) NULL,
	[SDPTC] [nchar](3) NULL,
	[SDASN] [nchar](8) NULL,
	[SDPRGR] [nchar](8) NULL,
	[SDCLVL] [nchar](3) NULL,
	[SDKCO] [nchar](5) NULL,
	[SDDOC] [float] NULL,
	[SDDCT] [nchar](2) NULL,
	[SDTAX1] [nchar](1) NULL,
	[SDTXA1] [nchar](10) NULL,
	[SDEXR1] [nchar](2) NULL,
	[SDATXT] [nchar](1) NULL,
	[SDROUT] [nchar](3) NULL,
	[SDSTOP] [nchar](3) NULL,
	[SDZON] [nchar](3) NULL,
	[SDFRTH] [nchar](3) NULL,
	[SDUOM1] [nchar](2) NULL,
	[SDPQOR] [float] NULL,
	[SDUOM2] [nchar](2) NULL,
	[SDSQOR] [float] NULL,
	[SDUOM4] [nchar](2) NULL,
	[SDRPRC] [nchar](8) NULL,
	[SDORPR] [nchar](8) NULL,
	[SDORP] [nchar](1) NULL,
	[SDGLC] [nchar](4) NULL,
	[SDCTRY] [float] NULL,
	[SDFY] [float] NULL,
	[SDACOM] [nchar](1) NULL,
	[SDCMCG] [nchar](8) NULL,
	[SDRCD] [nchar](3) NULL,
	[SDUPC1] [nchar](2) NULL,
	[SDUPC2] [nchar](2) NULL,
	[SDUPC3] [nchar](2) NULL,
	[SDTORG] [nchar](10) NULL,
	[SDVR03] [nchar](25) NULL,
	[SDNUMB] [float] NULL,
	[SDAAID] [float] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F42119_PK] PRIMARY KEY CLUSTERED 
(
	[SDDOCO] ASC,
	[SDDCTO] ASC,
	[SDKCOO] ASC,
	[SDLNID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F56M0000]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F56M0000]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F56M0000](
	[GFSY] [nchar](4) NOT NULL,
	[GFCXPJ] [numeric](18, 0) NULL,
	[GFDTEN] [numeric](18, 0) NULL,
	[GFLAVJ] [numeric](18, 0) NULL,
	[GFOBJ] [nchar](6) NULL,
	[GFMCU] [nchar](12) NULL,
	[GFSUB] [nchar](8) NULL,
	[GFPST] [nchar](1) NULL,
	[GFEV01] [nchar](1) NULL,
	[GFEV02] [nchar](1) NULL,
	[GFEV03] [nchar](1) NULL,
	[GFMATH01] [float] NULL,
	[GFMATH02] [float] NULL,
	[GFMATH03] [float] NULL,
	[GFCFSTR1] [nchar](3) NULL,
	[GFCFSTR2] [nchar](8) NULL,
	[GFGS1A] [nchar](10) NULL,
	[GFGS1B] [nchar](10) NULL,
	[GFGS2A] [nchar](20) NULL,
	[GFGS2B] [nchar](20) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F56M0000_PK] PRIMARY KEY CLUSTERED 
(
	[GFSY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F56M0001]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F56M0001]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F56M0001](
	[FFUSER] [nchar](10) NOT NULL,
	[FFROUT] [nchar](3) NOT NULL,
	[FFMCU] [nchar](12) NOT NULL,
	[FFHMCU] [nchar](12) NULL,
	[FFBUVAL] [nchar](12) NULL,
	[FFAN8] [float] NULL,
	[FFPA8] [float] NULL,
	[FFSTOP] [nchar](3) NULL,
	[FFZON] [nchar](3) NULL,
	[FFLOCN] [nchar](20) NULL,
	[FFLOCF] [nchar](20) NULL,
	[FFEV01] [nchar](1) NULL,
	[FFEV02] [nchar](1) NULL,
	[FFEV03] [nchar](1) NULL,
	[FFMATH01] [float] NULL,
	[FFMATH02] [float] NULL,
	[FFMATH03] [float] NULL,
	[FFCXPJ] [numeric](18, 0) NULL,
	[FFCLRJ] [numeric](18, 0) NULL,
	[FFDTE] [numeric](18, 0) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F56M0001_PK] PRIMARY KEY CLUSTERED 
(
	[FFUSER] ASC,
	[FFROUT] ASC,
	[FFMCU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F90CA003]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F90CA003]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F90CA003](
	[SMAN8] [float] NOT NULL,
	[SMSLSM] [float] NOT NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F90CA003_PK] PRIMARY KEY CLUSTERED 
(
	[SMAN8] ASC,
	[SMSLSM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F90CA042]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F90CA042]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F90CA042](
	[EMAN8] [numeric](8, 0) NOT NULL,
	[EMPA8] [numeric](8, 0) NOT NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F90CA042_PK] PRIMARY KEY CLUSTERED 
(
	[EMAN8] ASC,
	[EMPA8] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[F90CA086]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[F90CA086]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[F90CA086](
	[CRCUAN8] [numeric](8, 0) NOT NULL,
	[CRCRAN8] [numeric](8, 0) NOT NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [F90CA086_PK] PRIMARY KEY CLUSTERED 
(
	[CRCUAN8] ASC,
	[CRCRAN8] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[M0111]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M0111]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[M0111](
	[CDAN8] [numeric](8, 0) NOT NULL,
	[CDIDLN] [numeric](5, 0) NOT NULL,
	[CDRCK7] [numeric](5, 0) NOT NULL,
	[CDCNLN] [numeric](5, 0) NOT NULL,
	[CDAR1] [nchar](6) NULL,
	[CDPH1] [nchar](20) NULL,
	[CDEXTN1] [nchar](8) NULL,
	[CDPHTP1] [numeric](8, 0) NULL,
	[CDDFLTPH1] [bit] NULL,
	[CDREFPH1] [int] NULL,
	[CDAR2] [nchar](6) NULL,
	[CDPH2] [nchar](20) NULL,
	[CDEXTN2] [nchar](8) NULL,
	[CDPHTP2] [numeric](8, 0) NULL,
	[CDDFLTPH2] [bit] NULL,
	[CDREFPH2] [int] NULL,
	[CDAR3] [nchar](6) NULL,
	[CDPH3] [nchar](20) NULL,
	[CDEXTN3] [nchar](8) NULL,
	[CDPHTP3] [numeric](8, 0) NULL,
	[CDDFLTPH3] [bit] NULL,
	[CDREFPH3] [int] NULL,
	[CDAR4] [nchar](6) NULL,
	[CDPH4] [nchar](20) NULL,
	[CDEXTN4] [nchar](8) NULL,
	[CDPHTP4] [numeric](8, 0) NULL,
	[CDDFLTPH4] [bit] NULL,
	[CDREFPH4] [int] NULL,
	[CDEMAL1] [nvarchar](256) NULL,
	[CDETP1] [numeric](3, 0) NULL,
	[CDDFLTEM1] [bit] NULL,
	[CDREFEM1] [int] NULL,
	[CDEMAL2] [nvarchar](256) NULL,
	[CDETP2] [numeric](3, 0) NULL,
	[CDDFLTEM2] [bit] NULL,
	[CDREFEM2] [int] NULL,
	[CDEMAL3] [nvarchar](256) NULL,
	[CDETP3] [numeric](3, 0) NULL,
	[CDDFLTEM3] [bit] NULL,
	[CDREFEM3] [int] NULL,
	[CDGNNM] [nchar](25) NULL,
	[CDMDNM] [nchar](25) NULL,
	[CDSRNM] [nchar](25) NULL,
	[CDTITL] [nchar](10) NULL,
	[CDID] [numeric](8, 0) IDENTITY(1,1) NOT NULL,
	[CDACTV] [bit] NULL,
	[CDDFLT] [bit] NULL,
	[CDSET] [int] NULL,
	[CDCRBY] [nchar](25) NULL,
	[CDCRDT] [date] NULL,
	[CDUPBY] [nchar](25) NULL,
	[CDUPDT] [date] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [M0111_PK] PRIMARY KEY CLUSTERED 
(
	[CDAN8] ASC,
	[CDIDLN] ASC,
	[CDRCK7] ASC,
	[CDCNLN] ASC,
	[CDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[M03011]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M03011]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[M03011](
	[CSAN8] [numeric](8, 0) NOT NULL,
	[CSCO] [nchar](5) NOT NULL,
	[CSUAMT] [numeric](8, 4) NULL,
	[CSOBAL] [numeric](8, 4) NULL,
	[CSCRBY] [nchar](10) NULL,
	[CSCRDT] [date] NULL,
	[CSUPBY] [nchar](10) NULL,
	[CSUPDT] [date] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_M03011_1] PRIMARY KEY CLUSTERED 
(
	[CSAN8] ASC,
	[CSCO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[M03042]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M03042]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[M03042](
	[PDAN8] [numeric](8, 0) NOT NULL,
	[PDCO] [nchar](5) NOT NULL,
	[PDID] [numeric](8, 0) NOT NULL,
	[PDPAMT] [numeric](8, 4) NULL,
	[PDPMODE] [bit] NULL,
	[PDCHQNO] [nchar](10) NULL,
	[PDCHQDT] [date] NULL,
	[PDCRBY] [nchar](10) NULL,
	[PDCRDT] [date] NULL,
	[PDUPBY] [nchar](10) NULL,
	[PDUPDT] [date] NULL,
	[PDRCID] [int] NULL,
	[PDTRMD] [bit] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_M03011] PRIMARY KEY CLUSTERED 
(
	[PDAN8] ASC,
	[PDCO] ASC,
	[PDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[M080111]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M080111]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[M080111](
	[CTID] [numeric](8, 0) NOT NULL,
	[CTTYP] [nchar](10) NULL,
	[CTCD] [nchar](5) NOT NULL,
	[CTDSC1] [nchar](50) NULL,
	[CTTXA1] [nchar](10) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [M080111_PK] PRIMARY KEY CLUSTERED 
(
	[CTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[M4016]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[M4016]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[M4016](
	[OPORTP] [nchar](8) NOT NULL,
	[OPAN8] [numeric](8, 0) NOT NULL,
	[OPOSEQ] [numeric](4, 0) NULL,
	[OPITM] [nchar](25) NOT NULL,
	[OPLITM] [nchar](25) NULL,
	[OPQTYU] [int] NULL,
	[OPUOM] [nchar](2) NULL,
	[OPLNTY] [nchar](2) NULL,
	[SRP1] [nchar](3) NULL,
	[SRP5] [nchar](3) NULL,
	[CSCRBY] [nchar](10) NULL,
	[CSCRDT] [date] NULL,
	[CSUPBY] [nchar](10) NULL,
	[CSUPDT] [date] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_M4016_1] PRIMARY KEY CLUSTERED 
(
	[OPORTP] ASC,
	[OPAN8] ASC,
	[OPITM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[Order_Detail]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Order_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[Order_Detail](
	[Order_Detail_Id] [int] NOT NULL,
	[Order_ID] [int] NOT NULL,
	[Item_Number] [nchar](25) NULL,
	[Order_Qty] [int] NULL,
	[Order_UOM] [nchar](2) NULL,
	[Unit_Price] [float] NULL,
	[Extn_Price] [float] NULL,
	[Reason_Code] [varchar](5) NULL,
	[last_modified] [datetime] NOT NULL,
	[IsTaxable] [bit] NULL,
 CONSTRAINT [PK__Order_De__1581C763EA9656A9] PRIMARY KEY CLUSTERED 
(
	[Order_Detail_Id] ASC,
	[Order_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BUSDTA].[Order_Header]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Order_Header]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[Order_Header](
	[Order_ID] [int] NOT NULL,
	[Customer_Id] [float] NULL,
	[Order_Date] [date] NULL,
	[Created_By] [int] NULL,
	[Created_On] [date] NOT NULL,
	[Is_Deleted] [bit] NULL,
	[Total_Coffee] [float] NULL,
	[Total_Allied] [float] NULL,
	[Energy_Surcharge] [float] NULL,
	[Order_Total_Amt] [float] NULL,
	[Sales_Tax_Amt] [float] NULL,
	[Invoice_Total] [float] NULL,
	[Surcharge_Reason_Code] [varchar](5) NULL,
	[last_modified] [datetime] NOT NULL,
	[payment_type] [nvarchar](10) NULL,
	[payment_id] [nvarchar](10) NULL,
	[Order_State] [nvarchar](20) NULL,
	[Order_Sub_State] [nvarchar](20) NULL,
	[updated_at] [datetime] NULL,
	[VoidReason] [bigint] NULL,
	[OrderSeries] [int] NOT NULL,
	[RouteNo] [nchar](3) NULL,
	[ChargeOnAccount] [bit] NULL,
 CONSTRAINT [PK__Order_He__F1E4639B20F5B8E4] PRIMARY KEY CLUSTERED 
(
	[Order_ID] ASC,
	[OrderSeries] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BUSDTA].[Payment_Ref_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Payment_Ref_Map]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[Payment_Ref_Map](
	[Payment_Ref_Map_Id] [int] NOT NULL,
	[Payment_Id] [int] NOT NULL,
	[Ref_Id] [int] NOT NULL,
	[Ref_Type] [nchar](3) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_Payment_Ref_Map] PRIMARY KEY CLUSTERED 
(
	[Payment_Ref_Map_Id] ASC,
	[Payment_Id] ASC,
	[Ref_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[PickOrder]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[PickOrder]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[PickOrder](
	[PickOrder_Id] [int] NOT NULL,
	[Order_ID] [int] NULL,
	[Item_Number] [nchar](25) NULL,
	[Order_Qty] [int] NULL,
	[Order_UOM] [nchar](2) NULL,
	[Picked_Qty_Primary_UOM] [int] NULL,
	[Primary_UOM] [nchar](2) NULL,
	[Order_Qty_Primary_UOM] [int] NULL,
	[On_Hand_Qty_Primary] [int] NULL,
	[Last_Scan_Mode] [bit] NULL,
	[Item_Scan_Sequence] [int] NULL,
	[Picked_By] [int] NULL,
	[IsOnHold] [int] NULL,
	[last_modified] [datetime] NOT NULL,
	[Reason_Code_Id] [int] NULL,
	[ManuallyPickCount] [int] NULL,
 CONSTRAINT [PK__PickOrde__29E5E9D847724AE3] PRIMARY KEY CLUSTERED 
(
	[PickOrder_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[PickOrder_Exception]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[PickOrder_Exception]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[PickOrder_Exception](
	[PickOrder_Exception_Id] [int] NOT NULL,
	[Order_Id] [int] NULL,
	[Item_Number] [nchar](25) NULL,
	[Exception_Qty] [int] NULL,
	[UOM] [nchar](2) NULL,
	[Exception_Reason] [varchar](25) NULL,
	[last_modified] [datetime] NOT NULL,
	[ManualPickReasonCode] [int] NULL,
	[ManuallyPickCount] [int] NULL,
 CONSTRAINT [PK__PickOrde__B740DAEFC0CE8AEC] PRIMARY KEY CLUSTERED 
(
	[PickOrder_Exception_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BUSDTA].[ReasonCodeMaster]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[ReasonCodeMaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[ReasonCodeMaster](
	[ReasonCodeId] [int] NOT NULL,
	[ReasonCode] [nvarchar](5) NULL,
	[ReasonCodeDescription] [nvarchar](100) NULL,
	[ReasonCodeType] [nvarchar](50) NULL,
	[last_modified] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ReasonCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[Route_Device_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Route_Device_Map]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[Route_Device_Map](
	[Route_Id] [varchar](8) NOT NULL,
	[Device_Id] [varchar](30) NOT NULL,
	[Active] [int] NULL,
	[Remote_Id] [varchar](30) NULL,
	[last_modified] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Route_Id] ASC,
	[Device_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BUSDTA].[Route_User_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[Route_User_Map]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[Route_User_Map](
	[App_user_id] [int] NOT NULL,
	[Route_Id] [varchar](8) NOT NULL,
	[Active] [int] NULL,
	[last_modified] [datetime] NOT NULL,
	[Default_Route] [int] NULL,
 CONSTRAINT [PK__Route_Us__02A552DDD6A45ADA] PRIMARY KEY CLUSTERED 
(
	[App_user_id] ASC,
	[Route_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BUSDTA].[UDCKEYLIST]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[UDCKEYLIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[UDCKEYLIST](
	[DTSY] [nchar](4) NOT NULL,
	[DTRT] [nchar](2) NOT NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_UDCKEYLIST] PRIMARY KEY CLUSTERED 
(
	[DTSY] ASC,
	[DTRT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [BUSDTA].[user_master]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[user_master]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[user_master](
	[App_user_id] [int] IDENTITY(1,1) NOT NULL,
	[App_User] [varchar](30) NULL,
	[Name] [varchar](30) NULL,
	[DomainUser] [varchar](20) NULL,
	[AppPassword] [varchar](20) NULL,
	[last_modified] [datetime] NOT NULL,
	[active] [bit] NULL,
	[Created_On] [datetime] NULL,
 CONSTRAINT [PK__user_mas__0479CF4C9B24123B] PRIMARY KEY CLUSTERED 
(
	[App_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BUSDTA].[User_Role_Map]    Script Date: 4/8/2015 3:37:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BUSDTA].[User_Role_Map]') AND type in (N'U'))
BEGIN
CREATE TABLE [BUSDTA].[User_Role_Map](
	[App_user_id] [int] NOT NULL,
	[Role] [varchar](8) NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK__User_Rol__0479CF4CCB74B8EE] PRIMARY KEY CLUSTERED 
(
	[App_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Device_Ma__last___69478F08]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Device_Master] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0004__last_modi__31583BA0]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0004] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0005__last_modi__3434A84B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0005] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0006__last_modi__371114F6]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0006] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0014__last_modi__39ED81A1]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0014] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0101__last_modi__4A23E96A]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0101] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0115__last_modi__4D005615]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0115] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F01151__last_mod__55959C16]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F01151] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0116__last_modi__4FDCC2C0]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0116] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F0150__last_modi__52B92F6B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F0150] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F03012__last_mod__587208C1]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F03012] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F40073__last_mod__77EAB41A]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F40073] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4015__last_modi__5B4E756C]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4015] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4070__last_modi__5E2AE217]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4070] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4071__last_modi__61074EC2]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4071] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4072__last_modi__63E3BB6D]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4072] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4075__last_modi__66C02818]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4075] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4076__last_modi__699C94C3]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4076] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4092__last_modi__6C79016E]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4092] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F40942__last_mod__7AC720C5]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F40942] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F41002__last_mod__7DA38D70]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F41002] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4101__last_modi__6F556E19]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4101] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4102__last_modi__7231DAC4]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4102] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F4106__last_modi__750E476F]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F4106] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F42019__last_mod__007FFA1B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F42019] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F42119__last_mod__035C66C6]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F42119] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F56M0000__last_m__3CC9EE4C]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F56M0000] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F56M0001__last_m__3FA65AF7]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F56M0001] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F90CA003__last_m__4282C7A2]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F90CA003] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F90CA042__last_m__3F122971]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F90CA042] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__F90CA086__last_m__47477CBF]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[F90CA086] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__M03011__last_mod__3024EB9C]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[M03011] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__M03042__last_mod__36D1E92B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[M03042] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__M4016__last_modi__3D7EE6BA]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[M4016] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Order_Det__last___4F52B2DB]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Order_Detail] ADD  CONSTRAINT [DF__Order_Det__last___4F52B2DB]  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Order_Hea__last___55FFB06A]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Order_Header] ADD  CONSTRAINT [DF__Order_Hea__last___55FFB06A]  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Payment_R__last___442BE449]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Payment_Ref_Map] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__ReasonCod__last___15B0212B]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[ReasonCodeMaster] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Route_Dev__last___51700577]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Route_Device_Map] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__Route_Use__last___4AC307E8]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[Route_User_Map] ADD  CONSTRAINT [DF__Route_Use__last___4AC307E8]  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__UDCKEYLIS__last___33AA9866]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[UDCKEYLIST] ADD  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__user_mast__last___44160A59]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[user_master] ADD  CONSTRAINT [DF__user_mast__last___44160A59]  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF_user_master_active]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[user_master] ADD  CONSTRAINT [DF_user_master_active]  DEFAULT ((1)) FOR [active]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF_user_master_Created_On]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[user_master] ADD  CONSTRAINT [DF_user_master_Created_On]  DEFAULT (getdate()) FOR [Created_On]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[BUSDTA].[DF__User_Role__last___3D690CCA]') AND type = 'D')
BEGIN
ALTER TABLE [BUSDTA].[User_Role_Map] ADD  CONSTRAINT [DF__User_Role__last___3D690CCA]  DEFAULT (getdate()) FOR [last_modified]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[BUSDTA].[FK__Route_Use__App_u__32EB7E57]') AND parent_object_id = OBJECT_ID(N'[BUSDTA].[Route_User_Map]'))
ALTER TABLE [BUSDTA].[Route_User_Map]  WITH CHECK ADD  CONSTRAINT [FK__Route_Use__App_u__32EB7E57] FOREIGN KEY([App_user_id])
REFERENCES [BUSDTA].[user_master] ([App_user_id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[BUSDTA].[FK__Route_Use__App_u__32EB7E57]') AND parent_object_id = OBJECT_ID(N'[BUSDTA].[Route_User_Map]'))
ALTER TABLE [BUSDTA].[Route_User_Map] CHECK CONSTRAINT [FK__Route_Use__App_u__32EB7E57]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[BUSDTA].[FK__User_Role__App_u__37B03374]') AND parent_object_id = OBJECT_ID(N'[BUSDTA].[User_Role_Map]'))
ALTER TABLE [BUSDTA].[User_Role_Map]  WITH CHECK ADD  CONSTRAINT [FK__User_Role__App_u__37B03374] FOREIGN KEY([App_user_id])
REFERENCES [BUSDTA].[user_master] ([App_user_id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[BUSDTA].[FK__User_Role__App_u__37B03374]') AND parent_object_id = OBJECT_ID(N'[BUSDTA].[User_Role_Map]'))
ALTER TABLE [BUSDTA].[User_Role_Map] CHECK CONSTRAINT [FK__User_Role__App_u__37B03374]
GO
