USE [MobileDataModel2]
GO
/****** Object:  Table [BUSDTA].[M0101]    Script Date: 3/20/2015 7:05:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BUSDTA].[M0101](
	[ABAN8] [numeric](8, 0) NOT NULL,
	[ABALKY] [nchar](20) NULL,
	[ABTAX] [nchar](20) NULL,
	[ABALPH] [nchar](40) NULL,
	[ABMCU] [nchar](12) NULL,
	[ABAT1] [nchar](3) NULL,
	[ABCM] [nchar](2) NULL,
	[ABTAXC] [nchar](1) NULL,
	[ABAT2] [nchar](1) NULL,
	[ABAN81] [float] NULL,
	[ABAN82] [float] NULL,
	[ABAN83] [float] NULL,
	[ABAN84] [float] NULL,
	[ABAN86] [float] NULL,
	[ABAN85] [float] NULL,
	[ABAC01] [nchar](3) NULL,
	[ABAC02] [nchar](3) NULL,
	[ABAC03] [nchar](3) NULL,
	[ABAC04] [nchar](3) NULL,
	[ABAC05] [nchar](3) NULL,
	[ABAC06] [nchar](3) NULL,
	[ABAC07] [nchar](3) NULL,
	[ABAC08] [nchar](3) NULL,
	[ABAC09] [nchar](3) NULL,
	[ABTX2] [nchar](20) NULL,
	[ABALP1] [nchar](40) NULL,
 CONSTRAINT [M0101_PK] PRIMARY KEY CLUSTERED 
(
	[ABAN8] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [BUSDTA].[M0111]    Script Date: 3/20/2015 7:05:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BUSDTA].[M0111](
	[CDAN8] [numeric](8, 0) NOT NULL,
	[CDIDLN] [numeric](5, 0) NOT NULL,
	[CDRCK7] [numeric](5, 0) NOT NULL,
	[CDCNLN] [numeric](5, 0) NOT NULL,
	[CDPHTP] [numeric](8, 0) NULL,
	[CDAR1] [nchar](6) NULL,
	[CDPH1] [nchar](20) NULL,
	[CDETP] [nchar](4) NULL,
	[CDEMAL] [nvarchar](256) NULL,
	[CDFNAME] [nchar](100) NULL,
	[CDLNAME] [nchar](100) NULL,
	[CDID] [numeric](8, 0) NULL,
	[CDEXTN] [numeric](8, 0) NULL,
	[CDACTV] [bit] NULL,
	[CDDFLT] [bit] NULL,
	[CDTITL] [nchar](10) NULL,
 CONSTRAINT [M0111_PK] PRIMARY KEY CLUSTERED 
(
	[CDAN8] ASC,
	[CDIDLN] ASC,
	[CDCNLN] ASC,
	[CDRCK7] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [BUSDTA].[M03012]    Script Date: 3/20/2015 7:05:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BUSDTA].[M03012](
	[CMAN8] [numeric](8, 0) NOT NULL,
	[CMCO] [nchar](5) NOT NULL,
	[CMMCUR] [nchar](12) NULL,
	[CMTXA1] [nchar](10) NULL,
	[CMEXR1] [nchar](2) NULL,
	[CMACL] [float] NULL,
	[CMHDAR] [nchar](1) NULL,
	[CMTRAR] [nchar](3) NULL,
	[CMSTTO] [nchar](1) NULL,
	[CMRYIN] [nchar](1) NULL,
	[CMSTMT] [nchar](1) NULL,
	[CMARPY] [float] NULL,
	[CMSITO] [nchar](1) NULL,
	[CMCYCN] [nchar](2) NULL,
	[CMBO] [nchar](1) NULL,
	[CMTSTA] [nchar](2) NULL,
	[CMCKHC] [nchar](1) NULL,
	[CMDLC] [numeric](18, 0) NULL,
	[CMDNLT] [nchar](1) NULL,
	[CMPLCR] [nchar](10) NULL,
	[CMRVDJ] [numeric](18, 0) NULL,
	[CMDSO] [float] NULL,
	[CMCMGR] [nchar](10) NULL,
	[CMCLMG] [nchar](10) NULL,
	[CMAB2] [nchar](1) NULL,
	[CMDT1J] [numeric](18, 0) NULL,
	[CMDFIJ] [numeric](18, 0) NULL,
	[CMDLIJ] [numeric](18, 0) NULL,
	[CMDLP] [numeric](18, 0) NULL,
	[CMASTY] [float] NULL,
	[CMSPYE] [float] NULL,
	[CMAHB] [float] NULL,
	[CMALP] [float] NULL,
	[CMABAM] [float] NULL,
	[CMABA1] [float] NULL,
	[CMAPRC] [float] NULL,
	[CMMAXO] [float] NULL,
	[CMMINO] [float] NULL,
	[CMOYTD] [float] NULL,
	[CMOPY] [float] NULL,
	[CMPOPN] [nchar](10) NULL,
	[CMDAOJ] [numeric](18, 0) NULL,
	[CMAN8R] [float] NULL,
	[CMBADT] [nchar](1) NULL,
	[CMCPGP] [nchar](8) NULL,
	[CMORTP] [nchar](8) NULL,
	[CMTRDC] [float] NULL,
	[CMINMG] [nchar](10) NULL,
	[CMEXHD] [nchar](1) NULL,
	[CMHOLD] [nchar](2) NULL,
	[CMROUT] [nchar](3) NULL,
	[CMSTOP] [nchar](3) NULL,
	[CMZON] [nchar](3) NULL,
	[CMCARS] [float] NULL,
	[CMDEL1] [nchar](30) NULL,
	[CMDEL2] [nchar](30) NULL,
	[CMLTDT] [float] NULL,
	[CMFRTH] [nchar](3) NULL,
	[CMAFT] [nchar](1) NULL,
	[CMAPTS] [nchar](1) NULL,
	[CMSBAL] [nchar](1) NULL,
	[CMBACK] [nchar](1) NULL,
	[CMPORQ] [nchar](1) NULL,
	[CMPRIO] [nchar](1) NULL,
	[CMARTO] [nchar](1) NULL,
	[CMINVC] [float] NULL,
	[CMICON] [nchar](1) NULL,
	[CMBLFR] [nchar](1) NULL,
	[CMNIVD] [numeric](18, 0) NULL,
	[CMLEDJ] [numeric](18, 0) NULL,
	[CMPLST] [nchar](1) NULL,
	[CMEDF1] [nchar](1) NULL,
	[CMEDF2] [nchar](1) NULL,
	[CMASN] [nchar](8) NULL,
	[CMDSPA] [nchar](1) NULL,
	[CMCRMD] [nchar](1) NULL,
	[CMAMCR] [float] NULL,
	[CMAC01] [nchar](3) NULL,
	[CMAC02] [nchar](3) NULL,
	[CMAC03] [nchar](3) NULL,
	[CMAC04] [nchar](3) NULL,
	[CMAC05] [nchar](3) NULL,
	[CMAC06] [nchar](3) NULL,
	[CMAC07] [nchar](3) NULL,
	[CMAC08] [nchar](3) NULL,
	[CMAC09] [nchar](3) NULL,
	[CMAC10] [nchar](3) NULL,
	[CMAC11] [nchar](3) NULL,
	[CMAC12] [nchar](3) NULL,
	[CMAC13] [nchar](3) NULL,
	[CMAC14] [nchar](3) NULL,
	[CMAC15] [nchar](3) NULL,
	[CMAC16] [nchar](3) NULL,
	[CMAC17] [nchar](3) NULL,
	[CMAC18] [nchar](3) NULL,
	[CMAC19] [nchar](3) NULL,
	[CMAC20] [nchar](3) NULL,
	[CMAC21] [nchar](3) NULL,
	[CMAC22] [nchar](3) NULL,
	[CMAC23] [nchar](3) NULL,
	[CMAC24] [nchar](3) NULL,
	[CMAC25] [nchar](3) NULL,
	[CMAC26] [nchar](3) NULL,
	[CMAC27] [nchar](3) NULL,
	[CMAC28] [nchar](3) NULL,
	[CMAC29] [nchar](3) NULL,
	[CMAC30] [nchar](3) NULL,
	[CMTERRID] [float] NULL,
	[CMCIG] [float] NULL,
	[CMTORG] [nchar](10) NULL,
 CONSTRAINT [M03012_PK] PRIMARY KEY CLUSTERED 
(
	[CMAN8] ASC,
	[CMCO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [BUSDTA].[M080111]    Script Date: 3/20/2015 7:05:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BUSDTA].[M080111](
	[CTID] [numeric](8, 0) NOT NULL,
	[CTTYP] [nchar](10) NULL,
	[CTCD] [nchar](5) NOT NULL,
	[CTDSC1] [nchar](50) NULL,
	[CTTXA1] [nchar](10) NULL,
 CONSTRAINT [M080111_PK] PRIMARY KEY CLUSTERED 
(
	[CTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]

GO
