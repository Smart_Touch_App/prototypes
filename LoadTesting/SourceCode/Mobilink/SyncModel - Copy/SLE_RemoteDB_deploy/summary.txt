This summary was created on April 8, 2015 3:08:48 PM IST
The following files have been generated:
    D:\DB Synchronisation\Synchronization Proj\Post POC\SYNCSQLSERVER\SYNCSQLSERVER2012\SLE_RemoteDB_deploy\remote_setup.sql      (SQL to setup a remote database)
    D:\DB Synchronisation\Synchronization Proj\Post POC\SYNCSQLSERVER\SYNCSQLSERVER2012\SLE_RemoteDB_deploy\cons_setup.sql      (SQL to setup a consolidated database)
    D:\DB Synchronisation\Synchronization Proj\Post POC\SYNCSQLSERVER\SYNCSQLSERVER2012\SLE_RemoteDB_deploy\remote_setup.bat      (Command file to execute SQL against a remote database.)
    D:\DB Synchronisation\Synchronization Proj\Post POC\SYNCSQLSERVER\SYNCSQLSERVER2012\SLE_RemoteDB_deploy\cons_setup.bat      (Command file to execute SQL against a consolidated database.)
    D:\DB Synchronisation\Synchronization Proj\Post POC\SYNCSQLSERVER\SYNCSQLSERVER2012\SLE_RemoteDB_deploy\mlsrv.bat      (Command file to start the MobiLink server.)
    D:\DB Synchronisation\Synchronization Proj\Post POC\SYNCSQLSERVER\SYNCSQLSERVER2012\SLE_RemoteDB_deploy\sync.bat      (Command file to start the synchronization client.)
Consolidated database: MobileDataModel
MobiLink server command line: CommandLine_1
MobiLink server options: -c "DSN=MSDEVDB01" -b -v -zp -x TCPIP(host="localhost";port="2439")
Client network options: TCPIP(host=localhost;port=2439)
MobiLink user: FBM783
MobiLink password is not set.
Synchronization profile name: SLE_RemoteDB_FBM783
Remote synchronization options: Subscription=SLE_RemoteDB_FBM783
SQL will be executed against the consolidated database to prepare it for synchronization.
A new remote database will be created and the SQL executed to prepare it for synchronization.
    The new database will be written to: D:\DB Synchronisation\Synchronization Proj\Post POC\SYNCSQLSERVER\SYNCSQLSERVER2012\SLE_RemoteDB_deploy\SLE_RemoteDB_remote.db
