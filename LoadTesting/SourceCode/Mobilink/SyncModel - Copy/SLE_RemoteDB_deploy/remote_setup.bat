@echo off
setlocal

if not "%sqlany16%"=="" goto have_sqlanyn
set _sa_bin=
goto after_find_sa_bin
:have_sqlanyn
set _sa_bin="%sqlany16%\Bin64\"
if exist %_sa_bin%dbisql.exe goto after_find_sa_bin
set _sa_bin="%sqlany16%\Bin32\"
:after_find_sa_bin

REM Setting variables. . .
set DEPLOY_FILE=%CD%\SLE_RemoteDB_remote.db
set CONNECTION=dbf=%DEPLOY_FILE%;uid=dba;pwd=sql
set SQL_FILE=remote_setup.sql

REM Executing commands. . .
echo %_sa_bin%dberase -y "%DEPLOY_FILE%"
%_sa_bin%dberase -y "%DEPLOY_FILE%"
echo %_sa_bin%dbinit "%DEPLOY_FILE%"
%_sa_bin%dbinit "%DEPLOY_FILE%"
echo %_sa_bin%dbisql -nogui -c "%CONNECTION%" READ ENCODING UTF8 '%SQL_FILE%'
%_sa_bin%dbisql -nogui -c "%CONNECTION%" READ ENCODING UTF8 '%SQL_FILE%'

:end
