@echo off
setlocal

if not "%sqlany16%"=="" goto have_sqlanyn
set _sa_bin=
goto after_find_sa_bin
:have_sqlanyn
set _sa_bin="%sqlany16%\Bin64\"
if exist %_sa_bin%mlsrv16.exe goto after_find_sa_bin
set _sa_bin="%sqlany16%\Bin32\"
:after_find_sa_bin

REM Setting variables. . .
set CONNECTION=DSN=MSDEVDB01

REM Executing commands. . .
echo start "" %_sa_bin%mlsrv16 -c "%CONNECTION%" -b -v -zp -x TCPIP(host="localhost";port="2439")
start "" %_sa_bin%mlsrv16 -c "%CONNECTION%" -b -v -zp -x TCPIP(host="localhost";port="2439")

:end
