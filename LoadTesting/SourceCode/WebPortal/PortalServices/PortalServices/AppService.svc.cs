﻿using IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PortalServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class AppService : IDataService.IAppService
    {
        public HealthResponse IsServerAvailable()
        {
            return new HealthResponse { IsServerReachable = false };
        }
        public DataResponse NewDataAvailable(string remoteID)
        {
            System.Threading.Thread.Sleep(9000);
            return new DataResponse { IsServerReachable = true, RemoteID = remoteID, Payload = new List<string> { "ImmediatePublication", "DelayedPublication" } };
        }
    }
}
