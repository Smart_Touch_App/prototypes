﻿using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Controllers
{
      [Authorize]
    public class DeviceController : Controller
    {
          readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Device
        public ActionResult Index()
        {
            try
            {
                logger.Info("DeviceController Index");
                SLEWebPortal.Models.DeviceViewModel model = new Models.DeviceViewModel();
                model.Devices = new Managers.DeviceManager().GetDeviceList();
                model.TotalDevices = model.Devices.Count;
                model.ActiveDevices = Convert.ToInt32(model.Devices.Count(dev => dev.Active == 1));
                return View(model);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceController Index" + ex.Message);
                throw ex;
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddDevice(DeviceViewModel mod)
        {
            try {
                logger.Info("DeviceController AddDevice Parameters: "+mod);
            if (!ModelState.IsValid)
            {
                SLEWebPortal.Models.DeviceViewModel model = new Models.DeviceViewModel();
                model.Devices = new Managers.DeviceManager().GetDeviceList();
                model.TotalDevices = model.Devices.Count;
                model.ActiveDevices = Convert.ToInt32(model.Devices.Count(dev => dev.Active == 1));
                return View("Index", model);
            }

            var deviceid = mod.DeviceID;
            var deviceModel = mod.Model;
            var deviceManufacturer = mod.Manufacturer;
            new Managers.DeviceManager().AddDevice(deviceid, deviceModel, deviceManufacturer);
            SLEWebPortal.Models.DeviceViewModel model2 = new Models.DeviceViewModel();
            model2.Devices = new Managers.DeviceManager().GetDeviceList();
            model2.TotalDevices = model2.Devices.Count;
            model2.ActiveDevices = Convert.ToInt32(model2.Devices.Count(dev => dev.Active == 1));
            return View("Index", model2);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceController AddDevice" + ex.Message);
                throw ex;
            }
        }
        public ActionResult ToggleActive(string State, string DeviceID)
        {
            try {
                logger.Info("DeviceController ToggleActive Parameters: "+State+" "+DeviceID);
            new Managers.DeviceManager().UpdateDevice(State, DeviceID);
            SLEWebPortal.Models.DeviceViewModel model = new Models.DeviceViewModel();
            model.Devices = new Managers.DeviceManager().GetDeviceList();
            model.TotalDevices = model.Devices.Count;
            model.ActiveDevices = Convert.ToInt32(model.Devices.Count(dev => dev.Active == 1));
            return RedirectToAction("Index", "Device");
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceController ToggleActive" + ex.Message);
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult AddDeviceTemp()
        {
            try {
                logger.Info("DeviceController AddDeviceTemp");
            decimal principle = Convert.ToDecimal(Request["txtAmount"].ToString());
            decimal rate = 2;
            int time = 3;

            decimal simpleInteresrt = (principle * time * rate) / 100;

            StringBuilder sbInterest = new StringBuilder();
            sbInterest.Append("<b>Amount :</b> " + principle + "<br/>");
            sbInterest.Append("<b>Rate :</b> " + rate + "<br/>");
            sbInterest.Append("<b>Time(year) :</b> " + time + "<br/>");
            sbInterest.Append("<b>Interest :</b> " + simpleInteresrt);
            return Content(sbInterest.ToString());
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceController AddDeviceTemp" + ex.Message);
                throw ex;
            }
        }
    }
}