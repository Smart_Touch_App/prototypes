﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
namespace SLEWebPortal.Controllers
{
      [Authorize]
    public class CustomerController : Controller
    {
          readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Customer
        public ActionResult Index()
        {
            try
            {
                logger.Info("CustomerController Index");
                Models.CustomerModels model = new Models.CustomerModels();
                model.Customers = new Managers.CustomerManager().GetAllCustomers();
                return View(model);
            }
            catch (Exception ex)
            {
                logger.Error("Error in CustomerController Index" + ex.Message);
                throw ex;
            }
        }
    }
}