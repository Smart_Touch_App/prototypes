﻿using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using log4net;
namespace SLEWebPortal.Controllers
{
    public class AccountController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Account
        public ActionResult Index()
        {
            return View("Login");
        }
        public ActionResult Login(Login loginModel)
        {
            try
            {
                logger.Info("AccountController Login Parameters: " + loginModel);
                Managers.UserManager userManager = new Managers.UserManager();
               LoginViewModel authenticatedUser = userManager.LoginUser(loginModel.UserName, loginModel.Password);
                if (!ModelState.IsValid)
                {
                    return View(loginModel);
                }
                if (authenticatedUser != null)
                {
                    FormsAuthentication.SignOut();
                    Session["user"] = authenticatedUser;
                    FormsAuthentication.SetAuthCookie("userLogCookie", true);
                    if (authenticatedUser.Role == Role.ITADMIN.ToString())
                    {
                        return RedirectToAction("Index", "User");
                    }
                    else if (authenticatedUser.Role == Role.DSM.ToString())
                    {
                        return RedirectToAction("Index", "DSADashboard");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Dashboard");
                      //  return RedirectToAction("Index", "DSADashboard");
                    }
                }
                else
                {
                    return View(loginModel);
                }
            }
            catch(Exception ex)
            {
                logger.Error("Error in AccountController Login" + ex.Message);
                throw ex;
            }
        }
        
        public ActionResult Logout()
        {
            try
            {
                logger.Info("AccountController Logout");
                FormsAuthentication.SignOut();
                return RedirectToAction("Index", "Account");
            }
            catch (Exception ex)
            {
                logger.Error("Error in AccountController Logout" + ex.Message);
                throw ex;
            }
        }
    }
}