﻿using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Controllers
{
    [Authorize]
    public class RouteController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Route
        public ActionResult Index()
        {
            try {
                logger.Info("RouteController Index");
            RouteModels models = new RouteModels();
            models.Routes = new Managers.RouteManager().GetRouteList();
            return View(models);
                }
            catch(Exception ex)
            {
                logger.Error("Error in RouteController Index" + ex.Message);
                throw ex;
            }
        }
        public ActionResult RouteCustomers(string routeID)
        {
            try{
                logger.Info("RouteController RouteCustomers Parameters: " + routeID);
            CustomerModels model = new CustomerModels();
            model.Customers = new Managers.CustomerManager().GetCustomersForRoute(routeID);
            return View("RouteCustomers", model);
             }
            catch(Exception ex)
            {
                logger.Error("Error in RouteController RouteCustomers" + ex.Message);
                throw ex;
            }
        }
        public ActionResult RouteDevices(string routeID)
        {
            try {
                logger.Info("RouteController RouteDevices Parameters: " + routeID);
            DeviceViewModel model = new DeviceViewModel();
            model.RouteDevices = new Managers.DeviceManager().GetDeviceListForRoute(routeID);
            return View("RouteDevices", model);
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteController RouteDevices" + ex.Message);
                throw ex;
            }
        }
    }
}