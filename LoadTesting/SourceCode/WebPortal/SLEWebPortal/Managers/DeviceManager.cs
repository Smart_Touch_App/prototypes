﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using log4net;
namespace SLEWebPortal.Managers
{
    public class DeviceManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Device> GetDeviceList()
        {
            try {
                logger.Info("DeviceManager GetDeviceList");
            string query = string.Empty;
            query = "select device_id as DeviceID,model as Model,manufacturer as Manufacturer,Active from Busdta.Device_Master order by last_modified desc ";
            DataSet dsDevices = DbHelper.ExecuteDataSet(query);
            return dsDevices.GetEntityList<Device>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceManager GetDeviceList" + ex.Message);
                throw ex;
            }
        }
        public List<RouteDevice> GetDeviceListForRoute(string routeID)
        {
            try
            {
                logger.Info("DeviceManager GetDeviceListForRoute Parameters: "+routeID);
            string query = string.Empty;
            query = "select dm.device_id as DeviceID,dm.Active, dm.Model, dm.Manufacturer,rdm.Route_Id as [Route] ";
            query = query + "from Busdta.Device_Master dm inner join  ";
            query = query + "busdta.Route_Device_Map rdm on rdm.Device_Id = dm.Device_Id ";
            query = query + "where rdm.Route_Id = '{0}'";
            query = string.Format(query, routeID.Trim());
            DataSet dsDevices = DbHelper.ExecuteDataSet(query);
            return dsDevices.GetEntityList<RouteDevice>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceManager GetDeviceListForRoute" + ex.Message);
                throw ex;
            }
        }
        public bool AddDevice(string deviceID, string model, string manufacturer)
        {
            string query = string.Empty;
            int result = -1;
            try
            {
                logger.Info("DeviceManager AddDevice Parameters: " + deviceID + " " + model + " " + manufacturer);
                query = "select count(Device_Id) from Busdta.Device_Master where Device_Id='{0}'";
                query = string.Format(query, deviceID);
                result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                if (result == 0)
                {
                    query = "insert into Busdta.Device_Master values('{0}',1,getdate(),'{1}','{2}')";
                    query = string.Format(query, deviceID, manufacturer, model);
                    result = DbHelper.ExecuteNonQuery(query);
                }
                
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceManager AddDevice" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }
        public bool UpdateDevice(string state, string deviceID)
        {
            int result = -1;
            try
            {
                logger.Info("DeviceManager UpdateDevice Parameters: " + state + " " + deviceID);
                string query = string.Empty;
                int toggleTo = state.ToLower() == "on" ? 1 : 0;
                query = "update  Busdta.Device_Master set active=" + toggleTo + " where device_id='" + deviceID + "'";
                result = DbHelper.ExecuteNonQuery(query);
                
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeviceManager UpdateDevice" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }
    }
}