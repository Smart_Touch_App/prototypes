﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using System.Web.Mvc;
using log4net;
namespace SLEWebPortal.Managers
{
    public class RouteManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Route> GetRouteList()
        {
            try
            {
                logger.Info("RouteManager GetRouteList");
                string query = string.Empty;
                query = query + "select distinct ";
                query = query + "r.FFUSER as 'RouteID', ";
                query = query + "r.FFROUT as 'RouteNo', ";
                query = query + "r.FFMCU as 'Branch', ";
                query = query + "descp.DRDL01 as 'Description' ";
                query = query + "from BUSDTA.F56M0001 r left outer join ";
                query = query + "BUSDTA.F0005 descp on LTRIM(rtrim(descp.DRKY))= LTRIM(rtrim(r.FFROUT))";
                query = query + "where descp.DRSY = '42' and descp.DRRT = 'RT' ";
                DataSet dsRoute = DbHelper.ExecuteDataSet(query);
                List<Route> routes = new List<Route>();
                if (dsRoute.HasData())
                {
                    routes = dsRoute.GetEntityList<Route>();
                }
                return routes;
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteManager GetRouteList" + ex.Message);
                throw ex;
            }
        }
        public IEnumerable<SelectListItem> GetRouteSelectableList()
        {
            
            List<SelectListItem> list = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("RouteManager GetRouteSelectableList");
            query = query + "select distinct ";
            query = query + "r.FFUSER as 'RouteID', ";
            query = query + "r.FFROUT as 'RouteNo', ";
            query = query + "r.FFMCU as 'Branch', ";
            query = query + "descp.DRDL01 as 'Description' ";
            query = query + "from BUSDTA.F56M0001 r left outer join ";
            query = query + "BUSDTA.F0005 descp on LTRIM(rtrim(descp.DRKY))= LTRIM(rtrim(r.FFROUT))";
            query = query + "where descp.DRSY = '42' and descp.DRRT = 'RT' ";
            DataSet dsRoute = DbHelper.ExecuteDataSet(query);
            List<Route> routes = new List<Route>();
            if (dsRoute.HasData())
            {
                foreach (DataRow route in dsRoute.Tables[0].Rows) {
                    list.Add(new SelectListItem { Text = route["RouteID"].ToString(), Value = route["RouteID"].ToString() });
                }
            }
            return new SelectList(list, "Value", "Text");
                }
            catch(Exception ex)
            {
                logger.Error("Error in RouteManager GetRouteSelectableList" + ex.Message);
                throw ex;
            }
        }
       
    }
}