﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using log4net;
namespace SLEWebPortal.Managers
{
    public class CustomerManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Customer> GetCustomersForRoute(string routeID)
        {
           
            try
            {
                logger.Info("CustomerManager GetOrdersForCustomer Parameters: " + routeID);
                string query = string.Empty;
                query = query + "SELECT distinct a.aban8,AIZON,AISTOP as DeliveryCode,convert(varchar,a.aban8) AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , ";
                query = query + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
                query = query + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
                query = query + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip ";
                query = query + "FROM busdta.f0101 AS a ";
                query = query + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
                query = query + "JOIN busdta.f0101 AS b ON a.aban81 =b.aban8 ";
                query = query + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
                query = query + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
                query = query + "where AIZON>' ' and AISTOP>' '  ";
                query = query + "and a.abac03 ='{0}'";
                query = string.Format(query, routeID.Replace("FBM", ""));
                List<Customer> customersList = new List<Customer>();
                DataSet dsCustomersForRoute = DbHelper.ExecuteDataSet(query);
                return dsCustomersForRoute.GetEntityList<Customer>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CustomerManager GetCustomersForRoute" + ex.Message);
                throw ex;
            }
        }
        public List<Customer> GetAllCustomers()
        {
            try {
                logger.Info("CustomerManager GetAllCustomers");
            string query = string.Empty;
            query = query + "SELECT distinct a.aban8,AIZON,AISTOP as DeliveryCode,convert(varchar,a.aban8) AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , ";
            query = query + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
            query = query + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
            query = query + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip ";
            query = query + "FROM busdta.f0101 AS a ";
            query = query + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
            query = query + "JOIN busdta.f0101 AS b ON a.aban81 =b.aban8 ";
            query = query + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
            query = query + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
            query = query + "where AIZON>' ' and AISTOP>' '  ";
            List<Customer> customersList = new List<Customer>();
            DataSet dsCustomersForRoute = DbHelper.ExecuteDataSet(query);
            return dsCustomersForRoute.GetEntityList<Customer>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CustomerManager GetAllCustomers" + ex.Message);
                throw ex;
            }
        }
    }
}