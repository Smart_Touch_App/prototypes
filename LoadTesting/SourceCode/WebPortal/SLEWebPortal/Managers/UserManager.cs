﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using System.Web.Mvc;
using log4net;
namespace SLEWebPortal.Managers
{
    public class UserManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public LoginViewModel LoginUser(string userID, string password)
        {
            try
            {
                logger.Info("UserManager LoginUser parameters: " + userID + " " + password);
                string query = string.Empty;
                query = "select u.app_user as UserName,u.Name as DisplayName, u.app_user_id + '' as ID,rm.Role  from busdta.user_master u  ";
                query = query + "left outer join BUSDTA.User_Role_Map rm  ";
                query = query + "on rm.App_user_id = u.App_user_id ";
                query = query + "where  ";
                query = query + "u.App_User = '{0}'  ";
                query = query + "and u.AppPassword='{1}'";
                query = string.Format(query, userID, password);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);
                return dsDevices.HasData() ? dsDevices.GetEntityList<LoginViewModel>().First<LoginViewModel>() : null;
            }
            catch (Exception ex)
            {
                logger.Error("Error in UserManager LoginUser" + ex.Message);
                throw ex;
            }
        }
        public string AddApplciationUser(string displayName, string loginID, string password)
        {
            string query = string.Empty;
            string userID = string.Empty;
            int result = -1;
            try {
                logger.Info("UserManager AddApplciationUser parameters: " + displayName + " " + loginID + " " + password);
            query = "select count(app_user) from busdta.user_master where app_user='{0}'";
            query = string.Format(query, loginID.Trim());
            result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
            if (result == 0)
            {
                query = "insert into busdta.user_master(app_user,appPassword,Name,DomainUser) values('{0}','{1}','{2}','{3}')";
                query = string.Format(query, loginID.Trim(), password, displayName.Trim(), displayName.Trim());
                result = DbHelper.ExecuteNonQuery(query);
            }
            if (result > 0)
            {
                query = "select IDENT_CURRENT('busdta.user_master')";
                userID = DbHelper.ExecuteScalar(query).ToString();
            }
           
                }
            catch(Exception ex)
            {

                logger.Error("Error in UserManager AddApplciationUser" + ex.Message);
                throw ex;
            }
            return userID;
        }
        public bool AddApplciationUserRole(string userID, string role)
        {
            string query = string.Empty;
            try
            {
                logger.Info("UserManager AddApplciationUserRole parameters: " + userID + " " + role);
                query = "insert into busdta.User_Role_Map(app_user_id,Role,last_modified) values('{0}','{1}',{2})";
                query = string.Format(query, userID, role, DateTime.Now.ToShortDateString());
                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch(Exception ex)
            {

                logger.Error("Error in UserManager AddApplciationUserRole" + ex.Message);
                  throw ex;
            }
        }
        public bool UpdateApplciationUser(string state, string loginID)
        {
            string query = string.Empty;
            try
            {
                logger.Info("UserManager UpdateApplciationUser parameters: " + state + " " + loginID);
                int toggleTo = state.ToLower() == "on" ? 1 : 0;
                query = "update busdta.user_master set active=" + toggleTo + " where app_user_id='" + loginID + "'";
                int result = DbHelper.ExecuteNonQuery(query);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {

                logger.Error("Error in UserManager UpdateApplciationUser" + ex.Message);
                  throw ex;
            }
        }
        public bool MapApplciationUserToRoute(string userID, string routeID)
        {
            string query = string.Empty;
            int result = -1;
            int result1;
            try
            {
                logger.Info("UserManager MapApplciationUserToRoute parameters: " + userID + " " + routeID);
                query = "select count(App_user_id) from BUSDTA.Route_User_Map where App_user_id ='{0}' and Route_Id ='{1}'";
                query = string.Format(query, userID, routeID.Trim());
                result1 = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                if (result1 == 0)
                {
                    query = "insert into BUSDTA.Route_User_Map (App_user_id,Route_Id,Active,last_modified) values('{0}','{1}',0,'{2}')";
                    query = string.Format(query, userID, routeID.Trim(), DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                    result = DbHelper.ExecuteNonQuery(query);
                }
               
            }
            catch (Exception ex)
            {

                logger.Error("Error in UserManager MapApplciationUserToRoute" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }
        public IEnumerable<SelectListItem> GetUserRoles()
        {
            try
            {
                logger.Info("UserManager GetUserRoles");
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem { Text = "RSR", Value = "RSR" });
                list.Add(new SelectListItem { Text = "RSM", Value = "RSM" });
                list.Add(new SelectListItem { Text = "DSM", Value = "DSM" });
                list.Add(new SelectListItem { Text = "Administrator", Value = "ITADMIN" });
                return new SelectList(list, "Value", "Text");
            }
            catch (Exception ex)
            {

                logger.Error("Error in UserManager GetUserRoles");
                throw ex;
            }
        }
        public List<LoginViewModel> GetApplciationUsers()
        {
            string query = string.Empty;
            try
            {
                logger.Info("UserManager GetApplciationUsers");
                query = "select u.Active, u.app_user as UserName,u.Name as DisplayName, u.appPassword as Password, u.app_user_id + '' as ID,rm.Role,u.Created_on as CreatedOn, rum.Route_ID as Route from busdta.user_master u ";
                query = query + "left outer join BUSDTA.User_Role_Map rm  ";
                query = query + "left outer join BUSDTA.Route_User_Map rum  ";
                query = query + "on rm.app_user_id = rum.app_user_id  ";
                query = query + "on rm.App_user_id = u.App_user_id  order by u.Created_On desc";
                DataSet result = DbHelper.ExecuteDataSet(query);
                return result.GetEntityList<LoginViewModel>();
            }
            catch (Exception ex)
            {

                logger.Error("Error in UserManager GetApplciationUsers");
                throw ex;
            
            }
        }
        public IEnumerable<SelectListItem> GetApplciationUserSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            string query = string.Empty;
            try
            {
                logger.Info("UserManager GetApplciationUserSelectList");
                query = "select u.Active, u.app_user as UserName,u.Name as DisplayName, u.appPassword as Password, u.app_user_id + '' as ID,rm.Role,u.Created_on as CreatedOn from busdta.user_master u ";
                query = query + "left outer join BUSDTA.User_Role_Map rm  ";
                query = query + "on rm.App_user_id = u.App_user_id  order by u.Created_On desc";
                DataSet result = DbHelper.ExecuteDataSet(query);
                if (result.HasData())
                {
                    foreach (DataRow route in result.Tables[0].Rows)
                    {
                        list.Add(new SelectListItem { Text = route["UserName"].ToString(), Value = route["ID"].ToString() });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {

                logger.Error("Error in UserManager GetApplciationUserSelectList" + ex.Message);
                throw ex;
            
            }
        }        
    }  
}
