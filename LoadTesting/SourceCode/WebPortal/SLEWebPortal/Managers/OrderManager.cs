﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using log4net;


namespace SLEWebPortal.Managers
{
    public class OrderManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Order> GetRecentOrders()
        {
            try
            {
                logger.Info("OrderManager GetRecentOrders");
                Models.LoginViewModel model = (Models.LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.Order_ID as OrderID ";
                query = query + ",h.Order_Date as OrderDate ";
                query = query + ",h.Total_Coffee as TotalCoffee ";
                query = query + ",h.Total_Allied as TotalAllied ";
                query = query + ",h.Energy_Surcharge as EnergySurcharge ";
                query = query + ",h.Order_Total_Amt as OrderTotal ";
                query = query + ",h.Sales_Tax_Amt as SalesTaxAmount ";
                query = query + ",h.Invoice_Total as InvoiceTotal ";
                query = query + ",h.Surcharge_Reason_Code as SurchargeReasonCode ";
                query = query + ",h.Order_State as OrderState ";
                query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + ",h.updated_at as Updated ";
                query = query + ",a.aban8 AS CustomerNo ";

                query = query + ", a.abalph AS Name ";
                query = query + ", a.abat1 AS Address_type ";
                query = query + ",a.abac03 AS Route ";
                query = query + ", a.abmcu AS Route_Branch  ";
                query = query + ", a.aban81 AS Bill_To  ";
                query = query + ", h.Payment_Type AS PaymentType  ";
                query = query + ", h.Payment_ID AS PaymentID  ";
                query = query + ", a.aban81 AS Bill_To  ";
                query = query + "from BUSDTA.F0101 a  ";
                query = query + "left outer join  ";
                query = query + "BUSDTA.Order_Header h  ";
                query = query + "on h.Customer_Id = a.aban8 ";
                query = query + "where  ";
                query = query + "order_date between dateadd(day, datediff(day, 0 ,getdate())-30, 0) and getdate() and h.Created_By=" + model.ID + " ";
                query = query + "";
                DataSet dsRecentOrders = DbHelper.ExecuteDataSet(query);
                return dsRecentOrders.GetEntityList<Order>();
            }
            catch(Exception ex)
            {
                logger.Error("Error in OrderManager GetRecentOrders" + ex.Message);
                throw ex;
            }
        }



        public List<graphorder> GetYearlyCoffee()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "ROUND(sum(h.Total_Coffee),2)as graphcoffee";
                query = query + ",ROUND(sum(h.Total_Allied),2)as graphallie";
                query = query + ",ROUND(sum(h.Order_Total_Amt),2)as graphordertotal ";
                query = query + "from BUSDTA.Order_Header h where h.Created_By=" + model.ID + " and DATEPART(yy,h.Order_Date)=DATEPART(yy,Getdate()) ";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<graphorder> orders = new List<graphorder>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<graphorder>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }

        public List<graphorder> GetMonthlyCoffee()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "ROUND(sum(h.Total_Coffee),2)as graphcoffee";
                query = query + ",ROUND(sum(h.Total_Allied),2)as graphallie";
                query = query + ",ROUND(sum(h.Order_Total_Amt),2)as graphordertotal ";
                query = query + "from BUSDTA.Order_Header h where h.Created_By=" + model.ID + " and DATEPART(mm,h.Order_Date)=DATEPART(mm,Getdate()) ";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<graphorder> orders = new List<graphorder>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<graphorder>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }

        public List<graphorder> GetTodayCoffee()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "ROUND(sum(h.Total_Coffee),2)as graphcoffee";
                query = query + ",ROUND(sum(h.Total_Allied),2)as graphallie";
                query = query + ",ROUND(sum(h.Order_Total_Amt),2)as graphordertotal ";
                query = query + "from BUSDTA.Order_Header h where h.Created_By=" + model.ID + " and DATEPART(dd,h.Order_Date)=DATEPART(dd,Getdate()) ";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<graphorder> orders = new List<graphorder>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<graphorder>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }

        //public Order GetMonthlyCoffee()
        //{
        //    try
        //    {
        //        logger.Info("GetOrderList");
        //        LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
        //        string query = string.Empty;
        //        query = "select ";
        //        query = query + "sum(h.TotalCoffee)";
        //        query = query + "from BUSDTA.Order_Header h where h.Created_By=" + model.ID + " and DATEPART(mm,OrderDate)=DATEPART(mm,Getdate()) order by OrderDate desc";
        //        string coffee = DbHelper.ExecuteScalar(query);
        //        //List<Order> orders = new List<Order>();
        //        //if (dsOrderList.HasData())
        //        //{
        //        //    orders = dsOrderList.GetEntityList<Order>();
        //        //}
        //        return coffee;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("Error in GetOrderList" + ex.Message);
        //        throw ex;
        //    }
        //}


        public List<Order> GetOrderList()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.Order_ID as OrderID ";
                query = query + ",h.Customer_Id as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.Order_Date as OrderDate ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Coffee,2),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Allied,2),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Round(h.Order_Total_Amt,2),0)) as OrderTotal ";
                query = query + ",h.Order_State as OrderState ";
                query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f where h.Customer_id=f.ABAN8 And h.Created_By=" + model.ID + " order by OrderDate desc";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }
        public List<Order> GetOrdersForLast30Days()
        {
            try
            {
                logger.Info("OrderManager GetOrdersForLast30Days");
                string query = string.Empty;
                query = "select * from BUSDTA.Order_Header where order_date between dateadd(day, datediff(day, 0 ,getdate())-30, 0) and getdate()";
                DataSet dsRouteOrders = DbHelper.ExecuteDataSet(query);
                return dsRouteOrders.GetEntityList<Order>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrdersForLast30Days" + ex.Message);
                throw ex;
            }
        }
        
        public List<Order> GetOrderDetails(string orderID)
        {
            try
            {
                logger.Info("OrderManager GetOrderDetails Parameters: " + orderID);
                string query = string.Empty;
                query = "select ";
                query = query + "g.Order_ID as OrderID ";
                query = query + ",g.Item_Number as ItemNumber ";
                //query = query + ",IMDSC1 as ItemDescription ";
                query = query + ",g.Order_Qty as Quantity ";
                query = query + ",g.Order_UOM as UOM ";
                query = query + ",convert(varchar,Round(g.Unit_Price,2)) as UnitPrice ";
                query = query + ",convert(varchar,Round(g.Extn_Price,2)) as ExtnPrice ";
                query = query + "from BUSDTA.Order_Detail g ";
                query = query + "where  ";
                query = query + "order_id = " + orderID;
                DataSet dsOrderDetails = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsOrderDetails.HasData())
                {
                    orders = dsOrderDetails.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderDetails" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetVoidOrderList()
        {
            try
            {
                logger.Info("OrderManager GetVoidOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.Order_ID as OrderID ";
                query = query + ",h.Customer_Id as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.Order_Date as OrderDate ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Coffee,2),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Allied,2),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Round(h.Order_Total_Amt,2),0)) as OrderTotal ";
                query = query + ",h.Order_State as OrderState ";
                query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f where h.Customer_id=f.ABAN8 and h.Order_Sub_State = 'Void' and h.Created_By=" + model.ID + " order by OrderDate desc ";
                DataSet dsVoidOrderList = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsVoidOrderList.HasData())
                {
                    orders = dsVoidOrderList.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrderList" + ex.Message);
                throw ex;
            }
        }
        public List<Order> GetOrdersForToday(string orderID)
        {
            try
            {
                logger.Info("OrderManager GetOrdersForToday Parameters: " + orderID);
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "h.Order_ID as OrderID ";
                query = query + ",h.Customer_Id as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.Order_Date as OrderDate ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Coffee,2),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Allied,2),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Round(h.Order_Total_Amt,2),0)) as OrderTotal ";
                query = query + ",h.Order_State as OrderState ";
                query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f where h.Customer_id=f.ABAN8 ";
                query = query + "and h.Created_By=" + model.ID + " and ";
                query = query + "Order_Date = CONVERT (date, GETDATE()) ";
                DataSet dsOrdersForToday = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsOrdersForToday.HasData())
                {
                    orders = dsOrdersForToday.GetEntityList<Order>();
                }
                return orders;
            }
            catch(Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrderList" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetVoidOrders()
        {
            try
            {
                logger.Info("OrderManager GetVoidOrders");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.Order_ID as OrderID ";
                query = query + ",h.Customer_Id as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.Order_Date as OrderDate ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Coffee,2),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Allied,2),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Round(h.Order_Total_Amt,2),0)) as OrderTotal ";
                query = query + ",h.Order_State as OrderState ";
                query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f where h.Customer_id=f.ABAN8 And h.Created_By=" + model.ID + " And h.Order_Sub_State = 'Void' ";
                DataSet dsVoidOrders = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsVoidOrders.HasData())
                {
                    orders = dsVoidOrders.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrders" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetVoidOrdersForToday(string orderID)
        {
            try
            {
                logger.Info("OrderManager GetVoidOrdersForToday Parameters: " + orderID);
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.Order_ID as OrderID ";
                query = query + ",h.Customer_Id as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.Order_Date as OrderDate ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Coffee,2),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Round(h.Total_Allied,2),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Round(h.Order_Total_Amt,2),0)) as OrderTotal ";
                query = query + ",h.Order_State as OrderState ";
                query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f where h.Customer_id=f.ABAN8 ";
                query = query + "and h.Created_By=" + model.ID + " and  h.Order_Sub_State = 'Void' and ";
                query = query + "Order_Date = CONVERT (date, GETDATE()) ";
                DataSet dsVoidOrdersForToday = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsVoidOrdersForToday.HasData())
                {
                    orders = dsVoidOrdersForToday.GetEntityList<Order>();
                }
                return orders;
            }
            catch(Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrdersForToday" + ex.Message);
                throw ex;

            }
        }
        
        public void GetOrdersForCustomer()
        {
            try
            {
                logger.Info("OrderManager GetOrdersForCustomer");
                string query = string.Empty;
                DataSet dsCustomerOrders = DbHelper.ExecuteDataSet(query);
                if (dsCustomerOrders.HasData())
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrdersForCustomer" + ex.Message);
                throw ex;
            }
        }
        public void GetOrdersForRoute()
        {
            try
            {
                logger.Info("OrderManager GetOrdersForRoute");
                string query = string.Empty;
                DataSet dsRouteOrders = DbHelper.ExecuteDataSet(query);
                if (dsRouteOrders.HasData())
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrdersForRoute" + ex.Message);
                throw ex;
            }
        }
    }
}