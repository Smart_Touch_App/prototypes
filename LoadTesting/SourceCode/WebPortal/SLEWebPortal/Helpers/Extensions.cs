﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel;
using System.Reflection;


namespace SLEWebPortal.Helpers
{
    public static class Extensions
    {
       static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool HasData(this DataSet data)
        {
            if (!(data == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0))
            {
                return true;
            }
            return false;
        }
        public static bool HasValue(this DataTable data, string column, string value)
        {
            try {
                logger.Info("Extensions HasValue Parameters: " + data + " " + column + " " + value);
            data.DefaultView.RowFilter = column + "='" + value + "'";
            int count = data.DefaultView.ToTable().Rows.Count;
            data.DefaultView.RowFilter = "";

            return count > 0 ? true : false;
                }
            catch(Exception ex)
            {
                logger.Error("Error in Extensions HasValue" + ex.Message);
                throw ex;
            }
        }
        public static List<T> GetEntityList<T>(this DataSet ds) where T : new()
        {
            try
            {
                logger.Info("Extensions GetEntityList Parameters: " + ds);
                List<T> entityList = new List<T>();
                var castToEntity = new T();
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (castToEntity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                if (ds.HasData())
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var entity = new T();
                        foreach (DataColumn col in row.Table.Columns)
                        {
                            var tru = properties.FirstOrDefault(i => i.Name == col.ColumnName);
                            if (tru != null)
                            {
                                if (tru.PropertyType == typeof(string))
                                {
                                    tru.SetValue(entity, row[tru.Name].ToString());
                                    continue;
                                }
                                if (tru.PropertyType == typeof(DateTime))
                                {
                                    if (row[tru.Name] != DBNull.Value)
                                    {
                                        tru.SetValue(entity, Convert.ToDateTime(row[tru.Name]));
                                    }
                                    continue;
                                }
                                tru.SetValue(entity, row[tru.Name]);
                            }
                        }
                        entityList.Add(entity);
                    }
                }

                return entityList;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Extensions ExecuteDataSet" + ex.Message);
                throw ex;
            }
        }
        public static T GetEntity<T>(DataRow row) where T : new()
        {
            try
            {
                logger.Info("Extensions GetEntity");
                var entity = new T();
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (entity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                foreach (DataColumn col in row.Table.Columns)
                {
                    var tru = properties.FirstOrDefault(i => i.Name == col.ColumnName);
                    if (tru != null)
                    {
                        tru.SetValue(entity, row[tru.Name]);
                    }
                }
                return entity;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Extensions GetEntity" + ex.Message);
                throw ex;
            }
        }
    }
}