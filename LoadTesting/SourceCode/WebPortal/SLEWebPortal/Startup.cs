﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SLEWebPortal.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace SLEWebPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
