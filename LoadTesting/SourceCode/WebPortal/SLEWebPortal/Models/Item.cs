﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class Item
    {
  

        string _ItemNumber, _UM, _UMPrice, _ItemDescription, _SalesCat1, _SalesCat5, _StkType, _ShortDesc;
        decimal _ExtendedPrice, _UnitPrice;
        int _OrderQty;
        decimal _OldUnitPrice;
        int _ReasonCode;
        public string ItemNumber
        {
            get
            {
                return _ItemNumber;
            }
            set
            {
                _ItemNumber = value;
            }
        }
        public string ItemDescription
        {
            get
            {
                return _ItemDescription;
            }
            set
            {
                _ItemDescription = value;
            }
        }
        public string SalesCat1
        {
            get
            {
                return _SalesCat1;
            }
            set
            {
                _SalesCat1 = value;
            }
        }
        public string SalesCat5
        {
            get
            {
                return _SalesCat5;
            }
            set
            {
                _SalesCat5 = value;
            }
        }
        public string StkType
        {
            get
            {
                return _StkType;
            }
            set
            {
                _StkType = value;
            }
        }
        public string UM
        {
            get
            {
                return _UM;
            }
            set
            {
                _UM = value;

            }
        }
        public string UMPrice
        {
            get
            {
                return _UMPrice;
            }
            set
            {
                _UMPrice = value;
            }
        }
        public decimal ExtendedPrice
        {
            get
            {
                return _ExtendedPrice;
            }
            set
            {
                _ExtendedPrice = value;
            }
        }
        public decimal UnitPrice
        {
            get
            {
                return _UnitPrice;
            }
            set
            {
                _UnitPrice = value;

            }
        }
        public string ShortDesc
        {
            get
            {
                return _ShortDesc;
            }
            set
            {
                _ShortDesc = value;
            }
        }
        public int OrderQty
        {
            get
            {
                return _OrderQty;
            }
            set
            {
                _OrderQty = value;
                if (_OrderQty <= 0)
                {

                }
            }
        }
        public int ReasonCode
        {
            get
            {
                return _ReasonCode;
            }
            set
            {
                _ReasonCode = value;
            }
        }
        public string OrderDate
        {
            get;
            set;
        }
       
  
    }
}