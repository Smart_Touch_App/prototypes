﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class Order
    {
        public string OrderID { get; set; }        
        public string OrderDate { get; set; }
        public string TotalCoffee { get; set; }
        public string TotalAllied { get; set; }
        public string EnergySurcharge { get; set; }
        public string OrderTotal { get; set; }
        public string SalesTaxAmount { get; set; }
        public string InvoiceTotal { get; set; }
        public string SurchargeReasonCode { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string CreatedBy { get; set; }
        public string Address_type { get; set; }
        public string Route { get; set; }
        public string Route_Branch { get; set; }
        public string Bill_To { get; set; }
        public string PaymentType { get; set; }
        public string PaymentID { get; set; }
        public string OrderState { get; set; }
        public string OrderSubState { get; set; }
        public string Order_Detail_Id { get; set; }
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string Quantity { get; set; }
        public string UOM { get; set; }
        public string UnitPrice { get; set; }
        public string ExtnPrice { get; set; }
        public string Reason_Code { get; set; }
        public string last_modified { get; set; }
        public string IsTaxable { get; set; }                  
    }
}