﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class Customer
    {
        public string CustomerNo
        {
            get;
            set;
        }
        public string BillType
        {
            get;
            set;
        }
        public string BillTo
        {
            get;
            set;
        }
        public string DeliveryCode
        {
            get;
            set;
        }

        public string Route
        {
            get;
            set;
        }
        public string RouteBranch
        {
            get;
            set;
        }
        public string Address
        {
            get;
            set;
        }
        public string City
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }
        public string State
        {
            get;
            set;
        }
        public string Zip
        {
            get;
            set;
        }
    }
}