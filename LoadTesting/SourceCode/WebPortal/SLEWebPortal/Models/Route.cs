﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class Route
    {
        public string RouteID { get; set; }
        public string Branch { get; set; }
        public string Description { get; set; }
        public int Customers { get; set; }
        public int Devices { get; set; }
        
    }
}