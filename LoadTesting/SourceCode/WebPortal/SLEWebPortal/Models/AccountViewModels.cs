﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompareAttribute = System.Web.Mvc.CompareAttribute;

namespace SLEWebPortal.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Please Enter User Name")]
        [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed.")]
        [Display(Name = "User Name")]       
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please Enter Password")]
        [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]       
        public string Password { get; set; }        
    }


    public class CheckAgeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid
          (object value, ValidationContext validationContext)
        {
            string sErrorMessage = "No Spaces";
            if (value.ToString()!=" ")
            { return new ValidationResult(sErrorMessage); }
            return ValidationResult.Success;
        }
    }
    //[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    //public class MaxValueAttribute : ValidationAttribute
    //{
    //    private readonly int _maxValue;

    //    public MaxValueAttribute(int maxValue)
    //    {
    //        _maxValue = maxValue;
    //    }

    //    public override bool IsValid(object value)
    //    {
    //        return value.ToString().Length <= _maxValue;
    //    }
    //}

    public class MinValueAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly int _minValue;

        //public MinValueAttribute(double minValue)
        //{
        //    _minValue = minValue;
        //    ErrorMessage = "Enter a value greater or equal than " + _minValue;
        //}

        public MinValueAttribute(int minValue)
        {
            _minValue = minValue;
            ErrorMessage = "Enter a value greater or equal than " + _minValue;
        }

        public override bool IsValid(object value)
        {
            return value.ToString().Length >= _minValue;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = ErrorMessage;
            rule.ValidationParameters.Add("min", _minValue);
          //  rule.ValidationParameters.Add("max", Double.MaxValue);
            rule.ValidationType = "range";
            yield return rule;
        }

    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please Enter User Name")]
     //   [CheckAge]
      //  [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed")]
        [Display(Name = "User Name")]
        [StringLength(15, ErrorMessage = "{0} Must contain at least {2} and maximum 15 characters.", MinimumLength = 3)]
      //  [MinValue(45,ErrorMessage="Maximum 10 Characters allowed.") ]
        [RegularExpression(@"^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$", ErrorMessage = "Alphabets with Numbers only!!!")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed.")]      
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [StringLength(15, ErrorMessage = "{0} Must contain at least {2} and maximum 15 characters.", MinimumLength = 6)]
       
        
        public string Password { get; set; }

        [Required(ErrorMessage = "Please Re-Enter Password")]
        [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed.")]      
         [DataType(DataType.Password)]
         [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Passwords do not match.")]  
        public string confirmpassword { get; set; }

       // [Required(ErrorMessage = "Please Select Role")]
       // [Display(Name = "Role")]
        public string Role { get; set; }

        [Required(ErrorMessage = "Please Enter Display Name")]
      //  [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed.")]
        [StringLength(45, ErrorMessage = "{0} must contain at least {2} and maximum 45 characters.", MinimumLength = 3)]
      //  [RegularExpression(@"^(?=.*[0-9])(?=.*[a-z])([a-z0-9_-]+)$", ErrorMessage = "Minimum 1 Alphabet and 1 Number required!!!")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Only Alphabets allowed!!!")]
        [Display(Name = "Display Name")]
     
        public string DisplayName { get; set; }
        public string Route { get; set; }
        public string ID { get; set; }
        public bool Active { get; set; }
        public bool Selected { get; set; }
        public DateTime CreatedOn { get; set; }
    }
    public class UserRoleViewModel
    {
        // Display Attribute will appear in the Html.LabelFor                
        [Display(Name = "User Role")]
        public int SelectedUserRoleId { get; set; }
        public IEnumerable<SelectListItem> UserRoles { get; set; }
    }
}
