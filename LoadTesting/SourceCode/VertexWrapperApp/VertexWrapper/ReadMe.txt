========================================================================
    DYNAMIC LINK LIBRARY : VertexWrapper Project Overview
========================================================================

AppWizard has created this VertexWrapper DLL for you.  

This file contains a summary of what you will find in each of the files that
make up your VertexWrapper application.

VertexWrapper.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

VertexWrapper.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

VertexWrapper.cpp
    This is the main DLL source file.

VertexWrapper.h
    This file contains a class declaration.

AssemblyInfo.cpp
	Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////////////////////////
Invoice Array's

for Customer  ==  1108911

ProductCode[0] = "004006"; 
            ProductCode[1] = "004011" ;
            ProductCode[2] = "042144" ;
            ProductCode[3] = "071046" ;
            ProductCode[4] = "095133" ;
            ProductCode[5] = "111328" ;
            ProductCode[6] = "120097" ;
            ProductCode[7] = "121013" ;
            ProductCode[8] = "121223" ;
            ProductCode[9] ="121241" ;
            ProductCode[10] ="160096" ;
            ProductCode[11] ="160098" ;
            ProductCode[12] = "160099";
            ProductCode[13] ="160100 ";
            ProductCode[14] ="5838699";


            ProductQty[0] = 8;
            ProductQty[1] = 4;
            ProductQty[2] = 18;
            ProductQty[3] = 3;
            ProductQty[4] = 4;
            ProductQty[5] = 2;
            ProductQty[6] = 18;
            ProductQty[7] = 10;
            ProductQty[8] = 12;
            ProductQty[9] = 1;
            ProductQty[10] = 1;
            ProductQty[11] = 3;
            ProductQty[12] = 3;
            ProductQty[13] = 1;
            ProductQty[14] = 1;

             ExtendedAmount[0] =   390.80;
             ExtendedAmount[1] =   212.32;
             ExtendedAmount[2] =    81.00;
             ExtendedAmount[3] =    97.08;
             ExtendedAmount[4] =    64.28;
             ExtendedAmount[5] =    27.72;
             ExtendedAmount[6] =    62.46;
             ExtendedAmount[7] =    35.10;
             ExtendedAmount[8] =    37.32;
             ExtendedAmount[9] =    16.07;
             ExtendedAmount[10] =    4.05 ;
             ExtendedAmount[11] =   12.15 ;
             ExtendedAmount[12] =   12.15 ;
             ExtendedAmount[13] =    4.05 ;
             ExtendedAmount[14] = 42.48;

             ComponentCode[0] = "";
             ComponentCode[1] = "";
             ComponentCode[2] = "";
             ComponentCode[3] = "";
             ComponentCode[4] = "";
             ComponentCode[5] = "";
             ComponentCode[6] = "";
             ComponentCode[7] = "";
             ComponentCode[8] = "";
             ComponentCode[9] = "";
             ComponentCode[10] = "";
             ComponentCode[11] = "";
             ComponentCode[12] = "";
             ComponentCode[13] = "";
             ComponentCode[14] = "";


/////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////
for customer = 7799696

 ProductCode[0] = "B";
            ProductCode[1] = "B";
            ProductCode[2] = "B";
            ProductCode[3] = "B";

            ProductQty[0] = 4;
            ProductQty[1] = 1;
            ProductQty[2] = 1;
            ProductQty[3] = 1;

            ExtendedAmount[0] = 155.64;
            ExtendedAmount[1] = 66.38;
            ExtendedAmount[2] = 146.75;
            ExtendedAmount[3] = 224.96;

			/////////////////////////////////////////////////////////