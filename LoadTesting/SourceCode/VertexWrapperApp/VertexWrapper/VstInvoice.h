#pragma once
using namespace System;

namespace VertexWrapper{
	public ref class VstInvoice
	{
	public :
		String^ InvoiceNumber;
		String^ ProductSetCode;
		array<String^>^ ProductCod;
		String^ ComponentCode;
		unsigned long  GeoCode;
		double ProductQty, ComponentQty, ExtdAmt;
		String^ Customer;
		String^ CustomerClassCode;
		String^ CompCode;
	};
}
