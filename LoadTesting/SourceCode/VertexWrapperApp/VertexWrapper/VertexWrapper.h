// VertexWrapper.h

#pragma once

using namespace System;
using namespace System::Runtime::InteropServices;

namespace VertexWrapper {
	public ref class VertexApi
	{

#define  SERVER         NULL
#define  USER           NULL
#define  PASSWORD       NULL
		//#define  DATASOURCE		"c:\\vertexDB\\"

	public:
		tVstConnHdl      lConnHdl = NULL;
		tVstHdl          lDataHdl = NULL;
		tVstCalcRetCd    lCalcRetCd;
		int              lInvoiceIdx;
		String^ Initialize(bool *ReturnStatus, String^ DataSource);
		String^ SetInvoice(String^ pCompCode,
						   String^ pCustomer,
						   String^ pCustomerClassCode,
						   double pGeoCode,
						   String^ pInvoiceNumber,
						   String^ pProductSetCode,
						   int pItemNumberCount,
						   array<String^>^ pItemNumber,
						   array<String^>^ pProductCode,
						   array<String^>^ pComponentCode,
						   array<int>^ pProductQty,
						   array<double>^ pExtendedAmount,
						   bool *ReturnStatus);
		String^ GetTax(    String^ pInvoiceNumber,
			               double* opInvoiceTax,
			               array<double>^ opItemTax,
			               double* opGrossAmt,
			               bool *ReturnStatus);
		String^ Cleanup(bool *ReturnStatus);
	private:
		char* DATASOURCE;
		bool ComputeTax()
		{
			String^ opStatus;
			bool bRetValue = true;
#ifdef VSTDEMODEBUG

			/* Write out data handle information to a debug text file. */

			if (VstDebugHdl(lDataHdl, "vstdebug.txt", "Before VstCalcTax") == FALSE)
			{
				opStatus = "VstDebugHdl function failed prior to tax calculation.";

				bRetValue = false;
			}

#endif

			/* Perform tax calculations. */

			lCalcRetCd = VstCalcTax(lConnHdl, lDataHdl, NULL, NULL);

			/* Evaluate the calculation return code. */

			switch (lCalcRetCd)
			{
			case eVstCalcRetCdNone:
			case eVstCalcRetCdTdmMtDefault:
				/* Success. */
				break;

			default:
				opStatus = "Tax calculation routine failed.";
				bRetValue = false;
				break;
			}

#ifdef VSTDEMODEBUG

			/* Write out data handle information to a debug text file. */

			if (VstDebugHdl(lDataHdl, "vstdebug.txt", "After VstCalcTax") == FALSE)
			{
				opStatus = "VstDebugHdl function failed after tax calculation.";
			}

#endif
			return bRetValue;
		}

	};
}
