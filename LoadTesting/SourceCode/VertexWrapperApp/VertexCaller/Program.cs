﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using VertexWrapper;
using System.IO;
using System.Runtime.InteropServices;
namespace VertexCaller
{
    class Program
    {
        static void Main(string[] args)
        {
           OrderInvoice objInvoiceObject = new OrderInvoice();
            VertexApi va = new VertexApi();
            int ItemNumberCount = 4;
            string CompCode = "00800";
            string Customer = "7799696";
            string CustomerClassCode = "R";
            string Datasource = "c:\\dfsdf\\";
            double GeoCode = 371090550;
            string InvoiceNumber = "001";
            string ProductSetCode = "";
            string[] ItemNumber = new string[ItemNumberCount];
            string[] ProductCode = new string[ItemNumberCount];
            string[] ComponentCode = new string[ItemNumberCount];
            int[] ProductQty = new int[ItemNumberCount];
            double[] ExtendedAmount = new double[ItemNumberCount];
            double InvoiceTax = 0;
            double GrossAmt = 0;
            double[] ItemTax = new double[ItemNumberCount];
            bool ReturnStatus;
            for (int index = 0; index < ItemNumberCount; index++)
            {
                ItemNumber[index] = "1111" + index;
               
            }

                 unsafe
                {
                    
                   // ComplitionStatus = (sbyte*)CompStatus;
                    va.Initialize(&ReturnStatus, Datasource);
                    va.SetInvoice(CompCode, 
                                      Customer,
                                      CustomerClassCode,
                                      GeoCode,
                                      InvoiceNumber,
                                      ProductSetCode,
                                      ItemNumberCount,
                                      ItemNumber,
                                      ProductCode,
                                      ComponentCode,
                                      ProductQty,
                                      ExtendedAmount,
                                      &ReturnStatus);
                    va.GetTax(InvoiceNumber, &InvoiceTax, ItemTax, &GrossAmt, &ReturnStatus);
                    va.Cleanup(&ReturnStatus);
                    
                }
                Console.WriteLine("GrossAmt : {0}", GrossAmt);
                
                for (int i = 0; i < ItemNumberCount; i++)
                {
                    Console.WriteLine("\n****************\n");
                    Console.WriteLine("ItemNumber : {0}",ItemNumber[i]);
                    Console.WriteLine("ProductCode : {0}", ProductCode[i]);
                    Console.WriteLine("ComponentCode : {0}", ComponentCode[i]);
                    Console.WriteLine("ProductQty : {0}", ProductQty[i]);
                    Console.WriteLine("ExtendedAmount : {0}", ExtendedAmount[i]);
                    Console.WriteLine("ItemTax : {0}", ItemTax[i]);
                    Console.WriteLine("\n****************\n");

                }

                    //IntPtr ptr = new IntPtr();
                    //ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(InvoiceObject)));
                    //Marshal.StructureToPtr(invObj, ptr, false);
                    //Marshal.FreeHGlobal(ptr);
                    Console.ReadKey();
        }
    }
    public class OrderInvoice
    {
        public OrderInvoice()
        {

        }
        private string _InvoiceNumber, _ProductSetCode, _Customer, _CustomerClassCode, _CompCode;
        private long _GeoCode;
        private double _InvoiceTax;
        private double _GrossAmt;
        public List<InvoiceItem> listP = new List<InvoiceItem>();
          
        
        public string InvoiceNumber
        {
            get { return _InvoiceNumber; }
            set { _InvoiceNumber = value; }
        }
        public string ProductSetCode
        {
            get { return _ProductSetCode; }
            set { _ProductSetCode = value; }
        }
        public string Customer
        {
            get { return _Customer; }
            set { _Customer = value; }
        }
        public string CustomerClassCode
        {
            get { return _CustomerClassCode; }
            set { _CustomerClassCode = value; }
        }
        public string CompCode
        {
            get { return _CompCode; }
            set { _CompCode = value; }
        }
        public long GeoCode
        {
            get { return _GeoCode; }
            set { _GeoCode = value; }
        }
        public double InvoiceTax
        {
            get { return _InvoiceTax; }
            set { _InvoiceTax = value; }
        }

        public double GrossAmt
        {
            get { return _GrossAmt; }
            set { _GrossAmt = value; }
        }
        //public Array ItemObjectProp
        //{
        //    get { return _ItemObject; }
        //    set { _ItemObject = value; }
        //}
       
       
    }
    public class InvoiceItem 
    {
        string _ProductCode;
        string _ItemNumber;
        double _ProductQuantity;
        double _ExtendedAmount;
        public InvoiceItem(){

        }
        public InvoiceItem(string productcode, string itemno, double prodqty, double extdAmt){
            this._ProductCode = productcode;
            this._ItemNumber = itemno;
            this._ProductQuantity = prodqty;
            this._ExtendedAmount = extdAmt;
        }

        public string ProductCode
        {
            get;
            set;
        }
        public string ItemNumber
        {
            get;
            set;
        }
        public double ProductQuantity
        {
            get;
            set;
        }
        public double ExtendedAmount
        {
            get;
            set;
          
        }

    }
}
