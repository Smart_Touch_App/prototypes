﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iAnywhere.Data.SQLAnywhere;
using Extensions;

namespace DeploymentLib
{
    public class Deployment
    {
        public static void SetupCDB(string workingDirectory, string CDBSetupCommand, string CDBSetupArguments)
        {
            workingDirectory.ShellExecute(CDBSetupCommand, Console.Out, CDBSetupArguments);
        }

        /// <summary>
        /// Create Remote DB
        /// </summary>
        public static void SetupRDB(string SQLAnywherePath,string remoteDBFilePathName)
        {

            //For DSN Entry
            //string CMD = @"dbdsn ";
            //string Argumants = string.Format(@" -w dsn_remote_{0} -y -c uid=DBA;pwd=sql;dbf={1}\remote.db;server=remote_eng",remoteDBName, remoteDBPath);

            string CMD = @"dbinit ";
            string Argumants = string.Format(@" -b {0}", remoteDBFilePathName);

            SQLAnywherePath.ShellExecute(CMD,Console.Out, Argumants);
            
        }

        public static void RunSQLOnRDB(string SQLAnywherePath, string remoteDBConnectionString, string remoteSetupSQLFileNamePath,string remoteDBScriptFileNames)
        {

            /*
                 "%__SABIN%\dbinit" -b remote.db
                  if errorlevel 1 goto RunTimeError
                  echo Setting Schema by running remote_setup.sql... >> setup.log
                 "%__SABIN%\dbisql" -onerror exit -c "dsn=dsn_remote_%2" read "remote_setup.sql"
            
            */



            var ScriptFileNames = remoteDBScriptFileNames.Split(',');

            foreach (var ScriptFileName in ScriptFileNames)
            {

               string path =  System.IO.Path.Combine(remoteSetupSQLFileNamePath,ScriptFileName);
                
               
                string CMD = @"dbisql ";
                string Argumants = string.Format(@"-onerror continue -c {0} -nogui read {1}", remoteDBConnectionString, path);
                SQLAnywherePath.ShellExecute(CMD, Console.Out, Argumants);
            }





        }


    }
}
