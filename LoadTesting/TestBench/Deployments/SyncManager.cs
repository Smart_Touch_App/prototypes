﻿using iAnywhere.MobiLink.Client;
//using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iAnywhere.MobiLink.Script;
using System.Configuration;
using System.Data;
using iAnywhere.Data.SQLAnywhere;
namespace DeploymentLib
{
    public class SyncManager
    {
        private bool _Started;
        private bool _Connected;


        DBSC_Event ev1;
        DbmlsyncClient _syncClient = null;
        DBSC_StartType dbStartType;
        //UInt32 syncHandle;
        List<UInt32> syncHandles = new List<UInt32>();
        // public SyncUpdateType SyncState { get; set; }

        ~SyncManager()
        {
            ShutDownSyncClient();
        }



        public void ShutDownSyncClient()
        {
            if (_syncClient != null)
            {
                _syncClient.ShutdownServer(DBSC_ShutdownType.DBSC_SHUTDOWN_ON_EMPTY_QUEUE);
                _syncClient.WaitForServerShutdown(10000);
                _syncClient.Disconnect();
                _syncClient.Fini();
                _syncClient = null;
            }
        }

 
        public bool isConnected
        {
            get{return _Connected;}
        }

        public bool isStarted
        {
            get { return _Started; }
        }


        public void StartClient(string sqlAnywherePath, string cmdLine, int clientPort, string serverHost, int serverPort, string user, string password)
        {



            if (_syncClient == null)
            {
                try
                {

                    if (!_Started)
                    {
                        _syncClient = DbmlsyncClient.InstantiateClient();
                        _syncClient.Init();

                        // Setting the "server path" is usually required on Windows
                        // Mobile/CE. In other environments the server path is usually
                        // not required unless you SA install is not in your path or
                        // you have multiple versions of the product installed
                        //_syncClient.SetProperty("server path", "C:\\Program Files\\SQL Anywhere 16\\Bin64");
                        _syncClient.SetProperty("server path", sqlAnywherePath);
                        //SERVER=remote_eng;
                        _syncClient.SetProperty("host", serverHost);
                        _syncClient.SetProperty("port", serverPort.ToString());

                        while (!_Started)
                        {
                            _Started = _syncClient.StartServer(clientPort, cmdLine, DbmlsyncClient.DBSC_INFINITY, out dbStartType);
                        }
                    }
                    while (!_Connected)
                    {
                        _Connected = _syncClient.Connect(serverHost, clientPort, user, password);
                    }

                }
                catch
                {

                    throw;
                }
            }
        }
        /// <summary>
        ///  The name of a synchronization profile defined in the remote database that contains the options for the synchronization
        /// </summary>
        /// <param name="ScriptVersion"></param>
        /// <returns></returns>
        public bool Sync(string synchronizationProfile, uint eventTimeout = 10000)
        {
            try
            {
                var syncHdl = _syncClient.Sync(synchronizationProfile, null);
                while (_syncClient.GetEvent(out ev1, eventTimeout)
                      == DBSC_GetEventRet.DBSC_GETEVENT_OK)
                {
                    if (ev1.hdl == syncHdl)
                    {
                        // Console.WriteLine("Event Type : {0}", ev1.type);
                        if (ev1.type == DBSC_EventType.DBSC_EVENTTYPE_INFO_MSG)
                        {
                            // Console.WriteLine("Info : {0}", ev1.str1);
                        }
                        if (ev1.type == DBSC_EventType.DBSC_EVENTTYPE_SYNC_DONE)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch
            {

                throw;
            }
        }

    }

}

