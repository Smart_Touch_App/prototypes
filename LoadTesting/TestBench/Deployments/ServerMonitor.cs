﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extensions;
using System.Diagnostics;
namespace DeploymentLib
{
   public  class ServerMonitor
    {
       static int ProcessID = 0;
        public static void StartServer(string SQLAnyWherePath,string CDBConnectionString,string serverHost,string serverPort,bool shouldHookDotNetClass,
            string assemblyPath = "", string startClasses = "DRH.SyncHandler", string clrVersion = "v4.0.30319")
        {
            //if (ProcessID != 0)
                //return;

            //mlsrv16 -c "%CONNECTION%" -b -v -zp -x TCPIP(host="localhost";port="2439")

            //Start ML Server
            string StartServerCommand = "mlsrv16";

            if (shouldHookDotNetClass)
            {
                string[] StartServerArguments = {string.Format("-c {0}",CDBConnectionString),
                                             " -b -v -zp -x",
                                             string.Format(@" TCPIP(host='{0}';port='{1}')",
                                                               serverHost, serverPort),
                                             string.Format("-sl dnet (-MLAutoLoadPath={0} -MLStartClasses={1} -clrVersion={2})"
                                                                      , assemblyPath ,startClasses,clrVersion)
                                            };
                ProcessID = SQLAnyWherePath.ShellExecute(StartServerCommand, Console.Out, StartServerArguments);
            }
            else
            {

                string[] StartServerArguments = {string.Format(" -c {0}",CDBConnectionString),
                                             " -b -v -zp -x",
                                             string.Format(@" TCPIP(host='{0}';port='{1}')",
                                                               serverHost, serverPort),
                                            };

                ProcessID = SQLAnyWherePath.ShellExecute(StartServerCommand, Console.Out, StartServerArguments);
            }

            //start "" %_sa_bin%mlsrv16 -c "%CONNECTION%" -v -x TCPIP(host="localhost";port="2439") 




        }

        public static void ShutdownServer()
        {
            //Process p = Process.GetProcessById(ProcessID);
            //p.Kill();
            ProcessID = 0;
        }
 

    }
}
