﻿using System;
using System.IO;
using System.Threading;
using iAnywhere.MobiLink.Script;


namespace DRH
{
    public class SyncHandler
    {
        ServerContext _sc;
        bool _exit_loop;
        Thread _thread;
        DBConnection _conn;

        //DRH.DownloadHandler.HandleDownload
        public SyncHandler(ServerContext sc)
        {
            
            Console.WriteLine("Sync Handler Instantiated...");
            // Perform setup first so that an exception 
            // causes MobiLink startup to fail.
            _sc = sc;

            // Create connection for use later.
            _conn = _sc.MakeConnection();
            _exit_loop = false;
            _thread = new Thread(new ThreadStart(run));
            _thread.IsBackground = true;
            _thread.Start();
        }

        public void run()
        {
            ShutdownCallback callback = new ShutdownCallback(shutdownPerformed);
            _sc.ShutdownListener += callback;

            // run() can't throw exceptions.
            try
            {
                Console.WriteLine("Sync Handler Running...");
                handlerLoop();
                _conn.Close();
                _conn = null;
            }
            catch (Exception e)
            {
                // Print some error output to the MobiLink log.
                Console.Error.Write(e.ToString());

                // There is no need to be notified of shutdown.
                _sc.ShutdownListener -= callback;

                // Ask server to shut down so this fatal error can be fixed.
                _sc.Shutdown();
            }

            // Shortly after return, this thread no longer exists.
            return;
        }

        public void shutdownPerformed(ServerContext sc)
        {
            // Stop the event handler loop.
            try
            {
                _exit_loop = true;
                Console.WriteLine("Sync Handler being stopped in 1 minute...");
                // Wait a maximum of 10 seconds for thread to die.
                _thread.Join(10 * 60000);
            }
            catch (Exception e)
            {
                // Print some error output to the MobiLink log.
                Console.Error.Write(e.ToString());
            }
        }

        private void handlerLoop()
        {
            while (!_exit_loop)
            {
                //Console.WriteLine("Sync Handler in Loop waiting for event...");
                // Handle events in this loop.
                Thread.Sleep(1 * 1000);

                

            }
        }





    }
}
