﻿using System;
using System.Data;
using System.IO;
using iAnywhere.MobiLink.Script;
using iAnywhere.MobiLink;
using iAnywhere.Data.SQLAnywhere;

namespace DRH
{
    /// <summary>
    /// Tests that scripts are called correctly for most sync events.
    /// Following script to be executed at CDB
    /// exec ml_add_dnet_connection_script
    ///   'ver1', 'handle_DownloadData',
    ///   'DRH.DownloadHandler.HandleDownload'

    /// </summary>
    public class DownloadHandler
    {
        private DBConnectionContext _cc;

        public DownloadHandler(DBConnectionContext cc)
        {
            _cc = cc;
        }

        ~DownloadHandler()
        {
        }

        public void HandleDownload()
        {

            Console.WriteLine("Handling Download");

            DataSet ds = new DataSet();

            // Get DownloadData instance for current synchronization.
            DownloadData my_dd = _cc.GetDownloadData();

            // Get a DownloadTableData instance for the remoteOrders table.
            DownloadTableData td = my_dd.GetDownloadTableByName("Customer");

            foreach (var t in my_dd.GetDownloadTables())
            {
                Console.WriteLine(string.Format("Tables in the download: {0} ", t.GetName()));

            }



            using (var connection = new System.Data.SqlClient.SqlConnection(@"Data Source=BIRMIS-PC\SQLSERVER2012;Initial Catalog=NonMobilinkSource;User ID=sa;Password=sa123"))
            {
                connection.Open();
                using (var comm = new System.Data.SqlClient.SqlCommand("SELECT [ID],[Name],[Address] FROM [dbo].[Customer]"
                                                                       , connection))
                {
                    using (var da = new System.Data.SqlClient.SqlDataAdapter(comm))
                    {
                        da.Fill(ds);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            Console.WriteLine(string.Format("Found {0} Rows in Other Source ", ds.Tables[0].Rows.Count));
                    }
                }
            }


            if (td != null)
            {
                Console.WriteLine("Found Download Table");
                // Get an IDbCommand for upsert (update/insert) operations.
                IDbCommand upsert_stmt = td.GetUpsertCommand();

                IDataParameterCollection parameters = upsert_stmt.Parameters;

                Console.WriteLine("Adding Customers from other Source");
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    int ctr = 99;
                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        Console.WriteLine(string.Format("Adding: {0} ", r["Address"].ToString()));

                        // Set values for one row.
                        parameters = upsert_stmt.Parameters;

                        ((IDataParameter)(parameters[0])).Value = Convert.ToInt32(r["ID"]);
                        ((IDataParameter)(parameters[1])).Value = r["Name"].ToString();
                        ((IDataParameter)(parameters[2])).Value = r["Address"].ToString();

                        // Add the values to the download.
                        int update_result = upsert_stmt.ExecuteNonQuery();
                    }
                }
            }
            Console.WriteLine("Data Added from other Source");
            //// Set values for one row.
            //parameters[0] = 2300;
            //parameters[1] = 100;


            //// Set values for another row.
            //parameters[0] = 2301;
            //parameters[1] = 50;
            //update_result = upsert_stmt.ExecuteNonQuery();

            // ...
        }

    }
}