﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;


namespace DeployementCenter
{
    public partial class Form1 : Form
    {
        public bool EventExecuted;


        DeploymentLib.SyncManager man = new DeploymentLib.SyncManager();

        string RemoteDBFileNamePath = @"D:\Arvind\TestBench\REmoteDBs\User1\Test1.db";
        string RemoteDBSetupScriptsPath = @"D:\Arvind\TestBench\REmoteDBs\REmoteDBSetupScripts\";
        string RemoteDBSCriptFileNames = "remote_setup.sql,CustomizeRemote.sql";
        string AssemblyPath = @"D:\Arvind\TestBench\DRHAssembly";


        public Form1()
        {
            InitializeComponent();
            SetDefaults();

            CreateRDBs();
            while (true)
            {
                System.Threading.Thread.Sleep(10000);

            }





        }


        private void CreateRDBs()
        {
           

            Parallel.For(41,51, (ctr) =>
                {

                    string RDBDir = string.Format(@"D:\Arvind\TestBench\REmoteDBs\User{0}",ctr);
                    if (!System.IO.Directory.Exists(RDBDir))
                        System.IO.Directory.CreateDirectory(RDBDir);

                    var RDBFilePath = string.Format(@"D:\Arvind\TestBench\REmoteDBs\User{0}\Test{0}.db", ctr);
                    string rdbcon = Configurations.BuildConnection(RDBFilePath, "dba", "sql");
                    string cmdline = string.Format(@"-c {0}", rdbcon);

                    if (!System.IO.File.Exists(RDBFilePath))
                    {
                        DeploymentLib.Deployment.SetupRDB(Configurations.SQLAny16, RDBFilePath);
                        DeploymentLib.Deployment.RunSQLOnRDB(Configurations.SQLAny16,
                                                             rdbcon,
                                                             @"D:\Arvind\TestBench\REmoteDBs\REmoteDBSetupScripts\",
                                                             "remote_setup.sql,CustomizeRemote.sql");

                    }


                    DeploymentLib.SyncManager man = new DeploymentLib.SyncManager();
                    man.StartClient(Configurations.SQLAny16,
                                    cmdline,
                                    3425 + ctr,
                                    "localhost", 2439,
                                    "dba", "sql");
                    man.Sync("prof_everything");

                });




        }






        private void SetDefaults(bool initialize = false)
        {

            txtSetUpScriptsFilePath.Text = RemoteDBSetupScriptsPath;

            txtScripFileNames.Text = RemoteDBSCriptFileNames;

            if (string.IsNullOrEmpty(txtServerPath.Text))
                txtServerPath.Text = Configurations.SQLAny16;


            if (!chkDRHConfigs.Checked)
            {
                if (string.IsNullOrEmpty(txtRDBFilePath.Text) || initialize)
                    txtRDBFilePath.Text = Configurations.RDBFilePath;


                if (string.IsNullOrEmpty(txtUser.Text) || initialize)
                    txtUser.Text = "dba";

                if (string.IsNullOrEmpty(txtPassword.Text) || initialize)
                    txtPassword.Text = "sql";

                if (string.IsNullOrEmpty(txtSynchronizationProfile.Text) || initialize)
                    txtSynchronizationProfile.Text = Configurations.SynchronizationProfile;
                if (string.IsNullOrEmpty(txtCDBConnection.Text) || initialize)
                    txtCDBConnection.Text = Configurations.CDBConnection;


            }
            else
            {
                if (string.IsNullOrEmpty(txtRDBFilePath.Text) || initialize)
                    txtRDBFilePath.Text = @"D:\Arvind\Projects\Wonderbiz\MLProject_SalesExpress\SLE\Remote_SLE_CDB_deploy\Remote_SLE_CDB_remote.db";


                if (string.IsNullOrEmpty(txtUser.Text) || initialize)
                    txtUser.Text = "dba";

                if (string.IsNullOrEmpty(txtPassword.Text) || initialize)
                    txtPassword.Text = "sql";


                if (string.IsNullOrEmpty(txtSynchronizationProfile.Text) || initialize)
                    txtSynchronizationProfile.Text = "Remote_SLE_CDB_DBA";

                if (string.IsNullOrEmpty(txtCDBConnection.Text) || initialize)
                    txtCDBConnection.Text = "DSN=sysSLE_CDB;UID=sa;PWD=sa123";


            }

            //txtCDBConnection.Text = Configurations.BuildConnection(txtRDBFilePath.Text, txtUser.Text, txtPassword.Text);
            txtCMDLine.Text = string.Format(@"-c {0}", Configurations.BuildConnection(txtRDBFilePath.Text, txtUser.Text, txtPassword.Text));

        }

        private void btnDeploy_Click(object sender, EventArgs e)
        {


        }

        private void btnShutdown_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnStartServer_Click(object sender, EventArgs e)
        {
            var MLServer = Task.Factory.StartNew(() =>
            {
                DeploymentLib.ServerMonitor.StartServer(Configurations.SQLAny16, txtCDBConnection.Text,
                                                        Configurations.ServerHost, Configurations.ServerPort, chkHookDotNetClass.Checked, AssemblyPath, "DRH.SyncHandler", "v4.0.30319");

            });

        }

        private void btnShutdownServer_Click(object sender, EventArgs e)
        {
            DeploymentLib.ServerMonitor.ShutdownServer();
        }


        private void btnStartTwoClients_Click(object sender, EventArgs e)
        {

            Task v1 = Task.Factory.StartNew(() =>
            {
                DeploymentLib.SyncManager man1 = new DeploymentLib.SyncManager();
                man1.StartClient(
                                 @"C:\Program Files\SQL Anywhere 16\Bin64\",
                                 @"-c DBF=D:\Arvind\TestBench\REmoteDBs\Test1.db;UID=dba;PWD=sql",
                                 3426, "localhost", 2439,
                                 "dba",
                                 "sql"

                                 );
                man1.Sync(txtSynchronizationProfile.Text);

            });


            Task v2 = Task.Factory.StartNew(() =>
            {
                DeploymentLib.SyncManager man2 = new DeploymentLib.SyncManager();
                man2.StartClient(
                                 @"C:\Program Files\SQL Anywhere 16\Bin64\",
                                 @"-c DBF=D:\Arvind\TestBench\REmoteDBs\Test2.db;UID=dba;PWD=sql",
                                 3427, "localhost", 2439,
                                 "dba",
                                 "sql"

                    );
                man2.Sync(txtSynchronizationProfile.Text);
            });

            Task.WaitAll(v1, v2);

        }


        private void btnDeploy_Click_1(object sender, EventArgs e)
        {
            //DRH.SyncManager.Test();

            man.StartClient(txtServerPath.Text, txtCMDLine.Text,
                3426, "localhost", 2439,
                txtUser.Text, txtPassword.Text);
            man.Sync(txtSynchronizationProfile.Text);


        }

        private void btnShutdown_Click_1(object sender, EventArgs e)
        {
            man.ShutDownSyncClient();
        }

        private void tabMobilinkServer_Click(object sender, EventArgs e)
        {

        }

        private void btnRefreshConfigs_Click(object sender, EventArgs e)
        {
            SetDefaults();
        }

        private void btnCreateRDB_Click(object sender, EventArgs e)
        {
            DeploymentLib.Deployment.SetupRDB(Configurations.SQLAny16, txtRDBFilePath.Text);


            DeploymentLib.Deployment.RunSQLOnRDB(Configurations.SQLAny16,
                                                 Configurations.BuildConnection(txtRDBFilePath.Text, txtUser.Text,
                                                                                txtPassword.Text),
                                                                                txtSetUpScriptsFilePath.Text, txtScripFileNames.Text);

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void chkHookDotNetClass_CheckedChanged(object sender, EventArgs e)
        {
            SetDefaults(true);
        }

        private void chkDRHConfigs_CheckedChanged(object sender, EventArgs e)
        {
            SetDefaults(true);
        }


    }
}
