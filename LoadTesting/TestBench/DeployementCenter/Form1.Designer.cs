﻿namespace DeployementCenter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabServer = new System.Windows.Forms.TabControl();
            this.tabDeployment = new System.Windows.Forms.TabPage();
            this.btnStartTwoClients = new System.Windows.Forms.Button();
            this.tabClientSynchronization = new System.Windows.Forms.TabPage();
            this.txtScripFileNames = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSetUpScriptsFilePath = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkDRHConfigs = new System.Windows.Forms.CheckBox();
            this.btnCreateRDB = new System.Windows.Forms.Button();
            this.btnRefreshConfigs = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCMDLine = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSynchronizationProfile = new System.Windows.Forms.TextBox();
            this.btnShutdown = new System.Windows.Forms.Button();
            this.txtRDBFilePath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtServerPath = new System.Windows.Forms.TextBox();
            this.btnDeploy = new System.Windows.Forms.Button();
            this.tabMobilinkServer = new System.Windows.Forms.TabPage();
            this.chkHookDotNetClass = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCDBConnection = new System.Windows.Forms.TextBox();
            this.btnShutdownServer = new System.Windows.Forms.Button();
            this.btnStartServer = new System.Windows.Forms.Button();
            this.tabServer.SuspendLayout();
            this.tabDeployment.SuspendLayout();
            this.tabClientSynchronization.SuspendLayout();
            this.tabMobilinkServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabServer
            // 
            this.tabServer.Controls.Add(this.tabDeployment);
            this.tabServer.Controls.Add(this.tabClientSynchronization);
            this.tabServer.Controls.Add(this.tabMobilinkServer);
            this.tabServer.Location = new System.Drawing.Point(12, 12);
            this.tabServer.Name = "tabServer";
            this.tabServer.SelectedIndex = 0;
            this.tabServer.Size = new System.Drawing.Size(825, 476);
            this.tabServer.TabIndex = 16;
            // 
            // tabDeployment
            // 
            this.tabDeployment.Controls.Add(this.btnStartTwoClients);
            this.tabDeployment.Location = new System.Drawing.Point(4, 22);
            this.tabDeployment.Name = "tabDeployment";
            this.tabDeployment.Padding = new System.Windows.Forms.Padding(3);
            this.tabDeployment.Size = new System.Drawing.Size(817, 450);
            this.tabDeployment.TabIndex = 0;
            this.tabDeployment.Text = "tabDeployments";
            this.tabDeployment.UseVisualStyleBackColor = true;
            // 
            // btnStartTwoClients
            // 
            this.btnStartTwoClients.Location = new System.Drawing.Point(131, 54);
            this.btnStartTwoClients.Name = "btnStartTwoClients";
            this.btnStartTwoClients.Size = new System.Drawing.Size(108, 23);
            this.btnStartTwoClients.TabIndex = 0;
            this.btnStartTwoClients.Text = "Start Two server";
            this.btnStartTwoClients.UseVisualStyleBackColor = true;
            this.btnStartTwoClients.Click += new System.EventHandler(this.btnStartTwoClients_Click);
            // 
            // tabClientSynchronization
            // 
            this.tabClientSynchronization.Controls.Add(this.txtScripFileNames);
            this.tabClientSynchronization.Controls.Add(this.label9);
            this.tabClientSynchronization.Controls.Add(this.txtSetUpScriptsFilePath);
            this.tabClientSynchronization.Controls.Add(this.label8);
            this.tabClientSynchronization.Controls.Add(this.chkDRHConfigs);
            this.tabClientSynchronization.Controls.Add(this.btnCreateRDB);
            this.tabClientSynchronization.Controls.Add(this.btnRefreshConfigs);
            this.tabClientSynchronization.Controls.Add(this.txtPassword);
            this.tabClientSynchronization.Controls.Add(this.label7);
            this.tabClientSynchronization.Controls.Add(this.txtUser);
            this.tabClientSynchronization.Controls.Add(this.label6);
            this.tabClientSynchronization.Controls.Add(this.txtCMDLine);
            this.tabClientSynchronization.Controls.Add(this.label4);
            this.tabClientSynchronization.Controls.Add(this.label3);
            this.tabClientSynchronization.Controls.Add(this.txtSynchronizationProfile);
            this.tabClientSynchronization.Controls.Add(this.btnShutdown);
            this.tabClientSynchronization.Controls.Add(this.txtRDBFilePath);
            this.tabClientSynchronization.Controls.Add(this.label2);
            this.tabClientSynchronization.Controls.Add(this.label1);
            this.tabClientSynchronization.Controls.Add(this.txtServerPath);
            this.tabClientSynchronization.Controls.Add(this.btnDeploy);
            this.tabClientSynchronization.Location = new System.Drawing.Point(4, 22);
            this.tabClientSynchronization.Name = "tabClientSynchronization";
            this.tabClientSynchronization.Padding = new System.Windows.Forms.Padding(3);
            this.tabClientSynchronization.Size = new System.Drawing.Size(817, 450);
            this.tabClientSynchronization.TabIndex = 1;
            this.tabClientSynchronization.Text = "Client Synchronization";
            this.tabClientSynchronization.UseVisualStyleBackColor = true;
            // 
            // txtScripFileNames
            // 
            this.txtScripFileNames.Location = new System.Drawing.Point(188, 139);
            this.txtScripFileNames.Name = "txtScripFileNames";
            this.txtScripFileNames.Size = new System.Drawing.Size(623, 20);
            this.txtScripFileNames.TabIndex = 36;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(88, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 35;
            this.label9.Text = "Scripts File Names";
            // 
            // txtSetUpScriptsFilePath
            // 
            this.txtSetUpScriptsFilePath.Location = new System.Drawing.Point(188, 116);
            this.txtSetUpScriptsFilePath.Name = "txtSetUpScriptsFilePath";
            this.txtSetUpScriptsFilePath.Size = new System.Drawing.Size(623, 20);
            this.txtSetUpScriptsFilePath.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(67, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Set Up Scripts File Path";
            // 
            // chkDRHConfigs
            // 
            this.chkDRHConfigs.AutoSize = true;
            this.chkDRHConfigs.Location = new System.Drawing.Point(32, 22);
            this.chkDRHConfigs.Name = "chkDRHConfigs";
            this.chkDRHConfigs.Size = new System.Drawing.Size(115, 17);
            this.chkDRHConfigs.TabIndex = 32;
            this.chkDRHConfigs.Text = "Load DRH Configs";
            this.chkDRHConfigs.UseVisualStyleBackColor = true;
            this.chkDRHConfigs.CheckedChanged += new System.EventHandler(this.chkDRHConfigs_CheckedChanged);
            // 
            // btnCreateRDB
            // 
            this.btnCreateRDB.Location = new System.Drawing.Point(645, 263);
            this.btnCreateRDB.Name = "btnCreateRDB";
            this.btnCreateRDB.Size = new System.Drawing.Size(126, 23);
            this.btnCreateRDB.TabIndex = 31;
            this.btnCreateRDB.Text = "Create RDB";
            this.btnCreateRDB.UseVisualStyleBackColor = true;
            this.btnCreateRDB.Click += new System.EventHandler(this.btnCreateRDB_Click);
            // 
            // btnRefreshConfigs
            // 
            this.btnRefreshConfigs.Location = new System.Drawing.Point(650, 22);
            this.btnRefreshConfigs.Name = "btnRefreshConfigs";
            this.btnRefreshConfigs.Size = new System.Drawing.Size(126, 23);
            this.btnRefreshConfigs.TabIndex = 30;
            this.btnRefreshConfigs.Text = "Refresh Configs";
            this.btnRefreshConfigs.UseVisualStyleBackColor = true;
            this.btnRefreshConfigs.Click += new System.EventHandler(this.btnRefreshConfigs_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(426, 162);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(172, 20);
            this.txtPassword.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(367, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Password";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(187, 162);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(172, 20);
            this.txtUser.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(136, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "UserID";
            // 
            // txtCMDLine
            // 
            this.txtCMDLine.Location = new System.Drawing.Point(139, 301);
            this.txtCMDLine.Name = "txtCMDLine";
            this.txtCMDLine.Size = new System.Drawing.Size(649, 20);
            this.txtCMDLine.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 301);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Command Line";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 341);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Synchronization Profile";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtSynchronizationProfile
            // 
            this.txtSynchronizationProfile.Location = new System.Drawing.Point(139, 338);
            this.txtSynchronizationProfile.Name = "txtSynchronizationProfile";
            this.txtSynchronizationProfile.Size = new System.Drawing.Size(649, 20);
            this.txtSynchronizationProfile.TabIndex = 22;
            // 
            // btnShutdown
            // 
            this.btnShutdown.Location = new System.Drawing.Point(447, 381);
            this.btnShutdown.Name = "btnShutdown";
            this.btnShutdown.Size = new System.Drawing.Size(75, 23);
            this.btnShutdown.TabIndex = 21;
            this.btnShutdown.Text = "ShutDown client";
            this.btnShutdown.UseVisualStyleBackColor = true;
            this.btnShutdown.Click += new System.EventHandler(this.btnShutdown_Click_1);
            // 
            // txtRDBFilePath
            // 
            this.txtRDBFilePath.Location = new System.Drawing.Point(139, 88);
            this.txtRDBFilePath.Name = "txtRDBFilePath";
            this.txtRDBFilePath.Size = new System.Drawing.Size(649, 20);
            this.txtRDBFilePath.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "RDB File Path";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Server Path";
            // 
            // txtServerPath
            // 
            this.txtServerPath.Location = new System.Drawing.Point(139, 61);
            this.txtServerPath.Name = "txtServerPath";
            this.txtServerPath.ShortcutsEnabled = false;
            this.txtServerPath.Size = new System.Drawing.Size(649, 20);
            this.txtServerPath.TabIndex = 17;
            // 
            // btnDeploy
            // 
            this.btnDeploy.Location = new System.Drawing.Point(355, 381);
            this.btnDeploy.Name = "btnDeploy";
            this.btnDeploy.Size = new System.Drawing.Size(75, 23);
            this.btnDeploy.TabIndex = 16;
            this.btnDeploy.Text = "Synchronize";
            this.btnDeploy.UseVisualStyleBackColor = true;
            this.btnDeploy.Click += new System.EventHandler(this.btnDeploy_Click_1);
            // 
            // tabMobilinkServer
            // 
            this.tabMobilinkServer.Controls.Add(this.chkHookDotNetClass);
            this.tabMobilinkServer.Controls.Add(this.label5);
            this.tabMobilinkServer.Controls.Add(this.txtCDBConnection);
            this.tabMobilinkServer.Controls.Add(this.btnShutdownServer);
            this.tabMobilinkServer.Controls.Add(this.btnStartServer);
            this.tabMobilinkServer.Location = new System.Drawing.Point(4, 22);
            this.tabMobilinkServer.Name = "tabMobilinkServer";
            this.tabMobilinkServer.Padding = new System.Windows.Forms.Padding(3);
            this.tabMobilinkServer.Size = new System.Drawing.Size(817, 450);
            this.tabMobilinkServer.TabIndex = 2;
            this.tabMobilinkServer.Text = "Server";
            this.tabMobilinkServer.UseVisualStyleBackColor = true;
            this.tabMobilinkServer.Click += new System.EventHandler(this.tabMobilinkServer_Click);
            // 
            // chkHookDotNetClass
            // 
            this.chkHookDotNetClass.AutoSize = true;
            this.chkHookDotNetClass.Location = new System.Drawing.Point(139, 84);
            this.chkHookDotNetClass.Name = "chkHookDotNetClass";
            this.chkHookDotNetClass.Size = new System.Drawing.Size(103, 17);
            this.chkHookDotNetClass.TabIndex = 21;
            this.chkHookDotNetClass.Text = "Hook .Net Class";
            this.chkHookDotNetClass.UseVisualStyleBackColor = true;
            this.chkHookDotNetClass.CheckedChanged += new System.EventHandler(this.chkHookDotNetClass_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "CDB Connection";
            // 
            // txtCDBConnection
            // 
            this.txtCDBConnection.Location = new System.Drawing.Point(139, 38);
            this.txtCDBConnection.Name = "txtCDBConnection";
            this.txtCDBConnection.ShortcutsEnabled = false;
            this.txtCDBConnection.Size = new System.Drawing.Size(649, 20);
            this.txtCDBConnection.TabIndex = 19;
            // 
            // btnShutdownServer
            // 
            this.btnShutdownServer.Location = new System.Drawing.Point(562, 163);
            this.btnShutdownServer.Name = "btnShutdownServer";
            this.btnShutdownServer.Size = new System.Drawing.Size(193, 23);
            this.btnShutdownServer.TabIndex = 1;
            this.btnShutdownServer.Text = "Shutdown Server";
            this.btnShutdownServer.UseVisualStyleBackColor = true;
            this.btnShutdownServer.Click += new System.EventHandler(this.btnShutdownServer_Click);
            // 
            // btnStartServer
            // 
            this.btnStartServer.Location = new System.Drawing.Point(334, 163);
            this.btnStartServer.Name = "btnStartServer";
            this.btnStartServer.Size = new System.Drawing.Size(193, 23);
            this.btnStartServer.TabIndex = 0;
            this.btnStartServer.Text = "Start Server";
            this.btnStartServer.UseVisualStyleBackColor = true;
            this.btnStartServer.Click += new System.EventHandler(this.btnStartServer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 486);
            this.Controls.Add(this.tabServer);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabServer.ResumeLayout(false);
            this.tabDeployment.ResumeLayout(false);
            this.tabClientSynchronization.ResumeLayout(false);
            this.tabClientSynchronization.PerformLayout();
            this.tabMobilinkServer.ResumeLayout(false);
            this.tabMobilinkServer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabServer;
        private System.Windows.Forms.TabPage tabDeployment;
        private System.Windows.Forms.TabPage tabClientSynchronization;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCMDLine;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSynchronizationProfile;
        private System.Windows.Forms.Button btnShutdown;
        private System.Windows.Forms.TextBox txtRDBFilePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServerPath;
        private System.Windows.Forms.Button btnDeploy;
        private System.Windows.Forms.TabPage tabMobilinkServer;
        private System.Windows.Forms.Button btnStartServer;
        private System.Windows.Forms.Button btnShutdownServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCDBConnection;
        private System.Windows.Forms.Button btnRefreshConfigs;
        private System.Windows.Forms.Button btnCreateRDB;
        private System.Windows.Forms.CheckBox chkHookDotNetClass;
        private System.Windows.Forms.CheckBox chkDRHConfigs;
        private System.Windows.Forms.TextBox txtScripFileNames;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSetUpScriptsFilePath;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnStartTwoClients;

    }
}

