﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensions
{

    public static class ShellExtension
    {
        //Usage:  @"E:\Temp\MyWorkingDirectory".ShellExecute(@"C:\Program Files\Microsoft SDKs\Windows\v6.0A\Bin\svcutil.exe", Console.Out);

        public static int ShellExecute(this string workingDirectory, string command, TextWriter writer, params string[] arguments)
        {
            int ProcessID = 0;
           
            var processStartInfo = new ProcessStartInfo
            {
                WorkingDirectory = workingDirectory,
                FileName = command,
                Arguments = string.Join(" ", arguments),
                //UserName = "Birmis",

                LoadUserProfile = true,
                CreateNoWindow = false,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
            using (var process = Process.Start(processStartInfo))
            {
                ProcessID = process.Id;
                using (process.StandardOutput)
                {
                    try
                    {
                        writer.WriteLine(process.StandardOutput.ReadToEnd());
                    }
                    catch (Exception ex)
                    {

                    }
                }
                using (process.StandardError)
                {
                    try
                    {
                        writer.WriteLine(process.StandardError.ReadToEnd());
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }

            return ProcessID;
        }

    }
}

