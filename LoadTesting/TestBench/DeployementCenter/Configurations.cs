﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeployementCenter
{
    public class Configurations
    {

        //For MobileDataModel 
        static string _CDBConnection = "DSN=MSDEVDB01";  //;UID=sa;PWD=sa123";

        //For SLE_CDB (Mutiple DB Demo)  
        //static string _CDBConnection = "DSN=SYS_SLE_CDB;UID=sa;PWD=sa123";


        static string _RDBFilePath = @"D:\Arvind\TestBench\REmoteDBs\Test1.db";

        //For SLE_CDB (Mutiple DB Demo) 
        //static string _SynchronizationProfile = "Remote_SLE_CDB_DBA";

        //For MobileDataModel
        static string _SynchronizationProfile = "prof_everything"; //"prof_validate_user";

        static string _SQLAny16 = System.Environment.GetEnvironmentVariable("SQLAny16") + @"\Bin64\";
        



        public static string _ServerHost = "localhost";//"192.168.1.109";
        static string _ServerPort = "2439";

        public static string BuildConnection(string RemoteDBFilePathNme,string userID = "dba",string password="sql")
        {
            string c = string.Format(@"DBF={0};UID={1};PWD={2}", RemoteDBFilePathNme,userID,password);
            return c;
        }

        public static string SynchronizationProfile
        {

            get { return _SynchronizationProfile; }

            set { _SynchronizationProfile = value; }

        }




        public static string ServerHost
        {

            get { return _ServerHost; }

            set { _ServerHost = value; }

        }

        public static string ServerPort
        {

            get { return _ServerPort; }

            set { _ServerPort = value; }

        }

        public static string SQLAny16
        {

            get { return _SQLAny16; }

            set { _SQLAny16 = value; }

        }

        public static string RDBFilePath
        {

            get { return _RDBFilePath; }

            set { _RDBFilePath = value; }

        }

        public static string CDBConnection
        {

            get { return _CDBConnection; }

            set { _CDBConnection = value; }

        }




        //public static string SQLFilePath
        //{

        //    get { return _SQLFilePath; }

        //    set { _SQLFilePath = value; }

        //}

        //public static string RDBConnection
        //{

        //    get { return _RDBConnection; }

        //    set { _RDBConnection = value; }

        //}




        //public static string CDBSetupCommand
        //{

        //    get { return _CDBSetupCommand; }

        //    set { _CDBSetupCommand = value; }

        //}
        //public static string[] CDBSetupArguments
        //{

        //    get { return _Arguments; }

        //    set { _Arguments = value; }

        //}
    }
}
