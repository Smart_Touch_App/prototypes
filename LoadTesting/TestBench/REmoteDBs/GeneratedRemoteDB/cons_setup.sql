/*------------------------------------------------------------------------------
* ML Install Script generated 2015-04-13 10:14:48 for Microsoft SQL Server (Consolidated)
*                            by MobiLink 16 Plug-in
*-----------------------------------------------------------------------------*/

SET XACT_ABORT OFF
GO
BEGIN TRANSACTION
GO
SET NOCOUNT ON
GO
SET quoted_identifier ON
GO

BEGIN
DECLARE @action varchar(32)
EXEC ml_model_begin_install 'SLE_RemoteDB';

/*------------------------------------------------------------------------------
* Creating schema related to the table 'MobileDataModel.BUSDTA.Device_Master'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.Device_Master_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Device_Master_del', 'Device_Master_del', '6900624f4dd1d734ae628ad5dc4ffdd6'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Device_Master_del', 'Device_Master_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'Device_Master_del', 'Device_Master_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."Device_Master_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Device_Master_del', 'Device_Master_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."Device_Master_del" (
		"Device_Id" varchar(30) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("Device_Id")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'Device_Master_del', 'Device_Master_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."Device_Master_del"', '6900624f4dd1d734ae628ad5dc4ffdd6', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Device_Master', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Device_Master', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'Device_Master', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."Device_Master" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Device_Master', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."Device_Master" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'Device_Master', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."Device_Master" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'Device_Master_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_ins', 'c4a5bfffe8cddc9d1492d70873b6ff4f';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_ins'
	DROP TRIGGER "BUSDTA"."Device_Master_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Device_Master_ins"
		ON "MobileDataModel"."BUSDTA"."Device_Master" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."Device_Master_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."Device_Master_del"."Device_Id" = inserted."Device_Id"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_ins', 'DROP TRIGGER "BUSDTA"."Device_Master_ins"', 'c4a5bfffe8cddc9d1492d70873b6ff4f', 0
END;
/* Create the timestamp trigger 'Device_Master_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_upd', '68e9d4d232f1ce9bea7953919114b731';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_upd'
	DROP TRIGGER "BUSDTA"."Device_Master_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Device_Master_upd"
		ON "MobileDataModel"."BUSDTA"."Device_Master" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."Device_Master"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."Device_Master"."Device_Id" = inserted."Device_Id";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_upd', 'DROP TRIGGER "BUSDTA"."Device_Master_upd"', '68e9d4d232f1ce9bea7953919114b731', 0
END;
/* Create the shadow delete trigger 'Device_Master_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_dlt', 'c2d5cce1be33975dce6cba846eda4907';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_dlt'
	DROP TRIGGER "BUSDTA"."Device_Master_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Device_Master_dlt"
		ON "MobileDataModel"."BUSDTA"."Device_Master" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."Device_Master_del" ( "Device_Id", "last_modified" )
			SELECT deleted."Device_Id",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Device_Master', 'Device_Master_dlt', 'DROP TRIGGER "BUSDTA"."Device_Master_dlt"', 'c2d5cce1be33975dce6cba846eda4907', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Device_Master', 'Device_Master_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Device_Master', 'Device_Master_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Device_Master', 'Device_Master_ml'
	DROP INDEX "Device_Master"."Device_Master_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Device_Master', 'Device_Master_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Device_Master_ml" ON "MobileDataModel"."BUSDTA"."Device_Master"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Device_Master'', ''Device_Master_ml'', ''DROP INDEX "Device_Master"."Device_Master_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Device_Master_del', 'Device_Master_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Device_Master_del', 'Device_Master_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Device_Master_del', 'Device_Master_mld'
	DROP INDEX "Device_Master_del"."Device_Master_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Device_Master_del', 'Device_Master_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Device_Master_mld" ON "MobileDataModel"."BUSDTA"."Device_Master_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Device_Master_del'', ''Device_Master_mld'', ''DROP INDEX "Device_Master_del"."Device_Master_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0004'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F0004_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0004_del', 'F0004_del', 'a46a853117fd97c4676ab7a1e8b63039'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0004_del', 'F0004_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F0004_del', 'F0004_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F0004_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0004_del', 'F0004_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F0004_del" (
		"DTSY" nchar(4) not null,
		"DTRT" nchar(2) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("DTSY", "DTRT")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F0004_del', 'F0004_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F0004_del"', 'a46a853117fd97c4676ab7a1e8b63039', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0004', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0004', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F0004', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0004" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0004', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0004" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F0004', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F0004" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F0004_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_ins', '6eb6324123942a395de26621faf7557e';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_ins'
	DROP TRIGGER "BUSDTA"."F0004_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0004_ins"
		ON "MobileDataModel"."BUSDTA"."F0004" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F0004_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F0004_del"."DTSY" = inserted."DTSY" AND
					"MobileDataModel"."BUSDTA"."F0004_del"."DTRT" = inserted."DTRT"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_ins', 'DROP TRIGGER "BUSDTA"."F0004_ins"', '6eb6324123942a395de26621faf7557e', 0
END;
/* Create the timestamp trigger 'F0004_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_upd', '3090d44de5de4a37a0f14d2cf4ed556a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_upd'
	DROP TRIGGER "BUSDTA"."F0004_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0004_upd"
		ON "MobileDataModel"."BUSDTA"."F0004" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F0004"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F0004"."DTSY" = inserted."DTSY" AND
				"MobileDataModel"."BUSDTA"."F0004"."DTRT" = inserted."DTRT";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_upd', 'DROP TRIGGER "BUSDTA"."F0004_upd"', '3090d44de5de4a37a0f14d2cf4ed556a', 0
END;
/* Create the shadow delete trigger 'F0004_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_dlt', 'ffe373a5f29a0030958c2c09f3945a92';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_dlt'
	DROP TRIGGER "BUSDTA"."F0004_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0004_dlt"
		ON "MobileDataModel"."BUSDTA"."F0004" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F0004_del" ( "DTSY", "DTRT", "last_modified" )
			SELECT deleted."DTSY",
				deleted."DTRT",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0004', 'F0004_dlt', 'DROP TRIGGER "BUSDTA"."F0004_dlt"', 'ffe373a5f29a0030958c2c09f3945a92', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0004', 'F0004_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0004', 'F0004_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0004', 'F0004_ml'
	DROP INDEX "F0004"."F0004_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0004', 'F0004_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0004_ml" ON "MobileDataModel"."BUSDTA"."F0004"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0004'', ''F0004_ml'', ''DROP INDEX "F0004"."F0004_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0004_del', 'F0004_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0004_del', 'F0004_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0004_del', 'F0004_mld'
	DROP INDEX "F0004_del"."F0004_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0004_del', 'F0004_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0004_mld" ON "MobileDataModel"."BUSDTA"."F0004_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0004_del'', ''F0004_mld'', ''DROP INDEX "F0004_del"."F0004_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0005'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F0005_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0005_del', 'F0005_del', '9744188bf35c642e15e03d2bf12de9eb'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0005_del', 'F0005_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F0005_del', 'F0005_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F0005_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0005_del', 'F0005_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F0005_del" (
		"DRSY" nchar(4) not null,
		"DRRT" nchar(2) not null,
		"DRKY" nchar(10) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("DRSY", "DRRT", "DRKY")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F0005_del', 'F0005_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F0005_del"', '9744188bf35c642e15e03d2bf12de9eb', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0005', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0005', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F0005', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0005" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0005', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0005" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F0005', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F0005" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F0005_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_ins', 'a03cb46f09394da8b09f8ada55df2164';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_ins'
	DROP TRIGGER "BUSDTA"."F0005_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0005_ins"
		ON "MobileDataModel"."BUSDTA"."F0005" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F0005_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F0005_del"."DRSY" = inserted."DRSY" AND
					"MobileDataModel"."BUSDTA"."F0005_del"."DRRT" = inserted."DRRT" AND
					"MobileDataModel"."BUSDTA"."F0005_del"."DRKY" = inserted."DRKY"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_ins', 'DROP TRIGGER "BUSDTA"."F0005_ins"', 'a03cb46f09394da8b09f8ada55df2164', 0
END;
/* Create the timestamp trigger 'F0005_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_upd', '937eeafcb3d1652ed55a3396da0b510c';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_upd'
	DROP TRIGGER "BUSDTA"."F0005_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0005_upd"
		ON "MobileDataModel"."BUSDTA"."F0005" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F0005"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F0005"."DRSY" = inserted."DRSY" AND
				"MobileDataModel"."BUSDTA"."F0005"."DRRT" = inserted."DRRT" AND
				"MobileDataModel"."BUSDTA"."F0005"."DRKY" = inserted."DRKY";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_upd', 'DROP TRIGGER "BUSDTA"."F0005_upd"', '937eeafcb3d1652ed55a3396da0b510c', 0
END;
/* Create the shadow delete trigger 'F0005_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_dlt', '2e56ffdef8c76fc8a969c21a50578cf3';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_dlt'
	DROP TRIGGER "BUSDTA"."F0005_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0005_dlt"
		ON "MobileDataModel"."BUSDTA"."F0005" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F0005_del" ( "DRSY", "DRRT", "DRKY", "last_modified" )
			SELECT deleted."DRSY",
				deleted."DRRT",
				deleted."DRKY",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0005', 'F0005_dlt', 'DROP TRIGGER "BUSDTA"."F0005_dlt"', '2e56ffdef8c76fc8a969c21a50578cf3', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0005', 'F0005_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0005', 'F0005_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0005', 'F0005_ml'
	DROP INDEX "F0005"."F0005_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0005', 'F0005_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0005_ml" ON "MobileDataModel"."BUSDTA"."F0005"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0005'', ''F0005_ml'', ''DROP INDEX "F0005"."F0005_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0005_del', 'F0005_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0005_del', 'F0005_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0005_del', 'F0005_mld'
	DROP INDEX "F0005_del"."F0005_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0005_del', 'F0005_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0005_mld" ON "MobileDataModel"."BUSDTA"."F0005_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0005_del'', ''F0005_mld'', ''DROP INDEX "F0005_del"."F0005_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0006'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F0006_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0006_del', 'F0006_del', 'e34ae44960e0624305b60be2391cda8f'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0006_del', 'F0006_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F0006_del', 'F0006_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F0006_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0006_del', 'F0006_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F0006_del" (
		"MCMCU" nchar(12) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("MCMCU")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F0006_del', 'F0006_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F0006_del"', 'e34ae44960e0624305b60be2391cda8f', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0006', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0006', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F0006', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0006" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0006', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0006" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F0006', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F0006" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F0006_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_ins', '976396578d3983ea51e2d9f1583c13ca';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_ins'
	DROP TRIGGER "BUSDTA"."F0006_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0006_ins"
		ON "MobileDataModel"."BUSDTA"."F0006" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F0006_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F0006_del"."MCMCU" = inserted."MCMCU"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_ins', 'DROP TRIGGER "BUSDTA"."F0006_ins"', '976396578d3983ea51e2d9f1583c13ca', 0
END;
/* Create the timestamp trigger 'F0006_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_upd', '1a42e2c9b813753b87f0af4c7ac800b4';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_upd'
	DROP TRIGGER "BUSDTA"."F0006_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0006_upd"
		ON "MobileDataModel"."BUSDTA"."F0006" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F0006"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F0006"."MCMCU" = inserted."MCMCU";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_upd', 'DROP TRIGGER "BUSDTA"."F0006_upd"', '1a42e2c9b813753b87f0af4c7ac800b4', 0
END;
/* Create the shadow delete trigger 'F0006_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_dlt', '04e2a6f38dbb143affe7a7bd7bdb71b0';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_dlt'
	DROP TRIGGER "BUSDTA"."F0006_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0006_dlt"
		ON "MobileDataModel"."BUSDTA"."F0006" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F0006_del" ( "MCMCU", "last_modified" )
			SELECT deleted."MCMCU",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0006', 'F0006_dlt', 'DROP TRIGGER "BUSDTA"."F0006_dlt"', '04e2a6f38dbb143affe7a7bd7bdb71b0', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0006', 'F0006_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0006', 'F0006_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0006', 'F0006_ml'
	DROP INDEX "F0006"."F0006_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0006', 'F0006_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0006_ml" ON "MobileDataModel"."BUSDTA"."F0006"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0006'', ''F0006_ml'', ''DROP INDEX "F0006"."F0006_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0006_del', 'F0006_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0006_del', 'F0006_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0006_del', 'F0006_mld'
	DROP INDEX "F0006_del"."F0006_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0006_del', 'F0006_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0006_mld" ON "MobileDataModel"."BUSDTA"."F0006_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0006_del'', ''F0006_mld'', ''DROP INDEX "F0006_del"."F0006_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0014'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F0014_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0014_del', 'F0014_del', '10a4a05015f19f3a08950357303bce41'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0014_del', 'F0014_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F0014_del', 'F0014_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F0014_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0014_del', 'F0014_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F0014_del" (
		"PNPTC" nchar(3) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("PNPTC")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F0014_del', 'F0014_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F0014_del"', '10a4a05015f19f3a08950357303bce41', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0014', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0014', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F0014', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0014" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0014', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0014" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F0014', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F0014" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F0014_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_ins', '4184353746929a0902772d12f7d13446';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_ins'
	DROP TRIGGER "BUSDTA"."F0014_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0014_ins"
		ON "MobileDataModel"."BUSDTA"."F0014" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F0014_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F0014_del"."PNPTC" = inserted."PNPTC"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_ins', 'DROP TRIGGER "BUSDTA"."F0014_ins"', '4184353746929a0902772d12f7d13446', 0
END;
/* Create the timestamp trigger 'F0014_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_upd', '659958362f6cbaea05b82be211958b40';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_upd'
	DROP TRIGGER "BUSDTA"."F0014_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0014_upd"
		ON "MobileDataModel"."BUSDTA"."F0014" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F0014"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F0014"."PNPTC" = inserted."PNPTC";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_upd', 'DROP TRIGGER "BUSDTA"."F0014_upd"', '659958362f6cbaea05b82be211958b40', 0
END;
/* Create the shadow delete trigger 'F0014_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_dlt', 'e19d7dd64327c2f7b1234db97c8808eb';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_dlt'
	DROP TRIGGER "BUSDTA"."F0014_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0014_dlt"
		ON "MobileDataModel"."BUSDTA"."F0014" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F0014_del" ( "PNPTC", "last_modified" )
			SELECT deleted."PNPTC",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0014', 'F0014_dlt', 'DROP TRIGGER "BUSDTA"."F0014_dlt"', 'e19d7dd64327c2f7b1234db97c8808eb', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0014', 'F0014_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0014', 'F0014_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0014', 'F0014_ml'
	DROP INDEX "F0014"."F0014_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0014', 'F0014_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0014_ml" ON "MobileDataModel"."BUSDTA"."F0014"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0014'', ''F0014_ml'', ''DROP INDEX "F0014"."F0014_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0014_del', 'F0014_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0014_del', 'F0014_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0014_del', 'F0014_mld'
	DROP INDEX "F0014_del"."F0014_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0014_del', 'F0014_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0014_mld" ON "MobileDataModel"."BUSDTA"."F0014_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0014_del'', ''F0014_mld'', ''DROP INDEX "F0014_del"."F0014_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0101'.
*-----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0115'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F0115_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0115_del', 'F0115_del', '272dbefd4a810d76b2ef9634ce212487'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0115_del', 'F0115_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F0115_del', 'F0115_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F0115_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0115_del', 'F0115_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F0115_del" (
		"WPAN8" float not null,
		"WPIDLN" float not null,
		"WPRCK7" float not null,
		"WPCNLN" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("WPAN8", "WPIDLN", "WPRCK7", "WPCNLN")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F0115_del', 'F0115_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F0115_del"', '272dbefd4a810d76b2ef9634ce212487', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0115', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0115', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F0115', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0115" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0115', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0115" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F0115', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F0115" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F0115_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_ins', '0e20557e48f6d2e01d6603ea66df13ae';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_ins'
	DROP TRIGGER "BUSDTA"."F0115_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0115_ins"
		ON "MobileDataModel"."BUSDTA"."F0115" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F0115_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F0115_del"."WPAN8" = inserted."WPAN8" AND
					"MobileDataModel"."BUSDTA"."F0115_del"."WPIDLN" = inserted."WPIDLN" AND
					"MobileDataModel"."BUSDTA"."F0115_del"."WPRCK7" = inserted."WPRCK7" AND
					"MobileDataModel"."BUSDTA"."F0115_del"."WPCNLN" = inserted."WPCNLN"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_ins', 'DROP TRIGGER "BUSDTA"."F0115_ins"', '0e20557e48f6d2e01d6603ea66df13ae', 0
END;
/* Create the timestamp trigger 'F0115_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_upd', '2bd9f81d713c857b735ee55cd1e98793';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_upd'
	DROP TRIGGER "BUSDTA"."F0115_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0115_upd"
		ON "MobileDataModel"."BUSDTA"."F0115" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F0115"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F0115"."WPAN8" = inserted."WPAN8" AND
				"MobileDataModel"."BUSDTA"."F0115"."WPIDLN" = inserted."WPIDLN" AND
				"MobileDataModel"."BUSDTA"."F0115"."WPRCK7" = inserted."WPRCK7" AND
				"MobileDataModel"."BUSDTA"."F0115"."WPCNLN" = inserted."WPCNLN";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_upd', 'DROP TRIGGER "BUSDTA"."F0115_upd"', '2bd9f81d713c857b735ee55cd1e98793', 0
END;
/* Create the shadow delete trigger 'F0115_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_dlt', '5beedc228a2b25400e9aab41e9392eb7';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_dlt'
	DROP TRIGGER "BUSDTA"."F0115_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0115_dlt"
		ON "MobileDataModel"."BUSDTA"."F0115" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F0115_del" ( "WPAN8", "WPIDLN", "WPRCK7", "WPCNLN", "last_modified" )
			SELECT deleted."WPAN8",
				deleted."WPIDLN",
				deleted."WPRCK7",
				deleted."WPCNLN",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0115', 'F0115_dlt', 'DROP TRIGGER "BUSDTA"."F0115_dlt"', '5beedc228a2b25400e9aab41e9392eb7', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0115', 'F0115_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0115', 'F0115_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0115', 'F0115_ml'
	DROP INDEX "F0115"."F0115_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0115', 'F0115_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0115_ml" ON "MobileDataModel"."BUSDTA"."F0115"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0115'', ''F0115_ml'', ''DROP INDEX "F0115"."F0115_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0115_del', 'F0115_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0115_del', 'F0115_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0115_del', 'F0115_mld'
	DROP INDEX "F0115_del"."F0115_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0115_del', 'F0115_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0115_mld" ON "MobileDataModel"."BUSDTA"."F0115_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0115_del'', ''F0115_mld'', ''DROP INDEX "F0115_del"."F0115_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F01151'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F01151_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F01151_del', 'F01151_del', 'd35b25b1c3d14440c4a106569d20be2d'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F01151_del', 'F01151_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F01151_del', 'F01151_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F01151_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F01151_del', 'F01151_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F01151_del" (
		"EAAN8" float not null,
		"EAIDLN" float not null,
		"EARCK7" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("EAAN8", "EAIDLN", "EARCK7")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F01151_del', 'F01151_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F01151_del"', 'd35b25b1c3d14440c4a106569d20be2d', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F01151', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F01151', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F01151', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F01151" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F01151', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F01151" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F01151', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F01151" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F01151_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_ins', 'd1162132edb80d418e15aa625d28c1ae';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_ins'
	DROP TRIGGER "BUSDTA"."F01151_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F01151_ins"
		ON "MobileDataModel"."BUSDTA"."F01151" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F01151_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F01151_del"."EAAN8" = inserted."EAAN8" AND
					"MobileDataModel"."BUSDTA"."F01151_del"."EAIDLN" = inserted."EAIDLN" AND
					"MobileDataModel"."BUSDTA"."F01151_del"."EARCK7" = inserted."EARCK7"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_ins', 'DROP TRIGGER "BUSDTA"."F01151_ins"', 'd1162132edb80d418e15aa625d28c1ae', 0
END;
/* Create the timestamp trigger 'F01151_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_upd', 'b92ae08a0050d60d8fccc1c49f6e84bc';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_upd'
	DROP TRIGGER "BUSDTA"."F01151_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F01151_upd"
		ON "MobileDataModel"."BUSDTA"."F01151" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F01151"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F01151"."EAAN8" = inserted."EAAN8" AND
				"MobileDataModel"."BUSDTA"."F01151"."EAIDLN" = inserted."EAIDLN" AND
				"MobileDataModel"."BUSDTA"."F01151"."EARCK7" = inserted."EARCK7";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_upd', 'DROP TRIGGER "BUSDTA"."F01151_upd"', 'b92ae08a0050d60d8fccc1c49f6e84bc', 0
END;
/* Create the shadow delete trigger 'F01151_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_dlt', '706bbf4c5fa3e9a9527f2e6b7334f48b';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_dlt'
	DROP TRIGGER "BUSDTA"."F01151_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F01151_dlt"
		ON "MobileDataModel"."BUSDTA"."F01151" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F01151_del" ( "EAAN8", "EAIDLN", "EARCK7", "last_modified" )
			SELECT deleted."EAAN8",
				deleted."EAIDLN",
				deleted."EARCK7",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F01151', 'F01151_dlt', 'DROP TRIGGER "BUSDTA"."F01151_dlt"', '706bbf4c5fa3e9a9527f2e6b7334f48b', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F01151', 'F01151_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F01151', 'F01151_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F01151', 'F01151_ml'
	DROP INDEX "F01151"."F01151_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F01151', 'F01151_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F01151_ml" ON "MobileDataModel"."BUSDTA"."F01151"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F01151'', ''F01151_ml'', ''DROP INDEX "F01151"."F01151_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F01151_del', 'F01151_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F01151_del', 'F01151_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F01151_del', 'F01151_mld'
	DROP INDEX "F01151_del"."F01151_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F01151_del', 'F01151_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F01151_mld" ON "MobileDataModel"."BUSDTA"."F01151_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F01151_del'', ''F01151_mld'', ''DROP INDEX "F01151_del"."F01151_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0116'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F0116_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0116_del', 'F0116_del', '17f8cb2cbae2f52517c670ef243cabb9'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0116_del', 'F0116_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F0116_del', 'F0116_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F0116_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0116_del', 'F0116_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F0116_del" (
		"ALAN8" float not null,
		"ALEFTB" numeric(18, 0) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("ALAN8", "ALEFTB")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F0116_del', 'F0116_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F0116_del"', '17f8cb2cbae2f52517c670ef243cabb9', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0116', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0116', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F0116', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0116" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0116', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0116" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F0116', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F0116" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F0116_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_ins', 'ede9b2a5d1b9b084cc51ccdd081bb393';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_ins'
	DROP TRIGGER "BUSDTA"."F0116_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0116_ins"
		ON "MobileDataModel"."BUSDTA"."F0116" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F0116_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F0116_del"."ALAN8" = inserted."ALAN8" AND
					"MobileDataModel"."BUSDTA"."F0116_del"."ALEFTB" = inserted."ALEFTB"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_ins', 'DROP TRIGGER "BUSDTA"."F0116_ins"', 'ede9b2a5d1b9b084cc51ccdd081bb393', 0
END;
/* Create the timestamp trigger 'F0116_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_upd', 'f6f7246fd21d3963ff3d597bfaf02337';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_upd'
	DROP TRIGGER "BUSDTA"."F0116_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0116_upd"
		ON "MobileDataModel"."BUSDTA"."F0116" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F0116"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F0116"."ALAN8" = inserted."ALAN8" AND
				"MobileDataModel"."BUSDTA"."F0116"."ALEFTB" = inserted."ALEFTB";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_upd', 'DROP TRIGGER "BUSDTA"."F0116_upd"', 'f6f7246fd21d3963ff3d597bfaf02337', 0
END;
/* Create the shadow delete trigger 'F0116_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_dlt', 'a200362237928f5825411cc387f6bf84';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_dlt'
	DROP TRIGGER "BUSDTA"."F0116_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0116_dlt"
		ON "MobileDataModel"."BUSDTA"."F0116" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F0116_del" ( "ALAN8", "ALEFTB", "last_modified" )
			SELECT deleted."ALAN8",
				deleted."ALEFTB",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0116', 'F0116_dlt', 'DROP TRIGGER "BUSDTA"."F0116_dlt"', 'a200362237928f5825411cc387f6bf84', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0116', 'F0116_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0116', 'F0116_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0116', 'F0116_ml'
	DROP INDEX "F0116"."F0116_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0116', 'F0116_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0116_ml" ON "MobileDataModel"."BUSDTA"."F0116"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0116'', ''F0116_ml'', ''DROP INDEX "F0116"."F0116_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0116_del', 'F0116_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0116_del', 'F0116_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0116_del', 'F0116_mld'
	DROP INDEX "F0116_del"."F0116_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0116_del', 'F0116_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0116_mld" ON "MobileDataModel"."BUSDTA"."F0116_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0116_del'', ''F0116_mld'', ''DROP INDEX "F0116_del"."F0116_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F0150'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F0150_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0150_del', 'F0150_del', '93aba966745ba260de977872bd2087c2'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0150_del', 'F0150_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F0150_del', 'F0150_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F0150_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F0150_del', 'F0150_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F0150_del" (
		"MAOSTP" nchar(3) not null,
		"MAPA8" float not null,
		"MAAN8" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("MAOSTP", "MAPA8", "MAAN8")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F0150_del', 'F0150_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F0150_del"', '93aba966745ba260de977872bd2087c2', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0150', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0150', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F0150', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0150" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F0150', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F0150" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F0150', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F0150" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F0150_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_ins', '7fd8587013b42f232319fc8018af0fa5';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_ins'
	DROP TRIGGER "BUSDTA"."F0150_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0150_ins"
		ON "MobileDataModel"."BUSDTA"."F0150" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F0150_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F0150_del"."MAOSTP" = inserted."MAOSTP" AND
					"MobileDataModel"."BUSDTA"."F0150_del"."MAPA8" = inserted."MAPA8" AND
					"MobileDataModel"."BUSDTA"."F0150_del"."MAAN8" = inserted."MAAN8"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_ins', 'DROP TRIGGER "BUSDTA"."F0150_ins"', '7fd8587013b42f232319fc8018af0fa5', 0
END;
/* Create the timestamp trigger 'F0150_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_upd', '03a18fe8b54973d3b11f69375fea816d';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_upd'
	DROP TRIGGER "BUSDTA"."F0150_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0150_upd"
		ON "MobileDataModel"."BUSDTA"."F0150" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F0150"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F0150"."MAOSTP" = inserted."MAOSTP" AND
				"MobileDataModel"."BUSDTA"."F0150"."MAPA8" = inserted."MAPA8" AND
				"MobileDataModel"."BUSDTA"."F0150"."MAAN8" = inserted."MAAN8";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_upd', 'DROP TRIGGER "BUSDTA"."F0150_upd"', '03a18fe8b54973d3b11f69375fea816d', 0
END;
/* Create the shadow delete trigger 'F0150_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_dlt', '88601abc6b9c10b105115ab9fd886bab';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_dlt'
	DROP TRIGGER "BUSDTA"."F0150_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F0150_dlt"
		ON "MobileDataModel"."BUSDTA"."F0150" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F0150_del" ( "MAOSTP", "MAPA8", "MAAN8", "last_modified" )
			SELECT deleted."MAOSTP",
				deleted."MAPA8",
				deleted."MAAN8",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F0150', 'F0150_dlt', 'DROP TRIGGER "BUSDTA"."F0150_dlt"', '88601abc6b9c10b105115ab9fd886bab', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0150', 'F0150_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0150', 'F0150_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0150', 'F0150_ml'
	DROP INDEX "F0150"."F0150_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0150', 'F0150_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0150_ml" ON "MobileDataModel"."BUSDTA"."F0150"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0150'', ''F0150_ml'', ''DROP INDEX "F0150"."F0150_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0150_del', 'F0150_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0150_del', 'F0150_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F0150_del', 'F0150_mld'
	DROP INDEX "F0150_del"."F0150_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F0150_del', 'F0150_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F0150_mld" ON "MobileDataModel"."BUSDTA"."F0150_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F0150_del'', ''F0150_mld'', ''DROP INDEX "F0150_del"."F0150_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F03012'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F03012_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F03012_del', 'F03012_del', 'f6193ea86d91945a9a8ad85f1f3afa9c'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F03012_del', 'F03012_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F03012_del', 'F03012_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F03012_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F03012_del', 'F03012_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F03012_del" (
		"AIAN8" float not null,
		"AICO" nchar(5) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("AIAN8", "AICO")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F03012_del', 'F03012_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F03012_del"', 'f6193ea86d91945a9a8ad85f1f3afa9c', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F03012', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F03012', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F03012', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F03012" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F03012', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F03012" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F03012', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F03012" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F03012_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_ins', '76a9954a881a971117a4c1b565c95871';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_ins'
	DROP TRIGGER "BUSDTA"."F03012_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F03012_ins"
		ON "MobileDataModel"."BUSDTA"."F03012" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F03012_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F03012_del"."AIAN8" = inserted."AIAN8" AND
					"MobileDataModel"."BUSDTA"."F03012_del"."AICO" = inserted."AICO"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_ins', 'DROP TRIGGER "BUSDTA"."F03012_ins"', '76a9954a881a971117a4c1b565c95871', 0
END;
/* Create the timestamp trigger 'F03012_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_upd', 'f73e19d15e7be7b877b200229cdd7242';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_upd'
	DROP TRIGGER "BUSDTA"."F03012_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F03012_upd"
		ON "MobileDataModel"."BUSDTA"."F03012" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F03012"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F03012"."AIAN8" = inserted."AIAN8" AND
				"MobileDataModel"."BUSDTA"."F03012"."AICO" = inserted."AICO";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_upd', 'DROP TRIGGER "BUSDTA"."F03012_upd"', 'f73e19d15e7be7b877b200229cdd7242', 0
END;
/* Create the shadow delete trigger 'F03012_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_dlt', '3ab134a715e597a0a97ada1595cfefb4';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_dlt'
	DROP TRIGGER "BUSDTA"."F03012_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F03012_dlt"
		ON "MobileDataModel"."BUSDTA"."F03012" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F03012_del" ( "AIAN8", "AICO", "last_modified" )
			SELECT deleted."AIAN8",
				deleted."AICO",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F03012', 'F03012_dlt', 'DROP TRIGGER "BUSDTA"."F03012_dlt"', '3ab134a715e597a0a97ada1595cfefb4', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F03012', 'F03012_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F03012', 'F03012_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F03012', 'F03012_ml'
	DROP INDEX "F03012"."F03012_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F03012', 'F03012_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F03012_ml" ON "MobileDataModel"."BUSDTA"."F03012"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F03012'', ''F03012_ml'', ''DROP INDEX "F03012"."F03012_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F03012_del', 'F03012_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F03012_del', 'F03012_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F03012_del', 'F03012_mld'
	DROP INDEX "F03012_del"."F03012_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F03012_del', 'F03012_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F03012_mld" ON "MobileDataModel"."BUSDTA"."F03012_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F03012_del'', ''F03012_mld'', ''DROP INDEX "F03012_del"."F03012_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F40073'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F40073_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40073_del', 'F40073_del', '6fa5c66d436d92b18e315ce9d5f401a9'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40073_del', 'F40073_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F40073_del', 'F40073_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F40073_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40073_del', 'F40073_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F40073_del" (
		"HYPRFR" nchar(2) not null,
		"HYHYID" nchar(10) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("HYPRFR", "HYHYID")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F40073_del', 'F40073_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F40073_del"', '6fa5c66d436d92b18e315ce9d5f401a9', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40073', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40073', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F40073', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F40073" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40073', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F40073" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F40073', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F40073" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F40073_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_ins', '52141d7d1111f53cb5ee9212996db162';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_ins'
	DROP TRIGGER "BUSDTA"."F40073_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40073_ins"
		ON "MobileDataModel"."BUSDTA"."F40073" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F40073_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F40073_del"."HYPRFR" = inserted."HYPRFR" AND
					"MobileDataModel"."BUSDTA"."F40073_del"."HYHYID" = inserted."HYHYID"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_ins', 'DROP TRIGGER "BUSDTA"."F40073_ins"', '52141d7d1111f53cb5ee9212996db162', 0
END;
/* Create the timestamp trigger 'F40073_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_upd', 'a407aa35410503b9f2177e9e52c05e35';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_upd'
	DROP TRIGGER "BUSDTA"."F40073_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40073_upd"
		ON "MobileDataModel"."BUSDTA"."F40073" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F40073"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F40073"."HYPRFR" = inserted."HYPRFR" AND
				"MobileDataModel"."BUSDTA"."F40073"."HYHYID" = inserted."HYHYID";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_upd', 'DROP TRIGGER "BUSDTA"."F40073_upd"', 'a407aa35410503b9f2177e9e52c05e35', 0
END;
/* Create the shadow delete trigger 'F40073_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_dlt', 'af7147ada006b3c3e94b351a43d68236';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_dlt'
	DROP TRIGGER "BUSDTA"."F40073_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40073_dlt"
		ON "MobileDataModel"."BUSDTA"."F40073" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F40073_del" ( "HYPRFR", "HYHYID", "last_modified" )
			SELECT deleted."HYPRFR",
				deleted."HYHYID",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40073', 'F40073_dlt', 'DROP TRIGGER "BUSDTA"."F40073_dlt"', 'af7147ada006b3c3e94b351a43d68236', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40073', 'F40073_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40073', 'F40073_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F40073', 'F40073_ml'
	DROP INDEX "F40073"."F40073_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40073', 'F40073_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F40073_ml" ON "MobileDataModel"."BUSDTA"."F40073"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F40073'', ''F40073_ml'', ''DROP INDEX "F40073"."F40073_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40073_del', 'F40073_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40073_del', 'F40073_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F40073_del', 'F40073_mld'
	DROP INDEX "F40073_del"."F40073_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40073_del', 'F40073_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F40073_mld" ON "MobileDataModel"."BUSDTA"."F40073_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F40073_del'', ''F40073_mld'', ''DROP INDEX "F40073_del"."F40073_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4015'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4015_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4015_del', 'F4015_del', '3edea90d2e29dd351a1fbba32d4ab7ea'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4015_del', 'F4015_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4015_del', 'F4015_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4015_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4015_del', 'F4015_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4015_del" (
		"OTORTP" nchar(8) not null,
		"OTAN8" float not null,
		"OTOSEQ" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("OTORTP", "OTAN8", "OTOSEQ")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4015_del', 'F4015_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4015_del"', '3edea90d2e29dd351a1fbba32d4ab7ea', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4015', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4015', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4015', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4015" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4015', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4015" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4015', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4015" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4015_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_ins', 'bb9e69c305d104687a099bf931d6c3f8';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_ins'
	DROP TRIGGER "BUSDTA"."F4015_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4015_ins"
		ON "MobileDataModel"."BUSDTA"."F4015" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4015_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4015_del"."OTORTP" = inserted."OTORTP" AND
					"MobileDataModel"."BUSDTA"."F4015_del"."OTAN8" = inserted."OTAN8" AND
					"MobileDataModel"."BUSDTA"."F4015_del"."OTOSEQ" = inserted."OTOSEQ"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_ins', 'DROP TRIGGER "BUSDTA"."F4015_ins"', 'bb9e69c305d104687a099bf931d6c3f8', 0
END;
/* Create the timestamp trigger 'F4015_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_upd', '797ae1017283de66e8ba1142921490c3';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_upd'
	DROP TRIGGER "BUSDTA"."F4015_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4015_upd"
		ON "MobileDataModel"."BUSDTA"."F4015" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4015"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4015"."OTORTP" = inserted."OTORTP" AND
				"MobileDataModel"."BUSDTA"."F4015"."OTAN8" = inserted."OTAN8" AND
				"MobileDataModel"."BUSDTA"."F4015"."OTOSEQ" = inserted."OTOSEQ";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_upd', 'DROP TRIGGER "BUSDTA"."F4015_upd"', '797ae1017283de66e8ba1142921490c3', 0
END;
/* Create the shadow delete trigger 'F4015_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_dlt', 'cc75524f0eef2ca00f106c80514d082e';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_dlt'
	DROP TRIGGER "BUSDTA"."F4015_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4015_dlt"
		ON "MobileDataModel"."BUSDTA"."F4015" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4015_del" ( "OTORTP", "OTAN8", "OTOSEQ", "last_modified" )
			SELECT deleted."OTORTP",
				deleted."OTAN8",
				deleted."OTOSEQ",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4015', 'F4015_dlt', 'DROP TRIGGER "BUSDTA"."F4015_dlt"', 'cc75524f0eef2ca00f106c80514d082e', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4015', 'F4015_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4015', 'F4015_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4015', 'F4015_ml'
	DROP INDEX "F4015"."F4015_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4015', 'F4015_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4015_ml" ON "MobileDataModel"."BUSDTA"."F4015"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4015'', ''F4015_ml'', ''DROP INDEX "F4015"."F4015_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4015_del', 'F4015_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4015_del', 'F4015_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4015_del', 'F4015_mld'
	DROP INDEX "F4015_del"."F4015_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4015_del', 'F4015_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4015_mld" ON "MobileDataModel"."BUSDTA"."F4015_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4015_del'', ''F4015_mld'', ''DROP INDEX "F4015_del"."F4015_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4070'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4070_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4070_del', 'F4070_del', '163d6b9433bafaf93d017ebd56f79ef8'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4070_del', 'F4070_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4070_del', 'F4070_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4070_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4070_del', 'F4070_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4070_del" (
		"SNASN" nchar(8) not null,
		"SNOSEQ" float not null,
		"SNANPS" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("SNASN", "SNOSEQ", "SNANPS")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4070_del', 'F4070_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4070_del"', '163d6b9433bafaf93d017ebd56f79ef8', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4070', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4070', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4070', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4070" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4070', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4070" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4070', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4070" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4070_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_ins', '767c1dd05de989a7e00c0cf9820ab779';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_ins'
	DROP TRIGGER "BUSDTA"."F4070_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4070_ins"
		ON "MobileDataModel"."BUSDTA"."F4070" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4070_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4070_del"."SNASN" = inserted."SNASN" AND
					"MobileDataModel"."BUSDTA"."F4070_del"."SNOSEQ" = inserted."SNOSEQ" AND
					"MobileDataModel"."BUSDTA"."F4070_del"."SNANPS" = inserted."SNANPS"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_ins', 'DROP TRIGGER "BUSDTA"."F4070_ins"', '767c1dd05de989a7e00c0cf9820ab779', 0
END;
/* Create the timestamp trigger 'F4070_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_upd', 'c29726b27c4139949737c71f4515f093';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_upd'
	DROP TRIGGER "BUSDTA"."F4070_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4070_upd"
		ON "MobileDataModel"."BUSDTA"."F4070" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4070"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4070"."SNASN" = inserted."SNASN" AND
				"MobileDataModel"."BUSDTA"."F4070"."SNOSEQ" = inserted."SNOSEQ" AND
				"MobileDataModel"."BUSDTA"."F4070"."SNANPS" = inserted."SNANPS";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_upd', 'DROP TRIGGER "BUSDTA"."F4070_upd"', 'c29726b27c4139949737c71f4515f093', 0
END;
/* Create the shadow delete trigger 'F4070_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_dlt', 'ba2595c5484bbe51b285f50aaf74d954';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_dlt'
	DROP TRIGGER "BUSDTA"."F4070_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4070_dlt"
		ON "MobileDataModel"."BUSDTA"."F4070" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4070_del" ( "SNASN", "SNOSEQ", "SNANPS", "last_modified" )
			SELECT deleted."SNASN",
				deleted."SNOSEQ",
				deleted."SNANPS",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4070', 'F4070_dlt', 'DROP TRIGGER "BUSDTA"."F4070_dlt"', 'ba2595c5484bbe51b285f50aaf74d954', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4070', 'F4070_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4070', 'F4070_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4070', 'F4070_ml'
	DROP INDEX "F4070"."F4070_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4070', 'F4070_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4070_ml" ON "MobileDataModel"."BUSDTA"."F4070"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4070'', ''F4070_ml'', ''DROP INDEX "F4070"."F4070_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4070_del', 'F4070_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4070_del', 'F4070_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4070_del', 'F4070_mld'
	DROP INDEX "F4070_del"."F4070_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4070_del', 'F4070_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4070_mld" ON "MobileDataModel"."BUSDTA"."F4070_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4070_del'', ''F4070_mld'', ''DROP INDEX "F4070_del"."F4070_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4071'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4071_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4071_del', 'F4071_del', '9fadec0b589dad5210468009968d98be'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4071_del', 'F4071_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4071_del', 'F4071_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4071_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4071_del', 'F4071_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4071_del" (
		"ATAST" nchar(8) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("ATAST")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4071_del', 'F4071_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4071_del"', '9fadec0b589dad5210468009968d98be', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4071', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4071', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4071', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4071" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4071', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4071" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4071', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4071" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4071_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_ins', '34425e6dfc2860462bfcc01079ae0e5a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_ins'
	DROP TRIGGER "BUSDTA"."F4071_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4071_ins"
		ON "MobileDataModel"."BUSDTA"."F4071" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4071_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4071_del"."ATAST" = inserted."ATAST"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_ins', 'DROP TRIGGER "BUSDTA"."F4071_ins"', '34425e6dfc2860462bfcc01079ae0e5a', 0
END;
/* Create the timestamp trigger 'F4071_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_upd', '892f9a51c354e1166c09cd72e7ff5f73';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_upd'
	DROP TRIGGER "BUSDTA"."F4071_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4071_upd"
		ON "MobileDataModel"."BUSDTA"."F4071" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4071"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4071"."ATAST" = inserted."ATAST";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_upd', 'DROP TRIGGER "BUSDTA"."F4071_upd"', '892f9a51c354e1166c09cd72e7ff5f73', 0
END;
/* Create the shadow delete trigger 'F4071_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_dlt', '8ded7f75ae6ae0feb0a66cd27a660c26';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_dlt'
	DROP TRIGGER "BUSDTA"."F4071_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4071_dlt"
		ON "MobileDataModel"."BUSDTA"."F4071" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4071_del" ( "ATAST", "last_modified" )
			SELECT deleted."ATAST",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4071', 'F4071_dlt', 'DROP TRIGGER "BUSDTA"."F4071_dlt"', '8ded7f75ae6ae0feb0a66cd27a660c26', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4071', 'F4071_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4071', 'F4071_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4071', 'F4071_ml'
	DROP INDEX "F4071"."F4071_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4071', 'F4071_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4071_ml" ON "MobileDataModel"."BUSDTA"."F4071"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4071'', ''F4071_ml'', ''DROP INDEX "F4071"."F4071_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4071_del', 'F4071_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4071_del', 'F4071_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4071_del', 'F4071_mld'
	DROP INDEX "F4071_del"."F4071_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4071_del', 'F4071_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4071_mld" ON "MobileDataModel"."BUSDTA"."F4071_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4071_del'', ''F4071_mld'', ''DROP INDEX "F4071_del"."F4071_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4072'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4072_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4072_del', 'F4072_del', '25cf2602077bf9ac1239481ed75b5101'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4072_del', 'F4072_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4072_del', 'F4072_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4072_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4072_del', 'F4072_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4072_del" (
		"ADAST" nchar(8) not null,
		"ADITM" float not null,
		"ADAN8" float not null,
		"ADIGID" float not null,
		"ADCGID" float not null,
		"ADOGID" float not null,
		"ADCRCD" nchar(3) not null,
		"ADUOM" nchar(2) not null,
		"ADMNQ" float not null,
		"ADEXDJ" numeric(18, 0) not null,
		"ADUPMJ" numeric(18, 0) not null,
		"ADTDAY" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("ADAST", "ADITM", "ADAN8", "ADIGID", "ADCGID", "ADOGID", "ADCRCD", "ADUOM", "ADMNQ", "ADEXDJ", "ADUPMJ", "ADTDAY")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4072_del', 'F4072_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4072_del"', '25cf2602077bf9ac1239481ed75b5101', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4072', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4072', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4072', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4072" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4072', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4072" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4072', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4072" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4072_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_ins', '7c8625657f0c01704ce6d7e901a91b1d';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_ins'
	DROP TRIGGER "BUSDTA"."F4072_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4072_ins"
		ON "MobileDataModel"."BUSDTA"."F4072" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4072_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4072_del"."ADAST" = inserted."ADAST" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADITM" = inserted."ADITM" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADAN8" = inserted."ADAN8" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADIGID" = inserted."ADIGID" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADCGID" = inserted."ADCGID" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADOGID" = inserted."ADOGID" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADCRCD" = inserted."ADCRCD" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADUOM" = inserted."ADUOM" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADMNQ" = inserted."ADMNQ" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADEXDJ" = inserted."ADEXDJ" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADUPMJ" = inserted."ADUPMJ" AND
					"MobileDataModel"."BUSDTA"."F4072_del"."ADTDAY" = inserted."ADTDAY"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_ins', 'DROP TRIGGER "BUSDTA"."F4072_ins"', '7c8625657f0c01704ce6d7e901a91b1d', 0
END;
/* Create the timestamp trigger 'F4072_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_upd', '45e49f827f8a1f37e3f0caeb893ce84e';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_upd'
	DROP TRIGGER "BUSDTA"."F4072_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4072_upd"
		ON "MobileDataModel"."BUSDTA"."F4072" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4072"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4072"."ADAST" = inserted."ADAST" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADITM" = inserted."ADITM" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADAN8" = inserted."ADAN8" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADIGID" = inserted."ADIGID" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADCGID" = inserted."ADCGID" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADOGID" = inserted."ADOGID" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADCRCD" = inserted."ADCRCD" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADUOM" = inserted."ADUOM" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADMNQ" = inserted."ADMNQ" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADEXDJ" = inserted."ADEXDJ" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADUPMJ" = inserted."ADUPMJ" AND
				"MobileDataModel"."BUSDTA"."F4072"."ADTDAY" = inserted."ADTDAY";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_upd', 'DROP TRIGGER "BUSDTA"."F4072_upd"', '45e49f827f8a1f37e3f0caeb893ce84e', 0
END;
/* Create the shadow delete trigger 'F4072_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_dlt', 'c9cd8b64d394885abb5a96749aa978d7';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_dlt'
	DROP TRIGGER "BUSDTA"."F4072_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4072_dlt"
		ON "MobileDataModel"."BUSDTA"."F4072" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4072_del" ( "ADAST", "ADITM", "ADAN8", "ADIGID", "ADCGID", "ADOGID", "ADCRCD", "ADUOM", "ADMNQ", "ADEXDJ", "ADUPMJ", "ADTDAY", "last_modified" )
			SELECT deleted."ADAST",
				deleted."ADITM",
				deleted."ADAN8",
				deleted."ADIGID",
				deleted."ADCGID",
				deleted."ADOGID",
				deleted."ADCRCD",
				deleted."ADUOM",
				deleted."ADMNQ",
				deleted."ADEXDJ",
				deleted."ADUPMJ",
				deleted."ADTDAY",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4072', 'F4072_dlt', 'DROP TRIGGER "BUSDTA"."F4072_dlt"', 'c9cd8b64d394885abb5a96749aa978d7', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4072', 'F4072_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4072', 'F4072_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4072', 'F4072_ml'
	DROP INDEX "F4072"."F4072_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4072', 'F4072_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4072_ml" ON "MobileDataModel"."BUSDTA"."F4072"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4072'', ''F4072_ml'', ''DROP INDEX "F4072"."F4072_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4072_del', 'F4072_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4072_del', 'F4072_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4072_del', 'F4072_mld'
	DROP INDEX "F4072_del"."F4072_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4072_del', 'F4072_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4072_mld" ON "MobileDataModel"."BUSDTA"."F4072_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4072_del'', ''F4072_mld'', ''DROP INDEX "F4072_del"."F4072_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4075'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4075_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4075_del', 'F4075_del', '0239f75215ba340674aa8f6d2f064b44'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4075_del', 'F4075_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4075_del', 'F4075_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4075_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4075_del', 'F4075_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4075_del" (
		"VBVBT" nchar(10) not null,
		"VBEFTJ" numeric(18, 0) not null,
		"VBUPMJ" numeric(18, 0) not null,
		"VBTDAY" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("VBVBT", "VBEFTJ", "VBUPMJ", "VBTDAY")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4075_del', 'F4075_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4075_del"', '0239f75215ba340674aa8f6d2f064b44', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4075', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4075', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4075', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4075" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4075', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4075" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4075', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4075" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4075_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_ins', 'c16933ae8b499bc1dd3d31110ef5dac5';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_ins'
	DROP TRIGGER "BUSDTA"."F4075_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4075_ins"
		ON "MobileDataModel"."BUSDTA"."F4075" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4075_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4075_del"."VBVBT" = inserted."VBVBT" AND
					"MobileDataModel"."BUSDTA"."F4075_del"."VBEFTJ" = inserted."VBEFTJ" AND
					"MobileDataModel"."BUSDTA"."F4075_del"."VBUPMJ" = inserted."VBUPMJ" AND
					"MobileDataModel"."BUSDTA"."F4075_del"."VBTDAY" = inserted."VBTDAY"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_ins', 'DROP TRIGGER "BUSDTA"."F4075_ins"', 'c16933ae8b499bc1dd3d31110ef5dac5', 0
END;
/* Create the timestamp trigger 'F4075_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_upd', '58f4550f9642a79e10d4f68011b7276b';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_upd'
	DROP TRIGGER "BUSDTA"."F4075_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4075_upd"
		ON "MobileDataModel"."BUSDTA"."F4075" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4075"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4075"."VBVBT" = inserted."VBVBT" AND
				"MobileDataModel"."BUSDTA"."F4075"."VBEFTJ" = inserted."VBEFTJ" AND
				"MobileDataModel"."BUSDTA"."F4075"."VBUPMJ" = inserted."VBUPMJ" AND
				"MobileDataModel"."BUSDTA"."F4075"."VBTDAY" = inserted."VBTDAY";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_upd', 'DROP TRIGGER "BUSDTA"."F4075_upd"', '58f4550f9642a79e10d4f68011b7276b', 0
END;
/* Create the shadow delete trigger 'F4075_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_dlt', '1a1d06242f2faa1f7836169c2dd4c1a0';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_dlt'
	DROP TRIGGER "BUSDTA"."F4075_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4075_dlt"
		ON "MobileDataModel"."BUSDTA"."F4075" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4075_del" ( "VBVBT", "VBEFTJ", "VBUPMJ", "VBTDAY", "last_modified" )
			SELECT deleted."VBVBT",
				deleted."VBEFTJ",
				deleted."VBUPMJ",
				deleted."VBTDAY",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4075', 'F4075_dlt', 'DROP TRIGGER "BUSDTA"."F4075_dlt"', '1a1d06242f2faa1f7836169c2dd4c1a0', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4075', 'F4075_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4075', 'F4075_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4075', 'F4075_ml'
	DROP INDEX "F4075"."F4075_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4075', 'F4075_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4075_ml" ON "MobileDataModel"."BUSDTA"."F4075"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4075'', ''F4075_ml'', ''DROP INDEX "F4075"."F4075_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4075_del', 'F4075_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4075_del', 'F4075_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4075_del', 'F4075_mld'
	DROP INDEX "F4075_del"."F4075_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4075_del', 'F4075_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4075_mld" ON "MobileDataModel"."BUSDTA"."F4075_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4075_del'', ''F4075_mld'', ''DROP INDEX "F4075_del"."F4075_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4076'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4076_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4076_del', 'F4076_del', 'd277699ef7d464af716bda0c165171d0'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4076_del', 'F4076_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4076_del', 'F4076_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4076_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4076_del', 'F4076_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4076_del" (
		"FMFRMN" nchar(10) not null,
		"FMUPMJ" numeric(18, 0) not null,
		"FMTDAY" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("FMFRMN", "FMUPMJ", "FMTDAY")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4076_del', 'F4076_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4076_del"', 'd277699ef7d464af716bda0c165171d0', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4076', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4076', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4076', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4076" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4076', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4076" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4076', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4076" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4076_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_ins', '7cc77c6476037d99737581c9124d3f0f';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_ins'
	DROP TRIGGER "BUSDTA"."F4076_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4076_ins"
		ON "MobileDataModel"."BUSDTA"."F4076" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4076_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4076_del"."FMFRMN" = inserted."FMFRMN" AND
					"MobileDataModel"."BUSDTA"."F4076_del"."FMUPMJ" = inserted."FMUPMJ" AND
					"MobileDataModel"."BUSDTA"."F4076_del"."FMTDAY" = inserted."FMTDAY"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_ins', 'DROP TRIGGER "BUSDTA"."F4076_ins"', '7cc77c6476037d99737581c9124d3f0f', 0
END;
/* Create the timestamp trigger 'F4076_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_upd', 'd368e75b384d1ebfd9f8a22fda5319f5';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_upd'
	DROP TRIGGER "BUSDTA"."F4076_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4076_upd"
		ON "MobileDataModel"."BUSDTA"."F4076" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4076"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4076"."FMFRMN" = inserted."FMFRMN" AND
				"MobileDataModel"."BUSDTA"."F4076"."FMUPMJ" = inserted."FMUPMJ" AND
				"MobileDataModel"."BUSDTA"."F4076"."FMTDAY" = inserted."FMTDAY";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_upd', 'DROP TRIGGER "BUSDTA"."F4076_upd"', 'd368e75b384d1ebfd9f8a22fda5319f5', 0
END;
/* Create the shadow delete trigger 'F4076_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_dlt', 'f563de24dd8e8aa613c8768b138dd163';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_dlt'
	DROP TRIGGER "BUSDTA"."F4076_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4076_dlt"
		ON "MobileDataModel"."BUSDTA"."F4076" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4076_del" ( "FMFRMN", "FMUPMJ", "FMTDAY", "last_modified" )
			SELECT deleted."FMFRMN",
				deleted."FMUPMJ",
				deleted."FMTDAY",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4076', 'F4076_dlt', 'DROP TRIGGER "BUSDTA"."F4076_dlt"', 'f563de24dd8e8aa613c8768b138dd163', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4076', 'F4076_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4076', 'F4076_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4076', 'F4076_ml'
	DROP INDEX "F4076"."F4076_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4076', 'F4076_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4076_ml" ON "MobileDataModel"."BUSDTA"."F4076"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4076'', ''F4076_ml'', ''DROP INDEX "F4076"."F4076_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4076_del', 'F4076_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4076_del', 'F4076_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4076_del', 'F4076_mld'
	DROP INDEX "F4076_del"."F4076_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4076_del', 'F4076_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4076_mld" ON "MobileDataModel"."BUSDTA"."F4076_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4076_del'', ''F4076_mld'', ''DROP INDEX "F4076_del"."F4076_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4092'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4092_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4092_del', 'F4092_del', 'c41ff3c7e1debf7ca331b6a17805346c'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4092_del', 'F4092_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4092_del', 'F4092_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4092_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4092_del', 'F4092_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4092_del" (
		"GPGPTY" nchar(1) not null,
		"GPGPC" nchar(8) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("GPGPTY", "GPGPC")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4092_del', 'F4092_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4092_del"', 'c41ff3c7e1debf7ca331b6a17805346c', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4092', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4092', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4092', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4092" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4092', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4092" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4092', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4092" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4092_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_ins', 'bf3a1f58a46e0f79455397a99a62cbeb';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_ins'
	DROP TRIGGER "BUSDTA"."F4092_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4092_ins"
		ON "MobileDataModel"."BUSDTA"."F4092" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4092_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4092_del"."GPGPTY" = inserted."GPGPTY" AND
					"MobileDataModel"."BUSDTA"."F4092_del"."GPGPC" = inserted."GPGPC"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_ins', 'DROP TRIGGER "BUSDTA"."F4092_ins"', 'bf3a1f58a46e0f79455397a99a62cbeb', 0
END;
/* Create the timestamp trigger 'F4092_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_upd', '4b13910e8fce947a34af6bc24a6779b4';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_upd'
	DROP TRIGGER "BUSDTA"."F4092_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4092_upd"
		ON "MobileDataModel"."BUSDTA"."F4092" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4092"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4092"."GPGPTY" = inserted."GPGPTY" AND
				"MobileDataModel"."BUSDTA"."F4092"."GPGPC" = inserted."GPGPC";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_upd', 'DROP TRIGGER "BUSDTA"."F4092_upd"', '4b13910e8fce947a34af6bc24a6779b4', 0
END;
/* Create the shadow delete trigger 'F4092_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_dlt', '3cbd9407dc92109aa02103cc17cd085f';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_dlt'
	DROP TRIGGER "BUSDTA"."F4092_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4092_dlt"
		ON "MobileDataModel"."BUSDTA"."F4092" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4092_del" ( "GPGPTY", "GPGPC", "last_modified" )
			SELECT deleted."GPGPTY",
				deleted."GPGPC",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4092', 'F4092_dlt', 'DROP TRIGGER "BUSDTA"."F4092_dlt"', '3cbd9407dc92109aa02103cc17cd085f', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4092', 'F4092_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4092', 'F4092_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4092', 'F4092_ml'
	DROP INDEX "F4092"."F4092_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4092', 'F4092_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4092_ml" ON "MobileDataModel"."BUSDTA"."F4092"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4092'', ''F4092_ml'', ''DROP INDEX "F4092"."F4092_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4092_del', 'F4092_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4092_del', 'F4092_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4092_del', 'F4092_mld'
	DROP INDEX "F4092_del"."F4092_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4092_del', 'F4092_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4092_mld" ON "MobileDataModel"."BUSDTA"."F4092_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4092_del'', ''F4092_mld'', ''DROP INDEX "F4092_del"."F4092_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F40941'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F40941_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40941_del', 'F40941_del', '16f17fc42db9c2c53e895a5e256f06a0'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40941_del', 'F40941_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F40941_del', 'F40941_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F40941_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40941_del', 'F40941_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F40941_del" (
		"IKPRGR" nchar(8) not null,
		"IKIGP1" nchar(6) not null,
		"IKIGP2" nchar(6) not null,
		"IKIGP3" nchar(6) not null,
		"IKIGP4" nchar(6) not null,
		"IKIGP5" nchar(6) not null,
		"IKIGP6" nchar(6) not null,
		"IKIGP7" nchar(6) not null,
		"IKIGP8" nchar(6) not null,
		"IKIGP9" nchar(6) not null,
		"IKIGP10" nchar(6) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("IKPRGR", "IKIGP1", "IKIGP2", "IKIGP3", "IKIGP4", "IKIGP5", "IKIGP6", "IKIGP7", "IKIGP8", "IKIGP9", "IKIGP10")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F40941_del', 'F40941_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F40941_del"', '16f17fc42db9c2c53e895a5e256f06a0', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40941', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40941', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F40941', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F40941" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40941', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F40941" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F40941', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F40941" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F40941_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_ins', 'e0a6e95b93cabe307896a51434eb70ed';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_ins'
	DROP TRIGGER "BUSDTA"."F40941_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40941_ins"
		ON "MobileDataModel"."BUSDTA"."F40941" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F40941_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F40941_del"."IKPRGR" = inserted."IKPRGR" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP1" = inserted."IKIGP1" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP2" = inserted."IKIGP2" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP3" = inserted."IKIGP3" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP4" = inserted."IKIGP4" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP5" = inserted."IKIGP5" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP6" = inserted."IKIGP6" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP7" = inserted."IKIGP7" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP8" = inserted."IKIGP8" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP9" = inserted."IKIGP9" AND
					"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP10" = inserted."IKIGP10"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_ins', 'DROP TRIGGER "BUSDTA"."F40941_ins"', 'e0a6e95b93cabe307896a51434eb70ed', 0
END;
/* Create the timestamp trigger 'F40941_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_upd', 'f6fd8d4bc31f83debad18dfa9b14fc0d';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_upd'
	DROP TRIGGER "BUSDTA"."F40941_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40941_upd"
		ON "MobileDataModel"."BUSDTA"."F40941" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F40941"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F40941"."IKPRGR" = inserted."IKPRGR" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP1" = inserted."IKIGP1" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP2" = inserted."IKIGP2" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP3" = inserted."IKIGP3" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP4" = inserted."IKIGP4" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP5" = inserted."IKIGP5" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP6" = inserted."IKIGP6" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP7" = inserted."IKIGP7" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP8" = inserted."IKIGP8" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP9" = inserted."IKIGP9" AND
				"MobileDataModel"."BUSDTA"."F40941"."IKIGP10" = inserted."IKIGP10";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_upd', 'DROP TRIGGER "BUSDTA"."F40941_upd"', 'f6fd8d4bc31f83debad18dfa9b14fc0d', 0
END;
/* Create the shadow delete trigger 'F40941_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_dlt', '5884e8f5f8878ff58496f706aeb13257';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_dlt'
	DROP TRIGGER "BUSDTA"."F40941_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40941_dlt"
		ON "MobileDataModel"."BUSDTA"."F40941" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F40941_del" ( "IKPRGR", "IKIGP1", "IKIGP2", "IKIGP3", "IKIGP4", "IKIGP5", "IKIGP6", "IKIGP7", "IKIGP8", "IKIGP9", "IKIGP10", "last_modified" )
			SELECT deleted."IKPRGR",
				deleted."IKIGP1",
				deleted."IKIGP2",
				deleted."IKIGP3",
				deleted."IKIGP4",
				deleted."IKIGP5",
				deleted."IKIGP6",
				deleted."IKIGP7",
				deleted."IKIGP8",
				deleted."IKIGP9",
				deleted."IKIGP10",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40941', 'F40941_dlt', 'DROP TRIGGER "BUSDTA"."F40941_dlt"', '5884e8f5f8878ff58496f706aeb13257', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40941', 'F40941_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40941', 'F40941_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F40941', 'F40941_ml'
	DROP INDEX "F40941"."F40941_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40941', 'F40941_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F40941_ml" ON "MobileDataModel"."BUSDTA"."F40941"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F40941'', ''F40941_ml'', ''DROP INDEX "F40941"."F40941_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40941_del', 'F40941_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40941_del', 'F40941_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F40941_del', 'F40941_mld'
	DROP INDEX "F40941_del"."F40941_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40941_del', 'F40941_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F40941_mld" ON "MobileDataModel"."BUSDTA"."F40941_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F40941_del'', ''F40941_mld'', ''DROP INDEX "F40941_del"."F40941_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F40942'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F40942_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40942_del', 'F40942_del', 'a6eae053d0d3f0ae47c27c55183cdd74'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40942_del', 'F40942_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F40942_del', 'F40942_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F40942_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F40942_del', 'F40942_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F40942_del" (
		"CKCPGP" nchar(8) not null,
		"CKCGP1" nchar(3) not null,
		"CKCGP2" nchar(3) not null,
		"CKCGP3" nchar(3) not null,
		"CKCGP4" nchar(3) not null,
		"CKCGP5" nchar(3) not null,
		"CKCGP6" nchar(3) not null,
		"CKCGP7" nchar(3) not null,
		"CKCGP8" nchar(3) not null,
		"CKCGP9" nchar(3) not null,
		"CKCGP10" nchar(3) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("CKCPGP", "CKCGP1", "CKCGP2", "CKCGP3", "CKCGP4", "CKCGP5", "CKCGP6", "CKCGP7", "CKCGP8", "CKCGP9", "CKCGP10")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F40942_del', 'F40942_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F40942_del"', 'a6eae053d0d3f0ae47c27c55183cdd74', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40942', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40942', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F40942', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F40942" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F40942', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F40942" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F40942', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F40942" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F40942_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_ins', 'aa854fcd0476dc6aa18578da94dca78f';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_ins'
	DROP TRIGGER "BUSDTA"."F40942_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40942_ins"
		ON "MobileDataModel"."BUSDTA"."F40942" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F40942_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F40942_del"."CKCPGP" = inserted."CKCPGP" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP1" = inserted."CKCGP1" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP2" = inserted."CKCGP2" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP3" = inserted."CKCGP3" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP4" = inserted."CKCGP4" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP5" = inserted."CKCGP5" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP6" = inserted."CKCGP6" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP7" = inserted."CKCGP7" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP8" = inserted."CKCGP8" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP9" = inserted."CKCGP9" AND
					"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP10" = inserted."CKCGP10"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_ins', 'DROP TRIGGER "BUSDTA"."F40942_ins"', 'aa854fcd0476dc6aa18578da94dca78f', 0
END;
/* Create the timestamp trigger 'F40942_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_upd', '5a7c933d2fbdd9a72ca9ff05f7860a4b';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_upd'
	DROP TRIGGER "BUSDTA"."F40942_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40942_upd"
		ON "MobileDataModel"."BUSDTA"."F40942" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F40942"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F40942"."CKCPGP" = inserted."CKCPGP" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP1" = inserted."CKCGP1" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP2" = inserted."CKCGP2" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP3" = inserted."CKCGP3" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP4" = inserted."CKCGP4" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP5" = inserted."CKCGP5" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP6" = inserted."CKCGP6" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP7" = inserted."CKCGP7" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP8" = inserted."CKCGP8" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP9" = inserted."CKCGP9" AND
				"MobileDataModel"."BUSDTA"."F40942"."CKCGP10" = inserted."CKCGP10";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_upd', 'DROP TRIGGER "BUSDTA"."F40942_upd"', '5a7c933d2fbdd9a72ca9ff05f7860a4b', 0
END;
/* Create the shadow delete trigger 'F40942_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_dlt', 'b35d4fc20b1bf1dabb3259a2093491dc';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_dlt'
	DROP TRIGGER "BUSDTA"."F40942_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F40942_dlt"
		ON "MobileDataModel"."BUSDTA"."F40942" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F40942_del" ( "CKCPGP", "CKCGP1", "CKCGP2", "CKCGP3", "CKCGP4", "CKCGP5", "CKCGP6", "CKCGP7", "CKCGP8", "CKCGP9", "CKCGP10", "last_modified" )
			SELECT deleted."CKCPGP",
				deleted."CKCGP1",
				deleted."CKCGP2",
				deleted."CKCGP3",
				deleted."CKCGP4",
				deleted."CKCGP5",
				deleted."CKCGP6",
				deleted."CKCGP7",
				deleted."CKCGP8",
				deleted."CKCGP9",
				deleted."CKCGP10",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F40942', 'F40942_dlt', 'DROP TRIGGER "BUSDTA"."F40942_dlt"', 'b35d4fc20b1bf1dabb3259a2093491dc', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40942', 'F40942_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40942', 'F40942_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F40942', 'F40942_ml'
	DROP INDEX "F40942"."F40942_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40942', 'F40942_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F40942_ml" ON "MobileDataModel"."BUSDTA"."F40942"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F40942'', ''F40942_ml'', ''DROP INDEX "F40942"."F40942_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40942_del', 'F40942_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40942_del', 'F40942_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F40942_del', 'F40942_mld'
	DROP INDEX "F40942_del"."F40942_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F40942_del', 'F40942_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F40942_mld" ON "MobileDataModel"."BUSDTA"."F40942_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F40942_del'', ''F40942_mld'', ''DROP INDEX "F40942_del"."F40942_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F41002'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F41002_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F41002_del', 'F41002_del', '845bb57548fcf82c4b39c09042a8f3e1'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F41002_del', 'F41002_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F41002_del', 'F41002_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F41002_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F41002_del', 'F41002_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F41002_del" (
		"UMMCU" nchar(12) not null,
		"UMITM" numeric(8, 0) not null,
		"UMUM" nchar(2) not null,
		"UMRUM" nchar(2) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("UMMCU", "UMITM", "UMUM", "UMRUM")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F41002_del', 'F41002_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F41002_del"', '845bb57548fcf82c4b39c09042a8f3e1', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F41002', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F41002', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F41002', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F41002" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F41002', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F41002" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F41002', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F41002" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F41002_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_ins', '04f5efc9077111f4cf4290947dc9e70c';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_ins'
	DROP TRIGGER "BUSDTA"."F41002_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F41002_ins"
		ON "MobileDataModel"."BUSDTA"."F41002" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F41002_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F41002_del"."UMMCU" = inserted."UMMCU" AND
					"MobileDataModel"."BUSDTA"."F41002_del"."UMITM" = inserted."UMITM" AND
					"MobileDataModel"."BUSDTA"."F41002_del"."UMUM" = inserted."UMUM" AND
					"MobileDataModel"."BUSDTA"."F41002_del"."UMRUM" = inserted."UMRUM"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_ins', 'DROP TRIGGER "BUSDTA"."F41002_ins"', '04f5efc9077111f4cf4290947dc9e70c', 0
END;
/* Create the timestamp trigger 'F41002_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_upd', 'f73e98f6674908db87573f8b29d83a4a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_upd'
	DROP TRIGGER "BUSDTA"."F41002_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F41002_upd"
		ON "MobileDataModel"."BUSDTA"."F41002" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F41002"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F41002"."UMMCU" = inserted."UMMCU" AND
				"MobileDataModel"."BUSDTA"."F41002"."UMITM" = inserted."UMITM" AND
				"MobileDataModel"."BUSDTA"."F41002"."UMUM" = inserted."UMUM" AND
				"MobileDataModel"."BUSDTA"."F41002"."UMRUM" = inserted."UMRUM";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_upd', 'DROP TRIGGER "BUSDTA"."F41002_upd"', 'f73e98f6674908db87573f8b29d83a4a', 0
END;
/* Create the shadow delete trigger 'F41002_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_dlt', 'ffec396e944e52702e0d5d12e4ad03c7';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_dlt'
	DROP TRIGGER "BUSDTA"."F41002_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F41002_dlt"
		ON "MobileDataModel"."BUSDTA"."F41002" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F41002_del" ( "UMMCU", "UMITM", "UMUM", "UMRUM", "last_modified" )
			SELECT deleted."UMMCU",
				deleted."UMITM",
				deleted."UMUM",
				deleted."UMRUM",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F41002', 'F41002_dlt', 'DROP TRIGGER "BUSDTA"."F41002_dlt"', 'ffec396e944e52702e0d5d12e4ad03c7', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F41002', 'F41002_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F41002', 'F41002_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F41002', 'F41002_ml'
	DROP INDEX "F41002"."F41002_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F41002', 'F41002_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F41002_ml" ON "MobileDataModel"."BUSDTA"."F41002"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F41002'', ''F41002_ml'', ''DROP INDEX "F41002"."F41002_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F41002_del', 'F41002_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F41002_del', 'F41002_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F41002_del', 'F41002_mld'
	DROP INDEX "F41002_del"."F41002_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F41002_del', 'F41002_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F41002_mld" ON "MobileDataModel"."BUSDTA"."F41002_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F41002_del'', ''F41002_mld'', ''DROP INDEX "F41002_del"."F41002_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4101'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4101_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4101_del', 'F4101_del', '831de4a715b4a9eb6685d691159e2c89'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4101_del', 'F4101_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4101_del', 'F4101_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4101_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4101_del', 'F4101_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4101_del" (
		"IMITM" numeric(8, 0) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("IMITM")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4101_del', 'F4101_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4101_del"', '831de4a715b4a9eb6685d691159e2c89', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4101', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4101', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4101', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4101" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4101', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4101" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4101', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4101" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4101_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_ins', '78549e19df9c92e6306c8b74b2205387';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_ins'
	DROP TRIGGER "BUSDTA"."F4101_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4101_ins"
		ON "MobileDataModel"."BUSDTA"."F4101" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4101_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4101_del"."IMITM" = inserted."IMITM"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_ins', 'DROP TRIGGER "BUSDTA"."F4101_ins"', '78549e19df9c92e6306c8b74b2205387', 0
END;
/* Create the timestamp trigger 'F4101_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_upd', '41c595d459faa442219fc992639a440a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_upd'
	DROP TRIGGER "BUSDTA"."F4101_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4101_upd"
		ON "MobileDataModel"."BUSDTA"."F4101" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4101"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4101"."IMITM" = inserted."IMITM";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_upd', 'DROP TRIGGER "BUSDTA"."F4101_upd"', '41c595d459faa442219fc992639a440a', 0
END;
/* Create the shadow delete trigger 'F4101_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_dlt', '9c9c2f1a0197c296a0ff7185aca56b25';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_dlt'
	DROP TRIGGER "BUSDTA"."F4101_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4101_dlt"
		ON "MobileDataModel"."BUSDTA"."F4101" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4101_del" ( "IMITM", "last_modified" )
			SELECT deleted."IMITM",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4101', 'F4101_dlt', 'DROP TRIGGER "BUSDTA"."F4101_dlt"', '9c9c2f1a0197c296a0ff7185aca56b25', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4101', 'F4101_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4101', 'F4101_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4101', 'F4101_ml'
	DROP INDEX "F4101"."F4101_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4101', 'F4101_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4101_ml" ON "MobileDataModel"."BUSDTA"."F4101"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4101'', ''F4101_ml'', ''DROP INDEX "F4101"."F4101_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4101_del', 'F4101_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4101_del', 'F4101_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4101_del', 'F4101_mld'
	DROP INDEX "F4101_del"."F4101_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4101_del', 'F4101_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4101_mld" ON "MobileDataModel"."BUSDTA"."F4101_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4101_del'', ''F4101_mld'', ''DROP INDEX "F4101_del"."F4101_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4102'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4102_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4102_del', 'F4102_del', '1ba06f531d1c5d337a181d2d7aa2e612'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4102_del', 'F4102_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4102_del', 'F4102_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4102_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4102_del', 'F4102_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4102_del" (
		"IBITM" numeric(8, 0) not null,
		"IBMCU" nchar(12) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("IBITM", "IBMCU")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4102_del', 'F4102_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4102_del"', '1ba06f531d1c5d337a181d2d7aa2e612', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4102', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4102', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4102', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4102" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4102', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4102" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4102', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4102" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4102_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_ins', '5b94d218f6e3479e0ee7f03b56f20bd5';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_ins'
	DROP TRIGGER "BUSDTA"."F4102_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4102_ins"
		ON "MobileDataModel"."BUSDTA"."F4102" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4102_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4102_del"."IBITM" = inserted."IBITM" AND
					"MobileDataModel"."BUSDTA"."F4102_del"."IBMCU" = inserted."IBMCU"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_ins', 'DROP TRIGGER "BUSDTA"."F4102_ins"', '5b94d218f6e3479e0ee7f03b56f20bd5', 0
END;
/* Create the timestamp trigger 'F4102_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_upd', '845569ead00dc2e2c1c604c2f3db3808';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_upd'
	DROP TRIGGER "BUSDTA"."F4102_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4102_upd"
		ON "MobileDataModel"."BUSDTA"."F4102" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4102"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4102"."IBITM" = inserted."IBITM" AND
				"MobileDataModel"."BUSDTA"."F4102"."IBMCU" = inserted."IBMCU";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_upd', 'DROP TRIGGER "BUSDTA"."F4102_upd"', '845569ead00dc2e2c1c604c2f3db3808', 0
END;
/* Create the shadow delete trigger 'F4102_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_dlt', '4da2a3c1c8b3a0899f46e4d4c2104114';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_dlt'
	DROP TRIGGER "BUSDTA"."F4102_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4102_dlt"
		ON "MobileDataModel"."BUSDTA"."F4102" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4102_del" ( "IBITM", "IBMCU", "last_modified" )
			SELECT deleted."IBITM",
				deleted."IBMCU",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4102', 'F4102_dlt', 'DROP TRIGGER "BUSDTA"."F4102_dlt"', '4da2a3c1c8b3a0899f46e4d4c2104114', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4102', 'F4102_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4102', 'F4102_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4102', 'F4102_ml'
	DROP INDEX "F4102"."F4102_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4102', 'F4102_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4102_ml" ON "MobileDataModel"."BUSDTA"."F4102"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4102'', ''F4102_ml'', ''DROP INDEX "F4102"."F4102_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4102_del', 'F4102_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4102_del', 'F4102_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4102_del', 'F4102_mld'
	DROP INDEX "F4102_del"."F4102_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4102_del', 'F4102_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4102_mld" ON "MobileDataModel"."BUSDTA"."F4102_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4102_del'', ''F4102_mld'', ''DROP INDEX "F4102_del"."F4102_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F4106'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F4106_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4106_del', 'F4106_del', '8b5fca984d670fffee002214da297201'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4106_del', 'F4106_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F4106_del', 'F4106_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F4106_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F4106_del', 'F4106_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F4106_del" (
		"BPITM" numeric(8, 0) not null,
		"BPMCU" nchar(12) not null,
		"BPLOCN" nchar(20) not null,
		"BPLOTN" nchar(30) not null,
		"BPAN8" numeric(8, 0) not null,
		"BPIGID" numeric(8, 0) not null,
		"BPCGID" numeric(8, 0) not null,
		"BPLOTG" nchar(3) not null,
		"BPFRMP" numeric(7, 0) not null,
		"BPCRCD" nchar(3) not null,
		"BPUOM" nchar(2) not null,
		"BPEXDJ" numeric(18, 0) not null,
		"BPUPMJ" numeric(18, 0) not null,
		"BPTDAY" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("BPITM", "BPMCU", "BPLOCN", "BPLOTN", "BPAN8", "BPIGID", "BPCGID", "BPLOTG", "BPFRMP", "BPCRCD", "BPUOM", "BPEXDJ", "BPUPMJ", "BPTDAY")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F4106_del', 'F4106_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F4106_del"', '8b5fca984d670fffee002214da297201', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4106', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4106', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F4106', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4106" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F4106', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F4106" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F4106', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F4106" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F4106_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_ins', 'f4ec446b24ec391d43a30eca601c07a8';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_ins'
	DROP TRIGGER "BUSDTA"."F4106_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4106_ins"
		ON "MobileDataModel"."BUSDTA"."F4106" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F4106_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F4106_del"."BPITM" = inserted."BPITM" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPMCU" = inserted."BPMCU" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPLOCN" = inserted."BPLOCN" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPLOTN" = inserted."BPLOTN" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPAN8" = inserted."BPAN8" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPIGID" = inserted."BPIGID" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPCGID" = inserted."BPCGID" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPLOTG" = inserted."BPLOTG" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPFRMP" = inserted."BPFRMP" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPCRCD" = inserted."BPCRCD" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPUOM" = inserted."BPUOM" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPEXDJ" = inserted."BPEXDJ" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPUPMJ" = inserted."BPUPMJ" AND
					"MobileDataModel"."BUSDTA"."F4106_del"."BPTDAY" = inserted."BPTDAY"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_ins', 'DROP TRIGGER "BUSDTA"."F4106_ins"', 'f4ec446b24ec391d43a30eca601c07a8', 0
END;
/* Create the timestamp trigger 'F4106_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_upd', 'd18dc39dcedf656801551452a210818a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_upd'
	DROP TRIGGER "BUSDTA"."F4106_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4106_upd"
		ON "MobileDataModel"."BUSDTA"."F4106" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F4106"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F4106"."BPITM" = inserted."BPITM" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPMCU" = inserted."BPMCU" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPLOCN" = inserted."BPLOCN" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPLOTN" = inserted."BPLOTN" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPAN8" = inserted."BPAN8" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPIGID" = inserted."BPIGID" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPCGID" = inserted."BPCGID" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPLOTG" = inserted."BPLOTG" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPFRMP" = inserted."BPFRMP" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPCRCD" = inserted."BPCRCD" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPUOM" = inserted."BPUOM" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPEXDJ" = inserted."BPEXDJ" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPUPMJ" = inserted."BPUPMJ" AND
				"MobileDataModel"."BUSDTA"."F4106"."BPTDAY" = inserted."BPTDAY";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_upd', 'DROP TRIGGER "BUSDTA"."F4106_upd"', 'd18dc39dcedf656801551452a210818a', 0
END;
/* Create the shadow delete trigger 'F4106_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_dlt', '741c0a494235d112b5c475408ea2db2a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_dlt'
	DROP TRIGGER "BUSDTA"."F4106_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F4106_dlt"
		ON "MobileDataModel"."BUSDTA"."F4106" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F4106_del" ( "BPITM", "BPMCU", "BPLOCN", "BPLOTN", "BPAN8", "BPIGID", "BPCGID", "BPLOTG", "BPFRMP", "BPCRCD", "BPUOM", "BPEXDJ", "BPUPMJ", "BPTDAY", "last_modified" )
			SELECT deleted."BPITM",
				deleted."BPMCU",
				deleted."BPLOCN",
				deleted."BPLOTN",
				deleted."BPAN8",
				deleted."BPIGID",
				deleted."BPCGID",
				deleted."BPLOTG",
				deleted."BPFRMP",
				deleted."BPCRCD",
				deleted."BPUOM",
				deleted."BPEXDJ",
				deleted."BPUPMJ",
				deleted."BPTDAY",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F4106', 'F4106_dlt', 'DROP TRIGGER "BUSDTA"."F4106_dlt"', '741c0a494235d112b5c475408ea2db2a', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4106', 'F4106_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4106', 'F4106_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4106', 'F4106_ml'
	DROP INDEX "F4106"."F4106_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4106', 'F4106_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4106_ml" ON "MobileDataModel"."BUSDTA"."F4106"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4106'', ''F4106_ml'', ''DROP INDEX "F4106"."F4106_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4106_del', 'F4106_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4106_del', 'F4106_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F4106_del', 'F4106_mld'
	DROP INDEX "F4106_del"."F4106_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F4106_del', 'F4106_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F4106_mld" ON "MobileDataModel"."BUSDTA"."F4106_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F4106_del'', ''F4106_mld'', ''DROP INDEX "F4106_del"."F4106_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F42019'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F42019_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F42019_del', 'F42019_del', 'b1f7d2494e525d4aaf94fddc46810f8b'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F42019_del', 'F42019_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F42019_del', 'F42019_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F42019_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F42019_del', 'F42019_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F42019_del" (
		"SHKCOO" nchar(5) not null,
		"SHDOCO" numeric(8, 0) not null,
		"SHDCTO" nchar(2) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("SHKCOO", "SHDOCO", "SHDCTO")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F42019_del', 'F42019_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F42019_del"', 'b1f7d2494e525d4aaf94fddc46810f8b', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F42019', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F42019', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F42019', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F42019" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F42019', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F42019" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F42019', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F42019" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F42019_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_ins', 'd0ec75c34001e7ca3aa4a8777fd77407';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_ins'
	DROP TRIGGER "BUSDTA"."F42019_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F42019_ins"
		ON "MobileDataModel"."BUSDTA"."F42019" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F42019_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F42019_del"."SHKCOO" = inserted."SHKCOO" AND
					"MobileDataModel"."BUSDTA"."F42019_del"."SHDOCO" = inserted."SHDOCO" AND
					"MobileDataModel"."BUSDTA"."F42019_del"."SHDCTO" = inserted."SHDCTO"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_ins', 'DROP TRIGGER "BUSDTA"."F42019_ins"', 'd0ec75c34001e7ca3aa4a8777fd77407', 0
END;
/* Create the timestamp trigger 'F42019_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_upd', '47b44e65ac2319d8a0a3b128039dcf23';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_upd'
	DROP TRIGGER "BUSDTA"."F42019_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F42019_upd"
		ON "MobileDataModel"."BUSDTA"."F42019" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F42019"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F42019"."SHKCOO" = inserted."SHKCOO" AND
				"MobileDataModel"."BUSDTA"."F42019"."SHDOCO" = inserted."SHDOCO" AND
				"MobileDataModel"."BUSDTA"."F42019"."SHDCTO" = inserted."SHDCTO";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_upd', 'DROP TRIGGER "BUSDTA"."F42019_upd"', '47b44e65ac2319d8a0a3b128039dcf23', 0
END;
/* Create the shadow delete trigger 'F42019_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_dlt', '96bc295dbd2e72263766ff67b78203c2';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_dlt'
	DROP TRIGGER "BUSDTA"."F42019_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F42019_dlt"
		ON "MobileDataModel"."BUSDTA"."F42019" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F42019_del" ( "SHKCOO", "SHDOCO", "SHDCTO", "last_modified" )
			SELECT deleted."SHKCOO",
				deleted."SHDOCO",
				deleted."SHDCTO",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F42019', 'F42019_dlt', 'DROP TRIGGER "BUSDTA"."F42019_dlt"', '96bc295dbd2e72263766ff67b78203c2', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42019', 'F42019_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42019', 'F42019_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F42019', 'F42019_ml'
	DROP INDEX "F42019"."F42019_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42019', 'F42019_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F42019_ml" ON "MobileDataModel"."BUSDTA"."F42019"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F42019'', ''F42019_ml'', ''DROP INDEX "F42019"."F42019_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42019_del', 'F42019_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42019_del', 'F42019_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F42019_del', 'F42019_mld'
	DROP INDEX "F42019_del"."F42019_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42019_del', 'F42019_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F42019_mld" ON "MobileDataModel"."BUSDTA"."F42019_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F42019_del'', ''F42019_mld'', ''DROP INDEX "F42019_del"."F42019_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.F42119'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F42119_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F42119_del', 'F42119_del', 'e5f7844f109d11c425d4608dd14e6639'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F42119_del', 'F42119_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F42119_del', 'F42119_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F42119_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F42119_del', 'F42119_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F42119_del" (
		"SDKCOO" nchar(5) not null,
		"SDDOCO" numeric(8, 0) not null,
		"SDDCTO" nchar(2) not null,
		"SDLNID" numeric(7, 0) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("SDKCOO", "SDDOCO", "SDDCTO", "SDLNID")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F42119_del', 'F42119_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F42119_del"', 'e5f7844f109d11c425d4608dd14e6639', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F42119', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F42119', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F42119', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F42119" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F42119', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F42119" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F42119', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F42119" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F42119_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_ins', '4e671e3efed69c9c10b4c299c5fabd71';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_ins'
	DROP TRIGGER "BUSDTA"."F42119_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F42119_ins"
		ON "MobileDataModel"."BUSDTA"."F42119" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F42119_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F42119_del"."SDKCOO" = inserted."SDKCOO" AND
					"MobileDataModel"."BUSDTA"."F42119_del"."SDDOCO" = inserted."SDDOCO" AND
					"MobileDataModel"."BUSDTA"."F42119_del"."SDDCTO" = inserted."SDDCTO" AND
					"MobileDataModel"."BUSDTA"."F42119_del"."SDLNID" = inserted."SDLNID"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_ins', 'DROP TRIGGER "BUSDTA"."F42119_ins"', '4e671e3efed69c9c10b4c299c5fabd71', 0
END;
/* Create the timestamp trigger 'F42119_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_upd', '8325d8652382f43d68aafbfb3c980236';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_upd'
	DROP TRIGGER "BUSDTA"."F42119_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F42119_upd"
		ON "MobileDataModel"."BUSDTA"."F42119" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F42119"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F42119"."SDKCOO" = inserted."SDKCOO" AND
				"MobileDataModel"."BUSDTA"."F42119"."SDDOCO" = inserted."SDDOCO" AND
				"MobileDataModel"."BUSDTA"."F42119"."SDDCTO" = inserted."SDDCTO" AND
				"MobileDataModel"."BUSDTA"."F42119"."SDLNID" = inserted."SDLNID";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_upd', 'DROP TRIGGER "BUSDTA"."F42119_upd"', '8325d8652382f43d68aafbfb3c980236', 0
END;
/* Create the shadow delete trigger 'F42119_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_dlt', '261a1209c5f4753c9b6cae600baef233';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_dlt'
	DROP TRIGGER "BUSDTA"."F42119_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F42119_dlt"
		ON "MobileDataModel"."BUSDTA"."F42119" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F42119_del" ( "SDKCOO", "SDDOCO", "SDDCTO", "SDLNID", "last_modified" )
			SELECT deleted."SDKCOO",
				deleted."SDDOCO",
				deleted."SDDCTO",
				deleted."SDLNID",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F42119', 'F42119_dlt', 'DROP TRIGGER "BUSDTA"."F42119_dlt"', '261a1209c5f4753c9b6cae600baef233', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42119', 'F42119_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42119', 'F42119_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F42119', 'F42119_ml'
	DROP INDEX "F42119"."F42119_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42119', 'F42119_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F42119_ml" ON "MobileDataModel"."BUSDTA"."F42119"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F42119'', ''F42119_ml'', ''DROP INDEX "F42119"."F42119_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42119_del', 'F42119_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42119_del', 'F42119_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F42119_del', 'F42119_mld'
	DROP INDEX "F42119_del"."F42119_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F42119_del', 'F42119_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F42119_mld" ON "MobileDataModel"."BUSDTA"."F42119_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F42119_del'', ''F42119_mld'', ''DROP INDEX "F42119_del"."F42119_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*    Creating schema related to the table 'MobileDataModel.BUSDTA.F56M0000'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F56M0000_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F56M0000_del', 'F56M0000_del', '91e5448708e3dbec8f8cdcc55fd0fbc0'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F56M0000_del', 'F56M0000_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F56M0000_del', 'F56M0000_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F56M0000_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F56M0000_del', 'F56M0000_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F56M0000_del" (
		"GFSY" nchar(4) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("GFSY")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F56M0000_del', 'F56M0000_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F56M0000_del"', '91e5448708e3dbec8f8cdcc55fd0fbc0', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F56M0000', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F56M0000', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F56M0000', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F56M0000" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F56M0000', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F56M0000" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F56M0000', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F56M0000" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F56M0000_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_ins', 'bb267e809de0679a1d087a85b4cef3ba';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_ins'
	DROP TRIGGER "BUSDTA"."F56M0000_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F56M0000_ins"
		ON "MobileDataModel"."BUSDTA"."F56M0000" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F56M0000_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F56M0000_del"."GFSY" = inserted."GFSY"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_ins', 'DROP TRIGGER "BUSDTA"."F56M0000_ins"', 'bb267e809de0679a1d087a85b4cef3ba', 0
END;
/* Create the timestamp trigger 'F56M0000_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_upd', 'e4e0c150f78cad868119b4409b4f3bd7';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_upd'
	DROP TRIGGER "BUSDTA"."F56M0000_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F56M0000_upd"
		ON "MobileDataModel"."BUSDTA"."F56M0000" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F56M0000"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F56M0000"."GFSY" = inserted."GFSY";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_upd', 'DROP TRIGGER "BUSDTA"."F56M0000_upd"', 'e4e0c150f78cad868119b4409b4f3bd7', 0
END;
/* Create the shadow delete trigger 'F56M0000_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_dlt', '9d3da9fee9b55b1cde1ce099b240f091';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_dlt'
	DROP TRIGGER "BUSDTA"."F56M0000_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F56M0000_dlt"
		ON "MobileDataModel"."BUSDTA"."F56M0000" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F56M0000_del" ( "GFSY", "last_modified" )
			SELECT deleted."GFSY",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F56M0000', 'F56M0000_dlt', 'DROP TRIGGER "BUSDTA"."F56M0000_dlt"', '9d3da9fee9b55b1cde1ce099b240f091', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0000', 'F56M0000_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0000', 'F56M0000_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F56M0000', 'F56M0000_ml'
	DROP INDEX "F56M0000"."F56M0000_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0000', 'F56M0000_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F56M0000_ml" ON "MobileDataModel"."BUSDTA"."F56M0000"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F56M0000'', ''F56M0000_ml'', ''DROP INDEX "F56M0000"."F56M0000_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0000_del', 'F56M0000_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0000_del', 'F56M0000_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F56M0000_del', 'F56M0000_mld'
	DROP INDEX "F56M0000_del"."F56M0000_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0000_del', 'F56M0000_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F56M0000_mld" ON "MobileDataModel"."BUSDTA"."F56M0000_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F56M0000_del'', ''F56M0000_mld'', ''DROP INDEX "F56M0000_del"."F56M0000_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*    Creating schema related to the table 'MobileDataModel.BUSDTA.F56M0001'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F56M0001_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F56M0001_del', 'F56M0001_del', '7aae7284878a170fd80856f6d80f7660'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F56M0001_del', 'F56M0001_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F56M0001_del', 'F56M0001_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F56M0001_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F56M0001_del', 'F56M0001_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F56M0001_del" (
		"FFUSER" nchar(10) not null,
		"FFROUT" nchar(3) not null,
		"FFMCU" nchar(12) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("FFUSER", "FFROUT", "FFMCU")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F56M0001_del', 'F56M0001_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F56M0001_del"', '7aae7284878a170fd80856f6d80f7660', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F56M0001', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F56M0001', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F56M0001', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F56M0001" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F56M0001', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F56M0001" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F56M0001', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F56M0001" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F56M0001_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_ins', '01d17011ec24d3db8ba9711f113673c2';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_ins'
	DROP TRIGGER "BUSDTA"."F56M0001_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F56M0001_ins"
		ON "MobileDataModel"."BUSDTA"."F56M0001" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F56M0001_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F56M0001_del"."FFUSER" = inserted."FFUSER" AND
					"MobileDataModel"."BUSDTA"."F56M0001_del"."FFROUT" = inserted."FFROUT" AND
					"MobileDataModel"."BUSDTA"."F56M0001_del"."FFMCU" = inserted."FFMCU"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_ins', 'DROP TRIGGER "BUSDTA"."F56M0001_ins"', '01d17011ec24d3db8ba9711f113673c2', 0
END;
/* Create the timestamp trigger 'F56M0001_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_upd', '3d9f032a49ca3e0349e7861e614bb791';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_upd'
	DROP TRIGGER "BUSDTA"."F56M0001_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F56M0001_upd"
		ON "MobileDataModel"."BUSDTA"."F56M0001" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F56M0001"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F56M0001"."FFUSER" = inserted."FFUSER" AND
				"MobileDataModel"."BUSDTA"."F56M0001"."FFROUT" = inserted."FFROUT" AND
				"MobileDataModel"."BUSDTA"."F56M0001"."FFMCU" = inserted."FFMCU";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_upd', 'DROP TRIGGER "BUSDTA"."F56M0001_upd"', '3d9f032a49ca3e0349e7861e614bb791', 0
END;
/* Create the shadow delete trigger 'F56M0001_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_dlt', '91872e0e99fc9fe450cdf967fb3a76f4';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_dlt'
	DROP TRIGGER "BUSDTA"."F56M0001_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F56M0001_dlt"
		ON "MobileDataModel"."BUSDTA"."F56M0001" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F56M0001_del" ( "FFUSER", "FFROUT", "FFMCU", "last_modified" )
			SELECT deleted."FFUSER",
				deleted."FFROUT",
				deleted."FFMCU",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F56M0001', 'F56M0001_dlt', 'DROP TRIGGER "BUSDTA"."F56M0001_dlt"', '91872e0e99fc9fe450cdf967fb3a76f4', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0001', 'F56M0001_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0001', 'F56M0001_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F56M0001', 'F56M0001_ml'
	DROP INDEX "F56M0001"."F56M0001_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0001', 'F56M0001_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F56M0001_ml" ON "MobileDataModel"."BUSDTA"."F56M0001"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F56M0001'', ''F56M0001_ml'', ''DROP INDEX "F56M0001"."F56M0001_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0001_del', 'F56M0001_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0001_del', 'F56M0001_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F56M0001_del', 'F56M0001_mld'
	DROP INDEX "F56M0001_del"."F56M0001_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F56M0001_del', 'F56M0001_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F56M0001_mld" ON "MobileDataModel"."BUSDTA"."F56M0001_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F56M0001_del'', ''F56M0001_mld'', ''DROP INDEX "F56M0001_del"."F56M0001_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*    Creating schema related to the table 'MobileDataModel.BUSDTA.F90CA003'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F90CA003_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA003_del', 'F90CA003_del', '73c8234d01bd7a9d2951c263483e89e6'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA003_del', 'F90CA003_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F90CA003_del', 'F90CA003_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F90CA003_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA003_del', 'F90CA003_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F90CA003_del" (
		"SMAN8" float not null,
		"SMSLSM" float not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("SMAN8", "SMSLSM")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F90CA003_del', 'F90CA003_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F90CA003_del"', '73c8234d01bd7a9d2951c263483e89e6', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA003', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA003', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F90CA003', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA003" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA003', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA003" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F90CA003', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA003" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F90CA003_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_ins', '216a54edca2a756f2a5c4782d2fabc20';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_ins'
	DROP TRIGGER "BUSDTA"."F90CA003_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA003_ins"
		ON "MobileDataModel"."BUSDTA"."F90CA003" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F90CA003_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F90CA003_del"."SMAN8" = inserted."SMAN8" AND
					"MobileDataModel"."BUSDTA"."F90CA003_del"."SMSLSM" = inserted."SMSLSM"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_ins', 'DROP TRIGGER "BUSDTA"."F90CA003_ins"', '216a54edca2a756f2a5c4782d2fabc20', 0
END;
/* Create the timestamp trigger 'F90CA003_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_upd', 'f39c3134546711e4b89581f665a6dad5';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_upd'
	DROP TRIGGER "BUSDTA"."F90CA003_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA003_upd"
		ON "MobileDataModel"."BUSDTA"."F90CA003" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F90CA003"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F90CA003"."SMAN8" = inserted."SMAN8" AND
				"MobileDataModel"."BUSDTA"."F90CA003"."SMSLSM" = inserted."SMSLSM";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_upd', 'DROP TRIGGER "BUSDTA"."F90CA003_upd"', 'f39c3134546711e4b89581f665a6dad5', 0
END;
/* Create the shadow delete trigger 'F90CA003_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_dlt', 'fbabbffc66c5c3c51ba3ad9db9a956b6';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_dlt'
	DROP TRIGGER "BUSDTA"."F90CA003_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA003_dlt"
		ON "MobileDataModel"."BUSDTA"."F90CA003" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F90CA003_del" ( "SMAN8", "SMSLSM", "last_modified" )
			SELECT deleted."SMAN8",
				deleted."SMSLSM",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA003', 'F90CA003_dlt', 'DROP TRIGGER "BUSDTA"."F90CA003_dlt"', 'fbabbffc66c5c3c51ba3ad9db9a956b6', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA003', 'F90CA003_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA003', 'F90CA003_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F90CA003', 'F90CA003_ml'
	DROP INDEX "F90CA003"."F90CA003_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA003', 'F90CA003_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F90CA003_ml" ON "MobileDataModel"."BUSDTA"."F90CA003"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F90CA003'', ''F90CA003_ml'', ''DROP INDEX "F90CA003"."F90CA003_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA003_del', 'F90CA003_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA003_del', 'F90CA003_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F90CA003_del', 'F90CA003_mld'
	DROP INDEX "F90CA003_del"."F90CA003_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA003_del', 'F90CA003_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F90CA003_mld" ON "MobileDataModel"."BUSDTA"."F90CA003_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F90CA003_del'', ''F90CA003_mld'', ''DROP INDEX "F90CA003_del"."F90CA003_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*    Creating schema related to the table 'MobileDataModel.BUSDTA.F90CA042'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F90CA042_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA042_del', 'F90CA042_del', '7ed4da9c1e10403f361747a02eebe301'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA042_del', 'F90CA042_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F90CA042_del', 'F90CA042_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F90CA042_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA042_del', 'F90CA042_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F90CA042_del" (
		"EMAN8" numeric(8, 0) not null,
		"EMPA8" numeric(8, 0) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("EMAN8", "EMPA8")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F90CA042_del', 'F90CA042_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F90CA042_del"', '7ed4da9c1e10403f361747a02eebe301', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA042', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA042', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F90CA042', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA042" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA042', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA042" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F90CA042', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA042" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F90CA042_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_ins', 'e8dee5056dcfe7bd3362bded69978e65';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_ins'
	DROP TRIGGER "BUSDTA"."F90CA042_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA042_ins"
		ON "MobileDataModel"."BUSDTA"."F90CA042" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F90CA042_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F90CA042_del"."EMAN8" = inserted."EMAN8" AND
					"MobileDataModel"."BUSDTA"."F90CA042_del"."EMPA8" = inserted."EMPA8"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_ins', 'DROP TRIGGER "BUSDTA"."F90CA042_ins"', 'e8dee5056dcfe7bd3362bded69978e65', 0
END;
/* Create the timestamp trigger 'F90CA042_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_upd', '4c60ed5857a48fffe5d46c22ac7ec626';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_upd'
	DROP TRIGGER "BUSDTA"."F90CA042_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA042_upd"
		ON "MobileDataModel"."BUSDTA"."F90CA042" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F90CA042"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F90CA042"."EMAN8" = inserted."EMAN8" AND
				"MobileDataModel"."BUSDTA"."F90CA042"."EMPA8" = inserted."EMPA8";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_upd', 'DROP TRIGGER "BUSDTA"."F90CA042_upd"', '4c60ed5857a48fffe5d46c22ac7ec626', 0
END;
/* Create the shadow delete trigger 'F90CA042_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_dlt', '4691af087d9a982f75066d2eea2cb977';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_dlt'
	DROP TRIGGER "BUSDTA"."F90CA042_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA042_dlt"
		ON "MobileDataModel"."BUSDTA"."F90CA042" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F90CA042_del" ( "EMAN8", "EMPA8", "last_modified" )
			SELECT deleted."EMAN8",
				deleted."EMPA8",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA042', 'F90CA042_dlt', 'DROP TRIGGER "BUSDTA"."F90CA042_dlt"', '4691af087d9a982f75066d2eea2cb977', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA042', 'F90CA042_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA042', 'F90CA042_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F90CA042', 'F90CA042_ml'
	DROP INDEX "F90CA042"."F90CA042_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA042', 'F90CA042_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F90CA042_ml" ON "MobileDataModel"."BUSDTA"."F90CA042"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F90CA042'', ''F90CA042_ml'', ''DROP INDEX "F90CA042"."F90CA042_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA042_del', 'F90CA042_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA042_del', 'F90CA042_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F90CA042_del', 'F90CA042_mld'
	DROP INDEX "F90CA042_del"."F90CA042_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA042_del', 'F90CA042_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F90CA042_mld" ON "MobileDataModel"."BUSDTA"."F90CA042_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F90CA042_del'', ''F90CA042_mld'', ''DROP INDEX "F90CA042_del"."F90CA042_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*    Creating schema related to the table 'MobileDataModel.BUSDTA.F90CA086'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.F90CA086_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA086_del', 'F90CA086_del', 'fc11de616e02874a368fd4e6b716d0c6'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA086_del', 'F90CA086_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'F90CA086_del', 'F90CA086_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."F90CA086_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'F90CA086_del', 'F90CA086_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."F90CA086_del" (
		"CRCUAN8" numeric(8, 0) not null,
		"CRCRAN8" numeric(8, 0) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("CRCUAN8", "CRCRAN8")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'F90CA086_del', 'F90CA086_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."F90CA086_del"', 'fc11de616e02874a368fd4e6b716d0c6', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA086', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA086', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'F90CA086', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA086" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'F90CA086', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA086" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'F90CA086', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."F90CA086" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'F90CA086_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_ins', '95773c03e722df03b05a358d7e9f2cc9';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_ins'
	DROP TRIGGER "BUSDTA"."F90CA086_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA086_ins"
		ON "MobileDataModel"."BUSDTA"."F90CA086" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."F90CA086_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."F90CA086_del"."CRCUAN8" = inserted."CRCUAN8" AND
					"MobileDataModel"."BUSDTA"."F90CA086_del"."CRCRAN8" = inserted."CRCRAN8"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_ins', 'DROP TRIGGER "BUSDTA"."F90CA086_ins"', '95773c03e722df03b05a358d7e9f2cc9', 0
END;
/* Create the timestamp trigger 'F90CA086_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_upd', 'e4dcb575d484745c2a755b821fdfd89c';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_upd'
	DROP TRIGGER "BUSDTA"."F90CA086_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA086_upd"
		ON "MobileDataModel"."BUSDTA"."F90CA086" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."F90CA086"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."F90CA086"."CRCUAN8" = inserted."CRCUAN8" AND
				"MobileDataModel"."BUSDTA"."F90CA086"."CRCRAN8" = inserted."CRCRAN8";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_upd', 'DROP TRIGGER "BUSDTA"."F90CA086_upd"', 'e4dcb575d484745c2a755b821fdfd89c', 0
END;
/* Create the shadow delete trigger 'F90CA086_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_dlt', '781eed013cb0a2f65d8644c0819988fa';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_dlt'
	DROP TRIGGER "BUSDTA"."F90CA086_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."F90CA086_dlt"
		ON "MobileDataModel"."BUSDTA"."F90CA086" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."F90CA086_del" ( "CRCUAN8", "CRCRAN8", "last_modified" )
			SELECT deleted."CRCUAN8",
				deleted."CRCRAN8",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'F90CA086', 'F90CA086_dlt', 'DROP TRIGGER "BUSDTA"."F90CA086_dlt"', '781eed013cb0a2f65d8644c0819988fa', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA086', 'F90CA086_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA086', 'F90CA086_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F90CA086', 'F90CA086_ml'
	DROP INDEX "F90CA086"."F90CA086_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA086', 'F90CA086_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F90CA086_ml" ON "MobileDataModel"."BUSDTA"."F90CA086"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F90CA086'', ''F90CA086_ml'', ''DROP INDEX "F90CA086"."F90CA086_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA086_del', 'F90CA086_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA086_del', 'F90CA086_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'F90CA086_del', 'F90CA086_mld'
	DROP INDEX "F90CA086_del"."F90CA086_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'F90CA086_del', 'F90CA086_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "F90CA086_mld" ON "MobileDataModel"."BUSDTA"."F90CA086_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''F90CA086_del'', ''F90CA086_mld'', ''DROP INDEX "F90CA086_del"."F90CA086_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*     Creating schema related to the table 'MobileDataModel.BUSDTA.M0111'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.M0111_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'M0111_del', 'M0111_del', '8be3676a14b74c1292cadbf2e2d436ce'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'M0111_del', 'M0111_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'M0111_del', 'M0111_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."M0111_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'M0111_del', 'M0111_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."M0111_del" (
		"CDAN8" numeric(8, 0) not null,
		"CDIDLN" numeric(5, 0) not null,
		"CDRCK7" numeric(5, 0) not null,
		"CDCNLN" numeric(5, 0) not null,
		"CDID" numeric(8, 0) not null,
		"CDACTV" bit null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("CDAN8", "CDIDLN", "CDRCK7", "CDCNLN", "CDID")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'M0111_del', 'M0111_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."M0111_del"', '8be3676a14b74c1292cadbf2e2d436ce', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'M0111', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'M0111', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'M0111', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."M0111" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'M0111', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."M0111" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'M0111', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."M0111" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'M0111_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_ins', '32d79b305591cee93de2579e3aa2f197';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_ins'
	DROP TRIGGER "BUSDTA"."M0111_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."M0111_ins"
		ON "MobileDataModel"."BUSDTA"."M0111" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."M0111_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."M0111_del"."CDAN8" = inserted."CDAN8" AND
					"MobileDataModel"."BUSDTA"."M0111_del"."CDIDLN" = inserted."CDIDLN" AND
					"MobileDataModel"."BUSDTA"."M0111_del"."CDRCK7" = inserted."CDRCK7" AND
					"MobileDataModel"."BUSDTA"."M0111_del"."CDCNLN" = inserted."CDCNLN" AND
					"MobileDataModel"."BUSDTA"."M0111_del"."CDID" = inserted."CDID"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_ins', 'DROP TRIGGER "BUSDTA"."M0111_ins"', '32d79b305591cee93de2579e3aa2f197', 0
END;
/* Create the timestamp trigger 'M0111_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_upd', '530f82a56c0c98f2bc19b604833a604c';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_upd'
	DROP TRIGGER "BUSDTA"."M0111_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."M0111_upd"
		ON "MobileDataModel"."BUSDTA"."M0111" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."M0111"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."M0111"."CDAN8" = inserted."CDAN8" AND
				"MobileDataModel"."BUSDTA"."M0111"."CDIDLN" = inserted."CDIDLN" AND
				"MobileDataModel"."BUSDTA"."M0111"."CDRCK7" = inserted."CDRCK7" AND
				"MobileDataModel"."BUSDTA"."M0111"."CDCNLN" = inserted."CDCNLN" AND
				"MobileDataModel"."BUSDTA"."M0111"."CDID" = inserted."CDID";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_upd', 'DROP TRIGGER "BUSDTA"."M0111_upd"', '530f82a56c0c98f2bc19b604833a604c', 0
END;
/* Create the shadow delete trigger 'M0111_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_dlt', 'a725818d20914ae125e9b79da6987c11';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_dlt'
	DROP TRIGGER "BUSDTA"."M0111_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."M0111_dlt"
		ON "MobileDataModel"."BUSDTA"."M0111" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."M0111_del" ( "CDAN8", "CDIDLN", "CDRCK7", "CDCNLN", "CDID", "CDACTV", "last_modified" )
			SELECT deleted."CDAN8",
				deleted."CDIDLN",
				deleted."CDRCK7",
				deleted."CDCNLN",
				deleted."CDID",
				deleted."CDACTV",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'M0111', 'M0111_dlt', 'DROP TRIGGER "BUSDTA"."M0111_dlt"', 'a725818d20914ae125e9b79da6987c11', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M0111', 'M0111_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M0111', 'M0111_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'M0111', 'M0111_ml'
	DROP INDEX "M0111"."M0111_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M0111', 'M0111_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "M0111_ml" ON "MobileDataModel"."BUSDTA"."M0111"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''M0111'', ''M0111_ml'', ''DROP INDEX "M0111"."M0111_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M0111_del', 'M0111_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M0111_del', 'M0111_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'M0111_del', 'M0111_mld'
	DROP INDEX "M0111_del"."M0111_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M0111_del', 'M0111_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "M0111_mld" ON "MobileDataModel"."BUSDTA"."M0111_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''M0111_del'', ''M0111_mld'', ''DROP INDEX "M0111_del"."M0111_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*    Creating schema related to the table 'MobileDataModel.BUSDTA.M080111'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.M080111_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'M080111_del', 'M080111_del', 'b363a2f35b3e2d964bd970c208618562'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'M080111_del', 'M080111_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'M080111_del', 'M080111_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."M080111_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'M080111_del', 'M080111_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."M080111_del" (
		"CTID" numeric(8, 0) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("CTID")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'M080111_del', 'M080111_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."M080111_del"', 'b363a2f35b3e2d964bd970c208618562', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'M080111', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'M080111', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'M080111', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."M080111" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'M080111', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."M080111" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'M080111', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."M080111" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'M080111_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_ins', '6fe982559b41bc400fc6bf65c3cb65b6';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_ins'
	DROP TRIGGER "BUSDTA"."M080111_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."M080111_ins"
		ON "MobileDataModel"."BUSDTA"."M080111" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."M080111_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."M080111_del"."CTID" = inserted."CTID"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_ins', 'DROP TRIGGER "BUSDTA"."M080111_ins"', '6fe982559b41bc400fc6bf65c3cb65b6', 0
END;
/* Create the timestamp trigger 'M080111_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_upd', 'f7902ac638af402c1c971601fcfcaeac';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_upd'
	DROP TRIGGER "BUSDTA"."M080111_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."M080111_upd"
		ON "MobileDataModel"."BUSDTA"."M080111" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."M080111"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."M080111"."CTID" = inserted."CTID";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_upd', 'DROP TRIGGER "BUSDTA"."M080111_upd"', 'f7902ac638af402c1c971601fcfcaeac', 0
END;
/* Create the shadow delete trigger 'M080111_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_dlt', '2b7bc2997e71f22a7f933b71458a5baa';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_dlt'
	DROP TRIGGER "BUSDTA"."M080111_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."M080111_dlt"
		ON "MobileDataModel"."BUSDTA"."M080111" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."M080111_del" ( "CTID", "last_modified" )
			SELECT deleted."CTID",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'M080111', 'M080111_dlt', 'DROP TRIGGER "BUSDTA"."M080111_dlt"', '2b7bc2997e71f22a7f933b71458a5baa', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M080111', 'M080111_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M080111', 'M080111_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'M080111', 'M080111_ml'
	DROP INDEX "M080111"."M080111_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M080111', 'M080111_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "M080111_ml" ON "MobileDataModel"."BUSDTA"."M080111"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''M080111'', ''M080111_ml'', ''DROP INDEX "M080111"."M080111_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M080111_del', 'M080111_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M080111_del', 'M080111_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'M080111_del', 'M080111_mld'
	DROP INDEX "M080111_del"."M080111_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'M080111_del', 'M080111_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "M080111_mld" ON "MobileDataModel"."BUSDTA"."M080111_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''M080111_del'', ''M080111_mld'', ''DROP INDEX "M080111_del"."M080111_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*  Creating schema related to the table 'MobileDataModel.BUSDTA.Order_Detail'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.Order_Detail_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_del', '00e6f88201eaca75c9f3180f62ebf8d2'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."Order_Detail_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."Order_Detail_del" (
		"Order_Detail_Id" int not null,
		"Order_ID" int not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("Order_Detail_Id", "Order_ID")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."Order_Detail_del"', '00e6f88201eaca75c9f3180f62ebf8d2', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Order_Detail', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Order_Detail', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'Order_Detail', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."Order_Detail" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Order_Detail', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."Order_Detail" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'Order_Detail', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."Order_Detail" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'Order_Detail_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_ins', 'b257c9c3b483e3516ffb27f617c9ae16';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_ins'
	DROP TRIGGER "BUSDTA"."Order_Detail_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Order_Detail_ins"
		ON "MobileDataModel"."BUSDTA"."Order_Detail" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."Order_Detail_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."Order_Detail_del"."Order_Detail_Id" = inserted."Order_Detail_Id" AND
					"MobileDataModel"."BUSDTA"."Order_Detail_del"."Order_ID" = inserted."Order_ID"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_ins', 'DROP TRIGGER "BUSDTA"."Order_Detail_ins"', 'b257c9c3b483e3516ffb27f617c9ae16', 0
END;
/* Create the timestamp trigger 'Order_Detail_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_upd', 'bfd1b89c12812ea0291cef11a36a33ea';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_upd'
	DROP TRIGGER "BUSDTA"."Order_Detail_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Order_Detail_upd"
		ON "MobileDataModel"."BUSDTA"."Order_Detail" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."Order_Detail"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."Order_Detail"."Order_Detail_Id" = inserted."Order_Detail_Id" AND
				"MobileDataModel"."BUSDTA"."Order_Detail"."Order_ID" = inserted."Order_ID";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_upd', 'DROP TRIGGER "BUSDTA"."Order_Detail_upd"', 'bfd1b89c12812ea0291cef11a36a33ea', 0
END;
/* Create the shadow delete trigger 'Order_Detail_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_dlt', '1360e37de8cde39e2e7ff0d62a64e420';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_dlt'
	DROP TRIGGER "BUSDTA"."Order_Detail_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Order_Detail_dlt"
		ON "MobileDataModel"."BUSDTA"."Order_Detail" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."Order_Detail_del" ( "Order_Detail_Id", "Order_ID", "last_modified" )
			SELECT deleted."Order_Detail_Id",
				deleted."Order_ID",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Order_Detail', 'Order_Detail_dlt', 'DROP TRIGGER "BUSDTA"."Order_Detail_dlt"', '1360e37de8cde39e2e7ff0d62a64e420', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Detail', 'Order_Detail_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Detail', 'Order_Detail_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Order_Detail', 'Order_Detail_ml'
	DROP INDEX "Order_Detail"."Order_Detail_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Detail', 'Order_Detail_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Order_Detail_ml" ON "MobileDataModel"."BUSDTA"."Order_Detail"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Order_Detail'', ''Order_Detail_ml'', ''DROP INDEX "Order_Detail"."Order_Detail_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_mld'
	DROP INDEX "Order_Detail_del"."Order_Detail_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Detail_del', 'Order_Detail_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Order_Detail_mld" ON "MobileDataModel"."BUSDTA"."Order_Detail_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Order_Detail_del'', ''Order_Detail_mld'', ''DROP INDEX "Order_Detail_del"."Order_Detail_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*  Creating schema related to the table 'MobileDataModel.BUSDTA.Order_Header'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.Order_Header_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Order_Header_del', 'Order_Header_del', '5a21277ee5078a6f70b9c19fba0c7f65'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Order_Header_del', 'Order_Header_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'Order_Header_del', 'Order_Header_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."Order_Header_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Order_Header_del', 'Order_Header_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."Order_Header_del" (
		"Order_ID" int not null,
		"OrderSeries" int not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("Order_ID", "OrderSeries")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'Order_Header_del', 'Order_Header_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."Order_Header_del"', '5a21277ee5078a6f70b9c19fba0c7f65', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Order_Header', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Order_Header', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'Order_Header', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."Order_Header" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Order_Header', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."Order_Header" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'Order_Header', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."Order_Header" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'Order_Header_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_ins', '3e33ce61e1c6b88af900553fce89ec81';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_ins'
	DROP TRIGGER "BUSDTA"."Order_Header_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Order_Header_ins"
		ON "MobileDataModel"."BUSDTA"."Order_Header" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."Order_Header_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."Order_Header_del"."Order_ID" = inserted."Order_ID" AND
					"MobileDataModel"."BUSDTA"."Order_Header_del"."OrderSeries" = inserted."OrderSeries"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_ins', 'DROP TRIGGER "BUSDTA"."Order_Header_ins"', '3e33ce61e1c6b88af900553fce89ec81', 0
END;
/* Create the timestamp trigger 'Order_Header_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_upd', '462b1e62898d64a70965cb98bf3db3ed';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_upd'
	DROP TRIGGER "BUSDTA"."Order_Header_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Order_Header_upd"
		ON "MobileDataModel"."BUSDTA"."Order_Header" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."Order_Header"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."Order_Header"."Order_ID" = inserted."Order_ID" AND
				"MobileDataModel"."BUSDTA"."Order_Header"."OrderSeries" = inserted."OrderSeries";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_upd', 'DROP TRIGGER "BUSDTA"."Order_Header_upd"', '462b1e62898d64a70965cb98bf3db3ed', 0
END;
/* Create the shadow delete trigger 'Order_Header_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_dlt', 'b18a0e83e4c9188e4c31bc59193ed224';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_dlt'
	DROP TRIGGER "BUSDTA"."Order_Header_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Order_Header_dlt"
		ON "MobileDataModel"."BUSDTA"."Order_Header" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."Order_Header_del" ( "Order_ID", "OrderSeries", "last_modified" )
			SELECT deleted."Order_ID",
				deleted."OrderSeries",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Order_Header', 'Order_Header_dlt', 'DROP TRIGGER "BUSDTA"."Order_Header_dlt"', 'b18a0e83e4c9188e4c31bc59193ed224', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Header', 'Order_Header_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Header', 'Order_Header_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Order_Header', 'Order_Header_ml'
	DROP INDEX "Order_Header"."Order_Header_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Header', 'Order_Header_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Order_Header_ml" ON "MobileDataModel"."BUSDTA"."Order_Header"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Order_Header'', ''Order_Header_ml'', ''DROP INDEX "Order_Header"."Order_Header_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Header_del', 'Order_Header_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Header_del', 'Order_Header_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Order_Header_del', 'Order_Header_mld'
	DROP INDEX "Order_Header_del"."Order_Header_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Order_Header_del', 'Order_Header_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Order_Header_mld" ON "MobileDataModel"."BUSDTA"."Order_Header_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Order_Header_del'', ''Order_Header_mld'', ''DROP INDEX "Order_Header_del"."Order_Header_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
* Creating schema related to the table 'MobileDataModel.BUSDTA.PickOrder_Exception'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.PickOrder_Exception_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_del', 'b7cb046bcfb21707e2377266ae0b26cc'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception_del" (
		"PickOrder_Exception_Id" int not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("PickOrder_Exception_Id")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"', 'b7cb046bcfb21707e2377266ae0b26cc', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'PickOrder_Exception', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'PickOrder_Exception', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'PickOrder_Exception', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'PickOrder_Exception', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'PickOrder_Exception', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'PickOrder_Exception_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ins', '8a160fa909b937a29ff4040b8333d994';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ins'
	DROP TRIGGER "BUSDTA"."PickOrder_Exception_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."PickOrder_Exception_ins"
		ON "MobileDataModel"."BUSDTA"."PickOrder_Exception" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"."PickOrder_Exception_Id" = inserted."PickOrder_Exception_Id"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ins', 'DROP TRIGGER "BUSDTA"."PickOrder_Exception_ins"', '8a160fa909b937a29ff4040b8333d994', 0
END;
/* Create the timestamp trigger 'PickOrder_Exception_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_upd', '4fa0a54da5eb6ff072a1df454659c840';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_upd'
	DROP TRIGGER "BUSDTA"."PickOrder_Exception_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."PickOrder_Exception_upd"
		ON "MobileDataModel"."BUSDTA"."PickOrder_Exception" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."PickOrder_Exception"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."PickOrder_Exception"."PickOrder_Exception_Id" = inserted."PickOrder_Exception_Id";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_upd', 'DROP TRIGGER "BUSDTA"."PickOrder_Exception_upd"', '4fa0a54da5eb6ff072a1df454659c840', 0
END;
/* Create the shadow delete trigger 'PickOrder_Exception_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_dlt', '82ef4287acb28eb6ea976a1e7ceb3e3a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_dlt'
	DROP TRIGGER "BUSDTA"."PickOrder_Exception_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."PickOrder_Exception_dlt"
		ON "MobileDataModel"."BUSDTA"."PickOrder_Exception" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."PickOrder_Exception_del" ( "PickOrder_Exception_Id", "last_modified" )
			SELECT deleted."PickOrder_Exception_Id",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_dlt', 'DROP TRIGGER "BUSDTA"."PickOrder_Exception_dlt"', '82ef4287acb28eb6ea976a1e7ceb3e3a', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ml'
	DROP INDEX "PickOrder_Exception"."PickOrder_Exception_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_Exception', 'PickOrder_Exception_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "PickOrder_Exception_ml" ON "MobileDataModel"."BUSDTA"."PickOrder_Exception"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''PickOrder_Exception'', ''PickOrder_Exception_ml'', ''DROP INDEX "PickOrder_Exception"."PickOrder_Exception_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_mld'
	DROP INDEX "PickOrder_Exception_del"."PickOrder_Exception_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_Exception_del', 'PickOrder_Exception_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "PickOrder_Exception_mld" ON "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''PickOrder_Exception_del'', ''PickOrder_Exception_mld'', ''DROP INDEX "PickOrder_Exception_del"."PickOrder_Exception_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*   Creating schema related to the table 'MobileDataModel.BUSDTA.PickOrder'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.PickOrder_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'PickOrder_del', 'PickOrder_del', '30ca3724f108b35b6ba13a9f81cb7e1c'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'PickOrder_del', 'PickOrder_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'PickOrder_del', 'PickOrder_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."PickOrder_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'PickOrder_del', 'PickOrder_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."PickOrder_del" (
		"PickOrder_Id" int not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("PickOrder_Id")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'PickOrder_del', 'PickOrder_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."PickOrder_del"', '30ca3724f108b35b6ba13a9f81cb7e1c', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'PickOrder', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'PickOrder', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'PickOrder', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."PickOrder" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'PickOrder', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."PickOrder" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'PickOrder', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."PickOrder" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'PickOrder_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_ins', '43393810dab824b6295e513fe71c7d1d';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_ins'
	DROP TRIGGER "BUSDTA"."PickOrder_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."PickOrder_ins"
		ON "MobileDataModel"."BUSDTA"."PickOrder" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."PickOrder_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."PickOrder_del"."PickOrder_Id" = inserted."PickOrder_Id"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_ins', 'DROP TRIGGER "BUSDTA"."PickOrder_ins"', '43393810dab824b6295e513fe71c7d1d', 0
END;
/* Create the timestamp trigger 'PickOrder_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_upd', 'a51f7caff094e992e3bf6a76e9bc24c3';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_upd'
	DROP TRIGGER "BUSDTA"."PickOrder_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."PickOrder_upd"
		ON "MobileDataModel"."BUSDTA"."PickOrder" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."PickOrder"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."PickOrder"."PickOrder_Id" = inserted."PickOrder_Id";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_upd', 'DROP TRIGGER "BUSDTA"."PickOrder_upd"', 'a51f7caff094e992e3bf6a76e9bc24c3', 0
END;
/* Create the shadow delete trigger 'PickOrder_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_dlt', 'e720e95a5ab59f8c615af74521edcfd3';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_dlt'
	DROP TRIGGER "BUSDTA"."PickOrder_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."PickOrder_dlt"
		ON "MobileDataModel"."BUSDTA"."PickOrder" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."PickOrder_del" ( "PickOrder_Id", "last_modified" )
			SELECT deleted."PickOrder_Id",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'PickOrder', 'PickOrder_dlt', 'DROP TRIGGER "BUSDTA"."PickOrder_dlt"', 'e720e95a5ab59f8c615af74521edcfd3', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder', 'PickOrder_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder', 'PickOrder_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'PickOrder', 'PickOrder_ml'
	DROP INDEX "PickOrder"."PickOrder_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder', 'PickOrder_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "PickOrder_ml" ON "MobileDataModel"."BUSDTA"."PickOrder"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''PickOrder'', ''PickOrder_ml'', ''DROP INDEX "PickOrder"."PickOrder_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_del', 'PickOrder_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_del', 'PickOrder_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'PickOrder_del', 'PickOrder_mld'
	DROP INDEX "PickOrder_del"."PickOrder_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'PickOrder_del', 'PickOrder_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "PickOrder_mld" ON "MobileDataModel"."BUSDTA"."PickOrder_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''PickOrder_del'', ''PickOrder_mld'', ''DROP INDEX "PickOrder_del"."PickOrder_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
* Creating schema related to the table 'MobileDataModel.BUSDTA.ReasonCodeMaster'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.ReasonCodeMaster_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_del', 'c7a57dfad3ecb068e3ea2bf01feace49'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del" (
		"ReasonCodeId" int not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("ReasonCodeId")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"', 'c7a57dfad3ecb068e3ea2bf01feace49', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'ReasonCodeMaster', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'ReasonCodeMaster', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'ReasonCodeMaster', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'ReasonCodeMaster', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'ReasonCodeMaster', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'ReasonCodeMaster_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ins', '7ef4b9148aa8618419927240367ba491';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ins'
	DROP TRIGGER "BUSDTA"."ReasonCodeMaster_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."ReasonCodeMaster_ins"
		ON "MobileDataModel"."BUSDTA"."ReasonCodeMaster" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"."ReasonCodeId" = inserted."ReasonCodeId"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ins', 'DROP TRIGGER "BUSDTA"."ReasonCodeMaster_ins"', '7ef4b9148aa8618419927240367ba491', 0
END;
/* Create the timestamp trigger 'ReasonCodeMaster_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_upd', '0a1f93434fe82edc3a03b798b95bd929';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_upd'
	DROP TRIGGER "BUSDTA"."ReasonCodeMaster_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."ReasonCodeMaster_upd"
		ON "MobileDataModel"."BUSDTA"."ReasonCodeMaster" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."ReasonCodeMaster"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."ReasonCodeMaster"."ReasonCodeId" = inserted."ReasonCodeId";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_upd', 'DROP TRIGGER "BUSDTA"."ReasonCodeMaster_upd"', '0a1f93434fe82edc3a03b798b95bd929', 0
END;
/* Create the shadow delete trigger 'ReasonCodeMaster_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_dlt', '4b15da74b1797c38e0afaae7749fbbe8';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_dlt'
	DROP TRIGGER "BUSDTA"."ReasonCodeMaster_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."ReasonCodeMaster_dlt"
		ON "MobileDataModel"."BUSDTA"."ReasonCodeMaster" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del" ( "ReasonCodeId", "last_modified" )
			SELECT deleted."ReasonCodeId",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_dlt', 'DROP TRIGGER "BUSDTA"."ReasonCodeMaster_dlt"', '4b15da74b1797c38e0afaae7749fbbe8', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ml'
	DROP INDEX "ReasonCodeMaster"."ReasonCodeMaster_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'ReasonCodeMaster', 'ReasonCodeMaster_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "ReasonCodeMaster_ml" ON "MobileDataModel"."BUSDTA"."ReasonCodeMaster"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''ReasonCodeMaster'', ''ReasonCodeMaster_ml'', ''DROP INDEX "ReasonCodeMaster"."ReasonCodeMaster_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_mld'
	DROP INDEX "ReasonCodeMaster_del"."ReasonCodeMaster_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'ReasonCodeMaster_del', 'ReasonCodeMaster_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "ReasonCodeMaster_mld" ON "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''ReasonCodeMaster_del'', ''ReasonCodeMaster_mld'', ''DROP INDEX "ReasonCodeMaster_del"."ReasonCodeMaster_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
* Creating schema related to the table 'MobileDataModel.BUSDTA.Route_Device_Map'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.Route_Device_Map_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_del', 'd2b2dd83fd477b3b6088ae0227746978'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map_del" (
		"Route_Id" varchar(8) not null,
		"Device_Id" varchar(30) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("Route_Id", "Device_Id")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map_del"', 'd2b2dd83fd477b3b6088ae0227746978', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Route_Device_Map', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Route_Device_Map', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'Route_Device_Map', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Route_Device_Map', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'Route_Device_Map', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'Route_Device_Map_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ins', '3a5ec53b09013f3c575579d60e697bcd';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ins'
	DROP TRIGGER "BUSDTA"."Route_Device_Map_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Route_Device_Map_ins"
		ON "MobileDataModel"."BUSDTA"."Route_Device_Map" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."Route_Device_Map_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."Route_Device_Map_del"."Route_Id" = inserted."Route_Id" AND
					"MobileDataModel"."BUSDTA"."Route_Device_Map_del"."Device_Id" = inserted."Device_Id"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ins', 'DROP TRIGGER "BUSDTA"."Route_Device_Map_ins"', '3a5ec53b09013f3c575579d60e697bcd', 0
END;
/* Create the timestamp trigger 'Route_Device_Map_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_upd', '3980513a870bf39fd88c6d3ccf494368';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_upd'
	DROP TRIGGER "BUSDTA"."Route_Device_Map_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Route_Device_Map_upd"
		ON "MobileDataModel"."BUSDTA"."Route_Device_Map" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."Route_Device_Map"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."Route_Device_Map"."Route_Id" = inserted."Route_Id" AND
				"MobileDataModel"."BUSDTA"."Route_Device_Map"."Device_Id" = inserted."Device_Id";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_upd', 'DROP TRIGGER "BUSDTA"."Route_Device_Map_upd"', '3980513a870bf39fd88c6d3ccf494368', 0
END;
/* Create the shadow delete trigger 'Route_Device_Map_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_dlt', '5f0ff7f056fdec07241395b997cdbd82';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_dlt'
	DROP TRIGGER "BUSDTA"."Route_Device_Map_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Route_Device_Map_dlt"
		ON "MobileDataModel"."BUSDTA"."Route_Device_Map" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."Route_Device_Map_del" ( "Route_Id", "Device_Id", "last_modified" )
			SELECT deleted."Route_Id",
				deleted."Device_Id",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_dlt', 'DROP TRIGGER "BUSDTA"."Route_Device_Map_dlt"', '5f0ff7f056fdec07241395b997cdbd82', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ml'
	DROP INDEX "Route_Device_Map"."Route_Device_Map_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_Device_Map', 'Route_Device_Map_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Route_Device_Map_ml" ON "MobileDataModel"."BUSDTA"."Route_Device_Map"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Route_Device_Map'', ''Route_Device_Map_ml'', ''DROP INDEX "Route_Device_Map"."Route_Device_Map_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_mld'
	DROP INDEX "Route_Device_Map_del"."Route_Device_Map_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_Device_Map_del', 'Route_Device_Map_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Route_Device_Map_mld" ON "MobileDataModel"."BUSDTA"."Route_Device_Map_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Route_Device_Map_del'', ''Route_Device_Map_mld'', ''DROP INDEX "Route_Device_Map_del"."Route_Device_Map_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
* Creating schema related to the table 'MobileDataModel.BUSDTA.Route_User_Map'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.Route_User_Map_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_del', '517e577b915d83110b764b7b27f17f0f'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."Route_User_Map_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."Route_User_Map_del" (
		"App_user_id" int not null,
		"Route_Id" varchar(8) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("App_user_id", "Route_Id")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."Route_User_Map_del"', '517e577b915d83110b764b7b27f17f0f', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Route_User_Map', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Route_User_Map', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'Route_User_Map', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."Route_User_Map" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'Route_User_Map', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."Route_User_Map" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'Route_User_Map', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."Route_User_Map" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'Route_User_Map_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ins', '4568cb1e8ddc1ee1452bcd709799dfd3';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ins'
	DROP TRIGGER "BUSDTA"."Route_User_Map_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Route_User_Map_ins"
		ON "MobileDataModel"."BUSDTA"."Route_User_Map" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."Route_User_Map_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."Route_User_Map_del"."App_user_id" = inserted."App_user_id" AND
					"MobileDataModel"."BUSDTA"."Route_User_Map_del"."Route_Id" = inserted."Route_Id"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ins', 'DROP TRIGGER "BUSDTA"."Route_User_Map_ins"', '4568cb1e8ddc1ee1452bcd709799dfd3', 0
END;
/* Create the timestamp trigger 'Route_User_Map_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_upd', 'ca026305ea80fed54dedcd3819ef758f';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_upd'
	DROP TRIGGER "BUSDTA"."Route_User_Map_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Route_User_Map_upd"
		ON "MobileDataModel"."BUSDTA"."Route_User_Map" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."Route_User_Map"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."Route_User_Map"."App_user_id" = inserted."App_user_id" AND
				"MobileDataModel"."BUSDTA"."Route_User_Map"."Route_Id" = inserted."Route_Id";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_upd', 'DROP TRIGGER "BUSDTA"."Route_User_Map_upd"', 'ca026305ea80fed54dedcd3819ef758f', 0
END;
/* Create the shadow delete trigger 'Route_User_Map_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_dlt', 'df2fdee6cac5b93edb7218307479de16';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_dlt'
	DROP TRIGGER "BUSDTA"."Route_User_Map_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."Route_User_Map_dlt"
		ON "MobileDataModel"."BUSDTA"."Route_User_Map" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."Route_User_Map_del" ( "App_user_id", "Route_Id", "last_modified" )
			SELECT deleted."App_user_id",
				deleted."Route_Id",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_dlt', 'DROP TRIGGER "BUSDTA"."Route_User_Map_dlt"', 'df2fdee6cac5b93edb7218307479de16', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ml'
	DROP INDEX "Route_User_Map"."Route_User_Map_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_User_Map', 'Route_User_Map_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Route_User_Map_ml" ON "MobileDataModel"."BUSDTA"."Route_User_Map"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Route_User_Map'', ''Route_User_Map_ml'', ''DROP INDEX "Route_User_Map"."Route_User_Map_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_mld'
	DROP INDEX "Route_User_Map_del"."Route_User_Map_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'Route_User_Map_del', 'Route_User_Map_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "Route_User_Map_mld" ON "MobileDataModel"."BUSDTA"."Route_User_Map_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''Route_User_Map_del'', ''Route_User_Map_mld'', ''DROP INDEX "Route_User_Map_del"."Route_User_Map_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*   Creating schema related to the table 'MobileDataModel.BUSDTA.UDCKEYLIST'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.UDCKEYLIST_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_del', 'a46a853117fd97c4676ab7a1e8b63039'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."UDCKEYLIST_del" (
		"DTSY" nchar(4) not null,
		"DTRT" nchar(2) not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("DTSY", "DTRT")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"', 'a46a853117fd97c4676ab7a1e8b63039', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'UDCKEYLIST', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'UDCKEYLIST', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'UDCKEYLIST', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."UDCKEYLIST" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'UDCKEYLIST', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."UDCKEYLIST" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'UDCKEYLIST', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."UDCKEYLIST" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'UDCKEYLIST_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ins', 'd35ddb5cfa1abef3585537d0dd79537a';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ins'
	DROP TRIGGER "BUSDTA"."UDCKEYLIST_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."UDCKEYLIST_ins"
		ON "MobileDataModel"."BUSDTA"."UDCKEYLIST" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"."DTSY" = inserted."DTSY" AND
					"MobileDataModel"."BUSDTA"."UDCKEYLIST_del"."DTRT" = inserted."DTRT"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ins', 'DROP TRIGGER "BUSDTA"."UDCKEYLIST_ins"', 'd35ddb5cfa1abef3585537d0dd79537a', 0
END;
/* Create the timestamp trigger 'UDCKEYLIST_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_upd', '09780dd7bb24f0b21a30e05c8c1e90e3';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_upd'
	DROP TRIGGER "BUSDTA"."UDCKEYLIST_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."UDCKEYLIST_upd"
		ON "MobileDataModel"."BUSDTA"."UDCKEYLIST" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."UDCKEYLIST"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."UDCKEYLIST"."DTSY" = inserted."DTSY" AND
				"MobileDataModel"."BUSDTA"."UDCKEYLIST"."DTRT" = inserted."DTRT";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_upd', 'DROP TRIGGER "BUSDTA"."UDCKEYLIST_upd"', '09780dd7bb24f0b21a30e05c8c1e90e3', 0
END;
/* Create the shadow delete trigger 'UDCKEYLIST_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_dlt', '5667f52d000f2428caca28647ce7b185';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_dlt'
	DROP TRIGGER "BUSDTA"."UDCKEYLIST_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."UDCKEYLIST_dlt"
		ON "MobileDataModel"."BUSDTA"."UDCKEYLIST" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."UDCKEYLIST_del" ( "DTSY", "DTRT", "last_modified" )
			SELECT deleted."DTSY",
				deleted."DTRT",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_dlt', 'DROP TRIGGER "BUSDTA"."UDCKEYLIST_dlt"', '5667f52d000f2428caca28647ce7b185', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ml'
	DROP INDEX "UDCKEYLIST"."UDCKEYLIST_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'UDCKEYLIST', 'UDCKEYLIST_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "UDCKEYLIST_ml" ON "MobileDataModel"."BUSDTA"."UDCKEYLIST"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''UDCKEYLIST'', ''UDCKEYLIST_ml'', ''DROP INDEX "UDCKEYLIST"."UDCKEYLIST_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_mld'
	DROP INDEX "UDCKEYLIST_del"."UDCKEYLIST_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'UDCKEYLIST_del', 'UDCKEYLIST_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "UDCKEYLIST_mld" ON "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''UDCKEYLIST_del'', ''UDCKEYLIST_mld'', ''DROP INDEX "UDCKEYLIST_del"."UDCKEYLIST_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
*  Creating schema related to the table 'MobileDataModel.BUSDTA.user_master'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.user_master_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'user_master_del', 'user_master_del', '4cd29b289d3d09fb444d1e221cee9f22'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'user_master_del', 'user_master_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'user_master_del', 'user_master_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."user_master_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'user_master_del', 'user_master_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."user_master_del" (
		"App_user_id" integer not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("App_user_id")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'user_master_del', 'user_master_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."user_master_del"', '4cd29b289d3d09fb444d1e221cee9f22', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'user_master', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'user_master', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'user_master', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."user_master" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'user_master', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."user_master" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'user_master', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."user_master" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'user_master_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_ins', 'b01a3912f7c36c79cf0e7e8eda2564a0';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_ins'
	DROP TRIGGER "BUSDTA"."user_master_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."user_master_ins"
		ON "MobileDataModel"."BUSDTA"."user_master" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."user_master_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."user_master_del"."App_user_id" = inserted."App_user_id"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_ins', 'DROP TRIGGER "BUSDTA"."user_master_ins"', 'b01a3912f7c36c79cf0e7e8eda2564a0', 0
END;
/* Create the timestamp trigger 'user_master_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_upd', '9f395076e108494e1010b3c7ad8e380e';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_upd'
	DROP TRIGGER "BUSDTA"."user_master_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."user_master_upd"
		ON "MobileDataModel"."BUSDTA"."user_master" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."user_master"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."user_master"."App_user_id" = inserted."App_user_id";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_upd', 'DROP TRIGGER "BUSDTA"."user_master_upd"', '9f395076e108494e1010b3c7ad8e380e', 0
END;
/* Create the shadow delete trigger 'user_master_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_dlt', 'ee2b34dc4cb2a21fd4fa3fa3ed821981';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_dlt'
	DROP TRIGGER "BUSDTA"."user_master_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."user_master_dlt"
		ON "MobileDataModel"."BUSDTA"."user_master" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."user_master_del" ( "App_user_id", "last_modified" )
			SELECT deleted."App_user_id",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'user_master', 'user_master_dlt', 'DROP TRIGGER "BUSDTA"."user_master_dlt"', 'ee2b34dc4cb2a21fd4fa3fa3ed821981', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'user_master', 'user_master_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'user_master', 'user_master_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'user_master', 'user_master_ml'
	DROP INDEX "user_master"."user_master_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'user_master', 'user_master_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "user_master_ml" ON "MobileDataModel"."BUSDTA"."user_master"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''user_master'', ''user_master_ml'', ''DROP INDEX "user_master"."user_master_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'user_master_del', 'user_master_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'user_master_del', 'user_master_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'user_master_del', 'user_master_mld'
	DROP INDEX "user_master_del"."user_master_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'user_master_del', 'user_master_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "user_master_mld" ON "MobileDataModel"."BUSDTA"."user_master_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''user_master_del'', ''user_master_mld'', ''DROP INDEX "user_master_del"."user_master_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/*------------------------------------------------------------------------------
* Creating schema related to the table 'MobileDataModel.BUSDTA.User_Role_Map'.
*-----------------------------------------------------------------------------*/

/* Create the shadow delete table 'MobileDataModel.BUSDTA.User_Role_Map_del'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_del', '4e2c555fec3c0d61cb64dd6fd17e67ba'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_del', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TABLE', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_del'
	DROP TABLE "MobileDataModel"."BUSDTA"."User_Role_Map_del"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TABLE', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_del', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	CREATE TABLE "MobileDataModel"."BUSDTA"."User_Role_Map_del" (
		"App_user_id" int not null,
		"last_modified" DATETIME DEFAULT GETDATE(),
		PRIMARY KEY ("App_user_id")
	);
	EXEC ml_model_register_schema 'TABLE', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_del', 'DROP TABLE "MobileDataModel"."BUSDTA"."User_Role_Map_del"', '4e2c555fec3c0d61cb64dd6fd17e67ba', 0
END;

/* Create the timestamp column 'last_modified'. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'User_Role_Map', 'last_modified', '4657787710f8fa6b7519e36795a0da55'
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'User_Role_Map', 'last_modified', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'COLUMN', 'BUSDTA', 'User_Role_Map', 'last_modified'
	ALTER TABLE "MobileDataModel"."BUSDTA"."User_Role_Map" DROP COLUMN "last_modified";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'COLUMN', 'BUSDTA', 'User_Role_Map', 'last_modified', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	ALTER TABLE "MobileDataModel"."BUSDTA"."User_Role_Map" ADD "last_modified" DATETIME NOT NULL DEFAULT GETDATE();
	EXEC ml_model_register_schema 'COLUMN', 'BUSDTA', 'User_Role_Map', 'last_modified', 'ALTER TABLE "MobileDataModel"."BUSDTA"."User_Role_Map" DROP COLUMN "last_modified"', '4657787710f8fa6b7519e36795a0da55', 0
END;

/* Create the shadow delete trigger 'User_Role_Map_ins'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ins', '44b69166965e8250019944573b51e5d4';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ins', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ins'
	DROP TRIGGER "BUSDTA"."User_Role_Map_ins"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ins', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."User_Role_Map_ins"
		ON "MobileDataModel"."BUSDTA"."User_Role_Map" AFTER INSERT
		AS
			/*
			* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
			* primary keys can be re-inserted.)
			*/
			DELETE FROM "MobileDataModel"."BUSDTA"."User_Role_Map_del"
			WHERE EXISTS (
				SELECT 1
				FROM inserted
				WHERE "MobileDataModel"."BUSDTA"."User_Role_Map_del"."App_user_id" = inserted."App_user_id"
				);

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ins', 'DROP TRIGGER "BUSDTA"."User_Role_Map_ins"', '44b69166965e8250019944573b51e5d4', 0
END;
/* Create the timestamp trigger 'User_Role_Map_upd'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_upd', 'a754092e1eeecf11836c0d67a5b83361';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_upd', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_upd'
	DROP TRIGGER "BUSDTA"."User_Role_Map_upd"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_upd', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."User_Role_Map_upd"
		ON "MobileDataModel"."BUSDTA"."User_Role_Map" AFTER UPDATE
		AS

			/* Update the column last_modified in modified row. */
			UPDATE "MobileDataModel"."BUSDTA"."User_Role_Map"
			SET "last_modified" = GETDATE()
			FROM inserted
			WHERE  "MobileDataModel"."BUSDTA"."User_Role_Map"."App_user_id" = inserted."App_user_id";

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_upd', 'DROP TRIGGER "BUSDTA"."User_Role_Map_upd"', 'a754092e1eeecf11836c0d67a5b83361', 0
END;
/* Create the shadow delete trigger 'User_Role_Map_dlt'. */

EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_dlt', '8700552c0b4a584dac5c5be3333da91b';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_dlt', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_dlt'
	DROP TRIGGER "BUSDTA"."User_Role_Map_dlt"
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_dlt', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE TRIGGER "BUSDTA"."User_Role_Map_dlt"
		ON "MobileDataModel"."BUSDTA"."User_Role_Map" AFTER DELETE
		AS
			/* Insert the row into the shadow delete table. */
			INSERT INTO "MobileDataModel"."BUSDTA"."User_Role_Map_del" ( "App_user_id", "last_modified" )
			SELECT deleted."App_user_id",
				GETDATE()
			FROM deleted;

		')
	EXEC ml_model_register_schema 'TRIGGER', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_dlt', 'DROP TRIGGER "BUSDTA"."User_Role_Map_dlt"', '8700552c0b4a584dac5c5be3333da91b', 0
END;

/* Create an index for the 'download_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ml', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ml', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ml'
	DROP INDEX "User_Role_Map"."User_Role_Map_ml";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'User_Role_Map', 'User_Role_Map_ml', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "User_Role_Map_ml" ON "MobileDataModel"."BUSDTA"."User_Role_Map"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''User_Role_Map'', ''User_Role_Map_ml'', ''DROP INDEX "User_Role_Map"."User_Role_Map_ml"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

/* Create an index for the 'download_delete_cursor' script. */
EXEC ml_model_register_schema_use 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_mld', '8f4d69a87e57fc1b18329de943f4dcce';
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_mld', 'OVERWRITE';
IF @action = 'REPLACE' BEGIN
	EXEC ml_model_deregister_schema 'INDEX', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_mld'
	DROP INDEX "User_Role_Map_del"."User_Role_Map_mld";
END;
EXEC @action = ml_model_get_schema_action 'SLE_RemoteDB', 'INDEX', 'BUSDTA', 'User_Role_Map_del', 'User_Role_Map_mld', 'OVERWRITE';
IF @action = 'CREATE' BEGIN
	EXECUTE('
		CREATE INDEX "User_Role_Map_mld" ON "MobileDataModel"."BUSDTA"."User_Role_Map_del"
		( "last_modified" );
		EXEC ml_model_register_schema ''INDEX'', ''BUSDTA'', ''User_Role_Map_del'', ''User_Role_Map_mld'', ''DROP INDEX "User_Role_Map_del"."User_Role_Map_mld"'', ''8f4d69a87e57fc1b18329de943f4dcce'', 0

	')
END;

EXEC ml_model_drop_unused_schema;
END
GO

COMMIT
GO
BEGIN TRANSACTION
GO

EXEC ml_add_lang_conn_script_chk
	'SLE_RemoteDB',
	'authenticate_user',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_conn_script_chk
	'SLE_RemoteDB',
	'begin_connection',
	'sql',
'
SET NOCOUNT ON
',
	'5BDE3E6264B47145301A73630789471E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Device_Master"."Device_Id",
	"MobileDataModel"."BUSDTA"."Device_Master"."Active",
	"MobileDataModel"."BUSDTA"."Device_Master"."manufacturer",
	"MobileDataModel"."BUSDTA"."Device_Master"."model"
FROM "MobileDataModel"."BUSDTA"."Device_Master"
WHERE "MobileDataModel"."BUSDTA"."Device_Master"."last_modified" >= {ml s.last_table_download}
',
	'9D096833AF1E4DACB1B8A4D439485EF7'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Device_Master_del"."Device_Id"
FROM "MobileDataModel"."BUSDTA"."Device_Master_del"
WHERE "MobileDataModel"."BUSDTA"."Device_Master_del"."last_modified" >= {ml s.last_table_download}
',
	'397D0745051BB1CD005E1B93E936A8E0'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Device_Master',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0004"."DTSY",
	"MobileDataModel"."BUSDTA"."F0004"."DTRT",
	"MobileDataModel"."BUSDTA"."F0004"."DTDL01",
	"MobileDataModel"."BUSDTA"."F0004"."DTCDL",
	"MobileDataModel"."BUSDTA"."F0004"."DTLN2",
	"MobileDataModel"."BUSDTA"."F0004"."DTCNUM"
FROM "MobileDataModel"."BUSDTA"."F0004"
WHERE "MobileDataModel"."BUSDTA"."F0004"."last_modified" >= {ml s.last_table_download}
',
	'36104F1473DA8DEF14A50E51E549982C'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0004_del"."DTSY",
	"MobileDataModel"."BUSDTA"."F0004_del"."DTRT"
FROM "MobileDataModel"."BUSDTA"."F0004_del"
WHERE "MobileDataModel"."BUSDTA"."F0004_del"."last_modified" >= {ml s.last_table_download}
',
	'4F596B0436F7616C9524207F8AECC0D2'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0004',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0005"."DRSY",
	"MobileDataModel"."BUSDTA"."F0005"."DRRT",
	"MobileDataModel"."BUSDTA"."F0005"."DRKY",
	"MobileDataModel"."BUSDTA"."F0005"."DRDL01",
	"MobileDataModel"."BUSDTA"."F0005"."DRDL02",
	"MobileDataModel"."BUSDTA"."F0005"."DRSPHD",
	"MobileDataModel"."BUSDTA"."F0005"."DRHRDC"
FROM "MobileDataModel"."BUSDTA"."F0005"
WHERE "MobileDataModel"."BUSDTA"."F0005"."last_modified" >= {ml s.last_table_download}
',
	'CA1299C32B3F7B7F52D3C6D987F2F25A'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0005_del"."DRSY",
	"MobileDataModel"."BUSDTA"."F0005_del"."DRRT",
	"MobileDataModel"."BUSDTA"."F0005_del"."DRKY"
FROM "MobileDataModel"."BUSDTA"."F0005_del"
WHERE "MobileDataModel"."BUSDTA"."F0005_del"."last_modified" >= {ml s.last_table_download}
',
	'07724DC20216ACDA7899063134F29180'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0005',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0006"."MCMCU",
	"MobileDataModel"."BUSDTA"."F0006"."MCSTYL",
	"MobileDataModel"."BUSDTA"."F0006"."MCLDM",
	"MobileDataModel"."BUSDTA"."F0006"."MCCO",
	"MobileDataModel"."BUSDTA"."F0006"."MCAN8",
	"MobileDataModel"."BUSDTA"."F0006"."MCDL01",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP01",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP02",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP03",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP04",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP05",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP06",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP07",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP08",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP09",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP10",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP11",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP12",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP13",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP14",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP15",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP16",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP17",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP18",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP19",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP20",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP21",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP22",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP23",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP24",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP25",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP26",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP27",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP28",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP29",
	"MobileDataModel"."BUSDTA"."F0006"."MCRP30"
FROM "MobileDataModel"."BUSDTA"."F0006"
WHERE "MobileDataModel"."BUSDTA"."F0006"."last_modified" >= {ml s.last_table_download}
',
	'9CADCC209BB4FEFCBC6D3F5B15494165'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0006_del"."MCMCU"
FROM "MobileDataModel"."BUSDTA"."F0006_del"
WHERE "MobileDataModel"."BUSDTA"."F0006_del"."last_modified" >= {ml s.last_table_download}
',
	'139157A664125D264F3AE61A69B77A0E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0006',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0014"."PNPTC",
	"MobileDataModel"."BUSDTA"."F0014"."PNPTD",
	"MobileDataModel"."BUSDTA"."F0014"."PNDCP",
	"MobileDataModel"."BUSDTA"."F0014"."PNDCD",
	"MobileDataModel"."BUSDTA"."F0014"."PNNDTP",
	"MobileDataModel"."BUSDTA"."F0014"."PNNSP",
	"MobileDataModel"."BUSDTA"."F0014"."PNDTPA",
	"MobileDataModel"."BUSDTA"."F0014"."PNPXDM",
	"MobileDataModel"."BUSDTA"."F0014"."PNPXDD"
FROM "MobileDataModel"."BUSDTA"."F0014"
WHERE "MobileDataModel"."BUSDTA"."F0014"."last_modified" >= {ml s.last_table_download}
',
	'78FE205687982D139F49EC74194FC48D'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0014_del"."PNPTC"
FROM "MobileDataModel"."BUSDTA"."F0014_del"
WHERE "MobileDataModel"."BUSDTA"."F0014_del"."last_modified" >= {ml s.last_table_download}
',
	'A70DFBBDB2C0247F118EAD8E6F49A061'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0014',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0101"."ABAN8",
	"MobileDataModel"."BUSDTA"."F0101"."ABALKY",
	"MobileDataModel"."BUSDTA"."F0101"."ABTAX",
	"MobileDataModel"."BUSDTA"."F0101"."ABALPH",
	"MobileDataModel"."BUSDTA"."F0101"."ABMCU",
	"MobileDataModel"."BUSDTA"."F0101"."ABSIC",
	"MobileDataModel"."BUSDTA"."F0101"."ABLNGP",
	"MobileDataModel"."BUSDTA"."F0101"."ABAT1",
	"MobileDataModel"."BUSDTA"."F0101"."ABCM",
	"MobileDataModel"."BUSDTA"."F0101"."ABTAXC",
	"MobileDataModel"."BUSDTA"."F0101"."ABAT2",
	"MobileDataModel"."BUSDTA"."F0101"."ABAN81",
	"MobileDataModel"."BUSDTA"."F0101"."ABAN82",
	"MobileDataModel"."BUSDTA"."F0101"."ABAN83",
	"MobileDataModel"."BUSDTA"."F0101"."ABAN84",
	"MobileDataModel"."BUSDTA"."F0101"."ABAN86",
	"MobileDataModel"."BUSDTA"."F0101"."ABAN85",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC01",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC02",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC03",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC04",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC05",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC06",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC07",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC08",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC09",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC10",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC11",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC12",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC13",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC14",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC15",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC16",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC17",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC18",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC19",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC20",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC21",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC22",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC23",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC24",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC25",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC26",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC27",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC28",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC29",
	"MobileDataModel"."BUSDTA"."F0101"."ABAC30",
	"MobileDataModel"."BUSDTA"."F0101"."ABRMK",
	"MobileDataModel"."BUSDTA"."F0101"."ABTXCT",
	"MobileDataModel"."BUSDTA"."F0101"."ABTX2",
	"MobileDataModel"."BUSDTA"."F0101"."ABALP1"
FROM "MobileDataModel"."BUSDTA"."F0101", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086
WHERE "MobileDataModel"."BUSDTA"."F0101"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and ABAN8=CRCRAN8 and FFUSER= {ml s.username}
',
	'122081EF51543F6765B0812A23D399C6'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."F0101"
WHERE "ABAN8" = {ml r."ABAN8"}
',
	'A93BFED46908CA0523C92C396570CF0B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."F0101" ( "ABAN8", "ABALKY", "ABTAX", "ABALPH", "ABMCU", "ABSIC", "ABLNGP", "ABAT1", "ABCM", "ABTAXC", "ABAT2", "ABAN81", "ABAN82", "ABAN83", "ABAN84", "ABAN86", "ABAN85", "ABAC01", "ABAC02", "ABAC03", "ABAC04", "ABAC05", "ABAC06", "ABAC07", "ABAC08", "ABAC09", "ABAC10", "ABAC11", "ABAC12", "ABAC13", "ABAC14", "ABAC15", "ABAC16", "ABAC17", "ABAC18", "ABAC19", "ABAC20", "ABAC21", "ABAC22", "ABAC23", "ABAC24", "ABAC25", "ABAC26", "ABAC27", "ABAC28", "ABAC29", "ABAC30", "ABRMK", "ABTXCT", "ABTX2", "ABALP1" )
VALUES ( {ml r."ABAN8"}, {ml r."ABALKY"}, {ml r."ABTAX"}, {ml r."ABALPH"}, {ml r."ABMCU"}, {ml r."ABSIC"}, {ml r."ABLNGP"}, {ml r."ABAT1"}, {ml r."ABCM"}, {ml r."ABTAXC"}, {ml r."ABAT2"}, {ml r."ABAN81"}, {ml r."ABAN82"}, {ml r."ABAN83"}, {ml r."ABAN84"}, {ml r."ABAN86"}, {ml r."ABAN85"}, {ml r."ABAC01"}, {ml r."ABAC02"}, {ml r."ABAC03"}, {ml r."ABAC04"}, {ml r."ABAC05"}, {ml r."ABAC06"}, {ml r."ABAC07"}, {ml r."ABAC08"}, {ml r."ABAC09"}, {ml r."ABAC10"}, {ml r."ABAC11"}, {ml r."ABAC12"}, {ml r."ABAC13"}, {ml r."ABAC14"}, {ml r."ABAC15"}, {ml r."ABAC16"}, {ml r."ABAC17"}, {ml r."ABAC18"}, {ml r."ABAC19"}, {ml r."ABAC20"}, {ml r."ABAC21"}, {ml r."ABAC22"}, {ml r."ABAC23"}, {ml r."ABAC24"}, {ml r."ABAC25"}, {ml r."ABAC26"}, {ml r."ABAC27"}, {ml r."ABAC28"}, {ml r."ABAC29"}, {ml r."ABAC30"}, {ml r."ABRMK"}, {ml r."ABTXCT"}, {ml r."ABTX2"}, {ml r."ABALP1"} )
',
	'56C1FFBB32B6C5528A183E75CE6D1C61'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0101"."ABAN8"
FROM "MobileDataModel"."BUSDTA"."F0101"
WHERE ABAN8 in (select SMAN8 from "BUSDTA"."F90CA003_del")
',
	'21B9D6D3AA8534DC1B2F3B12380E666D'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."F0101"
SET "ABALKY" = {ml r."ABALKY"},
	"ABTAX" = {ml r."ABTAX"},
	"ABALPH" = {ml r."ABALPH"},
	"ABMCU" = {ml r."ABMCU"},
	"ABSIC" = {ml r."ABSIC"},
	"ABLNGP" = {ml r."ABLNGP"},
	"ABAT1" = {ml r."ABAT1"},
	"ABCM" = {ml r."ABCM"},
	"ABTAXC" = {ml r."ABTAXC"},
	"ABAT2" = {ml r."ABAT2"},
	"ABAN81" = {ml r."ABAN81"},
	"ABAN82" = {ml r."ABAN82"},
	"ABAN83" = {ml r."ABAN83"},
	"ABAN84" = {ml r."ABAN84"},
	"ABAN86" = {ml r."ABAN86"},
	"ABAN85" = {ml r."ABAN85"},
	"ABAC01" = {ml r."ABAC01"},
	"ABAC02" = {ml r."ABAC02"},
	"ABAC03" = {ml r."ABAC03"},
	"ABAC04" = {ml r."ABAC04"},
	"ABAC05" = {ml r."ABAC05"},
	"ABAC06" = {ml r."ABAC06"},
	"ABAC07" = {ml r."ABAC07"},
	"ABAC08" = {ml r."ABAC08"},
	"ABAC09" = {ml r."ABAC09"},
	"ABAC10" = {ml r."ABAC10"},
	"ABAC11" = {ml r."ABAC11"},
	"ABAC12" = {ml r."ABAC12"},
	"ABAC13" = {ml r."ABAC13"},
	"ABAC14" = {ml r."ABAC14"},
	"ABAC15" = {ml r."ABAC15"},
	"ABAC16" = {ml r."ABAC16"},
	"ABAC17" = {ml r."ABAC17"},
	"ABAC18" = {ml r."ABAC18"},
	"ABAC19" = {ml r."ABAC19"},
	"ABAC20" = {ml r."ABAC20"},
	"ABAC21" = {ml r."ABAC21"},
	"ABAC22" = {ml r."ABAC22"},
	"ABAC23" = {ml r."ABAC23"},
	"ABAC24" = {ml r."ABAC24"},
	"ABAC25" = {ml r."ABAC25"},
	"ABAC26" = {ml r."ABAC26"},
	"ABAC27" = {ml r."ABAC27"},
	"ABAC28" = {ml r."ABAC28"},
	"ABAC29" = {ml r."ABAC29"},
	"ABAC30" = {ml r."ABAC30"},
	"ABRMK" = {ml r."ABRMK"},
	"ABTXCT" = {ml r."ABTXCT"},
	"ABTX2" = {ml r."ABTX2"},
	"ABALP1" = {ml r."ABALP1"}
WHERE "ABAN8" = {ml r."ABAN8"}
',
	'5D04F47410DB5FF6B129BCF65F40035D'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0101',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0115"."WPAN8",
	"MobileDataModel"."BUSDTA"."F0115"."WPIDLN",
	"MobileDataModel"."BUSDTA"."F0115"."WPRCK7",
	"MobileDataModel"."BUSDTA"."F0115"."WPCNLN",
	"MobileDataModel"."BUSDTA"."F0115"."WPPHTP",
	"MobileDataModel"."BUSDTA"."F0115"."WPAR1",
	"MobileDataModel"."BUSDTA"."F0115"."WPPH1"
FROM "MobileDataModel"."BUSDTA"."F0115", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086
WHERE "MobileDataModel"."BUSDTA"."F0115"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and WPAN8=CRCRAN8 and FFUSER= {ml s.username}
',
	'AE8B3A08CE0F55C0869D4A04ED24F339'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0115"."WPAN8",
"MobileDataModel"."BUSDTA"."F0115"."WPIDLN",
"MobileDataModel"."BUSDTA"."F0115"."WPRCK7",
"MobileDataModel"."BUSDTA"."F0115"."WPCNLN"
FROM "MobileDataModel"."BUSDTA"."F0115" where "BUSDTA"."F0115"."WPAN8" in (select SMAN8 from "BUSDTA"."F90CA003_del")
',
	'FE2E1A84D6F88A376EEE54392709593E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0115',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F01151"."EAAN8",
	"MobileDataModel"."BUSDTA"."F01151"."EAIDLN",
	"MobileDataModel"."BUSDTA"."F01151"."EARCK7",
	"MobileDataModel"."BUSDTA"."F01151"."EAETP",
	"MobileDataModel"."BUSDTA"."F01151"."EAEMAL"
FROM "MobileDataModel"."BUSDTA"."F01151", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086
WHERE "MobileDataModel"."BUSDTA"."F01151"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and EAAN8=CRCRAN8 and FFUSER= {ml s.username}
',
	'D92E131DC4EE75ACA457F10D5BAE4CCF'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F01151_del"."EAAN8",
	"MobileDataModel"."BUSDTA"."F01151_del"."EAIDLN",
	"MobileDataModel"."BUSDTA"."F01151_del"."EARCK7"
FROM "MobileDataModel"."BUSDTA"."F01151_del", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086
WHERE "MobileDataModel"."BUSDTA"."F01151_del"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and EAAN8=CRCRAN8 and FFUSER= {ml s.username}
',
	'973A250BF6E1A731067E38622C8BE524'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F01151',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0116"."ALAN8",
	"MobileDataModel"."BUSDTA"."F0116"."ALEFTB",
	"MobileDataModel"."BUSDTA"."F0116"."ALEFTF",
	"MobileDataModel"."BUSDTA"."F0116"."ALADD1",
	"MobileDataModel"."BUSDTA"."F0116"."ALADD2",
	"MobileDataModel"."BUSDTA"."F0116"."ALADD3",
	"MobileDataModel"."BUSDTA"."F0116"."ALADD4",
	"MobileDataModel"."BUSDTA"."F0116"."ALADDZ",
	"MobileDataModel"."BUSDTA"."F0116"."ALCTY1",
	"MobileDataModel"."BUSDTA"."F0116"."ALCOUN",
	"MobileDataModel"."BUSDTA"."F0116"."ALADDS"
FROM "MobileDataModel"."BUSDTA"."F0116", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086
WHERE "MobileDataModel"."BUSDTA"."F0116"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and ALAN8=CRCRAN8 and FFUSER= {ml s.username}
',
	'EA0C84D7ED02F11474DDF18FEE32CF8B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0116_del"."ALAN8",
	"MobileDataModel"."BUSDTA"."F0116_del"."ALEFTB"
FROM "MobileDataModel"."BUSDTA"."F0116_del", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086
WHERE "MobileDataModel"."BUSDTA"."F0116_del"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and ALAN8=CRCRAN8 and FFUSER= {ml s.username}
',
	'30F66E104D5C632AA79A88BE0394FBD6'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0116',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0150"."MAOSTP",
	"MobileDataModel"."BUSDTA"."F0150"."MAPA8",
	"MobileDataModel"."BUSDTA"."F0150"."MAAN8",
	"MobileDataModel"."BUSDTA"."F0150"."MABEFD",
	"MobileDataModel"."BUSDTA"."F0150"."MAEEFD"
FROM "MobileDataModel"."BUSDTA"."F0150", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.F03012
WHERE "MobileDataModel"."BUSDTA"."F0150"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and AIAN8=CRCRAN8 and AIAN8=MAAN8 and FFUSER= {ml s.username}
',
	'C3AB662F86E575D0A9460D38BA56867C'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F0150_del"."MAOSTP",
	"MobileDataModel"."BUSDTA"."F0150_del"."MAPA8",
	"MobileDataModel"."BUSDTA"."F0150_del"."MAAN8"
FROM "MobileDataModel"."BUSDTA"."F0150_del", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.F03012
WHERE "MobileDataModel"."BUSDTA"."F0150_del"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and AIAN8=CRCRAN8 and AIAN8=MAAN8 and FFUSER= {ml s.username}
',
	'F75AD636932F2B323E015B99D552FC24'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F0150',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F03012"."AIAN8",
	"MobileDataModel"."BUSDTA"."F03012"."AICO",
	"MobileDataModel"."BUSDTA"."F03012"."AIMCUR",
	"MobileDataModel"."BUSDTA"."F03012"."AITXA1",
	"MobileDataModel"."BUSDTA"."F03012"."AIEXR1",
	"MobileDataModel"."BUSDTA"."F03012"."AIACL",
	"MobileDataModel"."BUSDTA"."F03012"."AIHDAR",
	"MobileDataModel"."BUSDTA"."F03012"."AITRAR",
	"MobileDataModel"."BUSDTA"."F03012"."AISTTO",
	"MobileDataModel"."BUSDTA"."F03012"."AIRYIN",
	"MobileDataModel"."BUSDTA"."F03012"."AISTMT",
	"MobileDataModel"."BUSDTA"."F03012"."AIARPY",
	"MobileDataModel"."BUSDTA"."F03012"."AISITO",
	"MobileDataModel"."BUSDTA"."F03012"."AICYCN",
	"MobileDataModel"."BUSDTA"."F03012"."AIBO",
	"MobileDataModel"."BUSDTA"."F03012"."AITSTA",
	"MobileDataModel"."BUSDTA"."F03012"."AICKHC",
	"MobileDataModel"."BUSDTA"."F03012"."AIDLC",
	"MobileDataModel"."BUSDTA"."F03012"."AIDNLT",
	"MobileDataModel"."BUSDTA"."F03012"."AIPLCR",
	"MobileDataModel"."BUSDTA"."F03012"."AIRVDJ",
	"MobileDataModel"."BUSDTA"."F03012"."AIDSO",
	"MobileDataModel"."BUSDTA"."F03012"."AICMGR",
	"MobileDataModel"."BUSDTA"."F03012"."AICLMG",
	"MobileDataModel"."BUSDTA"."F03012"."AIAB2",
	"MobileDataModel"."BUSDTA"."F03012"."AIDT1J",
	"MobileDataModel"."BUSDTA"."F03012"."AIDFIJ",
	"MobileDataModel"."BUSDTA"."F03012"."AIDLIJ",
	"MobileDataModel"."BUSDTA"."F03012"."AIDLP",
	"MobileDataModel"."BUSDTA"."F03012"."AIASTY",
	"MobileDataModel"."BUSDTA"."F03012"."AISPYE",
	"MobileDataModel"."BUSDTA"."F03012"."AIAHB",
	"MobileDataModel"."BUSDTA"."F03012"."AIALP",
	"MobileDataModel"."BUSDTA"."F03012"."AIABAM",
	"MobileDataModel"."BUSDTA"."F03012"."AIABA1",
	"MobileDataModel"."BUSDTA"."F03012"."AIAPRC",
	"MobileDataModel"."BUSDTA"."F03012"."AIMAXO",
	"MobileDataModel"."BUSDTA"."F03012"."AIMINO",
	"MobileDataModel"."BUSDTA"."F03012"."AIOYTD",
	"MobileDataModel"."BUSDTA"."F03012"."AIOPY",
	"MobileDataModel"."BUSDTA"."F03012"."AIPOPN",
	"MobileDataModel"."BUSDTA"."F03012"."AIDAOJ",
	"MobileDataModel"."BUSDTA"."F03012"."AIAN8R",
	"MobileDataModel"."BUSDTA"."F03012"."AIBADT",
	"MobileDataModel"."BUSDTA"."F03012"."AICPGP",
	"MobileDataModel"."BUSDTA"."F03012"."AIORTP",
	"MobileDataModel"."BUSDTA"."F03012"."AITRDC",
	"MobileDataModel"."BUSDTA"."F03012"."AIINMG",
	"MobileDataModel"."BUSDTA"."F03012"."AIEXHD",
	"MobileDataModel"."BUSDTA"."F03012"."AIHOLD",
	"MobileDataModel"."BUSDTA"."F03012"."AIROUT",
	"MobileDataModel"."BUSDTA"."F03012"."AISTOP",
	"MobileDataModel"."BUSDTA"."F03012"."AIZON",
	"MobileDataModel"."BUSDTA"."F03012"."AICARS",
	"MobileDataModel"."BUSDTA"."F03012"."AIDEL1",
	"MobileDataModel"."BUSDTA"."F03012"."AIDEL2",
	"MobileDataModel"."BUSDTA"."F03012"."AILTDT",
	"MobileDataModel"."BUSDTA"."F03012"."AIFRTH",
	"MobileDataModel"."BUSDTA"."F03012"."AIAFT",
	"MobileDataModel"."BUSDTA"."F03012"."AIAPTS",
	"MobileDataModel"."BUSDTA"."F03012"."AISBAL",
	"MobileDataModel"."BUSDTA"."F03012"."AIBACK",
	"MobileDataModel"."BUSDTA"."F03012"."AIPORQ",
	"MobileDataModel"."BUSDTA"."F03012"."AIPRIO",
	"MobileDataModel"."BUSDTA"."F03012"."AIARTO",
	"MobileDataModel"."BUSDTA"."F03012"."AIINVC",
	"MobileDataModel"."BUSDTA"."F03012"."AIICON",
	"MobileDataModel"."BUSDTA"."F03012"."AIBLFR",
	"MobileDataModel"."BUSDTA"."F03012"."AINIVD",
	"MobileDataModel"."BUSDTA"."F03012"."AILEDJ",
	"MobileDataModel"."BUSDTA"."F03012"."AIPLST",
	"MobileDataModel"."BUSDTA"."F03012"."AIEDF1",
	"MobileDataModel"."BUSDTA"."F03012"."AIEDF2",
	"MobileDataModel"."BUSDTA"."F03012"."AIASN",
	"MobileDataModel"."BUSDTA"."F03012"."AIDSPA",
	"MobileDataModel"."BUSDTA"."F03012"."AICRMD",
	"MobileDataModel"."BUSDTA"."F03012"."AIAMCR",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC01",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC02",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC03",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC04",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC05",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC06",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC07",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC08",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC09",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC10",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC11",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC12",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC13",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC14",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC15",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC16",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC17",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC18",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC19",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC20",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC21",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC22",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC23",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC24",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC25",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC26",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC27",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC28",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC29",
	"MobileDataModel"."BUSDTA"."F03012"."AIAC30",
	"MobileDataModel"."BUSDTA"."F03012"."AIPRSN",
	"MobileDataModel"."BUSDTA"."F03012"."AIOPBO",
	"MobileDataModel"."BUSDTA"."F03012"."AITIER1",
	"MobileDataModel"."BUSDTA"."F03012"."AIPWPCP",
	"MobileDataModel"."BUSDTA"."F03012"."AICUSTS",
	"MobileDataModel"."BUSDTA"."F03012"."AISTOF",
	"MobileDataModel"."BUSDTA"."F03012"."AITERRID",
	"MobileDataModel"."BUSDTA"."F03012"."AICIG",
	"MobileDataModel"."BUSDTA"."F03012"."AITORG"
FROM "MobileDataModel"."BUSDTA"."F03012", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086
WHERE "MobileDataModel"."BUSDTA"."F03012"."last_modified" >= {ml s.last_table_download}
    AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and AIAN8=CRCRAN8 and FFUSER= {ml s.username}
',
	'43B5B3148D812228CA74D767B244C37B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F03012_del"."AIAN8",
	"MobileDataModel"."BUSDTA"."F03012_del"."AICO"
FROM "MobileDataModel"."BUSDTA"."F03012_del"
WHERE "MobileDataModel"."BUSDTA"."F03012_del"."last_modified" >= {ml s.last_table_download}
',
	'544D94307CDFCF9E9B523602D29F66BB'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F03012',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F40073"."HYPRFR",
	"MobileDataModel"."BUSDTA"."F40073"."HYHYID",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY01",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY02",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY03",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY04",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY05",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY06",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY07",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY08",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY09",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY10",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY11",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY12",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY13",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY14",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY15",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY16",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY17",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY18",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY19",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY20",
	"MobileDataModel"."BUSDTA"."F40073"."HYHY21"
FROM "MobileDataModel"."BUSDTA"."F40073"
WHERE "MobileDataModel"."BUSDTA"."F40073"."last_modified" >= {ml s.last_table_download}
',
	'EB857223FCD41FEDF9A8648CAA836D51'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F40073_del"."HYPRFR",
	"MobileDataModel"."BUSDTA"."F40073_del"."HYHYID"
FROM "MobileDataModel"."BUSDTA"."F40073_del"
WHERE "MobileDataModel"."BUSDTA"."F40073_del"."last_modified" >= {ml s.last_table_download}
',
	'AE520883513321DD5306CC3E246C9E6B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40073',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4015"."OTORTP",
	"MobileDataModel"."BUSDTA"."F4015"."OTAN8",
	"MobileDataModel"."BUSDTA"."F4015"."OTOSEQ",
	"MobileDataModel"."BUSDTA"."F4015"."OTITM",
	"MobileDataModel"."BUSDTA"."F4015"."OTLITM",
	"MobileDataModel"."BUSDTA"."F4015"."OTQTYU",
	"MobileDataModel"."BUSDTA"."F4015"."OTUOM",
	"MobileDataModel"."BUSDTA"."F4015"."OTLNTY",
	"MobileDataModel"."BUSDTA"."F4015"."OTEFTJ",
	"MobileDataModel"."BUSDTA"."F4015"."OTEXDJ"
FROM "MobileDataModel"."BUSDTA"."F4015", BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.F03012
WHERE "MobileDataModel"."BUSDTA"."F4015"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and AIAN8=CRCRAN8 and OTORTP=AIORTP and OTAN8=AIAN8 and FFUSER= {ml s.username}
',
	'F3EBE65A88E3B1E5F724766123A3A9B0'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."F4015"
WHERE "OTORTP" = {ml r."OTORTP"}
	AND "OTAN8" = {ml r."OTAN8"}
	AND "OTOSEQ" = {ml r."OTOSEQ"}
',
	'3C88717BB647F92B6D7DF76379AB7E06'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4015_del"."OTORTP",
	"MobileDataModel"."BUSDTA"."F4015_del"."OTAN8",
	"MobileDataModel"."BUSDTA"."F4015_del"."OTOSEQ"
FROM "MobileDataModel"."BUSDTA"."F4015_del"--, BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.F03012
WHERE "MobileDataModel"."BUSDTA"."F4015_del"."last_modified" >= {ml s.last_table_download}
--AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and AIAN8=CRCRAN8 and OTORTP=AIORTP and OTAN8=AIAN8 and FFUSER= {ml s.username}
',
	'3396357D6C8E5EE209E86ED60D149DD9'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."F4015" ( "OTORTP", "OTAN8", "OTOSEQ", "OTITM", "OTLITM", "OTQTYU", "OTUOM", "OTLNTY", "OTEFTJ", "OTEXDJ" )
VALUES ( {ml r."OTORTP"}, {ml r."OTAN8"}, {ml r."OTOSEQ"}, {ml r."OTITM"}, {ml r."OTLITM"}, {ml r."OTQTYU"}, {ml r."OTUOM"}, {ml r."OTLNTY"}, {ml r."OTEFTJ"}, {ml r."OTEXDJ"} )
',
	'EF8B89133C91FBCB73B170B8973F39F7'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."F4015"
SET "OTITM" = {ml r."OTITM"},
	"OTLITM" = {ml r."OTLITM"},
	"OTQTYU" = {ml r."OTQTYU"},
	"OTUOM" = {ml r."OTUOM"},
	"OTLNTY" = {ml r."OTLNTY"},
	"OTEFTJ" = {ml r."OTEFTJ"},
	"OTEXDJ" = {ml r."OTEXDJ"}
WHERE "OTORTP" = {ml r."OTORTP"}
	AND "OTAN8" = {ml r."OTAN8"}
	AND "OTOSEQ" = {ml r."OTOSEQ"}
',
	'999DE55D52A33368F5F041E044711709'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4015',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4070"."SNASN",
	"MobileDataModel"."BUSDTA"."F4070"."SNOSEQ",
	"MobileDataModel"."BUSDTA"."F4070"."SNANPS",
	"MobileDataModel"."BUSDTA"."F4070"."SNAST",
	"MobileDataModel"."BUSDTA"."F4070"."SNEFTJ",
	"MobileDataModel"."BUSDTA"."F4070"."SNEXDJ"
FROM "MobileDataModel"."BUSDTA"."F4070"
WHERE "MobileDataModel"."BUSDTA"."F4070"."last_modified" >= {ml s.last_table_download}
',
	'9E5D65F2BE164CAFD41657A3CF0D3C5E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4070_del"."SNASN",
	"MobileDataModel"."BUSDTA"."F4070_del"."SNOSEQ",
	"MobileDataModel"."BUSDTA"."F4070_del"."SNANPS"
FROM "MobileDataModel"."BUSDTA"."F4070_del"
WHERE "MobileDataModel"."BUSDTA"."F4070_del"."last_modified" >= {ml s.last_table_download}
',
	'2564FF41DAB9CE670605448F5C8A79E2'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4070',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4071"."ATAST",
	"MobileDataModel"."BUSDTA"."F4071"."ATPRGR",
	"MobileDataModel"."BUSDTA"."F4071"."ATCPGP",
	"MobileDataModel"."BUSDTA"."F4071"."ATSDGR",
	"MobileDataModel"."BUSDTA"."F4071"."ATPRFR",
	"MobileDataModel"."BUSDTA"."F4071"."ATLBT",
	"MobileDataModel"."BUSDTA"."F4071"."ATGLC",
	"MobileDataModel"."BUSDTA"."F4071"."ATSBIF",
	"MobileDataModel"."BUSDTA"."F4071"."ATACNT",
	"MobileDataModel"."BUSDTA"."F4071"."ATLNTY",
	"MobileDataModel"."BUSDTA"."F4071"."ATMDED",
	"MobileDataModel"."BUSDTA"."F4071"."ATABAS",
	"MobileDataModel"."BUSDTA"."F4071"."ATOLVL",
	"MobileDataModel"."BUSDTA"."F4071"."ATTXB",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA01",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA02",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA03",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA04",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA05",
	"MobileDataModel"."BUSDTA"."F4071"."ATENBM",
	"MobileDataModel"."BUSDTA"."F4071"."ATSRFLAG",
	"MobileDataModel"."BUSDTA"."F4071"."ATUSADJ",
	"MobileDataModel"."BUSDTA"."F4071"."ATATIER",
	"MobileDataModel"."BUSDTA"."F4071"."ATBTIER",
	"MobileDataModel"."BUSDTA"."F4071"."ATBNAD",
	"MobileDataModel"."BUSDTA"."F4071"."ATAPRP1",
	"MobileDataModel"."BUSDTA"."F4071"."ATAPRP2",
	"MobileDataModel"."BUSDTA"."F4071"."ATAPRP3",
	"MobileDataModel"."BUSDTA"."F4071"."ATAPRP4",
	"MobileDataModel"."BUSDTA"."F4071"."ATAPRP5",
	"MobileDataModel"."BUSDTA"."F4071"."ATAPRP6",
	"MobileDataModel"."BUSDTA"."F4071"."ATADJGRP",
	"MobileDataModel"."BUSDTA"."F4071"."ATMEADJ",
	"MobileDataModel"."BUSDTA"."F4071"."ATPDCL",
	"MobileDataModel"."BUSDTA"."F4071"."ATUSER",
	"MobileDataModel"."BUSDTA"."F4071"."ATPID",
	"MobileDataModel"."BUSDTA"."F4071"."ATJOBN",
	"MobileDataModel"."BUSDTA"."F4071"."ATUPMJ",
	"MobileDataModel"."BUSDTA"."F4071"."ATTDAY",
	"MobileDataModel"."BUSDTA"."F4071"."ATDIDP",
	"MobileDataModel"."BUSDTA"."F4071"."ATPMTN",
	"MobileDataModel"."BUSDTA"."F4071"."ATPHST",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA06",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA07",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA08",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA09",
	"MobileDataModel"."BUSDTA"."F4071"."ATPA10",
	"MobileDataModel"."BUSDTA"."F4071"."ATEFCN",
	"MobileDataModel"."BUSDTA"."F4071"."ATAPTYPE",
	"MobileDataModel"."BUSDTA"."F4071"."ATMOADJ",
	"MobileDataModel"."BUSDTA"."F4071"."ATPLGRP",
	"MobileDataModel"."BUSDTA"."F4071"."ATEXCPL",
	"MobileDataModel"."BUSDTA"."F4071"."ATUPMX",
	"MobileDataModel"."BUSDTA"."F4071"."ATMNMXAJ",
	"MobileDataModel"."BUSDTA"."F4071"."ATMNMXRL",
	"MobileDataModel"."BUSDTA"."F4071"."ATTSTRSNM",
	"MobileDataModel"."BUSDTA"."F4071"."ATADJQTY"
FROM "MobileDataModel"."BUSDTA"."F4071"
WHERE "MobileDataModel"."BUSDTA"."F4071"."last_modified" >= {ml s.last_table_download}
',
	'97A5720BE165081BEB495653BB6A38C5'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4071_del"."ATAST"
FROM "MobileDataModel"."BUSDTA"."F4071_del"
WHERE "MobileDataModel"."BUSDTA"."F4071_del"."last_modified" >= {ml s.last_table_download}
',
	'70E9CA4CBD8078FD7C2B5F997F0623CA'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4071',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4072"."ADAST",
	"MobileDataModel"."BUSDTA"."F4072"."ADITM",
	"MobileDataModel"."BUSDTA"."F4072"."ADLITM",
	"MobileDataModel"."BUSDTA"."F4072"."ADAITM",
	"MobileDataModel"."BUSDTA"."F4072"."ADAN8",
	"MobileDataModel"."BUSDTA"."F4072"."ADIGID",
	"MobileDataModel"."BUSDTA"."F4072"."ADCGID",
	"MobileDataModel"."BUSDTA"."F4072"."ADOGID",
	"MobileDataModel"."BUSDTA"."F4072"."ADCRCD",
	"MobileDataModel"."BUSDTA"."F4072"."ADUOM",
	"MobileDataModel"."BUSDTA"."F4072"."ADMNQ",
	"MobileDataModel"."BUSDTA"."F4072"."ADEFTJ",
	"MobileDataModel"."BUSDTA"."F4072"."ADEXDJ",
	"MobileDataModel"."BUSDTA"."F4072"."ADBSCD",
	"MobileDataModel"."BUSDTA"."F4072"."ADLEDG",
	"MobileDataModel"."BUSDTA"."F4072"."ADFRMN",
	"MobileDataModel"."BUSDTA"."F4072"."ADFVTR",
	"MobileDataModel"."BUSDTA"."F4072"."ADFGY",
	"MobileDataModel"."BUSDTA"."F4072"."ADATID",
	"MobileDataModel"."BUSDTA"."F4072"."ADNBRORD",
	"MobileDataModel"."BUSDTA"."F4072"."ADUOMVID",
	"MobileDataModel"."BUSDTA"."F4072"."ADFVUM",
	"MobileDataModel"."BUSDTA"."F4072"."ADPARTFG",
	"MobileDataModel"."BUSDTA"."F4072"."ADAPRS",
	"MobileDataModel"."BUSDTA"."F4072"."ADUPMJ",
	"MobileDataModel"."BUSDTA"."F4072"."ADTDAY",
	"MobileDataModel"."BUSDTA"."F4072"."ADBKTPID",
	"MobileDataModel"."BUSDTA"."F4072"."ADCRCDVID",
	"MobileDataModel"."BUSDTA"."F4072"."ADRULENAME"
FROM "MobileDataModel"."BUSDTA"."F4072"
WHERE "MobileDataModel"."BUSDTA"."F4072"."last_modified" >= {ml s.last_table_download}
',
	'3BC017B93522BAECC3B8A923B6A93CDC'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4072_del"."ADAST",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADITM",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADAN8",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADIGID",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADCGID",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADOGID",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADCRCD",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADUOM",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADMNQ",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADEXDJ",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADUPMJ",
	"MobileDataModel"."BUSDTA"."F4072_del"."ADTDAY"
FROM "MobileDataModel"."BUSDTA"."F4072_del"
WHERE "MobileDataModel"."BUSDTA"."F4072_del"."last_modified" >= {ml s.last_table_download}
',
	'6501A0B4ECA711E551F752793D76FE04'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4072',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4075"."VBVBT",
	"MobileDataModel"."BUSDTA"."F4075"."VBCRCD",
	"MobileDataModel"."BUSDTA"."F4075"."VBUOM",
	"MobileDataModel"."BUSDTA"."F4075"."VBUPRC",
	"MobileDataModel"."BUSDTA"."F4075"."VBEFTJ",
	"MobileDataModel"."BUSDTA"."F4075"."VBEXDJ",
	"MobileDataModel"."BUSDTA"."F4075"."VBAPRS",
	"MobileDataModel"."BUSDTA"."F4075"."VBUPMJ",
	"MobileDataModel"."BUSDTA"."F4075"."VBTDAY"
FROM "MobileDataModel"."BUSDTA"."F4075"
WHERE "MobileDataModel"."BUSDTA"."F4075"."last_modified" >= {ml s.last_table_download}
',
	'FDC4BE059783D2BD14CD321030D2B12A'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4075_del"."VBVBT",
	"MobileDataModel"."BUSDTA"."F4075_del"."VBEFTJ",
	"MobileDataModel"."BUSDTA"."F4075_del"."VBUPMJ",
	"MobileDataModel"."BUSDTA"."F4075_del"."VBTDAY"
FROM "MobileDataModel"."BUSDTA"."F4075_del"
WHERE "MobileDataModel"."BUSDTA"."F4075_del"."last_modified" >= {ml s.last_table_download}
',
	'01DB405B76986FE4722F4FFFC9462C82'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4075',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4076"."FMFRMN",
	"MobileDataModel"."BUSDTA"."F4076"."FMFML",
	"MobileDataModel"."BUSDTA"."F4076"."FMAPRS",
	"MobileDataModel"."BUSDTA"."F4076"."FMUPMJ",
	"MobileDataModel"."BUSDTA"."F4076"."FMTDAY"
FROM "MobileDataModel"."BUSDTA"."F4076"
WHERE "MobileDataModel"."BUSDTA"."F4076"."last_modified" >= {ml s.last_table_download}
',
	'DB3172CC3C5D5C6D02EE8B446CAE8557'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4076_del"."FMFRMN",
	"MobileDataModel"."BUSDTA"."F4076_del"."FMUPMJ",
	"MobileDataModel"."BUSDTA"."F4076_del"."FMTDAY"
FROM "MobileDataModel"."BUSDTA"."F4076_del"
WHERE "MobileDataModel"."BUSDTA"."F4076_del"."last_modified" >= {ml s.last_table_download}
',
	'BD8E3F4782A5473F42DBB55AE18AB141'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4076',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4092"."GPGPTY",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPC",
	"MobileDataModel"."BUSDTA"."F4092"."GPDL01",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK1",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK2",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK3",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK4",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK5",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK6",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK7",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK8",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK9",
	"MobileDataModel"."BUSDTA"."F4092"."GPGPK10"
FROM "MobileDataModel"."BUSDTA"."F4092"
WHERE "MobileDataModel"."BUSDTA"."F4092"."last_modified" >= {ml s.last_table_download}
',
	'A7B1BF0BD8C432FD2F44A7D983F4AD2E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4092_del"."GPGPTY",
	"MobileDataModel"."BUSDTA"."F4092_del"."GPGPC"
FROM "MobileDataModel"."BUSDTA"."F4092_del"
WHERE "MobileDataModel"."BUSDTA"."F4092_del"."last_modified" >= {ml s.last_table_download}
',
	'9E972BE1CCC0474A8B42DE2070C5BF7E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4092',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F40941"."IKPRGR",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP1",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP2",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP3",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP4",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP5",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP6",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP7",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP8",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP9",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGP10",
	"MobileDataModel"."BUSDTA"."F40941"."IKIGID"
FROM "MobileDataModel"."BUSDTA"."F40941"
WHERE "MobileDataModel"."BUSDTA"."F40941"."last_modified" >= {ml s.last_table_download}
',
	'15365BBC8788D63EC2367A1078FE96EE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F40941_del"."IKPRGR",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP1",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP2",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP3",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP4",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP5",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP6",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP7",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP8",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP9",
	"MobileDataModel"."BUSDTA"."F40941_del"."IKIGP10"
FROM "MobileDataModel"."BUSDTA"."F40941_del"
WHERE "MobileDataModel"."BUSDTA"."F40941_del"."last_modified" >= {ml s.last_table_download}
',
	'8E010903174878F3DA7572E3E584DC4E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40941',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F40942"."CKCPGP",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP1",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP2",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP3",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP4",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP5",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP6",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP7",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP8",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP9",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGP10",
	"MobileDataModel"."BUSDTA"."F40942"."CKCGID"
FROM "MobileDataModel"."BUSDTA"."F40942"
WHERE "MobileDataModel"."BUSDTA"."F40942"."last_modified" >= {ml s.last_table_download}
',
	'587F010D95D6745E6EF92F7E95A37943'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F40942_del"."CKCPGP",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP1",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP2",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP3",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP4",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP5",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP6",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP7",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP8",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP9",
	"MobileDataModel"."BUSDTA"."F40942_del"."CKCGP10"
FROM "MobileDataModel"."BUSDTA"."F40942_del"
WHERE "MobileDataModel"."BUSDTA"."F40942_del"."last_modified" >= {ml s.last_table_download}
',
	'B0861DD2A7411F4A2A178608F2BE7F77'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F40942',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F41002"."UMMCU",
	"MobileDataModel"."BUSDTA"."F41002"."UMITM",
	"MobileDataModel"."BUSDTA"."F41002"."UMUM",
	"MobileDataModel"."BUSDTA"."F41002"."UMRUM",
	"MobileDataModel"."BUSDTA"."F41002"."UMUSTR",
	"MobileDataModel"."BUSDTA"."F41002"."UMCONV",
	"MobileDataModel"."BUSDTA"."F41002"."UMCNV1"
FROM "MobileDataModel"."BUSDTA"."F41002"
WHERE "MobileDataModel"."BUSDTA"."F41002"."last_modified" >= {ml s.last_table_download}
',
	'90E2D5A186660920ED5F294D0F84D20B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F41002_del"."UMMCU",
	"MobileDataModel"."BUSDTA"."F41002_del"."UMITM",
	"MobileDataModel"."BUSDTA"."F41002_del"."UMUM",
	"MobileDataModel"."BUSDTA"."F41002_del"."UMRUM"
FROM "MobileDataModel"."BUSDTA"."F41002_del", BUSDTA.F56M0001 , BUSDTA.F4102
WHERE -- "MobileDataModel"."BUSDTA"."F41002"."last_modified" >= {ml s.last_table_download} AND
FFUSER= {ml s.username} and FFMCU=IBMCU and IBITM=UMITM
',
	'FB7EBE2971B8EB3A2B250953B354495D'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F41002',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4101"."IMITM",
	"MobileDataModel"."BUSDTA"."F4101"."IMLITM",
	"MobileDataModel"."BUSDTA"."F4101"."IMAITM",
	"MobileDataModel"."BUSDTA"."F4101"."IMDSC1",
	"MobileDataModel"."BUSDTA"."F4101"."IMDSC2",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP1",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP2",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP3",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP4",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP5",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP6",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP7",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP8",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP9",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRP0",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP1",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP2",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP3",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP4",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP5",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP6",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP7",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP8",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP9",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRP0",
	"MobileDataModel"."BUSDTA"."F4101"."IMCDCD",
	"MobileDataModel"."BUSDTA"."F4101"."IMPDGR",
	"MobileDataModel"."BUSDTA"."F4101"."IMDSGP",
	"MobileDataModel"."BUSDTA"."F4101"."IMPRGR",
	"MobileDataModel"."BUSDTA"."F4101"."IMRPRC",
	"MobileDataModel"."BUSDTA"."F4101"."IMORPR",
	"MobileDataModel"."BUSDTA"."F4101"."IMVCUD",
	"MobileDataModel"."BUSDTA"."F4101"."IMUOM1",
	"MobileDataModel"."BUSDTA"."F4101"."IMUOM2",
	"MobileDataModel"."BUSDTA"."F4101"."IMUOM4",
	"MobileDataModel"."BUSDTA"."F4101"."IMUOM6",
	"MobileDataModel"."BUSDTA"."F4101"."IMUWUM",
	"MobileDataModel"."BUSDTA"."F4101"."IMUVM1",
	"MobileDataModel"."BUSDTA"."F4101"."IMCYCL",
	"MobileDataModel"."BUSDTA"."F4101"."IMGLPT",
	"MobileDataModel"."BUSDTA"."F4101"."IMPLEV",
	"MobileDataModel"."BUSDTA"."F4101"."IMPPLV",
	"MobileDataModel"."BUSDTA"."F4101"."IMCLEV",
	"MobileDataModel"."BUSDTA"."F4101"."IMCKAV",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRCE",
	"MobileDataModel"."BUSDTA"."F4101"."IMSTKT",
	"MobileDataModel"."BUSDTA"."F4101"."IMLNTY",
	"MobileDataModel"."BUSDTA"."F4101"."IMBACK",
	"MobileDataModel"."BUSDTA"."F4101"."IMIFLA",
	"MobileDataModel"."BUSDTA"."F4101"."IMTFLA",
	"MobileDataModel"."BUSDTA"."F4101"."IMINMG",
	"MobileDataModel"."BUSDTA"."F4101"."IMABCS",
	"MobileDataModel"."BUSDTA"."F4101"."IMABCM",
	"MobileDataModel"."BUSDTA"."F4101"."IMABCI",
	"MobileDataModel"."BUSDTA"."F4101"."IMOVR",
	"MobileDataModel"."BUSDTA"."F4101"."IMCMCG",
	"MobileDataModel"."BUSDTA"."F4101"."IMSRNR",
	"MobileDataModel"."BUSDTA"."F4101"."IMFIFO",
	"MobileDataModel"."BUSDTA"."F4101"."IMLOTS",
	"MobileDataModel"."BUSDTA"."F4101"."IMSLD",
	"MobileDataModel"."BUSDTA"."F4101"."IMPCTM",
	"MobileDataModel"."BUSDTA"."F4101"."IMMMPC",
	"MobileDataModel"."BUSDTA"."F4101"."IMCMGL",
	"MobileDataModel"."BUSDTA"."F4101"."IMUPCN",
	"MobileDataModel"."BUSDTA"."F4101"."IMUMUP",
	"MobileDataModel"."BUSDTA"."F4101"."IMUMDF",
	"MobileDataModel"."BUSDTA"."F4101"."IMBBDD",
	"MobileDataModel"."BUSDTA"."F4101"."IMCMDM",
	"MobileDataModel"."BUSDTA"."F4101"."IMLECM",
	"MobileDataModel"."BUSDTA"."F4101"."IMLEDD",
	"MobileDataModel"."BUSDTA"."F4101"."IMPEFD",
	"MobileDataModel"."BUSDTA"."F4101"."IMSBDD",
	"MobileDataModel"."BUSDTA"."F4101"."IMU1DD",
	"MobileDataModel"."BUSDTA"."F4101"."IMU2DD",
	"MobileDataModel"."BUSDTA"."F4101"."IMU3DD",
	"MobileDataModel"."BUSDTA"."F4101"."IMU4DD",
	"MobileDataModel"."BUSDTA"."F4101"."IMU5DD",
	"MobileDataModel"."BUSDTA"."F4101"."IMLNPA",
	"MobileDataModel"."BUSDTA"."F4101"."IMLOTC"
FROM "MobileDataModel"."BUSDTA"."F4101", BUSDTA.F4102, BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F4101"."last_modified" >= {ml s.last_table_download}
AND IBITM=IMITM and FFMCU=IBMCU and FFUSER= {ml s.username}
',
	'4108030DBDF82F777859AE5EBC2952DA'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4101_del"."IMITM"
FROM "MobileDataModel"."BUSDTA"."F4101_del", BUSDTA.F4102, BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F4101_del"."last_modified" >= {ml s.last_table_download}
AND IBITM=IMITM and FFMCU=IBMCU and FFUSER= {ml s.username}
',
	'2EBF7748EF954076DB5CAE813D159D34'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4101',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4102"."IBITM",
	"MobileDataModel"."BUSDTA"."F4102"."IBLITM",
	"MobileDataModel"."BUSDTA"."F4102"."IBAITM",
	"MobileDataModel"."BUSDTA"."F4102"."IBMCU",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP1",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP2",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP3",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP4",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP5",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP6",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP7",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP8",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP9",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRP0",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP1",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP2",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP3",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP4",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP5",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP6",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP7",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP8",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP9",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRP0",
	"MobileDataModel"."BUSDTA"."F4102"."IBCDCD",
	"MobileDataModel"."BUSDTA"."F4102"."IBPDGR",
	"MobileDataModel"."BUSDTA"."F4102"."IBDSGP",
	"MobileDataModel"."BUSDTA"."F4102"."IBGLPT",
	"MobileDataModel"."BUSDTA"."F4102"."IBORIG",
	"MobileDataModel"."BUSDTA"."F4102"."IBSAFE",
	"MobileDataModel"."BUSDTA"."F4102"."IBSLD",
	"MobileDataModel"."BUSDTA"."F4102"."IBCKAV",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRCE",
	"MobileDataModel"."BUSDTA"."F4102"."IBLOTS",
	"MobileDataModel"."BUSDTA"."F4102"."IBMMPC",
	"MobileDataModel"."BUSDTA"."F4102"."IBPRGR",
	"MobileDataModel"."BUSDTA"."F4102"."IBRPRC",
	"MobileDataModel"."BUSDTA"."F4102"."IBORPR",
	"MobileDataModel"."BUSDTA"."F4102"."IBBACK",
	"MobileDataModel"."BUSDTA"."F4102"."IBIFLA",
	"MobileDataModel"."BUSDTA"."F4102"."IBABCS",
	"MobileDataModel"."BUSDTA"."F4102"."IBABCM",
	"MobileDataModel"."BUSDTA"."F4102"."IBABCI",
	"MobileDataModel"."BUSDTA"."F4102"."IBOVR",
	"MobileDataModel"."BUSDTA"."F4102"."IBSTKT",
	"MobileDataModel"."BUSDTA"."F4102"."IBLNTY",
	"MobileDataModel"."BUSDTA"."F4102"."IBFIFO",
	"MobileDataModel"."BUSDTA"."F4102"."IBCYCL",
	"MobileDataModel"."BUSDTA"."F4102"."IBINMG",
	"MobileDataModel"."BUSDTA"."F4102"."IBSRNR",
	"MobileDataModel"."BUSDTA"."F4102"."IBPCTM",
	"MobileDataModel"."BUSDTA"."F4102"."IBCMCG",
	"MobileDataModel"."BUSDTA"."F4102"."IBTAX1",
	"MobileDataModel"."BUSDTA"."F4102"."IBBBDD",
	"MobileDataModel"."BUSDTA"."F4102"."IBCMDM",
	"MobileDataModel"."BUSDTA"."F4102"."IBLECM",
	"MobileDataModel"."BUSDTA"."F4102"."IBLEDD",
	"MobileDataModel"."BUSDTA"."F4102"."IBMLOT",
	"MobileDataModel"."BUSDTA"."F4102"."IBSBDD",
	"MobileDataModel"."BUSDTA"."F4102"."IBU1DD",
	"MobileDataModel"."BUSDTA"."F4102"."IBU2DD",
	"MobileDataModel"."BUSDTA"."F4102"."IBU3DD",
	"MobileDataModel"."BUSDTA"."F4102"."IBU4DD",
	"MobileDataModel"."BUSDTA"."F4102"."IBU5DD"
FROM "MobileDataModel"."BUSDTA"."F4102", BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F4102"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} and FFMCU=IBMCU
',
	'92C29DAA850B21CAF64AB958ED52D867'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4102_del"."IBITM",
	"MobileDataModel"."BUSDTA"."F4102_del"."IBMCU"
FROM "MobileDataModel"."BUSDTA"."F4102_del", BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F4102_del"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} and FFMCU=IBMCU
',
	'FECCFDF2D76F4CAF0A61FD61919636CD'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4102',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4106"."BPITM",
	"MobileDataModel"."BUSDTA"."F4106"."BPLITM",
	"MobileDataModel"."BUSDTA"."F4106"."BPMCU",
	"MobileDataModel"."BUSDTA"."F4106"."BPLOCN",
	"MobileDataModel"."BUSDTA"."F4106"."BPLOTN",
	"MobileDataModel"."BUSDTA"."F4106"."BPAN8",
	"MobileDataModel"."BUSDTA"."F4106"."BPIGID",
	"MobileDataModel"."BUSDTA"."F4106"."BPCGID",
	"MobileDataModel"."BUSDTA"."F4106"."BPLOTG",
	"MobileDataModel"."BUSDTA"."F4106"."BPFRMP",
	"MobileDataModel"."BUSDTA"."F4106"."BPCRCD",
	"MobileDataModel"."BUSDTA"."F4106"."BPUOM",
	"MobileDataModel"."BUSDTA"."F4106"."BPEFTJ",
	"MobileDataModel"."BUSDTA"."F4106"."BPEXDJ",
	"MobileDataModel"."BUSDTA"."F4106"."BPUPRC",
	"MobileDataModel"."BUSDTA"."F4106"."BPUPMJ",
	"MobileDataModel"."BUSDTA"."F4106"."BPTDAY"
FROM "MobileDataModel"."BUSDTA"."F4106", BUSDTA.F56M0001 , BUSDTA.F4102
WHERE "MobileDataModel"."BUSDTA"."F4106"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} and FFMCU=IBMCU and IBITM=BPITM
',
	'A2AA0ACBE4EE3DAD8A24794E6D2413C6'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F4106_del"."BPITM",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPMCU",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPLOCN",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPLOTN",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPAN8",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPIGID",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPCGID",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPLOTG",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPFRMP",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPCRCD",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPUOM",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPEXDJ",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPUPMJ",
	"MobileDataModel"."BUSDTA"."F4106_del"."BPTDAY"
FROM "MobileDataModel"."BUSDTA"."F4106_del", BUSDTA.F56M0001 , BUSDTA.F4102
WHERE "MobileDataModel"."BUSDTA"."F4106_del"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} and FFMCU=IBMCU and IBITM=BPITM
',
	'C949EDE3B2161A240E72471AD69E549B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F4106',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F42019"."SHKCOO",
	"MobileDataModel"."BUSDTA"."F42019"."SHDOCO",
	"MobileDataModel"."BUSDTA"."F42019"."SHDCTO",
	"MobileDataModel"."BUSDTA"."F42019"."SHMCU",
	"MobileDataModel"."BUSDTA"."F42019"."SHAN8",
	"MobileDataModel"."BUSDTA"."F42019"."SHSHAN",
	"MobileDataModel"."BUSDTA"."F42019"."SHPA8",
	"MobileDataModel"."BUSDTA"."F42019"."SHDRQJ",
	"MobileDataModel"."BUSDTA"."F42019"."SHTRDJ",
	"MobileDataModel"."BUSDTA"."F42019"."SHPDDJ",
	"MobileDataModel"."BUSDTA"."F42019"."SHADDJ",
	"MobileDataModel"."BUSDTA"."F42019"."SHCNDJ",
	"MobileDataModel"."BUSDTA"."F42019"."SHPEFJ",
	"MobileDataModel"."BUSDTA"."F42019"."SHVR01",
	"MobileDataModel"."BUSDTA"."F42019"."SHVR02",
	"MobileDataModel"."BUSDTA"."F42019"."SHDEL1",
	"MobileDataModel"."BUSDTA"."F42019"."SHDEL2",
	"MobileDataModel"."BUSDTA"."F42019"."SHINMG",
	"MobileDataModel"."BUSDTA"."F42019"."SHPTC",
	"MobileDataModel"."BUSDTA"."F42019"."SHRYIN",
	"MobileDataModel"."BUSDTA"."F42019"."SHASN",
	"MobileDataModel"."BUSDTA"."F42019"."SHPRGP",
	"MobileDataModel"."BUSDTA"."F42019"."SHTXA1",
	"MobileDataModel"."BUSDTA"."F42019"."SHEXR1",
	"MobileDataModel"."BUSDTA"."F42019"."SHTXCT",
	"MobileDataModel"."BUSDTA"."F42019"."SHATXT",
	"MobileDataModel"."BUSDTA"."F42019"."SHHOLD",
	"MobileDataModel"."BUSDTA"."F42019"."SHROUT",
	"MobileDataModel"."BUSDTA"."F42019"."SHSTOP",
	"MobileDataModel"."BUSDTA"."F42019"."SHZON",
	"MobileDataModel"."BUSDTA"."F42019"."SHFRTH",
	"MobileDataModel"."BUSDTA"."F42019"."SHRCD",
	"MobileDataModel"."BUSDTA"."F42019"."SHFUF2",
	"MobileDataModel"."BUSDTA"."F42019"."SHOTOT",
	"MobileDataModel"."BUSDTA"."F42019"."SHAUTN",
	"MobileDataModel"."BUSDTA"."F42019"."SHCACT",
	"MobileDataModel"."BUSDTA"."F42019"."SHCEXP",
	"MobileDataModel"."BUSDTA"."F42019"."SHORBY",
	"MobileDataModel"."BUSDTA"."F42019"."SHTKBY",
	"MobileDataModel"."BUSDTA"."F42019"."SHDOC1",
	"MobileDataModel"."BUSDTA"."F42019"."SHDCT4"
FROM "MobileDataModel"."BUSDTA"."F42019", BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F42019"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} and SHMCU=FFMCU and SHDCTO=''SO''
',
	'AE51D460CA5B5465C922967D3B13EF28'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F42019_del"."SHKCOO",
	"MobileDataModel"."BUSDTA"."F42019_del"."SHDOCO",
	"MobileDataModel"."BUSDTA"."F42019_del"."SHDCTO"
FROM "MobileDataModel"."BUSDTA"."F42019_del", BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F42019_del"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} --and SHMCU=FFMCU and SHDCTO=''SO''
',
	'036C7E28575C578E1A3CD4F313A2C2A3'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42019',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F42119"."SDKCOO",
	"MobileDataModel"."BUSDTA"."F42119"."SDDOCO",
	"MobileDataModel"."BUSDTA"."F42119"."SDDCTO",
	"MobileDataModel"."BUSDTA"."F42119"."SDLNID",
	"MobileDataModel"."BUSDTA"."F42119"."SDMCU",
	"MobileDataModel"."BUSDTA"."F42119"."SDRKCO",
	"MobileDataModel"."BUSDTA"."F42119"."SDRORN",
	"MobileDataModel"."BUSDTA"."F42119"."SDRCTO",
	"MobileDataModel"."BUSDTA"."F42119"."SDRLLN",
	"MobileDataModel"."BUSDTA"."F42119"."SDAN8",
	"MobileDataModel"."BUSDTA"."F42119"."SDSHAN",
	"MobileDataModel"."BUSDTA"."F42119"."SDPA8",
	"MobileDataModel"."BUSDTA"."F42119"."SDDRQJ",
	"MobileDataModel"."BUSDTA"."F42119"."SDTRDJ",
	"MobileDataModel"."BUSDTA"."F42119"."SDPDDJ",
	"MobileDataModel"."BUSDTA"."F42119"."SDADDJ",
	"MobileDataModel"."BUSDTA"."F42119"."SDIVD",
	"MobileDataModel"."BUSDTA"."F42119"."SDCNDJ",
	"MobileDataModel"."BUSDTA"."F42119"."SDDGL",
	"MobileDataModel"."BUSDTA"."F42119"."SDPEFJ",
	"MobileDataModel"."BUSDTA"."F42119"."SDVR01",
	"MobileDataModel"."BUSDTA"."F42119"."SDVR02",
	"MobileDataModel"."BUSDTA"."F42119"."SDITM",
	"MobileDataModel"."BUSDTA"."F42119"."SDLITM",
	"MobileDataModel"."BUSDTA"."F42119"."SDAITM",
	"MobileDataModel"."BUSDTA"."F42119"."SDLOCN",
	"MobileDataModel"."BUSDTA"."F42119"."SDLOTN",
	"MobileDataModel"."BUSDTA"."F42119"."SDDSC1",
	"MobileDataModel"."BUSDTA"."F42119"."SDDSC2",
	"MobileDataModel"."BUSDTA"."F42119"."SDLNTY",
	"MobileDataModel"."BUSDTA"."F42119"."SDNXTR",
	"MobileDataModel"."BUSDTA"."F42119"."SDEMCU",
	"MobileDataModel"."BUSDTA"."F42119"."SDSRP1",
	"MobileDataModel"."BUSDTA"."F42119"."SDSRP2",
	"MobileDataModel"."BUSDTA"."F42119"."SDSRP3",
	"MobileDataModel"."BUSDTA"."F42119"."SDSRP4",
	"MobileDataModel"."BUSDTA"."F42119"."SDSRP5",
	"MobileDataModel"."BUSDTA"."F42119"."SDPRP1",
	"MobileDataModel"."BUSDTA"."F42119"."SDPRP2",
	"MobileDataModel"."BUSDTA"."F42119"."SDPRP3",
	"MobileDataModel"."BUSDTA"."F42119"."SDPRP4",
	"MobileDataModel"."BUSDTA"."F42119"."SDPRP5",
	"MobileDataModel"."BUSDTA"."F42119"."SDUOM",
	"MobileDataModel"."BUSDTA"."F42119"."SDUORG",
	"MobileDataModel"."BUSDTA"."F42119"."SDSOQS",
	"MobileDataModel"."BUSDTA"."F42119"."SDSOBK",
	"MobileDataModel"."BUSDTA"."F42119"."SDSOCN",
	"MobileDataModel"."BUSDTA"."F42119"."SDUPRC",
	"MobileDataModel"."BUSDTA"."F42119"."SDAEXP",
	"MobileDataModel"."BUSDTA"."F42119"."SDPROV",
	"MobileDataModel"."BUSDTA"."F42119"."SDINMG",
	"MobileDataModel"."BUSDTA"."F42119"."SDPTC",
	"MobileDataModel"."BUSDTA"."F42119"."SDASN",
	"MobileDataModel"."BUSDTA"."F42119"."SDPRGR",
	"MobileDataModel"."BUSDTA"."F42119"."SDCLVL",
	"MobileDataModel"."BUSDTA"."F42119"."SDKCO",
	"MobileDataModel"."BUSDTA"."F42119"."SDDOC",
	"MobileDataModel"."BUSDTA"."F42119"."SDDCT",
	"MobileDataModel"."BUSDTA"."F42119"."SDTAX1",
	"MobileDataModel"."BUSDTA"."F42119"."SDTXA1",
	"MobileDataModel"."BUSDTA"."F42119"."SDEXR1",
	"MobileDataModel"."BUSDTA"."F42119"."SDATXT",
	"MobileDataModel"."BUSDTA"."F42119"."SDROUT",
	"MobileDataModel"."BUSDTA"."F42119"."SDSTOP",
	"MobileDataModel"."BUSDTA"."F42119"."SDZON",
	"MobileDataModel"."BUSDTA"."F42119"."SDFRTH",
	"MobileDataModel"."BUSDTA"."F42119"."SDUOM1",
	"MobileDataModel"."BUSDTA"."F42119"."SDPQOR",
	"MobileDataModel"."BUSDTA"."F42119"."SDUOM2",
	"MobileDataModel"."BUSDTA"."F42119"."SDSQOR",
	"MobileDataModel"."BUSDTA"."F42119"."SDUOM4",
	"MobileDataModel"."BUSDTA"."F42119"."SDRPRC",
	"MobileDataModel"."BUSDTA"."F42119"."SDORPR",
	"MobileDataModel"."BUSDTA"."F42119"."SDORP",
	"MobileDataModel"."BUSDTA"."F42119"."SDGLC",
	"MobileDataModel"."BUSDTA"."F42119"."SDCTRY",
	"MobileDataModel"."BUSDTA"."F42119"."SDFY",
	"MobileDataModel"."BUSDTA"."F42119"."SDACOM",
	"MobileDataModel"."BUSDTA"."F42119"."SDCMCG",
	"MobileDataModel"."BUSDTA"."F42119"."SDRCD",
	"MobileDataModel"."BUSDTA"."F42119"."SDUPC1",
	"MobileDataModel"."BUSDTA"."F42119"."SDUPC2",
	"MobileDataModel"."BUSDTA"."F42119"."SDUPC3",
	"MobileDataModel"."BUSDTA"."F42119"."SDTORG",
	"MobileDataModel"."BUSDTA"."F42119"."SDVR03",
	"MobileDataModel"."BUSDTA"."F42119"."SDNUMB",
	"MobileDataModel"."BUSDTA"."F42119"."SDAAID"
FROM "MobileDataModel"."BUSDTA"."F42119", BUSDTA.F56M0001 , BUSDTA.F42019
WHERE "MobileDataModel"."BUSDTA"."F42119"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} and SHMCU=FFMCU and SHDCTO=''SO'' and SHKCOO=SDKCOO and SHDOCO=SDDOCO and SHDCTO=SDDCTO
',
	'6CD1D7D3DA344BB636D779787DE74C0A'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F42119_del"."SDKCOO",
	"MobileDataModel"."BUSDTA"."F42119_del"."SDDOCO",
	"MobileDataModel"."BUSDTA"."F42119_del"."SDDCTO",
	"MobileDataModel"."BUSDTA"."F42119_del"."SDLNID"
FROM "MobileDataModel"."BUSDTA"."F42119_del", BUSDTA.F56M0001 , BUSDTA.F42019
WHERE "MobileDataModel"."BUSDTA"."F42119_del"."last_modified" >= {ml s.last_table_download}
AND FFUSER= {ml s.username} and SHMCU=FFMCU and SHDCTO=''SO'' and SHKCOO=SDKCOO and SHDOCO=SDDOCO and SHDCTO=SDDCTO
',
	'48DD4721FB978013FA51D3B20CE4BE01'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F42119',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F56M0000"."GFSY",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFCXPJ",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFDTEN",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFLAVJ",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFOBJ",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFMCU",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFSUB",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFPST",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFEV01",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFEV02",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFEV03",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFMATH01",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFMATH02",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFMATH03",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFCFSTR1",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFCFSTR2",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFGS1A",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFGS1B",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFGS2A",
	"MobileDataModel"."BUSDTA"."F56M0000"."GFGS2B"
FROM "MobileDataModel"."BUSDTA"."F56M0000"
WHERE "MobileDataModel"."BUSDTA"."F56M0000"."last_modified" >= {ml s.last_table_download}
',
	'EC37900C57C1A113C6453FDBA18391EB'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F56M0000_del"."GFSY"
FROM "MobileDataModel"."BUSDTA"."F56M0000_del"
WHERE "MobileDataModel"."BUSDTA"."F56M0000_del"."last_modified" >= {ml s.last_table_download}
',
	'C4E8C9E39CE16264B4CCB6B5D4BECFDD'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0000',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F56M0001"."FFUSER",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFROUT",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFMCU",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFHMCU",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFBUVAL",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFAN8",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFPA8",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFSTOP",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFZON",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFLOCN",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFLOCF",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFEV01",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFEV02",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFEV03",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFMATH01",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFMATH02",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFMATH03",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFCXPJ",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFCLRJ",
	"MobileDataModel"."BUSDTA"."F56M0001"."FFDTE"
FROM "MobileDataModel"."BUSDTA"."F56M0001"
WHERE "MobileDataModel"."BUSDTA"."F56M0001"."last_modified" >= {ml s.last_table_download}
AND "MobileDataModel"."BUSDTA"."F56M0001"."FFUSER" = {ml s.username}
',
	'01300BEF3D85052E780E7B977CE672D5'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F56M0001_del"."FFUSER",
	"MobileDataModel"."BUSDTA"."F56M0001_del"."FFROUT",
	"MobileDataModel"."BUSDTA"."F56M0001_del"."FFMCU"
FROM "MobileDataModel"."BUSDTA"."F56M0001_del"
WHERE "MobileDataModel"."BUSDTA"."F56M0001_del"."last_modified" >= {ml s.last_table_download}
AND "MobileDataModel"."BUSDTA"."F56M0001_del"."FFUSER" = {ml s.username}
',
	'53B7F9B5FF4C6C681F14B45F49EA728C'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F56M0001',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F90CA003"."SMAN8",
	"MobileDataModel"."BUSDTA"."F90CA003"."SMSLSM"
FROM "MobileDataModel"."BUSDTA"."F90CA003", BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F90CA003"."last_modified" >= {ml s.last_table_download}
    AND FFAN8=SMSLSM and FFUSER= {ml s.username}
',
	'33BE96928A86E8A48597302E51DF4AB7'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F90CA003_del"."SMAN8",
	"MobileDataModel"."BUSDTA"."F90CA003_del"."SMSLSM"
FROM "MobileDataModel"."BUSDTA"."F90CA003_del", BUSDTA.F56M0001
WHERE "MobileDataModel"."BUSDTA"."F90CA003_del"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and FFUSER= {ml s.username}
',
	'DAB8259B6E145B2C95935EC83304EDBC'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA003',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F90CA042"."EMAN8",
	"MobileDataModel"."BUSDTA"."F90CA042"."EMPA8"
FROM "MobileDataModel"."BUSDTA"."F90CA042"
WHERE "MobileDataModel"."BUSDTA"."F90CA042"."last_modified" >= {ml s.last_table_download}
',
	'1FC78C5E6C928D4DF8F004747A7BF79F'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F90CA042_del"."EMAN8",
	"MobileDataModel"."BUSDTA"."F90CA042_del"."EMPA8"
FROM "MobileDataModel"."BUSDTA"."F90CA042_del"
WHERE "MobileDataModel"."BUSDTA"."F90CA042_del"."last_modified" >= {ml s.last_table_download}
',
	'123902F49EEC8DB60D74102A0A863751'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA042',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F90CA086"."CRCUAN8",
	"MobileDataModel"."BUSDTA"."F90CA086"."CRCRAN8"
FROM "MobileDataModel"."BUSDTA"."F90CA086", BUSDTA.F56M0001 , BUSDTA.F90CA003
WHERE --"MobileDataModel"."BUSDTA"."F90CA086"."last_modified" >= {ml s.last_table_download} AND
FFAN8=SMSLSM and SMAN8=CRCUAN8 and FFUSER= {ml s.username}
',
	'484EDCDBFC8E6233217A3AFEFBCB677B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."F90CA086_del"."CRCUAN8",
	"MobileDataModel"."BUSDTA"."F90CA086_del"."CRCRAN8"
FROM "MobileDataModel"."BUSDTA"."F90CA086_del"
where CRCUAN8 in (select SMAN8 from "BUSDTA"."F90CA003_del")
',
	'470B6CC9CB05BBC2597BA33FE39DCB08'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'F90CA086',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."M0111"."CDAN8",
	"MobileDataModel"."BUSDTA"."M0111"."CDIDLN",
	"MobileDataModel"."BUSDTA"."M0111"."CDRCK7",
	"MobileDataModel"."BUSDTA"."M0111"."CDCNLN",
	"MobileDataModel"."BUSDTA"."M0111"."CDAR1",
	"MobileDataModel"."BUSDTA"."M0111"."CDPH1",
	"MobileDataModel"."BUSDTA"."M0111"."CDEXTN1",
	"MobileDataModel"."BUSDTA"."M0111"."CDPHTP1",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLTPH1",
	"MobileDataModel"."BUSDTA"."M0111"."CDREFPH1",
	"MobileDataModel"."BUSDTA"."M0111"."CDAR2",
	"MobileDataModel"."BUSDTA"."M0111"."CDPH2",
	"MobileDataModel"."BUSDTA"."M0111"."CDEXTN2",
	"MobileDataModel"."BUSDTA"."M0111"."CDPHTP2",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLTPH2",
	"MobileDataModel"."BUSDTA"."M0111"."CDREFPH2",
	"MobileDataModel"."BUSDTA"."M0111"."CDAR3",
	"MobileDataModel"."BUSDTA"."M0111"."CDPH3",
	"MobileDataModel"."BUSDTA"."M0111"."CDEXTN3",
	"MobileDataModel"."BUSDTA"."M0111"."CDPHTP3",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLTPH3",
	"MobileDataModel"."BUSDTA"."M0111"."CDREFPH3",
	"MobileDataModel"."BUSDTA"."M0111"."CDAR4",
	"MobileDataModel"."BUSDTA"."M0111"."CDPH4",
	"MobileDataModel"."BUSDTA"."M0111"."CDEXTN4",
	"MobileDataModel"."BUSDTA"."M0111"."CDPHTP4",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLTPH4",
	"MobileDataModel"."BUSDTA"."M0111"."CDREFPH4",
	"MobileDataModel"."BUSDTA"."M0111"."CDEMAL1",
	"MobileDataModel"."BUSDTA"."M0111"."CDETP1",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLTEM1",
	"MobileDataModel"."BUSDTA"."M0111"."CDREFEM1",
	"MobileDataModel"."BUSDTA"."M0111"."CDEMAL2",
	"MobileDataModel"."BUSDTA"."M0111"."CDETP2",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLTEM2",
	"MobileDataModel"."BUSDTA"."M0111"."CDREFEM2",
	"MobileDataModel"."BUSDTA"."M0111"."CDEMAL3",
	"MobileDataModel"."BUSDTA"."M0111"."CDETP3",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLTEM3",
	"MobileDataModel"."BUSDTA"."M0111"."CDREFEM3",
	"MobileDataModel"."BUSDTA"."M0111"."CDGNNM",
	"MobileDataModel"."BUSDTA"."M0111"."CDMDNM",
	"MobileDataModel"."BUSDTA"."M0111"."CDSRNM",
	"MobileDataModel"."BUSDTA"."M0111"."CDTITL",
	"MobileDataModel"."BUSDTA"."M0111"."CDID",
	"MobileDataModel"."BUSDTA"."M0111"."CDACTV",
	"MobileDataModel"."BUSDTA"."M0111"."CDDFLT",
	"MobileDataModel"."BUSDTA"."M0111"."CDSET"
FROM "MobileDataModel"."BUSDTA"."M0111"
WHERE "MobileDataModel"."BUSDTA"."M0111"."last_modified" >= {ml s.last_table_download}
',
	'3CB5836B4B23ED2D128C544AA4E7C980'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."M0111"
WHERE "CDAN8" = {ml r."CDAN8"}
	AND "CDIDLN" = {ml r."CDIDLN"}
	AND "CDRCK7" = {ml r."CDRCK7"}
	AND "CDCNLN" = {ml r."CDCNLN"}
	AND "CDID" = {ml r."CDID"}
',
	'DD7CE18FE435B886048BBA0C5297E0D5'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."M0111_del"."CDAN8",
	"MobileDataModel"."BUSDTA"."M0111_del"."CDIDLN",
	"MobileDataModel"."BUSDTA"."M0111_del"."CDRCK7",
	"MobileDataModel"."BUSDTA"."M0111_del"."CDCNLN"
FROM "MobileDataModel"."BUSDTA"."M0111_del"
WHERE "MobileDataModel"."BUSDTA"."M0111_del"."last_modified" >= {ml s.last_table_download}
',
	'A574545558F9624705596587F93DC00F'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."M0111" ( "CDAN8", "CDIDLN", "CDRCK7", "CDCNLN", "CDAR1", "CDPH1", "CDEXTN1", "CDPHTP1", "CDDFLTPH1", "CDREFPH1", "CDAR2", "CDPH2", "CDEXTN2", "CDPHTP2", "CDDFLTPH2", "CDREFPH2", "CDAR3", "CDPH3", "CDPHTP3", "CDDFLTPH3", "CDREFPH3", "CDAR4", "CDPH4", "CDEXTN4", "CDPHTP4", "CDDFLTPH4", "CDREFPH4", "CDEMAL1", "CDDFLTEM1", "CDREFEM1", "CDEMAL2", "CDETP2", "CDDFLTEM2", "CDREFEM2", "CDEMAL3", "CDDFLTEM3", "CDREFEM3", "CDGNNM", "CDMDNM", "CDSRNM", "CDTITL", "CDID", "CDACTV", "CDDFLT", "CDSET", "CDEXTN3" )
VALUES ( {ml r."CDAN8"}, {ml r."CDIDLN"}, {ml r."CDRCK7"}, {ml r."CDCNLN"}, {ml r."CDAR1"}, {ml r."CDPH1"}, {ml r."CDEXTN1"}, {ml r."CDPHTP1"}, {ml r."CDDFLTPH1"}, {ml r."CDREFPH1"}, {ml r."CDAR2"}, {ml r."CDPH2"}, {ml r."CDEXTN2"}, {ml r."CDPHTP2"}, {ml r."CDDFLTPH2"}, {ml r."CDREFPH2"}, {ml r."CDAR3"}, {ml r."CDPH3"}, {ml r."CDPHTP3"}, {ml r."CDDFLTPH3"}, {ml r."CDREFPH3"}, {ml r."CDAR4"}, {ml r."CDPH4"}, {ml r."CDEXTN4"}, {ml r."CDPHTP4"}, {ml r."CDDFLTPH4"}, {ml r."CDREFPH4"}, {ml r."CDEMAL1"}, {ml r."CDDFLTEM1"}, {ml r."CDREFEM1"}, {ml r."CDEMAL2"}, {ml r."CDETP2"}, {ml r."CDDFLTEM2"}, {ml r."CDREFEM2"}, {ml r."CDEMAL3"}, {ml r."CDDFLTEM3"}, {ml r."CDREFEM3"}, {ml r."CDGNNM"}, {ml r."CDMDNM"}, {ml r."CDSRNM"}, {ml r."CDTITL"}, {ml r."CDID"}, {ml r."CDACTV"}, {ml r."CDDFLT"}, {ml r."CDSET"}, {ml r."CDEXTN3"} )
',
	'03587B3126951BBEDECF2A4924050696'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."M0111"
SET "CDAR1" = {ml r."CDAR1"},
	"CDPH1" = {ml r."CDPH1"},
	"CDEXTN1" = {ml r."CDEXTN1"},
	"CDPHTP1" = {ml r."CDPHTP1"},
	"CDDFLTPH1" = {ml r."CDDFLTPH1"},
	"CDREFPH1" = {ml r."CDREFPH1"},
	"CDAR2" = {ml r."CDAR2"},
	"CDPH2" = {ml r."CDPH2"},
	"CDEXTN2" = {ml r."CDEXTN2"},
	"CDPHTP2" = {ml r."CDPHTP2"},
	"CDDFLTPH2" = {ml r."CDDFLTPH2"},
	"CDREFPH2" = {ml r."CDREFPH2"},
	"CDAR3" = {ml r."CDAR3"},
	"CDPH3" = {ml r."CDPH3"},
	"CDPHTP3" = {ml r."CDPHTP3"},
	"CDDFLTPH3" = {ml r."CDDFLTPH3"},
	"CDREFPH3" = {ml r."CDREFPH3"},
	"CDAR4" = {ml r."CDAR4"},
	"CDPH4" = {ml r."CDPH4"},
	"CDEXTN4" = {ml r."CDEXTN4"},
	"CDPHTP4" = {ml r."CDPHTP4"},
	"CDDFLTPH4" = {ml r."CDDFLTPH4"},
	"CDREFPH4" = {ml r."CDREFPH4"},
	"CDEMAL1" = {ml r."CDEMAL1"},
	"CDDFLTEM1" = {ml r."CDDFLTEM1"},
	"CDREFEM1" = {ml r."CDREFEM1"},
	"CDEMAL2" = {ml r."CDEMAL2"},
	"CDETP2" = {ml r."CDETP2"},
	"CDDFLTEM2" = {ml r."CDDFLTEM2"},
	"CDREFEM2" = {ml r."CDREFEM2"},
	"CDEMAL3" = {ml r."CDEMAL3"},
	"CDDFLTEM3" = {ml r."CDDFLTEM3"},
	"CDREFEM3" = {ml r."CDREFEM3"},
	"CDGNNM" = {ml r."CDGNNM"},
	"CDMDNM" = {ml r."CDMDNM"},
	"CDSRNM" = {ml r."CDSRNM"},
	"CDTITL" = {ml r."CDTITL"},
	"CDACTV" = {ml r."CDACTV"},
	"CDDFLT" = {ml r."CDDFLT"},
	"CDSET" = {ml r."CDSET"},
	"CDEXTN3" = {ml r."CDEXTN3"}
WHERE "CDAN8" = {ml r."CDAN8"}
	AND "CDIDLN" = {ml r."CDIDLN"}
	AND "CDRCK7" = {ml r."CDRCK7"}
	AND "CDCNLN" = {ml r."CDCNLN"}
	AND "CDID" = {ml r."CDID"}
',
	'4DB95AB793CBABB55A36D0EA7842D7E9'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M0111',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."M080111"."CTID",
	"MobileDataModel"."BUSDTA"."M080111"."CTTYP",
	"MobileDataModel"."BUSDTA"."M080111"."CTCD",
	"MobileDataModel"."BUSDTA"."M080111"."CTDSC1",
	"MobileDataModel"."BUSDTA"."M080111"."CTTXA1"
FROM "MobileDataModel"."BUSDTA"."M080111"
WHERE "MobileDataModel"."BUSDTA"."M080111"."last_modified" >= {ml s.last_table_download}
',
	'6D2C87661C2359F75A771281FEDC58CD'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."M080111"
WHERE "CTID" = {ml r."CTID"}
',
	'6D660A051E53356BD95A0F5300A73080'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."M080111" ( "CTID", "CTTYP", "CTCD", "CTDSC1", "CTTXA1" )
VALUES ( {ml r."CTID"}, {ml r."CTTYP"}, {ml r."CTCD"}, {ml r."CTDSC1"}, {ml r."CTTXA1"} )
',
	'52057CCD0D34012B6F9695B0F8B3EDDB'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."M080111_del"."CTID"
FROM "MobileDataModel"."BUSDTA"."M080111_del"
WHERE "MobileDataModel"."BUSDTA"."M080111_del"."last_modified" >= {ml s.last_table_download}
',
	'3DC62F2F7980FEE7BD247B66EB15CF16'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."M080111"
SET "CTTYP" = {ml r."CTTYP"},
	"CTCD" = {ml r."CTCD"},
	"CTDSC1" = {ml r."CTDSC1"},
	"CTTXA1" = {ml r."CTTXA1"}
WHERE "CTID" = {ml r."CTID"}
',
	'F7094901C82225390B20A982D8150A36'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'M080111',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Order_Detail"."Order_Detail_Id",
	"MobileDataModel"."BUSDTA"."Order_Detail"."Order_ID",
	"MobileDataModel"."BUSDTA"."Order_Detail"."Item_Number",
	"MobileDataModel"."BUSDTA"."Order_Detail"."Order_Qty",
	"MobileDataModel"."BUSDTA"."Order_Detail"."Order_UOM",
	"MobileDataModel"."BUSDTA"."Order_Detail"."Unit_Price",
	"MobileDataModel"."BUSDTA"."Order_Detail"."Extn_Price",
	"MobileDataModel"."BUSDTA"."Order_Detail"."Reason_Code",
	"MobileDataModel"."BUSDTA"."Order_Detail"."IsTaxable"
FROM "MobileDataModel"."BUSDTA"."Order_Detail"
WHERE "MobileDataModel"."BUSDTA"."Order_Detail"."last_modified" >= {ml s.last_table_download}
',
	'89FF5EADE2BCD2E45FE529A1FE43C2F2'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."Order_Detail"
WHERE "Order_Detail_Id" = {ml r."Order_Detail_Id"}
	AND "Order_ID" = {ml r."Order_ID"}
',
	'C4326F4E5AC66A26F64E5592151C0FDA'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."Order_Detail" ( "Order_Detail_Id", "Order_ID", "Item_Number", "Order_Qty", "Order_UOM", "Unit_Price", "Extn_Price", "Reason_Code", "IsTaxable" )
VALUES ( {ml r."Order_Detail_Id"}, {ml r."Order_ID"}, {ml r."Item_Number"}, {ml r."Order_Qty"}, {ml r."Order_UOM"}, {ml r."Unit_Price"}, {ml r."Extn_Price"}, {ml r."Reason_Code"}, {ml r."IsTaxable"} )
',
	'0063D72C5DF52C4A53071CA10C4762FD'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Order_Detail_del"."Order_Detail_Id",
	"MobileDataModel"."BUSDTA"."Order_Detail_del"."Order_ID"
FROM "MobileDataModel"."BUSDTA"."Order_Detail_del"
WHERE "MobileDataModel"."BUSDTA"."Order_Detail_del"."last_modified" >= {ml s.last_table_download}
',
	'E8BE5DCFAD4D9C72CB7151D0C781F649'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."Order_Detail"
SET "Item_Number" = {ml r."Item_Number"},
	"Order_Qty" = {ml r."Order_Qty"},
	"Order_UOM" = {ml r."Order_UOM"},
	"Unit_Price" = {ml r."Unit_Price"},
	"Extn_Price" = {ml r."Extn_Price"},
	"Reason_Code" = {ml r."Reason_Code"},
	"IsTaxable" = {ml r."IsTaxable"}
WHERE "Order_Detail_Id" = {ml r."Order_Detail_Id"}
	AND "Order_ID" = {ml r."Order_ID"}
',
	'067A6006A6430C952E7AF35AF407BFD6'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Detail',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Order_Header"."Order_ID",
	"MobileDataModel"."BUSDTA"."Order_Header"."Customer_Id",
	"MobileDataModel"."BUSDTA"."Order_Header"."Order_Date",
	"MobileDataModel"."BUSDTA"."Order_Header"."Created_By",
	"MobileDataModel"."BUSDTA"."Order_Header"."Created_On",
	"MobileDataModel"."BUSDTA"."Order_Header"."Is_Deleted",
	"MobileDataModel"."BUSDTA"."Order_Header"."Total_Coffee",
	"MobileDataModel"."BUSDTA"."Order_Header"."Total_Allied",
	"MobileDataModel"."BUSDTA"."Order_Header"."Energy_Surcharge",
	"MobileDataModel"."BUSDTA"."Order_Header"."Order_Total_Amt",
	"MobileDataModel"."BUSDTA"."Order_Header"."Sales_Tax_Amt",
	"MobileDataModel"."BUSDTA"."Order_Header"."Invoice_Total",
	"MobileDataModel"."BUSDTA"."Order_Header"."Surcharge_Reason_Code",
    "MobileDataModel"."BUSDTA"."Order_Header"."payment_type",
	"MobileDataModel"."BUSDTA"."Order_Header"."payment_id",
	"MobileDataModel"."BUSDTA"."Order_Header"."order_state",
    "MobileDataModel"."BUSDTA"."Order_Header"."Order_Sub_State",
    "MobileDataModel"."BUSDTA"."Order_Header"."updated_at",
    "MobileDataModel"."BUSDTA"."Order_Header"."VoidReason",
    "MobileDataModel"."BUSDTA"."Order_Header"."OrderSeries",
    "MobileDataModel"."BUSDTA"."Order_Header"."RouteNo"
FROM "MobileDataModel"."BUSDTA"."Order_Header"
WHERE "MobileDataModel"."BUSDTA"."Order_Header"."last_modified" >= {ml s.last_table_download}
',
	'505E0604F6A9053C3A8DD398423C41D1'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."Order_Header"
WHERE "Order_ID" = {ml r."Order_ID"}
	AND "OrderSeries" = {ml r."OrderSeries"}
',
	'7A717123E774182355761779B07B43F3'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."Order_Header" ( "Order_ID", "Customer_Id", "Order_Date", "Created_By", "Created_On", "Is_Deleted", "Total_Coffee", "Total_Allied", "Energy_Surcharge", "Order_Total_Amt", "Sales_Tax_Amt", "Invoice_Total", "Surcharge_Reason_Code", "Order_State", "payment_type", "payment_id", "updated_at", "Order_Sub_State", "VoidReason", "OrderSeries", "RouteNo" )
VALUES ( {ml r."Order_ID"}, {ml r."Customer_Id"}, {ml r."Order_Date"}, {ml r."Created_By"}, {ml r."Created_On"}, {ml r."Is_Deleted"}, {ml r."Total_Coffee"}, {ml r."Total_Allied"}, {ml r."Energy_Surcharge"}, {ml r."Order_Total_Amt"}, {ml r."Sales_Tax_Amt"}, {ml r."Invoice_Total"}, {ml r."Surcharge_Reason_Code"}, {ml r."Order_State"}, {ml r."payment_type"}, {ml r."payment_id"}, {ml r."updated_at"}, {ml r."Order_Sub_State"}, {ml r."VoidReason"}, {ml r."OrderSeries"}, {ml r."RouteNo"} )
',
	'A16C677B1829F857DCCBE0CB83260137'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Order_Header_del"."Order_ID",
"MobileDataModel"."BUSDTA"."Order_Header_del"."OrderSeries"
FROM "MobileDataModel"."BUSDTA"."Order_Header_del"
WHERE "MobileDataModel"."BUSDTA"."Order_Header_del"."last_modified" >= {ml s.last_table_download}
',
	'879BFECB62B0F7D781560F88D296BF64'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."Order_Header"
SET "Customer_Id" = {ml r."Customer_Id"},
	"Order_Date" = {ml r."Order_Date"},
	"Created_By" = {ml r."Created_By"},
	"Created_On" = {ml r."Created_On"},
	"Is_Deleted" = {ml r."Is_Deleted"},
	"Total_Coffee" = {ml r."Total_Coffee"},
	"Total_Allied" = {ml r."Total_Allied"},
	"Energy_Surcharge" = {ml r."Energy_Surcharge"},
	"Order_Total_Amt" = {ml r."Order_Total_Amt"},
	"Sales_Tax_Amt" = {ml r."Sales_Tax_Amt"},
	"Invoice_Total" = {ml r."Invoice_Total"},
	"Surcharge_Reason_Code" = {ml r."Surcharge_Reason_Code"},
	"Order_State" = {ml r."Order_State"},
	"payment_type" = {ml r."payment_type"},
	"payment_id" = {ml r."payment_id"},
	"updated_at" = {ml r."updated_at"},
	"Order_Sub_State" = {ml r."Order_Sub_State"},
	"VoidReason" = {ml r."VoidReason"},
	"RouteNo" = {ml r."RouteNo"}
WHERE "Order_ID" = {ml r."Order_ID"}
	AND "OrderSeries" = {ml r."OrderSeries"}
',
	'ACDBC749B395CD783B35C8AE43ABA14E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Order_Header',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."PickOrder_Exception"."PickOrder_Exception_Id",
	"MobileDataModel"."BUSDTA"."PickOrder_Exception"."Order_Id",
	"MobileDataModel"."BUSDTA"."PickOrder_Exception"."Item_Number",
	"MobileDataModel"."BUSDTA"."PickOrder_Exception"."Exception_Qty",
	"MobileDataModel"."BUSDTA"."PickOrder_Exception"."UOM",
	"MobileDataModel"."BUSDTA"."PickOrder_Exception"."Exception_Reason",
	"MobileDataModel"."BUSDTA"."PickOrder_Exception"."ManualPickReasonCode",
	"MobileDataModel"."BUSDTA"."PickOrder_Exception"."ManuallyPickCount"
FROM "MobileDataModel"."BUSDTA"."PickOrder_Exception"
WHERE "MobileDataModel"."BUSDTA"."PickOrder_Exception"."last_modified" >= {ml s.last_table_download}
',
	'02BD125EAEFECBEA963AE137532507F5'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."PickOrder_Exception"
WHERE "PickOrder_Exception_Id" = {ml r."PickOrder_Exception_Id"}
',
	'212367CA8424BE35193C98AC5F8578BF'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."PickOrder_Exception" ( "PickOrder_Exception_Id", "Order_Id", "Item_Number", "Exception_Qty", "UOM", "Exception_Reason", "ManualPickReasonCode", "ManuallyPickCount" )
VALUES ( {ml r."PickOrder_Exception_Id"}, {ml r."Order_Id"}, {ml r."Item_Number"}, {ml r."Exception_Qty"}, {ml r."UOM"}, {ml r."Exception_Reason"}, {ml r."ManualPickReasonCode"}, {ml r."ManuallyPickCount"} )
',
	'44AF5A1658825BFEA9AFD1BF4981A21E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"."PickOrder_Exception_Id"
FROM "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"
WHERE "MobileDataModel"."BUSDTA"."PickOrder_Exception_del"."last_modified" >= {ml s.last_table_download}
',
	'89478808CF4610DEF56E82087B83AA80'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."PickOrder_Exception"
SET "Order_Id" = {ml r."Order_Id"},
	"Item_Number" = {ml r."Item_Number"},
	"Exception_Qty" = {ml r."Exception_Qty"},
	"UOM" = {ml r."UOM"},
	"Exception_Reason" = {ml r."Exception_Reason"},
	"ManualPickReasonCode" = {ml r."ManualPickReasonCode"},
	"ManuallyPickCount" = {ml r."ManuallyPickCount"}
WHERE "PickOrder_Exception_Id" = {ml r."PickOrder_Exception_Id"}
',
	'B2AAB9CEFE20E9B4C896F55F2B2A220E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."PickOrder"."PickOrder_Id",
	"MobileDataModel"."BUSDTA"."PickOrder"."Order_ID",
	"MobileDataModel"."BUSDTA"."PickOrder"."Item_Number",
	"MobileDataModel"."BUSDTA"."PickOrder"."Order_Qty",
	"MobileDataModel"."BUSDTA"."PickOrder"."Order_UOM",
	"MobileDataModel"."BUSDTA"."PickOrder"."Picked_Qty_Primary_UOM",
	"MobileDataModel"."BUSDTA"."PickOrder"."Primary_UOM",
	"MobileDataModel"."BUSDTA"."PickOrder"."Order_Qty_Primary_UOM",
	"MobileDataModel"."BUSDTA"."PickOrder"."On_Hand_Qty_Primary",
	"MobileDataModel"."BUSDTA"."PickOrder"."Last_Scan_Mode",
	"MobileDataModel"."BUSDTA"."PickOrder"."Item_Scan_Sequence",
	"MobileDataModel"."BUSDTA"."PickOrder"."Picked_By",
	"MobileDataModel"."BUSDTA"."PickOrder"."IsOnHold",
	"MobileDataModel"."BUSDTA"."PickOrder"."Reason_Code_Id",
	"MobileDataModel"."BUSDTA"."PickOrder"."ManuallyPickCount"
FROM "MobileDataModel"."BUSDTA"."PickOrder"
WHERE "MobileDataModel"."BUSDTA"."PickOrder"."last_modified" >= {ml s.last_table_download}
',
	'83BEBE322EE6B535AC28F520346B6665'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."PickOrder"
WHERE "PickOrder_Id" = {ml r."PickOrder_Id"}
',
	'2F936F6950658D0213D030450C607C33'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."PickOrder" ( "PickOrder_Id", "Order_ID", "Item_Number", "Order_Qty", "Order_UOM", "Picked_Qty_Primary_UOM", "Primary_UOM", "Order_Qty_Primary_UOM", "On_Hand_Qty_Primary", "Last_Scan_Mode", "Item_Scan_Sequence", "Picked_By", "IsOnHold", "Reason_Code_Id", "ManuallyPickCount" )
VALUES ( {ml r."PickOrder_Id"}, {ml r."Order_ID"}, {ml r."Item_Number"}, {ml r."Order_Qty"}, {ml r."Order_UOM"}, {ml r."Picked_Qty_Primary_UOM"}, {ml r."Primary_UOM"}, {ml r."Order_Qty_Primary_UOM"}, {ml r."On_Hand_Qty_Primary"}, {ml r."Last_Scan_Mode"}, {ml r."Item_Scan_Sequence"}, {ml r."Picked_By"}, {ml r."IsOnHold"}, {ml r."Reason_Code_Id"}, {ml r."ManuallyPickCount"} )
',
	'3DFE3B63D25E6071EC663114B35D7FA2'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."PickOrder_del"."PickOrder_Id"
FROM "MobileDataModel"."BUSDTA"."PickOrder_del"
WHERE "MobileDataModel"."BUSDTA"."PickOrder_del"."last_modified" >= {ml s.last_table_download}
',
	'7C2BF8903EF4B6A974BA443997165EA3'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."PickOrder"
SET "Order_ID" = {ml r."Order_ID"},
	"Item_Number" = {ml r."Item_Number"},
	"Order_Qty" = {ml r."Order_Qty"},
	"Order_UOM" = {ml r."Order_UOM"},
	"Picked_Qty_Primary_UOM" = {ml r."Picked_Qty_Primary_UOM"},
	"Primary_UOM" = {ml r."Primary_UOM"},
	"Order_Qty_Primary_UOM" = {ml r."Order_Qty_Primary_UOM"},
	"On_Hand_Qty_Primary" = {ml r."On_Hand_Qty_Primary"},
	"Last_Scan_Mode" = {ml r."Last_Scan_Mode"},
	"Item_Scan_Sequence" = {ml r."Item_Scan_Sequence"},
	"Picked_By" = {ml r."Picked_By"},
	"IsOnHold" = {ml r."IsOnHold"},
	"Reason_Code_Id" = {ml r."Reason_Code_Id"},
	"ManuallyPickCount" = {ml r."ManuallyPickCount"}
WHERE "PickOrder_Id" = {ml r."PickOrder_Id"}
',
	'44C00B2B98502E68DCE64406B0D3C9E4'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'PickOrder',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."ReasonCodeMaster"."ReasonCodeId",
	"MobileDataModel"."BUSDTA"."ReasonCodeMaster"."ReasonCode",
	"MobileDataModel"."BUSDTA"."ReasonCodeMaster"."ReasonCodeDescription",
	"MobileDataModel"."BUSDTA"."ReasonCodeMaster"."ReasonCodeType"
FROM "MobileDataModel"."BUSDTA"."ReasonCodeMaster"
WHERE "MobileDataModel"."BUSDTA"."ReasonCodeMaster"."last_modified" >= {ml s.last_table_download}
',
	'A8DC1C159B676E8EA83DEB6217DFB54B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"."ReasonCodeId"
FROM "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"
WHERE "MobileDataModel"."BUSDTA"."ReasonCodeMaster_del"."last_modified" >= {ml s.last_table_download}
',
	'5C2C6425D96ABED48E29159C813EEDBE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Route_Device_Map"."Route_Id",
	"MobileDataModel"."BUSDTA"."Route_Device_Map"."Device_Id",
	"MobileDataModel"."BUSDTA"."Route_Device_Map"."Active",
	"MobileDataModel"."BUSDTA"."Route_Device_Map"."Remote_Id"
FROM "MobileDataModel"."BUSDTA"."Route_Device_Map"
WHERE "MobileDataModel"."BUSDTA"."Route_Device_Map"."last_modified" >= {ml s.last_table_download}
',
	'2E8439E03C9D26D3B157FAC9165DF708'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'upload_delete',
	'sql',
'
/* Delete the row from the consolidated database. */
DELETE FROM "MobileDataModel"."BUSDTA"."Route_Device_Map"
WHERE "Route_Id" = {ml r."Route_Id"}
	AND "Device_Id" = {ml r."Device_Id"}
',
	'75C7426622000FFCF459BF1EFA59482A'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'upload_insert',
	'sql',
'
/* Insert the row into the consolidated database. */
INSERT INTO "MobileDataModel"."BUSDTA"."Route_Device_Map" ( "Route_Id", "Device_Id", "Active", "Remote_Id" )
VALUES ( {ml r."Route_Id"}, {ml r."Device_Id"}, {ml r."Active"}, {ml r."Remote_Id"} )
',
	'C1049A201F45F4B7C7B54079B778954B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Route_Device_Map_del"."Route_Id",
	"MobileDataModel"."BUSDTA"."Route_Device_Map_del"."Device_Id"
FROM "MobileDataModel"."BUSDTA"."Route_Device_Map_del"
WHERE "MobileDataModel"."BUSDTA"."Route_Device_Map_del"."last_modified" >= {ml s.last_table_download}
',
	'975134143F09B985ED3DB0A15D7E51DE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'upload_update',
	'sql',
'
/* Update the row in the consolidated database. */

UPDATE "MobileDataModel"."BUSDTA"."Route_Device_Map"
SET "Active" = {ml r."Active"},
	"Remote_Id" = {ml r."Remote_Id"}
WHERE "Route_Id" = {ml r."Route_Id"}
	AND "Device_Id" = {ml r."Device_Id"}
',
	'E1D2D8B0C54C0ED88AF9C7286F12C56A'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_Device_Map',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Route_User_Map"."App_user_id",
	"MobileDataModel"."BUSDTA"."Route_User_Map"."Route_Id",
	"MobileDataModel"."BUSDTA"."Route_User_Map"."Active",
	"MobileDataModel"."BUSDTA"."Route_User_Map"."Default_Route"
FROM "MobileDataModel"."BUSDTA"."Route_User_Map"
WHERE "MobileDataModel"."BUSDTA"."Route_User_Map"."last_modified" >= {ml s.last_table_download}
',
	'0F50A0A1C8A93C6E49845D2375C84E08'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."Route_User_Map_del"."App_user_id",
	"MobileDataModel"."BUSDTA"."Route_User_Map_del"."Route_Id"
FROM "MobileDataModel"."BUSDTA"."Route_User_Map_del"
WHERE "MobileDataModel"."BUSDTA"."Route_User_Map_del"."last_modified" >= {ml s.last_table_download}
',
	'D57574C3D4FC67BE1B290FD989B9E343'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'Route_User_Map',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."UDCKEYLIST"."DTSY",
	"MobileDataModel"."BUSDTA"."UDCKEYLIST"."DTRT"
FROM "MobileDataModel"."BUSDTA"."UDCKEYLIST"
WHERE "MobileDataModel"."BUSDTA"."UDCKEYLIST"."last_modified" >= {ml s.last_table_download}
',
	'E408320A7D980F44194F80E864F4B698'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"."DTSY",
	"MobileDataModel"."BUSDTA"."UDCKEYLIST_del"."DTRT"
FROM "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"
WHERE "MobileDataModel"."BUSDTA"."UDCKEYLIST_del"."last_modified" >= {ml s.last_table_download}
',
	'86318DED8B674694EA162BF15DBBD0ED'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."user_master"."App_user_id",
	"MobileDataModel"."BUSDTA"."user_master"."App_User",
	"MobileDataModel"."BUSDTA"."user_master"."Name",
	"MobileDataModel"."BUSDTA"."user_master"."DomainUser",
	"MobileDataModel"."BUSDTA"."user_master"."AppPassword",
	"MobileDataModel"."BUSDTA"."user_master"."active",
	"MobileDataModel"."BUSDTA"."user_master"."Created_On"
FROM "MobileDataModel"."BUSDTA"."user_master"
WHERE "MobileDataModel"."BUSDTA"."user_master"."last_modified" >= {ml s.last_table_download}
',
	'4E50BEC986AB175AA53916584AFE9D28'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."user_master_del"."App_user_id"
FROM "MobileDataModel"."BUSDTA"."user_master_del"
WHERE "MobileDataModel"."BUSDTA"."user_master_del"."last_modified" >= {ml s.last_table_download}
',
	'EB9AD25F455B98F81647BECF4859880E'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'user_master',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'upload_old_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'upload_fetch_column_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'upload_fetch',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'download_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."User_Role_Map"."App_user_id",
	"MobileDataModel"."BUSDTA"."User_Role_Map"."Role"
FROM "MobileDataModel"."BUSDTA"."User_Role_Map"
WHERE "MobileDataModel"."BUSDTA"."User_Role_Map"."last_modified" >= {ml s.last_table_download}
',
	'F16D3540ADC6C3085A3D81388CCE3D3B'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'upload_delete',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'upload_insert',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'download_delete_cursor',
	'sql',
'
SELECT "MobileDataModel"."BUSDTA"."User_Role_Map_del"."App_user_id"
FROM "MobileDataModel"."BUSDTA"."User_Role_Map_del"
WHERE "MobileDataModel"."BUSDTA"."User_Role_Map_del"."last_modified" >= {ml s.last_table_download}
',
	'185F0179C56B81E643739F4B00422765'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'upload_new_row_insert',
	'sql',
	null,
	null

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'upload_update',
	'sql',
'
--{ml_ignore}
',
	'549D0C590F803284690089186C78C2FE'

GO

EXEC ml_add_lang_table_script_chk
	'SLE_RemoteDB',
	'User_Role_Map',
	'resolve_conflict',
	'sql',
	null,
	null

GO

EXEC ml_add_column
	'SLE_RemoteDB',
	'Device_Master',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Device_Master',
	'Device_Id',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Device_Master',
	'Active',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Device_Master',
	'manufacturer',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Device_Master',
	'model',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0004',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0004',
	'DTSY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0004',
	'DTRT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0004',
	'DTDL01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0004',
	'DTCDL',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0004',
	'DTLN2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0004',
	'DTCNUM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	'DRSY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	'DRRT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	'DRKY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	'DRDL01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	'DRDL02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	'DRSPHD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0005',
	'DRHRDC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCSTYL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCLDM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCCO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCAN8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCDL01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP03',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP04',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP05',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP06',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP07',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP08',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP09',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP10',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP11',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP12',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP13',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP14',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP15',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP16',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP17',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP18',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP19',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP20',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP21',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP22',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP23',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP24',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP25',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP26',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP27',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP28',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP29',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0006',
	'MCRP30',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNPTC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNPTD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNDCP',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNDCD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNNDTP',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNNSP',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNDTPA',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNPXDM',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0014',
	'PNPXDD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABALKY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABTAX',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABALPH',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABSIC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABLNGP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAT1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABCM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABTAXC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAT2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAN81',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAN82',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAN83',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAN84',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAN86',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAN85',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC03',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC04',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC05',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC06',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC07',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC08',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC09',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC10',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC11',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC12',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC13',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC14',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC15',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC16',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC17',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC18',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC19',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC20',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC21',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC22',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC23',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC24',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC25',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC26',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC27',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC28',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC29',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABAC30',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABRMK',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABTXCT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABTX2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0101',
	'ABALP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	'WPAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	'WPIDLN',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	'WPRCK7',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	'WPCNLN',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	'WPPHTP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	'WPAR1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0115',
	'WPPH1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F01151',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F01151',
	'EAAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F01151',
	'EAIDLN',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F01151',
	'EARCK7',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F01151',
	'EAETP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F01151',
	'EAEMAL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALEFTB',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALEFTF',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALADD1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALADD2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALADD3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALADD4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALADDZ',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALCTY1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALCOUN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0116',
	'ALADDS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0150',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0150',
	'MAOSTP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0150',
	'MAPA8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0150',
	'MAAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0150',
	'MABEFD',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F0150',
	'MAEEFD',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIMCUR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AITXA1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIEXR1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIACL',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIHDAR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AITRAR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AISTTO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIRYIN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AISTMT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIARPY',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AISITO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICYCN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIBO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AITSTA',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICKHC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDLC',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDNLT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPLCR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIRVDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDSO',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICMGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICLMG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAB2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDT1J',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDFIJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDLIJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDLP',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIASTY',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AISPYE',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAHB',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIALP',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIABAM',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIABA1',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAPRC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIMAXO',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIMINO',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIOYTD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIOPY',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPOPN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDAOJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAN8R',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIBADT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICPGP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIORTP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AITRDC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIINMG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIEXHD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIHOLD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIROUT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AISTOP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIZON',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICARS',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDEL1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDEL2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AILTDT',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIFRTH',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAFT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAPTS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AISBAL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIBACK',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPORQ',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPRIO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIARTO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIINVC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIICON',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIBLFR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AINIVD',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AILEDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPLST',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPLST',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIEDF1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIEDF2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIASN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIDSPA',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICRMD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAMCR',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC03',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC04',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC05',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC06',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC07',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC08',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC09',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC10',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC11',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC12',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC13',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC14',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC15',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC16',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC17',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC18',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC19',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC20',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC21',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC22',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC23',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC24',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC25',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC26',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC27',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC28',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIAC30',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPRSN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIOPBO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AITIER1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AIPWPCP',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICUSTS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AISTOF',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AITERRID',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AICIG',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F03012',
	'AITORG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYPRFR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHYID',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY01',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY02',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY03',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY04',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY05',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY06',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY07',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY08',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY09',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY10',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY11',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY12',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY13',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY14',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY15',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY16',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY17',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY18',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY19',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY20',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40073',
	'HYHY21',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTORTP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTOSEQ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTITM',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTLITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTQTYU',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTUOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTLNTY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTEFTJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4015',
	'OTEXDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4070',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4070',
	'SNASN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4070',
	'SNOSEQ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4070',
	'SNANPS',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4070',
	'SNAST',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4070',
	'SNEFTJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4070',
	'SNEXDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAST',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPRGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATCPGP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATSDGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPRFR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATLBT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATGLC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATSBIF',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATACNT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATLNTY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATMDED',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATABAS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATOLVL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATTXB',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA03',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA04',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA05',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATENBM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATSRFLAG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATUSADJ',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATATIER',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATBTIER',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATBNAD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAPRP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAPRP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAPRP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAPRP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAPRP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAPRP6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATADJGRP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATMEADJ',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPDCL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATUSER',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPID',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATJOBN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATUPMJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATTDAY',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATDIDP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPMTN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPHST',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA06',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA07',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA08',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA09',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPA10',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATEFCN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATAPTYPE',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATMOADJ',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATPLGRP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATEXCPL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATUPMX',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATMNMXAJ',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATMNMXRL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATTSTRSNM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4071',
	'ATADJQTY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADAST',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADITM',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADLITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADAITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADIGID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADCGID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADOGID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADCRCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADUOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADMNQ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADEFTJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADEXDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADBSCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADLEDG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADFRMN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADFVTR',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADFGY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADATID',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADNBRORD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADUOMVID',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADFVUM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADPARTFG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADAPRS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADUPMJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADTDAY',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADBKTPID',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADCRCDVID',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4072',
	'ADRULENAME',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBVBT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBCRCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBUOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBUPRC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBEFTJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBEXDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBAPRS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBUPMJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4075',
	'VBTDAY',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4076',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4076',
	'FMFRMN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4076',
	'FMFML',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4076',
	'FMAPRS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4076',
	'FMUPMJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4076',
	'FMTDAY',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPTY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPDL01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK7',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK8',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK9',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4092',
	'GPGPK10',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKPRGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP7',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP8',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP9',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGP10',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40941',
	'IKIGID',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCPGP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP7',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP8',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP9',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGP10',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F40942',
	'CKCGID',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	'UMMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	'UMITM',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	'UMUM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	'UMRUM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	'UMUSTR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	'UMCONV',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F41002',
	'UMCNV1',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMITM',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMLITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMAITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMDSC1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMDSC2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP7',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP8',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP9',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRP0',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP7',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP8',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP9',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRP0',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMCDCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPDGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMDSGP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPRGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMRPRC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMORPR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMVCUD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUOM1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUOM2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUOM4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUOM6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUWUM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUVM1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMCYCL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMGLPT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPLEV',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPPLV',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMCLEV',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMCKAV',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRCE',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSTKT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMLNTY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMBACK',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMIFLA',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMTFLA',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMINMG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMABCS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMABCM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMABCI',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMOVR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMCMCG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSRNR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMFIFO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMLOTS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSLD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPCTM',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMMMPC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMCMGL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUPCN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUMUP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMUMDF',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMBBDD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMCMDM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMLECM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMLEDD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMPEFD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMSBDD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMU1DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMU2DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMU3DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMU4DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMU5DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMLNPA',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4101',
	'IMLOTC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBITM',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBLITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBAITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP7',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP8',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP9',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRP0',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP6',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP7',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP8',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP9',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRP0',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBCDCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPDGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBDSGP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBGLPT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBORIG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSAFE',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSLD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBCKAV',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRCE',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBLOTS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBMMPC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPRGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBRPRC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBORPR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBBACK',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBIFLA',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBABCS',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBABCM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBABCI',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBOVR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSTKT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBLNTY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBFIFO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBCYCL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBINMG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSRNR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBPCTM',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBCMCG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBTAX1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBBBDD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBCMDM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBLECM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBLEDD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBMLOT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBSBDD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBU1DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBU2DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBU3DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBU4DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4102',
	'IBU5DD',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPITM',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPLITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPLOCN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPLOTN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPIGID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPCGID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPLOTG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPFRMP',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPCRCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPUOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPEFTJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPEXDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPUPRC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPUPMJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F4106',
	'BPTDAY',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHKCOO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHDOCO',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHDCTO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHAN8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHSHAN',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHPA8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHDRQJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHTRDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHPDDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHADDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHCNDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHPEFJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHVR01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHVR02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHDEL1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHDEL2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHINMG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHPTC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHRYIN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHASN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHPRGP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHTXA1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHEXR1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHTXCT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHATXT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHHOLD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHROUT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHSTOP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHZON',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHFRTH',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHRCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHFUF2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHOTOT',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHAUTN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHCACT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHCEXP',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHORBY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHTKBY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHDOC1',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42019',
	'SHDCT4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDKCOO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDOCO',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDCTO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDLNID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDRKCO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDRORN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDRCTO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDRLLN',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDAN8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSHAN',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPA8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDRQJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDTRDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPDDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDADDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDIVD',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDCNDJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDGL',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPEFJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDVR01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDVR02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDITM',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDLITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDAITM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDLOCN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDLOTN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDSC1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDSC2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDLNTY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDNXTR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDEMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSRP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSRP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSRP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSRP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSRP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPRP1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPRP2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPRP3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPRP4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPRP5',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUORG',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSOQS',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSOBK',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSOCN',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUPRC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDAEXP',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPROV',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDINMG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPTC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDASN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPRGR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDCLVL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDKCO',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDOC',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDDCT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDTAX1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDTXA1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDEXR1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDATXT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDROUT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSTOP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDZON',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDFRTH',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUOM1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDPQOR',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUOM2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDSQOR',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUOM4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDRPRC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDORPR',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDORP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDGLC',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDCTRY',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDFY',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDACOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDCMCG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDRCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUPC1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUPC2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDUPC3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDTORG',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDVR03',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDNUMB',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F42119',
	'SDAAID',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFSY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFCXPJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFDTEN',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFLAVJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFOBJ',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFSUB',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFPST',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFEV01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFEV02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFEV03',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFMATH01',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFMATH02',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFMATH03',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFCFSTR1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFCFSTR2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFGS1A',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFGS1B',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFGS2A',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0000',
	'GFGS2B',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFUSER',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFROUT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFHMCU',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFBUVAL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFAN8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFPA8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFSTOP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFZON',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFLOCN',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFLOCF',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFEV01',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFEV02',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFEV03',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFMATH01',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFMATH02',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFMATH03',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFCXPJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFCLRJ',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F56M0001',
	'FFDTE',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA003',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA003',
	'SMAN8',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA003',
	'SMSLSM',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA042',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA042',
	'EMAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA042',
	'EMPA8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA086',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA086',
	'CRCUAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'F90CA086',
	'CRCRAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDAN8',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDIDLN',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDRCK7',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDCNLN',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDAR1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPH1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDEXTN1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPHTP1',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLTPH1',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDREFPH1',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDAR2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPH2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDEXTN2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPHTP2',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLTPH2',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDREFPH2',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDAR3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPH3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDEXTN3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPHTP3',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLTPH3',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDREFPH3',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDAR4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPH4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDEXTN4',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDPHTP4',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLTPH4',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDREFPH4',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDEMAL1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLTEM1',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDREFEM1',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDEMAL2',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDETP2',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLTEM2',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDREFEM2',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDEMAL3',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLTEM3',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDREFEM3',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDGNNM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDMDNM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDSRNM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDTITL',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDACTV',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDDFLT',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M0111',
	'CDSET',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M080111',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M080111',
	'CTID',
	'numeric'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M080111',
	'CTTYP',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M080111',
	'CTCD',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M080111',
	'CTDSC1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'M080111',
	'CTTXA1',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Order_Detail_Id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Order_ID',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Item_Number',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Order_Qty',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Order_UOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Unit_Price',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Extn_Price',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'Reason_Code',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Detail',
	'IsTaxable',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Order_ID',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Customer_Id',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Order_Date',
	'date'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Created_By',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Created_On',
	'date'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Is_Deleted',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Total_Coffee',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Total_Allied',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Energy_Surcharge',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Order_Total_Amt',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Sales_Tax_Amt',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Invoice_Total',
	'float'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Surcharge_Reason_Code',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'payment_type',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'payment_id',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Order_State',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'Order_Sub_State',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'updated_at',
	'datetime'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'VoidReason',
	'bigint'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'OrderSeries',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Order_Header',
	'RouteNo',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'PickOrder_Exception_Id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'Order_Id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'Item_Number',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'Exception_Qty',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'UOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'Exception_Reason',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'ManualPickReasonCode',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder_Exception',
	'ManuallyPickCount',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'PickOrder_Id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Order_ID',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Item_Number',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Order_Qty',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Order_UOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Picked_Qty_Primary_UOM',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Primary_UOM',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Order_Qty_Primary_UOM',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'On_Hand_Qty_Primary',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Last_Scan_Mode',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Item_Scan_Sequence',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Picked_By',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'IsOnHold',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'Reason_Code_Id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'PickOrder',
	'ManuallyPickCount',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'ReasonCodeId',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'ReasonCode',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'ReasonCodeDescription',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'ReasonCodeMaster',
	'ReasonCodeType',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_Device_Map',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_Device_Map',
	'Route_Id',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_Device_Map',
	'Device_Id',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_Device_Map',
	'Active',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_Device_Map',
	'Remote_Id',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_User_Map',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_User_Map',
	'App_user_id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_User_Map',
	'Route_Id',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_User_Map',
	'Active',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'Route_User_Map',
	'Default_Route',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'UDCKEYLIST',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'DTSY',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'UDCKEYLIST',
	'DTRT',
	'nvarchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	'App_user_id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	'App_User',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	'Name',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	'DomainUser',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	'AppPassword',
	'varchar'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	'active',
	'bit'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'user_master',
	'Created_On',
	'datetime'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'User_Role_Map',
	null,
	null

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'User_Role_Map',
	'App_user_id',
	'integer'

GO
EXEC ml_add_column
	'SLE_RemoteDB',
	'User_Role_Map',
	'Role',
	'varchar'

GO

EXEC ml_add_user 'FBM783', 0xda6dea8d1be996950312b387f3b49f9ad333ecf71af6a8081325766c493af0c4, null
GO

COMMIT
GO
