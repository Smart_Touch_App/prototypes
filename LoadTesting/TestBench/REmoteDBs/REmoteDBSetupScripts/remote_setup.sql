/*------------------------------------------------------------------------------
* ML Install Script generated 2015-01-14 17:14:40 for SQL Anywhere (Remote) by MobiLink
*                                  16 Plug-in
*-----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
*                 Drop synchronization profile 'busdta_FBM109'.
*-----------------------------------------------------------------------------*/
DROP SYNCHRONIZATION PROFILE IF EXISTS "prof_validate_user"
GO

/*------------------------------------------------------------------------------
* Drop the subscription 'busdta_FBM109' and any other subscription for MobiLink
*                    user 'FBM109' to publication 'busdta'.
*-----------------------------------------------------------------------------*/
IF EXISTS (
	SELECT 1
	FROM SYS.SYSSYNCSUBSCRIPTIONS
	WHERE site_name = 'FBM109' AND publication_name = 'busdta'
) THEN
	DROP SYNCHRONIZATION SUBSCRIPTION TO "busdta" FOR "FBM109";
END IF
GO
IF EXISTS (
	SELECT 1
	FROM SYS.SYSSYNC
	WHERE subscription_name = 'busdta_FBM109'
) THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "busdta_FBM109";
END IF
GO

/*------------------------------------------------------------------------------
*                            Drop the user 'FBM109'.
*-----------------------------------------------------------------------------*/
IF EXISTS (
	SELECT 1
	FROM SYS.SYSSYNC
	WHERE site_name = 'FBM109'
) THEN
	DROP SYNCHRONIZATION USER "FBM109";
END IF
GO

/*------------------------------------------------------------------------------
*                        Drop the publication 'busdta'.
*-----------------------------------------------------------------------------*/
DROP PUBLICATION IF EXISTS "busdta"
GO

COMMIT
GO

IF NOT EXISTS (
	SELECT 1
	FROM SYS.SYSUSER
	WHERE user_name = 'BUSDTA'
) THEN
	GRANT CONNECT TO "BUSDTA";
END IF
GO



/*------------------------------------------------------------------------------
*             Create table 'MobileDataModel.BUSDTA.Device_Master'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."Device_Master"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."Device_Master"     (
	"Device_Id" varchar(30) not null,
	"Active" integer null,
	"manufacturer" nvarchar(50) null,
	"model" nvarchar(50) null,
	PRIMARY KEY ("Device_Id")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0004'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0004"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0004"     (
	"DTSY" nvarchar(4) not null,
	"DTRT" nvarchar(2) not null,
	"DTDL01" nvarchar(30) null,
	"DTCDL" float null,
	"DTLN2" nvarchar(1) null,
	"DTCNUM" nvarchar(1) null,
	PRIMARY KEY ("DTSY", "DTRT")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0005'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0005"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0005"     (
	"DRSY" nvarchar(4) not null,
	"DRRT" nvarchar(2) not null,
	"DRKY" nvarchar(10) not null,
	"DRDL01" nvarchar(30) null,
	"DRDL02" nvarchar(30) null,
	"DRSPHD" nvarchar(10) null,
	"DRHRDC" nvarchar(1) null,
	PRIMARY KEY ("DRSY", "DRRT", "DRKY")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0006'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0006"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0006"     (
	"MCMCU" nvarchar(12) not null,
	"MCSTYL" nvarchar(2) null,
	"MCLDM" nvarchar(1) null,
	"MCCO" nvarchar(5) null,
	"MCAN8" float null,
	"MCDL01" nvarchar(30) null,
	"MCRP01" nvarchar(3) null,
	"MCRP02" nvarchar(3) null,
	"MCRP03" nvarchar(3) null,
	"MCRP04" nvarchar(3) null,
	"MCRP05" nvarchar(3) null,
	"MCRP06" nvarchar(3) null,
	"MCRP07" nvarchar(3) null,
	"MCRP08" nvarchar(3) null,
	"MCRP09" nvarchar(3) null,
	"MCRP10" nvarchar(3) null,
	"MCRP11" nvarchar(3) null,
	"MCRP12" nvarchar(3) null,
	"MCRP13" nvarchar(3) null,
	"MCRP14" nvarchar(3) null,
	"MCRP15" nvarchar(3) null,
	"MCRP16" nvarchar(3) null,
	"MCRP17" nvarchar(3) null,
	"MCRP18" nvarchar(3) null,
	"MCRP19" nvarchar(3) null,
	"MCRP20" nvarchar(3) null,
	"MCRP21" nvarchar(10) null,
	"MCRP22" nvarchar(10) null,
	"MCRP23" nvarchar(10) null,
	"MCRP24" nvarchar(10) null,
	"MCRP25" nvarchar(10) null,
	"MCRP26" nvarchar(10) null,
	"MCRP27" nvarchar(10) null,
	"MCRP28" nvarchar(10) null,
	"MCRP29" nvarchar(10) null,
	"MCRP30" nvarchar(10) null,
	PRIMARY KEY ("MCMCU")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0014'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0014"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0014"     (
	"PNPTC" nvarchar(3) not null,
	"PNPTD" nvarchar(30) null,
	"PNDCP" float null,
	"PNDCD" float null,
	"PNNDTP" float null,
	"PNNSP" float null,
	"PNDTPA" float null,
	"PNPXDM" float null,
	"PNPXDD" float null,
	PRIMARY KEY ("PNPTC")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0101'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0101"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0101"     (
	"ABAN8" numeric(8, 0) not null,
	"ABALKY" nvarchar(20) null,
	"ABTAX" nvarchar(20) null,
	"ABALPH" nvarchar(40) null,
	"ABMCU" nvarchar(12) null,
	"ABSIC" nvarchar(10) null,
	"ABLNGP" nvarchar(2) null,
	"ABAT1" nvarchar(3) null,
	"ABCM" nvarchar(2) null,
	"ABTAXC" nvarchar(1) null,
	"ABAT2" nvarchar(1) null,
	"ABAN81" float null,
	"ABAN82" float null,
	"ABAN83" float null,
	"ABAN84" float null,
	"ABAN86" float null,
	"ABAN85" float null,
	"ABAC01" nvarchar(3) null,
	"ABAC02" nvarchar(3) null,
	"ABAC03" nvarchar(3) null,
	"ABAC04" nvarchar(3) null,
	"ABAC05" nvarchar(3) null,
	"ABAC06" nvarchar(3) null,
	"ABAC07" nvarchar(3) null,
	"ABAC08" nvarchar(3) null,
	"ABAC09" nvarchar(3) null,
	"ABAC10" nvarchar(3) null,
	"ABAC11" nvarchar(3) null,
	"ABAC12" nvarchar(3) null,
	"ABAC13" nvarchar(3) null,
	"ABAC14" nvarchar(3) null,
	"ABAC15" nvarchar(3) null,
	"ABAC16" nvarchar(3) null,
	"ABAC17" nvarchar(3) null,
	"ABAC18" nvarchar(3) null,
	"ABAC19" nvarchar(3) null,
	"ABAC20" nvarchar(3) null,
	"ABAC21" nvarchar(3) null,
	"ABAC22" nvarchar(3) null,
	"ABAC23" nvarchar(3) null,
	"ABAC24" nvarchar(3) null,
	"ABAC25" nvarchar(3) null,
	"ABAC26" nvarchar(3) null,
	"ABAC27" nvarchar(3) null,
	"ABAC28" nvarchar(3) null,
	"ABAC29" nvarchar(3) null,
	"ABAC30" nvarchar(3) null,
	"ABRMK" nvarchar(30) null,
	"ABTXCT" nvarchar(20) null,
	"ABTX2" nvarchar(20) null,
	"ABALP1" nvarchar(40) null,
	PRIMARY KEY ("ABAN8")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0115'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0115"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0115"     (
	"WPAN8" numeric(8, 0) not null,
	"WPIDLN" numeric(5, 0) not null,
	"WPRCK7" numeric(5, 0) not null,
	"WPCNLN" numeric(5, 0) not null,
	"WPPHTP" nvarchar(4) null,
	"WPAR1" nvarchar(6) null,
	"WPPH1" nvarchar(20) null,
	PRIMARY KEY ("WPAN8", "WPIDLN", "WPRCK7", "WPCNLN")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F01151'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F01151"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F01151"     (
	"EAAN8" numeric(8, 0) not null,
	"EAIDLN" numeric(5, 0) not null,
	"EARCK7" numeric(5, 0) not null,
	"EAETP" nvarchar(4) null,
	"EAEMAL" nvarchar(256) null,
	PRIMARY KEY ("EAAN8", "EAIDLN", "EARCK7")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0116'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0116"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0116"     (
	"ALAN8" numeric(8, 0) not null,
	"ALEFTB" numeric(18, 0) not null,
	"ALEFTF" nvarchar(1) null,
	"ALADD1" nvarchar(40) null,
	"ALADD2" nvarchar(40) null,
	"ALADD3" nvarchar(40) null,
	"ALADD4" nvarchar(40) null,
	"ALADDZ" nvarchar(12) null,
	"ALCTY1" nvarchar(25) null,
	"ALCOUN" nvarchar(25) null,
	"ALADDS" nvarchar(3) null,
	PRIMARY KEY ("ALAN8", "ALEFTB")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F0150'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F0150"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F0150"     (
	"MAOSTP" nvarchar(3) not null,
	"MAPA8" numeric(8, 0) not null,
	"MAAN8" numeric(8, 0) not null,
	"MABEFD" numeric(18, 0) null,
	"MAEEFD" numeric(18, 0) null,
	PRIMARY KEY ("MAOSTP", "MAPA8", "MAAN8")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F03012'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F03012"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F03012"     (
	"AIAN8" numeric(8, 0) not null,
	"AICO" nvarchar(5) not null,
	"AIMCUR" nvarchar(12) null,
	"AITXA1" nvarchar(10) null,
	"AIEXR1" nvarchar(2) null,
	"AIACL" float null,
	"AIHDAR" nvarchar(1) null,
	"AITRAR" nvarchar(3) null,
	"AISTTO" nvarchar(1) null,
	"AIRYIN" nvarchar(1) null,
	"AISTMT" nvarchar(1) null,
	"AIARPY" float null,
	"AISITO" nvarchar(1) null,
	"AICYCN" nvarchar(2) null,
	"AIBO" nvarchar(1) null,
	"AITSTA" nvarchar(2) null,
	"AICKHC" nvarchar(1) null,
	"AIDLC" numeric(18, 0) null,
	"AIDNLT" nvarchar(1) null,
	"AIPLCR" nvarchar(10) null,
	"AIRVDJ" numeric(18, 0) null,
	"AIDSO" float null,
	"AICMGR" nvarchar(10) null,
	"AICLMG" nvarchar(10) null,
	"AIAB2" nvarchar(1) null,
	"AIDT1J" numeric(18, 0) null,
	"AIDFIJ" numeric(18, 0) null,
	"AIDLIJ" numeric(18, 0) null,
	"AIDLP" numeric(18, 0) null,
	"AIASTY" float null,
	"AISPYE" float null,
	"AIAHB" float null,
	"AIALP" float null,
	"AIABAM" float null,
	"AIABA1" float null,
	"AIAPRC" float null,
	"AIMAXO" float null,
	"AIMINO" float null,
	"AIOYTD" float null,
	"AIOPY" float null,
	"AIPOPN" nvarchar(10) null,
	"AIDAOJ" numeric(18, 0) null,
	"AIAN8R" float null,
	"AIBADT" nvarchar(1) null,
	"AICPGP" nvarchar(8) null,
	"AIORTP" nvarchar(8) null,
	"AITRDC" float null,
	"AIINMG" nvarchar(10) null,
	"AIEXHD" nvarchar(1) null,
	"AIHOLD" nvarchar(2) null,
	"AIROUT" nvarchar(3) null,
	"AISTOP" nvarchar(3) null,
	"AIZON" nvarchar(3) null,
	"AICARS" float null,
	"AIDEL1" nvarchar(30) null,
	"AIDEL2" nvarchar(30) null,
	"AILTDT" float null,
	"AIFRTH" nvarchar(3) null,
	"AIAFT" nvarchar(1) null,
	"AIAPTS" nvarchar(1) null,
	"AISBAL" nvarchar(1) null,
	"AIBACK" nvarchar(1) null,
	"AIPORQ" nvarchar(1) null,
	"AIPRIO" nvarchar(1) null,
	"AIARTO" nvarchar(1) null,
	"AIINVC" float null,
	"AIICON" nvarchar(1) null,
	"AIBLFR" nvarchar(1) null,
	"AINIVD" numeric(18, 0) null,
	"AILEDJ" numeric(18, 0) null,
	"AIPLST" nvarchar(1) null,
	"AIEDF1" nvarchar(1) null,
	"AIEDF2" nvarchar(1) null,
	"AIASN" nvarchar(8) null,
	"AIDSPA" nvarchar(1) null,
	"AICRMD" nvarchar(1) null,
	"AIAMCR" float null,
	"AIAC01" nvarchar(3) null,
	"AIAC02" nvarchar(3) null,
	"AIAC03" nvarchar(3) null,
	"AIAC04" nvarchar(3) null,
	"AIAC05" nvarchar(3) null,
	"AIAC06" nvarchar(3) null,
	"AIAC07" nvarchar(3) null,
	"AIAC08" nvarchar(3) null,
	"AIAC09" nvarchar(3) null,
	"AIAC10" nvarchar(3) null,
	"AIAC11" nvarchar(3) null,
	"AIAC12" nvarchar(3) null,
	"AIAC13" nvarchar(3) null,
	"AIAC14" nvarchar(3) null,
	"AIAC15" nvarchar(3) null,
	"AIAC16" nvarchar(3) null,
	"AIAC17" nvarchar(3) null,
	"AIAC18" nvarchar(3) null,
	"AIAC19" nvarchar(3) null,
	"AIAC20" nvarchar(3) null,
	"AIAC21" nvarchar(3) null,
	"AIAC22" nvarchar(3) null,
	"AIAC23" nvarchar(3) null,
	"AIAC24" nvarchar(3) null,
	"AIAC25" nvarchar(3) null,
	"AIAC26" nvarchar(3) null,
	"AIAC27" nvarchar(3) null,
	"AIAC28" nvarchar(3) null,
	"AIAC29" nvarchar(3) null,
	"AIAC30" nvarchar(3) null,
	"AIPRSN" nvarchar(8) null,
	"AIOPBO" nvarchar(30) null,
	"AITIER1" nvarchar(5) null,
	"AIPWPCP" float null,
	"AICUSTS" nvarchar(1) null,
	"AISTOF" nvarchar(1) null,
	"AITERRID" float null,
	"AICIG" float null,
	"AITORG" nvarchar(10) null,
	PRIMARY KEY ("AIAN8", "AICO")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F40073'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F40073"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F40073"     (
	"HYPRFR" nvarchar(2) not null,
	"HYHYID" nvarchar(10) not null,
	"HYHY01" float null,
	"HYHY02" float null,
	"HYHY03" float null,
	"HYHY04" float null,
	"HYHY05" float null,
	"HYHY06" float null,
	"HYHY07" float null,
	"HYHY08" float null,
	"HYHY09" float null,
	"HYHY10" float null,
	"HYHY11" float null,
	"HYHY12" float null,
	"HYHY13" float null,
	"HYHY14" float null,
	"HYHY15" float null,
	"HYHY16" float null,
	"HYHY17" float null,
	"HYHY18" float null,
	"HYHY19" float null,
	"HYHY20" float null,
	"HYHY21" float null,
	PRIMARY KEY ("HYPRFR", "HYHYID")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4015'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4015"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4015"     (
	"OTORTP" nvarchar(8) not null,
	"OTAN8" numeric(8, 0) not null,
	"OTOSEQ" numeric(4, 0) not null,
	"OTITM" float null,
	"OTLITM" nvarchar(25) null,
	--"OTAITM" nvarchar(25) null,
	"OTQTYU" float null,
	"OTUOM" nvarchar(2) null,
	"OTLNTY" nvarchar(2) null,
	"OTEFTJ" numeric(18, 0) null,
	"OTEXDJ" numeric(18, 0) null,
	PRIMARY KEY ("OTORTP", "OTAN8", "OTOSEQ")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4070'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4070"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4070"     (
	"SNASN" nvarchar(8) not null,
	"SNOSEQ" numeric(4, 0) not null,
	"SNANPS" numeric(8, 0) not null,
	"SNAST" nvarchar(8) null,
	"SNEFTJ" numeric(18, 0) null,
	"SNEXDJ" numeric(18, 0) null,
	PRIMARY KEY ("SNASN", "SNOSEQ", "SNANPS")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4071'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4071"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4071"     (
	"ATAST" nvarchar(8) not null,
	"ATPRGR" nvarchar(8) null,
	"ATCPGP" nvarchar(8) null,
	"ATSDGR" nvarchar(8) null,
	"ATPRFR" nvarchar(2) null,
	"ATLBT" nvarchar(1) null,
	"ATGLC" nvarchar(4) null,
	"ATSBIF" nvarchar(1) null,
	"ATACNT" nvarchar(1) null,
	"ATLNTY" nvarchar(2) null,
	"ATMDED" nvarchar(1) null,
	"ATABAS" nvarchar(1) null,
	"ATOLVL" nvarchar(1) null,
	"ATTXB" nvarchar(1) null,
	"ATPA01" nvarchar(1) null,
	"ATPA02" nvarchar(1) null,
	"ATPA03" nvarchar(1) null,
	"ATPA04" nvarchar(1) null,
	"ATPA05" nvarchar(1) null,
	"ATENBM" nvarchar(1) null,
	"ATSRFLAG" nvarchar(1) null,
	"ATUSADJ" nvarchar(1) null,
	"ATATIER" float null,
	"ATBTIER" float null,
	"ATBNAD" float null,
	"ATAPRP1" nvarchar(3) null,
	"ATAPRP2" nvarchar(3) null,
	"ATAPRP3" nvarchar(3) null,
	"ATAPRP4" nvarchar(6) null,
	"ATAPRP5" nvarchar(6) null,
	"ATAPRP6" nvarchar(6) null,
	"ATADJGRP" nvarchar(10) null,
	"ATMEADJ" nvarchar(1) null,
	"ATPDCL" nvarchar(1) null,
	"ATUSER" nvarchar(10) null,
	"ATPID" nvarchar(10) null,
	"ATJOBN" nvarchar(10) null,
	"ATUPMJ" numeric(18, 0) null,
	"ATTDAY" float null,
	"ATDIDP" nvarchar(12) null,
	"ATPMTN" nvarchar(12) null,
	"ATPHST" nvarchar(1) null,
	"ATPA06" nvarchar(1) null,
	"ATPA07" nvarchar(1) null,
	"ATPA08" nvarchar(1) null,
	"ATPA09" nvarchar(1) null,
	"ATPA10" nvarchar(1) null,
	"ATEFCN" nvarchar(1) null,
	"ATAPTYPE" nvarchar(2) null,
	"ATMOADJ" nvarchar(1) null,
	"ATPLGRP" nvarchar(3) null,
	"ATEXCPL" nvarchar(1) null,
	"ATUPMX" numeric(18, 0) null,
	"ATMNMXAJ" nvarchar(1) null,
	"ATMNMXRL" nvarchar(1) null,
	"ATTSTRSNM" nvarchar(30) null,
	"ATADJQTY" nvarchar(1) null,
	PRIMARY KEY ("ATAST")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4072'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4072"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4072"     (
	"ADAST" nvarchar(8) not null,
	"ADITM" numeric(8, 0) not null,
	"ADLITM" nvarchar(25) null,
	"ADAITM" nvarchar(25) null,
	"ADAN8" numeric(8, 0) not null,
	"ADIGID" numeric(8, 0) not null,
	"ADCGID" numeric(8, 0) not null,
	"ADOGID" numeric(8, 0) not null,
	"ADCRCD" nvarchar(3) not null,
	"ADUOM" nvarchar(2) not null,
	"ADMNQ" numeric(15, 0) not null,
	"ADEFTJ" numeric(18, 0) null,
	"ADEXDJ" numeric(18, 0) not null,
	"ADBSCD" nvarchar(1) null,
	"ADLEDG" nvarchar(2) null,
	"ADFRMN" nvarchar(10) null,
	"ADFVTR" float null,
	"ADFGY" nvarchar(1) null,
	"ADATID" float null,
	"ADNBRORD" float null,
	"ADUOMVID" nvarchar(2) null,
	"ADFVUM" nvarchar(2) null,
	"ADPARTFG" nvarchar(1) null,
	"ADAPRS" nvarchar(1) null,
	"ADUPMJ" numeric(18, 0) not null,
	"ADTDAY" numeric(6, 0) not null,
	"ADBKTPID" float null,
	"ADCRCDVID" nvarchar(3) null,
	"ADRULENAME" nvarchar(10) null,
	PRIMARY KEY ("ADAST", "ADITM", "ADAN8", "ADIGID", "ADCGID", "ADOGID", "ADCRCD", "ADUOM", "ADMNQ", "ADEXDJ", "ADUPMJ", "ADTDAY")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4075'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4075"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4075"     (
	"VBVBT" nvarchar(10) not null,
	"VBCRCD" nvarchar(3) null,
	"VBUOM" nvarchar(2) null,
	"VBUPRC" float null,
	"VBEFTJ" numeric(18, 0) not null,
	"VBEXDJ" numeric(18, 0) null,
	"VBAPRS" nvarchar(1) null,
	"VBUPMJ" numeric(18, 0) not null,
	"VBTDAY" numeric(6, 0) not null,
	PRIMARY KEY ("VBVBT", "VBEFTJ", "VBUPMJ", "VBTDAY")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4076'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4076"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4076"     (
	"FMFRMN" nvarchar(10) not null,
	"FMFML" nvarchar(160) null,
	"FMAPRS" nvarchar(1) null,
	"FMUPMJ" numeric(18, 0) not null,
	"FMTDAY" numeric(6, 0) not null,
	PRIMARY KEY ("FMFRMN", "FMUPMJ", "FMTDAY")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4092'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4092"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4092"     (
	"GPGPTY" nvarchar(1) not null,
	"GPGPC" nvarchar(8) not null,
	"GPDL01" nvarchar(30) null,
	"GPGPK1" nvarchar(10) null,
	"GPGPK2" nvarchar(10) null,
	"GPGPK3" nvarchar(10) null,
	"GPGPK4" nvarchar(10) null,
	"GPGPK5" nvarchar(10) null,
	"GPGPK6" nvarchar(10) null,
	"GPGPK7" nvarchar(10) null,
	"GPGPK8" nvarchar(10) null,
	"GPGPK9" nvarchar(10) null,
	"GPGPK10" nvarchar(10) null,
	PRIMARY KEY ("GPGPTY", "GPGPC")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F40941'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F40941"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F40941"     (
	"IKPRGR" nvarchar(8) not null,
	"IKIGP1" nvarchar(6) not null,
	"IKIGP2" nvarchar(6) not null,
	"IKIGP3" nvarchar(6) not null,
	"IKIGP4" nvarchar(6) not null,
	"IKIGP5" nvarchar(6) not null,
	"IKIGP6" nvarchar(6) not null,
	"IKIGP7" nvarchar(6) not null,
	"IKIGP8" nvarchar(6) not null,
	"IKIGP9" nvarchar(6) not null,
	"IKIGP10" nvarchar(6) not null,
	"IKIGID" float null,
	PRIMARY KEY ("IKPRGR", "IKIGP1", "IKIGP2", "IKIGP3", "IKIGP4", "IKIGP5", "IKIGP6", "IKIGP7", "IKIGP8", "IKIGP9", "IKIGP10")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F40942'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F40942"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F40942"     (
	"CKCPGP" nvarchar(8) not null,
	"CKCGP1" nvarchar(3) not null,
	"CKCGP2" nvarchar(3) not null,
	"CKCGP3" nvarchar(3) not null,
	"CKCGP4" nvarchar(3) not null,
	"CKCGP5" nvarchar(3) not null,
	"CKCGP6" nvarchar(3) not null,
	"CKCGP7" nvarchar(3) not null,
	"CKCGP8" nvarchar(3) not null,
	"CKCGP9" nvarchar(3) not null,
	"CKCGP10" nvarchar(3) not null,
	"CKCGID" float null,
	PRIMARY KEY ("CKCPGP", "CKCGP1", "CKCGP2", "CKCGP3", "CKCGP4", "CKCGP5", "CKCGP6", "CKCGP7", "CKCGP8", "CKCGP9", "CKCGP10")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F41002'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F41002"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F41002"     (
	"UMMCU" nvarchar(12) not null,
	"UMITM" numeric(8, 0) not null,
	"UMUM" nvarchar(2) not null,
	"UMRUM" nvarchar(2) not null,
	"UMUSTR" nvarchar(1) null,
	"UMCONV" float null,
	"UMCNV1" float null,
	PRIMARY KEY ("UMMCU", "UMITM", "UMUM", "UMRUM")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4101'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4101"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4101"     (
	"IMITM" numeric(8, 0) not null,
	"IMLITM" nvarchar(25) null,
	"IMAITM" nvarchar(25) null,
	"IMDSC1" nvarchar(30) null,
	"IMDSC2" nvarchar(30) null,
	"IMSRP1" nvarchar(3) null,
	"IMSRP2" nvarchar(3) null,
	"IMSRP3" nvarchar(3) null,
	"IMSRP4" nvarchar(3) null,
	"IMSRP5" nvarchar(3) null,
	"IMSRP6" nvarchar(6) null,
	"IMSRP7" nvarchar(6) null,
	"IMSRP8" nvarchar(6) null,
	"IMSRP9" nvarchar(6) null,
	"IMSRP0" nvarchar(6) null,
	"IMPRP1" nvarchar(3) null,
	"IMPRP2" nvarchar(3) null,
	"IMPRP3" nvarchar(3) null,
	"IMPRP4" nvarchar(3) null,
	"IMPRP5" nvarchar(3) null,
	"IMPRP6" nvarchar(6) null,
	"IMPRP7" nvarchar(6) null,
	"IMPRP8" nvarchar(6) null,
	"IMPRP9" nvarchar(6) null,
	"IMPRP0" nvarchar(6) null,
	"IMCDCD" nvarchar(15) null,
	"IMPDGR" nvarchar(3) null,
	"IMDSGP" nvarchar(3) null,
	"IMPRGR" nvarchar(8) null,
	"IMRPRC" nvarchar(8) null,
	"IMORPR" nvarchar(8) null,
	"IMVCUD" float null,
	"IMUOM1" nvarchar(2) null,
	"IMUOM2" nvarchar(2) null,
--	"IMUOM3" nvarchar(2) null,
	"IMUOM4" nvarchar(2) null,
	"IMUOM6" nvarchar(2) null,
	"IMUWUM" nvarchar(2) null,
	"IMUVM1" nvarchar(2) null,
	"IMCYCL" nvarchar(3) null,
	"IMGLPT" nvarchar(4) null,
	"IMPLEV" nvarchar(1) null,
	"IMPPLV" nvarchar(1) null,
	"IMCLEV" nvarchar(1) null,
	"IMCKAV" nvarchar(1) null,
	"IMSRCE" nvarchar(1) null,
	"IMSTKT" nvarchar(1) null,
	"IMLNTY" nvarchar(2) null,
	"IMBACK" nvarchar(1) null,
	"IMIFLA" nvarchar(2) null,
	"IMTFLA" nvarchar(2) null,
	"IMINMG" nvarchar(10) null,
	"IMABCS" nvarchar(1) null,
	"IMABCM" nvarchar(1) null,
	"IMABCI" nvarchar(1) null,
	"IMOVR" nvarchar(1) null,
	"IMCMCG" nvarchar(8) null,
	"IMSRNR" nvarchar(1) null,
	"IMFIFO" nvarchar(1) null,
	"IMLOTS" nvarchar(1) null,
	"IMSLD" float null,
	"IMPCTM" float null,
	"IMMMPC" float null,
	"IMCMGL" nvarchar(1) null,
	"IMUPCN" nvarchar(13) null,
	"IMUMUP" nvarchar(2) null,
	"IMUMDF" nvarchar(2) null,
	"IMBBDD" float null,
	"IMCMDM" nvarchar(1) null,
	"IMLECM" nvarchar(1) null,
	"IMLEDD" float null,
	"IMPEFD" float null,
	"IMSBDD" float null,
	"IMU1DD" float null,
	"IMU2DD" float null,
	"IMU3DD" float null,
	"IMU4DD" float null,
	"IMU5DD" float null,
	"IMLNPA" nvarchar(1) null,
	"IMLOTC" nvarchar(3) null,
	PRIMARY KEY ("IMITM")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4102'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4102"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4102"     (
	"IBITM" numeric(8, 0) not null,
	"IBLITM" nvarchar(25) null,
	"IBAITM" nvarchar(25) null,
	"IBMCU" nvarchar(12) not null,
	"IBSRP1" nvarchar(3) null,
	"IBSRP2" nvarchar(3) null,
	"IBSRP3" nvarchar(3) null,
	"IBSRP4" nvarchar(3) null,
	"IBSRP5" nvarchar(3) null,
	"IBSRP6" nvarchar(6) null,
	"IBSRP7" nvarchar(6) null,
	"IBSRP8" nvarchar(6) null,
	"IBSRP9" nvarchar(6) null,
	"IBSRP0" nvarchar(6) null,
	"IBPRP1" nvarchar(3) null,
	"IBPRP2" nvarchar(3) null,
	"IBPRP3" nvarchar(3) null,
	"IBPRP4" nvarchar(3) null,
	"IBPRP5" nvarchar(3) null,
	"IBPRP6" nvarchar(6) null,
	"IBPRP7" nvarchar(6) null,
	"IBPRP8" nvarchar(6) null,
	"IBPRP9" nvarchar(6) null,
	"IBPRP0" nvarchar(6) null,
	"IBCDCD" nvarchar(15) null,
	"IBPDGR" nvarchar(3) null,
	"IBDSGP" nvarchar(3) null,
	"IBGLPT" nvarchar(4) null,
	"IBORIG" nvarchar(3) null,
	"IBSAFE" float null,
	"IBSLD" float null,
	"IBCKAV" nvarchar(1) null,
	"IBSRCE" nvarchar(1) null,
	"IBLOTS" nvarchar(1) null,
	"IBMMPC" float null,
	"IBPRGR" nvarchar(8) null,
	"IBRPRC" nvarchar(8) null,
	"IBORPR" nvarchar(8) null,
	"IBBACK" nvarchar(1) null,
	"IBIFLA" nvarchar(2) null,
	"IBABCS" nvarchar(1) null,
	"IBABCM" nvarchar(1) null,
	"IBABCI" nvarchar(1) null,
	"IBOVR" nvarchar(1) null,
	"IBSTKT" nvarchar(1) null,
	"IBLNTY" nvarchar(2) null,
	"IBFIFO" nvarchar(1) null,
	"IBCYCL" nvarchar(3) null,
	"IBINMG" nvarchar(10) null,
	"IBSRNR" nvarchar(1) null,
	"IBPCTM" float null,
	"IBCMCG" nvarchar(8) null,
	"IBTAX1" nvarchar(1) null,
	"IBBBDD" float null,
	"IBCMDM" nvarchar(1) null,
	"IBLECM" nvarchar(1) null,
	"IBLEDD" float null,
	"IBMLOT" nvarchar(1) null,
	"IBSBDD" float null,
	"IBU1DD" float null,
	"IBU2DD" float null,
	"IBU3DD" float null,
	"IBU4DD" float null,
	"IBU5DD" float null,
	PRIMARY KEY ("IBITM", "IBMCU")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F4106'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F4106"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F4106"     (
	"BPITM" numeric(8, 0) not null,
	"BPLITM" nvarchar(25) null,
	"BPMCU" nvarchar(12) not null,
	"BPLOCN" nvarchar(20) not null,
	"BPLOTN" nvarchar(30) not null,
	"BPAN8" numeric(8, 0) not null,
	"BPIGID" numeric(8, 0) not null,
	"BPCGID" numeric(8, 0) not null,
	"BPLOTG" nvarchar(3) not null,
	"BPFRMP" numeric(7, 0) not null,
	"BPCRCD" nvarchar(3) not null,
	"BPUOM" nvarchar(2) not null,
	"BPEFTJ" numeric(18, 0) null,
	"BPEXDJ" numeric(18, 0) not null,
	"BPUPRC" float null,
	"BPUPMJ" numeric(18, 0) not null,
	"BPTDAY" float not null,
	PRIMARY KEY ("BPITM", "BPMCU", "BPLOCN", "BPLOTN", "BPAN8", "BPIGID", "BPCGID", "BPLOTG", "BPFRMP", "BPCRCD", "BPUOM", "BPEXDJ", "BPUPMJ", "BPTDAY")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F42019'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F42019"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F42019"     (
	"SHKCOO" nvarchar(5) not null,
	"SHDOCO" numeric(8, 0) not null,
	"SHDCTO" nvarchar(2) not null,
	"SHMCU" nvarchar(12) null,
	--"SHOKCO" nvarchar(5) null,
	--"SHOORN" nvarchar(8) null,
	--"SHOCTO" nvarchar(2) null,
	"SHAN8" float null,
	"SHSHAN" float null,
	"SHPA8" float null,
	"SHDRQJ" numeric(18, 0) null,
	"SHTRDJ" numeric(18, 0) null,
	"SHPDDJ" numeric(18, 0) null,
	"SHADDJ" numeric(18, 0) null,
	"SHCNDJ" numeric(18, 0) null,
	"SHPEFJ" numeric(18, 0) null,
	"SHVR01" nvarchar(25) null,
	"SHVR02" nvarchar(25) null,
	"SHDEL1" nvarchar(30) null,
	"SHDEL2" nvarchar(30) null,
	"SHINMG" nvarchar(10) null,
	"SHPTC" nvarchar(3) null,
	"SHRYIN" nvarchar(1) null,
	"SHASN" nvarchar(8) null,
	"SHPRGP" nvarchar(8) null,
	"SHTXA1" nvarchar(10) null,
	"SHEXR1" nvarchar(2) null,
	"SHTXCT" nvarchar(20) null,
	"SHATXT" nvarchar(1) null,
	"SHHOLD" nvarchar(2) null,
	"SHROUT" nvarchar(3) null,
	"SHSTOP" nvarchar(3) null,
	"SHZON" nvarchar(3) null,
	"SHFRTH" nvarchar(3) null,
	"SHRCD" nvarchar(3) null,
	"SHFUF2" nvarchar(1) null,
	"SHOTOT" float null,
	"SHAUTN" nvarchar(10) null,
	"SHCACT" nvarchar(25) null,
	"SHCEXP" numeric(18, 0) null,
	"SHORBY" nvarchar(10) null,
	"SHTKBY" nvarchar(10) null,
	"SHDOC1" float null,
	"SHDCT4" nvarchar(2) null,
	PRIMARY KEY ("SHKCOO", "SHDOCO", "SHDCTO")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.F42119'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F42119"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F42119"     (
	"SDKCOO" nvarchar(5) not null,
	"SDDOCO" numeric(8, 0) not null,
	"SDDCTO" nvarchar(2) not null,
	"SDLNID" numeric(7, 0) not null,
	"SDMCU" nvarchar(12) null,
	"SDRKCO" nvarchar(5) null,
	"SDRORN" nvarchar(8) null,
	"SDRCTO" nvarchar(2) null,
	"SDRLLN" float null,
	"SDAN8" float null,
	"SDSHAN" float null,
	"SDPA8" float null,
	"SDDRQJ" numeric(18, 0) null,
	"SDTRDJ" numeric(18, 0) null,
	"SDPDDJ" numeric(18, 0) null,
	"SDADDJ" numeric(18, 0) null,
	"SDIVD" numeric(18, 0) null,
	"SDCNDJ" numeric(18, 0) null,
	"SDDGL" numeric(18, 0) null,
	"SDPEFJ" numeric(18, 0) null,
	"SDVR01" nvarchar(25) null,
	"SDVR02" nvarchar(25) null,
	"SDITM" float null,
	"SDLITM" nvarchar(25) null,
	"SDAITM" nvarchar(25) null,
	"SDLOCN" nvarchar(20) null,
	"SDLOTN" nvarchar(30) null,
	"SDDSC1" nvarchar(30) null,
	"SDDSC2" nvarchar(30) null,
	"SDLNTY" nvarchar(2) null,
	"SDNXTR" nvarchar(3) null,
	"SDEMCU" nvarchar(12) null,
	"SDSRP1" nvarchar(3) null,
	"SDSRP2" nvarchar(3) null,
	"SDSRP3" nvarchar(3) null,
	"SDSRP4" nvarchar(3) null,
	"SDSRP5" nvarchar(3) null,
	"SDPRP1" nvarchar(3) null,
	"SDPRP2" nvarchar(3) null,
	"SDPRP3" nvarchar(3) null,
	"SDPRP4" nvarchar(3) null,
	"SDPRP5" nvarchar(3) null,
	"SDUOM" nvarchar(2) null,
	"SDUORG" float null,
	"SDSOQS" float null,
	"SDSOBK" float null,
	"SDSOCN" float null,
	"SDUPRC" float null,
	"SDAEXP" float null,
	"SDPROV" nvarchar(1) null,
	"SDINMG" nvarchar(10) null,
	"SDPTC" nvarchar(3) null,
	"SDASN" nvarchar(8) null,
	"SDPRGR" nvarchar(8) null,
	"SDCLVL" nvarchar(3) null,
	"SDKCO" nvarchar(5) null,
	"SDDOC" float null,
	"SDDCT" nvarchar(2) null,
	"SDTAX1" nvarchar(1) null,
	"SDTXA1" nvarchar(10) null,
	"SDEXR1" nvarchar(2) null,
	"SDATXT" nvarchar(1) null,
	"SDROUT" nvarchar(3) null,
	"SDSTOP" nvarchar(3) null,
	"SDZON" nvarchar(3) null,
	"SDFRTH" nvarchar(3) null,
	"SDUOM1" nvarchar(2) null,
	"SDPQOR" float null,
	"SDUOM2" nvarchar(2) null,
	"SDSQOR" float null,
	"SDUOM4" nvarchar(2) null,
	"SDRPRC" nvarchar(8) null,
	"SDORPR" nvarchar(8) null,
	"SDORP" nvarchar(1) null,
	"SDGLC" nvarchar(4) null,
	"SDCTRY" float null,
	"SDFY" float null,
	"SDACOM" nvarchar(1) null,
	"SDCMCG" nvarchar(8) null,
	"SDRCD" nvarchar(3) null,
	"SDUPC1" nvarchar(2) null,
	"SDUPC2" nvarchar(2) null,
	"SDUPC3" nvarchar(2) null,
	"SDTORG" nvarchar(10) null,
	"SDVR03" nvarchar(25) null,
	"SDNUMB" float null,
	"SDAAID" float null,
	PRIMARY KEY ("SDKCOO", "SDDOCO", "SDDCTO", "SDLNID")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.F56M0000'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F56M0000"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F56M0000"     (
	"GFSY" nvarchar(4) not null,
	"GFCXPJ" numeric(18, 0) null,
	"GFDTEN" numeric(18, 0) null,
	"GFLAVJ" numeric(18, 0) null,
	"GFOBJ" nvarchar(6) null,
	"GFMCU" nvarchar(12) null,
	"GFSUB" nvarchar(8) null,
	"GFPST" nvarchar(1) null,
	"GFEV01" nvarchar(1) null,
	"GFEV02" nvarchar(1) null,
	"GFEV03" nvarchar(1) null,
	"GFMATH01" float null,
	"GFMATH02" float null,
	"GFMATH03" float null,
	"GFCFSTR1" nvarchar(3) null,
	"GFCFSTR2" nvarchar(8) null,
	"GFGS1A" nvarchar(10) null,
	"GFGS1B" nvarchar(10) null,
	"GFGS2A" nvarchar(20) null,
	"GFGS2B" nvarchar(20) null,
	PRIMARY KEY ("GFSY")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.F56M0001'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F56M0001"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F56M0001"     (
	"FFUSER" nvarchar(10) not null,
	"FFROUT" nvarchar(3) not null,
	"FFMCU" nvarchar(12) not null,
	"FFHMCU" nvarchar(12) null,
	"FFBUVAL" nvarchar(12) null,
	"FFAN8" float null,
	"FFPA8" float null,
	"FFSTOP" nvarchar(3) null,
	"FFZON" nvarchar(3) null,
	"FFLOCN" nvarchar(20) null,
	"FFLOCF" nvarchar(20) null,
	"FFEV01" nvarchar(1) null,
	"FFEV02" nvarchar(1) null,
	"FFEV03" nvarchar(1) null,
	"FFMATH01" float null,
	"FFMATH02" float null,
	"FFMATH03" float null,
	"FFCXPJ" numeric(18, 0) null,
	"FFCLRJ" numeric(18, 0) null,
	"FFDTE" numeric(18, 0) null,
	PRIMARY KEY ("FFUSER", "FFROUT", "FFMCU")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.M56M0002'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M56M0002"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M56M0002"     (
	"DCDDC" nvarchar(3) not null,
	"DCDDCD" nvarchar(50) null,
	"DCPN" numeric(4, 0) not null,
	"DCWN" numeric(4, 0) not null,
	"DCDN" numeric(4, 0) not null,
	"DCISST" numeric(4, 0) null default ((1)),
	"DCCRBY" nvarchar(50) null,
	"DCCRDT" date null,
	"DCUPBY" nvarchar(50) null,
	"DCUPDT" date null,
	PRIMARY KEY ("DCDDC", "DCPN", "DCWN", "DCDN")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.M56M0003'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M56M0003"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M56M0003"     (
	"RSROUT" nvarchar(10) not null,
	"RSAN8" numeric(8, 0) not null,
	"RSWN" numeric(4, 0) not null,
	"RSDN" numeric(4, 0) not null,
	"RSSN" numeric(4, 0) null,
	"RSCRBY" nvarchar(50) null,
	"RSCRDT" date null,
	"RSUPBY" nvarchar(50) null,
	"RSUPDT" date null,
	PRIMARY KEY ("RSROUT", "RSAN8", "RSWN", "RSDN")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.M56M0004'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M56M0004"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M56M0004"     (
	"RPSTID" numeric(8, 0)  not null  DEFAULT autoincrement,
	"RPROUT" nvarchar(10) not null,
	"RPAN8" numeric(8, 0) not null,
	"RPSTDT" date not null,
	"RPOGDT" date null,
	"RPSN" numeric(4, 0) null,
	"RPVTTP" nvarchar(10) null,
	"RPSTTP" nvarchar(15) null,
	"RPRSTID" numeric(8, 0) null,
	"RPCRBY" nvarchar(50) null,
	"RPCRDT" date null,
	"RPUPBY" nvarchar(50) null,
	"RPUPDT" date null,
	PRIMARY KEY ("RPSTID", "RPROUT", "RPAN8", "RPSTDT")
)
GO

COMMIT
GO

/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.F90CA003'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F90CA003"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F90CA003"     (
	"SMAN8" float not null,
	"SMSLSM" float not null,
	PRIMARY KEY ("SMAN8", "SMSLSM")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.F90CA042'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F90CA042"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F90CA042"     (
	"EMAN8" numeric(8, 0) not null,
	"EMPA8" numeric(8, 0) not null,
	--"EMEMH" float null,
	--"EMEDATE" datetime null,
	--"EMUSER" nvarchar(10) null,
	--"EMUDTTM" datetime null,
	--"EMMKEY" nvarchar(15) null,
	--"EMENTDBY" float null,
	PRIMARY KEY ("EMAN8", "EMPA8")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.F90CA086'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."F90CA086"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."F90CA086"     (
	"CRCUAN8" numeric(8, 0) not null,
	"CRCRAN8" numeric(8, 0) not null,
	PRIMARY KEY ("CRCUAN8", "CRCRAN8")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.M0111'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M0111"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M0111"     (
	"CDAN8" numeric(8, 0) not null,
	"CDIDLN" numeric(5, 0) not null,
	"CDRCK7" numeric(5, 0) not null,
	"CDCNLN" numeric(5, 0) not null,
	"CDAR1" nvarchar(6) null,
	"CDPH1" nvarchar(20) null,
	"CDEXTN1" nvarchar(8) null,
	"CDPHTP1" numeric(8, 0) null,
	"CDDFLTPH1" bit null default ((1)),
	"CDREFPH1" integer null,
	"CDAR2" nvarchar(6) null,
	"CDPH2" nvarchar(20) null,
	"CDEXTN2" nvarchar(8) null,
	"CDPHTP2" numeric(8, 0) null,
	"CDDFLTPH2" bit null default ((0)),
	"CDREFPH2" integer null,
	"CDAR3" nvarchar(6) null,
	"CDPH3" nvarchar(20) null,
	"CDEXTN3" nvarchar(8) null,
	"CDPHTP3" numeric(8, 0) null,
	"CDDFLTPH3" bit null default ((0)),
	"CDREFPH3" integer null,
	"CDAR4" nvarchar(6) null,
	"CDPH4" nvarchar(20) null,
	"CDEXTN4" nvarchar(8) null,
	"CDPHTP4" numeric(8, 0) null,
	"CDDFLTPH4" bit null default ((0)),
	"CDREFPH4" integer null,
	"CDEMAL1" nvarchar(256) null,
	"CDETP1" numeric(3, 0) null,
	"CDDFLTEM1" bit null default ((1)),
	"CDREFEM1" integer null,
	"CDEMAL2" nvarchar(256) null,
	"CDETP2" numeric(3, 0) null,
	"CDDFLTEM2" bit null default ((0)),
	"CDREFEM2" integer null,
	"CDEMAL3" nvarchar(256) null,
	"CDETP3" numeric(3, 0) null,
	"CDDFLTEM3" bit null default ((0)),
	"CDREFEM3" integer null,
	"CDGNNM" nvarchar(75) null,
	"CDMDNM" nvarchar(25) null,
	"CDSRNM" nvarchar(25) null,
	"CDTITL" nvarchar(50) null,
	"CDID" numeric(8, 0) not null,
	"CDACTV" bit null,
	"CDDFLT" bit null default ((0)),
	"CDSET" integer null,
	"CDCRBY" nvarchar(25) null,
	"CDCRDT" date null,
	"CDUPBY" nvarchar(25) null,
	"CDUPDT" date null,
	PRIMARY KEY ("CDAN8", "CDIDLN", "CDRCK7", "CDCNLN")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.M0112'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M0112"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M0112"     (
	"NDAN8" numeric(8, 0) not null,
	"NDID" numeric(8, 0)  not null  DEFAULT autoincrement,
	"NDDTTM" datetime null,
	"NDDTLS" nvarchar(500) null,
	"NDTYP" nvarchar(15) null,
	"NDDFLT" bit null,
	"NDCRBY" nvarchar(50) null,
	"NDCRDT" date null,
	"NDUPBY" nvarchar(50) null,
	"NDUPDT" date null,
	PRIMARY KEY ("NDAN8", "NDID")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.M03011'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M03011"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M03011"     (
	"CSAN8" numeric(8, 0) not null,
	"CSCO" nvarchar(5) not null,
	"CSUAMT" numeric(8, 4) null,
	"CSOBAL" numeric(8, 4) null,
	"CSCRBY" nvarchar(10) null,
	"CSCRDT" date null,
	"CSUPBY" nvarchar(10) null,
	"CSUPDT" date null,
	PRIMARY KEY ("CSAN8", "CSCO")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.M03042'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M03042"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M03042"     (
	"PDAN8" numeric(8, 0) not null,
	"PDCO" nvarchar(5) not null,
	"PDID" numeric(8, 0) not null,
	"PDPAMT" numeric(8, 4) null,
	"PDPMODE" bit null,
	"PDCHQNO" nvarchar(10) null,
	"PDCHQDT" date null,
	"PDCRBY" nvarchar(10) null,
	"PDCRDT" date null,
	"PDUPBY" nvarchar(10) null,
	"PDUPDT" date null,
	"PDRCID" integer null,
	"PDTRMD" bit null,
	PRIMARY KEY ("PDAN8", "PDCO", "PDID")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                Create table 'MobileDataModel.BUSDTA.M080111'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M080111"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M080111"     (
	"CTID" numeric(8, 0) not null,
	"CTTYP" nvarchar(10) null,
	"CTCD" nvarchar(5) not null,
	"CTDSC1" nvarchar(50) null,
	"CTTXA1" nvarchar(10) null,
	PRIMARY KEY ("CTID")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*                 Create table 'MobileDataModel.BUSDTA.M4016'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."M4016"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."M4016"     (
	"OPORTP" nvarchar(8) not null,
	"OPAN8" numeric(8, 0) not null,
	"OPOSEQ" numeric(4, 0) null,
	"OPITM" nvarchar(25) not null,
	"OPLITM" nvarchar(25) null,
	"OPQTYU" integer null,
	"OPUOM" nvarchar(2) null,
	"OPLNTY" nvarchar(2) null,
	"SRP1" nvarchar(3) null,
	"SRP5" nvarchar(3) null,
	"CSCRBY" nvarchar(10) null,
	"CSCRDT" date null,
	"CSUPBY" nvarchar(10) null,
	"CSUPDT" date null,
	"OPSTDT" date not null,
	PRIMARY KEY ("OPORTP", "OPAN8", "OPITM","OPSTDT")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*              Create table 'MobileDataModel.BUSDTA.Order_Detail'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."Order_Detail"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."Order_Detail"     (
	"Order_Detail_Id" integer not null  DEFAULT autoincrement,
	"Order_ID" integer null,
	"Item_Number" nvarchar(25) null,
	"Order_Qty" integer null,
	"Order_UOM" nvarchar(2) null,
	"Unit_Price" float null,
	"Extn_Price" float null,
	"Reason_Code" varchar(5) null,
	"IsTaxable" bit NULL,
	PRIMARY KEY ("Order_Detail_Id", "Order_ID")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*              Create table 'MobileDataModel.BUSDTA.Order_Header'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."Order_Header"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."Order_Header"     (
	"Order_ID" integer not null,
	"Customer_Id" float null,
	"Order_Date" date null,
	"Created_By" integer null,
	"Created_On" date not null,
	"Is_Deleted" bit null,
	"Total_Coffee" float null,
	"Total_Allied" float null,
	"Energy_Surcharge" float null,
	"Order_Total_Amt" float null,
	"Sales_Tax_Amt" float null,
	"Invoice_Total" float null,
	"Surcharge_Reason_Code" varchar(5) null,
	"payment_type" nvarchar(10) null,
	"payment_id" nvarchar(10) null,
	"Order_State" nvarchar(20) null,
	"Order_Sub_State" nvarchar(20) null,
	"updated_at" datetime null,
	"VoidReason" bigint null,
	"OrderSeries" integer null,
	"RouteNo" nvarchar(3) null,
	PRIMARY KEY ("Order_ID", "OrderSeries")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*            Create table 'MobileDataModel.BUSDTA.Payment_Ref_Map'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."Payment_Ref_Map"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."Payment_Ref_Map"     (
	"Payment_Ref_Map_Id" integer not null DEFAULT autoincrement,
	"Payment_Id" integer not null,
	"Ref_Id" integer not null,
	"Ref_Type" nvarchar(3) null,
	PRIMARY KEY ("Payment_Ref_Map_Id", "Payment_Id", "Ref_Id")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*          Create table 'MobileDataModel.BUSDTA.PickOrder_Exception'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."PickOrder_Exception"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception"     (
	"PickOrder_Exception_Id" integer not null  DEFAULT autoincrement,
	"Order_Id" integer null,
	"Item_Number" nvarchar(25) null,
	"Exception_Qty" integer null,
	"UOM" nvarchar(2) null,
	"Exception_Reason" varchar(25) null,
	"ManualPickReasonCode" integer null,
	"ManuallyPickCount" integer null,
	PRIMARY KEY ("PickOrder_Exception_Id")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*               Create table 'MobileDataModel.BUSDTA.PickOrder'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."PickOrder"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."PickOrder"     (
	"PickOrder_Id" integer not null  DEFAULT autoincrement,
	"Order_ID" integer null,
	"Item_Number" nvarchar(25) null,
	"Order_Qty" integer null,
	"Order_UOM" nvarchar(2) null,
	"Picked_Qty_Primary_UOM" integer null,
	"Primary_UOM" nvarchar(2) null,
	"Order_Qty_Primary_UOM" integer null,
	"On_Hand_Qty_Primary" integer null,
	"Last_Scan_Mode" bit null,
	"Item_Scan_Sequence" integer null,
	"Picked_By" integer null,
	"IsOnHold" integer null,
	"Reason_Code_Id" integer null,
	"ManuallyPickCount" integer null,
	PRIMARY KEY ("PickOrder_Id")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*            Create table 'MobileDataModel.BUSDTA.ReasonCodeMaster'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."ReasonCodeMaster"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster"     (
	"ReasonCodeId" integer not null,
	"ReasonCode" nvarchar(5) null,
	"ReasonCodeDescription" nvarchar(100) null,
	"ReasonCodeType" nvarchar(50) null,
	PRIMARY KEY ("ReasonCodeId")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*            Create table 'MobileDataModel.BUSDTA.Route_Device_Map'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."Route_Device_Map"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map"     (
	"Route_Id" varchar(8) not null,
	"Device_Id" varchar(30) not null,
	"Active" integer null,
	"Remote_Id" varchar(30) null,
	PRIMARY KEY ("Route_Id", "Device_Id")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*             Create table 'MobileDataModel.BUSDTA.Route_User_Map'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."Route_User_Map"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."Route_User_Map"     (
	"App_user_id" integer not null,
	"Route_Id" varchar(8) not null,
	"Active" integer null,
	"Default_Route" integer null,
	PRIMARY KEY ("App_user_id", "Route_Id")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*               Create table 'MobileDataModel.BUSDTA.UDCKEYLIST'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."UDCKEYLIST"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."UDCKEYLIST"     (
	"DTSY" nvarchar(4) not null,
	"DTRT" nvarchar(2) not null,
	PRIMARY KEY ("DTSY", "DTRT")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*              Create table 'MobileDataModel.BUSDTA.user_master'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."user_master"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."user_master"     (
	"App_user_id" integer not null,
	"App_User" varchar(30) null,
	"Name" varchar(30) null,
	"DomainUser" varchar(20) null,
	"AppPassword" varchar(20) null,
	"active" bit null default ((1)),
	"Created_On" datetime null default (getdate()),
	PRIMARY KEY ("App_user_id")
)
GO

COMMIT
GO
/*------------------------------------------------------------------------------
*             Create table 'MobileDataModel.BUSDTA.User_Role_Map'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."User_Role_Map"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."User_Role_Map"     (
	"App_user_id" integer not null,
	"Role" varchar(8) null,
	PRIMARY KEY ("App_user_id")
)
GO

COMMIT
GO

/*------------------------------------------------------------------------------
*            Create table 'MobileDataModel.BUSDTA.ReasonCodeMaster'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."ReasonCodeMaster"
GO
CREATE TABLE "MobileDataModel"."BUSDTA"."ReasonCodeMaster"     (
	"ReasonCodeId" integer not null,
	"ReasonCode" nvarchar(5) null,
	"ReasonCodeDescription" nvarchar(100) null,
	"ReasonCodeType" nvarchar(50) null,
	PRIMARY KEY ("ReasonCodeId")
)
GO

COMMIT
GO

/*------------------------------------------------------------------------------
*             Create table 'MobileDataModel.BUSDTA.SYNC_QUEUE'.
*-----------------------------------------------------------------------------*/
DROP TABLE IF EXISTS "MobileDataModel"."BUSDTA"."SYNC_QUEUE"
GO
Create Table "MobileDataModel"."BUSDTA"."SYNC_QUEUE"(
[transaction] varchar(50),
publicationProfile varchar(50),
priority  varchar(50)
)
COMMIT
GO
/*------------------------------------------------------------------------------
*            Function to convert gregorian date to julian date
*-----------------------------------------------------------------------------*/
CREATE or replace FUNCTION "BUSDTA"."DateG2J" (@julian varchar(6))
RETURNS datetime AS BEGIN
	DECLARE @datetime datetime

	SET @datetime = CAST(19+CAST(SUBSTRING(@julian, 1, 1) as int) as varchar(4))+SUBSTRING(@julian, 2,2)+'-01-01'
	SET @datetime = DATEADD(day, CAST(SUBSTRING(@julian, 4,3) as int)-1, @datetime)

	RETURN @datetime
END
/*------------------------------------------------------------------------------*/
GO

/*------------------------------------------------------------------------------
*            Function to get UDC Description from database
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE  FUNCTION BUSDTA.GetUDCDescription (@DRSY varchar(3), @DRRT varchar(3), @DRKY varchar(3))
RETURNS varchar(30)
AS BEGIN
	DECLARE @DRDL01 varchar(30)

	select  @DRDL01 = DRDL01  from BUSDTA.F0005 where  DRSY = @DRSY and DRRT = @DRRT and ltrim(DRKY) = @DRKY

	RETURN @DRDL01
END
/*------------------------------------------------------------------------------*/
GO

/*------------------------------------------------------------------------------
*            Function will return the Period for a given date with respect to BaseDate
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION "BUSDTA"."GetPeriodForDate" (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int
	DECLARE @varDiv28 float
	DECLARE @PeriodNumber float
	DECLARE @ModNumber float

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate)+1
	select @varDiv28 = CEILING (@DateDifference / 28.00)
	select @ModNumber = @varDiv28%3
	if(@ModNumber = 0)
		select @PeriodNumber = 3
	else
		select @PeriodNumber = @ModNumber
	RETURN cast(@PeriodNumber as int)
END
GO

/*------------------------------------------------------------------------------
*            Function will return the Week for a given date with respect to BaseDate.
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION "BUSDTA"."GetWeekForDate" (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int
	DECLARE @varDiv7 float
	DECLARE @WeekNumber float
	DECLARE @ModNumber float

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate)+1
	select @varDiv7 = CEILING (@DateDifference / 7.00)
	select @ModNumber = @varDiv7%4
	if(@ModNumber = 0)
		select @WeekNumber = 4
	else
		select @WeekNumber = @ModNumber
	RETURN cast(@WeekNumber as int)
END
GO

/*------------------------------------------------------------------------------
*            Function will return the Day, BaseDate argument can be removed
*-----------------------------------------------------------------------------*/
CREATE FUNCTION BUSDTA.GetDayForDate (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int

	select @DateDifference = DATEPART (DW, @CalendarDate)-1

	RETURN @DateDifference
END
GO

/*------------------------------------------------------------------------------
*            Function will return the description/name of the address book id that is passed to it
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION "BUSDTA"."GetABADescriptionFromABAId" (@ABAId numeric(8,0))
RETURNS nchar(40)
AS BEGIN
	DECLARE @ABALPH nchar(40)

	select @ABALPH =  ABALPH from BUSDTA.F0101 ab where ab.ABAN8 = @ABAId

	RETURN @ABALPH
END
GO

/*------------------------------------------------------------------------------
*            Function will return the default phone number from the list of phones 
*-----------------------------------------------------------------------------*/
create or replace function "BUSDTA"."GetDefaultPhone"( @CustomerId numeric(8) ) 
returns nchar(25) as
begin
  declare @PhoneNumber nchar(25)
  declare @PH1 bit
  declare @PH2 bit
  declare @PH3 bit
  declare @PH4 bit

  select @PH1 = "CDDFLTPH1" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1
  select @PH2 = "CDDFLTPH2" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1
  select @PH3 = "CDDFLTPH3" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1
  select @PH4 = "CDDFLTPH4" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1

  if @PH1 = 1
    select @PhoneNumber = "CDAR1"+"CDPH1" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1
  else if @PH2 = 1
    select @PhoneNumber = "CDAR2"+"CDPH2" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1
  else if @PH3 = 1
    select @PhoneNumber = "CDAR3"+"CDPH3" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1
  else if @PH4 = 1
    select @PhoneNumber = "CDAR4"+"CDPH4" from "BUSDTA"."M0111" where "CDAN8" = @CustomerId and "CDIDLN" = 0 and CDRCK7 = 1
  else
    select @PhoneNumber = 'NA'
  return @PhoneNumber
end

GO

/*------------------------------------------------------------------------------
*            Procedure to calculate energy surcharge
*-----------------------------------------------------------------------------*/
CREATE or replace  PROCEDURE "BUSDTA"."calculateEnergySurcharge" (CustomerNum varchar(10), OrderTotal float)
BEGIN
	DECLARE  FreightHandlingCode varchar(10);
	DECLARE  SpecialHandlingCode varchar(10);
	DECLARE  DescriptionCode varchar(10);
	DECLARE  HandlingCodeLength INTEGER ;

	Declare str_drsy varchar(10);
	Declare str_drrt varchar(10);
	Declare UPto varchar(10);
	Declare surcharge varchar(10);

	Select ltrim(rtrim(AIFRTH)) into FreightHandlingCode from busdta.F03012 where aian8 = CustomerNum;
	IF FreightHandlingCode = '' or FreightHandlingCode is null THEN
		select 0 from dummy;
	END IF;

	select  ltrim(rtrim(DRDL02)), ltrim(rtrim(DRSPHD)) into DescriptionCode, SpecialHandlingCode from busdta.F0005 where DRSY = '42' and DRRT = 'FR' and DRKY like '%'+FreightHandlingCode+'';
	
	IF DescriptionCode = 'MB|STATIC' THEN
		select SpecialHandlingCode from dummy;
	ELSEIF DescriptionCode = 'MB|UDC' THEN
		set str_drsy = substring(SpecialHandlingCode, 1, charindex('|',SpecialHandlingCode)-1);
		set str_drrt = substring(SpecialHandlingCode,  charindex('|',SpecialHandlingCode)+1);
		select  DRDL01 as UPto ,  DRDL02  as surcharge  into #tbdummysurcharge  from busdta.F0005 where DRSY = ''+str_drsy+'' and DRRT = ''+str_drrt+'';
			PRINT 'Procedure called successfully' ;
			IF OrderTotal <= 100 THEN
				select surcharge from #tbdummysurcharge where Upto <=100;
			ELSEIF OrderTotal > 100 AND OrderTotal <= 250 THEN
				select surcharge from #tbdummysurcharge where Upto >100 and Upto <=250
			ELSE
				select surcharge from #tbdummysurcharge where Upto >250
			END IF;
	ELSEIF DescriptionCode like '' THEN
		PRINT 'Procedure called successfully' ;
		select 0 from dummy;
	ELSE
		PRINT 'Procedure called successfully' ;
		if OrderTotal <= 100 then
		  select 45 from dummy
		elseif OrderTotal > 100 and OrderTotal <= 250 then
		  select 30 from dummy
		else
		  select 0 from dummy
		end if
	END IF;
END
/*------------------------------------------------------------------------------*/
GO
/*------------------------------------------------------------------------------
*             Procedure to pull history records for given customer
*-----------------------------------------------------------------------------*/

CREATE or replace  PROCEDURE "BUSDTA"."prepareHistoryRecords"(@shipTo varchar(25))
BEGIN
declare i nvarchar(2);
declare j nvarchar(2);
declare countHist integer;
declare strQuery nvarchar(20000);
drop table if exists busdta.localLastOrders; 
select NUMBER(*) as chronology,SHKCOO,SHDOCO,SHDCTO,SHTRDJ into busdta.localLastOrders from BUSDTA.F42019 
 where 
SHSHAN=@shipTo and 
SHDCTO='SO'  ORDER BY SHTRDJ DESC;
/* Order Item Last */
drop table if exists busdta.localLastItems;
select NUMBER(*)as ranking,SDLITM into busdta.localLastItems from busdta.localLastOrders join busdta.F42119 on SHKCOO=SDKCOO and SHDCTO=SDDCTO and SHDOCO=SDDOCO
 where chronology<=10 and SDLNTY='S ' GROUP BY SDLITM Order by Count(*)Desc,SDLITM;
/* Order History Panel Data */
drop table if exists busdta.localOrderHistoryPanel;
select count(*) into countHist from busdta.localLastOrders;
	IF countHist<> null or countHist<>'' THEN
		set strQuery = 'select im.imlitm,im.imdsc1, ';
		set i=1;
		WHILE i<=countHist LOOP
			set strQuery = strQuery+'h'+i+'d.sdsoqs/10000 as H'+i+'_Qty';
		   IF i <> countHist THEN
				set strQuery = strQuery+' , ';
			END IF;
			set i = i+1;
		END LOOP;
	
		set strQuery = strQuery+' into busdta.localOrderHistoryPanel from busdta.localLastItems li ';
		set strQuery = strQuery+'join busdta.F4101 im on li.SDLITM=im.IMLITM  ';
		set j=1;
		WHILE j<=countHist LOOP
			set strQuery = strQuery+' join busdta.localLastOrders h'+j+' on h'+j+'.chronology='+j ;
			set strQuery = strQuery+' left join busdta.F42119 h'+j+'d on h'+j+'.SHKCOO=h'+j+'d.SDKCOO and h'+j;
			set strQuery = strQuery+' .SHDCTO=h1d.SDDCTO and h1.SHDOCO=h'+j+'d.SDDOCO and im.imlitm=h'+j+'d.sdlitm ';
		set j = j+1;
		END LOOP;
		set strQuery = convert(varchar, strQuery||' Order By ranking;');
	ELSE 
		set strQuery = 'select 0 into busdta.localOrderHistoryPanel from dummy;';
	END IF;
--select strQuery from dummy;
	Execute Immediate strQuery;
END
/*------------------------------------------------------------------------------*/
GO
/*------------------------------------------------------------------------------
*             Procedure to save order invoice for reports
*-----------------------------------------------------------------------------*/

CREATE or replace  PROCEDURE "BUSDTA"."SaveMasterInvoice"( @InvoiceNo varchar(10),@OrderQty integer ,@UM varchar(2) ,@ItemCode varchar(10) ,@ProductDesc varchar(30),
@UnitPrice float,@ExtendedPrice float,@OrderDate varchar(50), @SalesCat1 varchar(3)
 )
/* RESULT( column_name column_type, ... ) */
BEGIN
	insert into BUSDTA.InvoiceDetails(
InvoiceNo,
OrderQty,
UM,
ItemCode,
ProductDesc,
UnitPrice,
ExtendedPrice,
OrderDate,
SalesCat1
)values(
@InvoiceNo,
@OrderQty,
@UM,
@ItemCode,
@ProductDesc,
@UnitPrice,
@ExtendedPrice,
@OrderDate,
@SalesCat1
);
RETURN  1;
END

GO

/*--------------------------------------------------------------------------------------------
*             This procedure, takes Customer Number, Selcted Date and BaseDate as its input and 
*				  generates a list of valid Next Stop Dates for  the respective customer.
*			  This procedure is used to generate the next stop for Pre-Order Pop-up screen.
*--------------------------------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE "BUSDTA"."getPreOrderDatesForCustomer" (In CustomerNum numeric(8,0), In SelectedDate date,In BaseDate date)
BEGIN
	DECLARE  PeriodNumber numeric(2,0); -- Period Number for SelectedDate
	DECLARE  WeekNumber numeric(2,0); -- Week Number for SelectedDate
	DECLARE  DayNumber numeric(2,0); -- Day Number for SelectedDate
    DECLARE i INTEGER = 1;
    DECLARE nextDate date;
    /*This cursor gets the pattern, for a Delivery Day Code of the customer.*/
    DECLARE cur_DayCodePattern CURSOR FOR
      SELECT DCPN, DCWN, DCDN
      FROM BUSDTA.M56M0002 
      WHERE DCDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerNum) and DCPN <>0;

    DECLARE  Period numeric(4,0); -- Period Number for Delivery Day Code
    DECLARE  Week numeric(4,0); -- Week Number for Delivery Day Code
    DECLARE  Day numeric(4,0); -- Day Number for Delivery Day Code

    DECLARE LOCAL TEMPORARY TABLE TempTab ( val as nchar(50) ); -- Temporary table to store the next dates
    
    /*Outer loop: Generates the Calendar dates from the Base date. These dates will be compared to the Pattern dates.*/    
    lp: LOOP
        IF i = 1000 THEN LEAVE lp END IF;
        select dateadd(day, i, BaseDate) into nextDate from dummy;

        /*Get the Period, Week and Day number for the given date, with respect to BaseDate*/
        select busdta.GetPeriodForDate(nextDate, BaseDate) into PeriodNumber from dummy;
        select busdta.GetWeekForDate(nextDate, BaseDate) into WeekNumber from dummy;
        select busdta.GetDayForDate(nextDate, BaseDate) into DayNumber from dummy ;
    
        /*Inner Loop: This loop generates the dates for pattern of Delivery Day Code, to be compared with the calendar dates.*/
        OPEN cur_DayCodePattern;    
        ilp: LOOP
            FETCH next cur_DayCodePattern into Period, Week, Day;
                IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                    /*If Calendar date mathes the Pattern dates, it is a qualified date.*/
                    IF Period = PeriodNumber AND Week = WeekNumber AND Day = DayNumber THEN
                        /*If qualified date is greater than than the selected date, then it si the next valid date.*/
                        IF SelectedDate < nextDate THEN
                            insert into busdta.TempTab values(nextDate);
                        END IF;
                    END IF;
        END LOOP; -- End Inner Loop    
        CLOSE cur_DayCodePattern;

        SET i = i + 1;
    END LOOP; -- End Outer Loop
    select val from TempTab; -- All valid next dates.
END

GO
/*------------------------------------------------------------------------------
*                         Grant all rights to user busdta.
*-----------------------------------------------------------------------------*/
grant all to BUSDTA;
/*------------------------------------------------------------------------------
*                         Create publication 'pub_validate_user'.
*-----------------------------------------------------------------------------*/
CREATE PUBLICATION IF NOT EXISTS "pub_validate_user" (
	TABLE "MobileDataModel"."BUSDTA"."User_Role_Map",
	TABLE "MobileDataModel"."BUSDTA"."user_master",
	TABLE "MobileDataModel"."BUSDTA"."Route_User_Map",
	TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map",
	TABLE "MobileDataModel"."BUSDTA"."Device_Master"
)
GO

/*------------------------------------------------------------------------------
*                           Create the user 'validate_user'.
*------------------------------------------------------------------------------*/
CREATE SYNCHRONIZATION USER "validate_user"
GO

/*------------------------------------------------------------------------------
*         Create subscription 'subs_validate_user' to 'pub_validate_user' for 'validate_user'.
*-----------------------------------------------------------------------------*/
CREATE SYNCHRONIZATION SUBSCRIPTION "subs_validate_user" TO "pub_validate_user" FOR "validate_user"
	TYPE tcpip ADDRESS 'host=localhost;port=2439'
	OPTION lt='off'
	SCRIPT VERSION 'SLE_RemoteDB'
GO
/*------------------------------------------------------------------------------
*                Create synchronization profile 'subs_validate_user'.
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE SYNCHRONIZATION PROFILE "prof_validate_user" 'Subscription=subs_validate_user'
GO

COMMIT
GO

COMMIT
GO
