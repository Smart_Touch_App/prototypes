﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Telerik.Windows.Controls;

namespace TelerikTheme
{
    public static class Extensions
    {
        public static bool HasData(this DataSet data)
        {
            if (!(data == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0))
            {
                return true;
            }
            return false;
        }
        public static List<T> GetEntityList<T>(this DataSet ds) where T : new()
        {
            List<T> entityList = new List<T>();
            var castToEntity = new T();
            var properties = typeof(T).GetProperties();
            PropertyInfo[] propertiess = (castToEntity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if (ds.HasData())
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    var entity = new T();
                    foreach (DataColumn col in row.Table.Columns)
                    {
                        var tru = properties.FirstOrDefault(i => i.Name == col.ColumnName);
                        if (tru != null)
                        {
                            if (tru.PropertyType == typeof(string))
                            {
                                tru.SetValue(entity, row[tru.Name].ToString());
                                continue;
                            }
                            if (tru.PropertyType == typeof(DateTime))
                            {
                                if (row[tru.Name] != DBNull.Value)
                                {
                                    tru.SetValue(entity, Convert.ToDateTime(row[tru.Name]));
                                }
                                continue;
                            }
                            if (tru.PropertyType == typeof(double))
                            {
                                tru.SetValue(entity, Convert.ToDouble(row[tru.Name]));
                                continue;
                            }
                            if (tru.PropertyType == typeof(int))
                            {
                                tru.SetValue(entity, Convert.ToInt32(row[tru.Name]));
                                continue;
                            }
                            if (tru.PropertyType == typeof(decimal))
                            {
                                tru.SetValue(entity, Convert.ToDecimal(row[tru.Name]));
                                continue;
                            }

                            tru.SetValue(entity, row[tru.Name]);
                        }
                    }
                    entityList.Add(entity);
                }
            }

            return entityList;
        }
    }
    public class MainViewModel : ViewModelBase, IDataErrorInfo
    {
        public ObservableCollection<PriceOvrCodes> BindPriceOvr = new ObservableCollection<PriceOvrCodes>();





        public List<OrderItem> MockData { get; set; }
        string[] str = {"CS",
                            "LY",
                            "P1",
                            "P2",
                            "PL",
                            "E2",
                            "FC",
                            "GW",
                            "HT",
                            "LB",
                            "LC",
                            "LG",
                            "LP",
                            "VC",
                            "VP",
                            "WD"};
        public MainViewModel()
        {
            DataSet queryResult = new DataSet();
            queryResult.ReadXml("OrderItems.xml");

            MockData = queryResult.GetEntityList<OrderItem>();

            for (int index = 0; index < MockData.Count(); index++)
            {
                for (int nextindex = 0; nextindex < str.Count(); nextindex++)
                {
                    MockData[index].AppliedUMS.Add(str[nextindex]);
                }
            }
        }




        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }
    }
    public class OrderItem : ViewModelBase, IDataErrorInfo
    {
        string _ItemNumber, _UM, _UMPrice, _ItemDescription, _SalesCat1, _SalesCat4, _SalesCat5, _StkType, _ShortDesc, _PrimaryUM;
        decimal _ExtendedPrice, _UnitPrice, _OldUnitPrice;
        private int _QtyOnHand;
        double _AverageStopQty = 0;
        public static bool ValidationEnabled = false, IsValidSelectedIndex = false;
        List<string> _AppliedUMS = new List<string>();
        string _IsTaxable;
        static string ErrorText = "", ColumnName = "";
        int _SelectedIndex = 0;
        private ObservableCollection<PriceOvrCodes> OvrCodes = new ObservableCollection<PriceOvrCodes>();


        public ObservableCollection<PriceOvrCodes> OVRCODES
        {
            get { return OvrCodes; }
            set { OvrCodes = value; OnPropertyChanged("OVRCODES"); }

        }

        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set { _SelectedIndex = value; OnPropertyChanged("SelectedIndex"); }
        }
        private string _SelectedReasonCode;
        public string SelectedReasonCode
        {
            get
            {
                return _SelectedReasonCode;
            }
            set
            {
                _SelectedReasonCode = value;
                OnPropertyChanged("SelectedReasonCode");
            }
        }
        public string IsTaxable
        {
            get { return _IsTaxable; }
            set
            {
                _IsTaxable = value;
                OnPropertyChanged("IsTaxable");
            }

        }
        public List<string> AppliedUMS
        {
            get { return _AppliedUMS; }
            set { _AppliedUMS = value; }
        }

        public double AverageStopQty
        {
            get { return _AverageStopQty; }
            set
            {
                _AverageStopQty = value;
                OnPropertyChanged("AverageStopQty");
            }
        }

        public int QtyOnHand
        {
            get
            {

                return _QtyOnHand;

            }
            set
            {
                _QtyOnHand = value;
                OnPropertyChanged("QtyOnHand");  // Trigger the change event if the value is changed!

            }
        }
        public string ItemNumber
        {
            get
            {
                return _ItemNumber;
            }
            set
            {
                _ItemNumber = value;
                OnPropertyChanged("ItemNumber");
            }
        }

        public string ItemDescription
        {
            get
            {
                return _ItemDescription;
            }
            set
            {
                _ItemDescription = value;
                OnPropertyChanged("ItemDescription");
            }
        }
        public string SalesCat1
        {
            get
            {
                return _SalesCat1;
            }
            set
            {
                _SalesCat1 = value;
                OnPropertyChanged("SalesCat1");
            }
        }
        public string SalesCat5
        {
            get
            {
                return _SalesCat5;
            }
            set
            {
                _SalesCat5 = value;
                OnPropertyChanged("SalesCat5");
            }
        }
        public string SalesCat4
        {
            get
            {
                return _SalesCat4;
            }
            set
            {
                _SalesCat4 = value;
                OnPropertyChanged("SalesCat5");
            }
        }
        public string StkType
        {
            get
            {
                return _StkType;
            }
            set
            {
                _StkType = value;
                OnPropertyChanged("StkType");
            }
        }
        public string UM
        {
            get
            {
                return _UM;
            }
            set
            {
                _UM = value;
                OnPropertyChanged("UM");
                this.OnPropertyChanged("ExtendedPrice");

            }
        }
        public string UMPrice
        {
            get
            {
                return _UMPrice;
            }
            set
            {
                _UMPrice = value;
                OnPropertyChanged("UMPrice");
            }
        }
        public decimal ExtendedPrice
        {
            get
            {
                return _ExtendedPrice;
            }
            set
            {
                _ExtendedPrice = value;
                OnPropertyChanged("ExtendedPrice");
            }
        }
        public decimal UnitPrice
        {
            get
            {
                return _UnitPrice;
            }
            set
            {
                _OldUnitPrice = _UnitPrice;
                _UnitPrice = value;
                OnPropertyChanged("UnitPrice");
                OnPropertyChanged("ExtendedPrice");
                OnPropertyChanged("OVRCODES");

            }
        }
        public string ShortDesc
        {
            get
            {
                return _ShortDesc;
            }
            set
            {
                _ShortDesc = value;
                OnPropertyChanged("ShortDesc");
            }
        }

        public string PrimaryUM
        {
            get
            {
                return _PrimaryUM;
            }
            set
            {
                _PrimaryUM = value;
                OnPropertyChanged("PrimaryUM");
            }
        }



        public static Dictionary<string, string> CheckErrors()
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            results.Add(ColumnName, ErrorText);

            return results;
        }


        public string Error
        {
            get { return ""; }
        }

        public string this[string columnName]
        {
            get
            {
                ColumnName = columnName;
                if (ValidationEnabled)
                    if ((_OldUnitPrice != _UnitPrice) && columnName == "OVRCODES")
                    {
                        if (SelectedIndex == 0)
                        {
                            ErrorText = "Please select valid Price Override reason";
                            return ErrorText;
                        }
                    }
                if (SelectedIndex > 0)
                {
                    IsValidSelectedIndex = true;
                    return "";
                }
                return ErrorText;
            }
        }


    }
    public class PriceOvrCodes
    {
        public PriceOvrCodes(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        public PriceOvrCodes() { }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}



