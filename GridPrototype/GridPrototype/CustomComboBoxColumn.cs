﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using TelerikTheme;
namespace TelerikTheme
{
    public class CustomComboBoxColumn : GridViewBoundColumnBase
    {
        MockDataForComboBox md = new MockDataForComboBox();
        public CustomComboBoxColumn()
        {

            md.BindPriceOvr.Add(new PriceOvrCodes(1, ""));
            md.BindPriceOvr.Add(new PriceOvrCodes(2, "CPR"));
            md.BindPriceOvr.Add(new PriceOvrCodes(3, "PKN"));
            md.BindPriceOvr.Add(new PriceOvrCodes(4, "SMP"));
            md.BindPriceOvr.Add(new PriceOvrCodes(5, "TPR"));
        }
        //public override void CopyPropertiesFrom(Telerik.Windows.Controls.GridViewColumn source)
        //{
        //    base.CopyPropertiesFrom(source);
        //    var radColorPickerColumn = source as RadColorPickerColumn;
        //    if (radColorPickerColumn != null)
        //    {
        //        this.MainPalette = radColorPickerColumn.MainPalette;
        //    }
        //}

        public override FrameworkElement CreateCellElement(GridViewCell cell, object dataItem)
        {
            TextBlock CellBlock = new TextBlock();
            //var valueBinding = new Binding(OrderItem.S)
            //    {
            //        Mode = BindingMode.TwoWay,
            //    };
            //CellBlock.SetBinding(Border.BackgroundProperty, valueBinding);
            CellBlock.Width = 45;
            CellBlock.Height = 20;
            return CellBlock;
        }

        public override FrameworkElement CreateCellEditElement(GridViewCell cell, object dataItem)
        {

            var cellEditElement = new RadComboBox();
            cellEditElement.DisplayMemberPath= "Name";
            var bindings = new Binding("OVRCODES")
            {
                Mode = BindingMode.TwoWay,
                ValidatesOnDataErrors = true,
                NotifyOnValidationError = true
            };
            //cellEditElement.ItemsSource = md.BindPriceOvr;
            cellEditElement.SelectedValuePath = "Name";
            cellEditElement.SetBinding(RadComboBox.ItemsSourceProperty, bindings);
            return cellEditElement as FrameworkElement;
        }

		private Binding CreateValueBinding()
		{
			Binding valueBinding = new Binding();
            valueBinding.Mode = BindingMode.TwoWay;
            valueBinding.NotifyOnValidationError = true;
            valueBinding.ValidatesOnExceptions = true;
            valueBinding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;
            valueBinding.Path = new PropertyPath(this.DataMemberBinding.Path.Path);
			return valueBinding;
		}

        public override object GetNewValueFromEditor(object editor)
        {
            RadComboBox cmb = editor as RadComboBox;
            if (cmb != null)
            {
                return cmb.SelectedValue;
            }
            else
            {
                return null;
            }
        }
       
        public override IList<string> UpdateSourceWithEditorValue(GridViewCell gridViewCell)
        {
            List<ValidationError> Validationerrors = new List<ValidationError>();
            RadComboBox editor = gridViewCell.GetEditingElement() as RadComboBox;
            BindingExpression bindingExpression = editor.ReadLocalValue(RadComboBox.SelectedValueProperty) as BindingExpression;
            if (bindingExpression != null)
            {
                bindingExpression.UpdateSource();
                Validationerrors.AddRange(Validation.GetErrors(editor));
            }
            List<string> errors = new List<string>();
            foreach (ValidationError error in Validationerrors)
            {
                string strError;
                strError = error.ErrorContent.ToString();
                errors.Add(strError);
            }
            return errors;
        }



     
    }
    public class MockDataForComboBox : INotifyPropertyChanged
    {
        private ObservableCollection<PriceOvrCodes> _BindPriceOvr = new ObservableCollection<PriceOvrCodes>();

        public ObservableCollection<PriceOvrCodes> BindPriceOvr
        {
            get
            {
                return _BindPriceOvr;
            }
            set
            {
                _BindPriceOvr = value;
                OnPropertyChanged("BindPriceOvr");
                throw new ArgumentException("Custom Thrown exception");
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
