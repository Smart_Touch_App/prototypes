﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Runtime.InteropServices;


namespace TelerikTheme
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        MainViewModel model = new MainViewModel();
        bool CellHasChanges = false;
        FrameworkElement frameworkElement = new FrameworkElement();

        public MainWindow()
        {
            InitializeComponent();

            for (int index = 0; index < model.MockData.Count(); index++)
            {

                model.MockData[index].OVRCODES.Add(new PriceOvrCodes(1, ""));
                model.MockData[index].OVRCODES.Add(new PriceOvrCodes(2, "CPR"));
                model.MockData[index].OVRCODES.Add(new PriceOvrCodes(3, "PKN"));
                model.MockData[index].OVRCODES.Add(new PriceOvrCodes(4, "SMP"));
                model.MockData[index].OVRCODES.Add(new PriceOvrCodes(5, "TPR"));
            }
            model.BindPriceOvr.Add(new PriceOvrCodes(1, ""));
            model.BindPriceOvr.Add(new PriceOvrCodes(2, "CPR"));
            model.BindPriceOvr.Add(new PriceOvrCodes(3, "PKN"));
            model.BindPriceOvr.Add(new PriceOvrCodes(4, "SMP"));
            model.BindPriceOvr.Add(new PriceOvrCodes(5, "TPR"));
            this.DataContext = model;
            combogrid.DataContext = model;

        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {

        }

        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {

        }

        private void grditems_RowValidating(object sender, Telerik.Windows.Controls.GridViewRowValidatingEventArgs e)
        {

            e.IsValid = false;

            RadGridView gridviewObj = sender as RadGridView;

            Dictionary<string, string> GetErrors = new Dictionary<string, string>();
            GetErrors = OrderItem.CheckErrors();
            if (CellHasChanges && !OrderItem.IsValidSelectedIndex)
            {
                e.IsValid = false;
                foreach (KeyValuePair<string, string> error in GetErrors)
                {
                    GridViewCellValidationResult r = new GridViewCellValidationResult();
                    r.PropertyName = error.Key;
                    r.ErrorMessage = error.Value;
                    e.ValidationResults.Add(r);

                    OrderItem.ValidationEnabled = true;

                }
                if (gridviewObj.CurrentColumn.UniqueName == "UnitPrice")
                {

                    keybd_event(0x09, 0, 0, (UIntPtr)0);
                    keybd_event(0x09, 0, 0x2, (UIntPtr)0);
                    gridviewObj.CurrentCell.IsCurrent = false;
                }



            }
            if (OrderItem.IsValidSelectedIndex)
            {
                e.IsValid = true;
                OrderItem.IsValidSelectedIndex = false;
            }
        }

       
        private void grditems_RowEditEnded(object sender, Telerik.Windows.Controls.GridViewRowEditEndedEventArgs e)
        {

        }

        private void grditems_CellEditEnded(object sender, Telerik.Windows.Controls.GridViewCellEditEndedEventArgs e)
        {

            if (e.Cell.Column.UniqueName == "UnitPrice")
            {
                double oldData = Convert.ToDouble(e.OldData);
                double newData = Convert.ToDouble(e.NewData);
                if (oldData != newData)
                {
                    CellHasChanges = true;
                }
            }
           


        }

        private void grditems_BeginningEdit(object sender, Telerik.Windows.Controls.GridViewBeginningEditRoutedEventArgs e)
        {

        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void grditems_Drop(object sender, DragEventArgs e)
        {

        }

        private void grditems_CellValidating(object sender, Telerik.Windows.Controls.GridViewCellValidatingEventArgs e)
        {
            
        }

        private void grditems_AddingNewDataItem(object sender, Telerik.Windows.Controls.GridView.GridViewAddingNewEventArgs e)
        {

        }

        private void grditems_RowActivated(object sender, Telerik.Windows.Controls.GridView.RowEventArgs e)
        {

        }


        private void grditems_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void PriceOvr_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void grditems_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void grditems_RowValidated(object sender, GridViewRowValidatedEventArgs e)
        {

        }

        private void grditems_CellValidated(object sender, GridViewCellValidatedEventArgs e)
        {
           
        }
    }

}
